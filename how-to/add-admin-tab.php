<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/*
 * How to Add your own admin tab to WPSuperDealer - Sample code
*/

/*
 * You need to namespace all your functions
 * In this example we "my" as the prefix to everything
 * This first function hooks into the settings map
 * It then adds our new tab using the HTML output by the 'content' parameter
*/
function my_wpsd_settings_map_filter( $tabs ) {
    //= Add our array key and the data to populate our tab.
    $tabs['my_tab'] = array(
        'label' => __( 'My Tab', 'wp-super-dealer' ),
        'content' => my_tab_content(),
        'action' => 'save',
        'save_button' => 'Click Me',
    );
    
    return $tabs;
}
add_filter( 'wpsd_settings_map_filter', 'my_wpsd_settings_map_filter', 10, 1 );

/*
 * This function is called to populate the content for our tab
 * We use the WP_SUPER_DEALER\WPSD_FIELDS to generate the HTML for our fields
*/
function my_tab_content() {
    //= Create a variable to store our HTML so we can return it later
	$html = '';
    
    //= Provide a filter for our HTML so other people can bypass it if they want
	$html = apply_filters( 'wpsd_my_settings_filter', $html );
    
    //= If someone has already created some HTML then let's stop here, use that and return
	if ( ! empty( $html ) ) {
		return $html;
	}

    //= Get the global WPSuperDealer options
	global $wpsd_options;
    
    //= Set our form fields class to a variable so we can use it
	$form_fields = new WP_SUPER_DEALER\WPSD_FIELDS;
    
    //= Use time() to generate a unique number for each field's id
    //= Alternatively you could just add [] and get the keys in order
    //= But I like making sure the keys are really unique and if this class
    //= is used more than once on a page then this will help insure they
    //= are all unique
	$time = time();

    //= Create an array of the fields we want to add
	$fields = array(
		$time + 1 => array(
			'label' => __( 'My Setting', 'wp-super-dealer' ),
			'slug' => 'my_setting',
			'type' => 'text',
			'value' => ( isset( $wpsd_options['my_setting'] ) ? $wpsd_options['my_setting'] : __( 'My Default Value', 'wp-super-dealer' ) ),
			'class' => '',
			'tip' => array(
				'title' => __( 'Tip title', 'wp-super-dealer' ),
				'content' => __( 'SLUG-TIP', 'wp-super-dealer' ),
			),
			'placeholder' => 'My Placeholde',
		),
	);

    //= Create a label for our section
	$label = __( 'My Options', 'wp-super-dealer' );
    
    //= Get the HTML for our $fields array
	$content = $form_fields->get_form_fields( $fields );

    //= Put the wrapper around everything so it looks consistent
	$html = wpsd_admin_group_wrap( $label, $content, $fields );
    
    return $html;
}

/*
 * Now we need a function to save our data
 * The function name must be wpsd_{your_tab_slug}_settings_save()
 * your_slug_tab is the array key name you used in the first function
 *
*/
function wpsd_my_tab_settings_save( $args ) {
	//= Get the global settings so we can update them
	global $wpsd_options;
    //= Check the first array and see if there were fields sent
	if ( ! isset( $args[0]['fields'] ) ) {
		return __( 'No fields found to update.', 'wp-super-dealer' );
	}
    //= Loop the fields
	foreach( $args['fields'] as $field=>$value ) {
		//= Swap dashes for underscores, because underscores are cool ;-)
		$field = str_replace( '-', '_', $field );
        //= Add our value to the WPSuperDealer settings array
		$wpsd_options[ $field ] = sanitize_text_field( $value );
	}
	//= Update the options
	update_option( 'wpsd_options', $wpsd_options );

    //= Send back a message to output on the screen, dance with muppets, order pizza, etc.
	return __( 'Settings Saved', 'wp-super-dealer' );
}
?>