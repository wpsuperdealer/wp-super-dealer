<?php
//klaatu barada nikto?
/*

                        ______________
                __..=='|'   |         ``-._
   \=====_..--'/'''    |    |              ``-._
   |'''''      ```---..|____|_______________[)>.``-.._____
   |\_______.....__________|____________     ''  \      __````---.._
 ./'     /.-'_'_`-.\       |  ' '       ```````---|---/.-'_'_`=.-.__```-._
 |.__  .'/ /     \ \`.      \                     | .'/ /     \ \`. ```-- `.
  \  ``|| |   o   | ||-------\-------------------/--|| |   o   | ||--------|
   "`--' \ \ _ _ / / |______________________________| \ \ _ _ / / |..----```
          `-.....-'                                    `-.....-'
                                            ,   ,
             ,    ,    /\   /\             /(   )\
            /( /\ )\  _\ \_/ /_            \ \_/ /   , /\ ,
            |\_||_/| < \_   _/ >           /_   _\  /| || |\
            \______/  \|X\"/X|/           | \> </ | |\_||_/|
              _\/_   _(_  ^  _)_          (_  ^  _)  \____/
             ( () ) /`\|V"""V|/`\       /`\|IIIII|/`\ _\/_
               {}   \  \__ __/  /       \  \_____/  /  ()
               ()   /\   )V(   /\       /\   )=(   /\  ()
               {}  /  \_/\=/\_/  \     /  `-.\=/.-'  \ ()

"Don't believe everything you read on the Internet." - Abraham Lincoln

"In every moment, there is the possibility of a better future. 
But you people won't believe it. 
And because you won't believe it, you won't do what is necessary to make it a reality. 
So you dwell on this all-terrible future and resign yourselves to it for one reason: 
Because that future doesn't ask anything of you today."
-- David Nix / Hugh Laurie, "Tomorrowland"

We like being pessimists when it comes to our future. 
When we imagine a brighter future, then we are responsible for doing what is necessary to create it.
But when we imagine a bleaker future, there's nothing we have to do to make it a reality.
We can just live as hedonists until our passing.
Is that the legacy you're happy with?

There's 10 kinds of people in the world. 
Those who understand binary and those who don't.

01001010 01100001 01111001 00100000 01000011 01101000 01110101 01100011 01101011
00100000 01001101 01100001 01101001 01101100 01100101 01101110 00001010 01101000 
01110100 01110100 01110000 01110011 00111010 00101111 00101111 01100101 01101100 
01100111 01100101 01100101 01101011 01101111 00101110 01100011 01101111 01101101
*/
?>