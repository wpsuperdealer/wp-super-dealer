// JavaScript Document
jQuery( document ).ready( function( $ ) {
	//= TO DO Future: make constant - set to false
	var wpsd_admin_log_on = true;
	
	var wpsd_setCookie = function( cname, cvalue, exdays ) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays*24*60*60*1000));
		var expires = 'expires=' +  d.toUTCString();
		document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
	};

	var wpsd_getCookie = function( cname ) {
		var name = cname + '=';
		var decodedCookie = decodeURIComponent(document.cookie);
		var ca = decodedCookie.split( ';' );
		for(var i = 0; i <ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ' ) {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return '';
	};

	var wpsd_manage_photos = function( post_id, attachment ) {
		var nonce = $( '#admin-edit-vehicle-nonce' ).val();
		jQuery.ajax({
			type: 'POST',
			data: {'post_id': post_id, 'action': 'wpsd_image_handler', 'attachment_id': attachment, 'option': 'add_vehicle_images', 'nonce': nonce},
			url: wpsdAdminParams.ajaxurl,
			timeout: 5000,
			error: function() {},
			dataType: "html",
			success: function(html){
				var new_body = html;
				var image_div = document.getElementById("vehicle_photo_attachments").innerHTML;
				document.getElementById("vehicle_photo_attachments").innerHTML = image_div + html;
			}
		})
		return false;
	};

	var wpsd_manage_vehicle_photos = function() {
        var custom_uploader = wp.media({
        	id: 'wpsd-frame',
            title: wpsdAdminParams.str_manage_vehicle_photos,
            editing:   true,
            multiple:  true,
            library: {
	            type: 'image'
            },
            button: {
                text: wpsdAdminParams.str_attach_photos_to_vehicle
            },
        })
        .on( 'select', function() {
			var selection = custom_uploader.state().get( 'selection' );
            var post_id = document.getElementById( 'attachment_post_id' ).value;
     		selection.map( function( attachment ) {
                attachment = attachment.toJSON();
                // Do something with attachment.id and/or attachment.url here
				wpsd_manage_photos(post_id, attachment.id);
			});
        })
        .open();
    };

	var wpsd_update_image_order = function( linked, attached ) {
		var nonce = $( '#admin-edit-vehicle-nonce' ).val();
		linked = JSON.stringify( linked );
		attached = JSON.stringify( attached );
		var post_id = $( '#vehicle_photo_links' ).data( 'post-id' );
		$.ajax({
			type: 'POST',
			data: {'action': 'wpsd_update_image_order', 'post_id': post_id, 'linked': linked, 'attached': attached, 'nonce': nonce},
			url: wpsdAdminParams.ajaxurl,
			timeout: 15000,
			error: function() {},
			dataType: "html",
			success: function( html ) {
				wpsd_admin_log( html );
			}
		});
	};

	var wpsd_get_image_order = function() {
		var linked = {};
		var link_list = '';
		var attached = {};
		var cnt = 0;
		var src = '';
		var type = 'linked';
		$( '.admin_box_vehicle_photo' ).each( function( i, e ) {
			++cnt;
			src = $( e ).data( 'src' );
			type = $( e ).data( 'type' );
			if ( 'linked' === type ) {
				linked[ cnt ] = src;
				link_list = link_list + ',' + src;
			} else {
				attached[ cnt ] = src;
			}
			link_list = '##' + link_list;
			link_list = link_list.replace( '##,', '' );
			link_list = link_list.replace( '##', '' );
			$( '.wpsd_image_links_list' ).val( link_list );
		});
		wpsd_update_image_order( linked, attached );
	};

	var wpsd_update_image_links = function( image_links ) {
		$( '.wpsd_image_links_list' ).css( 'background-color', '#ffc' );
		post_id = $( '#vehicle_photo_links' ).data( 'post-id' );
		var nonce = $( '#wpsd_update_image_links_nonce' ).val();
		var valid = wpsd_validate_image_urls( image_links );
		if ( false === valid ) {
			$( '.wpsd_image_links_list' ).css( 'background-color', '#fdd' );
			wpsd_admin_log( wpsdAdminParams.bad_image_links );
			wpsd_admin_log( image_links );
			alert( wpsdAdminParams.bad_image_links );
			setTimeout( function() {
				$( '.wpsd_image_links_list' ).css( 'background-color', 'transparent' );
			}, 3000 );
			return;
		}

		$.ajax({
			type: 'POST',
			data: {'action': 'wpsd_update_image_links', 'post_id': post_id, 'nonce': nonce, 'image_links': image_links},
			url: wpsdAdminParams.ajaxurl,
			timeout: 15000,
			error: function() {},
			dataType: "html",
			success: function( html ) {
				wpsd_admin_log( html );
				$( '.wpsd_image_links_list_wrap' ).slideUp();
				$( '.wpsd_edit_image_links' ).data( 'status', 'closed' );
				$( '.wpsd_image_links_list' ).css( 'background-color', '#dfd' );
				$( '#vehicle_photo_links' ).css( 'background-color', '#dfd' );
				post_id = $( '#vehicle_photo_links' ).data( 'post-id' );
				var image_links = $( '.wpsd_image_links_list' ).val();
				var image = image_links.split( ',' );
				var photo_box = wpsdAdminParams.photo_box;
				var cnt = 0;
				var image_html = '';
				var linked_images_html = '';
				$.each( image, function( i, e ) {
					++cnt;
					image_html = photo_box;
					image_html = image_html.replace(/POST_ID/g, post_id);
					image_html = image_html.replace(/CNT/g, cnt);
					image_html = image_html.replace(/THUMBNAIL/g, e);
					image_html = image_html.replace(/SRC_ID/g, e);
					image_html = image_html.replace(/TYPE/g, 'linked' );
					linked_images_html += image_html;
				});
				$( '#vehicle_photo_links' ).html( linked_images_html );
				setTimeout( function() {
					$( '.wpsd_image_links_list' ).css( 'background-color', 'transparent' );
					$( '#vehicle_photo_links' ).css( 'background-color', 'transparent' );
				}, 3000 );
			}
		});
	};

	var wpsd_validate_image_urls = function( image_links ) {
		var urls = image_links.replace(/\s/g,"").split( "," );
		var regex = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/;
		var valid = true;
		for ( var i = 0; i < urls.length; i++ ) {
			if ( urls[i] === '' || ! regex.test( urls[i] ) ) {
				valid = false;
			}
		}
		return valid;
	};

	var wpsd_update_main_image = function( attachment_id ) {
		var nonce = $( '#wpsd_edit_vehicle_nonce' ).val();
		$( '.wpsd_main_image_wrapper' ).css( 'background-color', '#ffc' );
		post_id = $( '#vehicle_photo_links' ).data( 'post-id' );
		$.ajax({
			type: 'POST',
			data: {'action': 'wpsd_update_main_image', 'post_id': post_id, 'attachment_id': attachment_id, 'nonce': nonce},
			url: wpsdAdminParams.ajaxurl,
			timeout: 15000,
			error: function() {},
			dataType: "html",
			success: function( html ) {
				wpsd_admin_log( html );
				$( '.wpsd_main_image_wrapper' ).css( 'background-color', 'transparent' );
			}
		});
	};
	
	if ( $( '#vehicle_photo_attachments' ).length > 0 ) {
		$( '#vehicle_photo_attachments' ).sortable({
			connectWith: '.connectedSortable',
			stop: function(event, ui) {
				wpsd_get_image_order();
			}
		}).disableSelection();

		$( '.wpsd_main_image' ).sortable();
		
		$( '#vehicle_photo_attachments, .wpsd_main_image' ).droppable({
			accept: '.item-container',
			drop: function (e, ui) {
				var dropped = ui.draggable;
				var droppedOn = $(this);
				$(this).append(dropped.clone().removeAttr( 'style' ).removeClass( 'item-container' ).addClass( 'item' ));
				dropped.remove();
			}
		});

		//= If one of the attached images is dropped on the main image container then update the main image
		$( '.wpsd_main_image' ).droppable({
			accept: '.admin_box_vehicle_photo_attached',
			drop: function (e, ui) {
				var dropped = ui.draggable;
				var droppedOn = $( this );
				var src = $( dropped.clone()[0] ).find( '.wpsd_thumbs' ).attr( 'src' );
				var attachment_id = $( dropped.clone()[0] ).data( 'post-id' );
				//= Update our thumbnail
				$( '.wpsd_main_image img' ).attr( 'src', src );
				//= Update the native WordPress Featured Image
				$( '#set-post-thumbnail img' ).attr( 'src', src );
				//= Remove the srcset so the new image will display
				$( '#set-post-thumbnail img' ).attr( 'srcset' , '' );
				//= Call our ajax function to update the main image
				wpsd_update_main_image( attachment_id );
			}
		});
	}

	if ( $( '#vehicle_photo_links' ).length > 0 ) {
		$( '#vehicle_photo_links' ).sortable({
			connectWith: '.connectedSortable',
			stop: function(event, ui) {
				wpsd_get_image_order();
			}
		}).disableSelection();
	}

	var wpsd_reverse_attached_images = function() {
		var ul = $( '#vehicle_photo_links' ); // your parent ul element
		ul.children().each(function(i,li){ul.prepend(li)})

		ul = $( '#vehicle_photo_attachments' ); // your parent ul element
		ul.children().each(function(i,li){ul.prepend(li)})
	}

	if ( $( '.wpsd_link_main_image_true' ).length > 0 ) {
		$( '#postimagediv' ).css( 'display', 'none' );
		$( '#postimagediv-hide' ).prop( 'checked', false );

	}
	
	var remove_vehicle_image = function( post_id, vehicle_link, cnt, src_id, type ) {
		var nonce = $( '#admin-edit-vehicle-nonce' ).val();
		if ( confirm( wpsdAdminParams.remove_image_msg ) ) {
			$( '#vehicle_photo_' + cnt + '_' + type ).slideUp();
			var data_obj = {};
			if ( 'linked' === type ) {
				data_obj = {'post_id': post_id, 'action': 'wpsd_image_handler', 'vehicle_link': vehicle_link, 'option': 'remove_vehicle_linked_image', 'type': type, 'cnt': cnt, 'nonce': nonce};
			} else {
				data_obj = {'post_id': post_id, 'action': 'wpsd_image_handler', 'attachment_id': src_id, 'option': 'remove_vehicle_attached_image', 'type': type, 'cnt': cnt, 'nonce': nonce};
			}
			jQuery.ajax({
				type: 'POST',
				data: data_obj,
				url: wpsdAdminParams.ajaxurl,
				timeout: 15000,
				error: function() {
					wpsd_admin_log( 'Error - Image NOT removed' );
				},
				dataType: 'json',
				success: function( json ) {
					if ( 'attachments' === json.type ) {
						$( '#vehicle_photo_' + json.type + ' #vehicle_photo_' + json.cnt ).remove();						
					}
					wpsd_admin_log( json );
					wpsd_admin_log( json.msg );
					//= enable to force refresh of images
					//wpsd_get_image_order();
				}
			});
		}
	};

	if ( $( '.post-type-vehicle #set-post-thumbnail' ).length > 0 ) {
		//= https://wordpress.stackexchange.com/questions/228279/trigger-js-in-custom-meta-box-if-a-featured-image-is-loaded-exists
		MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
		
		var observer = new MutationObserver(function(mutations, observer) {
			//= Fires every time a mutation occurs...
			//= forEach all mutations
			mutations.forEach(function(mutation) {
				//= Loop through added nodes
				for (var i = 0; i < mutation.addedNodes.length; ++i) {
					//= Any images added?
					if ( mutation.addedNodes[i].getElementsByTagName( 'img' ).length > 0) {
						//= Your featured image now exists
						var src = $( '#set-post-thumbnail img' ).attr( 'src' );
						$( '#set-post-thumbnail-btn' ).attr( 'src', src );
					}
				}
			});
		});
		
		//= Define what element should be observed (#postimagediv is the container for the featured image)
		//= and what types of mutations trigger the callback
		var $element = $( '#postimagediv' );
		var config = { subtree: true, childList: true, characterData: true };
		
		observer.observe( $element[0], config );
	}

	var wpsdBase64 = {
		// private property
		_keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
		
		// public method for encoding
		encode : function (input) {
			var output = '';
			var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
			var i = 0;
		
			input = wpsdBase64._utf8_encode(input);
		
			while (i < input.length) {
		
				chr1 = input.charCodeAt(i++);
				chr2 = input.charCodeAt(i++);
				chr3 = input.charCodeAt(i++);
		
				enc1 = chr1 >> 2;
				enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
				enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
				enc4 = chr3 & 63;
		
				if (isNaN(chr2)) {
					enc3 = enc4 = 64;
				} else if (isNaN(chr3)) {
					enc4 = 64;
				}
		
				output = output +
				wpsdBase64._keyStr.charAt(enc1) + wpsdBase64._keyStr.charAt(enc2) +
				wpsdBase64._keyStr.charAt(enc3) + wpsdBase64._keyStr.charAt(enc4);
		
			}
		
			return output;
		},
		
		// public method for decoding
		decode : function (input) {
			var output = '';
			var chr1, chr2, chr3;
			var enc1, enc2, enc3, enc4;
			var i = 0;
		
			input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
		
			while (i < input.length) {
		
				enc1 = wpsdBase64._keyStr.indexOf(input.charAt(i++));
				enc2 = wpsdBase64._keyStr.indexOf(input.charAt(i++));
				enc3 = wpsdBase64._keyStr.indexOf(input.charAt(i++));
				enc4 = wpsdBase64._keyStr.indexOf(input.charAt(i++));
		
				chr1 = (enc1 << 2) | (enc2 >> 4);
				chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
				chr3 = ((enc3 & 3) << 6) | enc4;
		
				output = output + String.fromCharCode(chr1);
		
				if (enc3 != 64) {
					output = output + String.fromCharCode(chr2);
				}
				if (enc4 != 64) {
					output = output + String.fromCharCode(chr3);
				}
		
			}
		
			output = wpsdBase64._utf8_decode(output);
		
			return output;
		
		},
		
		// private method for UTF-8 encoding
		_utf8_encode : function (string) {
			string = string.replace(/\r\n/g,"\n");
			var utftext = '';
		
			for (var n = 0; n < string.length; n++) {
		
				var c = string.charCodeAt(n);
		
				if (c < 128) {
					utftext += String.fromCharCode(c);
				}
				else if((c > 127) && (c < 2048)) {
					utftext += String.fromCharCode((c >> 6) | 192);
					utftext += String.fromCharCode((c & 63) | 128);
				}
				else {
					utftext += String.fromCharCode((c >> 12) | 224);
					utftext += String.fromCharCode(((c >> 6) & 63) | 128);
					utftext += String.fromCharCode((c & 63) | 128);
				}
		
			}
		
			return utftext;
		},
		
		// private method for UTF-8 decoding
		_utf8_decode : function (utftext) {
			var string = '';
			var i = 0;
			var c = c1 = c2 = 0;
		
			while ( i < utftext.length ) {
		
				c = utftext.charCodeAt(i);
		
				if (c < 128) {
					string += String.fromCharCode(c);
					i++;
				}
				else if((c > 191) && (c < 224)) {
					c2 = utftext.charCodeAt(i+1);
					string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
					i += 2;
				}
				else {
					c2 = utftext.charCodeAt(i+1);
					c3 = utftext.charCodeAt(i+2);
					string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
					i += 3;
				}
		
			}
			return string;
		}
	};

	if ($( '.wpsd_no_add_classified' ).length > 0) {

		var wpsd_menu = $( '#menu-posts-vehicle .wp-submenu li' );
		$.each(wpsd_menu, function(i,v) {
			var content = $( 'a', v).html();
			if (content == 'Add New' ) {
				$(this).css( 'display', 'none' );
			}
		});

		var add_btn_html = $( '.post-type-vehicle .page-title-action' );
		
		if (add_btn_html.html() == 'Add New' ) {
			$( '.post-type-vehicle .page-title-action' ).css( 'display', 'none' );
		}

		$( '#wp-admin-bar-new-vehicle' ).css( 'display', 'none' );

	}

	var get_vehicle_types = function() {
		var vehicle_types = {};
		var count = 0;
		$.each( $( '.wpsd-type-data-wrap' ), function( i, e ) {
			var current_type = $( this ).data( 'type' );
			if ( 'SLUG-TYPE' === current_type || 'SLUG' === current_type ) {
				return;
			}
			++count;
			vehicle_types[ count ] = {};
			vehicle_types[ count ].type = current_type;
			vehicle_types[ count ].icon = $( e ).find( '.wpsd-type-icon' ).prop( 'src' );
			//= Specifications
			vehicle_types[ count ].specification_groups = {};
			var specification_groups = $( e ).find( '.wpsd-spec-group-fieldset' );
			var count_spec_groups = 0;
			$.each( specification_groups, function( index, element ) {
				++count_spec_groups;
				var specification_group = $( element ).data( 'specification-group-value' );
				vehicle_types[ count ].specification_groups[ count_spec_groups ] = {};
				vehicle_types[ count ].specification_groups[ count_spec_groups ].label = specification_group;
				vehicle_types[ count ].specification_groups[ count_spec_groups ].specifications = {};
				var specifications = $( element ).find( '.wpsd-type-specification-field' );
				var count_specs = 0;
				$.each( specifications, function( i_spec, e_spec ) {
					++count_specs;
					var specification = $( e_spec ).data( 'value' );
					var specification_slug = wpsd_slugify( specification );
					var specification_group_slug = wpsd_slugify( specification_group );
					var spec_data = {};
					spec_data.name = specification;
					spec_data.taxonomy = false;
					if ( true === $( '.wpsd-type-specification-' + current_type + '-' + specification_group_slug + '-' + specification_slug + ' .wpsd-specification-field-option-taxonomy' ).prop( 'checked' ) ) {
						spec_data.taxonomy = true;
					} else {
						spec_data.taxonomy = false;
					}
					spec_data['disabled'] = false;
					if ( true === $( '.wpsd-type-specification-' + current_type + '-' + specification_group_slug + '-' + specification_slug + ' .wpsd-specification-field-option-disabled' ).prop( 'checked' ) ) {
						spec_data['disabled'] = true;
					} else {
						spec_data['disabled'] = false;
					}
					spec_data['restrict'] = false;
					if ( true === $( '.wpsd-type-specification-' + current_type + '-' + specification_group_slug + '-' + specification_slug + ' .wpsd-specification-field-option-restrict' ).prop( 'checked' ) ) {
						spec_data['restrict'] = true;
					} else {
						spec_data['restrict'] = false;
					}
					spec_data['show-ui'] = false;
					if ( true === $( '.wpsd-type-specification-' + current_type + '-' + specification_group_slug + '-' + specification_slug + ' .wpsd-specification-field-option-show-ui' ).prop( 'checked' ) ) {
						spec_data['show-ui'] = true;
					} else {
						spec_data['show-ui'] = false;
					}
					spec_data.plural = $( '.wpsd-type-specification-' + current_type + '-' + specification_group_slug + '-' + specification_slug + ' .wpsd-specification-field-option-plural' ).val();
					spec_data.label = $( '.wpsd-type-specification-' + current_type + '-' + specification_group_slug + '-' + specification_slug + ' .wpsd-specification-field-option-label' ).val();
					spec_data['default-value'] = $( '.wpsd-type-specification-' + current_type + '-' + specification_group_slug + '-' + specification_slug + ' .wpsd-specification-field-option-default-value' ).val();
					vehicle_types[ count ].specification_groups[ count_spec_groups ].specifications[ specification ] = spec_data;
				});
			});

			//= Options
			vehicle_types[ count ].options = {};
			var options = $( e ).find( '.wpsd-option-group' );
			var count_option_groups = 0;	
			//= Option Group
			$.each( options, function( index, element ) {
				++count_option_groups;
				var option_group = $( element ).data( 'option-group' );
				vehicle_types[ count ].options[ count_option_groups ] = {};
				vehicle_types[ count ].options[ count_option_groups ].name = option_group;

				//= Option Sub-Group
				vehicle_types[ count ].options[ count_option_groups ].subgroups = {};
				var sub_groups = $( element ).find( '.wpsd-option-sub-group' );
				var count_option_subgroups = 0;
				$.each( sub_groups, function( s_index, s_element ) {
					++count_option_subgroups;
					var option_sub_group = $( s_element ).data( 'option-sub-group' );
					vehicle_types[ count ].options[ count_option_groups ].subgroups[ count_option_subgroups ] = {};
					vehicle_types[ count ].options[ count_option_groups ].subgroups[ count_option_subgroups ].name = option_sub_group;
					vehicle_types[ count ].options[ count_option_groups ].subgroups[ count_option_subgroups ].option_tags = {};
					var option_tags = $( s_element ).find( '.wpsd-option-tag' );
					var count_option_tags = 0;
					$.each( option_tags, function( o_index, o_element ) {
						++count_option_tags;
						var option_tag = $( o_element ).data( 'value' );
						option_tag = $.trim( option_tag );
						vehicle_types[ count ].options[ count_option_groups ].subgroups[ count_option_subgroups ].option_tags[ count_option_tags ] = option_tag;
					});
				});
			});
		});
		return vehicle_types;
	};

    var wpsd_sort_specs = function() {
        if ( 0 < $( '.wpsd-spec-subgroup-items-wrap' ).length ) {
            $( ".wpsd-spec-subgroup-items-wrap" ).sortable({
                connectWith: '.wpsd-spec-subgroup-items-wrap',
                revert: true,
            });
        }
    };
    wpsd_sort_specs();
    
    var wpsd_sort_options = function() {
        if ( 0 < $( '.wpsd-option-subgroup-items-wrap' ).length ) {
            $( '.wpsd-option-subgroup-items-wrap' ).sortable({
                connectWith: '.wpsd-option-subgroup-items-wrap',
                revert: true,
            });
        }
    };
    wpsd_sort_options();
    
	var get_global_specs = function() {
		var global_specs = {};
		var specification = false;
		var global_spec_elements = $( '.wpsd-type-specification-global' );
		if ( 1 > global_spec_elements.length ) {
			return global_specs;
		}
		$.each( global_spec_elements, function( i, e ) {
			specification = $( e ).data( 'specification' );
			value = $( e ).data( 'value' );
			global_specs[ specification ] = get_spec_options( e, 'global', 'default' );
		});
		wpsd_admin_log( 'Global Specs =>' );
		wpsd_admin_log( global_specs );
		return global_specs;
	};

	var get_no_group_specs = function() {
		var no_group_specs = {};
		var toggle = false;
		var type = false;
		var toggles_specs = $( '.wpsd-toggle-button-group-specs' );
		if ( 1 > toggles_specs.length ) {
			return no_group_specs;
		}
		$.each( toggles_specs, function( i, e ) {
			toggle = $( e ).find( 'input' );
			if ( false === toggle.prop( 'checked' ) ) {
				type = $( e ).data( 'type' );
				no_group_specs[ type ] = true;
			}
		});
		return no_group_specs;
	};

	var get_no_group_options = function() {
		var no_group_options = {};
		var toggle = false;
		var type = false;
		var toggles_options = $( '.wpsd-toggle-button-group-options' );
		if ( 1 > toggles_options.length ) {
			return no_group_options;
		}
		$.each( toggles_options, function( i, e ) {
			toggle = $( e ).find( 'input' );
			if ( false === toggle.prop( 'checked' ) ) {
				type = $( e ).data( 'type' );
				no_group_options[ type ] = true;
			}
		});
		return no_group_options;
	};

	var get_options_list = function() {
		var options_list = {};
		var toggle = false;
		var type = false;
		var toggles_options = $( '.wpsd-toggle-button-group-options-list' );
		if ( 1 > toggles_options.length ) {
			return options_list;
		}
		$.each( toggles_options, function( i, e ) {
			toggle = $( e ).find( 'input' );
			if ( true === toggle.prop( 'checked' ) ) {
				type = $( e ).data( 'type' );
				options_list[ type ] = true;
			}
		});
		return options_list;
	};
	
	var wpsd_slugify = function( string ) {
		if ( 'undefined' === typeof( string ) ) {
			return '';
		}
		const a = 'àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõőṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;'
		const b = 'aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnoooooooooprrsssssttuuuuuuuuuwxyyzzz------'
		const p = new RegExp(a.split( '' ).join( '|' ), 'g' )
	
	  return string.toString().toLowerCase()
		.replace(/\s+/g, '-' ) // Replace spaces with -
		.replace(p, c => b.charAt(a.indexOf(c))) // Replace special characters
		.replace(/&/g, '-and-' ) // Replace & with 'and'
		.replace(/[^\w\-]+/g, '' ) // Remove all non-word characters
		.replace(/\-\-+/g, '-' ) // Replace multiple - with single -
		.replace(/^-+/, '' ) // Trim - from start of text
		.replace(/-+$/, '' ) // Trim - from end of text
	}

	var wpsd_type_exists = function( type ) {
		var exists = false;
		var current_type = '';
		$( '.wpsd-type-wrap' ).each( function( i, e ) {
			current_type = $( e ).data( 'type' );
			if ( type == current_type ) {
				//= Set the variable to true since it exists
				exists = true;
				//= Return false to break out of the .each() loop
				return false;
			}
		});
		return exists;
	};
	
	var wpsd_specification_group_exists = function( group, type ) {
		var exists = false;
		var current_type = '';
		$( '.wpsd-type-wrap-' + type + ' .wpsd-spec-group-fieldset' ).each( function( i, e ) {
			current_specification_group = $( e ).data( 'specification-group' );
			if ( group == current_specification_group ) {
				//= Set the variable to true since it exists
				exists = true;
				//= Return false to break out of the .each() loop
				return false;
			}
		});
		return exists;
	};

	var wpsd_specification_exists = function( specification, type, group ) {
		var exists = false;
		var current_type = '';
		$( '.wpsd-type-wrap-' + type + ' .wpsd-spec-group-fieldset-' + group + ' .wpsd-type-specification-field' ).each( function( i, e ) {
			current_specification = $( e ).data( 'specification' );
			if ( specification == current_specification ) {
				//= Set the variable to true since it exists
				exists = true;
				//= Return false to break out of the .each() loop
				return false;
			}
		});
		return exists;
	};

	var wpsd_option_group_exists = function( group, type ) {
		var exists = false;
		var current_type = '';
		$( '.wpsd-type-wrap-' + type + ' .wpsd-option-group' ).each( function( i, e ) {
			current_option_group = $( e ).data( 'option-group' );
			if ( group == current_option_group ) {
				//= Set the variable to true since it exists
				exists = true;
				//= Return false to break out of the .each() loop
				return false;
			}
		});
		return exists;
	};

	var wpsd_option_sub_group_exists = function( option_sub_group_slug, option_group_slug, type ) {
		var exists = false;
		var current_type = '';
		$( '.wpsd-type-wrap-' + type + ' .wpsd-option-group-' + type + '-' + option_group_slug + ' .wpsd-option-sub-group' ).each( function( i, e ) {
			current_option_group = $( e ).data( 'option-sub-group' );
			if ( option_sub_group_slug == current_option_group ) {
				//= Set the variable to true since it exists
				exists = true;
				//= Return false to break out of the .each() loop
				return false;
			}
		});
		return exists;
	};

	var wpsd_option_exists = function( option, type, group, sub_group ) {
		var exists = false;
		var current_option = '';
		$( '.wpsd-type-wrap-' + type + ' .wpsd-option-group-' + type + '-' + group + ' .wpsd-option-sub-group-' + type + '-' + group + '-' + sub_group + ' .wpsd-option-tag' ).each( function( i, e ) {
			current_option = $( e ).data( 'option' );
			if ( option == current_option ) {
				//= Set the variable to true since it exists
				exists = true;
				//= Return false to break out of the .each() loop
				return false;
			}
		});
		return exists;
	};
	
	var wpsd_are_you_sure = function() {
		return confirm( wpsdAdminParams.strings.str0 + '\n' + wpsdAdminParams.strings.str1 );
	};

	var wpsd_upload_images = function( type ) {
		var custom_uploader = wp.media({
			id: 'wpsd-frame',
			title: wpsdAdminParams.strings.str37,
			editing:   true,
			multiple:  true,
			library: {
				type: 'image'
			},
			button: {
				text: wpsdAdminParams.strings.str38
			},
		})
		.on( 'select', function() {
			var selection = custom_uploader.state().get( 'selection' );
			selection.map( function( attachment ) {
				attachment = attachment.toJSON();
				// Do something with attachment.id and/or attachment.url here
				$( '.wpsd-type-wrap-' + type + ' .wpsd-type-icon' ).prop( 'src', attachment.url );
				$( '.wpsd-type-wrap-' + type + ' .wpsd-type-icon-url' ).val( attachment.url );
			})
		})
		.open();
	};

	var wpsd_validate_text = function( string, field ) {
		var response = {};
		response.valid = true;
		response.message = field.prop( 'title' );

		if ( string.length < 2 ) {
			response.valid = false;
			response.message = wpsdAdminParams.strings.str26 + ' "' + field.prop( 'title' ) + '" ' + wpsdAdminParams.strings.str27;
		}

		if ( false === response.valid ) {
			field.addClass( 'wpsd-invalid' );
			setTimeout( function() {
				field.removeClass( 'wpsd-invalid' );
			}, 2000 );
		} else {
			field.addClass( 'wpsd-valid' );
			setTimeout( function() {
				field.removeClass( 'wpsd-valid' );
			}, 2000 );
		}

		return response;
	};

	var wpsd_open_tooltip = function( element, title, content ) {
		$( '.wpsd-tool-tip label' ).html( title );
		$( '.wpsd-tool-tip span' ).html( content );
		var top = element.offset().top;
		var left = element.offset().left;
		left = left + 20;
		var height = element.css( 'height' );
		height = height.replace( 'px', '' );
		height = parseFloat( height );
		top = top + height + 10;
		$( '.wpsd-tool-tip' ).css( 'top', top );
		$( '.wpsd-tool-tip' ).css( 'left', left );
		$( '.wpsd-tool-tip' ).css( 'display', 'block' );
		setTimeout( function() {
			$( '.wpsd-tool-tip' ).fadeOut();
		}, 4000 );
	};

	/*
	* START: Define functions as part of an object
	*/
	var actions = {};

	//= Add a new vehicle type
	actions['type-add'] = function( action, button ) {
		var field = $( '.wpsd-input-add-vehicle-type' );
		var string = field.val();
		wpsd_admin_log( 'Adding type: ' + string );
		var type_slug = wpsd_slugify( string );
		wpsd_admin_log( 'Type slug: ' + type_slug );
		var response = wpsd_validate_text( string, field );
		if ( response.valid ) {
			//= Does type already exist?
			var exists = wpsd_type_exists( type_slug );
			if ( true === exists ) {
				var error_title = wpsdAdminParams.strings.str2;
				var error_message = string + wpsdAdminParams.strings.str3;
				wpsd_open_tooltip( field, error_title, error_message );

				field.addClass( 'wpsd-invalid' );
				$( '.wpsd-type-wrap-' + type_slug ).addClass( 'wpsd-invalid' );
				$( '.wpsd-type-wrap-' + type_slug + ' .wpsd-type-data-wrap' ).slideDown();
				setTimeout( function() {
					field.removeClass( 'wpsd-invalid' );
					$( '.wpsd-type-wrap-' + type_slug ).removeClass( 'wpsd-invalid' );
				}, 3000 );
			} else {
				wpsd_admin_log( 'Inserting type into page: ' + type_slug );
				var new_type_html = $( '.wpsd-type-wrap-SLUG' )[0].outerHTML;
				var re = new RegExp( 'SLUG', 'g' );
				new_type_html = new_type_html.replace(re, type_slug);
				re = new RegExp( 'LABEL', 'g' );
				new_type_html = new_type_html.replace(re, string);
				$( new_type_html ).insertAfter( '.wpsd-type-wrap-SLUG' );
				field.val( '' );
			}
		} else {
			var error_title = wpsdAdminParams.strings.str4;
			var error_message = response.message;
			wpsd_open_tooltip( field, error_title, error_message );
			wpsd_admin_log( 'Action failure: ' + action + ' - ' + string );
		}
		field.focus();
		wpsd_admin_log( response.message );
	};

	//= Reset the vehicle type to default
	actions['type-reset'] = function( action, button ) {
		//= TO DO Future: add reset specs and options to their individual defaults
		wpsd_admin_log( 'Sorry, this feature has not been implemented.' );
	};

	actions['type-remove'] = function( action, button ) {
		if ( true === wpsd_are_you_sure() ) {
			var type = $( button ).data( 'type' );
			$( '.wpsd-type-wrap-' + type ).remove();
		}
	};
	
	actions['icon-remove'] = function( action, button ) {
		var type = $( button ).data( 'type' );
		$( '.wpsd-type-wrap-' + type + ' .wpsd-type-icon' ).prop( 'src', '' );
		$( '.wpsd-type-wrap-' + type + ' .wpsd-type-icon-url' ).val( '' );
	};
	
	actions['icon-change'] = function( action, button ) {
		var type = $( button ).data( 'type' );
		wpsd_upload_images( type );
	};
	
	actions['add-specification-group'] = function( action, button ) {
		var type = $( button ).data( 'type' );
		var type_slug = wpsd_slugify( type );
		var field = $( '.wpsd-input-add-specification-group-' + type );
		var string = field.val();
		var specification_group_slug = wpsd_slugify( string );
		var response = wpsd_validate_text( string, field );
		if ( response.valid ) {
			//= Does group already exist?
			var exists = wpsd_specification_group_exists( specification_group_slug, type );
			if ( true === exists ) {
				var error_title = wpsdAdminParams.strings.str6;
				var error_message = string + wpsdAdminParams.strings.str7;
				wpsd_open_tooltip( field, error_title, error_message );
				field.addClass( 'wpsd-invalid' );
				setTimeout( function() {
					field.removeClass( 'wpsd-invalid' );
				}, 2000 );
			} else {
				var new_type_html = $( '.wpsd-spec-group-fieldset-slug' )[0].outerHTML;
				var re = new RegExp( 'SLUG-TYPE', 'g' );
				new_type_html = new_type_html.replace(re, type_slug);
				re = new RegExp( 'SLUG', 'g' );
				new_type_html = new_type_html.replace(re, string);
				re = new RegExp( 'slug', 'g' );
				new_type_html = new_type_html.replace(re, specification_group_slug);
				$( new_type_html ).insertAfter( '.wpsd-clear-specification-group-' + type );
				field.val( '' );
			}
		} else {
			var error_title = wpsdAdminParams.strings.str8;
			var error_message = response.message;
			wpsd_open_tooltip( field, error_title, error_message );
			wpsd_admin_log( 'Action failure: ' + action + ' - ' + string );
		}
		field.focus();
		wpsd_admin_log( response.message );
	};

	actions['add-specification'] = function( action, button ) {
		var type = $( button ).data( 'type' );
		var group = $( button ).data( 'group' );
		var field = $( '.wpsd-input-add-specification-' + type + '-' + group );
		var string = field.val();
		var specification_slug = wpsd_slugify( string );
		var response = wpsd_validate_text( string, field );
		if ( response.valid ) {
			//= Does type already exist?
			var exists = wpsd_specification_exists( specification_slug, type, group );
			if ( true === exists ) {
				var error_title = wpsdAdminParams.strings.str10;
				var error_message = string + wpsdAdminParams.strings.str11;
				wpsd_open_tooltip( field, error_title, error_message );
				field.addClass( 'wpsd-invalid' );
				setTimeout( function() {
					field.removeClass( 'wpsd-invalid' );
				}, 2000 );
			} else {
				var new_type_html = $( '.wpsd-type-specification-SLUG-TYPE-SLUG-GROUP-slug-specification' )[0].outerHTML;
				var re = new RegExp( 'SLUG-TYPE', 'gi' );
				new_type_html = new_type_html.replace(re, type);
				re = new RegExp( 'SLUG-GROUP', 'gi' );
				new_type_html = new_type_html.replace(re, group);
				re = new RegExp( 'SLUG-SPECIFICATION', 'g' );
				new_type_html = new_type_html.replace(re, string);
				re = new RegExp( 'slug-specification', 'g' );
				new_type_html = new_type_html.replace(re, specification_slug);
                $( '.wpsd-spec-subgroup-items-wrap-' + type + '-' + group ).prepend( new_type_html );
				field.val( '' );
			}
		} else {
			var error_title = wpsdAdminParams.strings.str12;
			var error_message = response.message;
			wpsd_open_tooltip( field, error_title, error_message );
			wpsd_admin_log( 'Action failure: ' + action + ' - ' + string );
		}
		field.focus();
		wpsd_admin_log( response.message );
        wpsd_sort_specs();
        wpsd_sort_options();
	};

	actions['close-spec-options'] = function( action, button ) {
		var type = $( button ).data( 'type' );
		var group = $( button ).data( 'group' );
		var spec_slug = $( button ).data( 'spec' );
		var current_field = $( '.wpsd-type-specification-' + type + '-' + group + '-' + spec_slug );
		var is_tax = false;
		var is_tax_field = $( current_field ).find( '.wpsd-specification-field-option-taxonomy' );
		if ( true === is_tax_field.prop( 'checked' ) ){
			is_tax = true;
		}
		var show_ui = false;
		var show_ui_field = $( current_field ).find( '.wpsd-specification-field-option-show-ui' );
		if ( true === show_ui_field.prop( 'checked' ) ){
			show_ui = true;
		}

		var label = $( current_field ).find( '.wpsd-specification-field-option-label' ).val();
		var default_value = $( current_field ).find( '.wpsd-specification-field-option-default-value' ).val();
		$( '.wpsd-type-specification-field-label' ).removeClass( 'chevron-up' );
		$( current_field ).find( '.wpsd-type-specification-field-values' ).slideUp();
	};

	actions['remove-specification-group'] = function( action, button ) {
		if ( true === wpsd_are_you_sure() ) {
			var type = $( button ).data( 'type' );
			var group = $( button ).data( 'group' );
			$( '.wpsd-spec-group-fieldset-' + type + '-' + group ).remove();
		}
	};
	
	actions['add-option-group'] = function( action, button ) {
		var type = $( button ).data( 'type' );
		var type_slug = wpsd_slugify( type );
		var field = $( '.wpsd-input-add-option-group-' + type );
		var string = field.val();
		var option_group_slug = wpsd_slugify( string );
		var response = wpsd_validate_text( string, field );
		if ( response.valid ) {
			//= Does group already exist?
			var exists = wpsd_option_group_exists( option_group_slug, type );
			if ( true === exists ) {
				var error_title = wpsdAdminParams.strings.str14;
				var error_message = string + wpsdAdminParams.strings.str15;
				wpsd_open_tooltip( field, error_title, error_message );
				field.addClass( 'wpsd-invalid' );
				setTimeout( function() {
					field.removeClass( 'wpsd-invalid' );
				}, 2000 );
			} else {
				var new_type_html = $( '.wpsd-option-group-slug' )[0].outerHTML;
				var re = new RegExp( 'SLUG-TYPE', 'g' );
				new_type_html = new_type_html.replace(re, type_slug);
				re = new RegExp( 'slug-type', 'g' );
				new_type_html = new_type_html.replace(re, type_slug);
				re = new RegExp( 'SLUG-GROUP', 'g' );
				new_type_html = new_type_html.replace(re, option_group_slug);
				re = new RegExp( 'slug', 'g' );
				new_type_html = new_type_html.replace(re, option_group_slug);
				re = new RegExp( 'SLUG', 'g' );
				new_type_html = new_type_html.replace(re, string);
				$( new_type_html ).insertAfter( '.wpsd-clear-option-group-' + type );
				field.val( '' );
				$( '.wpsd-option-group-' + option_group_slug + ' .wpsd-option-group-label' ).trigger( 'click' );
			}
		} else {
			var error_title = wpsdAdminParams.strings.str16;
			var error_message = response.message;
			wpsd_open_tooltip( field, error_title, error_message );
			wpsd_admin_log( 'Action failure: ' + action + ' - ' + string );
		}
		field.focus();
		wpsd_admin_log( response.message );
	};
	
	actions['remove-option-group'] = function( action, button ) {
		if ( true === wpsd_are_you_sure() ) {
			var type = $( button ).data( 'type' );
			var group = $( button ).data( 'group' );
			$( '.wpsd-option-group-' + type + '-' + group ).remove();
		}
	};

	actions['add-option-sub-group'] = function( action, button ) {
		var type = $( button ).data( 'type' );
		var type_slug = wpsd_slugify( type );
		var group = $( button ).data( 'group' );
		var group_slug = wpsd_slugify( group );

		var field = $( '.wpsd-input-add-option-sub-group-' + type_slug + '-' + group_slug );
		wpsd_admin_log( 'Add option sub group type: ' + type );
		wpsd_admin_log( 'Add option sub group group: ' + group );
		wpsd_admin_log( 'Add option sub group type_slug: ' + type_slug );
		wpsd_admin_log( 'Add option sub group group_slug: ' + group_slug );

		var string = field.val();
		var sub_group_slug = wpsd_slugify( string );
		var response = wpsd_validate_text( string, field );
		if ( response.valid ) {
			//= Does sub-group already exist?
			var exists = wpsd_option_sub_group_exists( sub_group_slug, group_slug, type );
			if ( true === exists ) {
				var error_title = wpsdAdminParams.strings.str18;
				var error_message = string + wpsdAdminParams.strings.str19;
				wpsd_open_tooltip( field, error_title, error_message );
				field.addClass( 'wpsd-invalid' );
				setTimeout( function() {
					field.removeClass( 'wpsd-invalid' );
				}, 2000 );
			} else {
				var new_type_html = $( '.wpsd-option-sub-group-slug-type-slug-group-slug' )[0].outerHTML;
				var re = new RegExp( 'SLUG-TYPE', 'g' );
				new_type_html = new_type_html.replace(re, type_slug);
				re = new RegExp( 'SLUG-GROUP', 'g' );
				new_type_html = new_type_html.replace(re, group_slug);
				re = new RegExp( 'slug', 'g' );
				new_type_html = new_type_html.replace(re, sub_group_slug);
				re = new RegExp( 'SLUG', 'g' );
				new_type_html = new_type_html.replace(re, string);
				new_type_html += '<div class="wpsd-clear"></div>';
				$( '.wpsd-option-group-' + type_slug + '-' + group_slug + ' .wpsd-option-sub-groups' ).prepend( new_type_html );
				field.val( '' );
				$( '.wpsd-option-sub-group-' + sub_group_slug + ' .wpsd-option-sub-group-label' ).trigger( 'click' );
			}
		} else {
			var error_title = wpsdAdminParams.strings.str20;
			var error_message = response.message;
			wpsd_open_tooltip( field, error_title, error_message );
			wpsd_admin_log( 'Action failure: ' + action + ' - ' + string );
		}
		field.focus();
		wpsd_admin_log( response.message );
	};

	actions['add-option'] = function( action, button ) {
		var type = $( button ).data( 'type' );
		var group = $( button ).data( 'group' );
		var sub_group = $( button ).data( 'sub-group' );
		var sub_group_slug = wpsd_slugify( sub_group );
		var field = $( '.wpsd-input-add-option-' + type + '-' + group + '-' + sub_group_slug );
		var string = field.val();
		var option_slug = wpsd_slugify( string );
		var response = wpsd_validate_text( string, field );
		if ( response.valid ) {
			//= Does type already exist?
			var exists = wpsd_option_exists( option_slug, type, group, sub_group );
			if ( true === exists ) {
				var error_title = wpsdAdminParams.strings.str;
				var error_message = string + wpsdAdminParams.strings.str23;
				wpsd_open_tooltip( field, error_title, error_message );
				field.addClass( 'wpsd-invalid' );
				setTimeout( function() {
					field.removeClass( 'wpsd-invalid' );
				}, 2000 );
			} else {
				var new_type_html = $( '.wpsd-option-tag-slug-type-slug-group-slug-sub-group-slug' )[0].outerHTML;
				var re = new RegExp( 'SLUG-TYPE', 'gi' );
				new_type_html = new_type_html.replace(re, type);
				re = new RegExp( 'SLUG-GROUP', 'gi' );
				new_type_html = new_type_html.replace(re, group);
				re = new RegExp( 'SLUG-SUB-GROUP', 'gi' );
				new_type_html = new_type_html.replace(re, sub_group);
				re = new RegExp( 'SLUG', 'g' );
				new_type_html = new_type_html.replace(re, string);
				re = new RegExp( 'slug', 'g' );
				new_type_html = new_type_html.replace(re, option_slug);
                $( '.wpsd-option-subgroup-items-wrap-' + type + '-' + group + '-' + sub_group_slug ).prepend( new_type_html );
				field.val( '' );
			}
		} else {
			var error_title = wpsdAdminParams.strings.str24;
			var error_message = response.message;
			wpsd_open_tooltip( field, error_title, error_message );
			wpsd_admin_log( 'Action failure: ' + action + ' - ' + string );
		}
        wpsd_sort_specs();
        wpsd_sort_options();
		field.focus();
		wpsd_admin_log( response.message );
	wpsd-option-tag};
	
	actions['remove-spec-item'] = function( action, button ) {
		if ( true === wpsd_are_you_sure() ) {
			var type = $( button ).data( 'type' );
			var group = $( button ).data( 'group' );
			var specification = $( button ).data( 'specification' );
			$( '.wpsd-type-specification-' + type + '-' + group + '-' + specification ).remove();
		}
	}

	actions['remove-option-sub-group'] = function( action, button ) {
		if ( true === wpsd_are_you_sure() ) {
			var type = $( button ).data( 'type' );
			var group = $( button ).data( 'group' );
			var sub_group = $( button ).data( 'sub-group' );
			$( '.wpsd-option-sub-group-' + type + '-' + group + '-' + sub_group ).remove();
		}
	}

	actions['remove-option-item'] = function( action, button ) {
		if ( true === wpsd_are_you_sure() ) {
			var type = $( button ).data( 'type' );
			var group = $( button ).data( 'group' );
			var sub_group = $( button ).data( 'sub-group' );
			var option = $( button ).data( 'option' );
			$( '.wpsd-option-tag-' + type + '-' + group + '-' + sub_group + '-' + option ).remove();
		}
	}

	actions['close-default-types'] = function( action, button ) {
		$( '.wpsd-default-add-types-wrap, .wpsd-type-data-buttons-wrap' ).slideDown();
		$( '.wpsd-default-types-wrap' ).slideUp();
	}
	
	/*
	* END: Define functions as part of an object
	*/

	var wpsd_check_default_types = function() {
		var default_types = $( '.wpsd-default-type-item' );
		$.each( default_types, function( i, e ) {
			$( e ).removeClass( 'wpsd-default-type-exists' );
			var type = $( e ).data( 'type' );
			var exists = wpsd_type_exists( type );
			if ( true === exists ) {
				$( e ).addClass( 'wpsd-default-type-exists' );
			}
		});
	};
	
	$( document ).on( 'click', '.wpsd-default-add-types-wrap small', function() {
		wpsd_check_default_types();
		$( '.wpsd-default-add-types-wrap, .wpsd-type-data-buttons-wrap' ).slideUp();
		$( '.wpsd-default-types-wrap' ).slideDown();
	});
	
	var wpsd_edit_options_toggle = function( args ) {
		wpsd_admin_log( args );
		var json_args = JSON.parse( args.json );

		wpsd_admin_log( 'json_args =>' );
		wpsd_admin_log( json_args );
		var options_text_field = $( '.wpsd-edit-vehicle-options-list-' + json_args.group + '-' + json_args.sub_group );
		wpsd_admin_log( options_text_field );
		var current_options = options_text_field.val();
		wpsd_admin_log( 'Current Options for subgroup: ' + current_options );
		var value = json_args.value;
		wpsd_admin_log( 'Args value: ' + value );

		var action = 'add';
		if ( true !== args.checked ) {
			action = 'remove';
		}
		wpsd_admin_log( 'Toggle Action: ' + action );
		var options = '';

		var group_options = $( '.wpsd-edit-vehicle-options-item-' + json_args.group + '-' + json_args.sub_group + ' .wpsd-toggle-button' );
		$.each( group_options, function( i, e ) {
			if ( true === $( e ).prop( 'checked' ) ) {
				options += $( e ).data( 'type' ) + ',';
			}
		});
		options += '@@';
		options = options.replace( ',@@', '' );
		options = options.replace( '@@', '' );

		options_text_field.val( options );
		options_text_field.trigger( 'change' );
	};

	
	var wpsd_add_default_vehicle_type = function( type ) {
		wpsd_admin_log( 'Default type selected: ' + type );
		if ( true == wpsd_are_you_sure() ) {
			wpsd_admin_log( 'Adding default type' );

			var nonce = $( '#admin-vehicle_options-nonce' ).val();
			jQuery.ajax({
				type: 'POST',
				data: {'action': 'wpsd_add_default_vehicle_type', 'nonce': nonce, 'type': type},
				url: wpsdAdminParams.ajaxurl,
				timeout: 5000,
				error: function() {},
				dataType: 'json',
				success: function( json ) {
					wpsd_admin_log( json );
					wpsd_admin_log( 'reloading page' );
					location.reload();
				}
			});
			return false;
			
		}
	};

	var wpsd_group_options = function( group_them, type ) {
		var option_elements = $( '.wpsd-type-wrap-' + type ).find( '.wpsd-option-tag' );
		var options_html = '';
		var type_slug = wpsd_slugify( type );
		if ( false === group_them ) {
			wpsd_admin_log( 'Ungroup Options for: ' + type );
			$( '.wpsd-type-wrap-' + type ).addClass( 'wpsd-option-group-no' );
			$.each( option_elements, function( i, e ) {
				options_html += e.outerHTML;
			});
			$( '.wpsd-type-wrap-' + type + ' .wpsd-option-group' ).remove();
			$( '.wpsd-input-add-option-group-' + type ).val( wpsdAdminParams.strings.str34 );
			$( '.wpsd-type-wrap-' + type + ' .action-add-option-group' ).trigger( 'click' );
			$( '.wpsd-type-wrap-' + type + ' .wpsd-input-add-option-sub-group' ).val( wpsdAdminParams.strings.str34 );
			$( '.wpsd-type-wrap-' + type + ' .action-add-option-sub-group' ).trigger( 'click' );
			$( '.wpsd-type-wrap-' + type + ' .wpsd-option-subgroup-items-wrap' ).html( options_html );
			$( '.wpsd-option-group-no .wpsd-option-group-wrap' ).slideDown();
		} else {
			wpsd_admin_log( 'Group Options for: ' + type );
			$( '.wpsd-type-wrap-' + type ).removeClass( 'wpsd-option-group-no' );
		}
	};

	var wpsd_group_specs = function( group_them, type ) {
		var option_elements = $( '.wpsd-type-wrap-' + type ).find( '.wpsd-type-specification-field' );
		var options_html = '';
		var type_slug = wpsd_slugify( type );
		if ( false === group_them ) {
			$( '.wpsd-type-wrap-' + type ).addClass( 'wpsd-spec-group-no' );
			$.each( option_elements, function( i, e ) {
				options_html += e.outerHTML;
			});
			$( '.wpsd-input-add-specification-group-' + type ).val( wpsdAdminParams.strings.str34 );
			$( '.wpsd-type-wrap-' + type ).find( '.wpsd-spec-group-fieldset' ).remove();
			$( '.wpsd-type-wrap-' + type ).find( '.action-add-specification-group' ).trigger( 'click' );
			$( '.wpsd-type-wrap-' + type ).find( '.wpsd-spec-subgroup-items-wrap' ).html( options_html );
		} else {
			$( '.wpsd-type-wrap-' + type ).removeClass( 'wpsd-spec-group-no' );
		}
	};

	$( document ).on( 'click', '.wpsd-toggle-button', function() {
		var type = $( this ).data( 'type' );
		var section = $( this ).data( 'section' );
		var action = $( this ).data( 'action' );
		var checked = $( this ).prop( 'checked' );
		wpsd_admin_log( '*********' );
		wpsd_admin_log( 'Action: ' + action );
		wpsd_admin_log( 'Checked: ' + checked );
		wpsd_admin_log( 'Type: ' + type );
		wpsd_admin_log( 'Section: ' + section );
		wpsd_admin_log( '=========' );
		if ( 'wpsd-group-options' === action ) {
			wpsd_group_options( checked, type );
		} else if ( 'wpsd-group-options-list' === action ) {

		} else if ( 'wpsd-edit-options-toggle' === action ) {
			var args = {};
			var field_base64 = $( this ).data( 'base64' );
			var field_json = wpsdBase64.decode( field_base64 );
			args.checked = checked;
			args.type = type;
			args.section = section;
			args.field = $( this ).val();
			args.json = field_json;
			wpsd_edit_options_toggle( args );
		} else if ( 'wpsd-disable-global-spec' === action ) {
			wpsd_disable_global_spec( checked, type, this );
		} else if ( 'wpsd-group-specs' === action ) {
			wpsd_group_specs( checked, type );
		} else {
			wpsd_admin_log( 'Unknown action: ' + action );
		}
        wpsd_sort_specs();
        wpsd_sort_options();
	});
	
	$( document ).on( 'click', '.wpsd-default-type-item', function() {
		var type = $( this ).data( 'type' );
		var exists = wpsd_type_exists( type );
		wpsd_admin_log( type + ' exists: ' + exists );
		if ( false === exists ) {
			wpsd_add_default_vehicle_type( type );
		} else {
			alert( wpsdAdminParams.strings.str33 );
			wpsd_admin_log( wpsdAdminParams.strings.str33 );
		}
	});
	
	$( document ).on( 'click', '.wpsd-switch-tab', function() {
		var tab = $( this ).data( 'tab' );
		$( '.wpsd_settings_tab_title_' + tab ).trigger( 'click' );
		wpsd_admin_log( 'Tab clicked =>' );
		wpsd_admin_log( tab );
	});
	
	$( document ).on( 'click', '.wpsd-edit-location-button', function() {
		var slug = $( this ).data( 'slug' );
		wpsd_admin_log( 'Location selected: ' + slug );
		$( '.wpsd-admin-location-table' ).css( 'display', 'none' );
		$( '.wpsd-edit-location-wrap' ).css( 'display', 'none' );
		$( '.wpsd-edit-location-wrap-' + slug ).slideDown();
		$( '.wpsd-edit-view-locations' ).slideDown();
	});

	$( document ).on( 'click', '.wpsd-edit-view-locations', function() {
		$( this ).css( 'display', 'none' );
		$( '.wpsd-edit-location-wrap' ).css( 'display', 'none' );
		$( '.wpsd-admin-location-table' ).slideDown();
	});
	/*
	Load Tooltip html into page
	*/
	var tooltip = '';
	tooltip += '<div class="wpsd-tool-tip">';
		tooltip += '<label></label>';
		tooltip += '<span></span>';
	tooltip += '</div>';
	$( 'body' ).append( tooltip );

	$.formObject = function($o) {
		var o = {},
			real_value = function($field) {
				var val = $field.val() || "";

				// additional cleaning here, if needed

				return val;
			};

		if (typeof o != "object") {
			$o = $(o);
		}

		$(":input[name]", $o).each(function(i, field) {
			var $field = $(field),
				name = $field.attr("name"),
				value = real_value($field);

			if (o[name]) {
				if (!$.isArray(o[name])) {
					o[name] = [o[name]];
				}

				o[name].push(value);
			}

			else {
				o[name] = value;
			}
		});

		return o;
	}
	/*
	* Admin Page
	*/
	//= https://stackoverflow.com/questions/1184624/convert-form-data-to-javascript-object-with-jquery
    $.fn.serializeObject = function(){

        var self = this,
            json = {},
            push_counters = {},
            patterns = {
                "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
                "key":      /[a-zA-Z0-9_]+|(?=\[\])/g,
                "push":     /^$/,
                "fixed":    /^\d+$/,
                "named":    /^[a-zA-Z0-9_]+$/
            };


        this.build = function(base, key, value){
            base[key] = value;
            return base;
        };

        this.push_counter = function(key){
            if(push_counters[key] === undefined){
                push_counters[key] = 0;
            }
            return push_counters[key]++;
        };

        $.each($(this).serializeArray(), function(){

            // Skip invalid keys
            if(!patterns.validate.test(this.name)){
                return;
            }

            var k,
                keys = this.name.match(patterns.key),
                merge = this.value,
                reverse_key = this.name;

            while((k = keys.pop()) !== undefined){

                // Adjust reverse_key
                reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

                // Push
                if(k.match(patterns.push)){
                    merge = self.build([], self.push_counter(reverse_key), merge);
                }

                // Fixed
                else if(k.match(patterns.fixed)){
                    merge = self.build([], k, merge);
                }

                // Named
                else if(k.match(patterns.named)){
                    merge = self.build({}, k, merge);
                }
            }

            json = $.extend(true, json, merge);
        });

        return json;
    };
	var formToJSON = function( f ) {
		var obj = f.serializeObject();
		return JSON.stringify( obj );
	}

    var sortFormToJSON = function( f ) {
        var fd = $(f).serializeArray();
        var d = {};
        $(fd).each(function() {
            if (d[this.name] !== undefined){
                if (!Array.isArray(d[this.name])) {
                    d[this.name] = [d[this.name]];
                }
                d[this.name].push(this.value);
            }else{
                d[this.name] = this.value;
            }
        });
        return d;
        var obj = f.serializeObject();
        return JSON.stringify( obj );
    }
    
	var wpsd_isNumeric = function( n ) {
	  return !isNaN(parseFloat(n)) && isFinite(n);
	}
	
	var show_saving = function( msg ) {
		if ( '' === msg ) {
			msg = wpsdAdminParams.strings.str28;
		}
		if ( 0 < $( '.wpsd-admin-lightbox' ).length ) {
			$( '.wpsd-admin-lightbox' ).addClass( 'active-lightbox' );
		} else {
			var lightbox = '<div class="wpsd-admin-lightbox active-lightbox"><div class="msg"><i class="spinner is-active"></i>' + msg + '</div></div>';
			$( 'body' ).append( lightbox );
		}
	}

	if ( 0 < $( '.wpsd_settings_tab_title' ).length ) {
		//= Check the URL hash to see if this is a direct link to one of the tabs 
		var hash = location.hash.replace( '#tab-','' );
		if( hash != '' ) {
			//== A tab name was found - let's switch to it
			$( '.wpsd_settings_tab_title_' + hash ).trigger( 'click' );
		} else {
			var active_tab = $( '.wpsd_settings_tab_title.active' );
			if ( 0 < active_tab.length ) {
				//= It looks like an active tab was loaded from php
				//= This should be the last tab they had open
				var tab = active_tab.data( 'tab' );
				//= Try to load the url hash without jumping to it
				if( history.pushState ) {
					history.pushState( null, null, '#tab-' + tab );
				} else {
					location.hash = '#' + tab;
				}
			}
		}
	}
	
    /***************************************************/
    /* Start YouTube custom lazy load code for startup guide */
    if ( 0 < $( '.wpsd_settings_tab_startup_guide.active' ).length ) {
        if ( '' === $( '.wpsd-instructions-wrap-startup-guide .wpsd-youtube' ).html() ) {
            var videoURL = $( '.wpsd-instructions-wrap-startup-guide .wpsd-youtube' ).data( 'video-url' );
            var html = '';
//            wpsd_admin_log( 'Startup Guide Video loaded: ' + videoURL );
            html += '<iframe src="' + videoURL + '" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
            $( '.wpsd-instructions-wrap-startup-guide .wpsd-youtube' ).html( html );
        }
    }
    /* End YouTube custom lazy load for startup guide  */
    /***************************************************/

    
	$( document ).on( 'click', '.wpsd_admin_form h1', function() {
		if ($(this).next( '.wpsd_location' ).css( 'display' ) == 'none' ) {
			$(this).next( '.wpsd_location' ).slideDown();
		} else {
			$(this).next( '.wpsd_location' ).slideUp();
		}
	});

	$( document ).on( 'click', '.wpsd_settings_tab_title', function() {
		$( '.wpsd_settings_tab_title' ).removeClass( 'active' );
		$( '.wpsd_settings_tab' ).removeClass( 'active' );
		$(this).addClass( 'active' );
		var tab = $(this).data( 'tab' );
		//= Try to load the url hash without jumping to it
		if( history.pushState ) {
			history.pushState( null, null, '#tab-' + tab );
		} else {
			location.hash = '#tab-' + tab;
		}

        /***************************************************/
        /* Start YouTube custom lazy load code for startup guide */
        if ( 'startup_guide' === tab ) {
            if ( '' === $( '.wpsd-instructions-wrap-startup-guide .wpsd-youtube' ).html() ) {
                var videoURL = $( '.wpsd-instructions-wrap-startup-guide .wpsd-youtube' ).data( 'video-url' );
                var html = '';
                wpsd_admin_log( 'Startup Guide Video loaded: ' + videoURL );
                html += '<iframe src="' + videoURL + '" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                $( '.wpsd-instructions-wrap-startup-guide .wpsd-youtube' ).html( html );
            }
        }
        /* End YouTube custom lazy load for startup guide  */
        /***************************************************/
        
		wpsd_setCookie( 'wpsd-admin-tab', tab );
		$( '#'+tab).addClass( 'active' );
	});
	
	$( document ).on( 'click', '.wpsd_welcome_tab_title', function() {
		$( '.wpsd_welcome_tab_title' ).removeClass( 'active' );
		$( '.wpsd_welcome_tab' ).removeClass( 'active' );
		$(this).addClass( 'active' );
		var tab = $(this).data( 'tab' );
		$( '#'+tab).addClass( 'active' );
	});
	
	$( document ).on( 'click', '.create_inventory_btn', function() {
		var page_name = $( '.create_inventory' ).val();
		var include_search = $( '.create_inventory_search' ).prop( 'checked' );
		if (include_search == true) {
			include_search = 'yes';
		}
		var nonce = $( '#wpsd_create_inventory_nonce' ).val();
		$( '.wpsd_setup_inventory' ).css( 'background', '#ffa' );
		$.ajax({
			type: 'POST',
			data: {'action': 'wpsd_create_inventory_page', 'page_name': page_name, 'include_search': include_search, 'nonce': nonce},
			url: wpsdAdminParams.ajaxurl,
			timeout: 5000,
			error: function() {
				$( '.wpsd_setup_inventory' ).css( 'background', '#f99' );
				setTimeout( function() {
					$( '.wpsd_setup_inventory' ).css( 'background', '' );
				}, 3000 );
			},
			dataType: "html",
			success: function(html){
				var new_body = html;
				$( '.create_inventory_results' ).html(html);
				$( '.wpsd_setup_inventory' ).css( 'background', '#afa' );
				setTimeout( function() {
					$( '.wpsd_setup_inventory' ).css( 'background', '' );
				}, 3000 );
			}
		});
	});
	
	$( document ).on( 'click', '.wpsd_insert_samples_btn', function() {
		$( '.wpsd_sample_inventory' ).css( 'background', '#bfb' );
		$( '.wpsd_sample_inventory' ).css( 'font-weight', 'bold' );
		$( '.wpsd_insert_samples_btn' ).prop( 'disabled', true);
		var nonce = $( '#wpsd_insert_samples_nonce' ).val();
		var spinner = '<img src="' + wpsdAdminParams.spinner + '" />';
		var msg = wpsdAdminParams.sample_msg;
		var qty = $( '.sample_qty' ).val();
		$( '.wpsd_sample_inventory' ).html(spinner + ' ' + msg);
		$.ajax({
			type: 'POST',
			data: {'action': 'wpsd_insert_sample_vehicles', 'qty': qty, 'nonce': nonce},
			url: wpsdAdminParams.ajaxurl,
			timeout: 135000,
			error: function(html) {
				$( '.wpsd_sample_inventory' ).html( html );
				$( '.wpsd_sample_inventory' ).css( 'background', '' );
				$( '.wpsd_sample_inventory' ).css( 'color', '' );
				$( '.wpsd_insert_samples_btn' ).prop( 'disabled', false);
			},
			dataType: "html",
			success: function(html){
				$( '.wpsd_sample_inventory' ).html( html );
				$( '.wpsd_sample_inventory' ).css( 'background', '' );
				$( '.wpsd_sample_inventory' ).css( 'font-weight', '' );
				$( '.wpsd_insert_samples_btn' ).prop( 'disabled', false);
			}
		})
	});
	
	$( document ).on( 'click', '.itemtitle', function() {
		if ($(this).nextAll( '.itemcontent' ).css( 'display' ) == 'none' ) {
			$(this).nextAll( '.itemcontent' ).slideDown();
		} else {
			$(this).nextAll( '.itemcontent' ).slideUp();
		}
	});
	
	$( document ).on( 'click', '.wpsd_admin_show_all', function() {
		var text = $(this).data( 'open-close-text' );
		var status = $(this).data( 'status' );
		var html = $(this).html();
		var option_groups = $( '.wpsd_option_group' );
		if (status == 0) {
			option_groups.slideDown();
			$(this).data( 'status', 1);
		} else {
			option_groups.slideUp();
			$(this).data( 'status', 0);		
		}
		$(this).html(text);
		$(this).data( 'open-close-text', html);
	});
	
	$( document ).on( 'click', '.wpsd_admin_group legend', function() {
		var option_group = $(this).next( '.wpsd_option_group' );
		if (option_group.css( 'display' ) == 'none' ) {
			option_group.slideDown();
		} else {
			option_group.slideUp();		
		}
	});
	
	$( document ).on( 'click', '#wpsd_open_description', function() {
		$( '#description_tab' ).show( 500, function(){});
	});

	$( document ).on( 'click', '#wpsd_close_description', function() {
		$( '#description_tab' ).hide( 500, function(){});
	});
	
	$( document ).on( 'click', '#wpsd_add_description', function() {
		$( '#frm_add_description' ).show( 500, function(){});
	});
	
	$( document ).on( 'click', '#cancel_description', function() {
		$( '#frm_add_description' ).hide( 500, function(){});
	});
	
	$( document ).on( 'click', '.open_tab', function() {
		if (typeof($(this).data( 'status' )) != 'undefined' ) {
			if ($(this).data( 'status' ) == 'closed' ) {
				$(this).next( 'div' ).show( 500, function(){});
				$(this).data( 'status', 'open' )
			} else {
				$(this).next( 'div' ).hide( 500, function(){});
				$(this).data( 'status', 'closed' );
			}
		} else {
			$(this).next( 'div' ).show( 500, function(){});
			$(this).data( 'status', 'open' )
		}
	});
	
	$( document ).on( 'click', '.wpsd-instructions-hide, .wpsd-instructions-title', function() {
		var tab = $( this ).data( 'tab' );
		var status = $( '.wpsd-instructions-wrap-' + tab + ' .wpsd-instructions' ).css( 'display' );
		wpsd_admin_log( status );
		if ( 'none' === status ) {
            /***************************************************/
            /* Start YouTube custom lazy load code */
            if ( '' === $( '.wpsd-instructions-wrap-' + tab + ' .wpsd-youtube' ).html() ) {
                var videoURL = $( '.wpsd-instructions-wrap-' + tab + ' .wpsd-youtube' ).data( 'video-url' );
                var html = '';
                wpsd_admin_log( 'Video loaded: ' + videoURL );
                html += '<iframe src="' + videoURL + '" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                $( '.wpsd-instructions-wrap-' + tab + ' .wpsd-youtube' ).html( html );
            }
            /* End YouTube custom lazy load edit */
            /***************************************************/
			$( '.wpsd-instructions-wrap-' + tab + ' .wpsd-instructions-hide' ).html( wpsdAdminParams.strings.str29 );
			$( '.wpsd-instructions-wrap-' + tab + ' .wpsd-instructions' ).slideDown();
		} else {
			$( '.wpsd-instructions-wrap-' + tab + ' .wpsd-instructions-hide' ).html( wpsdAdminParams.strings.str30 );
			$( '.wpsd-instructions-wrap-' + tab + ' .wpsd-instructions' ).slideUp();			
		}
	});
	
	$( document ).on( 'click', '#wpsd_close_specs', function() {
		$( '#specs_tab' ).prev( '.open_tab' ).data( 'status', 'closed' );
		$( '#specs_tab' ).hide( 500, function(){});
	});

	$( document ).on( 'click', '#wpsd_add_specs', function() {
		$( '#frm_add_specs' ).show( 500, function(){});
	});
	
	$( document ).on( 'click', '#cancel_specs', function() {
		$( '#frm_add_specs' ).hide( 500, function(){});
	});
	
	$( document ).on( 'click', '#wpsd_close_safety', function() {
		$( '#safety_tab' ).hide( 500, function(){});
		$( '#safety_tab' ).prev( '.open_tab' ).data( 'status', 'closed' );
	});

	$( document ).on( 'click', '#wpsd_add_safety', function() {
		$( '#frm_add_safety' ).show( 500, function(){});
	});

	$( document ).on( 'click', '#cancel_safety', function() {
		$( '#frm_add_safety' ).hide( 500, function(){});
	});
	
	$( document ).on( 'click', '#wpsd_close_convenience', function() {
		$( '#convenience_tab' ).hide( 500, function(){});
		$( '#convenience_tab' ).prev( '.open_tab' ).data( 'status', 'closed' );
	});

	$( document ).on( 'click', '#wpsd_add_convenience', function() {
		$( '#frm_add_convenience' ).show( 500, function(){});
	});

	$( document ).on( 'click', '#cancel_convenience', function() {
		$( '#frm_add_convenience' ).hide( 500, function(){});
	});
	
	$( document ).on( 'click', '#wpsd_close_comfort', function() {
		$( '#comfort_tab' ).hide( 500, function(){});
		$( '#comfort_tab' ).prev( '.open_tab' ).data( 'status', 'closed' );
	});

	$( document ).on( 'click', '#wpsd_add_comfort', function() {
		$( '#frm_add_comfort' ).show( 500, function(){});
	});

	$( document ).on( 'click', '#cancel_comfort', function() {
		$( '#frm_add_comfort' ).hide( 500, function(){});
	});
	
	$( document ).on( 'click', '#wpsd_close_entertainment', function() {
		$( '#entertainment_tab' ).hide( 500, function(){});
		$( '#entertainment_tab' ).prev( '.open_tab' ).data( 'status', 'closed' );
	});

	$( document ).on( 'click', '#wpsd_add_entertainment', function() {
		$( '#frm_add_entertainment' ).show( 500, function(){});
	});

	$( document ).on( 'click', '#cancel_entertainment', function() {
		$( '#frm_add_entertainment' ).hide( 500, function(){});
	});
	
	$( document ).on( 'click', '#wpsd_close_about_us', function() {
		$( '#about_us_tab' ).hide( 500, function(){});
		$( '#about_us_tab' ).prev( '.open_tab' ).data( 'status', 'closed' );
	});

	$( document ).on( 'click', '#wpsd_add_about_us', function() {
		$( '#frm_add_about_us' ).show( 500, function(){});
	});

	$( document ).on( 'click', '#cancel_about_us', function() {
		$( '#frm_add_about_us' ).hide( 500, function(){});
	});

	$( document ).on( 'change', '.wpsd_dynamic_load', function() {
		if ($(this).val() == 'Yes' ) {
			$( '.wpsd_auto_load' ).slideDown();
		} else {
			$( '.wpsd_auto_load' ).slideUp();
		}
	});

	$( document ).on( 'click', '.wpsd_open_caps', function() {
		var fld_type = $(this).data( 'type' );
		$( '.wpsd_spec_cap_box.' + fld_type).slideToggle();
	});

	$( document ).on( 'click', '.reset_wp_super_dealer', function() {
		if ( confirm( wpsdAdminParams.reset_msg ) ) {
			return true;
		} else {
			return false;
		}
	});

	$( document ).on( 'click', '.manage_vehicle_photos', function() {
		wpsd_manage_vehicle_photos();
	});

	$( document ).on( 'click', '.wpsd_update_image_links_btn', function() {
		var image_links = $( '.wpsd_image_links_list' ).val();
		wpsd_update_image_links( image_links );
	});

	$( document ).on( 'click', '.wpsd_reverse_attachments_btn', function() {
		wpsd_reverse_attached_images();
		wpsd_get_image_order();
	});
	
	$( document ).on( 'click', '.wpsd_edit_image_links', function() {
		var status = $( this ).data( 'status' );
		if ( 'closed' === status ) {
			$( '.wpsd_image_links_list_wrap' ).slideDown();
			$( this ).data( 'status', 'open' );
		} else {
			$( '.wpsd_image_links_list_wrap' ).slideUp();
			$( this ).data( 'status', 'closed' );
		}
	});

	$( document ).on( 'click', '.wpsd_close_image_links', function() {
		$( '.wpsd_image_links_list_wrap' ).slideUp();
		$( '.wpsd_edit_image_links' ).data( 'status', 'closed' );
	});

	$( document ).on( 'click', '#set-post-thumbnail-btn', function() {
		$( '#set-post-thumbnail' ).trigger( 'click' );
	});

	$( document ).on( 'click', '#remove-post-thumbnail-btn', function() {
		$( '#remove-post-thumbnail' ).trigger( 'click' );
		var no_photo = wpsdAdminParams.no_photo;
		$( '.wpsd_main_image img' ).attr( 'src', no_photo );
	});

	$( document ).on( 'click', '.vehicle_photo_remove', function() {
		var post_id = $( '#vehicle_photo_links' ).data( 'post-id' );
		var vehicle_link = $( this ).data( 'car-link' );
		var src_id = $( this ).data( 'src-id' );
		var type = $( this ).data( 'type' );
		var cnt = $( this ).data( 'cnt' );
		console.log( 'Type: ' + type );
		remove_vehicle_image( post_id, vehicle_link, cnt, src_id, type );
	});

	$( document ).on( 'click', '.wpsd_api_validate', function() {
        show_saving( wpsdAdminParams.strings.str53 );
		var nonce = $( '#admin-api-nonce' ).val();
		var api_key = $( '.wpsd_api_key' ).val();

		$( '.wpsd_api_key' ).css( 'background', '#ff0' );

		$.ajax({
			type: 'POST',
			data: {'action': 'wpsd_api_validation', 'nonce': nonce, 'api_key': api_key},
			url: wpsdAdminParams.ajaxurl,
			timeout: 30000,
			error: function() {
                $( '.wpsd-admin-lightbox' ).removeClass( 'active-lightbox' );
				$( '.wpsd_api_key' ).css( 'background', '#f00' );
				setTimeout( function() {
					$( '.wpsd_api_key' ).css( 'background', '#fff' );
				}, 2000);
			},
			dataType: 'json',
			success: function( json ) {
                $( '.wpsd-admin-lightbox' ).removeClass( 'active-lightbox' );
				if ( 'success' === json.result ) {
					$( '.wpsd_api_key' ).css( 'background', '#0f0' );
					$( '.wpsd-approved' ).addClass( 'wpsd-approved-true' );
					setTimeout( function() {
						$( '.wpsd_api_key' ).css( 'background', '#fff' );
						location.reload();
					}, 1000);
				} else {
					$( '.wpsd_api_key' ).css( 'background', '#f00' );
					$( '.wpsd-approved' ).removeClass( 'wpsd-approved-true' );					
				}
			}
		}); //= end ajax request
		
	});
	
	$( document ).on( 'click', '.wpsd_tabs li', function() {
		$( '.wpsd_tabs li' ).removeClass( 'wpsd_active_tab' );
		$(this).addClass( 'wpsd_active_tab' );
		var tab = $(this).data( 'tab' );

		$( '.wpsd_user_area' ).removeClass( 'wpsd_active_page' );

		$( '.' + tab).addClass( 'wpsd_active_page' );
		
	});

	$( document ).on( 'click', '.wpsd-type-wrap label', function() {
		var current_type = $( this ).parent().find( '.wpsd-type-data-wrap' );
		$( '.wpsd-type-wrap label' ).removeClass( 'chevron-up' );
		if ( 'none' === current_type.css( 'display' ) ) {
			$( '.wpsd-type-wrap .wpsd-type-data-wrap' ).slideUp();
			$( this ).addClass( 'chevron-up' );
			current_type.slideDown();
		} else {
			current_type.slideUp();
		}
	});

	$( document ).on( 'click', '.wpsd-type-specification-field-label', function() {
		var current_field = $( this ).parent().find( '.wpsd-type-specification-field-values' );
		var container = current_field.closest( '.wpsd-type-specification-field' );
		$( '.wpsd-type-specification-field-label' ).removeClass( 'chevron-up' );
		if ( 'none' === current_field.css( 'display' ) ) {
			$( '.wpsd-type-specification-field-values' ).slideUp();
			$( this ).addClass( 'chevron-up' );
			$( '.wpsd-type-specification-field' ).removeClass( 'spec-active' );
			container.addClass( 'spec-active' );
			current_field.slideDown();
		} else {
			container.removeClass( 'spec-active' );
			current_field.slideUp();
		}
	});

	$( document ).on( 'click', '.wpsd-fieldset-closed', function() {
		var fieldset = $( this ).parent().find( '.wpsd-fieldset-content' );
		if ( $( this ).hasClass( 'wpsd-fieldset-open' ) ) {
			$( this ).removeClass( 'wpsd-fieldset-open' );
			fieldset.slideUp();
		} else {
			$( this ).addClass( 'wpsd-fieldset-open' );
			fieldset.slideDown();
		}
	});

	$( document ).on( 'click', '.wpsd-option-group-label', function() {
		var fields = $( this ).parent().find( '.wpsd-option-group-wrap' );
		$( '.wpsd-option-group-label' ).removeClass( 'chevron-up' );
		if ( $( this ).parent().hasClass( 'wpsd-option-group-open' ) ) {
			$( this ).parent().removeClass( 'wpsd-option-group-open' );
			fields.slideUp();
		} else {
			$( this ).parent().addClass( 'wpsd-option-group-open' );
			$( this ).addClass( 'chevron-up' );
			fields.slideDown();
		}
	});

	$( document ).on( 'click', '.wpsd-type-data-button, .wpsd-remove-button', function() {
		var action = $( this ).data( 'action' );
		wpsd_admin_log( 'Action: ' + action );
		var type = $( this ).data( 'type' );
		wpsd_admin_log( 'Type: ' + type );

		if ( 'global' === type ) {
			if ( false === $( this ).hasClass( 'wpsd-type-data-button' ) ) {
				return false;
			}
		}

		if ( 'function' === typeof( actions[ action ] ) ) {
			var run_this = actions[ action ]( action, $( this ) );
		} else {
			wpsd_admin_log( 'Action not defined: ' + action );
		}
		
	});

	$( document ).on( 'click', '.wpsd-input', function() {
		$( '.wpsd-tool-tip' ).css( 'display', 'none' );
	});

	$( document ).on( 'click', '.wpsd-specification-field-option-show-ui-wrap', function() {
		var checkbox = $( this ).find( '.wpsd-toggle-button' );
		var checked = checkbox.prop( 'checked' );
		slug = checkbox.data( 'slug' );
		wpsd_admin_log( 'Slug: ' + slug );
		if ( true === checked ) {
			wpsd_admin_log( 'true' );
			$( '.wpsd-specification-plural-' + slug ).addClass( 'show' );
			$( '.wpsd-specification-restrict-' + slug ).addClass( 'show' );
		} else {
			wpsd_admin_log( 'false' );
			$( '.wpsd-specification-plural-' + slug ).removeClass( 'show' );			
			$( '.wpsd-specification-restrict-' + slug ).removeClass( 'show' );
		}
	});
	
	$( document ).on( 'click', '.wpsd-specification-field-option-taxonomy', function() {
		var status = $( this ).prop( 'checked' );
		var type = $( this ).data( 'type' );
		var group = $( this ).data( 'group' );
		var specification = $( this ).data( 'specification' );

		if ( false === status ) {
			$( '.wpsd-type-specification-' + type + '-' + group + '-' + specification + ' .wpsd-specification-field-option-show-ui' ).prop( 'disabled', true );
			$( '.wpsd-type-specification-' + type + '-' + group + '-' + specification + ' .wpsd-specification-field-option-show-ui' ).prop( 'checked', false );
		} else {
			$( '.wpsd-type-specification-' + type + '-' + group + '-' + specification + ' .wpsd-specification-field-option-show-ui' ).prop( 'disabled', false );
		}
	});

	var get_spec_options = function( e_spec, type, group ) {
		var specification = $( e_spec ).data( 'value' );
		wpsd_admin_log( 'Specification: ' + specification );
		var slug = wpsd_slugify( specification );
		wpsd_admin_log( 'Slug: ' + slug );
		var group_slug = wpsd_slugify( group );
		wpsd_admin_log( 'Group Slug: ' + group_slug );
		var spec_options = {};
		spec_options.name = specification;
		spec_options.taxonomy = false;
		var spec_element = $( '.wpsd-type-specification-' + type + '-' + group_slug + '-' + slug );
		if ( true === spec_element.find( '.wpsd-specification-field-option-taxonomy' ).prop( 'checked' ) ) {
			spec_options.taxonomy = true;
		} else {
			spec_options.taxonomy = false;
		}
		spec_options['show-ui'] = false;
		if ( true === spec_element.find( '.wpsd-specification-field-option-show-ui' ).prop( 'checked' ) ) {
			spec_options['show-ui'] = true;
		} else {
			spec_options['show-ui'] = false;
		}
		spec_options.restrict = false;
		if ( true === spec_element.find( '.wpsd-specification-field-option-restrict' ).prop( 'checked' ) ) {
			spec_options.restrict = true;
		} else {
			spec_options.restrict = false;
		}
		spec_options.disabled = false;
		if ( true === spec_element.find( '.wpsd-specification-field-option-disabled' ).prop( 'checked' ) ) {
			spec_options.disabled = true;
		} else {
			spec_options.disabled = false;
		}
		spec_options.plural = spec_element.find( '.wpsd-specification-field-option-plural' ).val();
		spec_options.label = spec_element.find( '.wpsd-specification-field-option-label' ).val();
		spec_options['default-value'] = spec_element.find( '.wpsd-specification-field-option-default-value' ).val();
		wpsd_admin_log( 'spec_options =>' );
		wpsd_admin_log( spec_options );
		return spec_options;
	};

	//= Save settings
	$( document ).on( 'click', '.wpsd-save-settings-button', function() {
		var name = $( this ).data( 'name' );
		var action = $( this ).data( 'action' );
		var form = $( '.wpsd_setting_form_' + name );
	    var datastring = formToJSON( form );
		var groups = form.find( '.wpsd_admin_group' );
		wpsd_admin_log( 'Form: ' + form );
		wpsd_admin_log( 'Action: ' + action );
		wpsd_admin_log( 'datastring ===>' );
		wpsd_admin_log( datastring );
		wpsd_admin_log( 'Groups ===>' );
		wpsd_admin_log( groups );
		var fieldData = {};
		wpsd_admin_log( 'Save Button Clicked' );
		if ( 1 > groups.length ) {
			wpsd_admin_log( 'Multiple groups' );
			if ( 'save_vehicle_options' === action ) {
				var datastring = sortFormToJSON( form );
				//= The vehicle option tab is it's own beast
				//= Save the nonce to a variable so we can use it below
				var nonce = datastring['admin-vehicle_options-nonce'];
				//= Empty the datastring - we won't use it on the server side
				datastring = {};
				//= Put the nonce back in it because we'll need it on the server
				datastring['admin-vehicle_options-nonce'] = nonce;
				//= Set our fieldData.types to the fields on the screen
				fieldData = get_vehicle_types();
				datastring.global_specs = get_global_specs();
				datastring.no_group_specs = get_no_group_specs();
				datastring.no_group_options = get_no_group_options();
				datastring.group_options_list = get_options_list();
				wpsd_admin_log( 'save_vehicle_options datastring =>' );
				wpsd_admin_log( datastring );
			} else if ( 'save_search_form' === action ) {
				wpsd_admin_log( 'start save_search_form' );
				//= Save the nonce to a variable so we can use it below
				var nonce = $( '#admin-search_form-nonce' ).val();
				fieldData.fields = datastring;

				wpsd_admin_log( 'fieldData settings ===>' );
				wpsd_admin_log( fieldData );
				
				//= Empty the datastring - we won't use it on the server side
				datastring = {};
				//= Put the nonce back in it because we'll need it on the server
				datastring['admin-search_form-nonce'] = nonce;
				
			} else {
				wpsd_admin_log( 'Multiple groups - action not set' );
				wpsd_admin_log( form );
				return false;
			}
		} else {
			if ( 'save_locations' === action ) {
				var datastring = sortFormToJSON( form );
				$.each( groups, function( i, e ) {
					var fields = $( e ).data( 'fields' );
					fields = wpsdBase64.decode( fields );
					var fieldsObj = JSON.parse( fields );
					$.each(fieldsObj, function( k, v ) {
						var field = $( '#' + v.id_name );
						if ( 'undefined' !== typeof( field ) ) {
							var key = field.data( 'key' );
							if ( 'undefined' === typeof( key ) || 'undefined' === key ) {
								return;
							}
							if ( 'undefined' === typeof( fieldData[ key ] ) ) {
								fieldData[ key ] = {};
							}
							fieldData[ key ][ v.slug ] = field.val();
						}
					});
				});
				wpsd_admin_log( 'Location Data ===>' );
				wpsd_admin_log( fieldData );
            } else if ( 'save_sort' === action ) {
				var datastring = sortFormToJSON( form );
				wpsd_admin_log( 'Save Sort datastring ===>' );
				wpsd_admin_log( datastring );
            } else if ( 'save_api' === action ) {
				var datastring = sortFormToJSON( form );
				wpsd_admin_log( 'Save API datastring ===>' );
				wpsd_admin_log( datastring );
                if ( 'Yes' === $( '.wpsd-api-activate-vin_decodes' ).val() 
                    || 'Yes' === $( '.wpsd-api-activate-history_reports' ).val() )
                {
                    $( '.wpsd_api_items_wrap' ).removeClass( 'wpsd_api_items_inactive' );
                } else {
                    $( '.wpsd_api_items_wrap' ).addClass( 'wpsd_api_items_inactive' );
                }
			} else {
				var datastring = sortFormToJSON( form );
				wpsd_admin_log( 'Standard save triggered' );
				$.each( groups, function( i, e ) {
					var fields = $( e ).data( 'fields' );
					fields = wpsdBase64.decode( fields );
					var fieldsObj = JSON.parse( fields );
					$.each(fieldsObj, function( k, v ) {
						var field = $( '.cdff-' + v.type + '-' + v.slug );
						if ( 'checkbox' === v.type || 'radio' === v.type ) {
							field = $( '.cdff-' + v.type + '-item-' + v.slug );
							var checked = [];
							$.each( field, function( i, e ) {
								if ( $( e ).prop( 'checked' ) ) {
									checked.push( $( e ).val() );
								}
							});

							fieldData[ v.slug ] = checked;
						} else {
							fieldData[ v.slug ] = field.val();
						}
					});
				});
			}
		} //= end else
		show_saving( wpsdAdminParams.strings.str28 );

        datastring.action = 'wpsd_admin_handler';
		datastring.wpsd_action = action;
		datastring.setting = name;
		datastring[ 'admin' + name + '_nonce' ] = $( 'admin' + name + '_nonce' ).val();
		
		wpsd_admin_log( 'fieldData =>' );
		wpsd_admin_log( fieldData );
		var fieldDataJson = JSON.stringify( fieldData );
		wpsd_admin_log( 'fieldDataJson =>' );
		wpsd_admin_log( fieldDataJson );
		datastring.fieldData = wpsdBase64.encode( fieldDataJson );

		$.ajax({
            type: 'POST',
            url: wpsdAdminParams.ajaxurl,
			timeout: 5000,
			error: function() {
				$( '.wpsd-admin-lightbox' ).removeClass( 'active-lightbox' );
			},
			dataType: 'json',
			data: datastring,
            success: function( json ) {
				wpsd_admin_log( json );
				$( '.wpsd_settings_message_' + json.setting ).html( json.msg );
				$( '.wpsd_settings_message' ).addClass( 'active-message' );
				if ( 'save_vehicle_options' === action ) {
					$( '.wpsd_settings_message_' + json.setting ).html( json.msg + '<br />' + wpsdAdminParams.strings.str35 );
					location.reload();
				}
				setTimeout( function() {
					$( '.wpsd-admin-lightbox' ).removeClass( 'active-lightbox' );
					setTimeout( function() {
						$( '.wpsd_settings_message_' + json.setting ).html( '' );
						$( '.wpsd_settings_message' ).removeClass( 'active-message' );
					}, 3000);
				}, 500 );
				
            }
		});

	});

	var clean_option = function( option ) {
		re = new RegExp( '[(]', 'g' );
		option = option.replace( re, ' ' );
		re = new RegExp( '[)]', 'g' );
		option = option.replace( re, ' ' );
		wpsd_admin_log( 'Option Cleaned: ' + option );
		return option;
	};
	
	var maybe_add_option = function( action, options, option ) {
		option = clean_option( option );
		if ( 'remove' === action ) {
			var re = new RegExp( option + ', ', 'g' );
			options = options.replace( re, '' );
			re = new RegExp( option + ',', 'g' );
			options = options.replace( re, '' );
			re = new RegExp( option, 'g' );
			options = options.replace( re, '' );
			return options;
		}
		var exists = options.indexOf( option );
		if ( -1 === exists ) {
			var lastChar = options[ options.length -1 ];
			if ( ',' === lastChar ) {
				options += option;
			} else {
				if ( '' === options ) {
					options += option;
				} else {
					options += ',' + option;
				}
			}
		}
		options += '@@';
		options = options.replace( ',@@', '' );
		options = options.replace( '@@', '' );
		return options;
	};
	
	var wpsd_disable_global_spec = function( checked, type, field ) {
		var name = $( field ).attr( 'name' );
		if ( true === checked ) {
			$( '.wpsd-type-specification-global-default-' + name ).addClass( 'wpsd-field-disabled' );
		} else {
			$( '.wpsd-type-specification-global-default-' + name ).removeClass( 'wpsd-field-disabled' );			
		}
	};
	
	$( document ).on( 'click', '.wpsd-show-taxonomy-tip', function() {
		wpsd_open_tooltip( $( this ), wpsdAdminParams.strings.str31, wpsdAdminParams.strings.str32 );
	});

	$( document ).on( 'click', '.wpsdsp-update-json-button', function() {
		var do_it = confirm( wpsdAdminParams.strings.str1 );
		if ( true === do_it ) {
			var msg = wpsdAdminParams.strings.str44;
			show_saving( msg );
			var datastring = {};
			datastring.action = 'wpsdsp_manual_json_build';
			datastring.nonce = $( '#admin-search_form-nonce' ).val();

            $.ajax({
				type: 'POST',
				url: wpsdAdminParams.ajaxurl,
				timeout: 5000,
				error: function() {
					$( '.wpsd-admin-lightbox' ).removeClass( 'active-lightbox' );
				},
				dataType: 'json',
				data: datastring,
				success: function( json ) {
					$( '.wpsd-admin-lightbox' ).removeClass( 'active-lightbox' );
					wpsd_admin_log( json );
                    $( '.wpsdsp-update-json-msg' ).html( json.msg );
                    setTimeout( function() {
                        $( '.wpsdsp-update-json-msg' ).html( '' );
                    }, 4000 );
				}
			});
		} else {
			wpsd_admin_log( 'Build JSON canceled: ' + location_name );
		}
    });
	
    $( document ).on( 'click', '.wpsdsp-remove-schedule-button', function() {
		var do_it = confirm( wpsdAdminParams.strings.str1 );
		if ( true === do_it ) {
			var msg = wpsdAdminParams.strings.str46;
			show_saving( msg );
			var datastring = {};
			datastring.action = 'wpsdsp_set_schedule';
			datastring.nonce = $( '#admin-search_form-nonce' ).val();
            datastring.remove_schedule = '1';

            $.ajax({
				type: 'POST',
				url: wpsdAdminParams.ajaxurl,
				timeout: 5000,
				error: function() {
					$( '.wpsd-admin-lightbox' ).removeClass( 'active-lightbox' );
				},
				dataType: 'json',
				data: datastring,
				success: function( json ) {
					$( '.wpsd-admin-lightbox' ).removeClass( 'active-lightbox' );
					wpsd_admin_log( json );
                    $( '.wpsdsp-update-json-schedule-msg' ).html( json.msg );
                    $( '.wpsdsp-json-current-schedule-wrap' ).html( json.html );
                    setTimeout( function() {
                        $( '.wpsdsp-update-json-schedule-msg' ).html( '' );
                    }, 4000 );
				}
			});
		} else {
			wpsd_admin_log( 'Remove JSON canceled: ' + location_name );
		}
    });
    
    $( document ).on( 'click', '.wpsdsp-add-schedule-button', function() {
		var do_it = confirm( wpsdAdminParams.strings.str1 );
		if ( true === do_it ) {
			var msg = wpsdAdminParams.strings.str45;
			show_saving( msg );
			var datastring = {};
			datastring.action = 'wpsdsp_set_schedule';
			datastring.nonce = $( '#admin-search_form-nonce' ).val();
            datastring.set_wpsdsp_schedule = '1';
            datastring.hour = $( '.wpsdsp-schedule-hour' ).val();
            datastring.minute = $( '.wpsdsp-schedule-minute' ).val();

            $.ajax({
				type: 'POST',
				url: wpsdAdminParams.ajaxurl,
				timeout: 5000,
				error: function() {
					$( '.wpsd-admin-lightbox' ).removeClass( 'active-lightbox' );
				},
				dataType: 'json',
				data: datastring,
				success: function( json ) {
					$( '.wpsd-admin-lightbox' ).removeClass( 'active-lightbox' );
					wpsd_admin_log( json );
                    $( '.wpsdsp-update-json-schedule-msg' ).html( json.msg );
                    $( '.wpsdsp-json-current-schedule-wrap' ).html( json.html );
                    setTimeout( function() {
                        $( '.wpsdsp-update-json-schedule-msg' ).html( '' );
                    }, 4000 );
				}
			});
		} else {
			wpsd_admin_log( 'Build JSON canceled: ' + location_name );
		}
    });
    
	$( document ).on( 'change', '.wpsd-api-activate', function() {
		var field = $( this );
		var service = field.data( 'service' );
		var available = $( '.wpsd-td-' + service + '-available' ).html();
		var value = field.val();
		available = parseFloat( available );
		
		if ( wpsdAdminParams.strings.str48 === value ) {
			if ( 0 === available ) {
				wpsd_admin_log( 'None Available for ' + service );
				field.css( 'background-color', '#fbb' );
				$( '.wpsd-td-' + service + '-available' ).css( 'background-color', '#fbb' );
				$( '.wpsd-api-activate-msg-' + service ).html( wpsdAdminParams.strings.str47 );
				setTimeout( function() {
					field.css( 'background-color', '#fff' );
					$( '.wpsd-td-' + service + '-available' ).css( 'background-color', '#fff' );
					$( '.wpsd-api-activate-msg-' + service ).html( '' );
					$( '.wpsd-td-' + service + '-available' ).val( wpsdAdminParams.strings.str49 );
				}, 2000 );
				return;
			}
			return;
		}
	});
	
	var wpsd_remove_location = function( location_name ) {
		var location_rows = $( '.wpsd-admin-location-table tr' );
		var location_count = location_rows.length;
		//= subtract the header row
		location_count = location_count - 1;
		// IS THIS THE ONLY LOCATION?
		if ( 1 === location_count ) {
			alert( wpsdAdminParams.str41 );
			return;
		}
		var do_it = confirm( wpsdAdminParams.strings.str1 );
		if ( true === do_it ) {
			var msg = wpsdAdminParams.strings.str41;
			show_saving( msg );
			var datastring = {};
			datastring.action = 'wpsd_remove_location';
			datastring.nonce = $( '#admin-locations-nonce' ).val();
			datastring.location_name = location_name;
			$.ajax({
				type: 'POST',
				url: wpsdAdminParams.ajaxurl,
				timeout: 60000,
				error: function() {
					$( '.wpsd-admin-lightbox' ).removeClass( 'active-lightbox' );
				},
				dataType: 'json',
				data: datastring,
				success: function( json ) {
					$( '.wpsd-admin-lightbox' ).removeClass( 'active-lightbox' );
					wpsd_admin_log( json );
					wpsd_admin_log( 'reloading page' );
					location.reload();
				}
			});
		} else {
			wpsd_admin_log( 'Remove location canceled: ' + location_name );
		}
	};
	
	$( document ).on( 'click', '.wpsd-remove-location-button', function() {
		var location_name = $( this ).data( 'slug' );
		console.log( location_name );
		if ( '' === location_name || null === location_name ) {
			wpsd_admin_log( 'No location name given or remove location was canceled' );
		} else {
			wpsd_admin_log( 'Remove location named: ' + location_name );
			wpsd_remove_location( location_name );
		}
	});
	
	var wpsd_add_location = function( location_name ) {
		var msg = wpsdAdminParams.strings.str40;
		show_saving( msg );
		var datastring = {};
		datastring.action = 'wpsd_add_new_location';
		datastring.nonce = $( '#admin-locations-nonce' ).val();
		datastring.location_name = location_name;
		$.ajax({
			type: 'POST',
			url: wpsdAdminParams.ajaxurl,
			timeout: 5000,
			error: function() {
				$( '.wpsd-admin-lightbox' ).removeClass( 'active-lightbox' );
			},
			dataType: 'json',
			data: datastring,
			success: function( json ) {
				$( '.wpsd-admin-lightbox' ).removeClass( 'active-lightbox' );
				wpsd_admin_log( json );
				wpsd_admin_log( 'reloading page' );
				location.reload();
			}
		});
	};
	
	$( document ).on( 'click', '.wpsd-admin-location-add-button', function() {
		var location_name = prompt( wpsdAdminParams.strings.str39 );
		console.log( location_name );
		if ( '' === location_name || null === location_name ) {
			wpsd_admin_log( 'No location name given or add location was canceled' );
		} else {
			wpsd_admin_log( 'New location name: ' + location_name );
			wpsd_add_location( location_name );
		}
	});
	
	/***************************************************/
	/* Start vehicle edit */
	/***************************************************/
	var wpsd_vehicle_edit = function() {

		var wpsd_get_all_options = function() {
			var options = '';
			var option_lists = $( '.wpsd-edit-vehicle-options-list' );
			$.each( option_lists, function( i, e ) {
				var option_list = $( e ).val();
				if ( '' === option_list ) {
					return;
				}
				if ( '' === options ) {
					options += option_list;
					return;
				}
				var lastChar = options[options.length -1];
				if ( ',' !== lastChar ) {
					options += ',' + option_list;
				}
			});
			return options;
		};

		var wpsd_get_vehicle_type_edit = function( type ) {
			var datastring = {};
			datastring.action = 'wpsd_get_vehicle_type_edit';
			datastring.post_id = $( '.wpsd-edit-vehicle-options-metabox-wrap' ).data( 'post-id' );
			datastring.type = type;
			datastring.nonce = $( '#admin-edit-vehicle-nonce' ).val();
			show_saving( wpsdAdminParams.strings.str36 );
			
			$.ajax({
				type: 'POST',
				url: wpsdAdminParams.ajaxurl,
				timeout: 5000,
				error: function() {
					$( '.wpsd-admin-lightbox' ).removeClass( 'active-lightbox' );
				},
				dataType: 'json',
				data: datastring,
				success: function( json ) {
					wpsd_admin_log( json );
					$( '.wpsd-edit-vehicle-specs-metabox-wrap' ).replaceWith( json.specifications );
					$( '.wpsd-edit-vehicle-options-metabox-wrap' ).replaceWith( json.options );
					$( '.wpsd-admin-lightbox' ).removeClass( 'active-lightbox' );
					$( '.wpsd-edit-select-vehicle-type' ).css( 'background-color', '#fff' );
				}
			});
		};
		
		$( document ).on( 'click', '.wpsd-edit-vehicle-welcome-hide', function() {
			$( '#wpsd_edit_vehicle_welcome_metabox-hide' ).trigger( 'click' );
		});
		
		$( document ).on( 'change', '.wpsd-edit-select-vehicle-type', function() {
			$( this ).css( 'background-color', '#ff0' );
			var vehicle_type = $( this ).val();
			var vehicle_types_base64 = $( '.wpsd-edit-vehicle-specs-metabox-wrap' ).data( 'maps' );
			var vehicle_types_json = wpsdBase64.decode( vehicle_types_base64 );
			var vehicle_types_obj = JSON.parse( vehicle_types_json );
			wpsd_admin_log( vehicle_type );
			wpsd_admin_log( vehicle_types_json );
			wpsd_admin_log( vehicle_types_obj );
			var vehicle_type_data = vehicle_types_obj[ vehicle_type ];
			wpsd_admin_log( 'Selected vehicle type data =>' );
			wpsd_admin_log( vehicle_type_data );
			var icon = vehicle_type_data.icon;
			wpsd_admin_log( 'Icon: ' + icon );
			$( '.wpsd-edit-vehicle-type-icon' ).prop( 'src', icon );
			wpsd_get_vehicle_type_edit( vehicle_type );
		});

		$( document ).on( 'change', '.wpsd-edit-vehicle-options-list', function() {
			var all_options = wpsd_get_all_options();
			$( '.wpsd-edit-vehicle-options-list-all' ).val( all_options );		
		});
		
		$( document ).on( 'change', '.cdff-maybe-select-text', function() {
			var value = $( this ).val();
			$( this ).next( '.cdff-maybe-select-value' ).val( value );
			wpsd_admin_log( value );
		});
		
		$( document ).on( 'click', '.cdff-maybe-select', function() {
			var text_field = $( this ).find( '.cdff-maybe-select-text' );
			var value_field = $( this ).find( '.cdff-maybe-select-value' );
			var wrap_field = $( this ).find( '.cdff-maybe-select-option-wrap' );
			var options_base64 = wrap_field.data( 'options' );
			var options_json = wpsdBase64.decode( options_base64 );
			wpsd_admin_log( options_json );
			var options = JSON.parse( options_json );
			text_field.autocomplete({
				delay: 0,
				source: options,
				select: function ( event, ui ) {
					event.preventDefault();
					text_field.val( ui.item.label );
					wpsd_admin_log( 'Label: ' + ui.item.label );
					value_field.val( ui.item.value );
					wpsd_admin_log( 'Value: ' + ui.item.value );
				},
				minLength: 2
			});

		});
		
        var wpsd_get_service = function( post_id, vin, type ) {
            var msg = wpsdAdminParams.strings.str50;
            show_saving( msg );
            var datastring = {};
            var type_slug = type.replace( '-', '_' );
            datastring.action = 'wpsd_get_' + type_slug;
            datastring.nonce = $( '#wpsd_get_' + type_slug + '_nonce' ).val();
            datastring.post_id = post_id;
            datastring.vin = vin;

            wpsd_admin_log( 'Get ' + type + ' =>' );
            wpsd_admin_log( datastring );

            $.ajax({
                type: 'POST',
                url: wpsdAdminParams.ajaxurl,
                timeout: 60000,
                error: function( error ) {
                    console.log( 'Error' );
                    console.log( error );
                    //location.reload();
                    $( '.wpsd-admin-lightbox' ).removeClass( 'active-lightbox' );
                },
                dataType: 'json',
                data: datastring,
                success: function( json ) {
                    wpsd_admin_log( 'Service Results => ' );
                    wpsd_admin_log( json );
                    
                    //= Reload no matter what service was requested
                    window.location.href = json.edit;

                    //= TO DO Future: Remove or utilize the following code
                    if ( 'undefined' !== typeof( json.result ) ) {
                        if ( 'undefined' !== json.edit ) {
                            if ( 'vin_decodes' === json.service ) {
                                window.location.href = json.edit;
                            }
                        }
                    }
                    $( '.wpsd-admin-lightbox' ).removeClass( 'active-lightbox' );
                    //= End TO DO Future
                }
            });
            
        };

        $( document ).on( 'click', '.wpsd-open-data-api', function() {
            var status = $( '.wpsd-edit-vehicle-data-api' ).hasClass( 'wpsd-edit-vehicle-data-api-show' );
            if ( true === status ) {
                $( '.wpsd-edit-vehicle-data-api' ).removeClass( 'wpsd-edit-vehicle-data-api-show' );
                $( this ).val( wpsdAdminParams.strings.str51 );
            } else {
                $( '.wpsd-edit-vehicle-data-api' ).addClass( 'wpsd-edit-vehicle-data-api-show' );
                $( this ).val( wpsdAdminParams.strings.str52 );
            }
        });

        var wpsd_bad_vpd_vin = function( valid ) {
            if ( true === valid ) {
                $( '.wpsd-service-vin-value' ).removeClass( 'wpsd-bad-vin' );
                $( '.wpsd-service-data-api-message' ).html( '' );
            } else {
                $( '.wpsd-service-vin-value' ).addClass( 'wpsd-bad-vin' );
                var html = '';
                html += '<div class="wpsd-bad-vin wpsd-bad-vin-flash">';
                    html += wpsdAdminParams.strings.str58;
                html += '</div>';
                $( '.wpsd-service-data-api-message' ).html( html );
                setTimeout( function() {
                    $( '.wpsd-bad-vin' ).removeClass( 'wpsd-bad-vin-flash' );
                }, 2000 );
            }
        };
        
        $( document ).on( 'change', '.wpsd-service-vin-value', function() {
            var vin = $( this ).val();
            var is_valid = validateVin( vin );
            wpsd_admin_log( 'VIN validation: ' + is_valid );
            $( '.cdff-text-vin' ).val( vin );
            if ( true === is_valid ) {
                //= TO DO - HANDLE SUCCESS
                wpsd_bad_vpd_vin( is_valid );
            } else {
                //= TO DO - HANDLE FAIL
                wpsd_bad_vpd_vin( is_valid );
            }
        });
        
        var service_types = ['vin-decode', 'history-report'];

        $.each( service_types, function( i, e ) {
            $( document ).on( 'click', '.wpsd-service-' + e + '-button', function() {
                var post_id = $( this ).data( 'post-id' );
                var vin = $( '.wpsd-service-vin-value' ).val();
                var type = $( this ).data( 'type' );
                wpsd_admin_log( '<=== Start ' + type + ' ===>' );
                wpsd_admin_log( 'Post ID: ' + post_id );
                wpsd_admin_log( 'VIN: ' + vin );

                var is_valid = validateVin( vin );
                wpsd_admin_log( 'VIN validation: ' + is_valid );

                if ( true === is_valid ) {
                    wpsd_admin_log( 'Sending vin for ' + type + ': ' + vin );
                    wpsd_get_service( post_id, vin, e );
                } else {
                    //= TO DO - HANDLE FAIL
                    wpsd_bad_vpd_vin( is_valid );
                }

                wpsd_admin_log( '<=== End ' + type + ' ===>' );
            });

            $( document ).on( 'click', '.wpsd-view-' + e , function() {
                if ( 1 > $( '.wpsd-' + e + '-results' ).length ) {
                    return;
                }
                var decode = $( '.wpsd-' + e + '-results' ).val();
                var json = JSON.parse( decode );
                console.log( '<=== Start ' + e + ' raw results ===>' );
                console.log( decode );
                console.log( '<=== End ' + e + ' raw results ===>' );
                console.log( '<=== Start ' + e + ' json results ===>' );
                console.log( json );
                console.log( '<=== End ' + e + ' json results ===>' );
                if ( 'history-report' === e ) {
                    $( '.wpsd-vehicle-history-report-wrap' ).css( 'display', 'block' );
                    show_vehicle_edit_lightbox();
                }
            });
            
            $( document ).on( 'click', '.wpsd-remove-' + e , function() {
                var do_it = confirm( wpsdAdminParams.strings.str57 );
                if ( true != do_it ) {
                    return;
                }
                var msg = wpsdAdminParams.strings.str56;
                show_saving( msg );
                var datastring = {};
                var type = e;
                var post_id = $( this ).data( 'post-id' );
                var type_slug = type.replace( '-', '_' );
                datastring.action = 'wpsd_remove_report';
                datastring.nonce = $( '#wpsd_get_' + type_slug + '_nonce' ).val();
                datastring.post_id = post_id;
                datastring.type = type;
                wpsd_admin_log( 'Remove ' + type + ' =>' );
                wpsd_admin_log( datastring );
                $.ajax({
                    type: 'POST',
                    url: wpsdAdminParams.ajaxurl,
                    timeout: 60000,
                    error: function( error ) {
                        console.log( 'Error' );
                        console.log( error );
                        $( '.wpsd-admin-lightbox' ).removeClass( 'active-lightbox' );
                    },
                    dataType: 'json',
                    data: datastring,
                    success: function( json ) {
                        wpsd_admin_log( 'Service Results => ' );
                        wpsd_admin_log( json );
                        $( '.wpsd-view-' + json.type ).remove();
                        $( '.wpsd-remove-' + json.type ).remove();
                        $( '.wpsd-' + json.type + '-result-label' ).remove();
                        //= TO DO Future: Remove or utilize the following code
                        $( '.wpsd-admin-lightbox' ).removeClass( 'active-lightbox' );
                        //= End TO DO Future
                    }
                }); 
            });
            
        }); //= end $.each( service_types, function( i, e ) {
        
        $( document ).on( 'click', '.wpsd-close-report', function() {
            $( '.wpsd-vehicle-history-report-wrap' ).css( 'display', 'none' );
            $( '.wpsd-mv-report-wrap' ).css( 'display', 'none' );
            $( '.wpsd-oc-report-wrap' ).css( 'display', 'none' );
            hide_vehicle_edit_lightbox();
        });

        $( document ).on( 'click', '.wpsd-lightbox', function() {
            $( '.wpsd-vehicle-history-report-wrap' ).css( 'display', 'none' );
            $( '.wpsd-mv-report-wrap' ).css( 'display', 'none' );
            $( '.wpsd-oc-report-wrap' ).css( 'display', 'none' );
            hide_vehicle_edit_lightbox();
        });
        
        var show_vehicle_edit_lightbox = function() {
            if ( 1 > $( '.wpsd-lightbox' ).length ) {
                var lightbox = '<div class="wpsd-lightbox"></div>';
                $( 'body' ).append( lightbox );
            }
            $( '.wpsd-lightbox' ).css( 'display', 'block' );
        };
        
        var hide_vehicle_edit_lightbox = function() {
            $( '.wpsd-lightbox' ).css( 'display', 'none' );
        };
        
	};

	if ( 0 < $( '.post-type-vehicle' ).length ) {
		wpsd_vehicle_edit();
	}
    
    //= https://stackoverflow.com/questions/26407015/javascript-jquery-vin-validator/26408196
    var validateVinNonUS = function( vin ) {
        var re = new RegExp("^[A-HJ-NPR-Z\\d]{8}[\\dX][A-HJ-NPR-Z\\d]{2}\\d{6}$");
        return vin.match(re);
    }

    //= https://stackoverflow.com/questions/26407015/javascript-jquery-vin-validator/26408196
    var validateVin = function( vin ) {
        return validate(vin);

        // source: https://en.wikipedia.org/wiki/Vehicle_identification_number#Example_Code
        // ---------------------------------------------------------------------------------
        function transliterate( c ) {
            return '0123456789.ABCDEFGH..JKLMN.P.R..STUVWXYZ'.indexOf(c) % 10;
        }

        function get_check_digit( vin ) {
            var map = '0123456789X';
            var weights = '8765432X098765432';
            var sum = 0;
            for (var i = 0; i < 17; ++i)
                sum += transliterate(vin[i]) * map.indexOf(weights[i]);
                return map[sum % 11];
        }

        function validate( vin ) {
            if (vin.length !== 17) return false;
            return get_check_digit(vin) === vin[8];
        }
        // ---------------------------------------------------------------------------------
    }
    
	/***************************************************/
	/* End vehicle edit */
	/***************************************************/
	
	/***************************************************/
	/* Start vehicle column edit */
	/***************************************************/

	$( document ).on( 'change', '.wpsd-edit-field, .wpsd-edit-sold', function() {
		$( this ).addClass( 'saving' );
		var post_id = $( this ).data( 'post-id' );
		var type = $( this ).data( 'type' );
		var value = $( this ).val();
		
		var msg = wpsdAdminParams.strings.str43;
		show_saving( msg );
		var datastring = {};
		datastring.action = 'wpsd_edit_column_update';
		datastring.nonce = $( '#wpsd_admin_update_nonce_' + post_id ).val();
		datastring.post_id = post_id;
		datastring.type = type;
		datastring.value = value;

		console.log( datastring );

		$.ajax({
			type: 'POST',
			url: wpsdAdminParams.ajaxurl,
			timeout: 5000,
			error: function( error ) {
				console.log( 'Error' );
				console.log( error );
				$( '.wpsd-admin-lightbox' ).removeClass( 'active-lightbox' );
				$( '#' + json.type + '_' + json.post_id + '.wpsd-edit-' + json.type ).removeClass( 'saving' );
				$( '#' + json.type + '_' + json.post_id + '.wpsd-edit-' + json.type ).addClass( 'failed' );
				setTimeout( function() {
					$( '#' + json.type + '_' + json.post_id + '.wpsd-edit-' + json.type ).removeClass( 'failed' );
				}, 3000 );
			},
			dataType: 'json',
			data: datastring,
			success: function( json ) {
				wpsd_admin_log( json );
				$( '.wpsd-admin-lightbox' ).removeClass( 'active-lightbox' );
				$( '#' + json.type + '_' + json.post_id + '.wpsd-edit-' + json.type ).removeClass( 'saving' );
				$( '#' + json.type + '_' + json.post_id + '.wpsd-edit-' + json.type ).addClass( 'saved' );
				setTimeout( function() {
					$( '#' + json.type + '_' + json.post_id + '.wpsd-edit-' + json.type ).removeClass( 'saved' );
				}, 3000 );
			}
		});
	});
	
	/***************************************************/
	/* End vehicle column edit */
	/***************************************************/

	/***************************************************/
	/* Start Car Demon conversion code */
	/***************************************************/
    var wpsd_import_cd = function( type ) {
		var msg = wpsdAdminParams.strings.str54;
		show_saving( msg );
		var datastring = {};
		datastring.action = 'wpsd_car_demon_import_handler';
		datastring.nonce = $( '#admin-get-this-party-started-nonce' ).val();
		datastring.type = type;

		console.log( datastring );

		$.ajax({
			type: 'POST',
			url: wpsdAdminParams.ajaxurl,
			timeout: 5000,
			error: function( error ) {
				console.log( 'Error' );
				console.log( error );
				$( '.wpsd-admin-lightbox' ).removeClass( 'active-lightbox' );
                location.reload();
			},
			dataType: 'json',
			data: datastring,
			success: function( json ) {
				wpsd_admin_log( json );
				$( '.wpsd-admin-lightbox' ).removeClass( 'active-lightbox' );
                $( '.wpsd-cd-import-message' ).html( json.message );
                location.reload();
            }
		});
    };
    
    $( document ).on( 'click', '.wpsd-cd-import-button', function() {
        var agreed = $( '.wpsd-cd-agree' ).prop( 'checked' );
        
        if ( true !== agreed ) {
            $( '.wpsd-cd-agree-wrap' ).addClass( 'wpsd-no-agree' );
            alert( wpsdAdminParams.strings.str55 );
            setTimeout( function() {
                $( '.wpsd-cd-agree-wrap' ).removeClass( 'wpsd-no-agree' );
            }, 3000 );
            return;
        }
        
        wpsd_admin_log( 'Start Car Demon import' );
        var type = $( '.wpsd-cd-vehicle-type' ).val();
        //= if no type then pop an alert and highlight the fiels
        if ( '' === type ) {
                $('html, body').animate({
                    scrollTop: $( '.wpsd-cd-vehicle-type' ).offset().top - 100
                }, 100);
            alert( 'You must enter a name for your vehicle type.' );
            $( '.wpsd-cd-vehicle-type' ).css( 'border', '2px #f00 solid' );
            setTimeout( function() {
                $( '.wpsd-cd-vehicle-type' ).css( 'border', '1px #000 solid' );
            }, 3000 );
            return;
        }
        //= Request import
        wpsd_import_cd( type );
    });

    /***************************************************/
	/* End Car Demon conversion code */
	/***************************************************/
    
	/***************************************************/
	/* Start Forms tab code */
	/***************************************************/
    $( document ).on( 'click', '.wpsd-forms-tab', function() {
        var tab = $( this ).data( 'tab' );
        $( '.wpsd-forms-tab' ).removeClass( 'active' );
        $( this ).addClass( 'active' );
        $( '.wpsd-forms-tab-content' ).removeClass( 'active' );
        $( '.wpsd-forms-tab-content-' + tab ).addClass( 'active' );
    });
    
	/***************************************************/
	/* End Forms tab code */
	/***************************************************/

	var wpsd_admin_log = function( msg ) {
		if ( true === wpsd_admin_log_on ) {
			console.log( msg );
		}
	}
    
    //= Validate the vin field, if it exists, when the page loads
    if ( 0 < $( '.wpsd-service-vin-value' ).length ) {
        $( '.wpsd-service-vin-value' ).trigger( 'change' );
    }

});