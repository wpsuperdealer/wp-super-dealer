// JavaScript Document
"use strict";

/*****************************************************/
//= Start Calculator
/*****************************************************/
function returnPayment() {
	var Principal = document.calc.total.value
	Principal = Principal.replace(',','');
    var down = document.calc.down.value;
    down = down.replace(',','');
    Principal = Principal - down;
	if (document.calc.rate.value==0) {
		var Rate = 0.000000001
	} else {
		var Rate = (document.calc.rate.value/100)/12
	}
	var Rate = (document.calc.rate.value/100)/12
	var Term = document.calc.numPmtYr.value
	document.calc.pmt.value ="$" + find_payment(Principal, Rate, Term);
}

function find_payment(PR, IN, PE) {
	var PAY = ((PR * IN) / (1 - Math.pow(1 + IN, -PE)));
	return PAY.toFixed(2);
}
/*****************************************************/
//= End Calculator
/*****************************************************/

//= Start jQuery
jQuery( document ).ready(function($) {
	/*****************************************************/
	//= Start Dynamic Load
	/*****************************************************/
	var wpsd_autoload = function() {
		var has_next_page = $( wpsdParams.next ).length;
		if ( 0 < has_next_page ) {
			var loader_html = '';
			loader_html += '<div class="wpsd-load-more-wrap">';
				loader_html += '<img class="wpsd-load-more" src="' + wpsdParams.loader + '" />';
			loader_html += '</div>';
			$( window ).scroll( function() {
				// This is then function used to detect if the element is scrolled into view
				function elementScrolled( elem ) {
					var docViewTop = $( window ).scrollTop();
					var docViewBottom = docViewTop + $( window ).height();
					var elemTop = $( elem ).offset().top;
					return ( ( elemTop <= docViewBottom ) && ( elemTop >= docViewTop ) );
				}

				if ( 0 < $( wpsdParams.pagination + ' ' + wpsdParams.next ).length ) {
					if ( elementScrolled( wpsdParams.pagination + ' ' + wpsdParams.next ) ) {
						var next = $( wpsdParams.pagination + ' ' + wpsdParams.next + ' a' ).attr( 'href' );
						$( wpsdParams.pagination ).html( loader_html );
						$.get( next ).then( function( data ) {
							var next_page_obj = $( data );
							var inventory_wrap = next_page_obj.find( wpsdParams.container );
							var navigation = next_page_obj.find( wpsdParams.pagination );
							var navigation_html = $( navigation ).html();

							var items = next_page_obj.find( wpsdParams.items );
							if ( 0 < items.length ) {
								var items_html = '';
								$.each( items, function( i, e ) {
									items_html += $( e )[0].outerHTML;
								});
							}
							$( wpsdParams.items ).last().after( items_html );
							
							$( wpsdParams.pagination ).html( navigation_html );
							
						});
					}
				}
			});			
		}
	}; //= end var wpsd_autoload = function() {

	if ( false !== wpsdParams.dynamic_load ) {
		wpsd_autoload();
	}
	/*****************************************************/
	//= End Dynamic Load
	/*****************************************************/
	var wpsd_validateURL = function (textval) {
		var urlregex = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/;
		return urlregex.test(textval);
	};
	
	var wpsd_getCookie = function(name) {
		var value = "; " + document.cookie;
		var parts = value.split("; " + name + "=");
		if (parts.length == 2) return parts.pop().split(";").shift();
	}

	var srp_get_cookie = function( cname ) {
		var cookies = wpsd_getCookie( cname );
		return cookies;
	}
	
	var srp_set_cookie = function( cname, cvalue, exdays ) {
		var current_cookies = srp_get_cookie( 'srp_style' );

		if ( typeof( current_cookies ) === 'undefined' ) {
			current_cookies = [];
		}

		if ( current_cookies.length > 0 ) {
			var current_cookies_arr = current_cookies.split( ',' );
		} else {
			var current_cookies_arr = [];
		}

		var index = current_cookies_arr.indexOf( cvalue.toString() );
		if ( index === -1 ) {
			current_cookies_arr.push( cvalue.toString() );
		}

		current_cookies_arr.join( ',' );

		var d = new Date();
		d.setTime( d.getTime() + ( exdays * 24 * 60 * 60 * 1000 ) );
		var expires = "expires="+ d.toUTCString();
		document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
	};

	var wpsd_srp = function() {
				
		//= TO DO Future: need a more reliable js image replacement for bad images
		$( '#wpsd-inventory-wrap img' ).on( 'error', function () {
			//$( this ).attr( 'src', wpsdParams.error_image );
		});

		$( document ).on( 'click', '.wpsd-switch-srp-style-item', function() {
			var style = $( this ).data( 'style' );
			$( '.wpsd-switch-srp-style-item' ).removeClass( 'active' );
			$( this ).addClass( 'active' );
			$( '.srp-item' ).removeClass( 'full' );
			$( '.srp-item' ).removeClass( 'list' );
			$( '.srp-item' ).removeClass( 'grid' );
			$( '.srp-item' ).addClass( style );
			srp_set_cookie( 'srp_style', style, 30 );
		});
	};

	if ( 0 < $( '#wpsd-inventory-wrap' ).length ) {
		wpsd_srp();
	}
	
	var wpsd_vdp = function() {
        //lgZoom, lgAutoplay, lgComment, lgFullscreen , lgHash, lgPager, lgRotate, lgShare, lgThumbnail, lgVideo, lgMediumZoom
        var vdp_gallery = $( '.vdp-thumbnails-wrap' )[0];
        lightGallery( vdp_gallery, {
            plugins: [lgZoom, lgThumbnail, lgFullscreen, lgShare],
            selector: '.vdp-thumbnail-wrap',
			thumbnail: true,
			fullScreen: true,
			zoom: true,
			share: true,
        });
		
		$( document ).on( 'click', '.vdp-main-image, .vdp-thumbnails-view-all', function() {
			var thumbnails = $( '.vdp-thumbnail-wrap' );
			if ( 0 < thumbnails.length ) {
				$( '.vdp-thumbnails .vdp-thumbnail-wrap:nth-of-type(1)' ).trigger( 'click' );
			}
		});
	}
	if ( 0 < $( '.wpsd-vdp-wrap' ).length ) {
		wpsd_vdp();
	}
	
    //= Adjust payment calculator when downpayment is updated
    $( document ).on( 'change', '.wpsd-calc-price, .wpsd-calc-down-payment, .wpsd-calc-amount-financed', function() {
        var price = $( '.wpsd-calc-price' ).val();
        var down = $( '.wpsd-calc-down-payment' ).val();
        var amount_financed = price - down;
        $( '.wpsd-calc-amount-financed' ).val( amount_financed );
        returnPayment();
    });
    
    $( document ).on( 'change', '.wpsd-calc-pr, .wpsd-calc-payments', function() {
        returnPayment();
    });
    
    if ( 0 < $( '.wpsd-calc-payments' ).length ) {
        returnPayment();
    }
    
    //= Accordian
    var wpsd_accordian = function() {
        var flag = false;
        if ( 1 > $( '.wpsd-accordian-all-open' ).length ) {
            $( '.wpsd-vehicle-options-group' ).each( function( i, e ) {
                if ( false === flag ) {
                    flag = true;
                    return;
                }
                $( e ).find( '.wpsd-vehicle-options-sub-group' ).css( 'display', 'none' );
            });
        }
        
        $( document ).on( 'click', '.wpsd-vehicle-options-group-title', function() {
            var group = $( this ).data( 'slug' );
            var status = $( '.wpsd-vehicle-options-group-' + group + ' .wpsd-vehicle-options-sub-group' ).css( 'display' );
            if ( 'none' === status ) {
                if ( 0 < $( '.wpsd-one-panel' ).length ) {
                    $( '.wpsd-vehicle-options-sub-group' ).slideUp();
                }
                $( '.wpsd-vehicle-options-group-' + group + ' .wpsd-vehicle-options-sub-group' ).slideDown();
            } else {
                $( '.wpsd-vehicle-options-group-' + group + ' .wpsd-vehicle-options-sub-group' ).slideUp();
            }
        });
    };
    
    if ( 0 < $( '.wpsd-accordian' ).length ) {
        wpsd_accordian();
    }
    
    
});