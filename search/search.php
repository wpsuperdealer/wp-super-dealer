<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

define( 'WPSDSP_VER', '2.4.1' );

if ( ! defined( 'WPSDSP_NO_COOKIE_SEARCH' ) ) {
	define( 'WPSDSP_NO_COOKIE_SEARCH', true );
}

require_once( 'admin/wpsdsp-admin.php' );
require_once( 'includes/wpsdsp-handler.php' );
require_once( 'includes/search-form.php' );
require_once( 'includes/wpsdsp-fields.php' );
require_once( 'includes/widget.php' );
require_once( 'includes/wpsdsp-build-cache.php' );
require_once( 'includes/wpsdsp-build-cache-items.php' );
require_once( 'includes/wpsdsp-query.php' );
require_once( 'includes/wpsdsp-searched-by.php' );
require_once( 'includes/wpsdsp-import.php' );

add_action( 'admin_enqueue_scripts', 'wpsdsp_admin_enqueue_style' );
add_action( 'elementor/editor/before_enqueue_scripts', 'wpsdsp_admin_enqueue_style' );
function wpsdsp_admin_enqueue_style() {
	wp_enqueue_style( 'wpsdsp-admin-css', WPSD_PATH . '/search/css/wpsdsp-admin.css' );
	wp_register_script( 'wpsdsp-admin-js', WPSD_PATH . '/search/js/wpsdsp-admin.js', array( 'jquery' ), WPSDSP_VER );
	wp_localize_script( 'wpsdsp-admin-js', 'wpsdsp_AdminParameters', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'spinner' => WPSD_PATH . 'images/wpspin_light.gif',
	) );
	wp_enqueue_script( 'wpsdsp-admin-js' );
}

add_action( 'wp_enqueue_scripts', 'wpsdsp_enqueue_style' );
function wpsdsp_enqueue_style() {
	$search_options = wpsd_get_search_settings();
	
	$min_max_values = get_option( 'wpsdsp_min_max_values', array() );

	if ( count( $min_max_values ) > 0 ) {
		foreach( $min_max_values as $key=>$value ) {
			if ( isset( $_GET[ $key ] ) ) {
				if ( ! empty( $_GET[ $key ] ) ) {
					$min_max_values[ $key . '_start' ] = sanitize_text_field( $_GET[ $key ] );
				} else {
					$min_max_values[ $key . '_start' ] = $min_max_values[ $value ];
				}
			} else {
				if ( ! isset( $min_max_values[ $key . '_start' ] ) ) {
					$min_max_values[ $key . '_start' ] = $value;
				}
			}
		}
	}

	wp_enqueue_style( 'cds-form-css', plugins_url().'/wp-super-dealer/search/css/wpsdsp.css' );

	//= Load and localize js files as needed
	$blog_id = get_current_blog_id();
	global $wpsd_options;
	if (isset($wpsd_options['price_before'])) {
		$currency_before = $wpsd_options['price_before'];
	} else {
		$currency_before = "$";
	}
	if (isset($wpsd_options['price_after'])) {
		$currency_after = $wpsd_options['price_after'];
	} else {
		$currency_after = "";
	}

	$upload_dir = wp_upload_dir();
	$json_url = $upload_dir['baseurl'];
	if ( is_ssl() ) {
		$json_url = str_replace( 'http:', 'https:', $json_url );
	}

	$reset_on_back_page = false;

	if ( defined( 'WPSDSP_RESET_ON_BACKPAGE' ) ) {
		$reset_on_back_page = WPSDSP_RESET_ON_BACKPAGE;
	}

	if ( defined( 'WPSD_NO_ALL' ) ) {
		$all_str = '';
	} else {
		$all_str = __('ALL ', 'wp-super-dealer');
	}
	if ( defined( 'WPSD_ALL_STR' ) ) {
		$all_str = WPSD_ALL_STR;
	}

	$fields = wpsdsp_active_fields();

	$field_labels = get_wpsdsp_field_labels( true );
	$field_labels = apply_filters( 'wpsdsp_field_labels_filter', $field_labels );

	//= $template = false, $field = '', $text = '', $options = array()
	$options = array();

	$options['type'] = 'select';
	$template_select = get_wpsdsp_field( true, '', '{value}', $options );

	$options['type'] = 'checkbox';
	$template_checkbox = get_wpsdsp_field( true, '', '{value}', $options );
	
	$template_selected_html = wpsdsp_selected_html( true, '{field}', '{value}' );

	$loading_inventory = '<div class="wpsdsp_loading_inventory">';
		$loading_inventory .= '<div class="wpsdsp-loading-inventory-wrap"></div>';
		$loading_inventory .= '<img src="' . WPSD_PATH . '/search/images/loading_more_vehicles.gif" />';
	$loading_inventory .= '</div>';
	$loading_inventory = apply_filters( 'wpsdsp_loading_inventory_filter', $loading_inventory );

	$lang_strings = array(
		'view_results' => __( 'View Inventory', 'wp-super-dealer' ),
		'filter_inventory' => __( 'Filter Inventory', 'wp-super-dealer' ),
        'on' => __( 'on', 'wp-super-dealer' ),
        'yes' => __( 'yes', 'wp-super-dealer' ),
        'true' => __( 'true', 'wp-super-dealer' ),
        'false' => __( 'false', 'wp-super-dealer' ),
        'no' => __( 'no', 'wp-super-dealer' ),
	);

    
    if ( $search_options['save_json'] ) {
        $search_cache = __( 'on', 'wp-super-dealer' );
    } else {
        $search_cache = '';
    }
    
	wp_register_script( 'wp-super-dealer/search-js', WPSD_PATH . '/search/js/wpsdsp.js', array( 'jquery', 'jquery-ui-core', 'jquery-ui-slider' ) );
	wp_localize_script( 'wp-super-dealer/search-js', 'wpsdSearchParams', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'jsonurl' => apply_filters( 'wpsd_json_url_filter', trailingslashit( esc_url( $json_url ) ) . 'search-json' . esc_html( $blog_id ) . '.txt' ),
		'wpsd_path' => WPSD_PATH,
		'site_url' => get_bloginfo('wpurl'),
		'min_max_values' => $min_max_values,
		'save_cache' => esc_html( $search_cache ),
		'items_per_page' => 12,
		'num_display_entries' => 3,
		'num_edge_entries' => 2,
		'prev_text' => '&lt;',
		'next_text' => '&gt;',
		'is_home' => is_front_page(),
		'all_prices' => 1,
		'wpsdsp_all' => '',
		'blog_id' => esc_html( $blog_id ),
		'hide_count' => esc_html( $search_options['hide_count'] ),
		'currency_before' => esc_html( $currency_before ),
		'currency_after' => esc_html( $currency_after ),
		'all_str' => esc_html( $all_str ),
		'reset_on_back_page' => esc_html( $reset_on_back_page ),
		'fields' => $fields,
		'labels' => $field_labels,
		'template_select' => esc_html( $template_select ),
		'template_checkbox' => esc_html( $template_checkbox ),
		'template_selected_html' => esc_html( $template_selected_html ),
		'wpsdsp_autoload' => esc_html( $search_options['autoload'] ),
		'loading_inventory' => $loading_inventory,
		'is_mobile' => wp_is_mobile(),
		'lang_strings' => $lang_strings,
		'wpsdsp_mobile_search_form_action' => esc_html( $search_options['mobile_action'] ),
	));
	wp_enqueue_script( 'wp-super-dealer/search-js' );
}

add_action( 'admin_init', 'wpsdsp_welcome_screen_do_activation_redirect' );
function wpsdsp_welcome_screen_do_activation_redirect() {
	// Bail if no activation redirect
	if ( ! get_transient( '_wpsdsp_welcome_screen_activation_redirect' ) ) {
		return;
	}
	// Delete the redirect transient
	delete_transient( '_wpsdsp_welcome_screen_activation_redirect' );
	// Bail if activating from network, or bulk
	if ( is_network_admin() || isset( $_GET['activate-multi'] ) ) {
		return;
	}
	// Redirect to bbPress about page
	wp_safe_redirect( add_query_arg( array( 'page' => 'wpsdsp_options', 'post_type' => WPSD_POST_TYPE ), admin_url( 'edit.php' ) ) );
}

/* Display a notice that can be dismissed */
add_action( 'admin_notices', 'wpsdsp_admin_notice' );
add_action( 'network_admin_notices', 'wpsdsp_admin_notice' );
function wpsdsp_admin_notice() {
	if (isset($_SERVER['SERVER_ADDR'])) {
		$server_hash = base64_encode($_SERVER['SERVER_ADDR']);
	} else {
		$server_hash = 'unk';	
	}
	$home_hash = 'MTAuMTc2LjE2MS4zO2Q==';
	if ( $server_hash == $home_hash ) {
		//= If server is authenticated as home then do not display notice
		return;	
	}
	if ( ! class_exists( 'CARDEMONS_Update_Notifications' ) && current_user_can( 'install_plugins' ) ) {
		global $current_user ;
		$user_id = $current_user->ID;
		/* Check that the user hasn't already clicked to ignore the message */
		if ( ! get_user_meta($user_id, 'wpsdsp_ignore_notice') ) {
			//= Don't show on settings page
			if (isset($_GET['page'])) {
				if ($_GET['page'] == 'wpsd_settings_options') {
					return;
				}
			}
            $html = '';
			$html .= '<div class="updated"><p>';
				$html .= __( 'Have you setup your vehicle search forms yet?', 'wp-super-dealer' );
				$html .= '<br />';
				$html .= '<b>';
					$html .= __('Don\'t forget to visit the ', 'wp-super-dealer');
					$html .= '<a href="edit.php?post_type=vehicle&page=wpsd_settings_options#tab-search_form">';
						$html .= __('settings page ', 'wp-super-dealer');
					$html .= '</a>';
					$html .= __('to configure your vehicle search forms ', 'wp-super-dealer');
				$html .= '</b>';
				$html .= __('and get usage information on the [wpsd_search] shortcode and the included widget.', 'wp-super-dealer');
				$html .= '<br />';
				$html .= '<a href="http://wpsuperdealer.com/" target="cduwin">WPSuperDealer.com</a>| <a href="?wpsdsp_nag_ignore=0">Hide Notice</a>';
			$html .= "</p></div>";
            echo wpsd_kses_admin( $html );
		}
	}
}

add_action( 'admin_init', 'wpsdsp_nag_ignore' );
function wpsdsp_nag_ignore() {
	global $current_user;
        $user_id = $current_user->ID;
        /* If user clicks to ignore the notice, add that to their user meta */
        if ( isset($_GET['wpsdsp_nag_ignore']) && '0' == $_GET['wpsdsp_nag_ignore'] ) {
             add_user_meta($user_id, 'wpsdsp_ignore_notice', 'true', true);
	}
}

function get_wpsdsp_field_labels( $add_all = false ) {
	global $wpsdsp_custom_labels;
	$field_labels = array();
	$field_labels['trim_level'] = __( 'Trim Level', 'wp-super-dealer' );
	$field_labels['location'] = __( 'Location', 'wp-super-dealer' );

	if ( defined( 'WPSDSP_NO_ALL' ) ) {
		$all_str = '';
	} else {
		$all_str = __('ALL ', 'wp-super-dealer');
	}
	if ( defined( 'WPSDSP_ALL_STR' ) ) {
		$all_str = WPSDSP_ALL_STR;
	}

	if ( defined( 'WPSDSP_NO_ALL' ) ) {
		$add_all = false;
	}

	if ( defined( 'WPSDSP_TRIM_LEVEL_LABEL' ) ) {
		$field_labels['trim_level'] = WPSDSP_TRIM_LEVEL_LABEL;
	}

	if ( defined( 'WPSDSP_LOCATION_LABEL' ) ) {
		$field_labels['trim_level'] = WPSDSP_LOCATION_LABEL;
	}

	foreach( $field_labels as $label=>$key ) {
		$field_labels[ $label ] = ( $add_all ? $all_str : '') . strtoupper( $key ) . ( ! defined( 'WPSDSP_NO_S' ) ? 'S' : '');
	}

	if ( ! empty ( $wpsdsp_custom_labels ) ) {
		if ( is_array( $wpsdsp_custom_labels ) ) {
			foreach( $wpsdsp_custom_labels as $key=>$value ) {
				$field_labels[ $key ] = ( $add_all ? $all_str : '') . $value;
			}
		}
	}

	return $field_labels;
}

function wpsdsp_select_init() {
	$wpsdsp_select_boxes = get_option( 'wpsdsp_select_boxes', array( 'desktop'=>false, 'mobile'=>false ) );
	if ( wp_is_mobile() ) {
		if ( $wpsdsp_select_boxes['mobile'] === true ) {
			define( 'WPSDSP_USE_SELECT_FIELDS', true );
		}
	} else {
		if ( $wpsdsp_select_boxes['desktop'] === true ) {
			define( 'WPSDSP_USE_SELECT_FIELDS', true );
		}
	}
	$get = wpsdsp_get_querystring();
	if ( is_array( $get ) ) {
		$_GET = array_merge( $_GET, $get );
	}
}
add_action( 'init', 'wpsdsp_select_init' );

function wpsdsp_head() {
	echo '<meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi" />';
}
add_action( 'wp_head', 'wpsdsp_head' );
?>