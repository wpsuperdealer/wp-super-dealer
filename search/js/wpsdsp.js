// JavaScript Document
var json_url;
var json_data;
var form_fields;
var doNotUpdate = '1';
var first_load = true;
var wpsdsp_frmAction = 'click';
var wpsdsp_admin_log_on = true;
var wpsdspDebugLog = function( msg ) {
    if ( true === wpsdsp_admin_log_on ) {
        console.log( msg );
    }
};

if ( wpsdSearchParams.is_mobile ) {
	wpsdspDebugLog( 'Mobile Search Form' );
	wpsdsp_frmAction = wpsdSearchParams.wpsdsp_mobile_search_form_action; //= ie. tap, touchstart, touchend, click, etc.
	wpsdspDebugLog( 'Mobile button Action: ' + wpsdsp_frmAction );
} else {
	wpsdspDebugLog( 'NO Mobile Search Form' );	
}

jQuery(document).ready(function($) {
	"use strict";

	//= jQuery extension method: filterByText( textbox )
	$.fn.filterByText = function(textbox, field, element) {
	  return this.each(function() {
		var select = this;
		var options = [];
		$(select).find('option').each(function() {
		  options.push({
			value: $(this).val(),
			text: $(this).text()
		  });
		});

		$(select).data('options', options);
		$(textbox).bind('change keyup', function() {
		  var options = $(select).empty().data('options');
		  var search = $.trim($(this).val());
		  var regex = new RegExp(search, "gi");

		  $.each(options, function(i) {
			var option = options[i];
			if (option.text.match(regex) !== null) {
			  $(select).append(
				$('<option>').text(option.text).val(option.value)
			  );
			}
		  });
		});
	  });
	}; //= end jQuery extension method

	var keyPress = $.Event( "keypress", { which: 13 } );

	//= Mobile adjustments
	if ( $( '.wpsdsp-search-form' ).hasClass( 'wpsdsp-mobile-search-form' ) && ( false === $( '.wpsdsp-search-form' ).hasClass( 'wpsdsp-search-form-two' ) && false === $( '.wpsdsp-search-form' ).hasClass( 'wpsdsp_tb_no_mobile_button' ) ) ) {
		$( '.wpsdsp-mobile-filter-button' ).css( 'display', 'block' );
        $( '.wpsdsp-item-submit' ).css( 'display', 'none' );
	}

	var wpsdsp_decodeBase64 = function(s) {
		if ( 'undefined' === typeof( s ) ) {
			return false;
		}
		var e={},i,b=0,c,x,l=0,a,r='',w=String.fromCharCode,L=s.length;
		var A="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
		for(i=0;i<64;i++){e[A.charAt(i)]=i;}
		for(x=0;x<L;x++){
			c=e[s.charAt(x)];b=(b<<6)+c;l+=6;
			while(l>=8){((a=(b>>>(l-=8))&0xff)||(x<(L-2)))&&(r+=w(a));}
		}
		return r;
	};

	var wpsdsp_decode_setting = function( raw ) {
		var settings = wpsdsp_decodeBase64( raw );

		var regex_init = new RegExp( '\"', 'gi' );
		settings = settings.replace( regex_init, '"' );
		settings = JSON.parse( settings );

		return settings;
	};

	var wpsdsp_init = function() {
		wpsdspDebugLog( 'wpsdSearchParams ===>' );
        wpsdspDebugLog( wpsdSearchParams );

        //= Make sure the file is unique each time so it never goes in the cahce
		if ( wpsdSearchParams.save_cache === wpsdSearchParams.lang_strings.on ) {
			json_url = wpsdSearchParams.jsonurl+'?nocache=' + (new Date()).getTime();
		} else {
			json_url = wpsdSearchParams.jsonurl;
		}
		/* Form & Field settings
		var form_settings = $( '.wpsdsp-search-form' ).data( 'settings' );
		form_settings = wpsdsp_decode_setting( form_settings );		
		*/
		if ( $( '.wpsdsp-search-form' ).length > 0 ) {
            $.getJSON( json_url, function( data ) {
                json_data = data;
                wpsdsp_filter_form( '', '' );
            } );
			form_fields = $( '.wpsdsp-search-form' ).data( 'fields' );
			form_fields = wpsdsp_decode_setting( form_fields );
			wpsdspDebugLog( form_fields );
		} else {
            wpsdspDebugLog( 'No Vehicle Search Form Found' );
        }
	};

    if ( $( '.block-editor-page' ).length > 0 ) {
        setTimeout( function() {
            wpsdspDebugLog( 'Vehicle Search Form loaded for Block Editor' );
            wpsdsp_init();        
        }, 3000 );
    } else {
        wpsdsp_init();
    }
    
	$( document ).on( wpsdsp_frmAction, '.wpsdsp-mobile-filter-button', function( e ) {
        wpsdspDebugLog( 'Click mobile search form button' );
		wpsdspDebugLog( e );
		if ( 'none' === $( '.wpsdsp-search-form' ).css( 'display' ) ) {
	        wpsdspDebugLog( 'Open mobile search form' );
			$( '.wpsdsp-search-form' ).slideDown();
			$( '.wpsdsp-view-inventory-btn' ).css( 'display', 'block' );
		} else {
	        wpsdspDebugLog( 'Close mobile search form' );
			$( '.wpsdsp-search-form' ).slideUp();
			$( '.wpsdsp-view-inventory-btn' ).css( 'display', 'none' );			
		}
	} );

	var pages = $( '.wp-pagenavi span' ).length;
	if ( 2 >= pages ) {
		$( '.wp-pagenavi' ).css( 'display', 'none' );
	}

	$( document ).on( wpsdsp_frmAction, '.wpsdsp_show_inventory_btn, .wpsdsp-view-inventory-btn', function() {
		if ( $( '.keyword_search_criteria' ).length > 0 ) {
			var criteria = $( '.keyword_search_criteria' ).val();
			if ( '' !== criteria ) {
				wpsdsp_build_submit_form();
			}
		}
        $( '.wpsdsp-search-form' ).slideUp();
        $( '.wpsdsp-view-inventory-btn' ).css( 'display', 'none' );
        $( ':focus' ).blur();
		
		$( [ document.documentElement, document.body ] ).animate({
			scrollTop: $( '.wpsdsp-mobile-filter-button' ).offset().top
		}, 300);
	} );

	$( document ).on( wpsdsp_frmAction, '.wpsdsp-search', function() {
		var field = $( this ).data( 'field' );
		if ( 'keyword' === field ) {
			return;
		}

		var dropDown = $( '.search_' + field + '_options' );
		var length = $( '.search_' + field + '_options> option').length;
		var size = dropDown.attr( 'size' );
		var height = $( this ).css( 'height' );

		var e = jQuery.Event( 'keydown' );
		e.which = 38; // # Some key code value
		$( this ).trigger( e );

		if ( length > 5 ) {
			length = 5;
		}

		if ( size > 1 ) {
			dropDown.css( 'top', '0' );
			dropDown.css( 'z-index', 1 );
			dropDown.attr( 'size', 0 );
		} else {
			dropDown.css( 'top', height );
			dropDown.css( 'z-index', 3 );
			dropDown.attr( 'size', length );
		}

	} );

	var selected_wrap = function( field, value ) {
		var html = '';
		if ( '' === value ) {
			return '';
		}

		html = wpsdSearchParams.template_selected_html;
		var regex = new RegExp( '{field}', 'gi' );
		html = html.replace( regex, field );
		var regex = new RegExp( '{value}', 'gi' );
		html = html.replace( regex, value );
 
		return html;
	};

	$( document ).on( wpsdsp_frmAction, ' .selected-remove', function() {
		var field = $( this ).data( 'field' );
		var value = $( this ).next( '.selected_value' ).html();
		var containers = $( '.selected-wrap_' + field );
		var each_value;
		$.each( containers, function( i, e )  {
			each_value = $( e ).find( '.selected_value' ).html();
			if ( each_value === value ) {
				$( e ).remove();
			}
		} );

		var html = $( '.search_' + field + '_selected' ).html();
		if ( '' === html ) {
			$( '.wpsdsp-search-' + field ).val( '' );
		}

		wpsdsp_filter_form( '', '' );
	} );

	var wpsdsp_save_values = function( element ) {
		var field = $( element ).data( 'field' );
		var fields = $( '.wpsdsp-search-form' ).data( 'fields' );
		fields = wpsdsp_decode_setting( fields );

		if ( 'select' !== fields[ field ].type ) {
			return;
		}

		var value = $( element ).val();
		var selected = $( '.' + field + '_selected' );
		var html = selected.html();
		var new_html = selected_wrap( field, value );

		if ( -1 === html.indexOf( value ) ) {
			html = html + new_html;
			selected.html( html );
			wpsdsp_current_filters( field, new_html );
		}
	};

    var wpsdsp_tags_button = function( current_filters_html ) {
        var html = '';
        var label = $( '.wpsdsp-current-filters.wpsdsp-current-filters_bottom' ).data( 'tags-button' );
        if ( '' !== label ) {
            html += '<div class="wpsdsp-tags-button wpsdsp-button-apply wpsdsp-button">';
                html += '<i class="dashicons-before dashicons-search"></i>';
                html += label;
            html += '</div>';
        }
        if ( 0 < current_filters_html.indexOf( html ) ) {
            current_filters_html = current_filters_html.replace( html, '' );
        }
        return current_filters_html + html;
    };
    
	var wpsdsp_current_filters = function( field, new_html ) {
		var current_filters_html = $( '.wpsdsp-current-filters' ).html();
        if ( '' === current_filters_html ) {
            var tags_title = $( '.wpsdsp-current-filters.wpsdsp-current-filters_bottom' ).data( 'tags-title' );
            if ( '' !== tags_title ) {
                current_filters_html += '<div class="wpsdsp-tags-message">';
                    current_filters_html += tags_title;
                current_filters_html += '</div>';
            }
        }
		if ( -1 === current_filters_html.indexOf( new_html ) ) {
			current_filters_html += new_html;
            current_filters_html = wpsdsp_tags_button( current_filters_html );
			$( '.wpsdsp-current-filters' ).html( current_filters_html );
		}
	};

	var wpsdspOpenDropDown = function( element ) {
		var field = $( element ).data( 'field' );
		var input = $( '.wpsdsp-search-' + field );
		input.val( '' );
		$( element ).css( 'top', '0' );
		$( element ).css( 'z-index', 1 );
		$( element ).attr( 'size', 0 );
	};

	$( document ).on( wpsdsp_frmAction, '.selectOptions', function() {
		if ( false === $( this ).hasClass( 'range' ) &&
			false === $( this ).hasClass( 'checkbox' ) &&
			false === $( this ).hasClass( 'text' ) ) {
				$( this ).trigger( 'change' );
				update_drop_down = true;
				wpsdspOpenDropDown( this );
		}
	} );

	var update_drop_down = true;

	$( document ).on( 'change', '.selectOptions', function() {
		if ( update_drop_down ) {
			var is_checkbox = $( this ).hasClass( 'checkbox' );
			if ( is_checkbox ) {
				first_load = false;
			}

			var field = $( this ).data( 'field' );
			wpsdsp_save_values( this );
			wpsdsp_filter_form( field, this );
		}
	} );

	$( '.selectOptions' ).keydown( function( e ) {
		if ( false === $( this ).hasClass( 'range' ) &&
			false === $( this ).hasClass( 'checkbox' ) &&
			false === $( this ).hasClass( 'text' ) ) {
				var keycode = e.keyCode || e.which;
				if ( keycode === 13 ) {
					update_drop_down = true;
					wpsdsp_save_values( this );
					wpsdspOpenDropDown( this );
				} else if ( keycode === 38 ) {
					var index = $( this ).prop( 'selectedIndex' );
					if ( 0 === index ) {
						var field = $( this ).data( 'field' );
						var textBox = $( '.wpsdsp-search-' + field );
						textBox.focus();
					}
				} else if ( keycode === 40 ) {
					update_drop_down = false;
					return;
				}
		}
	} );

	$( '.wpsdsp-search' ).keydown( function( e ) {
		var topValue;
		var field = $( this ).data( 'field' );
		var keycode = e.keyCode || e.which;
		if ( 'keyword' === field ) {
			return;
		}

		//= check if enter( 13) or down arrow (40) was clicked
		if ( keycode === 13 || keycode === 40 ) {
			var selectBox = $( '.search_' + field + '_options' );
			var length = $( '.search_' + field + '_options> option').length;

			if ( '' === selectBox.val() ) {
				topValue = selectBox.find("option:first-child").val();
				selectBox.find("option:first-child").trigger( wpsdsp_frmAction );
			} else {
				topValue = selectBox.val();
			}

			if ( keycode === 13 ) {
				selectBox.attr( 'size', 0 );
				selectBox.css( 'top', '0' );
				selectBox.css( 'z-index', 1 );
				$( this ).val( topValue );
			} else if ( keycode === 40 ) {
				var height = $( this ).css( 'height' );
				selectBox.css( 'top', height );

				if ( length > 5 ) {
					length = 5;
				}

				selectBox.attr( 'size', length );
				selectBox.css( 'top', height );
				selectBox.css( 'z-index', 3 );
				selectBox.focus();
			} else {
				wpsdspOpenDropDown( selectBox );
			}
		}
	});

	$( document ).on( 'change', '.wpsdsp-range', function() {
		var field = $( this ).data( 'field' );
		var min_range, max_range, range_clicked;
		
		if ( $( this ).hasClass( 'wpsdsp_min' ) ) {
			range_clicked = 'min';
		} else if ( $( this ).hasClass( 'wpsdsp_max' ) ) {
			range_clicked = 'max';
		}

		min_range = $( '.wpsdsp-range.wpsdsp_min.wpsdsp_' + field );
		max_range = $( '.wpsdsp-range.wpsdsp_max.wpsdsp_' + field );

		var min_val = parseInt( min_range.val() );
		var max_val = parseInt( max_range.val() );

		min_range.find( 'option' ).css( 'display', 'block' );

		if ( 'max' === range_clicked ) {
			min_range.find( 'option' ).each( function( index, element ) {
				min_val = $( element ).val();
				min_val = parseInt( min_val );
				$( element ).css( 'display', 'block' );
				if ( min_val > max_val || min_val === max_val ) {
					$( element ).css( 'display', 'none' );
				}
			});
		} else {
			max_range.find( 'option' ).each( function( index, element ) {
			   max_val = $( element ).val();
			   max_val = parseInt( max_val );
			   $( element ).css( 'display', 'block' );
				if ( min_val > max_val || min_val === max_val ) {
					$( element ).css( 'display', 'none' );
				}
			});
		}
	} );

	$( document ).on( 'click touchend', '.wpsdsp-label .selectArrow', function( e ) {
		if ( wpsdSearchParams.is_mobile ) {
			$(this).prop( 'disabled', true );
			setTimeout( function( e ) {
			   $( this ).prop( 'disabled', false );
		   }, 500 );
		}
		var dont_expand = $( this ).parents( '.wpsdsp-search-form' ).hasClass( 'wpsdsp_no_expand' );
		if ( false === dont_expand ) {
			var field = $( this ).data( 'field' );
			var status = $( this ).data( 'status' );
			if ( 'open' === status ) {
				$( this ).html( '+' );
				$( '.search-' + field ).slideUp();
				$( '.search_' + field + '_selected' ).slideUp();
				$( this ).data( 'status', 'closed' );
			} else {
				$( this ).html( '-' );
				$( '.search-' + field ).slideDown();
				$( '.search_' + field + '_selected' ).slideDown();
				$( this ).data( 'status', 'open' );
			}
		}
	} );

	$( document ).on( wpsdsp_frmAction, '.wpsdsp-select-wrap', function() {
		var field = $( this ).data( 'field' );
		var input = $( '.wpsdsp-search-' + field );
		input.focus();
	} );

	var wpsdsp_build_submit_form = function() {
		var input;
		var form_data = wpsdsp_gather_form_data();
		var form_action = $( '.wpsdsp-search-form' ).data( 'action' );
		var form = $( document.createElement( 'form' ) );

		$( form ).attr( 'action', form_action );
		$( form ).attr( 'method', 'GET' );

		input = $( '<input>' ).attr( 'type', 'hidden' ).attr( 'name', 'vehicle' ).val( '1' );
		$( form ).append( $( input ) );

		$.each( form_data, function( index, element ) {
			if ( '' !== element && 'criteria' !== index ) {
				input = $( '<input>' ).attr( 'type', 'hidden' ).attr( 'name', index ).val( element );
				$( form ).append( $( input ) );
			}
		} );
		
		if ( $( '.keyword_search_criteria' ).length > 0 ) {
			var criteria = $( '.keyword_search_criteria' ).val();
			if ( '' !== criteria ) {
				input = $( '<input>' ).attr( 'type', 'hidden' ).attr( 'name', 'criteria' ).val( criteria );
				$( form ).append( $( input ) );
			}
		}

		if ( $( '.wpsd-sort-by-combined' ).length > 0 ) {
			var order_by = $( this ).find( ':selected' ).val();
			var order_by_dir = $( this ).find( ':selected' ).data( 'direction' );
			
			if ( '' !== order_by ) {
				if ( '' === order_by_dir ) {
					order_by_dir = 'desc';
				}
				if ( 'undefined' !== typeof( order_by ) ) {
					input = $( '<input>' ).attr( 'type', 'hidden' ).attr( 'name', 'order_by' ).val( order_by );
					$( form ).append( $( input ) );
				}
				if ( 'undefined' !== typeof( order_by_dir ) ) {
					input = $( '<input>' ).attr( 'type', 'hidden' ).attr( 'name', 'order_by_dir' ).val( order_by_dir );
					$( form ).append( $( input ) );
				}
			}
		}

        $( document.body ).append( form );
		$( form ).submit();
	};

	var wpsdsp_build_data_row = function( fields, field_name, row_data ) {
		var filtered_data = {};
		//= Field names in the json are stored as single letters to reduce file size
		var letter = fields[ field_name ].json_column;

		$.each( row_data, function( index, row ) {
			if ( 'undefined' === typeof( row ) ) {
				
			} else {
				if ( undefined === filtered_data[ row[ letter ] ] ) {
					filtered_data[ row[ letter ] ] = 1;
				} else {
					filtered_data[ row[ letter ] ] = ( filtered_data[ row[ letter ] ] + 1 );
				}
			}
		} );
		
		return filtered_data;
	};

	var wpsdsp_filter_data = function( row_data, field_name, field_value, fields ) {
		var data_filtered = {};
		var field_values_array;
		var matched = false;
		var cnt = 0;

		$.each( fields, function( name, value ) {
			if ( field_name === name ) {
				return cnt;
			}
			++cnt;
		} );

		//= Field names in the json are stored as single letters to reduce file size
		var letter = fields[ field_name ].json_column;
	
		// If there is no type selected, return all objects:
		if ( field_value === '' ) {
			return data_filtered;
		}

		//= if it has more than one value (ie , ) then parse it and run items in loop
		if ( field_name.indexOf( '|' ) ) {
			field_values_array = field_value.split( '|' );
			//= Start filtering the json_data

			cnt = 0;
			$.each( row_data , function( i, row ) {
				matched = false;

				$.each( field_values_array, function( index, value ) {
					if ( row[ letter ] === value ) {
						matched = true;
						return;
					} else {
						if ( matched === false ) {
							matched = false;
						}
					}
				} );
				
				if ( matched === true ) {
					data_filtered[ cnt ] = row;
					++cnt;
				}
			} );

		} else {
			//= Single value for this field can be easily filtered with filter()
			data_filtered = data_filtered.filter( function ( row ) {
			  return row[ letter ] === field_value;
			});
		}

		return data_filtered;
	}

	var wpsdsp_get_option_class = function( name ) {
		var regex = new RegExp( ' ', 'gi' );
		name = name.replace( regex, '_' );
		return name.replace(/[^\w\s]/gi, '');
	};
    
    var wpsd_slugify = function( str ) {
      return str.toLowerCase()
             .replace(/ /g, '-')
             .replace(/[^\w-]+/g, '');
    }

    var wpsd_is_true = function( str ) {
        if ( '' !== str
           && ( str !== wpsdSearchParams.lang_strings.no
            && str !== wpsdSearchParams.lang_strings.false ) ) {
            return true;
        }
        return false;
    }
    
	var wpsdsp_filter_form = function( this_field, field_element ) {
		var form_data = wpsdsp_gather_form_data();
		//var fields = wpsdSearchParams.fields;
		var fields = $( '.wpsdsp-search-form' ).data( 'fields' );
		fields = wpsdsp_decode_setting( fields );

		var json_data_filtered, field_value, template, row_template, field_html, field_values, num;
		var filtered_data = [];
		json_data_filtered = json_data;

		$.each( fields, function( field_name, value ) {
			//= Does this field get updated - return if it does not then return if needed
			if ( value.dynamic ) {
				field_value = form_data[ field_name ];

				//= If no value found then stop here and return
				if ( undefined === field_value ) {
					return;
				}
				if ( '' === field_value ) {
					return;
				}

				//= Start filtering the json_data
				json_data_filtered = wpsdsp_filter_data( json_data_filtered, field_name, field_value, fields );
			}

		} );

		//= Now that we've looped our fields and gotten the json_data_filtered we need to loop the fields again to build our final array
		$.each( fields, function( field_name, value ) {
			//= Does this field get updated?
			if ( value.dynamic ) {
				filtered_data[ field_name ] = wpsdsp_build_data_row( fields, field_name, json_data_filtered );
			}
		} );

		//= Create a variable to store the label value, which is made by cleaning up the name field
		var text;

		//= Create a variable to store the main element of the current field
		var containing_element;
		
		var checked, filter_options;
		
		var filter_checked = false;
		
		var option_class, text_regex, name_regex, field_values_count, checked_string;

        //= Keep the same value selected in a select box
        var simple_dropdowns = $( '.wpsdsp-search-form' ).data( 'simple-selects' );
        
        //= Create a variable to temporarily store the current fields value
        var current_value = '';
        
		//= filtered_data is now an object that contains all of the new field values based on the current form values.
        //= Loop the fields one last time and update their html by replacing their child elements
		$.each( fields, function( field_name, row_data ) {
            //= Get the current field value
            current_value = $( '.selectOptions.search_' + field_name + '_options' ).val();

            //= Do not update the current field
			var self_filter = false;
			if ( this_field === field_name ) {
                //= Are we keeping our select values?
                if ( true === wpsd_is_true( simple_dropdowns ) ) {
                    wpsdspDebugLog( 'Keep selected values' );
                    wpsdspDebugLog( current_value );
                    $( '.selected-wrap_' + this_field ).each( function( i, e ) {
                        var data_value = $( e ).data( 'value' );
                        wpsdspDebugLog( 'Removing tags since this select box is now blank' );
                        if ( current_value !== data_value ) {
                            $( '.selected-wrap_' + this_field ).remove();
                        }
                    });
                }

				wpsdspDebugLog( 'Maybe not filter this field: ' + field_name );
				if ( fields[ this_field ].self_filter ) {
					wpsdspDebugLog( 'Go ahead and filter: ' + this_field );
				} else {
					wpsdspDebugLog( 'Do not filter: ' + this_field );
					// if this_field is a checkbox and if at least 1 in that group is unchecked then don't return, go ahead and filter it
					if ( row_data.dynamic ) {
						if ( 'checkbox' === row_data.type ) {
							if ( this_field === row_data.search_field ) {
								if ( $( '.' + this_field + '.checkbox .selectOption input:checkbox:checked' ).length > 0 ) {
									return;
								} else {
									//= this was a checkbox and since it was unchecked we want to unfilter it
									self_filter = true;
								}
							}
						}
						if ( false === self_filter ) {
                            wpsdspDebugLog( 'Serious - do not filter: ' + this_field );
                            //= stop and return here
							return;
						}
					}
				}
			}

			checked = '';

			field_html = '';

            //= Does this field get updated - return if it does not
			if ( row_data.dynamic ) {

				field_values = filtered_data[ field_name ];
				if ( 'select' === row_data.type ) {
					template = wpsdSearchParams.template_select;
					field_values[' '] = '';
	
				} else if ( 'checkbox' === row_data.type ) {
					template = wpsdSearchParams.template_checkbox;
				}

				//= Put everything into alphabetical order
				Object.keys( field_values ).sort().forEach( function( key ) {
					var value = field_values[key];
					delete field_values[key];
					field_values[key] = value;
				});

				field_values_count = Object.keys( field_values ).length;
				$.each( field_values, function( name, value ) {
					//= Filter out _ and add count
					text = wpsdsp_replaceAll( name, '_', ' ' );
					if ( 'on' !== wpsdSearchParams.hide_count ) {
						text = text + ' (' + value  + ')';
					}

                    if ( undefined === form_data[ row_data.search_field ] ) {
						checked = '';
					} else {
						checked_string = form_data[ row_data.search_field ];
						checked_string = checked_string.replace( ',', ' ' );
						if ( checked_string.indexOf( name ) !== -1 || checked_string === name ) {
							checked = ' checked';
						} else {
							checked = '';
						}
					}

					//= name holds the current value of the new element, if empty do not add
					if ( '' === name ) {
						return;
					}

					if ( field_values_count < 4 ) {
						if ( 0 === name || '0' === name ) {
							text_regex = new RegExp( '0 ', 'gi' );
							text = text.replace( text_regex, 'No ' );
						} else if ( 1 === name || '1' === name ) {
							text_regex = new RegExp( '1 ', 'gi' );
							text = text.replace( text_regex, 'Yes ' );
						}
					}

					if ( null === name || 'null' === name ) {
						text_regex = new RegExp( 'null ', 'gi' );
						text = text.replace( text_regex, 'Unknown ' );
						name  = '';
						return;
					}
					
					if ( 'select' === row_data.type ) {
						text = text.trim();
						if ( '()' === text ) {
							text = '';
                            //= Add field as first option if CSS class is found
                            if ( 0 < $( '.wpsdsp-option-is-field-name' ).length ) {
                                text = row_data.label;
                            }
						}
						if ( ' ' === name ) {
							name = '';
						}
					}

					option_class = wpsdsp_get_option_class( name );
                    var slug = wpsd_slugify( name );
					row_template = wpsdsp_replaceAll( template, '{value}', name );
					row_template = wpsdsp_replaceAll( row_template, '{text}', text );
					row_template = wpsdsp_replaceAll( row_template, '{field}', field_name );
					row_template = wpsdsp_replaceAll( row_template, '{checked}', checked );
					row_template = wpsdsp_replaceAll( row_template, '{option_class}', option_class );
                    row_template = wpsdsp_replaceAll( row_template, '{slug}', slug );

					field_html = field_html + row_template;
				} );

                //= Remove the current values for this form field
				containing_element = $( '.selectOptions.search_' + field_name + '_options' );
				
				//= Insert our new fields html
				containing_element.html( field_html );

                //= If we keep the same value selected in a select box then we have to reset it back to its value
                if ( true === wpsd_is_true( simple_dropdowns ) ) {
                    $( '.selectOptions.search_' + field_name + '_options' ).val( current_value );
                }
                
				if ( filtered_data[ field_name ].length === 0 ) {
					wpsdspDebugLog( 'No Results:' + field_name );
				}

			}
		} );
	
	};

	//==========================================
	//= Submit Form
	//==========================================
	$( document ).on( wpsdsp_frmAction, '.wpsdsp-button-apply', function() {
		wpsdsp_build_submit_form();
	} );

	/**
	 * Gather up all the values of the search form and make them into an array of data
	 */
	var wpsdsp_gather_form_data = function() {
		var data = {};
		//var fields = wpsdSearchParams.fields;
		var fields = $( '.wpsdsp-search-form' ).data( 'fields' );
		fields = wpsdsp_decode_setting( fields );

		var field_name;
		var field_value;
		var field_data;
		var field_cnt;

		$.each( fields, function( index, element ) {
			field_name = element.search_field;
			field_data = '';
			field_cnt = 0;
			if ( 'text' === element.type ) {
				
				field_value = $( '.wpsdsp-search-' + index ).val();
				if ( '' === field_value ) {
					return true;
				}
				data[ field_name ] = field_value;
				
			} else if ( 'checkbox' === element.type ) {

				$( '.search_' + index + '_options input[type=checkbox]' ).each( function( index, element ) {
					if ( $( element ).prop( 'checked' ) ) {
						if ( 0 === field_cnt ) {
							field_value = $( element ).val();
							if ( '' === field_value ) {
								return true;
							}
							++field_cnt;
							field_data = field_value;
						} else {
							field_value = $( element ).val();
							if ( '' === field_value ) {
								return true;
							}
							field_data = field_data + '|' + field_value;
						}
					}
				});

				if ( '' === field_data ) {
					return true;
				}
				data[ field_name ] = field_data;
			} else if ( 'select' === element.type ) {
				$( '.' + element.search_field + '_selected .selected_value' ).each( function( index, element ) {
					if ( 0 === field_cnt ) {
						field_value = $( element ).html();
						if ( '' === field_value ) {
							return true;
						}
						++field_cnt;
						field_data = field_value;
					} else {
						field_value = $( element ).html();
						if ( '' === field_value ) {
							return true;
						}
						field_data = field_data + '|' + field_value;
					}

					if ( '' === field_data ) {
						return true;
					}
					data[ field_name ] = field_data;
				} );

			} else if ( 'range' === element.type ) {
				var wpsdsp_min, wpsdsp_max;

				wpsdsp_min = $( '.wpsdsp_min.wpsdsp_' + index + ' option:selected' ).attr('value');
				wpsdsp_max = $( '.wpsdsp_max.wpsdsp_' + index + ' option:selected' ).attr('value');
				if ( '' !== wpsdsp_min ) {
                    data[ element.search_field_min ] = wpsdsp_min;
				}
				
				if ( '' !== wpsdsp_max ) {
                    data[ element.search_field_max ] = wpsdsp_max;
				}
				
			} else {
				wpsdspDebugLog( 'Field not added to search form' );
				wpsdspDebugLog( element );
			}
			
		} );

		if ( true == wpsdSearchParams.wpsdsp_autoload ) {
			//= Auto load inventory
			wpsdspDebugLog( 'Autoload on search' );
			wpsdsp_load_inventory( data, '' );
		}

		$( '.selectOptions' ).each( function( index, element ) {
			var field = $( element ).data( 'field' );
			var input = $( '.wpsdsp-search-' + field );
			$( element ).filterByText( input, field, element );
		} );
		
		return data;
	};

	$( document ).on( wpsdsp_frmAction, '.selectOption input[type=checkbox]', function() {
		first_load = false;
	});

	var wpsdsp_load_inventory = function( form_data, form_action ) {
		if ( true === first_load ) {
			first_load = false;
			return;
		}

		var query_string = '?';
		if ( '' === form_action ) {
			form_action = $( '.wpsdsp-search-form' ).data( 'action' );
			$.each( form_data, function( index, element ) {
				if ( '' !== element && undefined !== element ) {
					query_string += index + '=' + element + '&';
				}
			} );
	
			if ( $( '.keyword_search_criteria' ).length > 0 ) {
				var criteria = $( '.keyword_search_criteria' ).val();
				if ( '' !== criteria ) {
					query_string += 'criteria=' + criteria + '&';
				}
			}

			if ( $( '.wpsd-sort-by-combined' ).length > 0 ) {
				var order_by = $( '.wpsd-sort-by-combined' ).find('option:selected').val();
				var order_by_dir = $( '.wpsd-sort-by-combined' ).find('option:selected').data( 'direction' );
				
				if ( '' !== order_by ) {
					if ( '' === order_by_dir ) {
						order_by_dir = 'desc';
					}
					if ( 'undefined' !== typeof( order_by ) ) {
						query_string += 'order_by=' + order_by + '&';
					}
					if ( 'undefined' !== typeof( order_by_dir ) ) {
						query_string += 'order_by_dir=' + order_by_dir + '&';
					}
				}
			}

			if ( '' !== query_string ) {
				wpsdsp_eraseCookie( 'wpsdsp-criteria' );
				wpsdsp_createCookie( 'wpsdsp-criteria', query_string, 60 );
			}

			form_action = form_action + query_string;
		}

		var inventory_element = $( '#wpsd-inventory-wrap' );
		if ( 0 === inventory_element.length ) {
			return;
		}

		if ( $( '.wpsdsp_loading_inventory' ).length < 1 ) {
			inventory_element.prepend( wpsdSearchParams.loading_inventory );
		}

		var height = $( '#wpsd-container-wrap' ).css( 'height' );
		$( '.wpsdsp_loading_inventory' ).css( 'height', height );

		var current_inventory = inventory_element.html();

		$.ajax( {
			type: 'POST',
			data: {},
			url: form_action,
			timeout: 15000,
			error: function( err ) {
				
			},
			dataType: "html",
			success: function( html ) {
				var new_inventory = $( html ).find( '#wpsd-inventory-wrap' );
				$( '#wpsd-inventory-wrap' ).html( new_inventory );

				if ( wpsdSearchParams.is_mobile ) {
					$( '.wpsdsp-mobile-filter-button' ).css( 'display', 'block' );
				}

			}
		} );
	};

	var isInViewport = function (elem) {
		var bounding = elem.getBoundingClientRect();
		return (
			bounding.top >= 0 &&
			bounding.left >= 0 &&
			bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
			bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
		);
	};
	
	var sT, sL;
	var scrollOff = false;
	var endOfForm = 0;
	var disableScroll = function () {
		if ( false === scrollOff ) {
			// Get the current page scroll position 
			sT = window.pageYOffset || document.documentElement.scrollTop; 
			sL = window.pageXOffset || document.documentElement.scrollLeft;
			scrollOff = true;
			if ( 0 === endOfForm ) {
				endOfForm = $( '.wpsdsp_end_form' ).offset().top;
			}
		}
		// if any scroll is attempted, set this to the previous value 
		window.onscroll = function() { 
			sT = sT - 4;
			window.scrollTo(sL, sT); 
		};
	} 

	var enableScroll = function () {
		scrollOff = false;
		window.onscroll = function() {}; 
	}

	$(window).scroll(function(){
		if ( 0 < $( '.wpsdsp-mobile-search-form_DISABLE' ).length ) {
			var status = $( '.wpsdsp-mobile-search-form' ).css( 'display' );
			if ( 'none' === status ) {
				return;
			}
			var element = $( '.wpsdsp_end_form' ).get(0);
			if ( isInViewport( element ) ) {
				disableScroll();
			} else{
				enableScroll();
			}
		}
	});
	
	//=================================================================================
	//= Remove searched by item
	//=================================================================================
	$( document ).on( wpsdsp_frmAction, '.wpsd-search-by-item-remove', function() {
		wpsdspDebugLog( 'remove vehicle search item' );
		var this_value;
		first_load = false;

        if ( true == wpsdSearchParams.wpsdsp_autoload ) {
			var value = $( this ).data( 'value' );
			var field = $( this ).data( 'field' );
			var fields = $( '.wpsdsp-search-form' ).data( 'fields' );
			fields = wpsdsp_decode_setting( fields );

			if ( undefined !== typeof( fields[ field ] ) ) {
				var search_form_remove_items = $( '.wpsdsp-current-filters .selected-wrap_' + field );
				if ( 0 < search_form_remove_items.length ) {
					$.each( search_form_remove_items, function( i, e ) {
						var item_value = $( e ).data( 'value' );
						if ( item_value === value ) {
							$( e ).remove();
							return;
						}
					});
				}
				//= handle checkboxes
				var checkbox_fields = $( '.search_' + field + '_options .selectOption input[type=checkbox]' );
				if ( undefined !== typeof( checkbox_fields ) ) {
					$.each( checkbox_fields, function( k, checkbox ) {
						if ( value == $( checkbox ).val() ) {
							$( checkbox ).trigger( 'click' );
							return;
						}
					} );
				}
				//= handle select boxes
				var selected = $( '.' + field + '_selected .selected-wrap' );
				if ( selected.length > 0 ) {
					$.each( selected, function( i, e ) {
						this_value = $( e ).data( 'value' );
						if ( value === this_value ) {
							$( e ).remove();
							wpsdsp_gather_form_data();
                            wpsdsp_filter_form( '', '' );
							return;
						}
					} );
				}
				//= handle range items
				var type = $( this ).data( 'type' );
				if ( 'max' === type || 'min' === type ) {
					$( '.wpsdsp-range.wpsdsp_' + type + '.wpsdsp_' + field ).val( '' );
					$( '.wpsdsp-range.wpsdsp_' + type + '.wpsdsp_' + field ).trigger( 'change' );
//					$( '.wpsdsp-range.wpsdsp_max.wpsdsp_' + field ).trigger( 'change' );
					return;
				}
				//= Handle keyword
				if ( 'keyword' === field ) {
					$( '.keyword_search_criteria' ).val( '' );
					wpsdsp_gather_form_data();
					return;
				}

				wpsdsp_gather_form_data();
				return;

			}
		} else {
			var new_url = $( this ).data( 'url' );
			window.location.href = new_url;
		}
	} );

	$( document ).on( wpsdsp_frmAction, '.view_inventory_button', function() {
		$( '.wpsdsp-button-apply' ).trigger( wpsdsp_frmAction );
	} );

	//=================================================================================
	//= Utility functions
	//=================================================================================

	var wpsdsp_escapeRegExp = function ( str ) {
		return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
	};

	var wpsdsp_replaceAll = function ( str, find, replace ) {
		return str.replace( new RegExp( wpsdsp_escapeRegExp( find ), 'g' ), replace );
	};

	var wpsdsp_urldecode = function( str ) {
		return decodeURIComponent((str+'').replace(/\+/g, '%20'));
	}; //= end var wpsdsp_urldecode

	var wpsdsp_createCookie = function( name, value, days ) {
		var expires;
		if ( days ) {
			var date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			expires = "; expires="+date.toGMTString();
		} else {
			expires = "";
		}
		document.cookie = name+"="+value+expires+"; path=/";
	}; //= end var wpsdsp_createCookie

	var wpsdsp_readCookie = function( name ) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)===' ') {
				c = c.substring(1,c.length);
			}
			if (c.indexOf(nameEQ) === 0) {
				return c.substring(nameEQ.length,c.length);
			}
		}
		return null;
	}; //= end var wpsdsp_readCookie
	
	var wpsdsp_eraseCookie = function( name ) {
		wpsdsp_createCookie(name,"",-1);
	}; //= end var wpsdsp_eraseCookie
	
	var wpsdsp_getQueryStrings = function() { 
		var assoc  = {};
		var decode = function (s) {
			var str = decodeURIComponent(s.replace(/\+/g, " "));
			str = decodeURIComponent(str.replace(/\-/g, " "));
			return str;
		};
	
		var queryString = location.search.substring(1) + '';
		if ( '' !== queryString ) {
			var keyValues = queryString.split('&'); 
			var i;
			for( i in keyValues ) { 
				var key = keyValues[i].split('=');
				if (key.length > 1) {
				  assoc[decode(key[0])] = decode(key[1]);
				}
			}
		}
		return assoc; 
	}; //= end var wpsdsp_getQueryStrings
	
	var remove_search = function( fld, val, query_string, form_action ) {
		query_string = query_string.replace( '+', ' ' );
		query_string = decodeURI(query_string);
		var remove_this = "&"+fld+"="+val;
		var reg = new RegExp(remove_this,"g");
		query_string = query_string.replace(reg, "");
		if (form_action !== '' && typeof(form_action) !== undefined) {
			window.location = form_action+'\\?'+query_string;
		} else {
			window.location = wpsdSearchParams.search_url+'\\?s=vehicles&'+query_string;
		}
	}; //= end var remove_search

	//===================================================================================
	//= Start Search Form Sliders
	//===================================================================================
	if ( 0 < $( '.search-form-sliders' ).length ) {
		wpsdspDebugLog( 'Search range sliders on' );
		var wpsd_make_range_slider = function( field ) {
			var range = {};
			range.field = field;
            range.label = field.replace( '_', ' ' );
            range.label = range.label.toLowerCase()
                .split(' ')
                .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
                .join(' ');
			range.min = $( '.wpsdsp_min.wpsdsp_' + field ).val();
			if ( '' === range.min ) {
				range.min = 0;
			}
			range.max = $( '.wpsdsp_max.wpsdsp_' + field + ' option:last-child' ).val();
			wpsdspDebugLog( 'Max: ' + range.max );
			range.step = 1000;
            if ( 'vehicle_year' === field ) {
                range.step = 1;
                range.min = 1980;
            }
			var before = '';
            var after = '';
			if ( 'price' === field ) {
				before = wpsdSearchParams.currency_before;
                after = wpsdSearchParams.currency_after;
			}
			range.before = before;
            range.after = after;
			wpsdspDebugLog( range );
			return range;
		};

		var range_fields = {};
		var ranges = {};
		var range_elements = $( '.wpsdsp-item.wpsdsp-range' );
		if ( 0 < range_elements.length ) {
			$.each( range_elements, function( i, e ) {
				wpsdspDebugLog( $(e) );
				var field = $( e ).find( '.wpsdsp-label' ).data( 'field' );
				range_fields[ field ] = i;
				ranges[ field ] = wpsd_make_range_slider( field );
			});
		}

		var slide_min, slide_max, default_min, default_max;

		$.each( ranges, function( i, e ) {
			$( '.wpsdsp_min.wpsdsp_' + e.field + ' option' ).remove();
			$( '.wpsdsp_max.wpsdsp_' + e.field + ' option' ).remove();
			$( '.wpsdsp_min.wpsdsp_' + e.field ).wrap( '<div class="wpsdsp-range-wrap"></div>' );
			$( '.wpsdsp_min.wpsdsp_' + e.field ).before( '<div class="wpsdsp-range-label">Min ' + e.label + '</div><div class="wpsdsp-range-min-value wpsdsp-range-' + e.field + '-min-value">' + e.before + e.min + '</div><div class="wpsdsp_' + e.field + '_slide_min"></div>' );
			$( '.wpsdsp_max.wpsdsp_' + e.field ).wrap( '<div class="wpsdsp-range-wrap"></div>' );
			$( '.wpsdsp_max.wpsdsp_' + e.field ).before( '<div class="wpsdsp-range-label">Max ' + e.label + '</div><div class="wpsdsp-range-max-value wpsdsp-range-' + e.field + '-max-value">' + e.before + e.max + '</div><div class="wpsdsp_' + e.field + '_slide_max"></div>' );

			if ( $( '.wpsdsp_' + e.field + '_slide_min' ).length > 0 ) {
				$( '.wpsdsp_' + e.field + '_slide_min' ).slider({
					min: e.min,
					max: e.max,
					step: e.step,
					range: "min",
					slide: function( event, ui ) {
						slide_min = ui.value;
						if ( false === ( $( '.wpsdsp_min.wpsdsp_' + e.field + ' option[value="' + slide_min + '"]' ).length > 0 ) ) {
							$( '.wpsdsp_min.wpsdsp_' + e.field + '' ).append('<option value="' + slide_min + '">' + e.before + slide_min + e.after + '</option>' );
						}
						$( '.wpsdsp_min.wpsdsp_' + e.field ).val( ui.value );
						$( '.wpsdsp-range-' + e.field + '-min-value' ).html( e.before + ui.value );
					},
					stop: function( event, ui ) {
						$( '.wpsdsp_max.wpsdsp_' + e.field ).trigger( 'change' );
						//= Get max
						//= Is the min above or equal to max?
							//= If yes then increase max by 5000
					}
				});
				$( '.wpsdsp_' + e.field + '_slide_min' ).wrap( '<div class="wpsdsp-range-slider-wrap"></div>' );

				$( '.wpsdsp_' + e.field + '_slide_max' ).slider({
					min: e.min,
					max: e.max,
					step: e.step,
					range: "max",
					value: e.max,
					slide: function( event, ui ) {
						slide_max = ui.value;
						if ( false === ( $( '.wpsdsp_max.wpsdsp_' + e.field + ' option[value="' + slide_max + '"]' ).length > 0 ) ) {
							$( '.wpsdsp_max.wpsdsp_' + e.field ).append('<option value="' + slide_max + '">' + e.before + slide_max + e.after + '</option>' );
						}
						$( '.wpsdsp_max.wpsdsp_' + e.field ).val( ui.value );
						$( '.wpsdsp-range-' + e.field + '-max-value' ).html( e.before + ui.value );
					},
					stop: function( event, ui ) {
						$( '.wpsdsp_max.wpsdsp_' + e.field ).trigger( 'change' );
					}
				});
				$( '.wpsdsp_' + e.field + '_slide_max' ).wrap( '<div class="wpsdsp-range-slider-wrap"></div>' );
			}
		});

	}
	//===================================================================================
	//= End Search Form Sliders
	//===================================================================================
	
} );