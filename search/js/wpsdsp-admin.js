// JavaScript Document
jQuery(document).ready(function($) {
	"use strict";

	$( '.wpsdsp_open_admin_field' ).on( 'click', function() {
		var field_slug = $( this ).data( 'field-slug' );
		var active = $( this ).html();
		if ( '+' === active ) {
			$( this ).html( '-' );
			$( '.custom_field_options_wrap.' + field_slug ).slideDown();
		} else {
			$( this ).html( '+' );
			$( '.custom_field_options_wrap.' + field_slug ).slideUp();
		}

	} );

	$( '.wpsdsp_activate_custom_field' ).on( 'click', function() {
		var field_slug = $( this ).data( 'field-slug' );
		var active = $( this ).prop( 'checked' );
		if ( active ) {
			$( '.custom_field_options_wrap.' + field_slug ).slideDown();
			$( '.wpsdsp_open_admin_field_' + field_slug ).html( '-' );
		} else {
			$( '.custom_field_options_wrap.' + field_slug ).slideUp();
			$( '.wpsdsp_open_admin_field_' + field_slug ).html( '+' );
		}
		
	} );

	$( '.wpsdsp_cf_select_type' ).on( 'change', function() {
		var value = $( this ).val();
		var field = $( this ).data( 'field' );
		var parent_field = $( this ).data( 'parent-field' );
		
		if ( 'select' === value ) {
			$( '.wpsdsp_custom_field_min.wpsdsp_custom_parent_field_' + parent_field ).slideUp();
			$( '.wpsdsp_custom_field_max.wpsdsp_custom_parent_field_' + parent_field ).slideUp();
			$( '.wpsdsp_custom_field_gap.wpsdsp_custom_parent_field_' + parent_field ).slideUp();
		} else if ( 'checkbox' === value ) {
			$( '.wpsdsp_custom_field_min.wpsdsp_custom_parent_field_' + parent_field ).slideUp();
			$( '.wpsdsp_custom_field_max.wpsdsp_custom_parent_field_' + parent_field ).slideUp();
			$( '.wpsdsp_custom_field_gap.wpsdsp_custom_parent_field_' + parent_field ).slideUp();
		} else if ( 'range' === value ) {			
			$( '.wpsdsp_custom_field_min.wpsdsp_custom_parent_field_' + parent_field ).slideDown();
			$( '.wpsdsp_custom_field_max.wpsdsp_custom_parent_field_' + parent_field ).slideDown();
			$( '.wpsdsp_custom_field_gap.wpsdsp_custom_parent_field_' + parent_field ).slideDown();
		}
		
	} );
	
	$( document ).on( 'click', '.wpsdsp_widget_field_settings_open_closed', function() {
		var status = $( this ).html();
		if ( '+' === status ) {
			$( '.wpsdsp_widget_fields_wrap' ).slideDown();
			$( this ).html( 'x' );
		} else {
			$( '.wpsdsp_widget_fields_wrap' ).slideUp();
			$( this ).html( '+' );
		}
		
	} );
	
	$( document ).on( 'click', '.wpsdsp_widget_fields_label', function() {
		var field = $( this ).data( 'field' );
		var status = $( '.wpsdsp_widget_field_open_closed_' + field ).html();
		if ( '+' === status ) {
			$( '.wpsdsp_widget_fields_' + field ).slideDown();
			$( '.wpsdsp_widget_field_open_closed_' + field ).html( 'x' );
		} else {
			$( '.wpsdsp_widget_fields_' + field ).slideUp();
			$( '.wpsdsp_widget_field_open_closed_' + field ).html( '+' );
		}
		
	} );

	$( document ).on( 'click', '.wpsdsp_advanced', function() {
		var field = $( this ).data( 'field' );
		var status = $( '.wpsdsp_custom_parent_field_' + field + '.wpsdsp_hidden' ).css( 'display' );
		console.log( field );
		console.log( status );
		if ( 'none' === status ) {
			$( '.wpsdsp_custom_parent_field_' + field + '.wpsdsp_hidden' ).css( 'display', 'block' );
		} else {
			$( '.wpsdsp_custom_parent_field_' + field + '.wpsdsp_hidden' ).css( 'display', 'none' );
		}
	});
} );