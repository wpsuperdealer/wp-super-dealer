<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//$vehicle_report = apply_filters( 'wpsd_import_report_hook', $vehicle_report, $cdii_settings );
add_filter( 'wpsd_import_report_hook', 'wpsdsp_import_filter', 10, 2 );
function wpsdsp_import_filter( $vehicle_report, $cdii_settings ) {
	wpsdsp_schedule();
	$vehicle_report .= '<br />';
	$vehicle_report .= __( 'Vehicle Search Cache was regenerated.', 'wp-super-dealer' );
	$vehicle_report .= '<br />';
    $vehicle_report = wp_kses_post( $vehicle_report );
	return $vehicle_report;
}
?>