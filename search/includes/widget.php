<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action( 'widgets_init', 'wpsdcp_wpsdsp_widgets' );
function wpsdcp_wpsdsp_widgets() {
	register_widget( 'cdp_wpsdsp_Widget' );
}

class cdp_wpsdsp_Widget extends WP_Widget {
	/**
	 * Widget setup.
	 */
	function __construct() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'wpsd_cds', 'description' => __( 'Vehicle Search widget for the WPSuperDealer PlugIn.', 'wp-super-dealer' ) );
		/* Widget control settings. */
		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'wpsd_cds-widget' );
		/* Create the widget. */
		parent::__construct( 'wpsd_cds-widget', __( 'WPSuperDealer Search', 'wp-super-dealer' ), $widget_ops, $control_ops );
	}
	/**
	 * How to display the widget on the screen.
	 */
	function widget( $args, $instance ) {

		/* Our variables from the widget settings. */
		$title = apply_filters( 'widget_title', $instance['title'] );

		/* Before widget (defined by themes). */
        if ( isset( $args['before_widget'] ) ) {
            echo wp_kses_post( $args['before_widget'] );
        }
		/* Display the widget title if one was input (before and after defined by themes). */
		if ( ! empty( $title ) ) {
            if ( ! isset( $args['before_title'] ) ) {
                $args['before_title'] = '';
            }
            if ( ! isset( $args['after_title'] ) ) {
                $args['after_title'] = '';
            }            
			$title = $args['before_title'] . $title . $args['after_title'];
		}
		echo wp_kses_post( $title );
		//= Insert Search Form Here ==========================

		// setup the setting to send to wpsdsp_get_search_form()
		$settings = array();
		$settings['hide'] = array();
		$settings['value'] = array();
		$settings['custom_class'] = $instance['custom_class'];
		$settings['form_action'] = $instance['form_action'];
		$settings['style'] = $instance['style'];
		$settings['field_map'] = $instance['field_map'];
        $settings['show_sort'] = $instance['show_sort'];
        $settings['tags_title'] = $instance['tags_title'];
        $settings['tags_button'] = $instance['tags_button'];
        $settings['option_is_field_name'] = $instance['option_is_field_name'];
        $settings['simple_selects'] = $instance['simple_selects'];
		$settings['submit_label'] = __( 'Search', 'wp-super-dealer' );

        //= function escapes html before returning
        $search_form = wpsdsp_get_search_form( $instance );
        //= let's escape again to be sure
        $search_form = wpsd_kses_search_form( $search_form );
        echo $search_form;
		/* After widget (defined by themes). */
        if ( isset( $args['after_widget'] ) ) {
            echo wp_kses_post( $args['after_widget'] );
        }
	}
	/**
	 * Update the widget settings.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		/* Strip tags for title and name to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['custom_class'] = strip_tags( $new_instance['custom_class'] );
		if ( empty( $new_instance['form_action'] ) ) {
			$new_instance['form_action'] = get_option( 'siteurl' );
		}
		$instance['form_action'] = strip_tags( $new_instance['form_action'] );
		$instance['style'] = strip_tags( $new_instance['style'] );
        $instance['show_sort'] = strip_tags( $new_instance['show_sort'] );
		$instance['field_map'] = strip_tags( $new_instance['field_map'] );
        $instance['tags_title'] = strip_tags( $new_instance['tags_title'] );
        $instance['tags_button'] = strip_tags( $new_instance['tags_button'] );
        $instance['option_is_field_name'] = strip_tags( $new_instance['option_is_field_name'] );
        $instance['simple_selects'] = strip_tags( $new_instance['simple_selects'] );
		$instance['submit_label'] = strip_tags( $new_instance['submit_label'] );

		$fields = wpsdsp_active_fields();
		foreach( $fields as $field=>$options ) {
			if ( isset( $new_instance[ $field ] ) ) {
				$instance[ $field ] = $new_instance[ $field ];
			} else {
				$instance[ $field ] = $options;
			}
		}

		return $instance;
	}
	/**
	 * Displays the widget settings controls on the widget panel.
	 * Make use of the get_field_id() and get_field_name() function * when creating your form elements. This handles the confusing stuff.
	 */
	function form( $instance ) {
		global $wpsd_options;
		$search_url = get_option( 'site_url' );
		if ( isset( $wpsd_options['inventory_page'] ) ) {
			$search_url = $wpsd_options['inventory_page'];
		}
		
		/* Set up some default widget settings. */
		$defaults = array( 
			'title' => __( 'Search', 'wp-super-dealer' ),
			'custom_class' => '',
			'form_action' => esc_url( $search_url ),
            'submit_label' => __( 'Submit', 'wp-super-dealer' ),
			'style' => '',
            'show_sort' => '',
			'field_map' => '',
            'tags_title' => __( 'Show me these vehicles:', 'wp-super-dealer' ),
            'tags_button' => __( 'Find Now', 'wp-super-dealer' ),
            'option_is_field_name' => false,
            'simple_selects' => __( 'Yes', 'wp-super-dealer' ),
		);

		$instance = wp_parse_args( (array) $instance, $defaults );

        $html = '';
		$html .= '<div class="wpsdsp_widget_admin">';
            $html .= '<p>';
                $html .= '<label for="' . esc_attr( $this->get_field_id( 'title' ) ) . '">' . __( 'Title:', 'wp-super-dealer' ) . '</label>';
                $html .= '<input id="' . esc_attr( $this->get_field_id( 'title' ) ) . '" name="' . $this->get_field_name( 'title' ) . '" value="' . esc_attr( $instance['title'] ) . '" />';
            $html .= '</p>';

            $html .= '<hr />';

            $html .= '<p>';
                $html .= '<label for="' . esc_attr( $this->get_field_id( 'style' ) ) . '">' . __( 'Style:', 'wp-super-dealer' ) . '</label>';
                $html .= '<select id="' . esc_attr( $this->get_field_id( 'style' ) ) . '" name="' . esc_attr( $this->get_field_name( 'style' ) ) . '">';
                    $html .= '<option value="' . __( 'one', 'wp-super-dealer' ). '"' . ( __( 'one', 'wp-super-dealer' )  === $instance['style'] ? ' selected' : '' ) . '>' . __( 'One', 'wp-super-dealer' ) . '</option>';
                    $html .= '<option value="' . __( 'two', 'wp-super-dealer' ). '"' . ( __( 'two', 'wp-super-dealer' )  === $instance['style'] ? ' selected' : '' ) . '>' . __( 'Two', 'wp-super-dealer' ) . '</option>';
                    $html .= '<option value="' . __( 'three', 'wp-super-dealer' ). '"' . ( __( 'three', 'wp-super-dealer' )  === $instance['style'] ? ' selected' : '' ) . '>' . __( 'Three', 'wp-super-dealer' ) . '</option>';
                    $html .= '<option value="' . __( 'four', 'wp-super-dealer' ). '"' . ( __( 'four', 'wp-super-dealer' )  === $instance['style'] ? ' selected' : '' ) . '>' . __( 'Four', 'wp-super-dealer' ) . '</option>';
                $html .= '</select>';
                $html .= '<br />';
                $html .= '<small>' . __( 'Simple select fields only allow you to select a single value from the dropdown.', 'wp-super-dealer' ) . '</small>';
            $html .= '</p>';
            
            $html .= '<p>';
                $html .= '<label for="' . esc_attr( $this->get_field_id( 'custom_class' ) ) . '">' . __( 'Custom CSS Class:', 'wp-super-dealer' ) . '</label>';
                $html .= '<input id="' . esc_attr( $this->get_field_id( 'custom_class' ) ) . '" name="' . esc_attr( $this->get_field_name( 'custom_class' ) ) . '" value="' . esc_attr( $instance['custom_class'] ) . '" />';
            $html .= '</p>';
            $html .= '<hr />';
            
            $html .= '<p>';
                $html .= '<label for="' . esc_attr( $this->get_field_id( 'form_action' ) ) . '">' . __( 'Inventory Page URL:', 'wp-super-dealer' ) . '</label>';
                $html .= '<input id="' . esc_attr( $this->get_field_id( 'form_action' ) ) . '" name="' . esc_attr( $this->get_field_name( 'form_action' ) ) . '" value="' . esc_attr( $instance['form_action'] ) . '" />';
            $html .= '</p>';
			$html .= '<hr />';

            $html .= '<p>';
                $html .= '<label for="' . esc_attr( $this->get_field_id( 'option_is_field_name' ) ) . '">' . __( 'First select option is field name:', 'wp-super-dealer' ) . '</label>';
                $html .= '<select id="' . esc_attr( $this->get_field_id( 'option_is_field_name' ) ) . '" name="' . esc_attr( $this->get_field_name( 'option_is_field_name' ) ) . '">';
                    $html .= '<option value="">' . __( 'No', 'wp-super-dealer' ) . '</option>';
                    $html .= '<option value="' . __( 'Yes', 'wp-super-dealer' ) . '"' . ( __( 'Yes', 'wp-super-dealer' )  === $instance['option_is_field_name'] ? ' selected' : '' ) . '>' . __( 'Yes', 'wp-super-dealer' ) . '</option>';
                $html .= '</select>';
                $html .= '<br />';
                $html .= '<small>' . __( 'Make the first option in all dropdowns the field name with no value.', 'wp-super-dealer' ) . '</small>';
            $html .= '</p>';
            $html .= '<hr />';
            
            $html .= '<p>';
                $html .= '<label for="' . esc_attr( $this->get_field_id( 'tags_title' ) ) . '">' . __( 'Tags Title:', 'wp-super-dealer' ) . '</label>';
                $html .= '<input id="' . esc_attr( $this->get_field_id( 'tags_title' ) ) . '" name="' . esc_attr( $this->get_field_name( 'tags_title' ) ) . '" value="' . esc_html( $instance['tags_title'] ) . '" placeholder="" />';
            $html .= '</p>';

            $html .= '<p>';
                $html .= '<label for="' . esc_attr( $this->get_field_id( 'tags_button' ) ) . '">' . __( 'Tags Button:', 'wp-super-dealer' ) . '</label>';
                $html .= '<input id="' . esc_attr( $this->get_field_id( 'tags_button' ) ) . '" name="' . esc_attr( $this->get_field_name( 'tags_button' ) ) . '" value="' . esc_html( $instance['tags_button'] ) . '" placeholder="" />';
            $html .= '</p>';
            $html .= '<hr />';

            $html .= '<p>';
                $html .= '<label for="' . esc_attr( $this->get_field_id( 'show_sort' ) ) . '">' . __( 'Show Sort:', 'wp-super-dealer' ) . '</label>';
                $html .= '<select id="' . esc_attr( $this->get_field_id( 'show_sort' ) ) . '" name="' . esc_attr( $this->get_field_name( 'show_sort' ) ) . '">';
                    $html .= '<option value="">' . __( 'No', 'wp-super-dealer' ) . '</option>';
                    $html .= '<option value="' . __( 'Yes', 'wp-super-dealer' ) . '"' . ( __( 'Yes', 'wp-super-dealer' )  === $instance['show_sort'] ? ' selected' : '' ) . '>' . __( 'Yes', 'wp-super-dealer' ) . '</option>';
                $html .= '</select>';
                $html .= '<br />';
                $html .= '<small>' . __( 'Simple select fields only allow you to select a single value from the dropdown.', 'wp-super-dealer' ) . '</small>';
            $html .= '</p>';

            $html .= '<hr />';

            $html .= '<p>';
                $html .= '<label for="' . esc_attr( $this->get_field_id( 'simple_selects' ) ) . '">' . __( 'Use simple select fields:', 'wp-super-dealer' ) . '</label>';
                $html .= '<select id="' . esc_attr( $this->get_field_id( 'simple_selects' ) ) . '" name="' . esc_attr( $this->get_field_name( 'simple_selects' ) ) . '">';
                    $html .= '<option value="">' . __( 'No', 'wp-super-dealer' ) . '</option>';
                    $html .= '<option value="' . __( 'Yes', 'wp-super-dealer' ) . '"' . ( __( 'Yes', 'wp-super-dealer' )  === esc_attr( $instance['simple_selects'] ) ? ' selected' : '' ) . '>' . __( 'Yes', 'wp-super-dealer' ) . '</option>';
                $html .= '</select>';
                $html .= '<br />';
                $html .= '<small>' . __( 'Simple select fields only allow you to select a single value from the dropdown.', 'wp-super-dealer' ) . '</small>';
            $html .= '</p>';
            $html .= '<hr />';
            
            $html .= '<p>';
                $html .= '<label for="' . esc_attr( $this->get_field_id( 'submit_label' ) ) . '">' . __( 'Submit Button:', 'wp-super-dealer' ) . '</label>';
                $html .= '<input id="' . esc_attr( $this->get_field_id( 'submit_label' ) ) . '" name="' . esc_attr( $this->get_field_name( 'submit_label' ) ) . '" value="' . esc_attr( $instance['submit_label'] ) . '" placeholder="" />';
            $html .= '</p>';
            $html .= '<hr />';

            $html .= '<p>';
                $html .= '<label for="' . esc_attr( $this->get_field_id( 'field_map' ) ) . '">' . __( 'Field Position:', 'wp-super-dealer') . '</label>';
                $html .= '<input id="' . esc_attr( $this->get_field_id( 'field_map' ) ) . '" name="' . esc_attr( $this->get_field_name( 'field_map' ) ) . '" value="' . esc_attr( $instance['field_map'] ) . '" placeholder="location|condition|year|make|model|body|year_range|price|mileage" />';
            $html .= '</p>';
            $html .= '<hr />';

			$fields = wpsdsp_active_fields();
			$field_options = array(
				'label' => array(
					'label' => __( 'Label', 'wp-super-dealer' ),
					'type' => 'input',
					'tip' => __( 'Customize the label for this field.', 'wp-super-dealer' ),
				),
				'type' => array(
					'label' => __( 'Type', 'wp-super-dealer' ),
					'type' => 'select',
					'tip' => __( 'Select the type of input element you want this field to be.', 'wp-super-dealer' ),
				),
				'value' => array(
					'label' => __( 'Default Value', 'wp-super-dealer' ),
					'type' => 'input',
					'tip' => __( 'Enter an optional default value.', 'wp-super-dealer' ),
				),
				'closed' => array(
					'label' => __( 'Closed', 'wp-super-dealer' ),
					'type' => 'checkbox',
					'text' => __( 'Should this field be closed when loaded?', 'wp-super-dealer' ),
					'tip' => __( 'Fields can be open or closed when the form loads.', 'wp-super-dealer' ),
				),
				'hide' => array(
					'label' => __( 'Hide', 'wp-super-dealer' ),
					'type' => 'checkbox',
					'text' => __( 'Should this field be hidden?', 'wp-super-dealer' ),
					'tip' => __( 'If you enter a default value and then hide this field it will force the form to always be filtered by that value.', 'wp-super-dealer' ),
				),
				'remove' => array(
					'label' => __( 'Remove', 'wp-super-dealer' ),
					'type' => 'checkbox',
					'text' => __( 'Do you want to remove this field from this form?', 'wp-super-dealer' ),
					'tip' => __( 'You should remove any fields you do not want to use.', 'wp-super-dealer' ),
				),
			);

			$html .= '<p>';
				$html .= '<span class="wpsdsp_widget_field_settings">';
					$html .= '<label class="wpsdsp_widget_field_settings_label">';
						$html .= __( 'Field Settings', 'wp-super-dealer' );
					$html .= '</label>';
					$html .= '<span class="wpsdsp_widget_field_settings_open_closed">';
						$html .= '+';
					$html .= '</span>';
				$html .= '</span>';

				foreach( $fields as $field=>$options ) {
					$html .= $this->wpsdsp_widget_field_settings( esc_html( $field ), $options, $field_options, $instance );
				}
			$html .= '</p>';
        $html .= '</div>';

        echo wpsd_kses_admin( $html );
    }

	function wpsdsp_widget_field_settings( $field, $options, $field_options, $instance ) {
		$x = '';
	
		$x .= '<div class="wpsdsp_widget_fields_wrap">';
			$x .= '<label class="wpsdsp_widget_fields_label" data-field="' . esc_attr( $field ) . '">';
				$label = $field;
				$label = str_replace( '_', ' ', $label );
				$x .= esc_html( ucwords( $label ) );
				$x .= '<span class="wpsdsp_widget_field_open_closed wpsdsp_widget_field_open_closed_' . esc_attr( $field ) . '">';
					$x .= '+';
				$x .= '</span>';
			$x .= '</label>';

			$x .= '<div class="wpsdsp_widget_fields wpsdsp_widget_fields_' . esc_attr( $field ) . '">';
				
				foreach( $field_options as $field_option=>$data ) {
					$x .= '<div class="wpsdsp_widget_field">';
						$x .= '<label class="wpsdsp_widget_field_label">';
							$x .= '<span class="wpsdsp_widget_field_tip_wrap">';
								$x .= '<span class="wpsdsp_widget_field_tip_icon">';
									$x .= '?';
								$x .= '</span>';
								$x .= '<span class="wpsdsp_widget_field_tip">';
									$x .= esc_html( $data['tip'] );
								$x .= '</span>';
							$x .= '</span>';
	
							$x .= '<span class="wpsdsp_widget_field_label_text">';
								$x .= esc_html( $data['label'] );
							$x .= '</span>';
						$x .= '</label>';
						$x .= '<span class="wpsdsp_widget_field_input">';
							$html = '';

							if ( isset( $options[ $field_option ] ) ) {
								if ( empty( $instance[ $field ][ $field_option ] ) && ! empty( $options[ $field_option ] ) ) {
									$instance[ $field ][ $field_option ] = $options[ $field_option ];
								}
							}
							
							if ( 'input' === $data['type'] ) {
								if ( ! isset( $instance[ $field ][ $field_option ] ) ) {
									$instance[ $field ][ $field_option ] = '';
								}
								$html = '<input type="text" id="' . esc_attr( $this->get_field_id( $field . '[' . $field_option . ']' ) ) . '" name="' . esc_attr( $this->get_field_name( $field . '[' . $field_option . ']' ) ) . '" value="' . esc_attr( $instance[ $field ][ $field_option ] ) . '" />';
							} else if ( 'select' === $data['type'] ) {
								//=  value="' . $instance[ $field ][ $field_option ] . '"
								$html = '<select id="' . esc_attr( $this->get_field_id( $field . '[' . $field_option . ']' ) ) . '" name="' . esc_attr( $this->get_field_name( $field . '[' . $field_option . ']' ) ) . '">';
									if ( 'keyword' === $field ) {
										$html .= '<option value="text">' . __( 'Input Field', 'wp-super-dealer' ) . '</option>';
									} else {
										$html .= '<option value="select"' . ( 'select' === $instance[ $field ][ $field_option ] ? ' selected' : '' ) . '>' . __( 'Select Field', 'wp-super-dealer' ) . '</option>';
										$html .= '<option value="checkbox"' . ( 'checkbox' === $instance[ $field ][ $field_option ] ? ' selected' : '' ) . '>' . __( 'Checkbox Field', 'wp-super-dealer' ) . '</option>';
										$html .= '<option value="range"' . ( 'range' === $instance[ $field ][ $field_option ] ? ' selected' : '' ) . '>' . __( 'Range Field', 'wp-super-dealer' ) . '</option>';
									}
								$html .= '</select>';
							} else if ( 'checkbox' === $data['type'] ) {
								$checked = '';
								if ( isset( $instance[ $field ] ) && isset( $instance[ $field ][ $field_option ] ) ) {
									if ( 'checked' === $instance[ $field ][ $field_option ] ) {
										$checked = ' checked';
									}
								}
								$html = '<input type="checkbox" id="' . esc_attr( $this->get_field_id( $field . '[' . $field_option . ']' ) ) . '" name="' . $this->get_field_name( $field . '[' . $field_option . ']' ) . '" value="checked"' . $checked . ' />';
								$html .= '<span class="wpsdsp_widget_checkbox_text">';
									$html .= esc_html( $data['text'] );
								$html .= '</span>';
							}

							$x .= $html;
						$x .= '</span>';
					$x .= '</div>';
				}
				
			$x .= '</div>';
		$x .= '</div>';
	
		return $x;	
	}
}
?>