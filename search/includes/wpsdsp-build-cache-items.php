<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsdsp_build_cache_item( $field, $value, $return = false ) {
	global $wpdb;
	$prefix = $wpdb->prefix;
	$wpsdsp_hide_count = get_option( 'wpsdsp_hide_count', 'off' );
	$field_labels = get_wpsdsp_field_labels();
	$fields = wpsdsp_active_fields();

	if ( defined( 'WPSDSP_NO_ALL' ) ) {
		$all_str = '';
	} else {
		$all_str = __( 'ALL ', 'wp-super-dealer' );
	}
	if ( defined( 'WPSDSP_ALL_STR' ) ) {
		$all_str = WPSDSP_ALL_STR;
	}

	if ( isset( $fields[ $field ] ) ) {
		
		$sql_field = $fields[ $field ]['field_name'];
		
		if ( 'taxonomy' === $fields[ $field ]['field_type'] ) {

			$value = esc_sql( $value );
			if ( ! empty( $value ) ) {
				$tax_srch = '((' . $prefix . 'terms.name)="'. $value .'") AND ((' . $prefix . 'term_taxonomy.taxonomy)="' . $sql_field . '") AND ';
			} else {
				$tax_srch = '';
			}
			$sql = 'SELECT DISTINCT ' . $prefix . 'terms_1.name AS new_name
				FROM ' . $prefix . 'postmeta RIGHT JOIN (((' . $prefix . 'term_relationships AS ' . $prefix . 'term_relationships_1 
				RIGHT JOIN (' . $prefix . 'term_relationships 
				LEFT JOIN (' . $prefix . 'terms RIGHT JOIN ' . $prefix . 'term_taxonomy ON ' . $prefix . 'terms.term_id = ' . $prefix . 'term_taxonomy.term_id) ON ' . $prefix . 'term_relationships.term_taxonomy_id = ' . $prefix . 'term_taxonomy.term_taxonomy_id) ON ' . $prefix . 'term_relationships_1.object_id = ' . $prefix . 'term_relationships.object_id) 
				LEFT JOIN ' . $prefix . 'term_taxonomy AS ' . $prefix . 'term_taxonomy_1 ON ' . $prefix . 'term_relationships_1.term_taxonomy_id = ' . $prefix . 'term_taxonomy_1.term_taxonomy_id) LEFT JOIN ' . $prefix . 'terms AS ' . $prefix . 'terms_1 ON ' . $prefix . 'term_taxonomy_1.term_id = ' . $prefix . 'terms_1.term_id) ON ' . $prefix . 'postmeta.post_id = ' . $prefix . 'term_relationships_1.object_id
				WHERE ('. $tax_srch .' 
					((' . $prefix . 'term_taxonomy_1.taxonomy)="' . $sql_field . '") 
					AND ((' . $prefix . 'postmeta.meta_key)="sold") 
					AND ((' . $prefix . 'postmeta.meta_value)="' . __( 'no', 'wp-super-dealer' ) . '")
				)
				ORDER BY new_name';

			$results = $wpdb->get_results( $sql );
            
			$cnt = 0;
			$label = $all_str . $field_labels['year'];
			//$array[] = array( '' => $label );
			$array[] = array( '' => '' );
			if ( ! empty( $results ) ) {
				foreach ( $results as $result ) {
					$cnt = wpsdsp_count_active_tax_items( $value, $result->new_name, $sql_field );
					$sel_val = get_term_by( 'name', $result->new_name, $sql_field );
					$slug = $sel_val->slug;
                    if ( $result->new_name !== '-' ) {
						if ( $wpsdsp_hide_count !== 'on' ) {
							$array[] = array( $slug => $result->new_name . ' (' . $cnt . ')' );
						} else {
							$array[] = array( $slug => $result->new_name );
						}
					} else {
						if ( ! empty( $year_srch ) ) {
							$array[] = array( $slug => 'Vintage' );
						}
					}
				}
			} else {
				$array[] = array('0' => 'No Match');
			}

			//= end if ( 'taxonomy' === $fields[ $field ]['field_type'] )
		} else if ( 'meta' === $fields[ $field ]['field_type'] ) {

			$value = esc_sql( $value );
			if ( ! empty( $value ) ) {
				$meta_srch = '((' . $prefix . 'postmeta_1.meta_value)="'. $value .'")) AND ';
			} else {
				$meta_srch = '';
			}
			$sql = 'SELECT DISTINCT ' . $prefix . 'postmeta_1.meta_value AS new_name
				FROM ' . $prefix . 'postmeta AS ' . $prefix . 'postmeta_1 
				RIGHT JOIN ' . $prefix . 'postmeta ON ' . $prefix . 'postmeta_1.post_id = ' . $prefix . 'postmeta.post_id
				WHERE ('. $meta_srch .' 
					((' . $prefix . 'postmeta.meta_key)="sold") 
					AND ((' . $prefix . 'postmeta.meta_value)="' . __( 'no', 'wp-super-dealer' ) . '") 
					AND ((' . $prefix . 'postmeta_1.meta_key)="' . $sql_field . '")
				)
				ORDER BY new_name';

			$results = $wpdb->get_results( $sql );

			$cnt_results = 0;
			if ( isset( $field_labels[ $sql_field ] ) ) {
				$label = $all_str . $field_labels[ $sql_field ];
			} else {
				if ( isset( $fields[ $field ]['label'] ) ) {
					$label = $all_str . $fields[ $field ]['label'];
				} else {
					$label = $all_str;
				}
			}
			//$array[] = array( '' => $label );
			$array[] = array( '' => '' );
			if ( ! empty( $results ) ) {
				foreach ( $results as $result ) {

					$cnt = wpsdsp_count_active_meta_items( $sql_field, $result->new_name );

					if ( $result->new_name != '-' ) {
						if ( $wpsdsp_hide_count !== 'on' ) {
							$array[] = array( $value => $result->new_name . ' (' . $cnt . ')' );
						} else {
							$array[] = array( $value => $result->new_name );
						}
					} else {
						if ( ! empty( $year_srch ) ) {
							$array[] = array( $value => 'Vintage' );
						}
					}
				}
			} else {
				$array[] = array( '0' => 'No Match' );
			}
			
		} //= end else if ( 'meta' === $fields[ $field ]['field_type'] )
		
	}

	if ( empty( $array ) ) {
		$array[] = array( '1' => 'No Match' );
		$array[] = array( '2' => 'No Match' );
	}

	if ( false === $return ) {
		echo wpsd_kses_json( json_encode( $array ) );
	} else {
		return json_encode( $array );
	}	
}

function wpsdsp_count_active_tax_items( $val, $type, $field ) {
	global $wpdb;
	$val = esc_sql( $val );
	$prefix = $wpdb->prefix;

//	$term = get_term_by( 'name', $val, $field );
//	$slug = esc_sql( $term->slug );
	$sql = 'SELECT Count(' . $prefix . 'terms.name) AS new_name
		FROM (' . $prefix . 'term_relationships 
		LEFT JOIN (' . $prefix . 'terms 
		RIGHT JOIN ' . $prefix . 'term_taxonomy 
			ON ' . $prefix . 'terms.term_id = ' . $prefix . 'term_taxonomy.term_id) 
			ON ' . $prefix . 'term_relationships.term_taxonomy_id = ' . $prefix . 'term_taxonomy.term_taxonomy_id) 
		LEFT JOIN ' . $prefix . 'postmeta ON ' . $prefix . 'term_relationships.object_id = ' . $prefix . 'postmeta.post_id
		WHERE (((' . $prefix . 'terms.name)="'. $type .'") 
		AND ((' . $prefix . 'term_taxonomy.taxonomy)="' . $field . '") 
		AND ((' . $prefix . 'postmeta.meta_key)="sold") 
		AND ((' . $prefix . 'postmeta.meta_value)="' . __( 'no', 'wp-super-dealer' ) . '"))';

	$total_cars = $wpdb->get_var( $sql );
	return $total_cars;
}

function wpsdsp_count_active_meta_items( $meta_field, $meta_value ) {
	global $wpdb;
	$prefix = $wpdb->prefix;
	$meta_srch = '';
	$sql = 'SELECT COUNT(*) as num
		FROM ' . $prefix . 'postmeta AS ' . $prefix . 'postmeta_1 
		RIGHT JOIN ' . $prefix . 'postmeta ON ' . $prefix . 'postmeta_1.post_id = ' . $prefix . 'postmeta.post_id
		WHERE ('. $meta_srch .' 
			((' . $prefix . 'postmeta.meta_key)="sold") 
			AND ((' . $prefix . 'postmeta.meta_value)="' . __( 'no', 'wp-super-dealer' ) . '") 
			AND ((' . $prefix . 'postmeta_1.meta_key)="' . $meta_field . '")
			AND ((' . $prefix . 'postmeta_1.meta_value)="' . $meta_value . '")
		)';
			
	$totals = $wpdb->get_var( $sql );

	return $totals;
}
?>