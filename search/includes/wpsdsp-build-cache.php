<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsdsp_build_cache( $return = false ) {
	set_time_limit( 12000 );
	if ( ! ini_get( 'safe_mode' ) ) ini_set( 'max_execution_time', '12000' );

	$item_value = '';

	$fields = wpsdsp_active_fields();
	
	foreach( $fields as $field=>$option ) {
		$cache = wpsdsp_build_cache_item( $field, '', 1 );
		update_option( 'wpsdsp_' . $field, $cache );
	}

	$current_inventory = wpsdsp_get_current_inventory();
	update_option( 'wpsdsp_current_inventory', $current_inventory );

	//= Send email report if we have an email address
	$wpsdsp_email_report = get_option( 'wpsdsp-email-report', '' );
	if ( ! empty( $wpsdsp_email_report ) ) {
		$msg = __( 'The json file for this site was updated: ', 'wp-super-dealer' ) . date( 'Y-m-d h:i' );
		$msg .= '<pre>';
			$msg .= print_r( $current_inventory, true );
		$msg .= '</pre>';
		wpsdsp_email_report( $msg );
	}

	if ( $return === true ) {
		return $current_inventory;
	}
}

function wpsdsp_delete_cache() {
	$fields = wpsdsp_active_fields();

	foreach( $fields as $field=>$option ) {
		$cache = wpsdsp_build_cache_item( $field, '', 1 );
		delete_option( 'wpsdsp_' . $field );
	}

	delete_option( 'wpsdsp_current_inventory' );
}

function wpsdsp_get_min_max( $end = 'max', $meta ) {
	if ( '_price' === $meta ) {
		if ( defined( 'WPSD_PRICE_START' ) && defined( 'WPSD_PRICE_STOP' ) ) {
			if ( 'max' === $end ) {
				return WPSD_PRICE_STOP;
			}
			if ( 'min' === $end ) {
				return WPSD_PRICE_START;
			}
		}
	}
	if ( '_mileage' === $meta ) {
		if ( defined( 'WPSD_MILEAGE_START' ) && defined( 'WPSD_MILEAGE_STOP' ) ) {
			if ( 'max' === $end ) {
				return WPSD_MILEAGE_STOP;
			}
			if ( 'min' === $end ) {
				return WPSD_MILEAGE_START;
			}
		}
	}
    $first = substr( $meta, 1 );
    if ( '_' !== $first ) {
        $meta = '_' . $meta;
    }

    global $wpdb;
	$prefix = $wpdb->prefix;
    $sql = $wpdb->prepare("
		SELECT ".$end."( cast( wpostmeta1.meta_value as UNSIGNED ) )
		FROM ". $prefix ."posts wposts
			LEFT JOIN ". $prefix ."postmeta wpostmeta ON wposts.ID = wpostmeta.post_id 
			LEFT JOIN ". $prefix ."postmeta wpostmeta1 ON wposts.ID = wpostmeta1.post_id 
		WHERE wposts.post_type='".WPSD_POST_TYPE."'
			AND wpostmeta.meta_key = 'sold'
			AND wpostmeta.meta_value = '". __( "no", "wp-super-dealer" ) . "'
			AND wpostmeta1.meta_key='%s'
		",
        $meta
    );

	$results = $wpdb->get_var( $sql );
    return $results;
}

function wpsdsp_remove_json_file() {
	$upload_dir = wp_upload_dir();
	$dir = $upload_dir['basedir'];
	$dir = trim( $dir );
	$blog_id = get_current_blog_id();
	$filename = trailingslashit( $dir ) . 'search-json' . $blog_id . '.txt';
	if ( file_exists( $filename ) ) {
		unlink( $filename );
	}
}

function wpsdsp_get_current_inventory() {
	$x = '';

	wpsdsp_remove_json_file();

	$array = array();
	$array1 = array();

	$cnt = 0;

	global $wpdb;
	$prefix = $wpdb->prefix;

	$query = array(
		WPSD_POST_TYPE => '1',
		'vehicles_per_page' => 1500,
	);

	$wpsd_query = wpsd_query_search( $query );

	$vehicle_query = new WP_Query();
	$vehicle_query->query( $wpsd_query );

	$count = $vehicle_query->found_posts;

	$json_array = array();

	$fields = wpsdsp_active_fields();

	if ( $vehicle_query->have_posts() ) {
	
		while ( $vehicle_query->have_posts() ) : $vehicle_query->the_post();
			++$cnt;
			$post_id = $vehicle_query->post->ID;
			$vehicle = wpsd_get_vehicle( $post_id );
			foreach( $fields as $field=>$data ) {

				if ( ! isset( $data['json_column'] ) ) {
					//= If this column doesn't have a letter then skip it
					continue;
				}
				if ( isset( $vehicle[ $data['field_name'] ] ) ) {
					$array1[ $data['json_column'] ] = $vehicle[ $data['field_name'] ];
				} else {
					//$array1[ $data['json_column'] ] = '';
					if ( isset( $vehicle[ $field ] ) ) {
						$array1[ $data['json_column'] ] = $vehicle[ $field ];
					} else {
						$array1[ $data['json_column'] ] = '0';
					}
				}
				//$array1[ $data['json_column'] ] = $vehicle[ $field ];
			}

			$new_line = $array1;
			wpsdsp_add_to_json( $new_line );
		
		endwhile;

	}

	//= Save the current min and max values for price and miles
	$min_max_values = array();
	foreach( $fields as $field=>$data ) {
		if ( 'range' === $data['type'] ) {
			$max = wpsdsp_get_min_max( 'max', $field );
			$min = wpsdsp_get_min_max( 'min', $field );

			$min_max_values['max_' . $field ] = $max;
			$min_max_values['min_' . $field ] = $min;
		}
	}

    update_option( 'wpsdsp_min_max_values', $min_max_values );
}

function wpsdsp_add_to_json($new_line) {
	$upload_dir = wp_upload_dir();
	$dir = $upload_dir['basedir'];
	$dir = trim( $dir );
	$blog_id = get_current_blog_id();
	$filename = trailingslashit( $dir ) . 'search-json' . $blog_id . '.txt';

	//= read the file if present
	$handle = @fopen($filename, 'r+');

	//= create the file if needed
	if ( ! file_exists( $filename ) ) {
		$handle = fopen( $filename, 'w+' );
	}
	
	if ( $handle ) {
		//= seek to the end
		fseek($handle, 0, SEEK_END);
	
		// are we at the end or is the file empty
		if (ftell($handle) > 0) {
			// move back a byte
			fseek($handle, -1, SEEK_END);
	
			// add the trailing comma
			fwrite($handle, ',', 1);
	
			// add the new json string
			fwrite($handle, json_encode($new_line) . ']');
		} else {
			// write the first event inside an array
			fwrite($handle, json_encode(array($new_line)));
		}

		// close the handle on the file
		fclose($handle);
	} else {
		echo 'fail';
	}
}
?>