<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_get_searched_by( $result_page = '' ) {
	$searched = '';

	$uri = $_SERVER['REQUEST_URI'];
	$protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";

	$uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
	$url = $protocol . $_SERVER['HTTP_HOST'] . $uri_parts[0];

	$query_string = $_SERVER['QUERY_STRING'];
	$query_string = str_replace( '%2C', ',', $query_string );
	$query_array_original = wp_parse_args( $query_string );
	
	$fields = wpsdsp_active_fields();

	foreach( $fields as $field=>$options ) {
		if ( 'range' === $options['type'] ) {
			$range = array( 'min', 'max' );
			foreach( $range as $key=>$item ) {
				if ( isset( $_GET[ $field . '_' . $item ] ) ) {
					if ( $_GET[ $field . '_' . $item ] ) {
						$selected_item = stripslashes_deep( sanitize_text_field( $_GET[  $field . '_' . $item  ] ) );
						$remove_query_string = '';
						$query_array = $query_array_original;
						$query_array[ $field . '_' . $item ] = str_replace( ',' . $selected_item, '', $query_array[ $field . '_' . $item ] );
						$query_array[ $field . '_' . $item ] = str_replace( $selected_item . ',', '', $query_array[ $field . '_' . $item ] );
						$query_array[ $field . '_' . $item ] = str_replace( $selected_item, '', $query_array[ $field . '_' . $item ] );
	
						if ( empty( $query_array[ $field .'_' . $item ] ) ) {
							unset( $query_array[ $field .'_' . $item ] );
						}
						foreach( $query_array as $name=>$value ) {
							if ( ! empty( $remove_query_string ) ) {
								$remove_query_string .= '&';
							}
							$remove_query_string .= $name . '=' . $value;
						}
						$remove_query_string = '?' . $remove_query_string;
						
						$remove_url = $url . $remove_query_string;
						$searched .= wpsd_get_searched_by_item( $field, $remove_url, $selected_item, $selected_item, $item );
					} //= end if ( $_GET[ $options[ $item ] ] )
				} //= end if ( isset( $_GET[ $options[ $item ] ] ) )
			} //= end foreach( $range as $key=>$item )
			
		} else { //= else for if ( 'range' === $options['type'] )

			if ( isset( $_GET[ $options['search_field'] ] ) ) {
				if ( ! empty ( $_GET[ $options['search_field'] ] ) || '0' === $_GET[ $options['search_field'] ] ) {
					$selected = stripslashes_deep( sanitize_text_field( $_GET[ $options['search_field'] ] ) );
	
					if ( strpos( $selected, '|' ) !== false ) {
						$selected_items = explode( '|', $selected );
					} else {
						$selected_items = array( $selected );
					}
	
					foreach( $selected_items as $selected_item ) {
						$field_value = $selected_item;
						$remove_query_string = '';
						$query_array = $query_array_original;
						if ( ! isset( $query_array[ $options['search_field'] ] ) ) {
							$query_array[ $options['search_field'] ] = '';
						}
						$query_array[ $options['search_field'] ] = str_replace( '|' . $selected_item, '', $query_array[ $options['search_field'] ] );
						$query_array[ $options['search_field'] ] = str_replace( $selected_item . '|', '', $query_array[ $options['search_field'] ] );
						$query_array[ $options['search_field'] ] = str_replace( $selected_item, '', $query_array[ $options['search_field'] ] );
	
						if ( empty( $query_array[ $field ] ) ) {
							unset( $query_array[ $field ] );
						}
						foreach( $query_array as $name=>$value ) {
							if ( ! empty( $remove_query_string ) ) {
								$remove_query_string .= '&';
							}
							$remove_query_string .= $name . '=' . $value;
						}
						$remove_query_string = '?' . $remove_query_string;
						
						$remove_url = $url . $remove_query_string;

						if ( isset( $options['yes_no'] ) ) {
							if ( $options['yes_no'] ) {
								$name = str_replace( '_', ' ', $field );
								if ( 0 == $selected_item && 'Yes' !== $selected_item ) {
									$selected_item = ucwords( $name ) . ': ' . __( 'No', 'wp-super-dealer' );
									$value = 'FALSE';

								} else {
									$selected_item = ucwords( $name ) . ': ' . __( 'Yes', 'wp-super-dealer' );
									$value = '1';
								}
							}
						}
						
						$searched .= wpsd_get_searched_by_item( $field, $remove_url, $field_value, $selected_item );
					} //= end foreach( $selected_items as $selected_item )
	
				} //= end if ( ! empty( $_GET[ $options['search_field'] ] ) )
	
			} //= if ( isset( $_GET[ $options['search_field'] ] ) )

		} //= end if ( 'range' === $options['type'] )

	} //= end foreach( $fields as $field=>$options )
	
	$searched .= '@@';
	$searched = str_replace( ', @@', '', $searched );
	$searched = str_replace( '@@', '', $searched );
	if ( ! empty( $searched ) ) {
		$searched = '<div class="wpsd-search-by-wrap">' . $searched . '</div>';
	}

    $searched = apply_filters( 'wpsd_searched_by_filter', $searched, $result_page );
	return $searched;
}

function wpsd_get_searched_by_item( $field, $remove_url, $value, $selected_item, $type = '' ) {
	$type_html = '';
	if ( empty( $value ) ) {
        if ( ! empty( $selected_item ) ) {
		  $value = $selected_item;
        } else {
            return;
        }
	}
	if ( 'FALSE' === $value ) {
		$value = '0';
	}
	if ( ! empty( $type ) ) {
		$type_html = ucwords( $field ) . ' ' . ucwords( $type ) . ': ';

		if ( 'price' === $field || 'mileage' === $field ) {

			$val = number_format_i18n( $selected_item, 0 );

			if ( 'price' === $field ) {
				global $wpsd_options;
				if ( isset( $wpsd_options['price_before'] ) ) {
					$currency_before = $wpsd_options['price_before'];
				} else {
					$currency_before = '$';
				}
				if ( isset( $wpsd_options['price_after'] ) ) {
					$currency_after = $wpsd_options['price_after'];
				} else {
					$currency_after = '';
				}
				$val = $currency_before . $val . $currency_after;
			}
			$type_html .= $val;
			$selected_item = '';
		}
	}

	$searched = '<span class="wpsd-search-by-item" data-value="' . trim( esc_attr( $value ) ) . '">';
		$searched .= '<span class="wpsd-search-by-item-remove" data-value="' . trim( esc_attr( $value ) ) . '" data-url="' . esc_url( $remove_url ) . '" data-type="' . esc_attr( $type ) . '" data-field="' . esc_attr( $field ) . '" data-selected-item="' . trim( esc_attr( $selected_item ) ) . '">';
			$searched .= 'x';
		$searched .= '</span>';
		$searched .= esc_html( $type_html );
		$searched .= '<span class="wpsd-search-by-item-title">';
			$searched .= trim( esc_html( $selected_item ) );
		$searched .= '</span> ';
	$searched .= '</span>';

    $elements = wpsd_searched_by_elements();
    $searched = wp_kses( $searched, $elements );

    $searched = apply_filters( 'wpsd_searched_by_filter_item', $searched, $field, $remove_url, $value, $selected_item, $type );
	return $searched;
}

function wpsd_searched_by_elements() {
    $elements = array(
        'span' => array(
            'class' => array(),
            'data-value' => array(),
            'data-url' => array(),
            'data-type' => array(),
            'data-field' => array(),
            'data-selected-item' => array(),
        ),
    );
    $elements = apply_filters( 'wpsd_searched_by_item_kses_element_filter', $elements );
    return $elements;
}
?>