<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_filter( 'wpsd_query_filter', 'wpsdsp_vehicle_query', 10, 1 );
function wpsdsp_vehicle_query( $wpsd_query ) {
	//= $custom_fields = get_option( 'wpsdsp_custom_fields', array() );
	$custom_fields = wpsdsp_active_fields();
	$get = wpsdsp_get_querystring();

	foreach( $custom_fields as $custom_field=>$data ) {
		$compare = '=';
		if ( isset( $get[ $custom_field ] ) || isset( $get[ $custom_field . '_min' ] ) || isset( $get[ $custom_field . '_max' ] ) ) {
			if ( isset( $get[ $custom_field ] ) ) {
				$value = sanitize_text_field( $get[ $custom_field ] );
			} else {
				$value = '';
			}
			$value = urldecode( $value );

			$meta_query = $wpsd_query['meta_query'];

			if ( 'taxonomy' === $data['field_type'] ) {
				continue;
			}
			//= is this a range field
			if ( 'range' === $data['type'] ) {
				$meta_query = wpsdsp_remove_from_meta( $data['field_name'], $meta_query );
				$custom_query = wpsdsp_range_item_query( $custom_field, $data );
			} else {
                //= other field types should already be taken care of in the main query
                continue;
			}

			if ( is_array( $custom_query ) && count( $custom_query ) > 0 ) {
				$meta_query = array_merge( $meta_query, $custom_query );
				$wpsd_query['meta_query'] = $meta_query;
			}

		} //= end if ( isset( $get[ $custom_field ] ) )
	} //= end foreach( $custom_fields as $custom_field=>$data )
    
	//= Fix empty taxonomy terms
	if ( isset( $wpsd_query['tax_query'] ) ) {
		if ( is_array( $wpsd_query['tax_query'] ) ) {
			foreach( $wpsd_query['tax_query'] as $key=>$data ) {
				if ( isset( $data['terms'] ) ) {
					if ( count( $data['terms'] ) < 1 ) {
						unset( $wpsd_query['tax_query'][ $key ] );
					}
				}
			}
		}
	}
	if ( isset( $wpsd_query['tax_query'] ) ) {
		if ( count( $wpsd_query['tax_query'] ) < 1 ) {
			unset( $wpsd_query['tax_query'] );
		}
	}

    return $wpsd_query;
}

function wpsdsp_range_item_query( $field, $data ) {
	$min_max_values = get_option( 'wpsdsp_min_max_values', array() );
	
	$get = wpsdsp_get_querystring();
	$meta_query = array();
	if ( isset( $get[ $field . '_min'] ) ) {
		if ( empty( $get[ $field . '_min'] ) ) {
			$get[ $field . '_min'] = 1;
		}
		$min = sanitize_text_field( $get[ $field . '_min'] ) - 1;
	} else {
		$min = '';
	}

	if ( isset( $get[ $field . '_max'] ) ) {
		if ( empty( $get[ $field . '_max'] ) && isset( $min_max_values[ 'max_' . $field ] ) ) {
			$max = $min_max_values[ 'max_' . $field ];
		} else {
			$max = $get[ $field . '_max'];
		}
	}

	if ( empty( $min ) && empty( $max ) ) {
		return;
	}

	if ( $max > 0 ) {
		if ( $min == 0 ) { $min = 1; }
		$meta_query = array_merge( $meta_query, array( array( 'key' => '_' . $data['field_name'],'value' => array( $min, $max ), 'compare' => 'BETWEEN', 'type' => 'numeric' ) ) );
	} else {
		if ( $min > 0 ) {
			$meta_query = array_merge( $meta_query, array( array( 'key' => '_' . $field,'value' => $min, 'compare' => '>', 'type' => 'numeric' ) ) );
		}
	}

	return $meta_query;
}

function wpsdsp_remove_from_meta( $field, $meta_query ) {
	foreach( $meta_query as $key=>$data ) {
		if ( isset( $data['key'] ) && $field === $data['key'] ) {
			unset( $meta_query[ $key ] );
		}
	}

	return $meta_query;
}
?>