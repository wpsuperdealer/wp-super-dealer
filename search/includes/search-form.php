<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsdsp_get_search_form( $settings = array() ) {
	$site_url = get_option( 'siteurl' );
	global $wpsd_options;
	if ( isset( $wpsd_options['inventory_page'] ) ) {
		$url = $wpsd_options['inventory_page'];
	}

	if ( isset( $settings['form_action'] ) ) {
		if ( ! empty( $settings['form_action'] ) ) {
			if ( $settings['form_action'] !== $site_url ) {
				$url = $settings['form_action'];
			}
		}
	}
    
	if ( isset( $settings['result_page'] ) ) {
		if ( ! empty( $settings['result_page'] ) ) {
			if ( $settings['result_page'] !== $site_url ) {
				$url = $settings['result_page'];
			}
		}
	}

    if ( empty( $url ) ) {
		$url = $site_url;
	}

	if ( '#' === $url ) {
		global $wp;
		$url = home_url( $wp->request );
	}

	if ( $url == $site_url ) {
		$search_fld = '<input type="hidden" name="s" value="vehicles" />';
	} else {
		$search_fld = '';
	}

	$custom_class = '';
	if ( isset( $settings['custom_class'] ) ) {
		if ( ! empty( $settings['custom_class'] ) ) {
			$custom_class = ' ' . $settings['custom_class'];
		}
	}

	$style = '';
	if ( isset( $settings['style'] ) ) {
		if ( ! empty( $settings['style'] ) ) {
			$custom_class .= ' wpsdsp-search-form-' . $settings['style'];
		}
	}

    $option_is_field_name = '';
    if ( isset( $settings['option_is_field_name'] ) ) {
		if ( wpsd_is_true( $settings['option_is_field_name'] ) ) {
			$custom_class .= ' wpsdsp-option-is-field-name';
		}
    }
	
	if ( ! isset( $settings['label'] ) ) {
		$label = array();
	} else {
		$label = $settings['label'];
	}

	if ( isset( $settings['sliders'] ) 
		&& ! empty( $settings['sliders'] )
		&& 'false' !== $settings['sliders']
		&& false !== $settings['sliders'] ) 
	{
		$custom_class .= ' search-form-sliders';
	}
	
	$filter_class = '';

	$fields = wpsdsp_active_fields();
	$field_array = array();
	$field_map = array();
	$cnt = 0;

	foreach ( $fields as $field=>$options ) {
		++$cnt;
		$field_array[ $cnt ] = $field;
		$field_map[ $field ] = $cnt;

		if ( isset( $settings[ $field ] ) ) {
			$options = wp_parse_args( $settings[ $field ], $options );
			
			if ( isset( $options['remove'] ) ) {
				if ( $options['remove'] ) {
					unset( $fields[ $field ] );
					continue;
				}
			}
			
			$fields[ $field ] = $options;
		}

		$get = wpsdsp_get_querystring();
		if ( isset( $get[ $options['search_field'] ] ) ) {
			if ( $get[ $options['search_field'] ] !== '' ) {
				$value = sanitize_text_field( $get[ $options['search_field'] ] );
				$value = wpsdsp_ucfirst( $value );
				$fields[ $field ]['value'] = $value;
				$filter_class = ' wpsdsp_filter_active';
			}
		}
		
		if ( isset( $settings['value'][ $field ] ) ) {
			if ( $settings['value'][ $field ] !== '' ) {
				$value = $settings['value'][ $field ];
				$value = wpsdsp_ucfirst( $value );
				$fields[ $field ]['value'] = $value;
				$filter_class = ' wpsdsp_filter_active';
			}
		}

		if ( isset( $settings['label'][ $field ] ) ) {
			$fields[ $field ]['label'] = $settings['label'][ $field ];
		}

		$fields[ $field ]['html'] = wpsdsp_build_field( $field, $options );

	} //= end foreach( $fields as $field=>$options )

	$field_array = apply_filters( 'wpsdsp_field_array_filter', $field_array );

	//= Do we have a custom map from settings (exp. body|location|condition)
	if ( isset( $settings['field_map'] ) ) {
		if ( ! empty( $settings['field_map'] ) ) {
			if ( strpos( $settings['field_map'], '|' ) !== false ) {
				$new_field_map = array();
				$new_map_fields = explode( '|', $settings['field_map'] );
				$new_map = array();
				foreach( $new_map_fields as $new_map_field ) {
					//= Does this field exist?
					if ( isset( $field_map[ $new_map_field ] ) ) {
						//= Add the field to our new array
						$new_map[ $new_map_field ]  = $field_map[ $new_map_field ];
						//= Remove it from the current map
						unset( $field_map[ $new_map_field ] );
					}
				}
				//= Merge the new map with the old map, putting the new map first
				//$field_map = array_merge( new_map, $field_map );
				//= Make the field_map match the new_map
				$field_map = $new_map;
			}
		}
	}

	$field_map = apply_filters( 'wpsdsp_field_map_filter', $field_map, $field_array );

	$fields_html = '';

	foreach( $field_map as $field=>$key ) {
		if ( isset( $fields[ $field ] ) ) {
			if ( isset( $fields[ $field ]['html'] ) ) {
				$fields_html .= $fields[ $field ]['html'];
			}
		}
	}

	$fields_html = apply_filters( 'wpsdsp_fields_html_filter', $fields_html );

	$style_class = '';
	$search_trigger = '';

	$custom_class .= ' wpsdsp-select-form';
	if ( wp_is_mobile() ) {
		$custom_class .= ' wpsdsp-mobile-search-form';
	}

	$fields_json = base64_encode( json_encode( $fields ) );
	$settings_json = base64_encode( json_encode( $settings ) );

    $tags_title = '';
    if ( isset( $settings['tags_title'] ) ) {
        $tags_title = $settings['tags_title'];
    }
    $tags_button = '';
    if ( isset( $settings['tags_button'] ) ) {
        $tags_button = $settings['tags_button'];
    }
    $simple_selects = '';
    if ( isset( $settings['simple_selects'] ) ) {
        if ( wpsd_is_true( $settings['simple_selects'] ) ) {
            $simple_selects = 'yes';
            $style_class = ' wpsdsp-simple-selects ';
        }
    }

    $x = '';
    $x .= '<div class="wpsdsp-mobile-filter-button">';
        $x .= '<span class="dashicons dashicons-filter"></span>';
        $x .= __( 'Filter Inventory', 'wp-super-dealer' );
    $x .= '</div>';
    
    $x .= '<a name="wpsdsp-search-form" id="wpsdsp-search-form"></a>';
    //= Build the form
	$x .= '<div class="wpsdsp-search-form'. esc_attr( $style_class ) . esc_attr( $custom_class ) . '" data-action="' . esc_attr( $url ) . '" data-method="get" id="wpsdsp-search-form" data-fields="' . esc_attr( $fields_json ) . '" data-settings="' . esc_attr( $settings_json ) . '" data-tags-title="' . esc_attr( $tags_title ) . '" data-tags-button="' . esc_attr( $tags_button ) . '" data-simple-selects="' . esc_attr( $simple_selects ) . '">';
        $x .= '<a id="wpsdsp-inventory-top" name="wpsdsp-inventory-top"></a>';
        $x .= $search_fld;
		$x .= $search_trigger;
		$x .= '<span id="wp-super-dealer-as">';
			$x .= '<div id="wpsdsp-current-filters-top" class="wpsdsp-current-filters wpsdsp-current-filters-top">';
			$x .= '</div>';
			$x .= $fields_html;
			$x .= '<div id="wpsdsp-current-filters-middle" class="wpsdsp-current-filters wpsdsp-current-filters-middle">';
			$x .= '</div>';

			if ( isset( $settings['show_sort'] ) 
				&& wpsd_is_true( $settings['show_sort'] ) )
			{
				$x .= '<div class="wpsdsp-item wpsdsp_select">';
					$x .= wpsd_sort_form();
				$x .= '</div>';
			}
			if ( ! isset( $settings['hide_search_button'] ) 
				|| ( isset( $settings['hide_search_button'] )
			   && ! wpsd_is_true( $settings['hide_search_button'] ) ) )
			{
				if ( ! isset( $settings['submit_label'] ) ) {
					$settings['submit_label'] = __( 'Search', 'wp-super-dealer' );
				}
				$x .= '<div class="wpsdsp-item wpsdsp-item-submit">';
					$x .= '<div id="wpsdsp-button-box" class="wpsdsp-button-box">';
						$x .= '<span class="wpsdsp-button-apply wpsdsp-button">';
							$x .= '<label class="wpsdsp-button-label">';
								$x .= esc_html( $settings['submit_label'] );
							$x .= '</label>';
							$x .= '<span class="wpsdsp-button-icon"></span>';
						$x .= '</span>';
					$x .= '</div>';
				$x .= '</div>';

			   $x .= '<div class="clear"></div>';
			}

			$x .= ' <div id="reset_wpsdsp_filters" class="reset_wpsdsp_filters reset_wpsdsp_filters' . esc_attr( $filter_class ) . '">';
				$x .= __( 'Reset', 'wp-super-dealer' );
			$x .= '</div>';
			$x .= '<div id="wpsdsp-current-filters-bottom" class="wpsdsp-current-filters wpsdsp-current-filters_bottom" data-tags-title="' . esc_attr( $tags_title ) . '" data-tags-button="' . esc_attr( $tags_button ) . '">';
			$x .= '</div>';
		$x .= '</span>';

		if ( isset( $settings['hide_search_button'] )
		   && wpsd_is_true( $settings['hide_search_button'] ) )
		{
			$x .= '<div class="view_inventory_button">';
				$x .= '<div>';
					$x .= esc_attr( $settings['submit_label'] );
				$x .= '</div>';
			$x .= '</div>';
		}

		$x .= '<div class="wpsdsp_end_form"></div>';
	$x .= '</div>';
    $x .= '<div class="wpsdsp-view-inventory-btn wpsdsp-view-inventory-btn-bottom">';
        $x .= '<span class="dashicons dashicons-filter"></span>';
        $x .= __( 'Close Filters', 'wp-super-dealer' );
    $x .= '</div>';
    $x .= '<div class="wpsd-clear"></div>';
	
    $x = wpsd_kses_search_form( $x );
    
	return $x;
}

function wpsdsp_build_field( $field, $options ) {
	$tag = $options['type'];
	if ( 'checkbox' === $tag || 'range' === $tag || 'text' === $tag ) {
		$tag = 'div';
	}
	$html = '';

	$get = wpsdsp_get_querystring();

	//= Open or closed status
	$status = 'open';
	//= Down arrow symbol
	$symbol = '-';
	//= Create variable to store a CSS class if this field is closed by default
	$open_or_closed = '';
	//= Is this field closed by default
	if ( isset( $options['closed'] ) && $options['closed'] && ! empty( $options['closed'] ) ) {
		$status = 'closed';
		$symbol = '+';
		$open_or_closed = ' wpsdsp_closed_field';
	}

	$hide_class = '';
	if ( isset( $options['hide'] ) ) {
		if ( $options['hide'] ) {
			$hide_class = ' wpsdsp_hide_field';
		}
	}

	$html .= '<div class="wpsdsp-item wpsdsp-' . esc_attr( $options['type'] ) . esc_attr( $hide_class ) . '">';
		$html .= '<div id="label-' . esc_attr( $field ) . '" class="label-' . esc_attr( $field ) . ' wpsdsp-label" data-field="' . esc_attr( $field ) . '" data-status="' . esc_attr( $status ) . '">';
			$html .= '<span class="wpsdsp-label-text">';
				$html .= esc_html( $options['label'] );
			$html .= '</span>';
			
			$html .= '<span class="selectArrow" data-field="' . esc_attr( $field ) . '" data-status="' . esc_attr( $status ) . '">' . esc_html( $symbol ) . '</span>';
		$html .= '</div>';

		$html .= '<div class="wpsdsp-select-wrap wpsdsp-select-wrap-' . esc_attr( $field ) . '" data-field="' . esc_attr( $field ) . '">';

			//= If this is a select box then we might need to pre-populate the selected element where we show selected tags
			$selected = '';
			$selected_unhide = '';

			if ( 'select' === $options['type'] || 'checkbox' === $options['type'] ) {

				if ( isset( $get[ $options['search_field'] ] ) ) {
					$options['value'] = sanitize_text_field( $get[ $options['search_field'] ] );
				}
				
				if ( ! isset( $options['value'] ) ) {
					$options['value'] = '';
				}

				$options['value'] = trim( $options['value'] );
				if ( empty( $options['value'] ) ) {
					unset( $options['value'] );
				}
				
				$selected_unhide = ' selected_unhide';
				$selected_values = '';
				if ( isset( $options['value'] ) ) {
					$selected_values = sanitize_text_field( $options['value'] );
				}
				//= Is there more than one selected value?
				if ( strpos( $selected_values, '|' ) !== false ) {
					$selected_values = explode( '|', $selected_values );
					$selected = '';
					foreach( $selected_values as $selected_value ) {
						$selected .= wpsdsp_selected_html( false, $field, $selected_value );
					}
				} else {
					if ( isset( $options['value'] ) ) {
						$query_value = sanitize_text_field( $options['value'] );
						$selected = wpsdsp_selected_html( false, $field, $query_value );
					} else {
						$selected = '';
					}
				}
				
			}

			$html .= '<span class="selected ' . esc_attr( $field ) . '_selected' . esc_attr( $selected_unhide ) . '" data-field="' . esc_attr( $field ) . '" id="' . esc_attr( $field ) . '_selected">' . esc_html( $selected ) . '</span>';

			$placeholder = __( 'Select ', 'wp-super-dealer' ) . $options['label'];
			$placeholder = str_replace( '_', ' ', $placeholder );
			$placeholder = ucwords( $placeholder );

			$html .= '<div class="selectBox search-' . esc_attr( $field ) . esc_attr( $open_or_closed ) . '" id="search-' . esc_attr( $field ) . '">';
				$time = '_' . time();
				$time = '';

				if ( 'criteria' === $options['search_field'] ) {
					$html .= do_shortcode( '[wpsd_keyword button="" message="" default_value="' . esc_attr( $options['value'] ) . '"]' );
				} else {
					$mobile_class = '';
					if ( wp_is_mobile() ) {
						$mobile_class = ' wpsdsp-mobile-search-form-input';
					}
					$value = '';
					if ( isset( $options[ $field ]['value'] ) ) {
						$value = $options[ $field ]['value'];
					}
					$html .= '<input type="text" data-field="' . esc_attr( $field ) . '" placeholder="' . esc_attr( $placeholder ) . '" autocomplete="off' . esc_attr( $time ) . '" id="' . esc_attr( $field ) . '_value" class="wpsdsp-search wpsdsp-search-' . esc_attr( $field ) . esc_attr( $mobile_class ) . '" name="' . esc_attr( $field ) . '" value="' . esc_attr( $value ) . '" />';
				}
				$html .= '<span class="wpsdsp-spacer">&nbsp;</span>';
				$html .= '<' . esc_attr( $tag ) . ' class="selectOptions wpsdsp-select-options-' . esc_attr( $tag ) . ' search_' . esc_attr( $field ) . '_options ' . esc_attr( $field ) . ' ' . esc_attr( $options['type'] ) . '" data-field="' . esc_attr( $field ) . '" id="' . esc_attr( $field ) . '_options">';
					if ( 'range' === $options['type'] ) {
						$html .= get_wpsdsp_range_item( $field, $options );
					} else if ( 'text' === $options['type'] ) {
						$html .= get_wpsdsp_text_item( $field, $options );
					} else {
						if ( 'select' === $options['type'] ) {
							$html .= '<option></option>';
						}
						$html .= get_wpsdsp_item( $field, $options );
					}
				$html .= '</' . esc_attr( $tag ) . '>';
				$html .= '<div id="apply-' . esc_attr( $field ) . '" class="wpsdsp-apply apply-' . esc_attr( $field ) . '">';
					$html .= 'Apply';
				$html .= '</div>';
				$html .= '<div id="apply-' . esc_attr( $field ) . '-cancel" class="wpsdsp-apply-cancel apply-' . esc_attr( $field ) . '-cancel">';
					$html .= 'Hide';
				$html .= '</div>';
			$html .= '</div>';

		$html .= '</div>';

	$html .= '</div>';
    
	return $html;
}

function get_wpsdsp_item( $field, $options ) {
	$x = '';

	$items = get_option( 'wpsdsp_' . $field );

	if ( isset( $options['value'] ) ) {
		if ( $options['value'] !== '' ) {
			$value = sanitize_text_field( $options['value'] );
			$value = str_replace( '%2C', '', $value );
			$value = str_replace( ',', '', $value );
			$value = ucfirst( $value );
			if ( stripos( $value, '-' ) ) {
				$value_array = explode( '-', $value );
				$value = $value_array[0];
			}
			if ( ! empty( $options['value'] ) ) {
//				$x .= get_wpsdsp_field( false, 'search_' . $field, $options['value'], $options );
			}
		}
	}

	$items_array = json_decode( $items, true );

	if ( is_object( $items_array ) ) {
		foreach ( $items_array as $item ) {
			foreach ( $item as $slug=>$value ) {
				if ( ! empty( $value ) ) {
					$x .= get_wpsdsp_field( false, $field, ucfirst( $value ), $options );
				}
			}
		}
	}

	return $x;
}

function get_wpsdsp_text_item( $field, $options ) {
	$x = '';
	//= Textbox is already output, so we need to do nothing
	
	return $x;
}

function get_wpsdsp_range_item( $field, $options ) {
	$x = '';

	if ( ! isset( $options['min'] ) ) {
		$options['min'] = '1';
	}

	$get = wpsdsp_get_querystring();
	$min_max_values = get_option( 'wpsdsp_min_max_values', true );

    $min = 0;
	if ( isset( $min_max_values['min_' . $field ] ) ) {
		$min = $min_max_values['min_' . $field ];
	}
	if ( isset( $min_max_values['max_' . $field ] ) ) {
		$max = $min_max_values['max_' . $field ];
	}
	$min = round( $min, -3 ); //= round down to nearest thousand

	if ( isset( $options['min'] ) && ! empty( ( $options['min'] ) ) ) {
		$min = $options['min'];
	}
	if ( isset( $options['max'] ) && ! empty( ( $options['min'] ) ) ) {
		$max = $options['max'];
	}
	$gap = $options['gap'];

	$stop = $min + $gap;

	$select_min = '';
	$select_min = '';
	$select_max_array = array();

	$min_selected = '';
	$max_selected = '';

	if ( isset( $get[ $field . '_min'] ) ) {
		$min_selected = sanitize_text_field( $get[ $field . '_min' ] );
	}
	if ( isset( $get[ $field . '_max' ] ) ) {
		$max_selected = sanitize_text_field( $get[ $field . '_max' ] );
	}

	$cnt = 0;
	
	while( $min < $max ) {
		$stop = $min + $gap;

		if ( 'price' === $field || 'mileage' === $field ) {
			$min_val = number_format_i18n( $min, 0 );
			$max_val = number_format_i18n( $stop, 0 );
		} else {
			$min_val = $min;
			$max_val = $stop;
		}

		if ( 'price' === $field ) {
			global $wpsd_options;
			if ( isset( $wpsd_options['price_before'] ) ) {
				$currency_before = $wpsd_options['price_before'];
			} else {
				$currency_before = '$';
			}
			if ( isset( $wpsd_options['price_after'] ) ) {
				$currency_after = $wpsd_options['price_after'];
			} else {
				$currency_after = '';
			}
			$min_val = $currency_before . $min_val . $currency_after;
			$max_val = $currency_before . $max_val . $currency_after;
		}

		if ( 0 == $min_selected ) {
			$min_selected = 0.1;
		}
		
		$select_min .= '<option value="' . esc_attr( $min ) . '"' . ( $min == $min_selected ? ' selected' : '' ) . '>' . esc_attr( $min_val ) . '</option>';
		$select_max_array[] = '<option value="' . esc_attr( $stop ) . '"' . ( $max_selected == $stop ? ' selected' : '' ) . '>' . esc_attr( $max_val ) . '</option>';
		$min = $min + $gap;

		++$cnt;
		if ( $cnt > 50 ) {
			break;
		}
	}

	//= TO DO Future: decide if we should reverse the order of the Max options
	//$select_item_2_array = array_reverse( $select_item_2_array );
	$select_max = '';
	foreach( $select_max_array as $key=>$html ) {
		$select_max .= $html;
	}

	$x .= '<select class="wpsdsp-range wpsdsp-range-item wpsdsp-range-item-min wpsdsp_min wpsdsp_' . esc_attr( $field ) . '" data-field="'. esc_attr( $field ) . '">';
		$x .= '<option value="">' . __( 'Min', 'wp-super-dealer' ) . '</option>';
		$x .= $select_min;
	$x .= '</select>';	

	$x .= '<select class="wpsdsp-range wpsdsp-range-item wpsdsp-range-item-max wpsdsp_max wpsdsp_' . esc_attr( $field ) . '" data-field="'. esc_attr( $field ) . '">';
		$x .= '<option value="">' . __( 'Max', 'wp-super-dealer' ) . '</option>';
		$x .= $select_max;
	$x .= '</select>';	
	
	return $x;
}

function get_wpsdsp_field( $template = false, $field = '', $text = '', $options = array(), $checked = false ) {

	if ( $checked ) {
		$checked = ' checked';
	} else {
		$checked = '';
	}

	$value_str = $text;
	if ( strpos( $value_str, '(' ) !== false ) {
		$value_array = explode( '(', $value_str );
		$value = $value_array[0];
	} else {
		$value = $value_str;
	}

	//= Do we hide the count?
	$wpsdsp_hide_count = get_option( 'wpsdsp_hide_count', 'off' );
	if ( 'on' === $wpsdsp_hide_count ) {
		$text = $value;
	}

	$value = trim( $value );

	if ( empty( $value ) ) {
		return '';
	}

	if ( 0 === $value || '0' === $value ) {
		$text = __( 'No', 'wp-super-dealer' );
	}
	if ( 1 === $value || '1' === $value ) {
		$text = __( 'Yes', 'wp-super-dealer' );
	}
	if ( null === $value || 'null' === $value ) {
		$text = __( 'Unknown', 'wp-super-dealer' );
		$value = '';
	}

    $slug = sanitize_title( $value );
    
	if ( $template ) {
		$value = '{value}';
		$text = '{text}';
		$field = '{field}';
		$checked = '{checked}';
        $slug = '{slug}';
	}

    $css_class = ' wpsdsp-item-value-' . $slug;

	if ( 'select' === $options['type'] ) {
		$x = '<option data-type="' . esc_attr( $options['type'] ) . '" class="selectOption ' . esc_attr( $css_class ) . '" value="' . esc_attr( $value ) . '">' . esc_attr( $text ) . '</option>';
	} else if ( 'checkbox' === $options['type'] ) {
		$checked = '{checked}';

		if ( isset( $options['value'] ) ) {
			$checked_values = sanitize_text_field( $options['value'] );
			//= Is there more than one selected value?
			if ( strpos( $checked_values, '|' ) !== false ) {
				$checked_values = explode( '|', $checked_values );
				if ( in_array( $value, $checked_values ) ) {
					$checked = ' checked';
				}
			} else {
				if ( $checked_values === $value ) {
					$checked = ' checked';
				}
			}
		}
    
		$checkbox = '<input type="checkbox" name="' . esc_attr( $field ) . '" class="wpsdsp-checkbox-element' . esc_attr( $css_class ) . '" value="' . esc_attr( $value ) . '"' . esc_attr( $checked ) . ' />';
		$x = '<div data-type="' . esc_attr( $options['type'] ) . '" class="selectOption selectOption-' . $slug . '" value="' . esc_attr( $value ) . '">' . esc_html( $checkbox ) . '<span class="wpsdsp-checkbox-element-label">' . esc_html( $text ) . '</span></div>';
	} else if ( 'ul' === $options['type'] ) {
		$x = '<li data-type="' . esc_attr( $options['type'] ) . '" class="selectOption selectOption-' . $slug . '' . esc_attr( $css_class ) . '" value="' . esc_attr( $value ) . '">' . esc_attr( $text ) . '</li>';
	} else {
		$x = '<span data-type="span" class="selectOption selectOption-' . $slug . ' wpsdsp_span' . esc_attr( $css_class ) . '" value="' . esc_attr( $value ) . '">' . esc_attr( $text ) . '</span>';
	}

	return $x;
}

function wpsdsp_selected_html( $template = true, $field, $value ) {
	$value = trim( $value );
	if ( empty( $value ) ) {
		return;
	}
	$x = '<span class="selected-wrap selected-wrap_' . esc_attr( $field ) . '" data-field="' . esc_attr( $field ) . '" data-value="' . esc_attr( $value ) . '">';
		$x .= '<span class="selected-remove" data-parent="selected-wrap_' . esc_attr( $field ) . '" data-field="' . esc_attr( $field ) . '" data-value="' . esc_attr( $value ) . '">';
			$x .= 'x';
		$x .= '</span>';
		$x .= '<span class="selected_value">';
			$x .= esc_html( $value );		
		$x .= '</span>';
	$x .= '</span>';
	return $x;
}

function wpsdsp_ucfirst( $value ) {
//	$value = str_replace( ',', '', $value );
//	$value = ucfirst( $value );
	$value = str_replace( '%2C', '', $value );
	$value = str_replace( ',', '', $value );

	return $value;	
}

function wpsdsp_get_querystring() {
	$get = $_GET;

	if ( defined( 'WPSDSP_NO_COOKIE_SEARCH' ) ) {
		if ( true === WPSDSP_NO_COOKIE_SEARCH ) {
			return $get;
		}
	}
	if ( isset( $_COOKIE['wpsdsp-criteria'] ) ) {
		$cookie = sanitizxe_text_field( $_COOKIE['wpsdsp-criteria'] );
		$cookie = str_replace( '?', '', $cookie );
		if ( ! empty( $cookie ) ) {
			if ( false !== strpos( $cookie, '&' ) ) {
				$search_items = explode( '&', $cookie );
				foreach( $search_items as $key=>$field ) {
					if ( false !== strpos( $field, '=' ) ) {
						$field_data = explode( '=', $field );
						if ( isset( $field_data[1] ) ) {
							$get[ $field_data[0] ] = $field_data[1];
						}
					}
				}
			} else {
				$field_data = explode( '=', $field );
				if ( isset( $field_data[1] ) ) {
					$get[ $field_data[0] ] = $field_data[1];
				}
			} //= end if ( false !== strpos( $cookie, '&' ) )
		}
	}
	if ( count( $get ) > 0 ) {
		$get[ WPSD_POST_TYPE ] = 1;
	}
	return $get;	
}

function wpsd_kses_search_form( $content ) {
    $elements = array(
        'div' => array(
            'class' => array(),
            'name' => array(),
            'id' => array(),
            'data-action' => array(),
            'data-method' => array(),
            'data-field' => array(),
            'data-fields' => array(),
            'data-tags-title' => array(),
            'data-tags-button' => array(),
            'data-simple-selects' => array(),
            'data-status' => array(),
        ),
        'a' => array(
            'name' => array(),
            'id' => array(),
        ),
        'span' => array(
            'id' => array(),
            'class' => array(),
            'data-field' => array(),
            'data-status' => array(),
        ),
        'input' => array(
            'id' => array(),
            'type' => array(),
            'class' => array(),
            'placeholder' => array(),
            'data-field' => array(),
            'data-status' => array(),
            'autocomplete' => array(),
            'name' => array(),
            'value' => array(),
        ),
        'select' => array(
            'id' => array(),
            'class' => array(),
            'data-field' => array(),
            'data-status' => array(),
            'name' => array(),
        ),
        'option' => array(
            'data-type' => array(),
            'class' => array(),
            'value' => array(),
            'selected' => array(),
        ),
        'label' => array(
            'class' => array(),
        ),
    );

    $elements = apply_filters( 'wpsd_kses_search_form_elements_filter', $elements, $content );
    return wp_kses( $content, $elements );
}
?>