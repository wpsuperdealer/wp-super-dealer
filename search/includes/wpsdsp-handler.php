<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action('wp_ajax_wpsdsp_manual_json_build', 'wpsdsp_manual_json_build');
function wpsdsp_manual_json_build() {
	if ( ! is_admin() ) {
		return;
	}
	if ( ! current_user_can( WPSD_ADMIN_CAP ) ) {
		return;
	}
	$nonce = $_POST['nonce'];
	if ( ! wp_verify_nonce( $nonce, 'admin-search_form-nonce' ) ) {
		echo esc_html( __( 'Nonce check failed - no changes saved.', 'wp-super-dealer' ) );
		exit();
	}

    $return = array();
    wpsdsp_delete_cache();
    $return['response'] = wpsdsp_build_cache( true );
    $return['msg'] = __( 'Search JSON Updated', 'wp-super-dealer' );
    $json = json_encode( $return );
    echo wpsd_kses_json( $json );
	exit();
}

function wpsdsp_set_schedule() {
	if ( ! is_admin() ) {
		return;
	}
	if ( ! current_user_can( WPSD_ADMIN_CAP ) ) {
		return;
	}
	$nonce = $_POST['nonce'];
	if ( ! wp_verify_nonce( $nonce, 'admin-search_form-nonce' ) ) {
		echo esc_html( __( 'Nonce check failed - no changes saved.', 'wp-super-dealer' ) );
		exit();
	}

    $return = array();
    
	if ( isset( $_POST['set_wpsdsp_schedule'] ) ) {
		$month = date( 'm' );
		$day = date( 'd' );
		$year = date( 'Y' );
		$sec = '0';
		$hour = sanitize_text_field( $_POST['hour'] );
		$min = sanitize_text_field( $_POST['minute'] );
		$new_time = mktime( $hour, $min, $sec, $month, $day, $year );
		wp_clear_scheduled_hook( 'build_wpsdsp_cache' );
		wp_schedule_event( $new_time, 'daily', 'build_wpsdsp_cache' );
        $return['msg'] = __( 'The build JSON schedule has been updated.', 'wp-super-dealer' );
	}
	if ( isset( $_POST['remove_schedule'] ) ) {
		wp_clear_scheduled_hook('build_wpsdsp_cache');
        $return['msg'] = __( 'The build JSON schedule has been removed.', 'wp-super-dealer' );
	}
    $return['html'] = wpsdsp_get_current_schedule();
    
    $json = json_encode( $return );
    echo wpsd_kses_json( $json );
	exit();
}
add_action( 'wp_ajax_wpsdsp_set_schedule', 'wpsdsp_set_schedule' );
?>