<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsdsp_active_fields() {
	$fields = get_option( 'wpsd-search-fields', array() );
	$fields = apply_filters( 'wpsdsp_active_fields_filter', $fields );
	$cnt = 0;
	
	foreach( $fields as $key=>$data ) {
		if ( ! isset( $data['active'] ) || false === $data['active'] ) {
			unset( $fields[ $key ] );
			continue;
		}
		if ( isset( $data['filter_by'] ) && $data['filter_by'] ) {
			$letter = wpsdsp_get_letter( $cnt );
			$fields[ $key ]['json_column'] = $letter;
			++$cnt;
		}
	}
	return $fields;
}

function wpsdsp_active_field_options() {
	$options = array(
		'label' => array(
			'type' => 'text',
			'tip' => __( 'This is the label you would like to be available for the search form.', 'wp-super-dealer' ),
			'hidden' => false,
		),
		'search_field' => array(
			'type' => 'text',
			'tip' => '',
			'hidden' => true,
			'value_from' => 'search_field',
		),
		'search_field_min' => array(
			'type' => 'text',
			'tip' => '',
			'hidden' => true,
		),
		'search_field_max' => array(
			'type' => 'text',
			'tip' => '',
			'hidden' => true,
		),
		'field_name' => array(
			'type' => 'text',
			'tip' => __( 'This is the field name you want to use for this item.', 'wp-super-dealer' ),
			'hidden' => true,
		),
		'field_type' => array(
			'type' => 'select',
			'tip' => __( 'What type of field is this?', 'wp-super-dealer' ),
			'hidden' => true,
			'options' => array(
				'meta' => 'meta',
				'taxonomy' => 'taxonomy',
			),
		),
		'type' => array(
			'type' => 'select',
			'tip' => __( 'This is the type of form field this item will be displayed as.<br />*NOTE - Range fields must contain ONLY numeric data and will not function properly if non-numeric data is found in this field.', 'wp-super-dealer' ),
			'hidden' => false,
			'options' => array(
				'select' => 'Select Box',
				'checkbox' => 'Checkboxes',
				'range' => 'Range Selector',
			),
		),
		'yes_no' => array(
			'type' => 'checkbox',
			'tip' => __( 'Is this a yes / no field where the values are actually 1 & 0.', 'wp-super-dealer' ),
			'hidden' => true,
		),
		'min' => array(
			'type' => 'text',
			'tip' => __( 'This is the SMALLEST value available in a range search.', 'wp-super-dealer' ),
			'hidden' => true,
		),
		'max' => array(
			'type' => 'text',
			'tip' => __( 'This is the LARGEST value available in a range search.', 'wp-super-dealer' ),
			'hidden' => true,
		),
		'gap' => array(
			'type' => 'text',
			'tip' => __( 'This is the gap between selectable range items. For searching a range of years you could enter a 1 to show each year. If you enter a 5 then it\'ll provide options in blocks of 5.', 'wp-super-dealer' ),
			'hidden' => true,
		),
		'value' => array(
			'type' => 'text',
			'tip' => __( 'Enter the default value you\'d like for your form.', 'wp-super-dealer' ),
			'hidden' => false,
		),
		'dynamic' => array(
			'type' => 'checkbox',
			'tip' => __( 'Should this field be filtered when a filterable field is updated?', 'wp-super-dealer' ),
			'hidden' => false,
		),
		'closed' => array(
			'type' => 'checkbox',
			'tip' => __( 'Should this field be closed when the form first loads?', 'wp-super-dealer' ),
			'hidden' => true,
		),
        'filter_by' => array(
			'type' => 'checkbox',
			'tip' => __( 'Should this field filter any fields marked as dynamic?', 'wp-super-dealer' ),
			'hidden' => false,
		),
		'self_filter' => array(
			'type' => 'checkbox',
			'tip' => __( 'Should this field filter itself? *NOTE: This will only work if this field is dynamic and marked to be filtered by.', 'wp-super-dealer' ),
			'hidden' => true,
		),
		'json_column' => array(
			'type' => 'text',
			'tip' => __( '', 'wp-super-dealer' ),
			'hidden' => true,
		),
	);
	return $options;
}

function wpsdsp_get_letter( $key ) {
	$alphabet = [ 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' ];
	$letter = $alphabet[ $key ];
	return $letter;
}
?>