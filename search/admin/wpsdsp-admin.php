<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsdsp_options_do_page( $echo = false ) {
	if ( ! current_user_can( 'manage_options' ) ) {
		return;
	}
	if ( ! is_admin() ) {
		return;
	}
	$month = date("m");
	$day = date("d");
	$year = date("Y");
	$sec = "0";
	$hour = "6"; // date("H");
	$minute = "01"; //date("i");
	$timestamp = mktime($hour,$minute,$sec,$month,$day,$year);
	
	$settings = wpsd_get_search_settings();
	
	$x = '<div class="wpsd-search-settings-wrap">';
		$x .= '<div class="wpsd_welcome_tab_title active" data-tab="wpsd_settings">';
			$x .= __('General Settings', 'wp-super-dealer');
		$x .= '</div>';
		$x .= '<div class="wpsd_welcome_tab_title" data-tab="wpsd_custom_fields">';
			$x .= __('Search Fields', 'wp-super-dealer');
		$x .= '</div>';

		$x .= '<div class="wpsd_welcome_clear"></div>';

		$x .= '<div class="wpsd_welcome_tab active" id="wpsd_settings">';
			$x .= '<h3>';
				$x .= __('I. Update your json file', 'wp-super-dealer');
			$x .= '</h3>';

			$x .= '<input type="button" class="wpsdsp-update-json-button" name="build_cache_now" value="' . __( 'Build json file now', 'wp-super-dealer' ) . '">';
            $x .= '<div class="wpsdsp-update-json-msg"></div>';
    
			$x .= '<br />';
			$x .= '<small>';
				$x .= __( 'You can manually update your search form\'s json file by clicking the button above.', 'wp-super-dealer' );
			$x .= '</small>';
			$x .= '<br />';
			$x .= '<small>';
			     $x .= __('If you are using a third party CDN or cache PlugIn then please make sure it is set to update this file daily.','wp-super-dealer');
			$x .= '</small>';
			$x .= '<hr />';


			//= Schedule Options
			$x .=  '<h3>';
				$x .= __( 'II. You can set a schedule here to automatically update your json file daily.', 'wp-super-dealer' );
			$x .= '</h3>';

			if ( defined( 'CDII_VER' ) ) {
				$x .= '<br />';
				$x .= '<small style="font-weight:700;color:#b00;">';
					$x .= __( 'If you have the WPSuperDealer Inventory Import and it is scheduled to import then you do NOT need to schedule the cache to be update. It will update when the import runs.', 'wp-super-dealer' );
				$x .= '</small>';
				$x .= '<br />';
			}
			$x .=  __( 'Time:', 'wp-super-dealer' );
			$x .= ' <select name="hour" class="wpsdsp-schedule-hour">';
				$i = 1;
				do {
					$x .=  '<option value="' . $i . '">' . $i . '</option>';
					++$i;
				} while ( $i < 25 );
			$x .=  '</select>';
			$x .=  '&nbsp;';
			$x .= '<select name="minute" class="wpsdsp-schedule-minute">';
				$x .= '<option value="00">00</option>';
				$i = 10;
				do {
					$x .= '<option value="' . $i . '">' . $i . '</option>';
					$i = $i + 1;
				} while ( $i < 60 );
			$x .= '</select>';
			$x .= '<input type="hidden" name="set_wpsdsp_schedule" value="1" />';
			$x .= '<input type="button" class="wpsdsp-add-schedule-button" name="wpsdsp-add-schedule-button" value="'.__('Set json build time', 'wp-super-dealer').'" />';

            $x .= '<div class="wpsdsp-update-json-schedule-msg"></div>';

            $x .= '<div class="wpsdsp-json-current-schedule-wrap">';
                $x .= wpsdsp_get_current_schedule();
            $x .= '</div>';
    
            $x .= '<br />';
			$x .= '<label class="wpsdsp-email-report-label">';
				$x .= __( 'Enter an email address to receive an alert when the json file is updated.', 'wp-super-dealer' );
			$x .= '</label>';
			$x .= '<br />';
			$x .= '<hr />';

			$x .= '<input name="search[email_report]" type="text" class="wpsdsp-email-report" placeholder="' . __( 'Email address for report', 'wp-super-dealer' ) . '" value="' . $settings['email_report'] . '" />';
			$x .= '<br />';
			$x .= '<small>';
				$x .= __( 'Leave blank to disable', 'wp-super-dealer' );
			$x .= '</small>';
			$x .=  '<hr />';

			$x .=  '<h3>'.__('III. Load json each time page is loaded:', 'wp-super-dealer').'</h3>';
			$x .=  '<blockquote>';
				$x .=  '<select id="save_json" name="search[save_json]">';
					$x .=  '<option value="1"' . ( '1' === $settings['save_json'] ? ' selected' : '' ) . '>Yes</option>';
					$x .=  '<option value="0"' . ( '0' === $settings['save_json'] ? ' selected' : '' ) . '>No</option>';
				$x .=  '</select>';
			$x .=  '</blockquote>';
			$x .= '<small>';
				$x .= __("Your json file will be cached for 24 hours unless you set this value to 'yes'.", "wp-super-dealer/search");
				$x .= '<br />';
				$x .= __("When set to 'yes' this will force your visitors to download a fresh copy of the json file each time they reload the page.", "wp-super-dealer/search");
				$x .= '<br />';
				$x .= __("Only set this value to 'yes' if you need to force visitors to update due to a recent manual change.", "wp-super-dealer/search");
			$x .= '</small>';
			$x .= '<hr />';

			$x .=  '<h3>'.__('IV. Hide Item Counts:', 'wp-super-dealer').'</h3>';
			$x .=  '<blockquote>';
				$x .=  '<select id="wpsdsp_hide_count_val" name="search[hide_count]">';
					$x .=  '<option value="on"' . ( 'on' === $settings['hide_count'] ? ' selected' : '' ) . '>' . __( 'Yes', 'wp-super-dealer' ) . '</option>';
					$x .=  '<option value="off"' . ( 'on' !== $settings['hide_count'] ? ' selected' : '' ) . '>' . __( 'No', 'wp-super-dealer' ) . '</option>';
				$x .=  '</select>';
			$x .=  '</blockquote>';
			$x .= '<hr />';

			$x .=  '<h3>'.__('V. Autoload inventory when filter selected:', 'wp-super-dealer').'</h3>';
			$x .=  '<blockquote>';
				$x .=  '<select id="wpsdsp_autoload" name="search[autoload]">';
					$x .=  '<option value="0"' . ( '0' === $settings['autoload'] ? ' selected' : '' ) . '>No</option>';
					$x .=  '<option value="1"' . ( '1' === $settings['autoload'] ? ' selected' : '' ) . '>Yes</option>';
				$x .=  '</select>';
			$x .=  '</blockquote>';
			$x .= '<small>';
				$x .= __( 'If checked then any inventory section on the page will auto filter based on selected values.', 'wp-super-dealer' );
			$x .= '</small>';
			$x .= '<hr />';

			$x .=  '<h3>'.__('VI. Mobile "tap" action:', 'wp-super-dealer').'</h3>';
			$x .=  '<blockquote>';
				$x .=  '<input type="text" name="search[mobile_action]" value="' . esc_attr( $settings['mobile_action'] ) . '" /> ';
			$x .=  '</blockquote>';
			$x .= '<small>';
				$x .= __( 'This is the browser event to trigger mobile actions. Enter a value like: "tap" or "touchend" or "tap click", etc.', 'wp-super-dealer' );
				$x .= '<br />';
				$x .= __( 'This setting is provided for legacy systems where the "tap" event may not be available.', 'wp-super-dealer' );
			$x .= '</small>';
			$x .= '<hr />';
		$x .= '</div>';

		$x .= '<div class="wpsd_welcome_tab" id="wpsd_custom_fields">';
			$x .='<h3>';
				$x .= __('Add Search Fields', 'wp-super-dealer');
			$x .= '</h3>';
			$x .= '<div class="">';
				$x .= __( 'The following custom vehicle specification fields can be added to your form.', 'wp-super-dealer' );
				$x .= '<br />';
				$x .= '<br />';
				$x .= '<b>';
					$x .= __( 'NOTE: The more fields you add the slower your forms will update when values are selected, the longer it will take to update your json file and the larger it will be. The take away here is that you should only add fields you really need.', 'wp-super-dealer' );
					$x .= '<br />';
					$x .= '<br />';
					$x .= __( 'Fields can be removed on a per form basis using shortcode parameters or widget options.', 'wp-super-dealer' );
				$x .= '</b>';
			$x .= '</div>';
			$x .= '<div class="wpsdsp_admin_msg"></div>';
			$x .= wpsdsp_fields_management();
			$x .= '<div class="wpsdsp_admin_msg"></div>';
		$x .= '</div>';

	$x .= '</div>';
	
	if ( $echo ) {
		echo wpsd_kses_admin( $x );
	} else {
		return $x;
	}
}

function wpsdsp_fields_management() {
	$html = '<div class="wpsd_gap"></div>';
	$wpsdsp_custom_fields = wpsd_get_search_fields();
	$specs = wpsd_get_all_specifications();
    
    $specs['type'] = __( 'Vehicle Type', 'wp-super-dealer' );

	$active_field_options = wpsdsp_active_field_options();

	foreach( $specs as $spec=>$name ) {
		$slug = sanitize_title( $spec );
		$_slug = str_replace( '-', '_', $slug );
		$html .= '<div class="wpsdsp_custom_fields ' . esc_attr( $_slug ) . '-wrap" data-field-slug="' . esc_attr( $_slug ) . '">';

			$html .= '<div class="wpsdsp_field_options_lanel">';
				$html .= '<span class="wpsdsp_open_admin_field wpsdsp_open_admin_field_' . esc_attr( $_slug ) . '" data-field-slug="' . esc_attr( $_slug ) . '">+</span>';
				$html .= trim( $name );
				$checked = ( isset( $wpsdsp_custom_fields[ $_slug ]['active']  ) && $wpsdsp_custom_fields[ $_slug ]['active'] ? ' checked' : '' );
				$html .= '<span class="wpsdsp_activate_custom_field_wrap">';
					$html .= '<input type="checkbox" name="field[' . esc_attr( $_slug ) . '][active]" class="wpsdsp_activate_custom_field wpsdsp_activate_' . esc_attr( $_slug ) . '" data-field="' . esc_attr( trim( $name ) ) . '" data-field-slug="' . esc_attr( $_slug ) . '" value="1"' . esc_attr( $checked ) . ' />';
				$html .= '</span>';
			$html .= '</div>';

			$html .= '<span class="custom_field_options_wrap ' . esc_attr( $_slug ) . '">';
				$args = array(
					'field' => $_slug,
					'wpsdsp_custom_fields' => $wpsdsp_custom_fields,
					'active_field_options' => $active_field_options,
				);
				$field_options = wpsdsp_custom_field_html( $args );
				$html .= $field_options;

				$html .= '<div class="wpsdsp_advanced" data-field="' . esc_attr( $_slug ) . '">';
					$html .= __( 'advanced', 'wp-super-dealer' );
				$html .= '</div>';
				$html .= '<div class="wpsd-clear"></div>';

			$html .= '</span>';
		$html .= '</div>';
	}
	
	return $html;
}

function wpsdsp_custom_field_html( $args ) {
	$html = '';

	$field = $args['field'];
	$wpsdsp_custom_fields = $args['wpsdsp_custom_fields'];
	$active_field_options = $args['active_field_options'];

	foreach( $active_field_options as $key=>$data ) {
		$value = '';
		if ( isset( $wpsdsp_custom_fields[ $field ][ $key ] ) ) {
			$value = $wpsdsp_custom_fields[ $field ][ $key ];
		}
		
		if ( empty( $value ) ) {
			if ( isset( $data['value'] ) ) {
				$value = trim( $data['value'] );
			}
		}

		if ( empty( $value ) ) {
			if ( 'label' === $key ) {
				$value = wpsdsp_deslugify( $field );
			}
			if ( 'field_name' === $key ) {
				$value = sanitize_title( $field );
				$value = str_replace( '-', '_', $value );
			}
			if ( 'search_field' === $key ) {
				$value = sanitize_title( $field );
				$value = str_replace( '-', '_', $value );
				$value = $value;
			}
		}

        if ( isset( $wpsdsp_custom_fields[ $field ]['type'] ) && 'range' === $wpsdsp_custom_fields[ $field ]['type'] ) {
            if ( 'search_field_min' === $key ) {
                if ( empty( $value ) ) {
                    $value = $field . '_min';
                }
            }
            if ( 'search_field_max' === $key ) {
                if ( empty( $value ) ) {
                    $value = $field . '_max';
                }
            }
        }

		$hidden = '';
		if ( isset( $data['hidden'] ) && $data['hidden'] ) {
			$hidden = ' wpsdsp_hidden';
		}

		if ( 'range' === $data['type'] ) {
			if ( 'min' === $key || 'max' === $key || 'gap' === $key ) {
				$hidden = '';
			}
		}

		$html .= '<div class="wpsdsp_custom_field  wpsdsp_custom_parent_field_' . esc_attr( $field ) . ' wpsdsp_custom_field_' . esc_attr( $key . $hidden ) . '" data-field="' . esc_attr( $key ) . '">';

			if ( isset( $data['tip'] ) ) {
				$html .= '<span class="wpsdsp_tip_wrap">';
					$html .= '<span class="wpsdsp_tip_label">';
						$html .= '?';
					$html .= '</span>';
					$html .= '<span class="wpsdsp_tip">';
						$html .= esc_html( $data['tip'] );
					$html .= '</span>';
				$html .= '</span>';
			}
			if ( 'text' === $data['type'] ) {
				$html .= '<label>';
					$html .= wpsdsp_deslugify( $key );
				$html .= '</label>';
				$html .= '<div class="wpsdsp_data_field_wrap">';
					$html .= '<input type="text" name="field[' . $args['field'] . '][' . sanitize_title( $key ) . ']" class="wpsdsp_cf ' . sanitize_title( $key ) . '-value" data-type="text" data-parent-field="' . sanitize_title( $field ) . '" data-field="' . esc_attr( $key ) . '" value="' . esc_attr( $value ) . '" />';
				$html .= '</div>';
			} else if ( 'select' === $data['type'] ) {
				$html .= '<label>';
					$html .= wpsdsp_deslugify( $key );
				$html .= '</label>';
				$html .= '<div class="wpsdsp_data_field_wrap">';
					$html .= '<select name="field[' . $args['field'] . '][' . sanitize_title( $key ) . ']" class="wpsdsp_cf wpsdsp_cf_select_' . esc_attr( $key ) . ' ' . sanitize_title( $key ) . '-value" data-type="select" data-parent-field="' . sanitize_title( $field ) . '" data-field="' . esc_attr( $key ) . '">';
						if ( isset( $data['options'] ) && is_array( $data['options'] ) ) {
							foreach( $data['options'] as $option=>$label ) {
								$html .= '<option value="' . esc_attr( $option ) . '"' . ( $value === $option ? ' selected' : '' ) . '>' . esc_html( $label ) . '</option>';
							}
						}
					$html .= '</select>';
				$html .= '</div>';
			} else if ( 'range' === $data['type'] ) {
				$html .= '<label>';
					$html .= wpsdsp_deslugify( $key );
				$html .= '</label>';
				$html .= '<div class="wpsdsp_data_field_wrap">';
					$html .= '<input type="" name="field[' . esc_attr( $args['field'] ) . '][' . sanitize_title( $key ) . ']" class="wpsdsp_cf ' . sanitize_title( $key ) . '-value" data-type="range" data-parent-field="' . sanitize_title( $field ) . '" data-field="' . esc_attr( $key ) . '" value="' . esc_attr( $value ) . '" />';
				$html .= '</div>';
			} else if ( 'checkbox' === $data['type'] ) {
				$html .= '<label>';
					$html .= wpsdsp_deslugify( $key );
				$html .= '</label>';
				$html .= '<div class="wpsdsp_data_field_wrap">';
					$html .= '<input type="checkbox" name="field[' . $args['field'] . '][' . sanitize_title( $key ) . ']" ' . ( $value ? 'checked="checked" ' : '' ) . ' class="wpsdsp_cf ' . sanitize_title( $key ) . '-value" data-type="checkbox" data-parent-field="' . sanitize_title( $field ) . '" data-field="' . esc_attr( $key ) . '" value="1" />' . __( 'Yes', 'wp-super-dealer' );
				$html .= '</div>';
			}

		$html .= '</div>';
	}
	
	return $html;
}

function wpsdsp_get_current_schedule() {
    $x = '';
    $x .= '<div class="wpsdsp-json-current-schedule">';
        $x .= '<div class="wpsdsp-json-server-time">';
            $x .= __( 'Current Server Time:', 'wp-super-dealer' ).' ';
            $blogtime = current_time( 'mysql' );
            //list( $today_year, $today_month, $today_day, $hour, $minute, $second ) = split( '([^0-9])', $blogtime );
            $x .= $blogtime.'<br />';
        $x .= '</div>';
        $timestamp = wp_next_scheduled( 'build_wpsdsp_cache' );
        if ( ! empty( $timestamp ) ) {
            $x .= '<br />';
            $x .= __( 'Next Scheduled:', 'wp-super-dealer' ) . ' ' . date( 'Y-m-d H:i', $timestamp );
            $x .= '<br />';
            $x .= '<input type="hidden" name="remove_schedule" value="1" />';
            $x .= '<input type="button" class="wpsdsp-remove-schedule-button" name="wpsdsp-remove-schedule-button" value="' . __('Remove Schedule', 'wp-super-dealer') . '" />';
        } else {
            $x .= '<br />';
            $x .= '<b>';
                $x .= __( 'Schedule has not been set to auto update your json file.', 'wp-super-dealer' );
            $x .= '</b>';
        }
    $x .= '</div>';
    return $x;
}

function wpsdsp_deslugify( $slug, $all_caps = false ) {
	$slug = str_replace( '-', ' ', $slug );
	$slug = str_replace( '_', ' ', $slug );
	
	if ( $all_caps ) {
		$slug = strtoupper( $slug );
	} else {
		$slug = ucwords( $slug );
	}
	
	return $slug;
}

function wpsdsp_schedule() {
	wpsdsp_delete_cache();
	wpsdsp_build_cache();
}
add_action( 'build_wpsdsp_cache', 'wpsdsp_schedule' );

function wpsdsp_email_report( $msg ) {
	$wpsdsp_email_report = get_option( 'wpsdsp-email-report', '' );
	if ( empty( $wpsdsp_email_report ) ) {
		return;
	}
	$to = $wpsdsp_email_report;
	// Always set content-type when sending HTML email
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
	// More headers
	$headers .= 'From: <' . $to . '>' . "\r\n";

	$subject = "Vehicle Search json Updated for " . get_bloginfo( 'name' );

	wp_mail( $to, $subject, $msg, $headers );	
}
?>