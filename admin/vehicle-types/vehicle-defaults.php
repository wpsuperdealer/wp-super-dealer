<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_get_vehicle_map_default() {
	$map = array();
	$map = get_option( 'wpsd_vehicle_option_map' );
	if ( ! isset( $map[ 'description'] ) ) {
		$map[ 'description'] = wpsd_get_default_description_maps();
	}
	if ( ! isset( $map[ 'specs'] ) ) {
		$map[ 'specs'] = wpsd_get_default_specs_maps();
	}
	if ( ! isset( $map[ 'safety'] ) ) {
		$map[ 'safety'] = wpsd_get_default_safety_maps();
	}
	if ( ! isset( $map[ 'convenience'] ) ) {
		$map[ 'convenience'] = wpsd_get_default_convenience_maps();
	}
	if ( ! isset( $map[ 'comfort'] ) ) {
		$map[ 'comfort'] = wpsd_get_default_comfort_maps();
	}
	if ( ! isset( $map[ 'entertainment'] ) ) {
		$map[ 'entertainment'] = wpsd_get_default_entertainment_maps();
	}
	if ( ! isset( $map[ 'about_us'] ) ) {
		$map[ 'about_us'] = wpsd_get_default_about_us_maps();
	}

	$map = apply_filters( 'vehicle_options_map_filter', $map );
	return $map;
}

function wpsd_get_default_description_maps() {
	$map = array();
	return $map;
}

function wpsd_get_default_specs_maps() {
	$map = array();
	$map[ __( 'Specifications', 'wp-super-dealer' ) ] = __( 'Trim Level, Production Seq. Number, Exterior Color, Interior Color, Manufactured in, Engine Type, Transmission, Driveline, Tank (gallon),Fuel Economy (City miles/gallon),Fuel Economy (Highway miles/gallon),Anti-Brake System,Steering Type,Length (in.),Width (in.),Height (in.)','wp-super-dealer' );
	return $map;
}

function wpsd_get_default_safety_maps() {
	$map = array();
	$map[ __( 'Anti-Theft & Locks','wp-super-dealer' ) ] = __( 'Child Safety Door Locks,Locking Pickup Truck Tailgate,Power Door Locks,Vehicle Anti-Theft', 'wp-super-dealer' );
	$map[ __( 'Braking & Traction','wp-super-dealer' ) ] = __( '4WD/AWD,ABS(2-Wheel/4-Wheel),Automatic Load-Leveling,Electronic Brake Assistance,Limited Slip Differential,Locking Differential,Traction Control,Vehicle Stability Control System', 'wp-super-dealer' );
	$map[ __( 'Safety','wp-super-dealer' ) ] = __( 'Driver Airbag,Front Side Airbag,Front Side Airbag with Head Protection,Passenger Airbag,Side Head Curtain Airbag,Second Row Side Airbag,Second Row Side Airbag with Head Protection,Electronic Parking Aid,First Aid Kit,Trunk Anti-Trap Device', 'wp-super-dealer' );
	return $map;
}

function wpsd_get_default_convenience_maps() {
	$map = array();
	$map[ __( 'Remote Controls & Release','wp-super-dealer' ) ] = __( 'Keyless Entry,Remote Ignition', 'wp-super-dealer' );
	$map[ __( 'Interior Features','wp-super-dealer' ) ] = __( 'Cruise Control,Tachometer,Tilt Steering Wheel,Tilt Steering Column,Heated Steering Wheel,Leather Steering Wheel,Steering Wheel Mounted Controls,Telescopic Steering Column,Adjustable Foot Pedals,Genuine Wood Trim,Tire Inflation/Pressure Monitor,Trip Computer', 'wp-super-dealer' );
	$map[ __( 'Storage','wp-super-dealer' ) ] = __( 'Cargo Area Cover,Cargo Area Tiedowns,Cargo Net,Load Bearing Exterior Rack,Pickup Truck Bed Liner', 'wp-super-dealer' );
	$map[ __( 'Roof','wp-super-dealer' ) ] = __( 'Power Sunroof/Moonroof,Manual Sunroof/Moonroof,Removable/Convertible Top', 'wp-super-dealer' );
	$map[ __( 'Climate Control','wp-super-dealer' ) ] = __( 'Air Conditioning,Separate Driver/Front Passenger Climate Controls', 'wp-super-dealer' );
	return $map;
}

function wpsd_get_default_comfort_maps() {
	$map = array();
	$map[ __( 'Seat','wp-super-dealer' ) ] = __( 'Driver Multi-Adjustable Power Seat,Front Cooled Seat,Front Heated Seat,Front Power Lumbar Support,Front Power Memory Seat,Front Split Bench Seat,Leather Seat,Passenger Multi-Adjustable Power Seat,Second Row Folding Seat,Second Row Heated Seat,Second Row Multi-Adjustable Power Seat,Second Row Removable Seat,Third Row Removable Seat', 'wp-super-dealer' );
	$map[ __( 'Exterior Lighting','wp-super-dealer' ) ] = __( 'Automatic Headlights,Daytime Running Lights,Fog Lights,High Intensity Discharge Headlights,Pickup Truck Cargo Box Light', 'wp-super-dealer' );
	$map[ __( 'Exterior Features','wp-super-dealer' ) ] = __( 'Bodyside/Cab Step or Running Board,Front Air Dam,Rear Spoiler,Skid Plate or Underbody Protection,Splash Guards,Wind Deflector or Buffer for Convertible,Power Sliding Side Van Door,Power Trunk Lid', 'wp-super-dealer' );
	$map[ __( 'Wheels','wp-super-dealer' ) ] = __( 'Alloy Wheels,Chrome Wheels,Steel Wheels', 'wp-super-dealer' );
	$map[ __( 'Tires','wp-super-dealer' ) ] = __( 'Full Size Spare Tire,Run Flat Tires', 'wp-super-dealer' );
	$map[ __( 'Windows','wp-super-dealer' ) ] = __( 'Power Windows,Glass Rear Window on Convertible,Sliding Rear Pickup Truck Window', 'wp-super-dealer' );
	$map[ __( 'Mirrors','wp-super-dealer' ) ] = __( 'Electrochromic Exterior Rearview Mirror,Heated Exterior Mirror,Electrochromic Interior Rearview Mirror,Power Adjustable Exterior Mirror', 'wp-super-dealer' );
	$map[ __( 'Wipers','wp-super-dealer' ) ] = __( 'Interval Wipers,Rain Sensing Wipers,Rear Wiper,Rear Window Defogger', 'wp-super-dealer' );
	$map[ __( 'Towings','wp-super-dealer' ) ] = __( 'Tow Hitch Receiver,Towing Preparation Package', 'wp-super-dealer' );
	return $map;
}

function wpsd_get_default_entertainment_maps() {
	$map = array();
	$map[ __( 'Entertainment, Communication & Navigation','wp-super-dealer' ) ] = __( 'AM/FM Radio,Cassette Player,CD Player,CD Changer,DVD Player,Hands Free/Voice Activated Telephone,Navigation Aid,Second Row Sound Controls or Accessories,Subwoofer,Telematic Systems', 'wp-super-dealer' );
	return $map;
}

function wpsd_get_default_about_us_maps() {
	$map = array();
	return $map;
}
?>