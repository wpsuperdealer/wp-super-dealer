<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

require_once( 'vehicle-defaults.php' );
require_once( 'vehicle-type-maps.php' );

function wpsd_get_all_specifications( $sort = false, $taxonomies_only = false, $include_global = true, $all_data = false ) {
	$specs = array();
	
	//= Include global fields if needed
	if ( $include_global ) {
		$global_fields = wpsd_get_global_fields();
		foreach( $global_fields as $spec=>$options ) {
			$slug = sanitize_title( $spec );
			$_slug = str_replace( '-', '_', $slug );
			$label = str_replace( '_', ' ', $_slug );
			$label = ucwords( $label );
			if ( $taxonomies_only ) {
				if ( isset( $options['taxonomy'] ) && $options['taxonomy'] ) {
					if ( $all_data ) {
						$specs[ $_slug ] = $options;
					} else {
						$specs[ $_slug ] = $label;
					}
				}
				continue;
			}
			if ( $all_data ) {
				if ( ! isset( $options['label'] ) || empty( $options['label'] ) ) {
					$options['label'] = $label;
				}
				$specs[ $_slug ] = $options;
			} else {
				$specs[ $_slug ] = $label;
			}
		}
	}
	
	//= Get specs for each vehicle type
	$types = wpsd_get_type_maps();
	//= Loop each vehicle type
	foreach( $types as $type=>$data ) {
		//= Does this vehicle type have an array of specifications?
		if ( isset( $data[ __( 'specifications', 'wp-super-dealer' ) ] ) ) {
			//= Make sure it's an array of data
			if ( is_array( $data[ __( 'specifications', 'wp-super-dealer' ) ] ) ) {
				//= Create a variable and store all the spec groups to it
				$spec_groups = $data[ __( 'specifications', 'wp-super-dealer' ) ];
				//= Loop all of the spec groups
				foreach( $spec_groups as $group=>$spec_data ) {
					if ( is_array( $spec_data ) ) {
						//= Loop though all the specifications and add them to our array
						foreach( $spec_data as $spec=>$options ) {
							$slug = sanitize_title( $spec );
							$_slug = str_replace( '-', '_', $slug );
							$label = str_replace( '_', ' ', $_slug );
							$label = ucwords( $label );
							if ( $taxonomies_only ) {
								if ( isset( $options['taxonomy'] ) && $options['taxonomy'] ) {
									if ( $all_data ) {
										if ( ! isset( $options['label'] ) || empty( $options['label'] ) ) {
											$options['label'] = $label;
										}
										$specs[ $_slug ] = $options;
									} else {
										$specs[ $_slug ] = $label;
									}
								}
								continue;
							}
							if ( $all_data ) {
								if ( ! isset( $options['label'] ) || empty( $options['label'] ) ) {
									$options['label'] = $label;
								}
								$specs[ $_slug ] = $options;
							} else {
								$specs[ $_slug ] = $label;
							}
						} //= foreach( $spec_data as $spec=>$options ) {
					} //= end if ( is_array
				} //= end foreach( $spec_groups as $group=>$spec_data ) {
			}
		} //= end if ( isset( $data[ __( 'specifications', 'wp-super-dealer' ) ] ) ) {
	} //= end foreach( $types as $type=>$data )

	//= Sort it alphabetically if needed
	if ( $sort ) {
		asort( $specs );
	}
	return $specs;
}

function wpsd_get_all_options( $sort = false) {
	$options = array();
	$types = wpsd_get_type_maps();
	//= Loop each vehicle type
	foreach( $types as $type=>$data ) {
		//= Does this vehicle type have an array of options?
		if ( isset( $data[ __( 'options', 'wp-super-dealer' ) ] ) ) {
			//= Make sure it's an array of data
			if ( is_array( $data[ __( 'options', 'wp-super-dealer' ) ] ) ) {
				//= Create a variable and store all the option groups to it
				$option_groups = $data[ __( 'options', 'wp-super-dealer' ) ];
				//= Loop all of the option groups
				foreach( $option_groups as $group=>$option_data ) {
					if ( is_array( $option_data ) ) {
						//= Loop though all the options and add them to our array
						foreach( $option_data as $option=>$option_sub_group ) {
							if ( false !== strpos( $option_sub_group, ',' ) ) {
								$option_array = explode( ',', $option_sub_group );
								foreach( $option_array as $key=>$option_item ) {
									$options[] = $option_item;
								}
							} else {
								$options[] = $option;
							}
						} //= foreach( $option_data as $option=>$options ) {
					} //= end if ( is_array
				} //= end foreach( $option_groups as $group=>$option_data ) {
			}
		} //= end if ( isset( $data[ __( 'options', 'wp-super-dealer' ) ] ) ) {
	} //= end foreach( $types as $type=>$data )

	//= Sort it alphabetically if needed
	if ( $sort ) {
		asort( $options );
	}
	return $options;
}

function wpsd_get_vehicle_type( $post_id ) {
	$vehicle_type = get_post_meta( $post_id, '_type', true );
	if ( empty( $vehicle_type ) ) {
		$maps = wpsd_get_type_maps();
		if ( 1 > count( $maps ) ) {
			$maps = wpsd_default_type_maps();
		}
		foreach( $maps as $type=>$map ) {
			$vehicle_type = $type;
			break;
		}
	}
	return $vehicle_type;
}

?>