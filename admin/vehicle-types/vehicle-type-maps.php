<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_get_type_maps() {
	$default_types = wpsd_default_type_maps();

	//= delete_option( 'wpsd-vehicle-types' );
	$types = get_option( 'wpsd-vehicle-types', $default_types );
	$types = apply_filters( 'wpsd_types_filter', $types );
	return $types;
}

function wpsd_default_type_maps() {
	$default_vehicle_map = wpsd_get_vehicle_map_default();
	$url = WP_PLUGIN_URL . '/wp-super-dealer/images/';

	$types = array(
		__( 'automobile', 'wp-super-dealer' ) => array( 
			__( 'label', 'wp-super-dealer' ) => __( 'Automobiles', 'wp-super-dealer' ),
			__( 'icon', 'wp-super-dealer' ) => $url . 'car-icon.jpg',
			__( 'specifications', 'wp-super-dealer' ) => wpsd_default_vehicle_specs(),
			__( 'options', 'wp-super-dealer' ) => array(
				__( 'safety', 'wp-super-dealer' ) => $default_vehicle_map['safety'],
				__( 'convenience', 'wp-super-dealer' ) => $default_vehicle_map['convenience'],
				__( 'comfort', 'wp-super-dealer' ) => $default_vehicle_map['comfort'],
				__( 'entertainment', 'wp-super-dealer' ) => $default_vehicle_map['entertainment'],
			)
		),
		__( 'semi-truck', 'wp-super-dealer' ) => array(
			__( 'label', 'wp-super-dealer' ) => __( 'Semi-Trucks', 'wp-super-dealer' ),
			__( 'icon', 'wp-super-dealer' ) => $url . 'truck-icon.jpg',
			__( 'specifications', 'wp-super-dealer' ) => wpsd_default_vehicle_specs(),
			__( 'options', 'wp-super-dealer' ) => array(
				__( 'default', 'wp-super-dealer' ) => array( 
					__( 'default', 'wp-super-dealer' ) => 
						__( 'Child Safety Door Locks,Locking Pickup Truck Tailgate,Power Door Locks,Vehicle Anti-Theft,
						4WD/AWD,ABS(2-Wheel/4-Wheel),Automatic Load-Leveling,Electronic Brake Assistance,Limited Slip Differential,
						Locking Differential,Traction Control,Vehicle Stability Control System,Driver Airbag,Front Side Airbag,
						Front Side Airbag with Head Protection,Passenger Airbag,Side Head Curtain Airbag,Second Row Side Airbag,
						Second Row Side Airbag with Head Protection,Electronic Parking Aid,First Aid Kit,Trunk Anti-Trap Device,
						Keyless Entry,Remote Ignition,Cruise Control,Tachometer,Tilt Steering Wheel,Tilt Steering Column,
						Heated Steering Wheel,Leather Steering Wheel,Steering Wheel Mounted Controls,Telescopic Steering Column,
						Adjustable Foot Pedals,Genuine Wood Trim,Tire Inflation/Pressure Monitor,Trip Computer,Cargo Area Cover,
						Cargo Area Tiedowns,Cargo Net,Load Bearing Exterior Rack,Pickup Truck Bed Liner,Power Sunroof/Moonroof,
						Manual Sunroof/Moonroof,Removable/Convertible Top,Air Conditioning,Separate Driver/Front Passenger Climate Controls,
						Driver Multi-Adjustable Power Seat,Front Cooled Seat,Front Heated Seat,Front Power Lumbar Support,
						Front Power Memory Seat,Front Split Bench Seat,Leather Seat,Passenger Multi-Adjustable Power Seat,
						Second Row Folding Seat,Second Row Heated Seat,Second Row Multi-Adjustable Power Seat,Second Row Removable Seat,
						Third Row Removable Seat,Automatic Headlights,Daytime Running Lights,Fog Lights,High Intensity Discharge Headlights,
						Pickup Truck Cargo Box Light,Bodyside/Cab Step or Running Board,Front Air Dam,Rear Spoiler,
						Skid Plate or Underbody Protection,Splash Guards,Wind Deflector or Buffer for Convertible,Power Sliding Side Van Door,Power Trunk Lid,
						Alloy Wheels,Chrome Wheels,Steel Wheels,Full Size Spare Tire,Run Flat Tires,Power Windows,
						Glass Rear Window on Convertible,Sliding Rear Pickup Truck Window,Electrochromic Exterior Rearview Mirror,
						Heated Exterior Mirror,Electrochromic Interior Rearview Mirror,Power Adjustable Exterior Mirror,Interval Wipers,
						Rain Sensing Wipers,Rear Wiper,Rear Window Defogger,Tow Hitch Receiver,Towing Preparation Package
						AM/FM Radio,Cassette Player,CD Player,CD Changer,DVD Player,Hands Free/Voice Activated Telephone,
						Navigation Aid,Second Row Sound Controls or Accessories,Subwoofer,Telematic Systems', 'wp-super-dealer' )
				),
			),
		),
		__( 'boat', 'wp-super-dealer' ) => array(
			__( 'label', 'wp-super-dealer' ) => __( 'Boats', 'wp-super-dealer' ),
			__( 'icon', 'wp-super-dealer' ) => $url . 'boat-icon.jpg',
			__( 'specifications', 'wp-super-dealer' ) => array(
				__( 'Specifications', 'wp-super-dealer' ) => array(
					__( 'Class', 'wp-super-dealer' ) => array(),
                    __( 'Length', 'wp-super-dealer' ) => array(),
					__( 'Beam', 'wp-super-dealer' ) => array(),
                    __( 'Max Bridge Clearance', 'wp-super-dealer' ) => array(),
					__( 'Propulsion Type', 'wp-super-dealer' ) => array(),
					__( 'Hull Material', 'wp-super-dealer' ) => array(),
                    __( 'Hull Shape', 'wp-super-dealer' ) => array(),
					__( 'Fuel Type', 'wp-super-dealer' ) => array(),
                    __( 'Fuel Tank', 'wp-super-dealer' ) => array(),
                    __( 'Engine Hours', 'wp-super-dealer' ) => array(),
					__( 'Engine Make', 'wp-super-dealer' ) => array(),
					__( 'Engine Year', 'wp-super-dealer' ) => array(),
					__( 'Total Power', 'wp-super-dealer' ) => array(),
					__( 'Engine Hours', 'wp-super-dealer' ) => array(),
					__( 'Engine Type', 'wp-super-dealer' ) => array(),
                    __( 'Cruising Speed', 'wp-super-dealer' ) => array(),
                    __( 'Max Speed', 'wp-super-dealer' ) => array(),
                    __( 'Fresh Water Tanks', 'wp-super-dealer' ) => array(),
				),
			),
			__( 'options', 'wp-super-dealer' ) => array(
				__( 'default', 'wp-super-dealer' ) => array( 
					__( 'default', 'wp-super-dealer' ) => __( 'VP Package,Bimini Top,Mooring Cover,Upgraded Captain\'s Seats,Vinyl Flooring,Dinette Table,In Wall LED Navigation and Docking Lights,In Floor Storage,
						LED Interior Courtesy Lighting,Depth Finder,Stainless Steel Ski Tow,Bluetooth Capable Stereo,Battery Switch,Mooring Cover,Docking Lights,Power Engine Hatch,
						Captains Call,Trailer,Bow Speakers', 'wp-super-dealer' )
				),
			)
		),
		__( 'bus', 'wp-super-dealer' ) => array(
			__( 'label', 'wp-super-dealer' ) => __( 'Buses', 'wp-super-dealer' ),
			__( 'icon', 'wp-super-dealer' ) => $url . 'bus-icon.jpg',
			__( 'specifications', 'wp-super-dealer' ) => array( 
				__( 'Specifications', 'wp-super-dealer' ) => array(
                    __( 'Passengers', 'wp-super-dealer' ) => array(),
                    __( 'Luggage', 'wp-super-dealer' ) => array(),
                    __( 'Length', 'wp-super-dealer' ) => array(),
                    __( 'Width', 'wp-super-dealer' ) => array(),
                    __( 'Height', 'wp-super-dealer' ) => array(),
                    __( 'Interior width', 'wp-super-dealer' ) => array(),
                    __( 'Interior Height', 'wp-super-dealer' ) => array(),        
                    __( 'Mileage', 'wp-super-dealer' ) => array(),
					__( 'MPG Highway', 'wp-super-dealer' ) => array(),
					__( 'MPG City', 'wp-super-dealer' ) => array(),
                    __( 'Wheelchair Accessible', 'wp-super-dealer' ) => array(),
                    __( 'Chassis', 'wp-super-dealer' ) => array(),
                    __( 'Engine', 'wp-super-dealer' ) => array(),
					__( 'Engine Type', 'wp-super-dealer' ) => array(),
                    __( 'Fuel Type', 'wp-super-dealer' ) => array(),
					__( 'Tank Size', 'wp-super-dealer' ) => array(),
                    __( 'Transmission', 'wp-super-dealer' ) => array(),
					__( 'Driveline', 'wp-super-dealer' ) => array(),
					__( 'Exterior Color', 'wp-super-dealer' ) => array(),
					__( 'Interior Color', 'wp-super-dealer' ) => array(),
					__( 'Brake System', 'wp-super-dealer' ) => array(),
					__( 'Steering Type', 'wp-super-dealer' ) => array(),
				),
			),
			__( 'options', 'wp-super-dealer' ) => array(
				__( 'default', 'wp-super-dealer' ) => array( 
					__( 'default', 'wp-super-dealer' ) => __( 'Seatbelts,Rear Door, Rear Hatch,VP Package,Vinyl Flooring,Dinette Table,In Floor Storage,
						LED Interior Courtesy Lighting,Bluetooth Capable Stereo', 'wp-super-dealer' )
				),
			)
		),
		__( 'rv', 'wp-super-dealer' ) => array(
			__( 'label', 'wp-super-dealer' ) => __( 'RVs', 'wp-super-dealer' ),
			__( 'icon', 'wp-super-dealer' ) => $url . 'rv-icon.jpg',
			__( 'specifications', 'wp-super-dealer' ) => array( 
				__( 'Specifications', 'wp-super-dealer' ) => array(
					__( 'Class', 'wp-super-dealer' ) => array(),
                    __( 'Mileage', 'wp-super-dealer' ) => array(),
                    __( 'Engine', 'wp-super-dealer' ) => array(),
                    __( 'Horsepower', 'wp-super-dealer' ) => array(),
                    __( 'Torque', 'wp-super-dealer' ) => array(),
                    __( 'Fuel Type', 'wp-super-dealer' ) => array(),
                    __( 'Fuel Capacity', 'wp-super-dealer' ) => array(),
                    __( 'MPG Highway', 'wp-super-dealer' ) => array(),
					__( 'MPG City', 'wp-super-dealer' ) => array(),
					__( 'Exterior Color', 'wp-super-dealer' ) => array(),
					__( 'Interior Color', 'wp-super-dealer' ) => array(),
					__( 'Length', 'wp-super-dealer' ) => array(),
                    __( 'Ext Height', 'wp-super-dealer' ) => array(),
					__( 'Dry Weight', 'wp-super-dealer' ) => array(),
					__( 'Hitch Weight', 'wp-super-dealer' ) => array(),
                    __( 'Gross Weight', 'wp-super-dealer' ) => array(),
                    __( 'Axle Weight Front', 'wp-super-dealer' ) => array(),
                    __( 'Axle Weight Back', 'wp-super-dealer' ) => array(),
					__( 'Leveling Jacks', 'wp-super-dealer' ) => array(),
					__( 'Water Capacity Fresh', 'wp-super-dealer' ) => array(),
					__( 'Water Capacity Gray', 'wp-super-dealer' ) => array(),
                    __( 'Water Capacity Balck', 'wp-super-dealer' ) => array(),
                    __( 'Water Capacity Hot', 'wp-super-dealer' ) => array(),
                    __( 'Sleeps', 'wp-super-dealer' ) => array(),
                    __( 'Slideouts', 'wp-super-dealer' ) => array(),
                    __( 'Awnings', 'wp-super-dealer' ) => array(),
                    __( 'Bedrooms', 'wp-super-dealer' ) => array(),
                    __( 'Bathrooms', 'wp-super-dealer' ) => array(),
                    __( 'Basement Storage', 'wp-super-dealer' ) => array(),
                    __( 'TVs', 'wp-super-dealer' ) => array(),
                    __( 'Air Conditioners', 'wp-super-dealer' ) => array(),
                    __( 'Self Contained', 'wp-super-dealer' ) => array(),
                    __( 'Wheels', 'wp-super-dealer' ) => array(),
                    __( 'Wheelbase', 'wp-super-dealer' ) => array(),
                    __( 'Tires', 'wp-super-dealer' ) => array(),
                    __( 'Axel Count', 'wp-super-dealer' ) => array(),
                    __( 'Furnace BTU', 'wp-super-dealer' ) => array(),
                    __( 'AC BTU', 'wp-super-dealer' ) => array(),
                    __( 'Refrigerator', 'wp-super-dealer' ) => array(),
                    __( 'Cooktop Burners', 'wp-super-dealer' ) => array(),
                    __( 'Showers', 'wp-super-dealer' ) => array(),
                    __( 'Shower Size', 'wp-super-dealer' ) => array(),
                    __( 'Shower Type', 'wp-super-dealer' ) => array(),
                    __( 'Electrical Service', 'wp-super-dealer' ) => array(),
				),
			),
			__( 'options', 'wp-super-dealer' ) => array(
				__( 'default', 'wp-super-dealer' ) => array( 
					__( 'default', 'wp-super-dealer' ) => __( 'Outdoor Kitchen,Fireplace,Microwave,Bathtub,Sleeper Sofa,
                    Automatic Load-Leveling,First Aid Kit,Storage Anti-Trap Device,Keyless Entry,
                    Remote Ignition,Cruise Control,Tachometer,Tilt Steering Wheel,Tilt Steering Column,
					Heated Steering Wheel,Leather Steering Wheel,Steering Wheel Mounted Controls,Telescopic Steering Column,
					Adjustable Foot Pedals,Genuine Wood Trim,Tire Inflation/Pressure Monitor,', 'wp-super-dealer' )
				),
			)
		),
		__( 'atv', 'wp-super-dealer' ) => array(
			__( 'label', 'wp-super-dealer' ) => __( 'ATVs', 'wp-super-dealer' ),
			__( 'icon', 'wp-super-dealer' ) => $url . 'atv-icon.jpg',
			__( 'specifications', 'wp-super-dealer' ) => array( 
				__( 'Specifications', 'wp-super-dealer' ) => array(
					__( 'MPG Highway', 'wp-super-dealer' ) => array(),
					__( 'MPG City', 'wp-super-dealer' ) => array(),
					__( 'Trim Level', 'wp-super-dealer' ) => array(),
					__( 'Production Number', 'wp-super-dealer') => array(),
					__( 'Color', 'wp-super-dealer' ) => array(),
					__( 'Engine Type', 'wp-super-dealer' ) => array(),
					__( 'Transmission', 'wp-super-dealer' ) => array(),
					__( 'Driveline', 'wp-super-dealer' ) => array(),
					__( 'Tank', 'wp-super-dealer' ) => array(),
					__( 'Anti-Brake System', 'wp-super-dealer' ) => array(),
					__( 'Steering Type', 'wp-super-dealer' ) => array(),
				),
			),
			__( 'options', 'wp-super-dealer' ) => array(
				__( 'default', 'wp-super-dealer' ) => array( 
					__( 'default', 'wp-super-dealer' ) => __( '', 'wp-super-dealer' )
				),
			)
		),
		__( 'motorcycle', 'wp-super-dealer' ) => array(
			__( 'label', 'wp-super-dealer' ) => __( 'Motorcycles', 'wp-super-dealer' ),
			__( 'icon', 'wp-super-dealer' ) => $url . 'motorcycle-icon.jpg',
			__( 'specifications', 'wp-super-dealer' ) => array( 
				__( 'Specifications', 'wp-super-dealer' ) => array(
					__( 'MPG Highway', 'wp-super-dealer' ) => array(),
					__( 'MPG City', 'wp-super-dealer' ) => array(),
					__( 'Trim Level', 'wp-super-dealer' ) => array(),
					__( 'Production Number', 'wp-super-dealer') => array(),
					__( 'Color', 'wp-super-dealer' ) => array(),
					__( 'Engine Type', 'wp-super-dealer' ) => array(),
					__( 'Transmission', 'wp-super-dealer' ) => array(),
					__( 'Driveline', 'wp-super-dealer' ) => array(),
					__( 'Tank', 'wp-super-dealer' ) => array(),
					__( 'Anti-Brake System', 'wp-super-dealer' ) => array(),
					__( 'Steering Type', 'wp-super-dealer' ) => array(),
				),
			),
			__( 'options', 'wp-super-dealer' ) => array(
				__( 'default', 'wp-super-dealer' ) => array( 
					__( 'default', 'wp-super-dealer' ) => __( '', 'wp-super-dealer' )
				),
			)
		),
		__( 'trailer', 'wp-super-dealer' ) => array(
			__( 'label', 'wp-super-dealer' ) => __( 'Trailers', 'wp-super-dealer' ),
			__( 'icon', 'wp-super-dealer' ) => $url . 'trailer-icon.jpg',
			__( 'specifications', 'wp-super-dealer' ) => array( 
				__( 'Specifications', 'wp-super-dealer' ) => array(
					__( 'MPG Highway', 'wp-super-dealer' ) => array(),
					__( 'MPG City', 'wp-super-dealer' ) => array(),
					__( 'Trim Level', 'wp-super-dealer' ) => array(),
					__( 'Production Number', 'wp-super-dealer') => array(),
					__( 'Exterior Color', 'wp-super-dealer' ) => array(),
					__( 'Interior Color', 'wp-super-dealer' ) => array(),
					__( 'Engine Type', 'wp-super-dealer' ) => array(),
					__( 'Transmission', 'wp-super-dealer' ) => array(),
					__( 'Driveline', 'wp-super-dealer' ) => array(),
					__( 'Tank', 'wp-super-dealer' ) => array(),
					__( 'Anti-Brake System', 'wp-super-dealer' ) => array(),
					__( 'Steering Type', 'wp-super-dealer' ) => array(),
				),
			),
			__( 'options', 'wp-super-dealer' ) => array(
				__( 'default', 'wp-super-dealer' ) => array( 
					__( 'default', 'wp-super-dealer' ) => __( '', 'wp-super-dealer' )
				),
			)
		),
		__( 'other', 'wp-super-dealer' ) => array(
			__( 'label', 'wp-super-dealer' ) => __( 'Other', 'wp-super-dealer' ),
			__( 'icon', 'wp-super-dealer' ) => $url . 'other-icon.jpg',
			__( 'specifications', 'wp-super-dealer' ) => array( 
				__( 'Specifications', 'wp-super-dealer' ) => array(
					__( 'MPG Highway', 'wp-super-dealer' ) => array(),
					__( 'MPG City', 'wp-super-dealer' ) => array(),
					__( 'Trim Level', 'wp-super-dealer' ) => array(),
					__( 'Production Number', 'wp-super-dealer') => array(),
					__( 'Exterior Color', 'wp-super-dealer' ) => array(),
					__( 'Interior Color', 'wp-super-dealer' ) => array(),
					__( 'Engine Type', 'wp-super-dealer' ) => array(),
					__( 'Transmission', 'wp-super-dealer' ) => array(),
					__( 'Driveline', 'wp-super-dealer' ) => array(),
					__( 'Tank', 'wp-super-dealer' ) => array(),
					__( 'Anti-Brake System', 'wp-super-dealer' ) => array(),
					__( 'Steering Type', 'wp-super-dealer' ) => array(),
                    __( 'Galaxy of Origin', 'wp-super-dealer' ) => array(),
                    __( 'Photon Torpedoes', 'wp-super-dealer' ) => array(),
				),
			),
			__( 'options', 'wp-super-dealer' ) => array(
				__( 'default', 'wp-super-dealer' ) => array( 
					__( 'default', 'wp-super-dealer' ) => __( '', 'wp-super-dealer' )
				),
			)
		),
	);
	$types = apply_filters( 'cscs_default_types_filter', $types );

	return $types;
}

function wpsd_build_types_array( $types ) {
    //= we're building a new array
    //= as we build it we sanitize each key and value we use from $types
	$types = base64_decode( $types );
	$types_array = json_decode( $types, true );

	$new_types = array();

	foreach( $types_array as $key=>$type ) {

        //= is $type an array
        if ( ! is_array( $type ) ) {
            continue;
        }

        //= make sure we have a type and it's an array
        if ( ! isset( $type['type'] ) ) {
            continue;
        }

        //= sanitize the type slug
        $type['type'] = wpsd_sanitize_array( $type['type'] );

        //= let's make a fresh, clean array to put our vaslues in
		$new_types[ $type['type'] ] = array();
		$new_types[ $type['type'] ]['label'] = ucwords( $type['type'] );
		$new_types[ $type['type'] ]['icon'] = sanitize_text_field( $type['icon'] );
		$new_types[ $type['type'] ]['specifications'] = array();

        //= now we build the sub-array for specification groups
		foreach( $type[ 'specification_groups'] as $group_key=>$group_data ) {
            if ( ! isset( $group_data['label'] ) ) {
                continue;
            }
            
			$new_types[ $type['type'] ]['specifications'][ sanitize_text_field( $group_data['label'] ) ] = array();
			foreach( $group_data['specifications'] as $spec_key=>$spec ) {
				$new_types[ $type['type'] ]['specifications'][ sanitize_text_field( $group_data['label'] ) ][ sanitize_text_field( $spec_key ) ] = wpsd_sanitize_array( $spec );
			}
		}

        //= now we build the sub-array for option groups
		$new_types[ $type['type'] ]['options'] = array();
		foreach( $type[ 'options'] as $group_key=>$group_data ) {
			$new_types[ $type['type'] ]['options'][ sanitize_text_field( $group_data['name'] ) ] = array();
			
			foreach( $group_data['subgroups'] as $sub_group_key=>$sub_group_data ) {
				$options = implode( ',', $sub_group_data['option_tags'] );
				$new_types[ $type['type'] ]['options'][ sanitize_text_field( $group_data['name'] ) ][ sanitize_text_field( $sub_group_data['name'] ) ] = sanitize_text_field( $options );
			}

		}
	}

	return $new_types;
}

function wpsd_array_map_assoc( $callback , $array ){
  $r = array();
  foreach ($array as $key=>$value)
    $r[$key] = $callback($key,$value);
  return $r;
}

function wpsd_global_fields() {
	$global_fields = array(
		__( 'location', 'wp-super-dealer' ) => array(
			'label' => __( 'Location', 'wp-super-dealer' ),
			'plural' => __( 'Locations', 'wp-super-dealer' ),
			'taxonomy' => true,
			'default-value' => __( 'default', 'wp-super-dealer' ),
			'locked' => true,
			'allow_taxonomy' => true,
		),
		__( 'stock-number', 'wp-super-dealer' ) => array(
			'label' => __( 'Stock Number', 'wp-super-dealer' ),
			'taxonomy' => false,
			'allow_taxonomy' => false,
		),
		__( 'vin', 'wp-super-dealer' ) => array(
			'label' => __( 'VIN', 'wp-super-dealer' ),
			'taxonomy' => false,
			'allow_taxonomy' => false,
		),
		__( 'condition', 'wp-super-dealer' ) => array(
			'label' => __( 'Condition', 'wp-super-dealer' ),
			'plural' => __( 'Conditions', 'wp-super-dealer' ),
			'taxonomy' => true,
			'locked' => true,
			'allow_taxonomy' => true,
		),
		__( 'vehicle-year', 'wp-super-dealer' ) => array(
			'label' => __( 'Vehicle Year', 'wp-super-dealer' ),
			'plural' => __( 'Vehicle Years', 'wp-super-dealer' ),
			'taxonomy' => true,
			'locked' => true,
			'allow_taxonomy' => true,
		),
		__( 'make', 'wp-super-dealer' ) => array(
			'label' => __( 'Make', 'wp-super-dealer' ),
			'plural' => __( 'Makes', 'wp-super-dealer' ),
			'taxonomy' => true,
			'locked' => true,
			'allow_taxonomy' => true,
		),
		__( 'model', 'wp-super-dealer' ) => array(
			'label' => __( 'Model', 'wp-super-dealer' ),
			'plural' => __( 'Models', 'wp-super-dealer' ),
			'taxonomy' => true,
			'locked' => true,
			'allow_taxonomy' => true,
		),
		__( 'vehicle-tags', 'wp-super-dealer' ) => array(
			'label' => __( 'Vehicle Tags', 'wp-super-dealer' ),
			'plural' => __( 'Vehicle Tags', 'wp-super-dealer' ),
			'taxonomy' => true,
			'show-ui' => true,
			'locked' => true,
			'allow_taxonomy' => true,
			'hide_edit' => true,
		),
		__( 'retail-price', 'wp-super-dealer' ) => array(
			'label' => __( 'Retail Price', 'wp-super-dealer' ),
			'taxonomy' => false,
			'allow_taxonomy' => false,
			'hide_edit' => true,
		),
		__( 'rebates', 'wp-super-dealer' ) => array(
			'label' => __( 'Rebates', 'wp-super-dealer' ),
			'taxonomy' => false,
			'allow_taxonomy' => false,
			'hide_edit' => true,
		),
		__( 'discount', 'wp-super-dealer' ) => array(
			'label' => __( 'Discount', 'wp-super-dealer' ),
			'taxonomy' => false,
			'allow_taxonomy' => false,
			'hide_edit' => true,
		),
		__( 'price', 'wp-super-dealer' ) => array(
			'label' => __( 'Price', 'wp-super-dealer' ),
			'taxonomy' => false,
			'allow_taxonomy' => false,
			'hide_edit' => true,
			'locked' => true,
		),
	);
	$global_fields = apply_filters( 'wpsd_global_fields_filter', $global_fields );
	return $global_fields;
}

function wpsd_get_global_fields() {
	$default_global_fields = wpsd_global_fields();

	$vehicle_options = wpsd_get_vehicle_options();
	if ( isset( $vehicle_options['global_specs'] ) 
		&& is_array( $vehicle_options['global_specs'] )
		&& ( 0 < count( $vehicle_options['global_specs'] ) )
		) {
		
		$global_fields = $vehicle_options['global_specs'];
	} else {
		$global_fields = array();
	}
	$global_fields = wp_parse_args( $global_fields, $default_global_fields );
	return $global_fields;
}

function wpsd_default_vehicle_specs() {
	$specs = array( 
		__( 'Specifications', 'wp-super-dealer' ) => array(
			__( 'Body Style', 'wp-super-dealer' ) => array(),
			__( 'Mileage', 'wp-super-dealer' ) => array(),
			__( 'MPG Highway', 'wp-super-dealer' ) => array(),
			__( 'MPG City', 'wp-super-dealer' ) => array(),
			__( 'Trim Level', 'wp-super-dealer' ) => array(),
			__( 'Exterior Color', 'wp-super-dealer' ) => array(),
			__( 'Interior Color', 'wp-super-dealer' ) => array(),
			__( 'Engine Type', 'wp-super-dealer' ) => array(),
			__( 'Transmission', 'wp-super-dealer' ) => array(),
			__( 'Driveline', 'wp-super-dealer' ) => array(),
			__( 'Tank', 'wp-super-dealer' ) => array(),
			__( 'Anti-Brake System', 'wp-super-dealer' ) => array(),
			__( 'Steering Type', 'wp-super-dealer' ) => array(),
		)
	);
	return $specs;
}

function wpsd_get_vehicle_map( $post_id ) {
	$vehicle_map = array();
	$vehicle_type = get_post_meta( $post_id, '_type', true );
	$maps = wpsd_get_type_maps();
	if ( 1 > count( $maps ) ) {
		$maps = wpsd_default_type_maps();
	}

	if ( empty( $vehicle_type ) ) {
		foreach( $maps as $type=>$map ) {
			$vehicle_type = $type;
			break;
		}
	}
	if ( isset( $maps[ $vehicle_type ] ) ) {
		$vehicle_map = $maps[ $vehicle_type ];
	}
	return $vehicle_map;
}

function wpsd_flatten_specs( $map ) {
	$flat_specs = array();
	if ( isset( $map['specifications'] ) ) {
		if ( is_array( $map['specifications'] ) && count( $map['specifications'] ) > 0 ) {
			foreach( $map['specifications'] as $group=>$spec ) {
				if ( is_array( $spec ) && count( $spec ) > 0 ) {
					foreach( $spec as $spec_name=>$spec_options ) {
						$slug = sanitize_title( $spec_name );
						$_slug = str_replace( '-', '_', $slug );
						$flat_specs[ $slug ] = $spec_options;
					}
				}
			}
		}
	} //= It's like a stairway to heaven around here
	return $flat_specs;
}

function wpsd_flatten_options( $map ) {
	$flat_options = array();
	if ( isset( $map['options'] ) ) {
		if ( is_array( $map['options'] ) && count( $map['options'] ) > 0 ) {
			foreach( $map['options'] as $group=>$sub_groups ) {
				if ( is_array( $sub_groups ) && count( $sub_groups ) > 0 ) {
					foreach( $sub_groups as $sub_group=>$options ) {
						if ( false !== strpos( $options, ',' ) ) {
							$options_array = explode( ',', $options );
							$flat_options = array_merge( $flat_options, $options_array );
						} else {
							$flat_options[] = $options;
						}
					}
				}
			}
		}
	} //= It's like a stairway to heaven around here
	return $flat_options;
}

function wpsd_get_specification_taxonomies() {
	//= Let's take care of the global fields
	$default_global_fields = wpsd_global_fields();
	$vehicle_options = wpsd_get_vehicle_options();
	if ( isset( $vehicle_options['global_specs'] ) 
		&& is_array( $vehicle_options['global_specs'] )
		&& ( 0 < count( $vehicle_options['global_specs'] ) )
		) {
		$global_fields = $vehicle_options['global_specs'];
	} else {
		$global_fields = array();
	}
	$global_fields = wp_parse_args( $global_fields, $default_global_fields );
	$specification_taxonomies = array();
	//= Get specification taxonomies from global fields
	foreach( $global_fields as $global_field=>$field_options ) {

		if ( isset( $field_options['taxonomy'] ) && (
			true === $field_options['taxonomy'] 
			|| 1 === $field_options['taxonomy']
			|| 'true' === $field_options['taxonomy'] ) ) 
		{
			$specification_taxonomies[ $global_field ] = $field_options;
		}
	}
	
	//= Get specification taxonomies from all vehicle types
	$types = wpsd_get_type_maps();
	foreach( $types as $types=>$data ) {
		if ( isset( $data['specifications'] ) ) {
			if ( is_array( $data['specifications'] ) && count( $data['specifications'] ) > 0 ) {
				foreach( $data['specifications'] as $group=>$spec ) {
					if ( is_array( $spec ) && count( $spec ) > 0 ) {
						foreach( $spec as $spec_name=>$spec_options ) {
							if ( isset( $spec_options['taxonomy'] ) && $spec_options['taxonomy'] ) {
								$specification_taxonomies[ $spec_name ] = $spec_options;
							}
						}
					}
				} //= end foreach( $data['specifications'] as $group=>$spec ) {
			}
		}
	}

	return $specification_taxonomies;	
}
?>