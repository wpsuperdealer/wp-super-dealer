<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//= TO DO: Decide if we should just kill these or maybe create a dynamic way to add user selected columns/quick edits?
add_action( 'manage_vehicle_posts_custom_column', 'manage_vehicle_columns' );
add_filter( 'manage_edit-vehicle_columns', 'vehicle_columns' );

function vehicle_columns( $columns ) {
	$show_hide = array(
		'price' => false,
		'discount' => false,
		'rebates' => false,
		'retail-price' => false,
	);

	$active_price_fields = get_active_price_fields();
	foreach( $show_hide as $field=>$value ) {
		if ( ! isset( $active_price_fields[ $field ] ) ) {
			$show_hide[ $field ] = true;
		}
	}

	$columns = array_merge( array(
		'sold' => __( 'Sold', 'wp-super-dealer' )
	), $columns );

	if ( current_user_can( WPSD_ADMIN_CAP ) ) {
		if ( $show_hide['price'] != true ) {
			$columns = array_merge( array(
				'price' => __( 'Price', 'wp-super-dealer' )
			), $columns );
		}
	}

	if ( current_user_can( WPSD_ADMIN_CAP ) ) {
		if ( $show_hide['discount'] != true ) {
			$columns = array_merge( array(
				'discount' => __( 'Discount', 'wp-super-dealer' )
			), $columns );
		}
	}

	if ( current_user_can( WPSD_ADMIN_CAP ) ) {
		if ( $show_hide['rebates'] != true ) {
			$columns = array_merge( array(
				'rebate' => __( 'Rebate', 'wp-super-dealer' )
			), $columns );
		}
	}

	if ( current_user_can( WPSD_ADMIN_CAP ) ) {
		if ( $show_hide['retail-price'] != true ) {
			$columns = array_merge( array(
				'msrp' => __( 'Retail Price', 'wp-super-dealer' )
			), $columns );
		}
	}
    
	$columns = array_merge( array(
		"cb" => "<input type=\"checkbox\" />",
		"title" => __( 'Vehicle Title', 'wp-super-dealer' ),
		"photo" => __( 'Photo', 'wp-super-dealer' ),
	), $columns );

	return $columns;
}

function manage_vehicle_columns( $column ) {
	global $post;
	$post_id = $post->ID;
	$post_title = $post->post_title;
	$wpsd_pluginpath = WPSD_PATH;
	$wpsd_pluginpath = str_replace( 'admin', '', $wpsd_pluginpath );
	$vehicle_pic = wpsd_main_photo( $post_id );

	if ( empty( $vehicle_pic ) ) {
		$wpsd_pluginpath = WPSD_PATH;
		$wpsd_pluginpath = str_replace( 'admin', '', $wpsd_pluginpath );
		$vehicle_pic = $wpsd_pluginpath . 'images/coming-soon.jpg';
	}
	$attachments = get_children( array( 'post_parent' => $post_id ) );
	$count = count( $attachments );
	if ( defined( 'WPSD_CUSTOM_NO_PHOTO' ) ) {
		$vehicle_image = '<img width="75" src="' . esc_url( $vehicle_pic ) . '" onerror="ImgError(this, \'' . WPSD_CUSTOM_NO_PHOTO . '\');" /><br />(' . esc_html( $count ) . ')';		
	} else {
		$vehicle_image = '<img width="75" src="' . esc_url( $vehicle_pic ) . '" onerror="ImgError(this, \'' . esc_url( $wpsd_pluginpath ) . 'images/coming-soon.jpg\');" /><br />(' . esc_html( $count ) . ')';
	}

    $output = '';
    
	if ( "ID" == $column ) {
		echo esc_html( $post_id );
		//= Add our nonce field
	} elseif ( "photo" == $column ) {
		$output .= $vehicle_image;
		$nonce = wp_create_nonce( 'admin-edit-column-nonce-' . $post_id );
		$output .= '<input type="hidden" name="wpsd_admin_update_nonce_' . esc_attr( $post_id ) . '" id="wpsd_admin_update_nonce_' . esc_attr( $post_id ) . '" value="' . $nonce . '" />';
	} elseif ( "msrp" == $column ) {
		$output .= '<input type="number" data-type="retail_price" id="retail_price_' . esc_attr( $post_id ) . '" data-post-id="' . esc_attr( $post_id ) . '" class="wpsd-edit-field wpsd-edit-retail_price" size="6" value="' . get_post_meta( $post_id, '_retail_price', true ) . '" />';
	} elseif ( "rebate" == $column ) {
		$output .= '<input type="number" data-type="rebates" id="rebates_' . esc_attr( $post_id ) . '" data-post-id="' . esc_attr( $post_id ) . '" class="wpsd-edit-field wpsd-edit-rebates" size="6" value="' . get_post_meta( $post_id, '_rebates', true ) . '" />';
	} elseif ( "discount" == $column ) {
		$output .= '<input type="number" data-type="discount" id="discount_' . esc_attr( $post_id ) . '" data-post-id="' . esc_attr( $post_id ) . '" class="wpsd-edit-field wpsd-edit-discount" size="6" value="' . get_post_meta( $post_id, '_discount', true ) . '" />';
	} elseif ( "price" == $column ) {
		$price_color = '';
		$msrp = get_post_meta( $post_id, '_msrp', true );
		$rebates = get_post_meta( $post_id, '_rebates', true );
		$discounts = get_post_meta( $post_id, '_discount', true );
		if ( empty( $msrp ) ) {$msrp = 0;}
		if ( empty( $rebates ) ) {$rebates = 0;}
		if ( empty( $discounts ) ) {$discounts = 0;}
		$calculated_price = $msrp - $rebates - $discounts;
		$final_price = get_post_meta( $post_id, '_price', true );
		if ( empty( $final_price ) ) {$final_price = 0;}
		$calculated_discount = $final_price - $msrp;
		$output .= '<input' . esc_attr( $price_color ) . ' data-type="price" data-post-id="' . esc_attr( $post_id ) . '" id="price_' . esc_attr( $post_id ) . '" class="wpsd-edit-field wpsd-edit-price" type="number" size="6" value="' . esc_attr( $final_price ) . '" />';
	} elseif ( 'sold' == $column ) {
		$sold = get_post_meta( $post_id, 'sold', true );
		$output .= '<div id="show_sold_' . esc_attr( $post_id ) . '">
				<select id="sold_' . esc_attr( $post_id ) . '" class="wpsd-edit-field wpsd-edit-sold" data-type="sold" data-post-id="' . esc_attr( $post_id ) . '">
					<option value="'. __( 'No', 'wp-super-dealer' ) .'"' . ( $sold == __( 'No', 'wp-super-dealer' ) || $sold == __( 'no', 'wp-super-dealer' ) ? ' selected' : '' ) . '>' . __( 'No', 'wp-super-dealer' ) . '</option>
					<option value="'. __( 'Yes', 'wp-super-dealer' ) .'"' . ( $sold == __( 'Yes', 'wp-super-dealer' ) || $sold == __( 'yes', 'wp-super-dealer' ) ? ' selected' : '' ) . '>' . __( 'Yes', 'wp-super-dealer' ) . '</option>
				</select>
			</div>';
	}
    if ( ! empty( $output ) ) {
        $elements = array(
            'a' => array(
                'href' => array(),
                'title' => array()
            ),
            'img' => array(
                'width' => array(),
                'onerror' => array(),
                'src' => array(),
            ),
            'br' => array(),
            'div' => array(),
            'select' => array(
                'id' => array(),
                'class' => array(),
                'data-type' => array(),
                'data-post-id' => array(),
            ),
            'option' => array(
                'value' => array(),
            ),
            'input' => array(
                'value' => array(),
                'data-type' => array(),
                'data-post-id' => array(),
                'class' => array(),
                'type' => array(),
                'size' => array(),
                'id' => array(),
            ),
        );

        echo wp_kses( $output, $elements );
    }
}

?>