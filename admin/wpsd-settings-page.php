<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_settings_page() {
	if ( isset( $_GET['page'] ) ) {
		if ( $_GET['page'] == 'wpsd_settings_options' ) {
			add_action( 'admin_enqueue_scripts', 'wpsd_admin_scripts' );
		}
	}
	if ( isset( $_GET['post_type'] ) ) {
		if ( $_GET['post_type'] == WPSD_POST_TYPE ) {
			add_action( 'admin_enqueue_scripts', 'wpsd_admin_scripts' );
		}
	}
	global $post;
	if ( isset( $_GET['action'] ) ) {
		if ( $_GET['action'] == 'edit' ) {
			if ( isset( $_GET['post'] ) ) {
				$post_type = get_post_type( $_GET['post'] );
				if ( $post_type == WPSD_POST_TYPE ) {
					add_action( 'admin_enqueue_scripts', 'wpsd_admin_scripts' );
				}
			}
		}
	}

	if ( defined( 'WPSD_ADMIN' ) ) {
		$admin_users = WPSD_ADMIN;
		$current_user = get_current_user_id();
		
		if ( strpos( $admin_users, ',' ) ) {
			$admin_users_array = explode( ',', $admin_users );
			if ( ! in_array( $current_user, $admin_users_array ) ) {
				return;
			}
		} else {
			if ( $admin_users != $current_user ) {
				return;
			}
		}
	}

    add_submenu_page( 'edit.php?post_type=vehicle', __( 'WPSuperDealer', 'wp-super-dealer' ) . __( 'Settings', 'wp-super-dealer' ), __( 'Settings', 'wp-super-dealer' ), 'edit_pages', 'wpsd_settings_options', 'wpsd_settings_options_do_page' );
}
add_action( 'admin_menu', 'wpsd_settings_page' );

function wpsd_get_settings_map() {
	$settings = array (
		'welcome' => array(
			'label' => __( 'Welcome', 'wp-super-dealer' ),
			'content' => wpsd_welcome(),
			'action' => '',
			'save_button' => '',
		),
		'startup_guide' =>  array(
			'label' => __( 'Startup Guide', 'wp-super-dealer' ),
			'content' => wpsd_setup_guide(),
			'action' => '',
		),
		'api' => array(
			'label' => __( 'API Keys', 'wp-super-dealer' ),
			'content' => wpsd_api_tab(),
			'action' => 'save_api',
			'save_button' => __( 'Save API Settings', 'wp-super-dealer' ),
			'fields' => array( 'ad for car fax/autocheck, API #, product info' ),
		),
		'general' =>  array(
			'label' => __( 'General', 'wp-super-dealer' ),
			'content' => wpsd_general_settings(),
			'action' => 'save_general',
			'save_button' => __( 'Save General Settings', 'wp-super-dealer' ),
			'fields' => array( '' ),
		),
		'locations' =>  array(
			'label' => __( 'Locations', 'wp-super-dealer' ),
			'content' => wpsd_locations_tab(),
			'action' => 'save_locations',
			'save_button' => __( 'Save Location Settings', 'wp-super-dealer' ),
			'fields' => array(),
		),
		'price' => array (
			'label' => __( 'Price Options', 'wp-super-dealer' ),
			'content' => wpsd_price_tab(),
			'action' => 'save_price',
			'save_button' => __( 'Save Price Settings', 'wp-super-dealer' ),
			'fields' => array(),
		),
		'vehicle_options' =>  array(
			'label' => __( 'Vehicle Options', 'wp-super-dealer' ),
			'content' => wpsd_vehicle_options_tab(),
			'action' => 'save_vehicle_options',
			'save_button' => __( 'Save', 'wp-super-dealer' ),
			'fields' => array( '' ),
		),
        /*
		'layout_options' =>  array(
			'label' => __( 'Layout Options', 'wp-super-dealer' ),
			'content' => wpsd_layout_tab(),
			'action' => 'save_layout_options',
			'save_button' => __( 'Save', 'wp-super-dealer' ),
			'fields' => array( '' ),
		),
        */
		'search_form' => array(
			'label' => __( 'Search Form', 'wp-super-dealer' ),
			'content' => wpsd_search_form_tab(),
			'action' => 'save_search_form',
			'save_button' => __( 'Save Search Form Settings', 'wp-super-dealer' ),
			'fields' => array( '' ),
		),
		'sort' =>  array(
			'label' => __( 'Vehicle Sorting', 'wp-super-dealer' ),
			'content' => wpsd_sort_admin_settings(),
			'action' => 'save_sort',
			'save_button' => __( 'Save Sorting Settings', 'wp-super-dealer' ),
			'fields' => array(),
		),
		'forms' =>  array(
			'label' => __( 'Forms', 'wp-super-dealer' ),
			'content' => wpsd_forms_tab(),
			'action' => '',
			'save_button' => __( 'Save', 'wp-super-dealer' ),
			'fields' => array(),
		),
        /*
		'change_log' =>  array(
			'label' => __( 'Change Log', 'wp-super-dealer' ),
			'content' => wpsd_readme(),
			'action' => '',
			'save_button' => __( 'Save', 'wp-super-dealer' ),
			'fields' => array(),
		),
		'news' =>  array(
			'label' => __( 'News', 'wp-super-dealer' ),
			'content' => wpsd_news_content(),
			'action' => '',
			'save_button' => __( 'Save', 'wp-super-dealer' ),
			'fields' => array(),
		),
        */
	);
	$settings = apply_filters( 'wpsd_settings_map_filter', $settings );
    
    if ( function_exists( 'car_demon_activate' ) ) {
        $settings['car_demon'] = array(
            'label' => __( 'Car Demon Options', 'wp-super-dealer' ),
            'content' => wpsd_car_demon_settings(),
            'action' => '',
            'save_button' => __( 'Save', 'wp-super-dealer' ),
            'fields' => array(),
        );        
    }
    
	$settings['go_pro'] = array(
		'label' => __( 'Go Pro!', 'wp-super-dealer' ),
		'content' => wpsd_go_pro_tab(),
		'action' => '',
		'save_button' => __( 'Save', 'wp-super-dealer' ),
		'fields' => array(),
	);
	return $settings;
}

function wpsd_settings_options_do_page() {
	$plugin_file = WPSD_DIR . 'wp-super-dealer.php';
	$plugin_data = get_plugin_data( $plugin_file, true, true );
	$wpsd_version = $plugin_data['Version'];

	$html = ' <div class="wrap wpsd_welcome wpsd-admin">';
		$html .= '<div class="wpsd_welcome_logo">';
            $html .= '<h2 class="wpsd-welcome-header wpsd-welcome-header-top">' . __( 'Unleash your hidden Hero', 'wp-super-dealer' ) . '</h2>';
            $html .= '<a href="http://wpsuperdealer.com/" target="_blank">';
				$html .= '<img src="' . WPSD_PATH . '/images/wpsd-certified-support.png'.'" />';
			$html .= '</a>';
            $html .= '<h2 class="wpsd-welcome-header wpsd-welcome-header-bottom">' . __( 'Super Powers for vehicle sales', 'wp-super-dealer' ) . '</h2>';
		$html .= '</div>';

		$settings = wpsd_get_settings_map();

		$tabs = '';
		$content = '';
		$cnt = 0;
		foreach ( $settings as $setting=>$params ) {
			if ( isset( $_COOKIE['wpsd-admin-tab'] ) ) {
				if ( $setting === $_COOKIE['wpsd-admin-tab'] ) {
					$active = ' active';
				} else {
					$active = '';
				}
			} else {
				if ( 0 === $cnt ) {
					$active = ' active';
				} else {
					$active = '';
				}
				++$cnt;
			}

			$tabs .= '<div class="wpsd_settings_tab_title wpsd_settings_tab_title_' . esc_attr( $setting . $active ) . '" data-tab="' . esc_attr( $setting ) . '">';
				$tabs .= '<a name="' . esc_attr( $setting ) . '">' . esc_html( $params['label'] ) . '</a>';
			$tabs .= '</div>';
			$content .= '<div class="wpsd_settings_tab wpsd_settings_tab_' . esc_attr( $setting . $active ) . '" id="' . esc_attr( $setting ) . '" >';
				$content .= '<form name="wpsd_setting_form_' . esc_attr( $setting ) . '" class="wpsd_setting_form_' . esc_attr( $setting ) . '" data-action="' . esc_attr( $params['action'] ) . '">';
					if ( ! empty( $params['action'] ) ) {
						$nonce = wp_create_nonce( 'admin-' . $setting . '-nonce' );
						$content .= '<input type="hidden" name="admin-' . esc_attr( $setting ) . '-nonce" id="admin-' . esc_attr( $setting ) . '-nonce" class="admin-' . esc_attr( $setting ) . '-nonce" value="' . $nonce . '" />';
						$content .= '<div class="wpsd-save-settings-button" data-name="' . esc_attr( $setting ) . '" data-action="' . esc_attr( $params['action'] ) . '">';
							$content .= '<span class="dashicons dashicons-thumbs-up"></span>';
							if ( isset( $params['save_button'] ) && ! empty( $params['save_button'] ) ) {
								$content .= $params['save_button'];
							} else {
								$content .= __( 'Save Settings', 'wp-super-dealer' );
							}
						$content .= '</div>';
						$content .= '<div class="wpsd_settings_message wpsd_settings_message_' . esc_attr( $setting ) . '">';
						$content .= '</div>';
					}
					if ( isset( $params['content'] ) ) {
						$content .= $params['content'];
					}
				$content .= '</form>';
			$content .= '</div>';
		}
		$html .= '<div class="wpsd-admin-tabs">' . $tabs . '</div>';
		$html .= '<div class="wpsd_welcome_clear"></div>';
		$html .= '<div class="wpsd-admin-tabs-content">' . $content . '</div>';

		$html .= '<div class="cd-clear"></div>';
		$html .= '<div class="wpsd_welcome_logo">';
			$html .= '<a href="http://wpsuperdealer.com/" target="_blank">';
				$html .= '<img style="max-width:600px;" src="' . WPSD_PATH . '/images/WP-Super-Dealer.png'.'" />';
			$html .= '</a>';
		$html .= '</div>';

	$html .= '</div>';
    echo wpsd_kses_admin( $html );
}

function wpsd_admin_group_wrap( $label = '', $content = '', $fields = array() ) {
	$slug = sanitize_title( $label );
	$fields_json = json_encode( $fields );
	$fields_base64 = base64_encode( $fields_json );
	$html = '<fieldset class="wpsd_admin_group wpsd_admin_group_' . esc_attr( $slug ) . '" data-slug="' . esc_attr( $slug ) . '" data-fields="' . esc_attr( $fields_base64 ) . '">';
		$html .= wp_nonce_field( 'admin-' . $slug . '-nonce', 'admin-' . $slug . '-nonce', true, false );
		$html .= '<legend data-slug="' . $slug . '"><i>+</i>';
			$html .= esc_html( $label );
		$html .= '</legend>';
		if ( ! empty( $content ) ) {
			$html .= $content;
		} else {
			if ( 0 < count( $fields ) ) {
				$form_fields = new WP_SUPER_DEALER\WPSD_FIELDS;
				foreach( $fields as $field=>$args ) {
					$html .= $form_fields->get_field( $field, $args );
				}
			}
		}
	$html .= '</fieldset>';
	return $html;
}

function wpsd_admin_update_validation( $args ) {
	$return = array(
		'msg' => '',
		'valid' => true,
	);
	if ( ! isset( $_POST['admin-' . $args['setting'] . '-nonce'] ) 
	  || ! wp_verify_nonce( $_POST['admin-' . $args['setting'] . '-nonce'], 'admin-' . $args['setting'] . '-nonce' ) ) {
		$return['msg'] = __( 'Save Failed.', 'wp-super-dealer' );
		$return['msg'] .= '<br />';
		$return['msg'] .= __( 'Request did not send a valid nonce.', 'wp-super-dealer' );
		$return['valid'] = false;
		return $return;
	} else {
		//= TO DO Future: make sure user has permission/correct capability
		if ( ! is_object( $args['fields'] ) ) {
			$return['msg'] = __( 'Save Failed.', 'wp-super-dealer' );
			$return['msg'] .= '<br />';
			$return['msg'] .= __( 'No fields sent: ', 'wp-super-dealer' ) . print_r( $args['fields'], true );
			$return['valid'] = false;
			return $return;
		}

		if ( 'save_' . $args['setting'] !== $args['action'] ) {
			$return['msg'] = __( 'Save Failed.', 'wp-super-dealer' );
			$return['msg'] .= '<br />';
			$return['msg'] .= __( 'Incorrect action sent: ', 'wp-super-dealer' ) . $args['action'];
			$return['valid'] = false;
			return $return;
		}

		if ( $args['setting'] !== $args['setting'] ) {
			$return['msg'] = __( 'Save Failed.', 'wp-super-dealer' );
			$return['msg'] .= '<br />';
			$return['msg'] .= __( 'Incorrect setting sent: ', 'wp-super-dealer' ) . $args['setting'];
			$return['valid'] = false;
			return $return;
		}
		
		//= Let's get our settings map
		$settings_map = wpsd_get_settings_map();
		if ( ! isset( $settings_map[ $args['setting'] ] ) ) {
			$return['msg'] = __( 'Save Failed.', 'wp-super-dealer' );
			$return['msg'] .= '<br />';
			$return['msg'] .= __( 'Setting not registered: ', 'wp-super-dealer' ) . print_r( $args['settings_map'], true );
			$return['valid'] = false;
			return $return;
		}
	}
	return $return;
}

function wpsd_instructions_footer() {
    $html = '';
    $html .= '<h2 class="wpsd-text-center">';
        $html .= __( 'For commercial assistance please visit ', 'wp-super-dealer' );
        $html .= '<a href="https://wpsuperdealer.com" target="_blank">';
            $html .= __( 'our website', 'wp-super-dealer' );
        $html .= '</a>';
        $html .= __( '.', 'wp-super-dealer' );
    $html .= '</h2>';
    return $html;
}

function reset_wp_super_dealer() {
	wpsd_set_default_options();
	wpsd_create_post_type();
	flush_rewrite_rules();
	echo '<h3 class="admin_settings_updated_title">' . esc_html( __( 'SETTINGS HAVE BEEN RESET', 'wp-super-dealer' ) ) . '</h3>';
}

function wpsd_kses_admin( $content ) {
    $elements = array(
        'a' => array(
            'id' => array(),
            'name' => array(),
            'href' => array(),
            'title' => array(),
            'target' => array(),
        ),
        'h2' => array(
            'class' => array(),
        ),
        'h3' => array(
            'class' => array(),
        ),
        'h4' => array(
            'class' => array(),
            'data-active' => array(),
            'data-item' => array(),
            'align' => array(),
        ),
        'img' => array(
            'id' => array(),
            'name' => array(),
            'width' => array(),
            'onerror' => array(),
            'src' => array(),
            'style' => array(),
            'class' => array(),
        ),
        'form' => array(
            'id' => array(),
            'name' => array(),
            'class' => array(),
            'data-action' => array(),
        ),
        'fieldset' => array(
            'id' => array(),
            'name' => array(),
            'class' => array(),
            'data-slug' => array(),
            'data-fields' => array(),
            'data-specification-group' => array(),
            'data-specification-group-value' => array(),
        ),
        'legend' => array(
            'id' => array(),
            'name' => array(),
            'class' => array(),
            'data-slug' => array(),
        ),
        'label' => array(
            'id' => array(),
            'name' => array(),
            'class' => array(),
            'data-slug' => array(),
            'data-field' => array(),
        ),
        'select' => array(
            'id' => array(),
            'name' => array(),
            'class' => array(),
            'data-type' => array(),
            'data-post-id' => array(),
            'data-key' => array(),
        ),
        'option' => array(
            'value' => array(),
            'selected' => array(),
            'data-direction' => array(),
            'data-icon' => array(),
        ),
        'textarea' => array(
            'id' => array(),
            'name' => array(),
            'class' => array(),
            'placeholder' => array(),
        ),
        'input' => array(
            'id' => array(),
            'name' => array(),
            'class' => array(),
            'value' => array(),
            'type' => array(),
            'size' => array(),
            'checked' => array(),
            'data-post-id' => array(),
            'data-type' => array(),
            'data-base64' => array(),
            'data-prefix' => array(),
            'data-slug' => array(),
            'data-action' => array(),
            'data-group' => array(),
            'data-specifications' => array(),
            'data-specification' => array(),
            'data-section' => array(),
            'data-value' => array(),
            'data-key' => array(),
            'placeholder' => array(),
            'min' => array(),
            'max' => array(),
            'step' => array(),
            'disabled' => array(),
        ),
        'br' => array(),
        'p' => array(
            'class' => array(),
        ),
        'i' => array(),
        'div' => array(
            'id' => array(),
            'name' => array(),
            'data-base64' => array(),
            'class' => array(),
            'style' => array(),
            'data-action' => array(),
            'data-id' => array(),
            'data-name' => array(),
            'data-slug' => array(),
            'data-tab' => array(),
            'data-option' => array(),
            'data-value' => array(),
            'data-checked' => array(),
            'data-section' => array(),
            'data-type' => array(),
            'data-group' => array(),
            'data-sub-group' => array(),
            'data-specification' => array(),
            'data-specification-group' => array(),
            'data-specification-group-value' => array(),
            'data-option-group' => array(),
            'data-option-group-value' => array(),
            'data-option-sub-group' => array(),
            'data-option-sub-group-value' => array(),
            'data-field' => array(),
            'data-field-slug' => array(),
            'data-car-link' => array(),
            'data-src-id' => array(),
            'data-src' => array(),
            'data-cnt' => array(),
            'data-maps' => array(),
            'data-post-id' => array(),
            'data-color' => array(),
        ),
        'span' => array(
            'id' => array(),
            'name' => array(),
            'class' => array(),
            'data-video-url' => array(),
            'data-field-slug' => array(),
        ),
        'iframe' => array(
            'src' => array(),
            'frameborder' => array(),
            'allow' => array(),
        ),
        'small' => array(
            'class' => array(),
            'title' => array(),
        ),
        'ul' => array(
            'class' => array(),
        ),
        'ol' => array(),
        'li' => array(),
        'table' => array(
            'class' => array(),
        ),
        'tr' => array(
            'class' => array(),
        ),
        'td' => array(
            'class' => array(),
        ),
        'pre' => array(),
        'code' => array(),
    );

    $elements = apply_filters( 'wpsd_kses_admin_elements_filter', $elements, $content );
    return wp_kses( $content, $elements );
}
?>