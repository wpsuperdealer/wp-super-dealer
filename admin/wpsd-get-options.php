<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_get_options() {
	$wpsd_pluginpath = WPSD_PATH;
	$default = array();
	//= TO DO Future: clean up default options
	$default['price_before'] = '$';
	$default['price_after'] = '';
	$default['wpsd_api_key'] = '';
	$default['wpsd_api_validated'] = '';
	$default['wpsd_api_vin_decodes'] = false;
	$default['wpsd_api_history_reports'] = false;	

	$default['vehicles_per_page'] = '9';
	$default['error_image'] = $wpsd_pluginpath . 'images/coming-soon.jpg';
	
	$default['wpsd_slug'] = 'vehicles-for-sale';
	//= TO DO Future: add before_listings to block
	$default['before_listings'] = '';
	$default['default_description'] = __( 'This vehicle is ready to go. Contact us for more information.', 'wp-super-dealer' );
	//= TO DO Future: add dynamic_load to block
	$default['dynamic_load'] = __( 'yes', 'wp-super-dealer' );
	$default['show_sold'] = __( 'no', 'wp-super-dealer' );
	$default['show_only_sold'] = __( 'no', 'wp-super-dealer' );
	$default['show_sort'] = __( 'yes', 'wp-super-dealer' );
	$default['show_searched_by'] = __( 'yes', 'wp-super-dealer' );
	$default['show_results_found'] = __( 'yes', 'wp-super-dealer' );
	$default['show_navigation'] = __( 'yes', 'wp-super-dealer' );
	$default['vehicle_container'] = '';
	$default['show_switch_style'] = __( 'yes', 'wp-super-dealer' );
	$default['default_style'] = __( 'full', 'wp-super-dealer' );
	$default['use_post_title'] = __( 'yes', 'wp-super-dealer' );
	$default['no_results'] = __( 'Sorry, but nothing matched your search criteria. Please try using a broader search selection.', 'wp-super-dealer' );
	$default['no_results_title'] = __( 'No vehicle results found', 'wp-super-dealer' );
	$default['title_trim'] = '49';
	$default['no_price'] = __( 'Call for price', 'wp-super-dealer');

	//= Auto Load Inventory Options
	$default['al_container'] = '#wpsd-inventory-wrap';
	$default['al_items'] = '.srp-item';
	$default['al_pagination'] = '#wpsd-nav-below .wpsd-navigation';
	$default['al_next'] = '.nextpostslink';
	$default['loader'] = $wpsd_pluginpath . 'images/ajax-loader.gif';

	$default['hide_updates'] = __( 'No', 'wp-super-dealer' );
	$default['use_theme_files'] = false;

	//delete_option( 'wpsd_options');
	$wpsd_options = array();

	$wpsd_options = get_option( 'wpsd_options', $default );
	$wpsd_options = wp_parse_args( $wpsd_options, $default );
	return $wpsd_options;
}

function wpsd_option_arrays() {
	$x = '';
	$options = array(
		'wpsd_options',
		'wpsd-vehicle-types',
		'wpsd-vehicle-options',
		'wpsd-search-settings',
		'wpsd-search-fields',
		'wp-super-dealer-slug',
		'wpsdsort-items',
		'wpsdsort-default',
		'wpsdsort-default-direction',
		'wpsdsort-default-sort',
	);
	foreach( $options as $option ) {
		$array = get_option( $option, '' );
		$x .= '<pre>';
			$x .= '<strong>';
				$x .= '$' . esc_html( $option ) . ' = ';
			$x .= '</strong>';
			$x .= var_export( $array, true );
		$x .= '</pre>';
	}
	return $x;
}

/**
 * Save default settings
 */
function wpsd_set_default_options() {
	$wpsd_options = wpsd_get_options();
    $wpsd_options['inventory_page'] = '/inventory/';
    /*
    Not adding default location
    Not saving search fields
    Not setting default sort fields
    */
    //= Make sure our post type and default taxonomies are loaded
    wpsd_create_post_type();
    
    //wpsd-vehicle-types
	$wpsd_all_vehicle_types = wpsd_get_type_maps();
    $wpsd_vehicle_type = $wpsd_all_vehicle_types['automobile'];
	$wpsd_vehicle_types['automobile'] = $wpsd_vehicle_type;
	
	//wpsd-vehicle-options
	$wpsd_vehicle_options = wpsd_get_vehicle_options();

	//wpsd-search-settings
	$wpsd_search_settings = wpsd_default_search_settings();

	//wpsd-search-fields
	$wpsd_search_fields = wpsd_default_search_fields();
	
	//wp-super-dealer-slug
	$wp_super_dealer_slug = 'vehicles-for-sale';
	
	//wpsdsort-items
	$wpsdsort_items = default_wpsdsort_items();

	//wpsdsort-default-direction
	$wpsdsort_default_direction = 'desc';

	//wpsdsort-default-sort
	$wpsdsort_default_sort = array (
        'first' => '',
        'second' => '_make',
        'third' => '_model',
	);
	//= Update all the options
	update_option( 'wpsd_options', $wpsd_options );
	update_option( 'wpsd-vehicle-types', $wpsd_vehicle_types );
	update_option( 'wpsd-vehicle-options', $wpsd_vehicle_options );
	update_option( 'wpsd-search-settings', $wpsd_search_settings );
	update_option( 'wpsd-search-fields', $wpsd_search_fields );
	update_option( 'wp-super-dealer-slug', $wp_super_dealer_slug );
	update_option( 'wpsdsort-items', $wpsdsort_items );
	update_option( 'wpsdsort-default', $wpsdsort_default_sort );
	update_option( 'wpsdsort-default-direction', $wpsdsort_default_direction );
	update_option( 'wpsdsort-default-sort', $wpsdsort_default_sort );
}
?>