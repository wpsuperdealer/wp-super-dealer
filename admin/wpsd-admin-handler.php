<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action( 'wp_ajax_wpsd_admin_handler', 'wpsd_admin_handler' );
function wpsd_admin_handler() {
	if ( ! is_admin() ) {
		return;
	}
	if ( ! current_user_can( WPSD_ADMIN_CAP ) ) {
		return;
	}
    $return = array();
	$return['msg'] = '';
	$return['setting'] = sanitize_text_field( $_POST['setting'] );
	$return['action'] = sanitize_text_field( $_POST['wpsd_action'] );
    $field_data = sanitize_text_field( $_POST['fieldData'] );
	$return['fieldsB64'] = $field_data;
	$return['fieldsJSON'] = base64_decode( $field_data );
    $return['fieldsJSON'] = sanitize_text_field( $return['fieldsJSON'] );
	$return['fields'] = json_decode( $return['fieldsJSON'] );

	$validate_update = wpsd_admin_update_validation( $return );
	if ( ! $validate_update['valid'] ) {
		$return['msg'] = $validate_update['msg'];
	} else {
		if ( function_exists( 'wpsd_' . $return['setting'] . '_settings_save' ) ) {
			$return['msg'] = call_user_func( 'wpsd_' . $return['setting'] . '_settings_save', array( $return ) );
		} else {
			$return['msg'] = __( 'SETTING DOES NOT HAVE A VALID SAVE FUNCTION - NO CHANGES MADE', 'wp-super-dealer' );
		} //= end if ( 'general' === $return['setting'] )
	} //= end if ( ! $validate_update['valid'] )

	$return = apply_filters( 'wpsd_admin_handler_filter', $return, $validate_update );

	$json = json_encode( $return );
	echo wpsd_kses_json( $json );
	exit();
}

add_action( 'wp_ajax_wpsd_api_validation', 'wpsd_api_validation' );
function wpsd_api_validation() {
	$results = array();

	if ( ! wp_verify_nonce( $_POST['nonce'], 'admin-api-nonce' ) ) {
		//= Nonce is invalid, return failure and message
		$results['msg'] = __( 'Nonce could not be validated.', 'wp-super-dealer' );
		$results['validation'] = false;
		$results['result'] = 'failure';
	} else {
		$api_key = sanitize_text_field( $_POST['api_key'] );
	
		//= Validate key
		$validation = wpsd_validate_key( $api_key );
		$result['validation'] = $validation;
		$wpsd_options = get_option( 'wpsd_options' );
		$wpsd_options['wpsd_api_validated'] = $validation;
		$wpsd_options['wpsd_api_key'] = $api_key;
		$results['api_key'] = $api_key;
		update_option( 'wpsd_options', $wpsd_options );
		$results['result'] = 'success';
		//$results['tab_content'] = wpsd_api_tab();
	}

	$json = json_encode( $results );
	echo wpsd_kses_json( $json );
	
	exit();
}

add_action( 'wp_ajax_wpsd_add_default_vehicle_type', 'wpsd_add_default_vehicle_type' );
function wpsd_add_default_vehicle_type() {
	if ( ! is_admin() ) {
		return;
	}
	if ( ! current_user_can( WPSD_ADMIN_CAP ) ) {
		return;
	}
	$nonce = $_POST['nonce'];
	if ( ! wp_verify_nonce( $nonce, 'admin-vehicle_options-nonce' ) ) {
		echo esc_html( __( 'Nonce check failed - no changes saved.', 'wp-super-dealer' ) );
		exit();
	}
	$results = array();
	$results['msg'] = __( 'Default type not loaded.', 'wp-super-dealer' );
	$results['response'] = false;

	if ( ! isset( $_POST['type'] ) ) {
		$json = json_encode( $results );
		echo wpsd_kses_json( $json );
		exit();
	}
	$type = sanitize_text_field( $_POST['type'] );
	$results['type'] = $type;
	$default_types = wpsd_default_type_maps();
	$types = get_option( 'wpsd-vehicle-types', $default_types );
	$types = apply_filters( 'wpsd_types_filter', $types );

	if ( isset( $default_types[ $type ] ) ) {
		$types[ $type ] = $default_types[ $type ];
		update_option( 'wpsd-vehicle-types', $types, false );
		$results['msg'] = __( 'Default type added: ', 'wp-super-dealer' ) . $type;
		$results['response'] = true;
		$results[ $type ] = $types[ $type ];
	}
	
	$json = json_encode( $results );
	echo wpsd_kses_json( $json );
	exit();
}

add_action( 'wp_ajax_wpsd_admin_update', 'wpsd_admin_update' );
function wpsd_admin_update() {
	if ( ! is_admin() ) {
		return;
	}
	if ( ! current_user_can( WPSD_ADMIN_CAP ) ) {
		return;
	}
	$post_id = sanitize_text_field( $_POST['post_id'] );
	$nonce = $_POST['nonce'];
	if ( ! wp_verify_nonce( $nonce, 'wpsd_admin_update_nonce_' . $post_id ) ) {
		echo esc_html( __( 'Nonce check failed - no changes saved.', 'wp-super-dealer' ) );
		exit();
	}
	$fld = sanitize_text_field( $_POST['fld'] );
	$val = sanitize_text_field( $_POST['val'] );
	update_post_meta( $post_id, $fld, $val );
}

add_action( 'wp_ajax_wpsd_create_inventory_page', 'wpsd_create_inventory_page' );
function wpsd_create_inventory_page() {
	if ( ! is_admin() ) {
		exit();
	}
	if ( ! current_user_can( WPSD_ADMIN_CAP ) ) {
		echo esc_html( __( 'You do not have permission to do this.', 'wp-super-dealer' ) );
		exit();
	}

	$nonce = $_POST['nonce'];
	if ( ! wp_verify_nonce( $nonce, 'wpsd_create_inventory_nonce' ) ) {
		echo esc_html( __( 'Nonce check failed - page not created.', 'wp-super-dealer' ) );
		exit();
	}
	if ( isset( $_POST['page_name'] ) && isset( $_POST['include_search'] ) ) {
		$page_name = sanitize_text_field( $_POST['page_name'] );
		$include_search = sanitize_text_field( $_POST['include_search'] );
		$html = '';
		if ( $include_search == 'yes' ) {
			$html .= '<!-- wp:wp-super-dealer/wpsd-search /-->';
		}
		$html .= '<!-- wp:wp-super-dealer/wpsd-inventory /-->';
        /*
        [wpsd_search]
        [wpsd_inventory]
        */
        
		// create the inventory shortcode page if it doesn't exist.
		if ( ! get_page_by_title( $page_name ) ) {
			// Create post object
			$page = array(
			  'post_title'    => $page_name,
			  'post_type'	  => 'page',
			  'post_content'  => $html,
			  'post_status'   => 'publish',
			  'post_author'   => 1,
			);
			// Insert the post into the database
			$p_id = wp_insert_post( $page );
			$wpsd_cdrf_options = array();
			$wpsd_cdrf_options = get_option( 'wpsd_options', $wpsd_cdrf_options );
			$wpsd_cdrf_options['inventory_page'] = get_permalink( $p_id );
			$wpsd_cdrf_options['result_page_created'] = true;
			update_option( 'wpsd_options', $wpsd_cdrf_options );
            $output = '';
			$output .= __( 'Your page has been created and the following has been added to it:', 'wp-super-dealer' );
			$ouput .= '<br />';
			$output .= wp_kses_post( $html );
			$output .= '<br />';
			$output .= '<a href="' . esc_url( $wpsd_cdrf_options['inventory_page'] ) . '" target="_blank">';
				$output .= __( 'View Your Page', 'wp-super-dealer' );
			$output .= '</a>';
            echo $output;
		} else {
			$page = get_page_by_title( $page_name );
			$link = $page->guid;
            $output = '';
			$output .= __( 'That page name has already been used.', 'wp-super-dealer' );
			$output .= '<br />';
			$output .= __( 'A new page could not be created, please use a new name and try again.', 'wp-super-dealer' );
			$output .= '<br />';
			$output .= '<a href="' . esc_url( $link ) . '" target="_blank">';
				$output .= __( 'View existing page', 'wp-super-dealer' );
			$output .= '</a>';
            echo $ouput;
		}
	} else {
		echo esc_html( __( 'Your page could not be created. Please make sure you entered a title.', 'wp-super-dealer' ) );
	}
	exit();	
}

function wpsd_select_sample_qty() {
	$qty = 1;
	$x = '<select class="sample_qty">';
	while($qty <= 30) {
		if ($qty == 3) {
			$select = ' selected';
		} else {
			$select = '';
		}
		$x .= '<option value="'. esc_attr( $qty ) .'"'. esc_attr( $select ) .'>'. esc_html( $qty ) .'</option>';
		++$qty;
	} 
	$x .= '</select>';
	return $x;
}

/***************************************************/
//= End Vehicle
/***************************************************/
add_action( 'wp_ajax_wpsd_get_vehicle_type_edit', 'wpsd_get_vehicle_type_edit' );
function wpsd_get_vehicle_type_edit() {
	if ( ! is_admin() ) {
		return;
	}
	if ( ! current_user_can( WPSD_ADMIN_CAP ) ) {
		return;
	}
	$nonce = $_POST['nonce'];
	if ( ! wp_verify_nonce( $nonce, 'admin-edit-vehicle-nonce' ) ) {
		echo esc_html( __( 'Nonce check failed - no changes saved.', 'wp-super-dealer' ) );
		exit();
	}
		
	$response = array();
	$response['result'] = false;
	$response['msg'] = __( 'Switching vehicle type specifications and options.', 'wp-super-dealer' );

	$post_id = sanitize_text_field( $_POST['post_id'] );
	$new_type = sanitize_text_field( $_POST['type'] );
	$old_type = get_post_meta( $post_id, '_type', true );
	update_post_meta( $post_id, '_type', $new_type );
	$post = get_post( $post_id );

	$response['specifications'] = wpsd_kses_admin( get_vehicle_specifications_metabox( $post ) );
	$response['options'] = wpsd_kses_admin( get_vehicle_options_metabox( $post ) );
	$response_json = json_encode( $response );
	echo $response_json;
	update_post_meta( $post_id, '_type', $old_type );
    exit();
}

/**********************************************/
/* Start edit vehicle images
/**********************************************/
add_action( 'wp_ajax_wpsd_update_image_links', 'wpsd_update_image_links' );
function wpsd_update_image_links() {
	if ( ! is_admin() ) {
		return;
	}
	if ( ! current_user_can( WPSD_ADMIN_CAP ) ) {
		return;
	}
	$nonce = $_POST['nonce'];
	if ( ! wp_verify_nonce( $nonce, 'wpsd_update_image_links_nonce' ) ) {
		echo esc_html( __( 'Nonce check failed - no changes saved.', 'wp-super-dealer' ) );
		exit();
	}
	$post_id = sanitize_text_field( $_POST['post_id'] );
	$image_links = sanitize_text_field( $_POST['image_links'] );
	update_post_meta( $post_id, '_image_urls', $image_links );
	echo esc_html( __( 'Image links updated', 'wp-super-dealer' ) );
	exit();
}

add_action( 'wp_ajax_wpsd_update_main_image', 'wpsd_update_main_image' );
function wpsd_update_main_image() {
	if ( ! is_admin() ) {
		return;
	}
	if ( ! current_user_can( WPSD_ADMIN_CAP ) ) {
		return;
	}
	$nonce = $_POST['nonce'];
	if ( ! wp_verify_nonce( $nonce, 'wpsd_edit_vehicle_nonce' ) ) {
		echo esc_html( __( 'Nonce check failed - no changes saved.', 'wp-super-dealer' ) );
		exit();
	}

	$post_id = sanitize_text_field( $_POST['post_id'] );
	$attachment_id = sanitize_text_field( $_POST['attachment_id'] );
	set_post_thumbnail( $post_id, $attachment_id );
	echo esc_html( __( 'Main Vehicle Photo updated', 'wp-super-dealer' ) );
	exit();
}

add_action( 'wp_ajax_wpsd_update_image_order', 'wpsd_update_image_order' );
function wpsd_update_image_order() {
	if ( ! is_admin() ) {
		return;
	}
	if ( ! current_user_can( WPSD_ADMIN_CAP ) ) {
		return;
	}
	$nonce = $_POST['nonce'];
	if ( ! wp_verify_nonce( $nonce, 'admin-edit-vehicle-nonce' ) ) {
		echo esc_html( __( 'Nonce check failed - no changes saved.', 'wp-super-dealer' ) );
		exit();
	}

	$post_id = sanitize_text_field( $_POST['post_id'] );
	$linked_json = stripslashes( sanitize_text_field( $_POST['linked'] ) );
	$linked = (array)json_decode( $linked_json );

	$attached_json = stripslashes( sanitize_text_field( $_POST['attached'] ) );
	$attached = (array)json_decode( $attached_json );

	$total_links = count( $linked );
	if ( $total_links > 0 ) {
		$linked_list = '';
		$cnt = 0;
		foreach( $linked as $key=>$image ) {
			++$cnt;
			$linked_list .= $image;
			if ( $cnt < $total_links ) {
				$linked_list .= ',';
			}
		}
		update_post_meta( $post_id, '_image_urls', $linked_list );
	}

	if ( count( $attached ) > 0 ) {
		$attached = array_reverse( $attached, true );
		$cnt = 0;
		foreach( $attached as $order=>$attachment_id ) {
			++$cnt;
			$post = array(
				'ID' => $attachment_id,
				'menu_order' => $cnt,
			);
			wp_update_post( $post );
		}
	}

	echo esc_html( __( 'Image order updated', 'wp-super-dealer' ) );
	exit();
}

add_action( 'wp_ajax_wpsd_image_handler', 'wpsd_image_handler' );
function wpsd_image_handler() {
	if ( ! is_admin() ) {
		return;
	}
	if ( ! current_user_can( WPSD_ADMIN_CAP ) ) {
		return;
	}
	$nonce = $_POST['nonce'];
	if ( ! wp_verify_nonce( $nonce, 'admin-edit-vehicle-nonce' ) ) {
		echo esc_html( __( 'Nonce check failed - no changes saved.', 'wp-super-dealer' ) );
		exit();
	}

	$post_id = sanitize_text_field( $_POST['post_id'] );
	$option = sanitize_text_field( $_POST['option'] );

	if ( 'add_vehicle_images' === $option ) {
		$attachment_id = sanitize_text_field( $_POST['attachment_id'] );
		$post_id = sanitize_text_field( $_POST['post_id'] );
        $output = wpsd_add_vehicle_images( $post_id, $attachment_id );
		echo wpsd_kses_json( $output );
		exit();
	} else if ( 'wpsd_update_image_order' === $option ) {
		wpsd_update_image_order();
	} else if ( 'remove_vehicle_attached_image' === $option ) {
		$return = array();
		if ( isset( $_POST['post_id'] ) ) {
			$post_id = sanitize_text_field( $_POST['post_id'] );
		} else {
			$post_id = 0;
		}
		if ( isset( $_POST['type'] ) ) {
			$return['type'] = sanitize_text_field( $_POST['type'] );
		} else {
			$return['type'] = 'attachments';
		}
		if ( isset( $_POST['cnt'] ) ) {
			$return['cnt'] = sanitize_text_field( $_POST['cnt'] );
		} else {
			$return['cnt'] = '0';
		}
		if ( isset( $_POST['attachment_id'] ) ) {
			$return['attachment_id'] = sanitize_text_field( $_POST['attachment_id'] );
		} else {
			$return['attachment_id'] = '';
		}
		/* - Remove from vehicle, but do not delete - kept for reference
		$post['ID'] = $attachment_id;
		$post['post_parent'] = 0;
		wp_update_post( $post );
		*/
        wp_delete_attachment( $return['attachment_id'] );
		$return['msg'] = __( 'Image Removed', 'wp-super-dealer' );
		$json = json_encode( $return );
		echo wpsd_kses_json( $json );
		exit();
	} else if ( 'remove_vehicle_linked_image' === $option ) {
		$return = array();
		$post_id = sanitize_text_field( $_POST['post_id'] );
		$image = sanitize_text_field( $_POST['vehicle_link'] );
		$cnt = sanitize_text_field( $_POST['cnt'] );
		$type = sanitize_text_field( $_POST['type'] );
		$image_list = get_post_meta( $post_id, '_image_urls', true );
		$return['post_id'] = $post_id;
		$return['image'] = $image;
		$return['cnt'] = $cnt;
		$return['type'] = $type;
		$return['image_list'] = $image_list;
		if ( false !== strpos( $image_list, ',' ) ) {
			$images_array = explode( ',', $image_list );
		} else {
			$images_array = array( $image_list );
		}
		if ( ( $key = array_search( $image, $images_array ) ) !== false) {
			unset( $images_array[ $key ] );
		}
		$images = implode( ',', $images_array );
		update_post_meta( $post_id, '_image_urls', $images );

		$return['images'] = $images;
		$return['msg'] = __( 'Image Link Removed', 'wp-super-dealer' );
        $json = json_encode( $return );
		echo wpsd_kses_json( $json );
		exit();
	}
	exit();
}

function wpsd_add_vehicle_images( $post_id, $attachment_id ) {
	$wpsd_post = array();
	$wpsd_post['ID'] = $attachment_id;
	$wpsd_post['post_parent'] = $post_id;
	wp_update_post( $wpsd_post );
	$guid = wp_get_attachment_url( $attachment_id );
	$cnt = ( rand( 10, 1000 ) );
	$html = '';
	$html .= '<div id="vehicle_photo_' . esc_attr( $cnt ) . '" name="vehicle_photo_' . esc_attr( $cnt ) . '" class="vehicle_photo_admin_box">';
		$html .= '<div class="vehicle_photo_remove" data-type="attachments" data-src-id="' . esc_attr( $attachment_id ) . '" data-cnt="' . esc_attr( $cnt ) . '">';
			$html .= 'X';
		$html .= '</div>';
		$html .= '<div align="center">';
			$html .= '<img class="wpsd_thumbs" style="cursor:pointer" src="' . esc_attr( trim( $guid ) ) . '" width="162" />';
		$html .= '</div>';
	$html .= '</div>';
	return $html;
}
/**********************************************/
/* End edit vehicle images
/**********************************************/
add_action( 'wp_ajax_wpsd_add_new_location', 'wpsd_add_new_location' );
function wpsd_add_new_location() {
	if ( ! is_admin() ) {
		return;
	}
	if ( ! current_user_can( WPSD_ADMIN_CAP ) ) {
		return;
	}
	$nonce = $_POST['nonce'];
	if ( ! wp_verify_nonce( $nonce, 'admin-locations-nonce' ) ) {
		echo esc_html( __( 'Nonce check failed - no changes saved.', 'wp-super-dealer' ) );
		exit();
	}

	$location = sanitize_text_field( $_POST['location_name'] );
	
	$term = wp_insert_term( $location, 'location' );
	$return = array();
	
	$return['term'] = $term;
	$json = json_encode( $return );
	echo wpsd_kses_json( $json );
	exit();
}

add_action( 'wp_ajax_wpsd_remove_location', 'wpsd_remove_location' );
function wpsd_remove_location() {
	if ( ! is_admin() ) {
		return;
	}
	if ( ! current_user_can( WPSD_ADMIN_CAP ) ) {
		return;
	}
	$nonce = $_POST['nonce'];
	if ( ! wp_verify_nonce( $nonce, 'admin-locations-nonce' ) ) {
		echo esc_html( __( 'Nonce check failed - no changes saved.', 'wp-super-dealer' ) );
		exit();
	}

	$return = array();
	$location = sanitize_text_field( $_POST['location_name'] );
	
	$term = get_term_by( 'slug', $location, 'location' );
	$return['term'] = $term;
	if ( ! is_wp_error( $term ) ) {
		$term_id = $term->term_id;
		wp_delete_term( $term_id, 'location' );
	}

	$json = json_encode( $return );
	echo wpsd_kses_json( $json );
	exit();
}

add_action( 'wp_ajax_wpsd_edit_column_update', 'wpsd_edit_column_update' );
function wpsd_edit_column_update() {
	if ( ! is_admin() ) {
		return;
	}
	if ( ! current_user_can( WPSD_ADMIN_CAP ) ) {
		return;
	}
	$nonce = $_POST['nonce'];
	
	$return = array();
	
	$return['post_id'] = sanitize_text_field( $_POST['post_id'] );
	
	if ( ! wp_verify_nonce( $nonce, 'admin-edit-column-nonce-' . $return['post_id'] ) ) {
		echo esc_html( __( 'Nonce check failed - no changes saved.', 'wp-super-dealer' ) );
		exit();
	}

	//= TO DO Future: make sure this is  valid field type
	$return['type'] = sanitize_text_field( $_POST['type'] );
	$return['value'] = sanitize_text_field( $_POST['value'] );
	
	if ( 'sold' === $return['type'] ) {
		$type  = $return['type'];
	} else if ( 'retail_price' === $return['type'] ) {
		$type  = '_retail_price';
	} else {
		$type  = '_' . $return['type'];
	}
	
	$return['result'] = update_post_meta( $return['post_id'], $type, $return['value'] );
	
	$json = json_encode( $return );
	echo wpsd_kses_json( $json );
	
	exit();
}

/**********************************************/
/* Start remove vehicle reports
/**********************************************/
add_action( 'wp_ajax_wpsd_remove_report', 'wpsd_remove_report' );
function wpsd_remove_report() {
	if ( ! is_admin() ) {
		return;
	}
	if ( ! current_user_can( WPSD_ADMIN_CAP ) ) {
		return;
	}
	$nonce = $_POST['nonce'];

    $return = array();
    $type = sanitize_text_field( $_POST['type'] );
    $return['type'] = $type;
    $type = str_replace( '-', '_', $type );
    $return['slug'] = $type;
    
	if ( ! wp_verify_nonce( $nonce, 'wpsd_get_' . $type . '_nonce' ) ) {
		echo esc_html( __( 'Nonce check failed - no changes saved.', 'wp-super-dealer' ) );
		exit();
	}

	$return['post_id'] = sanitize_text_field( $_POST['post_id'] );
    $return['result'] = delete_post_meta( $return['post_id'], '_' . $type, '' );
    $return['message'] = __( 'Report removed: ', 'wp-super-dealer' ) . $type;

    $json = json_encode( $return );
	echo wpsd_kses_json( $json );
	
	exit();
}

function wpsd_kses_json( $json ) {
    $elements = array(
        'a' => array(
            'href' => array(),
            'title' => array(),
        ),
        'img' => array(
            'src' => array(),
            'class' => array(),
            'style' => array(),
            'width' => array(),
        ),
        'br' => array(),
        'label' => array(),
        'div' => array(
            'id' => array(),
            'name' => array(),
            'class' => array(),
            'align' => array(),
            'data-type' => array(),
            'data-car-link' => array(),
            'class' => array(),
            'data-src-id' => array(),
            'data-cnt' => array(),
            'data-maps' => array(),
            'data-id' => array(),
            'data-post-id' => array(),
            'data-slug' => array(),
            'data-checked' => array(),
            'data-section' => array(),
            'data-action' => array(),
            'placeholder' => array(),
        ),
        'small' => array(
            'class' => array(),
            'title' => array(),
        ),
        'input' => array(
            'class' => array(),
            'title' => array(),
            'name' => array(),
            'type' => array(),
            'autocomplete' => array(),
            'value' => array(),
            'id' => array(),
            'data-slug' => array(),
            'data-key' => array(),
            'placeholder' => array(),
        ),        
        'span' => array(
            'class' => array(),
        ),
        'ul' => array(
            'data-options' => array(),
            'class' => array(),
        ),
        'li' => array(
            'data-text' => array(),
            'class' => array(),
            'data-value' => array(),
            'selected' => array(),
        ),
        'select' => array(
            'name' => array(),
            'class' => array(),
        ),
        'option' => array(
            'value' => array(),
            'data-icon' => array(),
            'selected' => array(),
        ),
        
    );

    $json = wp_kses( $json, $elements );
    return $json;
}
?>