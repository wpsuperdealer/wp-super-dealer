<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_validate_key( $apikey ) {
	$return = false;

	$url = 'https://wpsuperdealer.com/wp-json/wpsd/v1/services/?apikey=' . $apikey;

	$response = wp_remote_post( $url, array(
		'method' => 'GET',
		'timeout' => 45,
		'redirection' => 5,
		'httpversion' => '1.0',
		'blocking' => true,
		'headers' => array(),
		'body' => array(),
		'cookies' => array(),
		)
	);
	
	if ( is_wp_error( $response ) ) {
		$error_message = $response->get_error_message();
		echo esc_html( __( 'Something went wrong: ' . $error_message, '' ) );
	} else {
		$json = $response['body'];
		$array = json_decode( $json );
		if ( isset( $array->services ) ) {
            update_option( 'wpsdapi-services', $array->services );
            return $array->services;
		}
	
	}

	return $return;
}

function wpsd_get_vin_decode_results( $apikey, $vin ) {
	$return = false;
    $url = 'https://wpsuperdealer.com/wp-json/wpsd/v1/services';
    
    //= TO DO Future: this is inheriently insecure when false and should be an option for local debug only
    if ( ! defined( 'WPSD_SSL_VERIFYPEER' ) ) {
        define( 'WPSD_SSL_VERIFYPEER', true );
    }

	$response = wp_remote_post( $url, array(
		'method' => 'POST',
		'timeout' => 45,
		'redirection' => 5,
		'httpversion' => '1.1',
		'blocking' => true,
		'headers' => array(
            'Authorization' => 'none',
        ),
        'sslverify' => WPSD_SSL_VERIFYPEER,
		'body' => array(
            'apikey' => $apikey,
            'vin' => $vin,
            'service' => 'vin_decodes'
        ),
		'cookies' => array(),
		)
	);
	if ( is_wp_error( $response ) ) {
		$error_message = $response->get_error_message();
		echo esc_html( __( 'Something went wrong: ' . $error_message, '' ) );
	} else {
		$json = $response['body'];
        $array = json_decode( $json );

        if ( isset( $array->result ) ) {
            return $array;
		}
	}

	return $return;
}

function wpsd_get_history_report_results( $apikey, $vin ) {
	$return = false;
    $url = 'https://wpsuperdealer.com/wp-json/wpsd/v1/services';
    
    //= TO DO Future: this is inheriently insecure when false and should be an option for local debug only
    if ( ! defined( 'WPSD_SSL_VERIFYPEER' ) ) {
        define( 'WPSD_SSL_VERIFYPEER', true );
    }

	$response = wp_remote_post( $url, array(
		'method' => 'POST',
		'timeout' => 45,
		'redirection' => 5,
		'httpversion' => '1.1',
		'blocking' => true,
		'headers' => array(
            'Authorization' => 'none',
        ),
        'sslverify' => WPSD_SSL_VERIFYPEER,
		'body' => array(
            'apikey' => $apikey,
            'vin' => $vin,
            'service' => 'history_reports'
        ),
		'cookies' => array(),
		)
	);
	if ( is_wp_error( $response ) ) {
		$error_message = $response->get_error_message();
		echo esc_html( __( 'Something went wrong: ' . $error_message, '' ) );
	} else {
		$json = $response['body'];
        $array = json_decode( $json );

        if ( isset( $array->result ) ) {
            return $array;
		}
	}

	return $return;
}

?>