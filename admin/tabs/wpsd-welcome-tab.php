<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_welcome() {
	$wpsd_version = WPSD_VER;
	$html = '';
	//= wpsd_option_arrays();
	$html = apply_filters( 'wpsd_admin_welcome_filter', $html );
	if ( ! empty( $html ) ) {
		return $html;
	}
	$html .= '<h2>';
		$html .= __( 'Welcome to WPSuperDealer', 'wp-super-dealer' ) . ' ' . esc_html( $wpsd_version );
	$html .= '</h2>';
	$html .= '<div class="welcome_excerpt">';
		$html .= '<p>';
			$html .= __( 'Thank you for installing WPSuperDealer', 'wp-super-dealer' ) . ' ' . esc_html( $wpsd_version );
		$html .= '</p>';
		$html .= '<p>';
			$html .= __( 'You now have Super Powers for vehicle sales!', 'wp-super-dealer' );
		$html .= '</p>';
		$html .= '<p>';
			$html .= __( 'Please take the time to go through the "Startup Guide" tab and keep an eye out for the "Tips & Tricks" sections at the top of each tab.', 'wp-super-dealer' );
		$html .= '</p>';
		$html .= '<p>';
			$html .= __( 'For professional support and assistance please visit:', 'wp-super-dealer')  .' <a href="http://wpsuperdealer.com" target="_blank">WPSuperDealer.com</a>';
		$html .= '</p>';
	$html .= '</div>';
	$html .= '<div class="wpsd_welcome_clear"></div>';
	$html .= '<h3>';
		$html .= __( 'For add-ons, setup, installation and/or custom development to expand and enhance WPSuperDealer please visit:', 'wp-super-dealer').' <a href="http://wpsuperdealer.com" target="_blank">WPSuperDealer.com</a>';
	$html .= '</h3>';

	return $html;
}
?>