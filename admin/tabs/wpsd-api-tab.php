<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_api_tab() {
	global $wpsd_options;
	$html = '';
	$html = apply_filters( 'wpsd_admin_api_filter', $html );
	if ( ! empty( $html ) ) {
		return $html;
	}
    
    $html .= '<h3>';
        $html .= __( 'API Keys', 'wp-super-dealer' );
    $html .= '</h3>';
    
    $html .= wpsd_api_instructions();

	//= WPSD API Start		
	$html .= '<fieldset class="wpsd_admin_group wpsd_data_api">';
		$html .= '<legend>+ ';
			$html .= __( 'WPSuperDealer Data API', 'wp-super-dealer' );
		$html .= '</legend>';
		$html .= '<span class="wpsd_option_group wpsd_option_open">';

			$html .= '<span class="wpsd_option_item">';
	
				if ( ! isset( $wpsd_options['wpsd_api_key'] )
				   || empty( $wpsd_options['wpsd_api_key'] ) )
				{
					$html .= '<div class="wpsd-api-signup">';
						$html .= '<h3>';
							$html .= __( 'Sign up for a WPSuperDealer Data API account and get vehicle history reports & vehicle specifications for all of your vehicles!', 'wp-super-dealer' );
						$html .= '</h3>';
						$html .= '<a href="https://wpsuperdealer.com/product-category/data-services/" target="api_win">' . __( 'Learn more here!', 'wp-super-dealer' ) . '</a></strong>';
						$html .= '<br />';
					$html .= '</div>';
				}

				$html .= '<label>';
					$html .= __( 'WPSuperDealer Data API Key:', 'wp-super-dealer' );
				$html .= '</label>';
				$html .= '<input type="text" name="wpsd_api_key" class="wpsd_api_key" value="' . esc_attr( $wpsd_options['wpsd_api_key'] ) . '" />';

				$wpsd_api_validated_class = '';
				$html .= '<input type="button" name="wpsd_api_validate" class="wpsd_api_validate" value="' . __( 'Activate', 'wp-super-dealer') . '" />';
	
			$html .= '</span>';
			$html .= '<div class="wpsd_api_items' . $wpsd_api_validated_class . '">';

				$approved_class = '';
				if ( isset( $wpsd_options['wpsd_api_validated'] )
				   && is_object( $wpsd_options['wpsd_api_validated'] )
				   && isset( $wpsd_options['wpsd_api_key'] )
				   && ! empty( $wpsd_options['wpsd_api_key'] ) ) 
				{
					$services = get_option( 'wpsdapi-services', array() );
					$approved_class = ' wpsd-approved-true';
				} else {
					$services = array();
				}

				$html .= '<div class="wpsd-approved' . esc_attr( $approved_class ) . '">&#10003;' . __( 'API Key Validated', 'wp-super-dealer' ) . '</div>';

				if ( is_object( $services ) ) {
					$html .= '<h3>' . __( 'Choose which services to use on this site:', 'wp-super-dealer' ) . '</h3>';
					$html .= '<table class="wpsd-api-services">';
						$html .= '<tr class="wpsd-api-services-header">';
							$html .= '<td>';
								$html .= 'Service';
							$html .= '</td>';
							$html .= '<td>';
								$html .= 'Total';
							$html .= '</td>';
							$html .= '<td>';
								$html .= 'Available';
							$html .= '</td>';
							$html .= '<td>';
								$html .= 'Used';
							$html .= '</td>';					
						$html .= '</tr>';
						foreach( $services as $service=>$data ) {
							$html .= '<tr class="">';
								$html .= '<th>';
									$service_name = str_replace( '_', ' ', $service );
									$html .= esc_attr( ucwords( $service_name ) );
								$html .= '</th>';
								if ( is_object( $data ) ) {
									foreach( $data as $key=>$value ) {
                                        if ( is_object( $value ) ) {
                                            break;
                                        }
										$html .= '<td class="wpsd-td wpsd-td-' . esc_attr( $service ) . '-' . esc_attr( $key ) . '" data-service="' . esc_attr( $service ) . '" data-name="' . esc_attr( $key ) . '">';
											$html .= esc_html( $value );
										$html .= '</td>';
									}
								}
							$html .= '</tr>';
						}
					$html .= '</table>';

                    $active_api = ' wpsd_api_items_inactive';
                    
                    if ( $wpsd_options['wpsd_api_vin_decodes'] === __( 'Yes', 'wp-super-dealer' )
                        || $wpsd_options['wpsd_api_history_reports'] === __( 'Yes', 'wp-super-dealer' ) )
                    {
                        $active_api = '';
                    }
                    $html .= '<div class="wpsd_api_items_wrap' . esc_attr( $active_api ) . '">';
                    
                        $html .= '<div class="wpsd_api_items_wrap_title">';
                            $html .= __( 'You have NOT turned on any of your available services.', 'wp-super-dealer' );
                            $html .= '<span>';
                                $html .= __( 'Turn on the services you wish to use, save your settings and then refresh.', 'wp-super-dealer' );
                            $html .= '</span>';
                        $html .= '</div>';
                    
                        $html .= '<span class="wpsd_option_item cdi_api_item">';
                            $html .= '<label>';
                                $html .= '<a href="https://wpsuperdealer.com/product/wp-super-dealer-vin-decodes/" target="_blank">';
                                    $html .= __( 'Use VIN Decodes', 'wp-super-dealer' );
                                $html .= '</a>';
                            $html .= '</label>';
                            $html .= '<select data-service="vin_decodes" class="wpsd-api-activate wpsd-api-activate-vin_decodes" name="wpsd_api_vin_decodes">';
                                $html .= '<option value="'. __( 'No', 'wp-super-dealer' ) . '"' . ( $wpsd_options['wpsd_api_vin_decodes'] !== __( 'Yes', 'wp-super-dealer' ) ? ' selected' : "" ) . '>' . __( 'No', 'wp-super-dealer' ) . '</option>';
                                $html .= '<option value="'. __( 'Yes', 'wp-super-dealer' ) . '"' . ( $wpsd_options['wpsd_api_vin_decodes'] === __( 'Yes', 'wp-super-dealer' ) ? ' selected' : "" ) . '>' . __( 'Yes', 'wp-super-dealer' ) . '</option>';
                            $html .= '</select>';
                            $html .= '<div class="wpsd-api-activate-msg wpsd-api-activate-msg-vin_decodes"></div>';
                        $html .= '</span>';

                        $html .= '<span class="wpsd_option_item cdi_api_item">';
                            $html .= '<label>';
                                $html .= '<a href="https://wpsuperdealer.com/product/wpsd-vehicle-history-reports/" target="_blank">';
                                    $html .= __( 'Use Vehicle History Reports', 'wp-super-dealer' );
                                $html .= '</a>';
                            $html .= '</label>';
                            $html .= '<select data-service="history_reports" class="wpsd-api-activate wpsd-api-activate-history_reports" name="wpsd_api_history_reports">';
                                $html .= '<option value="'. __( 'No', 'wp-super-dealer' ) . '"' . ( $wpsd_options['wpsd_api_history_reports'] !== __( 'Yes', 'wp-super-dealer' ) ? ' selected' : "" ) . '>' . __( 'No', 'wp-super-dealer' ) . '</option>';
                                $html .= '<option value="'. __( 'Yes', 'wp-super-dealer' ) . '"' . ( $wpsd_options['wpsd_api_history_reports'] === __( 'Yes', 'wp-super-dealer' ) ? ' selected' : "" ) . '>' . __( 'Yes', 'wp-super-dealer' ) . '</option>';
                            $html .= '</select>';
                            $html .= '<div class="wpsd-api-activate-msg wpsd-api-activate-msg-history_reports"></div>';
                        $html .= '</span>';

                    $html .= '</div>';
				}
			$html .= '</div>';

		$html .= '</span>';
	$html .= '</fieldset>';
	//= WPSD API Stop

	return $html;
}

function wpsd_api_instructions() {
    $html = '';
	$html .= '<div class="wpsd-instructions-wrap wpsd-instructions-wrap-api">';
		$html .= '<div class="wpsd-instructions-title" data-tab="api">';
			$html .= '<span class="dashicons dashicons-welcome-learn-more"></span>';
			$html .=  __( 'Tips & Tricks', 'wp-super-dealer' );
		$html .= '</div>';

		$html .=  '<ul class="wpsd-instructions">';
            $html .= '<li>';
                $html .= '<div class="wpsd-tnt-wrap">';
                    $html .= '<div class="wpsd-tnt-col">';
						$html .= '<div class="wpsd-video-wrap">';
                            $html .= '<span class="wpsd-youtube" data-video-url="https://www.youtube.com/embed/s4sYvEN4iEI"></span>';
						$html .= '</div>';
                    $html .= '</div>';
                    $html .= '<div class="wpsd-tnt-col">';
                        $html .= '<h2>';
                            $html .=  __( 'Tips & Tricks for API Keys', 'wp-super-dealer' );
                        $html .= '</h2>';
                        $html .= '<ol>';
                            $html .= '<li>' . __( 'WPSuperDealer offers a Data API for autmotive sellers in the United States.', 'wp-super-dealer' ) . '</li>';
                            $html .= '<li>' . __( 'If you are outside of the United States and interested in a similar service then please let us know. We hope to add the rest of North America soon and need direction from you on where to focus next.', 'wp-super-dealer' ) . '</li>';
                            $html .= '<li>' . __( 'The Data API provides Vin Decodes & Vehicle History Reports.', 'wp-super-dealer' ) . '</li>';
                            $html .= '<li>' . __( 'Please visit our website for details and pricing.', 'wp-super-dealer' ) . '</li>';
                            $html .= '<li>';
                                //= TO DO Future: switch this to WordPress.org support forum
                                $html .= '<a href="https://wpsuperdealer.com" target="_blank">';
                                    $html .= __( 'https://WPSuperDealer.com', 'wp-super-dealer' );
                                $html .= '</a>';
                            $html .= '</li>';
                        $html .= '</ol>';
                    $html .= '</div>';
                $html .= '</div>'; //= end $html .= '<div class="wpsd-tnt-wrap">';
                $html .= wpsd_instructions_footer();
            $html .= '</li>';
		$html .= '</ul>';
		$html .= '<div class="wpsd-instructions-hide" data-tab="api">';
			$html .=  __( 'show', 'wp-super-dealer' );
		$html .= '</div>';
	$html .= '</div>';
    return $html;
}

function wpsd_api_settings_save( $args ) {
	global $wpsd_options;

	$api_fields = array(
		'key', 'vin_decodes', 'history_reports',
	);
	foreach( $api_fields as $api_field ) {
		if ( isset( $_POST['wpsd_api_' . $api_field ] ) ) {
			$value = sanitize_text_field( $_POST['wpsd_api_' . $api_field] );
			$wpsd_options['wpsd_api_' . $api_field] = $value;
		}
	}

	//= Update the general options
	update_option( 'wpsd_options', $wpsd_options );

	return __( 'Settings Saved', 'wp-super-dealer' );
}
?>