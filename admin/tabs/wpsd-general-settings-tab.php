<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_general_settings() {
	$html ='';
	$html = apply_filters( 'wpsd_admin_general_settings_filter', $html );
	if ( ! empty( $html ) ) {
		return $html;
	}
	
	global $wpsd_options;
	$form_fields = new WP_SUPER_DEALER\WPSD_FIELDS;
	$time = time();
    $html .= '<h3>';
        $html .= __( 'General Options', 'wp-super-dealer' );
    $html .= '</h3>';
    $html .= wpsd_general_settings_instructions();
	
	$fields = array();
	//inventory_page
	$options = array();
	$options[] = array(
		'label' => __( 'Homepage', 'wp-super-dealer' ),
		'value' => get_bloginfo( 'wpurl' ),
	);
	$selected = url_to_postid( $wpsd_options['inventory_page'] );
	$args = array(
		'depth'                 => 0,
		'child_of'              => 0,
		'show_option_none'      => 'Default', // string
		'show_option_no_change' => null, // string
		'option_none_value'     => null, // string
		'echo'					=> 0,
	);
	$pages = get_pages( $args );
	foreach( $pages as $id=>$data ) {
		$options[] = array(
			'label' => $data->post_title,
			'value' => get_permalink( $data->ID ),
		);
	}
	$tip_content = '';
	$tip_content .= '<p>';
		$tip_content .= __( 'This setting controls the default URL your vehicle search forms submit to.', 'wp-super-dealer' );
	$tip_content .= '</p>';
	$tip_content .= '<p>';
		$tip_content .= __( 'In most cases this will be a page called "Inventory" but it can be any page that you drop the inventory block or shortcode into.', 'wp-super-dealer' );
	$tip_content .= '</p>';
	$tip_content .= '<p>';
		$tip_content .= __( 'This global setting can be overridden by block, widget or shortcode settings.', 'wp-super-dealer' );
	$tip_content .= '</p>';
	$fields[ $time ] = array(
		'label' => __( 'Search Results Page', 'wp-super-dealer' ),
		'slug' => 'inventory-page',
		'type' => 'select',
		'value' => ( isset( $wpsd_options['inventory_page'] ) ? $wpsd_options['inventory_page'] : get_bloginfo( 'wpurl' ) ),
		'class' => '',
		'tip' => array(
			'title' => __( 'Search Results Page', 'wp-super-dealer' ),
			'content' => $tip_content.$wpsd_options['inventory_page'],
		),
		'options' => $options,
	);

	$tip_content = '';
	$tip_content .= '<p>';
		$tip_content .= __( 'This setting controls the URL path to your single vehicle pages.', 'wp-super-dealer' );
	$tip_content .= '</p>';
	$tip_content .= '<p>';
		$tip_content .= __( 'For example, if your slug is cars-for-sale then your vehicle URLs will follow this pattern: http://your-site/cars-for-sale/vehicle-slug/', 'wp-super-dealer' );
	$tip_content .= '</p>';
	$fields[ $time + 2 ] = array(
		'label' => __( 'URL Slug', 'wp-super-dealer' ),
		'slug' => 'wpsd-slug',
		'type' => 'text',
		'value' => ( isset( $wpsd_options['wpsd_slug'] ) ? $wpsd_options['wpsd_slug'] : __( 'vehicles-for-sale', 'wp-super-dealer' ) ),
		'class' => '',
		'tip' => array(
			'title' => __( 'URL Slug', 'wp-super-dealer' ),
			'content' => $tip_content,
		),
		'placeholder' => __( 'URL Slug', 'wp-super-dealer' ),
	);
	
	$tip_content = '';
	$tip_content .= '<p>';
		$tip_content .= __( 'This is the default image for vehicles that do not have an image.', 'wp-super-dealer' );
	$tip_content .= '</p>';
	$fields[ $time + 3 ] = array(
		'label' => __( 'Error Image', 'wp-super-dealer' ),
		'slug' => 'error-image',
		'type' => 'text',
		'value' => ( isset( $wpsd_options['error_image'] ) ? $wpsd_options['error_image'] : WPSD_PATH . 'images/coming-soon.jpg' ),
		'class' => '',
		'tip' => array(
			'title' => __( 'Error Image', 'wp-super-dealer' ),
			'content' => $tip_content,
		),
		'placeholder' => WPSD_PATH . 'images/coming-soon.jpg',
	);

	$label = __( 'General Options', 'wp-super-dealer' );
	$content = $form_fields->get_form_fields( $fields );

	$html .= wpsd_admin_group_wrap( $label, $content, $fields );

	$tip_content = __( 'This global setting can be overridden by block, widget or shortcode settings.', 'wp-super-dealer' );
	$fields = array();
	$fields[ $time + 4 ] = array(
		'label' => __( 'Vehicles per page', 'wp-super-dealer' ),
		'slug' => 'vehicles-per-page',
		'type' => 'number',
		'value' => ( isset( $wpsd_options['vehicles_per_page'] ) ? $wpsd_options['vehicles_per_page'] : '9' ),
		'class' => '',
		'tip' => array(
			'title' => __( 'Vehicles per page', 'wp-super-dealer' ),
			'content' => $tip_content,
		),
		'placeholder' => __( 'URL Slug', 'wp-super-dealer' ),
	);
	$fields[ $time + 5 ] = array(
		'label' => __( 'Before Listings', 'wp-super-dealer' ),
		'slug' => 'before-listings',
		'type' => 'textarea',
		'value' => ( isset( $wpsd_options['before_listings'] ) ? $wpsd_options['before_listings'] : '' ),
		'class' => '',
		'tip' => array(
			'title' => __( 'Before Listings', 'wp-super-dealer' ),
			'content' => $tip_content,
		),
		'placeholder' => '',
	);
	$fields[ $time + 6 ] = array(
		'label' => __( 'Load Next Inventory Page on Scroll', 'wp-super-dealer' ),
		'slug' => 'dynamic-load',
		'type' => 'select',
		'value' => ( isset( $wpsd_options['dynamic_load'] ) ? $wpsd_options['dynamic_load'] : __( 'no', 'wp-super-dealer' ) ),
		'class' => '',
		'tip' => array(
			'title' => __( 'Load Next Inventory Page on Scroll', 'wp-super-dealer' ),
			'content' => __( 'If this is set to yes then your inventory will continue to load as the user scrolls.', 'wp-super-dealer' ),
		),
		'options' => array(
			array(
				'label' => __( 'No', 'wp-super-dealer' ),
				'value' => __( 'no', 'wp-super-dealer' ),
			),
			array(
				'label' => __( 'Yes', 'wp-super-dealer' ),
				'value' => __( 'yes', 'wp-super-dealer' ),
			),
		),
		'placeholder' => '',
	);
	$fields[ $time + 7 ] = array(
		'label' => __( 'Include Sold Vehicles', 'wp-super-dealer' ),
		'slug' => 'show-sold',
		'type' => 'select',
		'value' => ( isset( $wpsd_options['show_sold'] ) ? $wpsd_options['show_sold'] : __( 'no', 'wp-super-dealer' ) ),
		'class' => '',
		'tip' => array(
			'title' => __( 'Show Sold', 'wp-super-dealer' ),
			'content' => $tip_content,
		),
		'options' => array(
			array(
				'label' => __( 'No', 'wp-super-dealer' ),
				'value' => __( 'no', 'wp-super-dealer' ),
			),
			array(
				'label' => __( 'Yes', 'wp-super-dealer' ),
				'value' => __( 'yes', 'wp-super-dealer' ),
			),
		),
		'placeholder' => '',
	);
	$fields[ $time + 8 ] = array(
		'label' => __( 'Show Sort By', 'wp-super-dealer' ),
		'slug' => 'show-sort',
		'type' => 'select',
		'value' => ( isset( $wpsd_options['show_sort'] ) ? $wpsd_options['show_sort'] : __( 'no', 'wp-super-dealer' ) ),
		'class' => '',
		'tip' => array(
			'title' => __( 'Show Sort By', 'wp-super-dealer' ),
			'content' => $tip_content,
		),
		'options' => array(
			array(
				'label' => __( 'No', 'wp-super-dealer' ),
				'value' => __( 'no', 'wp-super-dealer' ),
			),
			array(
				'label' => __( 'Yes', 'wp-super-dealer' ),
				'value' => __( 'yes', 'wp-super-dealer' ),
			),
		),
		'placeholder' => '',
	);
	$fields[ $time + 9 ] = array(
		'label' => __( 'Show Searched By', 'wp-super-dealer' ),
		'slug' => 'show-searched-by',
		'type' => 'select',
		'value' => ( isset( $wpsd_options['show_searched_by'] ) ? $wpsd_options['show_searched_by'] : __( 'no', 'wp-super-dealer' ) ),
		'class' => '',
		'tip' => array(
			'title' => __( 'Show Searched By', 'wp-super-dealer' ),
			'content' => $tip_content,
		),
		'options' => array(
			array(
				'label' => __( 'No', 'wp-super-dealer' ),
				'value' => __( 'no', 'wp-super-dealer' ),
			),
			array(
				'label' => __( 'Yes', 'wp-super-dealer' ),
				'value' => __( 'yes', 'wp-super-dealer' ),
			),
		),
		'placeholder' => '',
	);
	$fields[ $time + 10 ] = array(
		'label' => __( 'Show Results Found', 'wp-super-dealer' ),
		'slug' => 'show-results-found',
		'type' => 'select',
		'value' => ( isset( $wpsd_options['show_results_found'] ) ? $wpsd_options['show_results_found'] : __( 'no', 'wp-super-dealer' ) ),
		'class' => '',
		'tip' => array(
			'title' => __( 'Show Results Found', 'wp-super-dealer' ),
			'content' => $tip_content,
		),
		'options' => array(
			array(
				'label' => __( 'No', 'wp-super-dealer' ),
				'value' => __( 'no', 'wp-super-dealer' ),
			),
			array(
				'label' => __( 'Yes', 'wp-super-dealer' ),
				'value' => __( 'yes', 'wp-super-dealer' ),
			),
		),
		'placeholder' => '',
	);
	$fields[ $time + 11 ] = array(
		'label' => __( 'Show Navigation', 'wp-super-dealer' ),
		'slug' => 'show-navigation',
		'type' => 'select',
		'value' => ( isset( $wpsd_options['show_navigation'] ) ? $wpsd_options['show_navigation'] : __( 'no', 'wp-super-dealer' ) ),
		'class' => '',
		'tip' => array(
			'title' => __( 'Show Navigation', 'wp-super-dealer' ),
			'content' => $tip_content,
		),
		'options' => array(
			array(
				'label' => __( 'No', 'wp-super-dealer' ),
				'value' => __( 'no', 'wp-super-dealer' ),
			),
			array(
				'label' => __( 'Yes', 'wp-super-dealer' ),
				'value' => __( 'yes', 'wp-super-dealer' ),
			),
		),
		'placeholder' => '',
	);
	$fields[ $time + 12 ] = array(
		'label' => __( 'Show Switch Style', 'wp-super-dealer' ),
		'slug' => 'show-switch-style',
		'type' => 'select',
		'value' => ( isset( $wpsd_options['show_switch_style'] ) ? $wpsd_options['show_switch_style'] : __( 'no', 'wp-super-dealer' ) ),
		'class' => '',
		'tip' => array(
			'title' => __( 'Show Switch Style', 'wp-super-dealer' ),
			'content' => $tip_content,
		),
		'options' => array(
			array(
				'label' => __( 'No', 'wp-super-dealer' ),
				'value' => __( 'no', 'wp-super-dealer' ),
			),
			array(
				'label' => __( 'Yes', 'wp-super-dealer' ),
				'value' => __( 'yes', 'wp-super-dealer' ),
			),
		),
		'placeholder' => '',
	);
	$fields[ $time + 13 ] = array(
		'label' => __( 'Default Style', 'wp-super-dealer' ),
		'slug' => 'default-style',
		'type' => 'select',
		'value' => ( isset( $wpsd_options['default_style'] ) ? $wpsd_options['default_style'] : __( 'full', 'wp-super-dealer' ) ),
		'class' => '',
		'tip' => array(
			'title' => __( 'Show Switch Style', 'wp-super-dealer' ),
			'content' => $tip_content,
		),
		'options' => array(
			array(
				'label' => __( 'Full', 'wp-super-dealer' ),
				'value' => __( 'full', 'wp-super-dealer' ),
			),
			array(
				'label' => __( 'List', 'wp-super-dealer' ),
				'value' => __( 'list', 'wp-super-dealer' ),
			),
			array(
				'label' => __( 'Grid', 'wp-super-dealer' ),
				'value' => __( 'grid', 'wp-super-dealer' ),
			),
		),
		'placeholder' => '',
	);
	$fields[ $time + 14 ] = array(
		'label' => __( 'Inventory Width', 'wp-super-dealer' ),
		'slug' => 'vehicle_container',
		'type' => 'select',
		'value' => ( isset( $wpsd_options['vehicle_container'] ) ? $wpsd_options['vehicle_container'] : '' ),
		'class' => '',
		'tip' => array(
			'title' => __( 'Inventory Width', 'wp-super-dealer' ),
			'content' => $tip_content,
		),
		'options' => array(
			array(
				'label' => __( 'Default (800px)', 'wp-super-dealer' ),
				'value' => __( '', 'wp-super-dealer' ),
			),
			array(
				'label' => __( 'Narrow (600px)', 'wp-super-dealer' ),
				'value' => __( 'wpsd-narrow-width', 'wp-super-dealer' ),
			),
			array(
				'label' => __( 'Full Width', 'wp-super-dealer' ),
				'value' => __( 'wpsd-full-width', 'wp-super-dealer' ),
			),
		),
		'placeholder' => '',
	);

	$tip_content = __( 'Customize the title displayed in page when a search is made and no vehicles are found.', 'wp-super-dealer' );
	$fields[ $time + 15 ] = array(
		'label' => __( 'No Results Found Title', 'wp-super-dealer' ),
		'slug' => 'no-results-title',
		'type' => 'text',
		'value' => ( isset( $wpsd_options['no_results_title'] ) ? $wpsd_options['no_results_title'] : '' ),
		'class' => '',
		'tip' => array(
			'title' => __( 'No Results Found Title', 'wp-super-dealer' ),
			'content' => $tip_content,
		),
		'placeholder' => __( 'URL Slug', 'wp-super-dealer' ),
	);

	$tip_content = __( 'Customize the content displayed when a search is made and no vehicles are found.', 'wp-super-dealer' );
	$fields[ $time + 16 ] = array(
		'label' => __( 'No Results Found Message', 'wp-super-dealer' ),
		'slug' => 'no-results',
		'type' => 'textarea',
		'value' => ( isset( $wpsd_options['no_results'] ) ? $wpsd_options['no_results'] : '' ),
		'class' => '',
		'tip' => array(
			'title' => __( 'No Results Found', 'wp-super-dealer' ),
			'content' => $tip_content,
		),
		'placeholder' => '',
	);
	
	$label = __( 'List Options', 'wp-super-dealer' );
	$content = $form_fields->get_form_fields( $fields );
	$html .= wpsd_admin_group_wrap( $label, $content, $fields );

	$fields = array();
	$tip_content = '';
	$tip_content .= '<p>';
		$tip_content .= __( 'This will be shown as the description for any vehicle that does not have one.', 'wp-super-dealer' );
	$tip_content .= '</p>';
	$fields[ $time + 17 ] = array(
		'label' => __( 'Default Vehicle Description', 'wp-super-dealer' ),
		'slug' => 'default-description',
		'type' => 'textarea',
		'value' => ( isset( $wpsd_options['default_description'] ) ? $wpsd_options['default_description'] : '' ),
		'class' => '',
		'tip' => array(
			'title' => __( 'Default Vehicle Descriptions', 'wp-super-dealer' ),
			'content' => $tip_content,
		),
		'placeholder' => '',
	);
	
	$label = __( 'Single Vehicle Options', 'wp-super-dealer' );
	$content = $form_fields->get_form_fields( $fields );
	$html .= wpsd_admin_group_wrap( $label, $content, $fields );

	return $html;
}

function wpsd_general_settings_save( $args ) {
	//= Get the global settings so we can update them
	global $wpsd_options;
	if ( ! isset( $args[0]['fields'] ) ) {
		return __( 'No fields found to update.', 'wp-super-dealer' );
	}
	
	foreach( $args[0]['fields'] as $field=>$value ) {
		//= Swap dashes for underscores
		$field = str_replace( '-', '_', $field );
		$wpsd_options[ $field ] = sanitize_text_field( $value );
	}
	//= Update the options
	update_option( 'wpsd_options', $wpsd_options );
    
    //= Set a flag to flush the rewrite the next time an admin user reloads a page
    update_option( 'wpsd_flush_rewrite', time() );
    
	return __( 'Settings Saved', 'wp-super-dealer' );
}

function wpsd_general_settings_instructions() {
    $html = '';
	$html .= '<div class="wpsd-instructions-wrap wpsd-instructions-wrap-general-options">';
		$html .= '<div class="wpsd-instructions-title" data-tab="general-options">';
			$html .= '<span class="dashicons dashicons-welcome-learn-more"></span>';
			$html .=  __( 'Tips & Tricks - General Options', 'wp-super-dealer' );
		$html .= '</div>';

		$html .=  '<ul class="wpsd-instructions">';
			$html .= '<li>';
                $html .= '<div class="wpsd-tnt-wrap">';
                    $html .= '<div class="wpsd-tnt-col">';
						$html .= '<div class="wpsd-video-wrap">';
                            $html .= '<span class="wpsd-youtube" data-video-url="https://www.youtube.com/embed/9iJWpzznrGA"></span>';
						$html .= '</div>';
                    $html .= '</div>';
                    $html .= '<div class="wpsd-tnt-col">';
                        $html .= '<h2>';
                            $html .=  __( 'Tips & Tricks for the General Settings', 'wp-super-dealer' );
                        $html .= '</h2>';
                        $html .= '<ol>';
                            $html .= '<li>';
                                $html .= __( 'These options allow you to make basic changes to the behavior of your vehicle listings.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .= __( 'Next to each option you\'ll see a question mark (?) in a circle.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .= __( 'Use those tips to help guide you through your setup.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .= __( 'Many of these settings are global defaults and can be overidden when you drop the inventory into a page using blocks, shortcodes or widgets.', 'wp-super-dealer' );
                            $html .= '</li>';
                        $html .= '</ol>';
                    $html .= '</div>';
                $html .= '</div>'; //= end $html .= '<div class="wpsd-tnt-wrap">';
                $html .= wpsd_instructions_footer();
			$html .= '</li>';
		$html .= '</ul>';
		$html .= '<div class="wpsd-instructions-hide" data-tab="general-options">';
			$html .=  __( 'show', 'wp-super-dealer' );
		$html .= '</div>';
	$html .= '</div>';
    return $html;
}
?>