<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_layout_tab() {
	$html = '';
	$html = apply_filters( 'wpsd_admin_layout_filter', $html );
	if ( ! empty( $html ) ) {
		return $html;
	}
	global $wpsd_options;
    
	$html .= '<div class="wpsd-save-settings-button" data-name="layout_options" data-action="layout_options">';
		$html .= '<span class="dashicons dashicons-thumbs-up"></span>';
		$html .= __( 'Save Settings', 'wp-super-dealer' );
	$html .= '</div>';
	
    $html .= '<h3>';
        $html .= __( 'Layouts', 'wp-super-dealer' );
    $html .= '</h3>';
    
    $html .= wpsd_layout_instructions();
    
	//= List Option Start
	$html .= '<fieldset class="wpsd_admin_group">';
		$html .= '<legend>+ ';
			$html .= __( 'List Options', 'wp-super-dealer' );
		$html .= '</legend>';
		//==============
		$html .= '<span class="wpsd_option_group wpsd_option_group_layout">';

		$html .= '</span>';
	$html .= '</fieldset>';
	//= List Option Stop

	return $html;
}

function wpsd_layout_instructions() {
    $html = '';
	$html .= '<div class="wpsd-instructions-wrap wpsd-instructions-wrap-api">';
		$html .= '<div class="wpsd-instructions-title" data-tab="api">';
			$html .= '<span class="dashicons dashicons-welcome-learn-more"></span>';
			$html .=  __( 'Basic Instructions', 'wp-super-dealer' );
		$html .= '</div>';

		$html .=  '<ul class="wpsd-instructions">';
			$html .= '<li>';
				$html .=  __( 'WPSuperDealer supports multiple vehicle types.', 'wp-super-dealer' );
			$html .= '</li>';
			$html .= '<li>';
				$html .=  __( 'Each vehicle type has its own specifications and options.', 'wp-super-dealer' );
			$html .= '</li>';
			$html .= '<li>';
				$html .=  __( 'Specification fields can be used for searching and sorting.', 'wp-super-dealer' );
			$html .= '</li>';
			$html .= '<li>';
				$html .=  __( 'We suggest using as many similar specification names between types to simplify searching and sorting vehicles.', 'wp-super-dealer' );
			$html .= '</li>';
		$html .= '</ul>';
		$html .= '<div class="wpsd-instructions-hide" data-tab="api">';
			$html .=  __( 'show', 'wp-super-dealer' );
		$html .= '</div>';
	$html .= '</div>';
    return $html;
}
?>