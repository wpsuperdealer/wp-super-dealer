<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_forms_tab() {
	$html = '';
	$html = apply_filters( 'wpsd_admin_forms_filter', $html );
	if ( ! empty( $html ) ) {
		return $html;
	}

    $html .= '<h3>';
        $html .= __( 'Adding Contact Forms to WPSuperDealer', 'wp-super-dealer' );
    $html .= '</h3>';
	//= TO DO Future: Edit Pro Tab
	$html .= wpsd_forms_instructions();
    
    $html .= '<div class="wpsd-col-wrap">';
        //= Start col 1
        $html .= '<h2>';
            $html .= __( 'Basic Form Integration', 'wp-super-dealer' );
        $html .= '</h2>';
    
        $html .= '<div class="wpsd-col-wide">';
            $tabs = array(
                'intro' => array(
                    'label' => __( 'Get Started', 'wp-super-dealer' ),
                    'content' => wpsd_forms_tab_get_started(),
                ),
                'contactform7' => array(
                    'label' => __( 'Contact Form 7', 'wp-super-dealer' ),
                    'content' => wpsd_forms_tab_contactform7(),
                ),
                'other' => array(
                    'label' => __( 'Other', 'wp-super-dealer' ),
                    'content' => wpsd_forms_tab_other(),
                ),
            );
            
            $html .= '<div class="wpsd-forms-tab-wrap">';
                $active = ' active';
                foreach( $tabs as $tab=>$data ) {
                    $html .= '<div class="wpsd-forms-tab wpsd-forms-tab-' . $tab . $active . '" data-tab="' . $tab . '">';
                        $html .= $data['label'];
                    $html .= '</div>';
                    if ( ! empty( $active ) ) {
                        $active = '';
                    }
                }
            $html .= '</div>';
        $html .= '</div>';
    
        //= Start col 2
        $html .= '<div class="wpsd-col-wide">';
            $html .= '<div class="wpsd-forms-tab-content-wrap">';
                $active = ' active';
                foreach( $tabs as $tab=>$data ) {
                    $html .= '<div class="wpsd-forms-tab-content wpsd-forms-tab-content-' . $tab . $active . '" data-tab="' . $tab . '">';
                        $html .= $data['content'];
                    $html .= '</div>';
                    if ( ! empty( $active ) ) {
                        $active = '';
                    }
                }
            $html .= '</div>';
        $html .= '</div>';
    $html .= '</div>';
	return $html;
}

function wpsd_forms_instructions() {
    $html = '';
	$html .= '<div class="wpsd-instructions-wrap wpsd-instructions-wrap-go-pro">';
		$html .= '<div class="wpsd-instructions-title" data-tab="go-pro">';
			$html .= '<span class="dashicons dashicons-welcome-learn-more"></span>';
			$html .=  __( 'Tips & Tricks - Integrating Forms', 'wp-super-dealer' );
		$html .= '</div>';

		$html .=  '<ul class="wpsd-instructions">';
            $html .= '<li>';
                $html .= '<div class="wpsd-tnt-wrap">';
                    $html .= '<div class="wpsd-tnt-col">';
						$html .= '<div class="wpsd-image-wrap">';
                            $html .= '<img src="' . WPSD_PATH . '/images/wpsd-forms-logo.jpg" />';
						$html .= '</div>';
                    $html .= '</div>';
                    $html .= '<div class="wpsd-tnt-col">';
                        $html .= '<h2>';
                            $html .=  __( 'Tips & Tricks for Integrating Forms', 'wp-super-dealer' );
                        $html .= '</h2>';
                        $html .= '<ol>';
                            $html .= '<li>' . __( "Check our website, as time permits we'll post specific instructions for some of the more popular PlugIns.", "wp-super-dealer" ) . '</li>';
                            $html .= '<li>' . __( 'Start simple and build from there. Save the adfxml until after you get a basic vehicle contact form to work.', 'wp-super-dealer' ) . '</li>';
                            $html .= '<li>' . __( 'All text and html sidebar widgets on single vehicle pages will replace vehicle data tags with the value from the current vehicle.', 'wp-super-dealer' ) . '</li>';
                            $html .= '<li>' . __( 'Look at the Contact Form 7 example for a deeper look at vehicle data tags.', 'wp-super-dealer' ) . '</li>';
                            $html .= '<li>' . __( 'If you use our Template Manager add-on then you can drop your forms into the template with shortcodes or using the native WordPress do_shortcode() function anywhere you want.', 'wp-super-dealer' ) . '</li>';
                            $html .= '<li>' . __( 'If you are the developer of a PlugIn that you wish to integrate with WPSuperDealer then swing by our website and introduce yourself.', 'wp-super-dealer' ) . '</li>';
                        $html .= '</ol>';
                    $html .= '</div>';
                $html .= '</div>'; //= end $html .= '<div class="wpsd-tnt-wrap">';
                $html .= wpsd_instructions_footer();
            $html .= '</li>';
		$html .= '</ul>';
		$html .= '<div class="wpsd-instructions-hide" data-tab="locations">';
			$html .=  __( 'show', 'wp-super-dealer' );
		$html .= '</div>';
	$html .= '</div>';
    return $html;
}

function wpsd_forms_tab_get_started() {
    $html = '';
    
    $html .= '<div class="wpsd-col-wrap">';
        //= Start col 1
        $html .= '<h2>';
            $html .= __( 'Get Started', 'wp-super-dealer' );
        $html .= '</h2>';

        $html .= '<div class="wpsd-col">';
            $html .= '<p>';
                $html .= __( 'There are some really great PlugIns dedicated to form building and integrating them with WPSuperDealer is fairly easy.', 'wp-super-dealer' );
            $html .= '</p>';
            $html .= '<p>';
                $html .= __( "This is why WPSuperDealer, unlike it's predecessor, does not include it's own forms.", "wp-super-dealer" );
            $html .= '</p>';
            $html .= '<p>';
                $html .= __( "While it's possible to use the native WordPress comment system to collect lead information on vehicles, we advise against this.", "wp-super-dealer" );
            $html .= '</p>';
            $html .= '<p>';
                $html .= __( 'We suggest leveraging one of the many existing PlugIns for form creation.', 'wp-super-dealer' );
            $html .= '</p>';
            $html .= '<p>';
                $html .= __( "If you don't already use a PlugIn for building forms then we suggest looking at ", "wp-super-dealer" );
                $html .= '<a href="https://wordpress.org/plugins/contact-form-7/" target="_blank">';
                    $html .= __( 'Contact Form 7', 'wp-super-dealer' );
                $html .= '</a>. ';
                $html .= __( "It's free, easy to use and has an impressive amount of power.", "wp-super-dealer" );
            $html .= '</p>';
            $html .= '<p>';
                $html .= __( "Listed on this page you'll see a button with their name on it.", "wp-super-dealer" );
            $html .= '</p>';
            $html .= '<p>';
                $html .= __( "If you click their button you'll find some quick tips on integrating with it.", "wp-super-dealer" );
            $html .= '</p>';
            $html .= '<p>';
                $html .= __( "There's also a button for 'Other' that has some basic information to get you started with Gravity Forms, NinjaForms, WeForms and more.", "wp-super-dealer" );
            $html .= '</p>';
            $html .= '<p>';
                $html .= __( "Check the blog on our website for more information on setting up forms with CRM integration and sending adfxml attachments.", "wp-super-dealer" );
            $html .= '</p>';
            $html .= '<p>';
                $html .= __( "If you need help we offer a range of services and can provide you with an estimate to assist you.", "wp-super-dealer" );
            $html .= '</p>';
            $html .= '<p>';
                $html .= __( 'Just swing by our website, ', 'wp-super-dealer' );
                $html .= '<a href="https://wpsuperdealer.com/" target="_blank">';
                    $html .= __( 'WPSuperDealer.com', 'wp-super-dealer' );
                $html .= '</a> ';
                $html .= __( 'and let us know.', 'wp-super-dealer' );
            $html .= '</p>';
            $html .= '<h3>';
                $html .= __( '~ Unleash your hidden Hero ~', 'wp-super-dealer' );
            $html .= '</h3>';
        $html .= '</div>';
    
        $html .= '<div class="wpsd-col">';
            $html .= '<p>';
                $html .= '<img class="wpsd-forms-tab-img" src="' . WPSD_PATH . '\images\call in the sidekicks.png" />';
            $html .= '</p>';
            $html .= '<img class="wpsd-forms-tab-img" src="' . WPSD_PATH . '\images\WPSD Form Integration.png" />';
            $html .= '<div class="wpsd-clear"></div>';
        $html .= '</div>';

        $html .= '<div class="wpsd-clear"></div>';
        $html .= '<h3>';
            $html .= __( 'For more information please swing by our website ', 'wp-super-dealer' );
            $html .= '<a href="https://wpsuperdealer.com/" target="_blank">';
                $html .= __( 'WPSuperDealer.com', 'wp-super-dealer' );
            $html .= '</a> ';
        $html .= '</h3>';
    
    $html .= '</div>';
    $html .= '<div class="wpsd-clear"></div>';
    return $html;
}

function wpsd_forms_tab_contactform7() {
    $html = '';
    $html .= '<div class="">';
        $html .= '<h2>' . __( 'Integrating Contact Form 7 with WPSuperDealer', 'wp-super-dealer' ) . '</h2>';
        $html .= '<p><a href="https://wordpress.org/plugins/contact-form-7/" target="_blank"><img class="wpsd-forms-tab-img" width="96" height="96" src="' . WPSD_PATH . '/images/Contact-Form-7.png" alt="Contact-Form-7"></a></p>';
        $html .= '<p>' . __( 'Today we’re going to walk through creating a form with Contact Form 7 and then populating some hidden fields in that form with vehicle data. This will give us the ability to drop a form onto a single vehicle page with a widget and pass the current vehicle data when it is submitted.', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( 'Let’s get started! The first two steps are easy, just install a couple PlugIns:', 'wp-super-dealer' ) . '</p>';
        $html .= '<h4>' . __( '1. Download & install the ', 'wp-super-dealer' ) . '<a href="https://wordpress.org/plugins/contact-form-7/" target="_blank">Contact Form 7</a> PlugIn</h4>';
        $html .= '<h4>' . __( '2. Download & install the ', 'wp-super-dealer' ) . '<a href="https://wordpress.org/plugins/code-snippets/" target="_blank">Code Snippets</a> PlugIn</h4>';
        $html .= '<h4>' . __( '3. Create a new Form', 'wp-super-dealer' ) . '</h4>';
        $html .= '<p>' . __( 'Go to Dashboard-&gt;Contact-&gt;Add New Give it a descriptive name. For example, if you intend to put this form on the single vehicle pages then you could name it “Contact Single Vehicle Page”', 'wp-super-dealer' ) . '</p>';
        $html .= '<h4>' . __( '4. Inserting code into the body of the form', 'wp-super-dealer' ) . '</h4>';
        $html .= '<p>' . __( 'To set up our form we’re going to create several standard fields, like name and email. We’ll also add a section at the end to store the vin, stock number, etc. We wrap the vehicle data in a div tag that has it’s CSS style set to none so the visitors won’t see them.', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( 'You can customize the form how you see fit, adding or removing any fields you require.', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( 'After saving and publishing your form you should copy the shortcode that you see at the top of the page so you can get the ID number from it to use later.', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( 'Insert the following as the Body of your form', 'wp-super-dealer' ) . '</p>';
        $html .= '<p><pre>';
            $html .= '&lt;p&gt;Your Name (required)&lt;br /&gt;';
            $html .= '<br />';
            $html .= '[text* your-name] &lt;/p&gt;';
            $html .= '<br />';
            $html .= '<br />';
            $html .= '&lt;p&gt;Email (required)&lt;br /&gt;';
            $html .= '<br />';
            $html .= '[email* email] &lt;/p&gt;';
            $html .= '<br />';
            $html .= '<br />';
            $html .= '&lt;p&gt;Phone (required)&lt;br /&gt;';
            $html .= '<br />';
            $html .= '[tel* phone] &lt;/p&gt;';
            $html .= '<br />';
            $html .= '<br />';
            $html .= '&lt;p&gt;Your Message&lt;br /&gt;';
            $html .= '<br />';
            $html .= '[textarea your-message] &lt;/p&gt;';
            $html .= '<br />';
            $html .= '<br />';
            $html .= '&lt;div id="vehicle-wrap" style="display:none;"&gt;';
            $html .= '<br />';
            $html .= '&lt;p&gt;[text* vin default:shortcode_attr]&lt;/p&gt;';
            $html .= '<br />';
            $html .= '&lt;p&gt;[text* stock_number default:shortcode_attr]&lt;/p&gt;';
            $html .= '<br />';
            $html .= '&lt;p&gt;[text* vehicle_year default:shortcode_attr]&lt;/p&gt;';
            $html .= '<br />';
            $html .= '&lt;p&gt;[text* make default:shortcode_attr]&lt;/p&gt;';
            $html .= '<br />';
            $html .= '&lt;p&gt;[text* model default:shortcode_attr]&lt;/p&gt;';
            $html .= '<br />';
            $html .= '&lt;p&gt;[text* price default:shortcode_attr]&lt;/p&gt;';
            $html .= '<br />';
            $html .= '&lt;/div&gt;';
            $html .= '<br />';
            $html .= '&lt;p&gt;[submit "Send"]&lt;/p&gt;';
        $html .= '</pre></p>';

        $html .= '<h4>' . __( '5. Create a new ', 'wp-super-dealer' ) . '<a href="https://wordpress.org/plugins/code-snippets/" target="_blank">Code Snippet</a></h4>';
        $html .= '<p>' . __( 'Now we need to get tricky and use the code Snippet PlugIn to add a little custom PHP code. Don’t worry, it’s easy to do.', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( 'Go to Dashboard-&gt;Snippets-&gt;Add New', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( 'Give the snippet a descriptive name like, “Contact Form 7 Default values filter”.&nbsp;', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( 'Now paste the following code into the “Code” box.', 'wp-super-dealer' ) . '</p>';
        $html .= '<pre>';
            $html .= 'add_filter( \'shortcode_atts_wpcf7\', \'custom_shortcode_atts_wpcf7_filter\', 10, 3 );';
            $html .= '<br />';
            $html .= 'function custom_shortcode_atts_wpcf7_filter( $out, $pairs, $atts ) {';
            $html .= '<br />';
                $html .= '&nbsp;&nbsp;';
                $html .= '//= create an array of vehicle specifications that we want to make available in Contact Form 7 as shortcode attributes';
                $html .= '<br />';
                $html .= '&nbsp;&nbsp;';
                $html .= '$attributes = array(';
                    $html .= '<br />';
                    $html .= '&nbsp;&nbsp;&nbsp;&nbsp;';
                    $html .= '\'vin\',';
                    $html .= '<br />';
                    $html .= '&nbsp;&nbsp;&nbsp;&nbsp;';
                    $html .= '\'stock_number\',';
                    $html .= '<br />';
                    $html .= '&nbsp;&nbsp;&nbsp;&nbsp;';
                    $html .= '\'vehicle_year\',';
                    $html .= '<br />';
                    $html .= '&nbsp;&nbsp;&nbsp;&nbsp;';
                    $html .= '\'make\',';
                    $html .= '&nbsp;&nbsp;&nbsp;&nbsp;';
                    $html .= '<br />';
                    $html .= '&nbsp;&nbsp;&nbsp;&nbsp;';
                    $html .= '\'model\',';
                    $html .= '<br />';
                    $html .= '&nbsp;&nbsp;&nbsp;&nbsp;';
                    $html .= '\'price\',';
                    $html .= '<br />';
                    $html .= '&nbsp;&nbsp;&nbsp;&nbsp;';
                    $html .= '\'dealer-name\',';
                    $html .= '<br />';
                    $html .= '&nbsp;&nbsp;&nbsp;&nbsp;';
                    $html .= '\'dealer-address\',';
                    $html .= '<br />';
                    $html .= '&nbsp;&nbsp;&nbsp;&nbsp;';
                    $html .= '\'dealer-id\',';
                    $html .= '<br />';
                $html .= '&nbsp;&nbsp;';
                $html .= ');';
                $html .= '<br />';
                $html .= '&nbsp;&nbsp;';
                $html .= 'foreach( $attributes as $attribute ) { ';
                    $html .= '<br />';
                    $html .= '&nbsp;&nbsp;&nbsp;&nbsp;';
                    $html .= 'if ( isset( $atts[ $attribute ] ) ) { ';
                        $html .= '<br />';
                        $html .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                        $html .= '$out[ $attribute ] = $atts[ $attribute ];';
                    $html .= '<br />';
                    $html .= '&nbsp;&nbsp;&nbsp;&nbsp;';
                    $html .= '}';
                $html .= '<br />';
                $html .= '&nbsp;&nbsp;';
                $html .= '}';
                $html .= '<br />';
                $html .= '&nbsp;&nbsp;';
                $html .= 'return $out;';
            $html .= '<br />';
            $html .= '}';
        $html .= '</pre>';

        $html .= '<p>' . __( 'What’s this code doing?', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( 'It’s registering attributes to be used with the Contact Form 7 shortcode.', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( 'Do you see the array that lists vin, stock_number, vehicle_year, etc.?', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( 'Those are fields we want the Contact Form 7 shortcode to recognize.', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( 'In the next step we’ll show you how to use them.', 'wp-super-dealer' ) . '</p>';

        $html .= '<h4>' . __( '6. Add a widget to your Single Vehicle pages', 'wp-super-dealer' ) . '</h4>';
        $html .= '<p>' . __( 'There are several ways to add content to your single vehicle pages. But the easiest is to use one of the sidebar areas that are defined for us. You can find them by going to Dashboard-&gt;Appearance-&gt;Widgets', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( 'To make things simple let’s use the “Single Vehicle After” sidebar area.', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( 'Grab a “Custom HTML” widget and drop it into the “Single Vehicle After” sidebar area.', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( 'Remember earlier when we asked you to copy the shortcode for the form, because you’d need the ID from it later?', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( 'This is when you need to grab the id, let’s say it was 568.', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( 'Grab the following shortcode, replace the sample id (7576) with your id (568 is our example) and paste it into the “Custom HTML” widget you dropped into the “Single Vehicle After” sidebar area.', 'wp-super-dealer' ) . '</p>';
        $html .= '<pre>[contact-form-7 id="7576" title="Contact Single Vehicle Page (VDP)" vin="{vin}" stock_number="{stock_number}" vehicle_year="{year}" make="{make}" model="{model}" price="{price}"]</pre>';
        $html .= '<p>' . __( 'You see how we added each of the fields we registered earlier into the shortcode as attributes?', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( 'You’ll notice each field has a value which is the same as the field name, except it’s inside braces.', 'wp-super-dealer' ) . '</p>';
        $html .= '<pre>example: vin=”{vin}”</pre>';
        $html .= '<p>' . __( 'On single vehicle pages WPSuperDealer scans text and HTML widgets and looks for any vehicle specification slug wrapped in braces and replaces it with the value from the vehicle.', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( 'You can see this in action by adding another HTML widget and entering {year} {make} {model} into it.', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( 'When you view that on a single vehicle page it’ll show the values from the vehicle.', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( 'So when we drop the values into the shortcode they get replaced with actual values before the page is output.', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( 'This gives us a form on our page that will tell us what vehicle they were looking at when they filled out the form.', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( 'Now you know one of the easiest ways to integrate a form into your single vehicle pages and capture the vehicle details.', 'wp-super-dealer' ) . '</p>';

        $html .= '<h3>' . __( 'Bonus Points!', 'wp-super-dealer' ) . '</h3>';
        $html .= '<p>' . __( 'Using the above information try pre-populating the comment field with something like:', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( 'I’d like more information on stock # ???', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( 'but with the ??? replaced with the actual value from the vehicle', 'wp-super-dealer' ) . '</p>';
        $html .= '<p><b>' . __( 'Hint:</b>', 'wp-super-dealer' ) . __( ' add your-message to the array in the code snippet that registers our other vehicle fields. Then add the following parameter to your shortcode:', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( 'your-message=”I would like to know more about stock # {stock_number}. “', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( 'Your shortcode should now look like this:', 'wp-super-dealer' ) . '</p>';
        $html .= '<pre>[contact-form-7 id="586" title="Contact Single Vehicle Page (VDP)" your-message="I would like to know more about stock # {stock_number}. " vin="{vin}" stock_number="{stock_number}" vehicle_year="{year}" make="{make}" model="{model}" price="{price}"]</pre>';
    $html .= '</div>';    
    return $html;
}

function wpsd_forms_tab_other() {
    $html = '';
    $html .= '<div class="">';
        $html .= '<h2>' . __( 'Integrating other form PlugIns with WPSuperDealer', 'wp-super-dealer' ) . '</h2>';
        $html .= '<h3>';
            $html .= '<span class="wpsd-question">General Guidelines</span>';
        $html .= '</h3>';
        $html .= '<p>' . __( 'There are several really powerful tools for building forms in WordPress.', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( "It isn't possible to list them all or provide instructions for all of them right here.", "wp-super-dealer" ) . '</p>';
        $html .= '<p>' . __( 'But we can give some general guidelines to get you started.', 'wp-super-dealer' ) . '</p>';
    
        $html .= '<ol>';
            $html .= '<li>' . __( "Check our website, as time permits we'll post specific instructions for some of the more popular PlugIns.", "wp-super-dealer" ) . '</li>';
            $html .= '<li>' . __( 'Start simple and build from there. Save the adfxml until after you get a basic vehicle contact form to work.', 'wp-super-dealer' ) . '</li>';
            $html .= '<li>' . __( 'All text and html sidebar widgets on single vehicle pages will replace vehicle data tags with the value from the current vehicle.', 'wp-super-dealer' ) . '</li>';
            $html .= '<li>' . __( 'Look at the Contact Form 7 example for a deeper look at vehicle data tags.', 'wp-super-dealer' ) . '</li>';
            $html .= '<li>' . __( 'If you use our Template Manager add-on then you can drop your forms into the template with shortcodes or using the native WordPress do_shortcode() function anywhere you want.', 'wp-super-dealer' ) . '</li>';
            $html .= '<li>' . __( 'If you are the developer of a PlugIn that you wish to integrate with WPSuperDealer then swing by our website and introduce yourself.', 'wp-super-dealer' ) . '</li>';
        $html .= '</ol>';
    
        $html .= '<p>' . __( "Are you fighting with a CRM integration? Struggling with adfxml? Battling with dynamically routed emails going multiple sales managers, all while trying to figure out why they have more than one sales manager?", "wp-super-dealer" ) . '</p>';
        $html .= '<p>' . __( 'It can get overwhelming at times, especially when you start split testing changes and begin optimizing your traffic.', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( 'Building an awesome vehicle sales website is more than just making your listings look awesome.', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( 'At the end of the day your website probably has one primary goal, driving qualified leads.', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( 'Properly integrating your forms is a key componant to maximizing lead generation, which is probably the whole point.', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( 'Combined with call tracking they become the most important part of a vehicle sales website.', 'wp-super-dealer' ) . '</p>';
        $html .= '<p>' . __( 'We offer a wide range of services, including white label, to assist you in maximizing lead generation so your website can be the Hero it was meant to be.', 'wp-super-dealer' ) . '</p>';
    
        $html .= '<div class="wpsd-clear"></div>';
        $html .= '<h3>';
            $html .= __( 'For more information please swing by our website ', 'wp-super-dealer' );
            $html .= '<a href="https://wpsuperdealer.com/custom-projects/" target="_blank">';
                $html .= __( 'WPSuperDealer.com', 'wp-super-dealer' );
            $html .= '</a> ';
        $html .= '</h3>';
    
    $html .= '</div>';
	return $html;
}
?>