<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/*
TO DO:
Save search json
Cars must be marked not sold
Look for 'Not Cars' and pre-fill vehicle type
Import Locations
Import similar settings
*/

function wpsd_car_demon_settings() {
	$html ='';
	$html = apply_filters( 'wpsd_admin_car_demon_settings_filter', $html );
	if ( ! empty( $html ) ) {
		return $html;
	}
    if ( ! defined( 'CAR_DEMON_VER' ) ) {
        return $html;
    }
    global $wpsd_options;
	$form_fields = new WP_SUPER_DEALER\WPSD_FIELDS;
	$time = time();

    $html .= '<h3>';
        $html .= __( 'Car Demon Options', 'wp-super-dealer' );
    $html .= '</h3>';
    $html .= wpsd_cd_settings_instructions();

    $fields = array();

    $content = '';

    $message = '';
    $imported = get_option( 'wpsd_cd_imported', false );
    if ( $imported ) {
        $message = __( 'You have already imported Car Demon: ', 'wp-super-dealer' );
        $message .= date( 'Y-m-d h:i', $imported );
    }
    $content .= '<div class="wpsd-cd-import-message">' . esc_html( $message ) . '</div>';

    $content .= '<div class="wpsd-cd-installed">';
        $content .= __( 'It looks like you have Car Demon version ', 'wp-super-dealer' );
        $content .= esc_html( CAR_DEMON_VER );
        $content .= __( ' installed!', 'wp-super-dealer' );
    $content .= '</div>';

    $content .= wpsd_cd_note();
    $label = __( 'Car Demon Settings and Vehicle Review', 'wp-super-dealer' );
    $html .= wpsd_admin_group_wrap( $label, $content, $fields );

    $content = '';
    $content .= wpsd_cd_vehicle_type();
    $label = __( 'Create a new vehicle type', 'wp-super-dealer' );
    $html .= wpsd_admin_group_wrap( $label, $content, $fields );
    
    $content = '';
    $content .= '<h4>';
        $content .= __( 'This tool will create a new vehicle type & import your vehicles from Car Demon.', 'wp-super-dealer' );
    $content .= '</h4>';
    $content .= '<h4>';
        $content .= __( 'You will still need to replace shortcodes on your pages and manually adjust your settings.', 'wp-super-dealer' );
    $content .= '</h4>';
    $content .= '<h4>';
        $content .= __( 'Click the button to begin.', 'wp-super-dealer' );
    $content .= '</h4>';
    $content .= '<p class="wpsd-cd-agree-wrap">';
        $content .= '<input type="checkbox" value="1" name="" class="wpsd-cd-agree" />';
        $content .= '<label class="">';
            $content .= __( 'I have backed up my site, know how to restore it if something goes wrong and hereby agree to accept all responsibility should an issue arise.', 'wp-super-dealer' );
        $content .= '</label>';
    $content .= '</p>';
    $content .= '<input type="button" class="wpsd-cd-import-button" data-action="import" value=" ' . __( 'Import to WPSuperDealer', 'wp-super-dealer' ) . '" />';
    $content .= '<h4>';
        $content .= __( 'When you\'re finished don\'t forget to deactivate any Car Demon add-ons and then lastly, Car Demon itself.', 'wp-super-dealer' );
    $content .= '</h4>';
    $message = '';
    $imported = get_option( 'wpsd_cd_imported', false );
    if ( $imported ) {
        $message = __( 'You have already imported Car Demon: ', 'wp-super-dealer' );
        $message .= date( 'Y-m-d h:i', $imported );
    }
    $content .= '<div class="wpsd-cd-import-message">' . esc_html( $message ) . '</div>';
    $label = __( 'Get this party started', 'wp-super-dealer' );
    $html .= wpsd_admin_group_wrap( $label, $content, $fields );
    
    return $html;
}

function wpsd_cd_note() {
    $html = '';
    $html .= '<div class="wpsd-cd-note">';
        $html .= '<h3>';
            $html .= __( 'It\'s time to upgrade Car Demon to WPSuperDealer!', 'wp-super-dealer' );
        $html .= '</h3>';
        $html .= '<ul>';
            $html .= '<li>';
                $html .= __( 'As of October 2020 Car Demon is 7 years old.', 'wp-super-dealer' );
            $html .= '</li>';
            $html .= '<li>';
                $html .= __( 'In software years version 1.x is well past the time of retirement and ready to be replaced by version 2.x.', 'wp-super-dealer' );
            $html .= '</li>';
            $html .= '<li>';
                $html .= __( 'The problem? There is no Car Demon version 2.x', 'wp-super-dealer' );
            $html .= '</li>';
            $html .= '<li>';
                $html .= __( 'But you\'re in luck! WPSuperDealer is the official replacement for Car Demon!', 'wp-super-dealer' );
            $html .= '</li>';    
            $html .= '<li>';
                $html .= __( 'Think of it as Car Demon version 2.x with a brand new name.', 'wp-super-dealer' );
            $html .= '</li>';    
            $html .= '<li>';
                $html .= __( 'The two PlugIns are quite different, but we\'ve put together a tool to help make the move a bit easier', 'wp-super-dealer' );
            $html .= '</li>';
            $html .= '<li>';
                $html .= __( '', 'wp-super-dealer' );
            $html .= '</li>';
        $html .= '</ul>';
    $html .= '</div>';
    return $html;
}

function wpsd_cd_vehicle_type() {
    $html = '';
    
    $html .= '<div class="wpsd-cd-options-wrap">';
        $html .= '<div class="wpsd-cd-options-item wpsd-cd-options-item-box">';
            $html .= '<h3>';
                $html .= __( 'Import Car Demon Vehicle Options into a new vehicle type in WPSuperDealer.', 'wp-super-dealer' );
            $html .= '</h3>';
            $html .= '<div class="wpsd-clear">&nbsp;</div>';
            $html .= '<div class="wpsd-cd-group-wrap-options">';
                $html .= '<small>';
                    $html .= __( 'WPSuperDelaer supports multiple vehicle types.', 'wp-super-dealer' );
                $html .= '</small>';
                $html .= '<small>';
                    $html .= __( 'Your vehicle options from Car Demon will be imported into their own custom vehicle type so your specifications and options will match.', 'wp-super-dealer' );
                $html .= '</small>';
                $html .= '<small>';
                    $html .= __( 'After importing your vehicle options you can import your vehicles and they will be assigned to the vehicle type you enter.', 'wp-super-dealer' );
                $html .= '</small>';
    
                $html .= '<label>';
                    $html .= __( 'New Vehicle Type Name', 'wp-super-dealer' );
                $html .= '</label>';
                $html .= '<span>';
                    $value = __( 'Automobile', 'wp-super-dealer' );
                    $html .= '<input type="text" name="wpsd-cd-vehicle-type" class="wpsd-cd-vehicle-type" value="' . esc_attr( $value ) . '" />';
                $html .= '</span>';
                $html .= '<small>';
                    $html .= __( 'Enter a name for your new vehicle type. If that type already exists then it will be replaced by this tool.', 'wp-super-dealer.com' );
                $html .= '</small>';

                if ( defined( 'WPSD_SHOW_WPSD_SPECS' ) ) {
                    $cd_specs = wpsd_cd_specs();
                    $html .= '<div class="wpsd-cd-specs">';
                        foreach( $cd_specs as $group=>$cd_spec ) {
                            $html .= '<div class="wpsd-cd-spec">';
                                $html .= '<label>';
                                    $html .= esc_html( $group );
                                $html .= '</label>';
                                $html .= '<textarea name="" class="" data-group="' . esc_attr( $group ) . '" disabled="disabled">';
                                    $html .= esc_html( $cd_spec );
                                $html .= '</textarea>';
                            $html .= '</div>';
                        }
                    $html .= '</div>';

                    $cd_options = wpsd_cd_options();
                    $html .= '<div class="wpsd-cd-options">';
                        foreach( $cd_options as $group=>$cd_option ) {
                            $html .= '<div class="wpsd-cd-spec">';
                                $html .= '<label>';
                                    $html .= esc_html( $group );
                                $html .= '</label>';
                                $html .= '<textarea name="" class="" data-group="' . esc_attr( $group ) . '" disabled="disabled">';
                                    $html .= print_r( esc_html( $cd_option, true ) );
                                $html .= '</textarea>';
                            $html .= '</div>';
                        }
                    $html .= '</div>';
                }
            $html .= '</div>';
        $html .= '</div>';
    $html .= '</div>';
    return $html;
}

function wpsd_cd_specs() {
    $map = cd_get_vehicle_map();
    $specs_map = $map['specs'];
    return $specs_map;
}

function wpsd_cd_options() {
    $map = cd_get_vehicle_map();
    $options_array = array();
    $safety = array();
    $option_groups = array( 'safety', 'convenience', 'comfort', 'entertainment' );
    foreach( $option_groups as $option_group ) {
        foreach( $map[ $option_group ] as $group=>$options ) {
            $options_array[ $option_group ] = $options;
        }
    }
    return $options_array;
}

function wpsd_cd_settings_instructions() {
    $html = '';
	$path = WPSD_PATH;
	$html .= '<div class="wpsd-instructions-wrap wpsd-instructions-wrap-car_demon-options">';
		$html .= '<div class="wpsd-instructions-title" data-tab="car_demon-options">';
			$html .= '<span class="dashicons dashicons-welcome-learn-more"></span>';
			$html .=  __( 'Tips & Tricks - Upgrading from Car Demon', 'wp-super-dealer' );
		$html .= '</div>';

		$html .=  '<ul class="wpsd-instructions">';
			$html .= '<li>';
                $html .= '<div class="wpsd-tnt-wrap">';
                    $html .= '<div class="wpsd-tnt-col">';
                        $html .= '<img class="wpsd-tnt-logo" src="' . esc_url( $path ) . 'images/upgrade car demon to wpsuperdealer.jpg" />';
//						$html .= '<div class="wpsd-video-wrap">';
//							$html .= '<iframe src="https://www.youtube.com/embed/oWtJaUt3yUs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
//						$html .= '</div>';
                    $html .= '</div>';
                    $html .= '<div class="wpsd-tnt-col">';
                        $html .= '<h2>';
                            $html .=  __( 'Tips & Tricks for upgrading from Car Demon', 'wp-super-dealer' );
                        $html .= '</h2>';
                        $html .= '<ol>';
                            $html .= '<li>';
                                $html .= __( 'This tool will import your vehicle options and vehicles from Car Demon, but you will still need to update your pages with the correct blocks, shortcodes or widgets.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .= __( 'Back up your site before you do anything.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .= __( 'Download the backup of your site and put it in a safe place.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .= __( 'Make sure you know how to rapidly restore your site if something goes not so awesome.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .= __( 'Clone your site and test this on a staging or development site first.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .= __( 'Once you\'re done please deactivate any Car Demon add-ons and then lastly, Car Demon itself.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .= __( 'When Car Demon is deactivated this tool will disappear.', 'wp-super-dealer' );
                            $html .= '</li>';
                        $html .= '</ol>';
                    $html .= '</div>';
                $html .= '</div>'; //= end $html .= '<div class="wpsd-tnt-wrap">';
                $html .= wpsd_instructions_footer();
			$html .= '</li>';
		$html .= '</ul>';
		$html .= '<div class="wpsd-instructions-hide" data-tab="car_demon-options">';
			$html .=  __( 'show', 'wp-super-dealer' );
		$html .= '</div>';
	$html .= '</div>';
    return $html;
}

function wpsd_car_demon_settings_save( $args ) {
	//= Get the global settings so we can update them
	global $wpsd_options;
	if ( ! isset( $args[0]['fields'] ) ) {
		return __( 'No fields found to update.', 'wp-super-dealer' );
	}
	
	foreach( $args[0]['fields'] as $field=>$value ) {
		//= Swap dashes for underscores
		$field = str_replace( '-', '_', $field );
		$wpsd_options[ $field ] = sanitize_text_field( $value );
	}
	//= Update the options
	update_option( 'wpsd_options', $wpsd_options );
    
    //= Set a flag to flush the rewrite the next time an admin user reloads a page
    update_option( 'wpsd_flush_rewrite', time() );
    
	return __( 'Settings Saved', 'wp-super-dealer' );
}

//===========================================================
//= Start the import functions
//===========================================================
add_action( 'wp_ajax_wpsd_car_demon_import_handler', 'wpsd_car_demon_import_handler' );
function wpsd_car_demon_import_handler() {
	if ( ! is_admin() ) {
		return;
	}
	if ( ! current_user_can( WPSD_ADMIN_CAP ) ) {
		return;
	}
    $nonce = $_POST['nonce'];
	if ( ! wp_verify_nonce( $nonce, 'admin-get-this-party-started-nonce' ) ) {
		echo esc_html( __( 'Nonce check failed - no changes saved.', 'wp-super-dealer' ) );
		exit();
	}

    $return = array();
    $return['vehicle_type'] = sanitize_text_field( $_POST['type'] );
    if ( isset( $_POST['details'] ) ) {
        $return['details'] = array_map( 'sanitize_text_field', $_POST['details'] );
    }
    if ( isset( $_POST['vehicles'] ) ) {
        $return['vehicles'] = array_map( 'sanitize_text_field', $_POST['vehicles'] );
    }

    $slug = wpsd_cd_create_vehicle_type( $return );
    //= Import vehicles
    $args = array(
        'vehicle_type' => $slug
    );
    wpsd_cd_vehicles_import( $args );

    //= update the search json... what?! that function name makes total sense...
    //= TO DO Future: is there a better function to trigger building the json - if not, maybe fake one ;-p
    wpsdsp_schedule();
    
    $time = time();
    update_option( 'wpsd_cd_imported', $time, false );
    
    wpsdsp_delete_cache();
    $return['response'] = wpsdsp_build_cache( true );
    
    $return['message'] = __( 'Your vehicles have been imported.', 'wp-super-dealer' );
    
    $json = json_encode( $return );
    echo wpsd_kses_json( $json );
    
    exit();
}

function wpsd_cd_create_vehicle_type( $return ) {
    $cd_map = cd_get_vehicle_map();
	$types = get_option( 'wpsd-vehicle-types', array() );

    $cd_labels = get_default_field_labels();
    $cd_global_specs = array(
        'vin',
        'stock_number',
        'mileage',
        'body_style',
    );
    $first = true;
    $cd_spec_groups = $cd_map['specs'];
    $specs = array();
    foreach( $cd_spec_groups as $cd_spec_group=>$cd_specs ) {
        $specs[ $cd_spec_group ] = array();
        
        if ( $first ) {
            foreach( $cd_global_specs as $cd_global_spec ) {
                if ( 'stock_number' === $cd_global_spec ) {
                    $spec = 'stock_num';
                } else {
                    $spec = $cd_global_spec;
                }
                if ( isset( $cd_labels[ $spec ] ) ) {
                    $label = $cd_labels[ $spec ];
                } else {
                    $label = $cd_global_spec;
                    $label = str_replace( '_', ' ', $label );
                    $label = ucwords( $label );
                }
                $specs[ $cd_spec_group ][ $cd_global_spec ] = array(
                    'name' => $cd_global_spec,
                    'taxonomy' => false,
                    'disabled' => false,
                    'restrict' => false,
                    'show-ui' => false,
                    'plural' => $cd_global_spec,
                    'label' => $label,
                    'default-value' => '',
                );
            }
            $first = false;
        }
        
        if ( false !== strpos( $cd_specs, ',' ) ) {
            $specs_array = explode( ',', $cd_specs );
            if ( is_array( $specs_array ) && 0 < count( $specs_array ) ) {
                foreach( $specs_array as $spec_name ) {
                    $spec_name = trim( $spec_name );
                    if ( isset( $cd_labels[ $spec_name ] ) ) {
                        $label = $cd_labels[ $spec_name ];
                    } else {
                        $label = $spec_name;
                        $label = str_replace( '_', ' ', $label );
                        $label = ucwords( $label );
                    }
                    $specs[ $cd_spec_group ][ $spec_name ] = array(
                        'name' => $spec_name,
                        'taxonomy' => false,
                        'disabled' => false,
                        'restrict' => false,
                        'show-ui' => false,
                        'plural' => $spec_name,
                        'label' => $label,
                        'default-value' => '',
                    );
                } //= end foreach( $specs_array as $spec_name ) {
            } //= end if ( is_array( $specs_array ) && 0 < count( $specs_array ) ) {
        } //= end if ( false !== strpos( $cd_specs, ',' ) ) {
    } //= end foreach( $cd_spec_groups as $cd_spec_group=>$cd_specs ) {

    $slug = str_replace( ' ', '_', $return['vehicle_type'] );
    $slug = strtolower( $slug );
    $options = array();

    //= handle no groups in options
    global $car_demon_options;
	if ( isset( $car_demon_options['hide_tabs'] ) ) {
		if ( $car_demon_options['hide_tabs'] == __( 'Yes', 'wp-super-dealer' ) ) {
            $vehicle_options = get_option( 'wpsd-vehicle-options' );
            $vehicle_options['no_group_options'][ $slug ] = true;
            $vehicle_options['group_options_list'][ $slug ] = true;
            update_option( 'wpsd-vehicle-options', $vehicle_options, true );
            $options = array( 'default' => array( 'default' ) );
        }
    } else {
        $options['safety'] = $cd_map['safety'];
        $options['convenience'] = $cd_map['convenience'];
        $options['comfort'] = $cd_map['comfort'];
        $options['entertainment'] = $cd_map['entertainment'];
    }

    $type_details = array (
        'label' => $return['vehicle_type'],
        'icon' => '/wp-content/plugins/wp-super-dealer/images/other-icon.jpg',
        'specifications' => $specs,
        'options' => $options,
    );
    $types[ $slug ] = $type_details;
    
    update_option( 'wpsd-vehicle-types', $types, false );
    
    return $slug;
}

function wpsd_cd_vehicles_import( $args = array() ) {
    $default_args = array(
        'status' => array( 'publish' ),
        'vehicle_type' => 'automobile',
    );
    $args = wp_parse_args( $args, $default_args );

    global $car_demon_options;
    $query = array();
    $query['car'] = 1;
    $query['cars_per_page'] = -1;
    $car_demon_query = car_demon_query_search( $query );
	$car_query = new WP_Query();
	$car_query->query( $car_demon_query );
	$total_results = $car_query->found_posts;

		if ( $car_query->have_posts() ) : while ( $car_query->have_posts() ) : $car_query->the_post();
			$post_id = $car_query->post->ID;
            $vehicle = wpsd_cd_import_vehicle( $post_id, $args['vehicle_type'] );
    
			endwhile;

		endif;
}

function wpsd_cd_import_vehicle( $cd_post_id, $vehicle_type ) {
    $wpsd_vehicle = array();
    $cd_vehicle = cd_get_car( $cd_post_id );
    
    //= An array for creating the new vehicle's post
    $wpsd_post_args = array(
        'ID' => 0,
        'post_author' => $cd_vehicle['post_author'],
        'post_content' => $cd_vehicle['post_content'],
        'post_title' => $cd_vehicle['post_title'],
        'post_excerpt' => $cd_vehicle['post_excerpt'],
        'post_status' => $cd_vehicle['post_status'],
        'post_name' => $cd_vehicle['post_name'],
        'post_content_filtered' => $cd_vehicle['post_content_filtered'],
        'post_type' => 'vehicle',
    );
    $wpsd_post_id = wp_insert_post( $wpsd_post_args );

    foreach( $cd_vehicle as $key=>$value ) {
        if ( 'decode_string' === $key ) {
            continue;
        }
        if ( '_vehicle_options' === $key ) {
            $key = '_options';
        }
        if ( 'year' === $key ) {
            $key = '_vehicle_year';
        }
        if ( 'msrp' === $key ) {
            $key = '_retail_price';
        }

        $key = str_replace( 'decoded_', '', $key );
        $key = str_replace( '_decoded', '', $key );
        $key = str_replace( 'decoded', '', $key );
        $key = str_replace( '_value', '', $key );
        $key = str_replace( '__', '_', $key );
        $key = str_replace( '/', '_', $key );
        $key = str_replace( '-', '_', $key );
        
        //= clean bad characters
        $bad_chars = array( ',', '.', '(', ')' );
        foreach( $bad_chars as $bad_char ) {
            $key = str_replace( $bad_char, '', $key );
        }

        $key = '_' . $key;
        $key = str_replace( '__', '_', $key );
        update_post_meta( $wpsd_post_id, $key, $value );
    }

    $image_links = get_post_meta( $cd_post_id, '_images_value', true );
    update_post_meta( $wpsd_post_id, '_image_urls', $image_links );

    $options = get_post_meta( $cd_post_id, '_vehicle_options', true );
    update_post_meta( $wpsd_post_id, '_options', $options );
    
    $meta = get_post_meta( $cd_post_id );

    //= save the original vehicle meta data for later reference if needed
    update_post_meta( $wpsd_post_id, '_cd_import', $cd_vehicle );
    update_post_meta( $wpsd_post_id, '_cd_import_meta', $meta );

    //= add new vehicle type
    $tax = wp_set_post_terms( $wpsd_post_id, array( $vehicle_type ), 'vehicle_type' );
    update_post_meta( $wpsd_post_id, '_type', $vehicle_type );

    //= vehicle taxonomy, location, global fields, etc.
    $cd_taxonomies = array(
        'location',
        'year',
        'make',
        'model',
        'condition',
        'body_style',
        'vehicle_tag',
    );
    foreach( $cd_taxonomies as $cd_taxonomy ) {
        //= get the cd vehicle's value for this field
        if ( isset( $cd_vehicle[ $cd_taxonomy ] ) ) {
            $value = $cd_vehicle[ $cd_taxonomy ];
            if ( 'year' === $cd_taxonomy ) {
                $cd_taxonomy = 'vehicle_year';
            }
            //= add the data to a meta field so it can be searched
            update_post_meta( $wpsd_post_id, '_' . $cd_taxonomy, $value );
            //= add term to the taxonomy for WPSD
            wp_set_post_terms( $wpsd_post_id, array( $value ), $cd_taxonomy );
        } else {
            if ( 'vehicle_tag' === $cd_taxonomy ) {
                $terms = wp_get_post_terms( $cd_post_id, $cd_taxonomy );
                if ( ! is_wp_error( $terms ) ) {
                    foreach ( $terms as $term ) {
                        $name = $term->name;
                        //= add term to the taxonomy for WPSD
                        wp_set_post_terms( $wpsd_post_id, array( $name ), 'vehicle_tags' ); //= it has an s at the end in WPSD
                    }
                }
            }
            continue;
        }
    }

    $cd_global_specs = array(
        'vin',
        'stock_number',
        'mileage',
    );

    //= handle attached photos
	$thumbnails = get_children( array( 'post_parent' => $cd_post_id, 'post_type' => 'attachment', 'post_mime_type' =>'image', 'orderby' => 'menu_order ID' ) );
	$array = array();
	foreach( $thumbnails as $thumbnail ) {
        //$thumbnail->ID
        $thumbnail_post = get_post( $thumbnail->ID );
        $thumbnail_post->post_parent = $wpsd_post_id;
        wp_update_post( $thumbnail_post );
	}
    
    //= main photo & gallery photos
    $featured_image_id = get_post_thumbnail_id( $cd_post_id );
    set_post_thumbnail( $wpsd_post_id, $featured_image_id );
    
    //= mark as not sold
    $no = __( 'No', 'wp-super-dealer' );
    update_post_meta( $wpsd_post_id, 'sold', $no );
    
    return $wpsd_vehicle;
}
?>