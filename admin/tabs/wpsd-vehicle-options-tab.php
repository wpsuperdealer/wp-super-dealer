<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_vehicle_options_tab() {
	$html = '';
	$html = apply_filters( 'wpsd_admin_options_filter', $html );
	if ( ! empty( $html ) ) {
		return $html;
	}

	$html .=  '<h3>';
		$html .=  __('Vehicle Options', 'wp-super-dealer');
	$html .=  '</h3>';

    $html .= wpsd_vehicle_options_instructions();

	$html .=  '<legend>';
		$html .= '<span class="dashicons dashicons-admin-site"></span>';
		$html .=  __( 'Global Specifications', 'wp-super-dealer' );
	$html .=  '</legend>';
	$html .=  '<small>';
		$html .=  __( 'These specifications are part of all vehicle types.', 'wp-super-dealer' );
	$html .=  '</small>';

	$global_fields = wpsd_get_global_fields();
	$html .= '<div class="wpsd-global-fields-wrap">';
		foreach( $global_fields as $global_field=>$field_options ) {
			$html .= wpsd_get_spec_item( $global_field, 'global', 'default', $field_options );
		}
	$html .= '</div>';
	
	//= Vehicle Option Start
	$html .=  '<fieldset class="wpsd-admin-vehicle-options">';
		$html .=  '<legend>';
			$html .= '<span class="dashicons dashicons-performance"></span>';
			$html .=  __( 'Vehicle Types', 'wp-super-dealer' );
		$html .=  '</legend>';
		$html .=  '<div>';

			$html .= '<div class="wpsd-type-data-buttons-wrap">';
				$html .= '<div class="wpsd-type-data-button" data-action="type-add">';
					$html .= __( 'Add vehicle type', 'wp-super-dealer' );
				$html .= '</div>';
				$html .= '<input type="text" title="' . __( 'Add vehicle type', 'wp-super-dealer' ) . '" class="wpsd-input wpsd-input-add-vehicle-type">';
			$html .= '</div>'; //= end .wpsd-type-data-buttons-wrap

			$html .= '<div class="wpsd-default-add-types-wrap">';
				$html .= '<small>';
					$html .= __( 'Add one of the default vehicle types', 'wp-super-dealer' );
				$html .= '</small>';
			$html .= '</div>';
			$html .= '<div class="wpsd-clear"></div>';

			$html .= '<div class="wpsd-default-types-wrap">';
				$default_types = wpsd_default_type_maps();
				$html .= '<h3>';
					$html .= __( 'Add a default vehicle type', 'wp-super-dealer' );
				$html .= '</h3>';
				foreach( $default_types as $type=>$options ) {
					$html .= '<div class="wpsd-default-type-item" data-type="' . esc_attr( $type ) . '">';
						$html .= '<label>';
							$html .= esc_html( $options['label'] );
						$html .= '</label>';
						$html .= '<div class="wpsd-default-type-icon-wrap">';
							$html .= '<img src="' . esc_url( $options['icon'] ) . '" />';
						$html .= '</div>';
					$html .= '</div>';
				}
				$html .= '<div class="wpsd-remove-button" data-action="close-default-types">×</div>';
			$html .= '</div>';
	
			$data = array(
				'label' => 'LABEL',
				'icon' => WP_PLUGIN_URL . '/wp-super-dealer/images/ICON.PNG',
				'specifications' => array(),
				'options' => array(),
			);
			$html .= wpsd_get_type_form( 'SLUG', $data );
			$html .= wpsd_get_spec_group( 'SLUG', '', 'SLUG-TYPE' );
			$html .= wpsd_get_spec_item( 'SLUG-SPECIFICATION', 'SLUG-TYPE', 'SLUG-GROUP' );
			$html .= wpsd_get_option_groups( 'SLUG', 'SLUG-GROUP', 'SLUG-TYPE' );
			$html .= wpsd_get_option_sub_group( '', 'SLUG-TYPE', 'SLUG-GROUP', 'SLUG' );
			$html .= wpsd_get_option( 'SLUG', 'SLUG-TYPE', 'SLUG-GROUP', 'SLUG-SUB-GROUP' );

			$types = wpsd_get_type_maps();
			foreach( $types as $type=>$data ) {
				$html .= wpsd_get_type_form( $type, $data );
			} //= end foreach( $types as $type=>$data )
		$html .=  '</div>';
	$html .=  '</fieldset>';

	//= Vehicle Option Stop
	return $html;
}

function wpsd_get_vehicle_options() {
	$defaults = array(
		'global_specs' => wpsd_global_fields(),
		'no_group_specs' => array(),
		'no_group_options' => array(),
		'group_options_list' => array(),
	);
	$vehicle_options = get_option( 'wpsd-vehicle-options', $defaults );
	$vehicle_options['global_specs'] = wp_parse_args( $vehicle_options['global_specs'], $defaults['global_specs'] );
	$default_global_specs = $defaults['global_specs'];

	foreach( $vehicle_options['global_specs'] as $spec=>$options ) {
		//= We dont want to let everyone change certain properties of the global specs
		//= So let's make an array of fields we want to always keep from the default
		$locked_fields = array(
			'locked',
			'taxonomy',
			'allow_taxonomy',
			'hide_edit',
		);
		foreach( $locked_fields as $locked_field ) {
			if ( isset( $default_global_specs[ $spec ][ $locked_field ] ) ) {
				if ( true === $default_global_specs[ $spec ][ $locked_field ] ) {
					if ( empty( $default_global_specs[ $spec ][ $locked_field ] ) ) {
						$vehicle_options['global_specs'][ $spec ][ $locked_field ] = false;
					} else {
						$vehicle_options['global_specs'][ $spec ][ $locked_field ] = true;
					}
				} else {
					$vehicle_options['global_specs'][ $spec ][ $locked_field ] = false;
				}
			} else {
				$vehicle_options['global_specs'][ $spec ][ $locked_field ] = false;
			}
		}
	}
	return $vehicle_options;
}

function wpsd_get_type_form( $type, $data ) {
	$x = '';
	$vehicle_options = wpsd_get_vehicle_options();
	$group_specs = true;
	$custom_css = '';
	if ( isset( $vehicle_options['no_group_specs'] ) ) {
		if ( isset( $vehicle_options['no_group_specs'][ $type ] ) && ( $vehicle_options['no_group_specs'][ $type ] ) ) {
			$group_specs = false;
			$custom_css .= ' wpsd-spec-group-no';
		}
	}

	$group_options = true;
	if ( isset( $vehicle_options['no_group_options'] ) ) {
		if ( isset( $vehicle_options['no_group_options'][ $type ] ) && ( $vehicle_options['no_group_options'][ $type ] ) ) {
			$group_options = false;
			$custom_css .= ' wpsd-option-group-no';
		}
	}

	$group_options_list = false;
	if ( isset( $vehicle_options['group_options_list'] ) ) {
		if ( isset( $vehicle_options['group_options_list'][ $type ] ) && ( $vehicle_options['group_options_list'][ $type ] ) ) {
			$group_options_list = true;
		}
	}

	$x .= '<div class="wpsd-type-wrap wpsd-type-wrap-' . esc_attr( $type . $custom_css ) . '" data-type="' . esc_attr( $type ) . '">';
		$x .= '<label>';
			$x .= '<strong>';
				$x .= '<span class="dashicons dashicons-admin-generic"></span>';
				$x .= __( 'Vehicle Type: ', 'wp-super-dealer' );
			$x .= '</strong>';
			$x .= esc_html( $data['label'] );
		$x .= '</label>';
		$x .= '<div class="wpsd-type-data-wrap" data-type="' . esc_attr( $type ) . '">';
			$x .= '<div class="wpsd-type-data-buttons-no-width-wrap">';
				$x .= '<div class="wpsd-type-data-button" data-action="type-remove" data-type="' . esc_attr( $type ) . '">';
					$x .= __( 'Remove this vehicle type', 'wp-super-dealer' );
				$x .= '</div>';
				//= TO DO Future: Add reset to default button for built in vehicle types
				//$x .= '<div class="wpsd-type-data-button" data-action="type-reset" data-type="' . $type . '">';
					//$x .= __( 'Reset to default', 'wp-super-dealer' );
				//$x .= '</div>';
			$x .= '</div>'; //= end .wpsd-type-data-buttons-wrap
			$x .= '<div class="wpsd-type-icon-wrap">';
				$x .= '<img class="wpsd-type-icon" src="' . esc_url( $data['icon'] ) . '">';
				$x .= '<input type="hidden" class="wpsd-type-icon-url" value="' . esc_attr( $data['icon'] ) . '" />';
				$x .= '<div class="wpsd-type-data-buttons-no-width-wrap">';
					$x .= '<div class="wpsd-type-data-button" data-action="icon-change" data-type="' . esc_attr( $type ) . '">';
						$x .= __( 'Change Icon', 'wp-super-dealer' );
					$x .= '</div>';
					$x .= '<div class="wpsd-type-data-button" data-action="icon-remove" data-type="' . esc_attr( $type ) . '">';
						$x .= __( 'Remove Icon', 'wp-super-dealer' );
					$x .= '</div>';
				$x .= '</div>';
			$x .= '</div>';
			$x .= '<fieldset class="wpsd-specifications">';
				$x .= '<legend class="wpsd-fieldset-closed">';
					$x .= __( 'Specifications', 'wp-super-dealer' );
				$x .= '</legend>';
				$x .= '<div class="wpsd-fieldset-content">';
					$x .= '<div class="wpsd-type-data-buttons-wrap action-add-specification-group">';
						$x .= '<div class="wpsd-type-data-button action-add-specification-group" data-action="add-specification-group" data-type="' . $type . '">';
							$x .= __( 'Add Specification Group', 'wp-super-dealer' );
						$x .= '</div>';
						$x .= '<input type="text" title="' . __( 'Add Specification Group to ', 'wp-super-dealer' ) . esc_attr( $type ) . '" class="wpsd-input wpsd-input-add-specification-group wpsd-input-add-specification-group-' . esc_attr( $type ) . '">';
					$x .= '</div>';

					$args = array(
						'fieldname' => 'group-specs',
						'label' => __( 'Group specifications', 'wp-super-dealer' ),
						'value' => '1',
						'checked' => $group_specs,
						'type' => $type,
						'section' => 'specs',
						'action' => 'wpsd-group-specs',
					);
					$x .= wpsd_get_toggle( $args );
					$x .= '<div class="wpsd-clear wpsd-clear-specification-group-' . esc_attr( $type ) . '"></div>';
					$x .= '<div class="wpsd-spec-subgroup-section-wrap">';
						foreach( $data['specifications'] as $field=>$spec ) {
							$x .= wpsd_get_spec_group( $field, $spec, $type );
						} //= end foreach( $data['specifications'] as $field=>$options )
						$x .= '<div class="wpsd-clear"></div>';
					$x .= '</div>';
				$x .= '</div>';
			$x .= '</fieldset>';
			/* OPTIONS */
			$x .= '<fieldset class="wpsd-options">';
				$x .= '<legend class="wpsd-fieldset-closed">';
					$x .= __( 'Options', 'wp-super-dealer' );
				$x .= '</legend>';
				$x .= '<div class="wpsd-fieldset-content">';
					$x .= '<div class="wpsd-type-data-buttons-wrap action-add-option-group">';
						$x .= '<div class="wpsd-type-data-button action-add-option-group" data-action="add-option-group" data-type="' . esc_attr( $type ) . '">';
							$x .= __( 'Add Option Group', 'wp-super-dealer' );
						$x .= '</div>';
						$x .= '<input type="text" title="' . __( 'Add Option Group to ', 'wp-super-dealer' ) . esc_attr( $type ) . '" class="wpsd-input wpsd-input-add-option-group wpsd-input-add-option-group-' . esc_attr( $type ) . '">';
					$x .= '</div>';
					$x .= '<div class="wpsd-toggle-section-wrap">';
	
						$args = array(
							'fieldname' => 'group-options',
							'label' => __( 'Group options', 'wp-super-dealer' ),
							'value' => '1',
							'checked' => $group_options,
							'type' => $type,
							'section' => 'options',
							'action' => 'wpsd-group-options',
						);
						$x .= wpsd_get_toggle( $args );
						$x .= '<div class="wpsd-clear"></div>';

						$args = array(
							'fieldname' => 'group-options-list',
							'label' => __( 'Options can be entered as a list', 'wp-super-dealer' ),
							'value' => '1',
							'checked' => $group_options_list,
							'type' => $type,
							'section' => 'options',
							'action' => 'wpsd-group-options-list',
						);
						$x .= wpsd_get_toggle( $args );

						$x .= '<div class="wpsd-clear"></div>';

					$x .= '</div>';

					$x .= '<div class="wpsd-clear wpsd-clear-option-group-' . esc_attr( $type ) . '"></div>';
					$x .= '<div class="wpsd-option-groups-wrap wpsd-option-groups-wrap-' . esc_attr( $type ) . '">';
						if ( is_array( $data['options'] ) ) {
							foreach( $data['options'] as $group=>$sub_groups ) {
								$x .= wpsd_get_option_groups( $group, $sub_groups, $type );
							} //= end foreach( $data['options'] as $group=>$sub_group )
						} //= end if ( is_array( $data['options'] ) )
					$x .= '</div>';
				$x .= '</div>'; //= end .wpsd-fieldset-content
			$x .= '</fieldset>';
		$x .= '</div>';
	$x .= '</div>';
	return $x;
}

function wpsd_get_spec_group( $group, $specs, $type ) {
	$x = '';
	$group_slug = sanitize_title( $group );
	$x .= '<fieldset class="wpsd-spec-group-fieldset wpsd-spec-group-fieldset-' . esc_attr( $type ) . '-' . esc_attr( $group_slug ) . ' wpsd-spec-group-fieldset-' . esc_attr( $group_slug ) . '" data-specification-group="' . esc_attr( $group_slug ) . '" data-specification-group-value="' . esc_attr( $group ) . '">';
		$x .= '<legend>';
			$x .= __( 'Group: ', 'wp-super-dealer' ) . esc_attr( $group );
		$x .= '</legend>';
		$x .= '<div class="wpsd-type-data-buttons-wrap">';
			$x .= '<div class="wpsd-type-data-button action-remove-specification-group" data-action="remove-specification-group" data-type="' . esc_attr( $type ) . '" data-group="' . esc_attr( $group_slug ) . '">';
				$x .= __( 'Remove Group', 'wp-super-dealer' );
			$x .= '</div>';
			$x .= '<div class="wpsd-type-data-button" data-action="add-specification" data-type="' . esc_attr( $type ) . '" data-group="' . esc_attr( $group_slug ) . '">';
				$x .= __( 'Add Specification', 'wp-super-dealer' );
			$x .= '</div>';
			$x .= '<input type="text" title="' . __( 'Add Specification to ', 'wp-super-dealer' ) . ' ' . esc_attr( $type ) . ' ' . esc_attr( $group ) . '" class="wpsd-input wpsd-input-add-specification wpsd-input-add-specification-' . esc_attr( $type ) . '-' . esc_attr( $group_slug ) .'">';
		$x .= '</div>';
		$x .= '<div class="wpsd-clear wpsd-clear-specification-' . esc_attr( $type ) . '-' . esc_attr( $group_slug ) . '"></div>';
		$x .= '<div class="wpsd-spec-subgroup-items-wrap wpsd-spec-subgroup-items-wrap-' . esc_attr( $type ) . '-' . esc_attr( $group_slug ) . '">';
			if ( ! is_array( $specs ) ) {
				if ( false !== strpos( $specs, ',' ) ) {
					$specs_array_tmp = explode( ',', $specs );
					$specs_array = array();
					foreach( $specs_array_tmp as $key=>$spec ) {
						$specs_array[ $spec ] = array();
					}
				} else {
					$specs_array = array();
				}
			} else {
				$specs_array = $specs;
			}
			if ( count( $specs_array ) > 0 ) {
				foreach( $specs_array as $field=>$spec ) {
					$field = trim( $field );
					$x .= wpsd_get_spec_item( $field, $type, $group_slug, $spec );
				} //= end foreach( $options_array as $key=>$value )
			}
		$x .= '</div>';
	$x .= '</fieldset>';
	return $x;
}

function wpsd_get_spec_item( $specification, $type, $group, $field_settings = array() ) {
	$default_field_settings = array(
		'taxonomy' => false,
		'allow_taxonomy' => true,
		'show-ui' => false,
		'label' => $specification,
		'plural'=> $specification,
		'default-value' => '',
		'locked' => false,
		'restrict' => false,
		'disabled' => false,
		'hide_edit' => false,
	);

	foreach( $default_field_settings as $key=>$value ) {
		if ( ! isset( $field_settings[ $key ] ) ) {
			$field_settings[ $key ] = filter_var( $value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE );
		}
		if ( empty( $field_settings[ $key ] ) ) {
			$field_settings[ $key ] = filter_var( '', FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE );
		}
		if ( '' === $field_settings[ $key ] ) {
			$field_settings[ $key ] = filter_var( false, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE );
		}
	}

	$x = '';
	$specification_slug = sanitize_title( $specification );
	$class = '';
	$locked = filter_var( $field_settings['locked'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE );
	if ( isset( $field_settings['locked'] ) && $locked ) {
		$class = ' wpsd-spec-locked';
	}

	if ( ! isset( $field_settings['disabled'] ) ) {
		$field_settings['disabled'] = false;
	}
	
	if ( ! isset( $field_settings['hide_edit'] ) ) {
		$field_settings['hide_edit'] = false;
	}
	
	if ( true === $field_settings['disabled'] || 'true' === $field_settings['disabled'] ) {
		$disabled_class = ' wpsd-field-disabled';
	} else {
		$disabled_class = '';
	}
	
	$x .= '<div class="wpsd-type-specification-field wpsd-type-specification-' . esc_attr( $type ) . ' wpsd-type-specification-' . esc_attr( $type . '-' . $group . '-' . $specification_slug . $class . $disabled_class ) .'" data-specification="' . esc_attr( $specification_slug ) . '" data-value="' . esc_attr( $specification ) . '">';
		$x .= '<div class="wpsd-type-specification-field-label">';
			if ( defined( 'WPSD_SHOW_TRUE_NAME' ) ) {
				$x .= $specification;
			} else {
				$x .= ( isset( $field_settings['label'] ) && ! empty( $field_settings['label'] ) ? $field_settings['label'] : $specification );
			}
			$x .= '<div class="wpsd-remove-button" data-action="remove-spec-item" data-type="' . esc_attr( $type ) . '" data-group="' . esc_attr( $group ) . '" data-specification="' . esc_attr( $specification_slug ) . '">&#215;</div>';
		$x .= '</div>';

		$x .= '<div class="wpsd-type-specification-field-values wpsd-type-specification-field-values-' . esc_attr( $group . '-' . $specification_slug ) . '">';
			$x .= '<div class="wpsd-type-specification-field-values-label">';
				$x .= __( 'Options', 'wp-super-dealer' );
			$x .= '</div>';
			$x .= '<div>';
				$x .= '(' . esc_html( $specification ) . ')';
			$x .= '</div>';
			$disabled = '';
			$show_class = '';
			$show_ui_class = '';
			$show_plural = '';
			if ( false === $field_settings['taxonomy'] ) {
				$disabled = ' disabled="disabled"';
			}
			if ( true === $field_settings['locked'] ) {
				$show_class = ' wpsd-inactive';
				$show_ui_class = ' wpsd-inactive';
			}
			$data_string = ' data-type="' . esc_attr( $type ) . '" data-group="' . esc_attr( $group ) . '" data-specification="' . esc_attr( $specification_slug ) . '"';
			if ( 'global' === $type ) {
				$x .= '<div class="wpsd-specification-disabled-wrap"' . $data_string . '>';
					$args = array(
						'fieldname' => $specification_slug,
						'label' => __( 'Disabled', 'wp-super-dealer' ),
						'value' => '1',
						'checked' => $field_settings['disabled'],
						'type' => 'global',
						'section' => 'specs',
						'action' => 'wpsd-disable-global-spec',
						'class' => 'wpsd-specification-disabled',
						'slug' => $type . '-' . $group . '-' . $specification_slug,
						'element_class' => ' wpsd-specification-field-option wpsd-specification-field-option-disabled',
						'tip' => __( '', 'wp-super-dealer' ),
						'hide_edit' => $field_settings['hide_edit'],
					);
					$x .= wpsd_get_toggle( $args );
				$x .= '</div>';
			}

			if ( isset( $field_settings['allow_taxonomy'] ) ) {
				$allow_taxonomy = filter_var( $field_settings['allow_taxonomy'], FILTER_VALIDATE_BOOLEAN );
				if ( true === $allow_taxonomy ) {
					$x .= '<div class="wpsd-specification-field-option-wrap' . esc_attr( $show_class ) . '">';
						$x .= '<h4>';
							$x .= '<input type="checkbox"' . $data_string . ' name="spec-field[' . esc_attr( $type ) . '][' . esc_attr( $group ) . '][' . esc_attr( $specification_slug ) . '][taxonomy]" class="wpsd-specification-field-option wpsd-specification-field-option-taxonomy" value="1"' . ( true === $field_settings['taxonomy'] ? ' checked' : '' ) . ' />';
							$x .= __( 'Register as a taxonomy?', 'wp-super-dealer' );
						$x .= '</h4>';
					$x .= '</div>';
					
					if ( 'true' === $field_settings['restrict'] 
						|| true === $field_settings['restrict'] ) 
					{
						$field_settings['restrict'] = true;
					} else {
						$field_settings['restrict'] = false;
					}
					$hide_edit = false;
					if ( 'true' === $field_settings['hide_edit'] 
						|| true === $field_settings['hide_edit'] ) 
					{
						$field_settings['hide_edit'] = true;
						$hide_edit = true;
					}
					$x .= '<div class="wpsd-specification-field-option-wrap wpsd-specification-restrict wpsd-specification-restrict-' . esc_attr( $type . '-' . $group . '-' . $specification_slug . $show_plural ) . '">';
						$args = array(
							'fieldname' => 'spec-field[' . $type . '][' . $group . '][' . $specification_slug . '][restrict]',
							'label' => __( 'Restrict Options', 'wp-super-dealer' ),
							'value' => '1',
							'checked' => $field_settings['restrict'],
							'type' => 'global',
							'section' => 'specs',
							'action' => 'wpsd-restrict-spec-options-restrict',
							'class' => 'wpsd-restrict-spec-options-restrict-wrap',
							'slug' => $type . '-' . $group . '-' . $specification_slug,
							'element_class' => ' wpsd-specification-field-option wpsd-specification-field-option-restrict',
							'tip' => __( '', 'wp-super-dealer' ),
							'hide_edit' => $hide_edit,
						);
						$x .= wpsd_get_toggle( $args );
					$x .= '</div>';

					if ( 'true' === $field_settings['show-ui'] || true === $field_settings['show-ui'] ) {
						$field_settings['show-ui'] = true;
						$show_plural = ' show';
					} else {
						$field_settings['show-ui'] = false;
					}
					$x .= '<div class="wpsd-specification-field-option-wrap' . $show_ui_class . '">';
						$args = array(
							'fieldname' => 'spec-field[' . $type . '][' . $group . '][' . $specification_slug . '][show-ui]',
							'label' => __( 'Show Taxonomy UI', 'wp-super-dealer' ),
							'value' => '1',
							'checked' => $field_settings['show-ui'],
							'type' => 'global',
							'section' => 'specs',
							'action' => 'wpsd-specification-field-option-show-ui',
							'class' => 'wpsd-specification-field-option-show-ui-wrap',
							'slug' => $type . '-' . $group . '-' . $specification_slug,
							'element_class' => ' wpsd-specification-field-option wpsd-specification-field-option-show-ui',
							'tip' => __( '', 'wp-super-dealer' ),
						);
						$x .= wpsd_get_toggle( $args );

						$x .= '<div class="wpsd-specification-plural wpsd-specification-plural-' . esc_attr( $type . '-' . $group . '-' . $specification_slug . $show_plural ) . '">';
							$x .= '<h4>';
								$x .= __( 'Plural Label', 'wp-super-dealer' );
							$x .= '</h4>';
							if ( empty( $field_settings['plural'] ) ) {
								$field_settings['plural'] = $field_settings['label'];
								if ( empty( $field_settings['plural'] ) ) {
									$field_settings['plural'] = $specification;
								}
							}
							$x .= '<input type="text" name="spec-field[' . esc_attr( $type ) . '][' . esc_attr( $group ) . '][' . esc_attr( $specification_slug ) . '][plural]" class="wpsd-specification-field-option wpsd-specification-field-option-plural" value="' . esc_attr( $field_settings['plural'] ) . '" />';
						$x .= '</div>';
					$x .= '</div>';
				}
			}

			$x .= '<div class="wpsd-specification-field-option-wrap">';
				$x .= '<h4>';
					$x .= __( 'Label', 'wp-super-dealer' );
				$x .= '</h4>';
				$x .= '<input type="text" name="spec-field[' . esc_attr( $type ) . '][' . esc_attr( $group ) . '][' . esc_attr( $specification_slug ) . '][label]" class="wpsd-specification-field-option wpsd-specification-field-option-label" value="' . ( isset( $field_settings['label'] ) ? esc_attr( $field_settings['label'] ) : '' ) . '" />';
			$x .= '</div>';
			$x .= '<div class="wpsd-specification-field-option-wrap">';
				$x .= '<h4>';
					$x .= __( 'Default Value', 'wp-super-dealer' );
				$x .= '</h4>';
				$default_value = ( isset( $field_settings['default-value'] ) ? $field_settings['default-value'] : '' );
				if ( 0 === $default_value ) {
					$default_value = '';
				}
				$x .= '<input type="text" name="spec-field[' . esc_attr( $type ) . '][' . esc_attr( $group ) . '][' . esc_attr( $specification_slug ) . '][default-value]" class="wpsd-specification-field-option wpsd-specification-field-option-default-value" value="' . esc_attr( $default_value ) . '" />';
			$x .= '</div>';
			$x .= '<div class="wpsd-type-data-buttons-wrap">';
				$x .= '<div class="wpsd-type-data-button" data-action="close-spec-options" data-type="' . esc_attr( $type ) . '" data-group="' . esc_attr( $group ) . '" data-spec="' . esc_attr( $specification_slug ) . '">';
					$x .= __( 'Close', 'wp-super-dealer' );
				$x .= '</div>';
			$x .= '</div>';
			
		$x .= '</div>';
	$x .= '</div>';
	return $x;
}

function wpsd_get_option_groups( $group, $sub_groups, $type ) {
	if ( 0 === $group ) {
		$group = __( 'Default', 'wp-super-dealer' );
		$options = $sub_groups;
		$sub_groups = array();
		$sub_groups[ $group ] = $options;
	}
	$x = '';
	$type_slug = sanitize_title( $type );
	$group_slug = sanitize_title( $group );
	$x .= '<div class="wpsd-option-group wpsd-option-group-open wpsd-option-group-' . esc_attr( $type_slug . '-' . $group_slug ) . ' wpsd-option-group-' . esc_attr( $group_slug ) . '" data-option-group="' . esc_attr( $group_slug ) . '" data-option-group-value="' . esc_attr( $group ) . '">';
		$x .= '<label class="wpsd-option-group-label">';
			$x .= __( 'Group: ', 'wp-super-dealer' ) . esc_html( $group );
		$x .= '</label>';
		$x .= '<div class="clear"></div>';
		$x .= '<div class="wpsd-option-group-wrap wpsd-option-group-wrap-' . esc_attr( $type ) . '">';
			$x .= '<div class="wpsd-type-data-buttons-wrap">';
				$x .= '<div class="wpsd-type-data-button" data-action="remove-option-group" data-type="' . esc_attr( $type_slug ) . '" data-group="' . esc_attr( $group_slug ) . '">';
					$x .= __( 'Remove Group: ', 'wp-super-dealer' ) . esc_html( $group );
				$x .= '</div>';
				$x .= '<div class="wpsd-type-data-button action-add-option-sub-group" data-action="add-option-sub-group" data-type="' . esc_attr( $type_slug ) . '" data-group="' . esc_attr( $group_slug ) . '">';
					$x .= __( 'Add Option Sub-Group', 'wp-super-dealer' );
				$x .= '</div>';
				$x .= '<input type="text" title="' . __( 'Add Option Group to ', 'wp-super-dealer' ) . esc_attr( $type ) . ' ' . esc_attr( $group ) . '" data-type="' . esc_attr( $type ) . '" data-group="' . esc_attr( $group_slug ) . '" class="wpsd-input wpsd-input-add-option-sub-group wpsd-input-add-option-sub-group-' . esc_attr( $type_slug ) . '-' . esc_attr( $group_slug ) . '">';
			$x .= '</div>';
			$x .= '<div class="wpsd-clear"></div>';
			$x .= '<div class="wpsd-option-sub-groups wpsd-option-sub-groups-' . esc_attr( $group_slug ) . '">';
				if ( is_array( $sub_groups ) ) {
					foreach( $sub_groups as $sub_group=>$options ) {
						$x .= wpsd_get_option_sub_group( $options, $type, $group, $sub_group );
						$x .= '<div class="wpsd-clear"></div>';
					} //= end foreach( $sub_group as $sub_group_title=>$options )
				} //= end if ( is_array( $sub_group ) )
			$x .= '</div>'; //= end .wpsd-option-sub-groups
		$x .= '</div>'; //= end .wpsd-option-group-wrap
	$x .= '</div>'; //= end wpsd-option-group
	return $x;
}

function wpsd_get_option_sub_group( $options, $type, $group, $sub_group ) {
	$x = '';
	$type_slug = sanitize_title( $type );
	$group_slug = sanitize_title( $group );
	$sub_group_slug = sanitize_title( $sub_group );
	$x .= '<div class="wpsd-option-sub-group wpsd-option-sub-group-' . esc_attr( $type_slug ) . '-' . esc_attr( $group_slug ) . '-' . esc_attr( $sub_group_slug ) . '" data-option-sub-group="' . esc_attr( $sub_group ) . '">';
		$x .= '<div class="wpsd-option-sub-group-title">';
			$x .= __( 'Sub-group: ', 'wp-super-dealer' ) . esc_html( $sub_group );
			$x .= '<div class="wpsd-remove-button"  data-action="remove-option-sub-group" data-type="' . esc_attr( $type_slug ) . '" data-group="' . esc_attr( $group_slug ) . '" data-sub-group="' . esc_attr( $sub_group_slug ) . '">&#215;</div>';
		$x .= '</div>';
		$x .= '<div class="wpsd-type-data-buttons-wrap action-add-option">';
			$x .= '<div class="wpsd-type-data-button" data-action="add-option" data-type="' . esc_attr( $type_slug ) . '" data-group="' . esc_attr( $group_slug ) . '" data-sub-group="' . esc_attr( $sub_group_slug ) . '">';
				$x .= __( 'Add Option', 'wp-super-dealer' );
			$x .= '</div>';
			$x .= '<input type="text" title="' . __( 'Add Option to ', 'wp-super-dealer' ) . ' ' . esc_html( $type ) . ' ' . esc_html( $sub_group ) . '" class="wpsd-input wpsd-input-add-option wpsd-input-add-option-' . esc_attr( $type_slug . '-' . $group_slug . '-' . $sub_group_slug ) . '">';
		$x .= '</div>';
		$x .= '<div class="wpsd-clear wpsd-clear-option-' . esc_attr( $type_slug . '-' . $group_slug . '-' . $sub_group_slug ) . '"></div>';
		$x .= '<div class="wpsd-option-subgroup-items-wrap wpsd-option-subgroup-items-wrap-' . esc_attr( $type_slug . '-' . $group_slug . '-' . $sub_group_slug ) . '">';
			if ( false !== strpos( $options, ',' ) ) {
				$options_array = explode( ',', $options );
				if ( count( $options_array ) > 0 ) {
					foreach( $options_array as $key=>$option ) {
						$x .= wpsd_get_option( $option, $type, $group, $sub_group );
					} //= end foreach( $options_array as $key=>$option )
					$x .= '<div class="wpsd-clear"></div>';
				}
			} //= end if ( false !== strpos( $options, ',' ) )
		$x .= '</div>';
		$x .= '<div class="wpsd-clear"></div>';
	$x .= '</div>';
	return $x;
}

function wpsd_get_option( $option, $type, $group, $sub_group ) {
	$x = '';
	$option_slug = sanitize_title( $option );
	$type_slug = sanitize_title( $type );
	$group_slug = sanitize_title( $group );
	$sub_group_slug = sanitize_title( $sub_group );
	$x .= '<div class="wpsd-option-tag wpsd-option-tag-' . esc_attr( $type_slug . '-' . $group_slug . '-' . $sub_group_slug . '-' . $option_slug ) . '" data-type="' . esc_attr( $type_slug ) . '" data-group="' . esc_attr( $group_slug ) . '" data-sub-group="' . esc_attr( $sub_group_slug ) . '" data-option="' . esc_attr( $option_slug ) . '" data-value="' . esc_attr( $option ) . '">';
		$x .= $option;
		$x .= '<div class="wpsd-remove-button" data-action="remove-option-item" data-type="' . $type_slug . '" data-group="' . $group_slug . '" data-sub-group="' . $sub_group_slug . '" data-option="' . $option_slug . '">&#215;</div>';
	$x .= '</div>';
	return $x;
}

function wpsd_vehicle_options_settings_save() {
	$msg = __( 'Vehicle options failed to update.', 'wp-super-dealer' );
	if ( isset( $_POST['fieldData'] ) ) {
		$base64 = sanitize_text_field( $_POST['fieldData'] );

        //= Create a new array with sanitize values
		$vehicle_types = wpsd_build_types_array( $base64 );

        //= Run our new array through one last sanitize function then update
        $vehicle_types = wpsd_sanitize_array( $vehicle_types );
		update_option( 'wpsd-vehicle-types', $vehicle_types );

		$default_vehicle_options = wpsd_get_vehicle_options();
		$default_global_specs = $default_vehicle_options['global_specs'];

		$vehicle_options = array();

        $vehicle_option_fields = array(
            'global_specs',
            'no_group_specs',
            'no_group_options',
            'group_options_list',
        );

        foreach( $vehicle_option_fields as $vehicle_option_field ) {
            if ( isset( $_POST[ $vehicle_option_field ] ) ) {
                if ( 'global_specs' === $vehicle_option_field ) {
                    $vehicle_options[ $vehicle_option_field ] = map_deep( wp_unslash( $_POST[ $vehicle_option_field ] ), 'wpsd_sanitize_array' );
                } else {
                    $vehicle_options[ $vehicle_option_field ] = (array) map_deep( wp_unslash( $_POST[ $vehicle_option_field ] ), 'sanitize_text_field' );
                }
            } else {
                $vehicle_options[ $vehicle_option_field ] = array();
            }
        }

		update_option( 'wpsd-vehicle-options', $vehicle_options, false );
		$msg = __( 'Vehicle Options Saved', 'wp-super-dealer' );
	}

	return $msg;
}

function wpsd_sanitize_array( $dirty_array ) {
    if ( is_array( $dirty_array ) ) {
        //= we were sent an array - create new variable to build a clean array
        $array = array();
    } else {
        //= this is not an array - sanitize as text
        return sanitize_text_field( $dirty_array );
    }
    //= loop the dirty array and build the clean one
	foreach( $dirty_array as $key=>$value ) {
        //= there's no value for this key - make it false to keep consistent
		if ( empty( $value ) || '' === $value ) {
			$value = false;
		}
        if ( is_array( $value ) ) {
            //= the value is an array so we run it back through this function to clean it
            //= we also sanitize the key to make sure all aspects of the new array are clean
            $array[ sanitize_text_field( $key ) ] = wpsd_sanitize_array( $value );
        } else {
            //= if we have an actual true as a value then return true (sanitize_text_field changes true to 1)
            if ( true === $value ) {
                $array[ sanitize_text_field( $key ) ] = true;
            } else {
                //= the value is not an array - assume text and sanitize it
                //= we also sanitize the key to make sure all aspects of the new array are clean
                $array[ sanitize_text_field( $key ) ] = sanitize_text_field( $value );
            }
        }
	}
    //= return our clean array
	return $array;
}

function wpsd_get_toggle( $args ) {
	$default_args = array(
		'fieldname' => '',
		'label' => '',
		'value' => '',
		'checked' => false,
		'base64' => base64_encode( json_encode( array() ) ),
		'type' => 'default',
		'section' => 'specs',
		'action' => '',
		'class' => '',
		'element_class' => '',
		'tip' => '',
		'slug' => '',
	);
	$args = wp_parse_args( $args, $default_args );
	if ( true === $args['checked'] || 'true' === $args['checked'] ) {
		$checked = ' checked';
	} else {
		$checked = '';
	}
	$x = '';
	$x .= '<div data-checked="' . esc_attr( $args['checked'] ) . '" class="wpsd-toggle-button-wrap wpsd-toggle-button-' . esc_attr( $args['fieldname'] . ' ' . $args['class'] ) . '" data-type="' . esc_attr( $args['type']  ). '" data-section="' . esc_attr( $args['section'] ) . '" data-action="' . esc_attr( $args['action'] ) . '" data-slug="' . esc_attr( $args['slug'] ) . '">';
		$x .= '<div class="wpsd-toggle-wrap">';
			$x .= '<label class="wpsd-toggle">';
				$x .= '<input type="checkbox" name="' . esc_attr( $args['fieldname'] ) . '" class="wpsd-toggle-button' . esc_attr( $args['element_class'] ) . '" data-type="' . esc_attr( $args['type'] ) . '" data-section="' . esc_attr( $args['section'] ) . '" data-base64="' . esc_attr( $args['base64'] ) . '" data-slug="' . esc_attr( $args['slug'] ) . '" data-action="' . esc_attr( $args['action'] ) . '"' . esc_attr( $checked ) . ' value="1">';
				$x .= '<span class="slider round"></span>';
			$x .= '</label>';
			$x .= '<div class="wpsd-toggle-title" data-action="group-' . esc_attr( $args['section'] ) . '" data-type="' . esc_attr( $args['type'] ) . '">';
				$x .= esc_html( $args['label'] );
			$x .= '</div>';
		$x .= '</div>';
	$x .= '</div>';
	return $x;
}

function wpsd_vehicle_options_instructions() {
    $html = '';
	$html .= '<div class="wpsd-instructions-wrap wpsd-instructions-wrap-vehicle-options">';
		$html .= '<div class="wpsd-instructions-title" data-tab="vehicle-options">';
			$html .= '<span class="dashicons dashicons-welcome-learn-more"></span>';
			$html .=  __( 'Tips & Tricks - Vehicle Options', 'wp-super-dealer' );
		$html .= '</div>';

		$html .=  '<ul class="wpsd-instructions">';
            $html .= '<li>';
                $html .= '<div class="wpsd-tnt-wrap">';
                    $html .= '<div class="wpsd-tnt-col">';
                        $html .= '<div class="wpsd-video-wrap">';
                            $html .= '<span class="wpsd-youtube" data-video-url="https://www.youtube.com/embed/pwg1vGMoDbs"></span>';
                        $html .= '</div>';
                    $html .= '</div>';
                    $html .= '<div class="wpsd-tnt-col">';
                        $html .= '<h2>';
                            $html .=  __( 'Tips & Tricks for Vehicle Options', 'wp-super-dealer' );
                        $html .= '</h2>';
                        $html .= '<ul>';
                            $html .= '<li>';
                                $html .=  __( '1. WPSuperDealer supports multiple vehicle types.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .=  __( 'Each vehicle type has its own specifications and options.', 'wp-super-dealer' );
                            $html .= '</li>';

                            $html .= '<li><br /></li>';

                            $html .= '<li>';
                                $html .=  __( '2. Specification fields can be used for searching and sorting.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .=  __( 'If you\'ll be using multiple vehicle types then we suggest using as many similar specification names between vehicle types to simplify searching and sorting vehicles.', 'wp-super-dealer' );
                            $html .= '</li>';

                            $html .= '<li><br /></li>';

                            $html .= '<li>';
                                $html .=  __( '3. Specifications and options can placed into groups or you can choose to turn off grouping.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .=  __( 'It has been our experience that new vehicles benefit from having the grouping tured on, but only if all of the options are filled out or decoded.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .=  __( 'With pre-owned vehicles it may simplify things to leave the specifications and options ungrouped.', 'wp-super-dealer' );
                            $html .= '</li>';

                            $html .= '<li><br /></li>';

                            $html .= '<li>';
                                $html .=  __( '4. To further simplify things you also have the option to manually type in your options on the vehicle edit page, rather than constrain yourself to a fixed set of options.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .=  __( 'Specifications can also be turned into "taxonomies", that\'s a fancy way of saying they can behave like a category.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .=  __( 'This means you can create a set list of values for a specification and only allow those to be added to a vehicle.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .=  __( 'You can also use this option to create an autofill on the vehicle edit page that will show you options already used before that you can select from or just add a new one on the fly.', 'wp-super-dealer' );
                            $html .= '</li>';

                            $html .= '<li><br /></li>';

                            $html .= '<li>';
                                $html .=  __( '5. Global Specifications are part of every vehicle type and can be disabled and hidden if they\'re not needed.', 'wp-super-dealer' );
                            $html .= '</li>';

                        $html .= '</ul>';
                    $html .= '</div>';
                $html .= '</div>'; //= end $html .= '<div class="wpsd-tnt-wrap">';
                $html .= wpsd_instructions_footer();
            $html .= '</li>';

		$html .= '</ul>';
		$html .= '<div class="wpsd-instructions-hide" data-tab="vehicle-options">';
			$html .=  __( 'show', 'wp-super-dealer' );
		$html .= '</div>';
	$html .= '</div>';
    return $html;
}
?>