<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_locations_tab() {
    $html = '';

	$args = array(
		'style'              => 'none',
		'show_count'         => 0,
		'use_desc_for_title' => 0,
		'hierarchical'       => true,
		'echo'               => 0,
		'hide_empty'		 => 0,
		'taxonomy'           => 'location'
		);
	$locations = get_categories( $args );

    $time = time();
    $form_fields = new WP_SUPER_DEALER\WPSD_FIELDS;
    $table = array();

    $default_location = array(
            get_bloginfo( 'name' ),
            __( 'Address', 'wp-super-dealer' ),
            __( 'City', 'wp-super-dealer' ),
            __( 'State', 'wp-super-dealer' ),
            __( 'Zip', 'wp-super-dealer' ),
			__( 'Edit or remove', 'wp-super-dealer' ),
        );

    $table[ $time + rand( 10, 10000 ) ] = array(
            __( 'Location Name', 'wp-super-dealer' ),
            __( 'Address', 'wp-super-dealer' ),
            __( 'City', 'wp-super-dealer' ),
            __( 'State', 'wp-super-dealer' ),
            __( 'Zip', 'wp-super-dealer' ),
			__( 'Edit or remove', 'wp-super-dealer' ),
        );
    $table = apply_filters( 'wpsd_location_table_header_filter', $table );

	if ( 1 === count( $locations ) ) {
		$table_css = ' in-active';
		$location_css = ' active';
	} else {
		$table_css = '';
		$location_css = '';
	}
    $locations_html = '';
    foreach( $locations as $key=>$location ) {
        $data = array();
        //= Add to the array for our table
        $data['name'] = $location->name;
        $data['slug'] = $location->slug;

		$location_fields = wpsd_location_fields();

        foreach( $location_fields as $field=>$value ) {
            if ( 'name' === $field ) {
                //= Don't update the name with term data, wer want the tx term name
                continue;
            }
			$data[ $field ] = get_term_meta( $location->term_id, $field, true );
        }

		$data['edit'] = '<div class="wpsd-edit-location-button" data-slug="' . esc_attr( $location->slug ) . '"><span class="dashicons dashicons-edit"></span>' . __( 'edit', 'wp-super-dealer' ) . '</div>';
		$data['edit'] .= '<div class="wpsd-remove-location-button" data-slug="' . esc_attr( $location->slug ) . '"><span class="dashicons dashicons-trash"></span>' . __( 'remove', 'wp-super-dealer' ) . '</div>';
		$data = apply_filters( 'wpsd_location_table_data_filter', $data );
        $table[ $time + rand( 10, 10000 ) ] = array(
			'<span class="dashicons dashicons-store"></span>'. esc_attr( $data['name'] ),
			esc_attr( $data['address'] ),
			esc_attr( $data['city'] ),
			esc_attr( $data['state'] ),
			esc_attr( $data['zip'] ),
			$data['edit'],
		);

		//= Add to the HTML with all the location details
        $locations_html .= '<div class="wpsd-edit-location-wrap wpsd-edit-location-wrap-' . esc_attr( $location->slug . $location_css ) . '" data-slug="' . esc_attr( $location->slug ) . '">';
			$locations_html .= '<h3>';
				$locations_html .= esc_html( $location->name );
			$locations_html .= '</h3>';
			$locations_html .= wpsd_get_location_fields_html( $data );
        $locations_html .= '</div>';
    }
    $html .= '<h3>';
        $html .= __( 'Locations', 'wp-super-dealer' );
    $html .= '</h3>';

    $html .= wpsd_locations_instructions();
    
	$html .= '<div class="wpsd-admin-location-add-button">';
		$html .= __( 'Add Location', 'wp-super-dealer' );
	$html .= '</div>';
	
	//= Output the table with location data
    $html .= $form_fields->get_table( $table, 'wpsd-admin-location-table' . $table_css );
	$html .= '<div class="wpsd-edit-view-locations">';
		$html .= __( 'View location list', 'wp-super-dealer' );
	$html .= '</div>';
    //= Output the HTML for each individual location and all their details
    //= Note, this is hidden by default and displayed by user action with the table
    $html .= $locations_html;

    return $html;
}

function wpsd_get_location_fields_html( $location ) {
	$settings = array();
	$form_fields = new WP_SUPER_DEALER\WPSD_FIELDS;
	$time = time();
    $html = '';
    if ( ! isset( $location['slug'] ) ) {
        $location_slug = sanitize_title( $location['name'] );
        $location['slug'] = $location_slug;
    } else {
        $location_slug = $location['slug'];
    }

	//= TO DO future: move arrays into their own function
	//= also reference wpsd_location_fields
	//=======================================================
    //= Start address section
	$fields = wpsd_get_location_fields_address( $time, $location_slug, $location );
	$label = __( 'Location Address', 'wp-super-dealer' );
	$content = $form_fields->get_form_fields( $fields );
	$html .= wpsd_admin_group_wrap( $label, $content, $fields );

    //=======================================================
    //= Start vehicle information section
    //=======================================================
    $fields = wpsd_get_location_fields_vehicle( $time, $location_slug, $location );
	$label = __( 'Vehicle Settings', 'wp-super-dealer' );
	$content = $form_fields->get_form_fields( $fields );
	$html .= wpsd_admin_group_wrap( $label, $content, $fields );
    
    //=======================================================
    //= Start contact section
    //=======================================================
    $fields = wpsd_get_location_fields_contact( $time, $location_slug, $location );
	$label = __( 'Contact Settings', 'wp-super-dealer' );
	$content = $form_fields->get_form_fields( $fields );
	$html .= wpsd_admin_group_wrap( $label, $content, $fields );

    //=======================================================
    //= Start link section
    //=======================================================
    $fields = wpsd_get_location_fields_links( $time, $location_slug, $location );
	$label = __( 'Location URLs', 'wp-super-dealer' );
	$content = $form_fields->get_form_fields( $fields );

	$html .= wpsd_admin_group_wrap( $label, $content, $fields );
	
    return $html;
}

function wpsd_get_location_fields_address( $time, $location_slug, $location ) {
	$fields = array(
		$time + rand( 10,1000 ) => array(
			'id_name' => 'location-' . $location_slug . '-location_name',
			'key' => $location_slug,
			'label' => __( 'Location Name', 'wp-super-dealer' ),
			'slug' => 'location_name',
			'type' => 'text',
			'value' => $location['name'],
			'class' => '',
			'tip' => array(
				'title' => __( 'Tip', 'wp-super-dealer' ),
				'content' => __( 'Let everyone know who is listing the vehicles.', 'wp-super-dealer' ),
			),
			'placeholder' => '',
		),
		$time + rand( 10,1000 ) => array(
			'id_name' => 'location-' . $location_slug . '-phone_main',
			'key' => $location_slug,
			'label' => __( 'Main Phone', 'wp-super-dealer' ),
			'slug' => 'phone_main',
			'type' => 'text',
			'value' => ( isset( $location['phone_main'] ) ? $location['phone_main'] : '' ),
			'class' => '',
			'tip' => array(),
			'placeholder' => __( 'The main phone for this location.', 'wp-super-dealer' ),
		),
        $time + rand( 10,1000 ) => array(
			'label' => __( 'Location Address', 'wp-super-dealer' ),
			'slug' => 'comment',
			'type' => 'comment',
			'value' => __( 'Enter the address for the physical location of your vehicles.', 'wp-super-dealer' ),
			'class' => '',
			'placeholder' => '',
			'tip' => array(
				'title' => __( 'Tip', 'wp-super-dealer' ),
				'content' => __( 'Let everyone know where the vehicles are located.', 'wp-super-dealer' ),
			),
        ),
		$time + rand( 10,1000 ) => array(
			'id_name' => 'location-' . $location_slug . '-address',
			'key' => $location_slug,
			'label' => __( 'Address', 'wp-super-dealer' ),
			'slug' => 'address',
			'type' => 'text',
			'value' => ( isset( $location['address'] ) ? $location['address'] : '' ),
			'class' => '',
			'tip' => array(),
			'placeholder' => __( 'Location street address', 'wp-super-dealer' ),
		),
		$time + rand( 10,1000 ) => array(
			'id_name' => 'location-' . $location_slug . '-city',
			'key' => $location_slug,
			'label' => __( 'City', 'wp-super-dealer' ),
			'slug' => 'city',
			'type' => 'text',
			'value' => ( isset( $location['city'] ) ? $location['city'] : '' ),
			'class' => '',
			'tip' => array(),
			'placeholder' => __( 'Location city', 'wp-super-dealer' ),
		),
		$time + rand( 10,1000 ) => array(
			'id_name' => 'location-' . $location_slug . '-state',
			'key' => $location_slug,
			'label' => __( 'State', 'wp-super-dealer' ),
			'slug' => 'state',
			'type' => 'text',
			'value' => ( isset( $location['state'] ) ? $location['state'] : '' ),
			'class' => '',
			'tip' => array(),
			'placeholder' => __( 'Location state', 'wp-super-dealer' ),
		),
		$time + rand( 10,1000 ) => array(
			'id_name' => 'location-' . $location_slug . '-zip',
			'key' => $location_slug,
			'label' => __( 'Zip Code', 'wp-super-dealer' ),
			'slug' => 'zip',
			'type' => 'text',
			'value' => ( isset( $location['zip'] ) ? $location['zip'] : '' ),
			'class' => '',
			'tip' => array(),
			'placeholder' => __( 'Location zip code', 'wp-super-dealer' ),
		),
    );
    $fields = apply_filters( 'wpsd_location_address_fields_filter', $fields, 'address', $location );
    return $fields;
}

function wpsd_get_location_fields_vehicle( $time, $location_slug, $location ) {
	$fields = array(
		$time + rand( 10,1000 ) => array(
			'label' => __( 'Vehicle Information', 'wp-super-dealer' ),
			'key' => $location_slug,
			'slug' => 'comment_2',
			'type' => 'comment',
			'value' => __( 'This section gives you a little extra control over the vehicles from this location.', 'wp-super-dealer' ),
			'class' => '',
			'tip' => array(),
			'placeholder' => '',
		),
		$time + rand( 10,1000 ) => array(
			'id_name' => 'location-' . $location_slug . '-hide_prices',
			'key' => $location_slug,
			'label' => __( 'Hide Prices', 'wp-super-dealer' ),
			'slug' => 'hide_prices',
			'type' => 'select',
			'value' => ( isset( $location['hide_prices'] ) ? $location['hide_prices'] : '' ),
            'options' => array(
                array(
                    'label' => __( 'No', 'wp-super-dealer' ),
                    'value' => __( 'no', 'wp-super-dealer' ),
                ),
                array(
                    'label' => __( 'Yes', 'wp-super-dealer' ),
                    'value' => __( 'yes', 'wp-super-dealer' ),
                ),
            ),
			'class' => '',
			'tip' => array(
				'title' => __( 'Tip', 'wp-super-dealer' ),
				'content' => __( 'Set to "Yes" and the prices for all vehicles from this location will be replaced with the "no price" text from the "Price Options" tab.', 'wp-super-dealer' ),
			),
			'placeholder' => '',
		),
		$time + rand( 10,1000 ) => array(
			'id_name' => 'location-' . $location_slug . '-add_to_price',
			'key' => $location_slug,
			'label' => __( 'Add to vehicle price', 'wp-super-dealer' ),
			'slug' => 'add_to_price',
			'type' => 'number',
			'value' => ( isset( $location['add_to_price'] ) ? $location['add_to_price'] : '' ),
			'class' => '',
			'tip' => array(
				'title' => __( 'Tip', 'wp-super-dealer' ),
				'content' => __( 'Enter an amount and it will be added (or subtracted if negative) from the price of all vehicles from this location.', 'wp-super-dealer' ),
			),
			'placeholder' => '$$$',
		),
    );

	$fields = apply_filters( 'wpsd_location_vehicle_fields_filter', $fields, WPSD_POST_TYPE, $location );
    return $fields;
}

function wpsd_get_location_fields_contact( $time, $location_slug, $location ) {
	$fields = array(
		$time + rand( 10,1000 ) => array(
			'label' => __( 'Contact Information', 'wp-super-dealer' ),
			'slug' => 'comment_1',
			'type' => 'comment',
			'value' => __( 'This is the contact information for vehicles from this location.', 'wp-super-dealer' ),
			'class' => '',
			'tip' => array(),
			'placeholder' => '',
		),
		$time + rand( 10,1000 ) => array(
			'id_name' => 'location-' . $location_slug . '-contact_name',
			'key' => $location_slug,
			'label' => __( 'Contact Name', 'wp-super-dealer' ),
			'slug' => 'contact_name',
			'type' => 'text',
			'value' => ( isset( $location['contact_name'] ) ? $location['contact_name'] : '' ),
			'class' => '',
			'tip' => array(
				'title' => __( 'Tip', 'wp-super-dealer' ),
				'content' => __( 'This is the name or department that should be contacted for information on the vehicles from this location.', 'wp-super-dealer' ),
			),
			'placeholder' => __( 'Department or salesperson', 'wp-super-dealer' ),
		),
		$time + rand( 10,1000 ) => array(
			'id_name' => 'location-' . $location_slug . '-phone_contact',
			'key' => $location_slug,
			'label' => __( 'Contact Phone', 'wp-super-dealer' ),
			'slug' => 'phone_contact',
			'type' => 'text',
			'value' => ( isset( $location['phone_contact'] ) ? $location['phone_contact'] : '' ),
			'class' => '',
			'tip' => array(
				'title' => __( 'Tip', 'wp-super-dealer' ),
				'content' => __( 'This is the phone number someone should call for more information on vehicles from this location.', 'wp-super-dealer' ),
			),
			'placeholder' => __( '1-555-555-1212', 'wp-super-dealer' ),
		),
		$time + rand( 10,1000 ) => array(
			'id_name' => 'location-' . $location_slug . '-email',
			'key' => $location_slug,
			'label' => __( 'Email', 'wp-super-dealer' ),
			'slug' => 'email',
			'type' => 'email',
			'value' => ( isset( $location['email'] ) ? $location['email'] : '' ),
			'class' => '',
			'tip' => array(
				'title' => __( 'Tip', 'wp-super-dealer' ),
				'content' => __( 'This is the email address someone should use for more information on vehicles from this location.', 'wp-super-dealer' ),
			),
			'placeholder' => __( 'sales@some-site.com', 'wp-super-dealer' ),
		),
    );

	$fields = apply_filters( 'wpsd_location_contact_fields_filter', $fields, 'contact', $location );
    return $fields;
}

function wpsd_get_location_fields_links( $time, $location_slug, $location ) {
    $tip_content = '';
    $tip_content .= '<p>';
        $tip_content .= __( 'Enter a link to the form you would like to use.', 'wp-super-dealer' );
    $tip_content .= '</p>';
    $tip_content .= '<p>';
        $tip_content .= __( 'You may use vehicle template tags in your links by enclosing a specification field in brackets {}.', 'wp-super-dealer' );
    $tip_content .= '</p>';
    $tip_content .= '<p>';
        $tip_content .= __( 'exp: https://some-site.com/finance/?stock_number={stock_number}&year={vehicle_year}&make={make}&model={model}', 'wp-super-dealer' );
    $tip_content .= '</p>';
    $fields = array(
		$time + rand( 10,1000 ) => array(
			'id_name' => 'location-' . $location_slug . '-url_contact_form',
			'label' => __( 'Contact Form URL', 'wp-super-dealer' ),
			'key' => $location_slug,
			'slug' => 'url_contact_form',
			'type' => 'text',
			'value' => ( isset( $location['url_contact_form'] ) ? $location['url_contact_form'] : '' ),
			'class' => '',
			'tip' => array(
				'title' => __( 'Tip', 'wp-super-dealer' ),
				'content' => $tip_content,
			),
			'placeholder' => __( 'exp: https://some-site.com/contact/', 'wp-super-dealer' ),
		),
		$time + rand( 10,1000 ) => array(
			'id_name' => 'location-' . $location_slug . '-url_finance',
			'label' => __( 'Finance Application URL', 'wp-super-dealer' ),
			'key' => $location_slug,
			'slug' => 'url_finance',
			'type' => 'text',
			'value' => ( isset( $location['url_finance'] ) ? $location['url_finance'] : '' ),
			'class' => '',
			'tip' => array(
				'title' => __( 'Tip', 'wp-super-dealer' ),
				'content' => $tip_content,
			),
			'placeholder' => __( 'exp: https://some-site.com/finance/', 'wp-super-dealer' ),
		),
		$time + rand( 10,1000 ) => array(
			'id_name' => 'location-' . $location_slug . '-url_trade_in',
			'label' => __( 'Trade-In Form URL', 'wp-super-dealer' ),
			'key' => $location_slug,
			'slug' => 'url_trade_in',
			'type' => 'text',
			'value' => ( isset( $location['url_trade_in'] ) ? $location['url_trade_in'] : '' ),
			'class' => '',
			'tip' => array(
				'title' => __( 'Tip', 'wp-super-dealer' ),
				'content' => $tip_content,
			),
            'placeholder' => __( 'exp: https://some-site.com/trade-in/', 'wp-super-dealer' ),
		),
		$time + rand( 10,1000 ) => array(
			'id_name' => 'location-' . $location_slug . '-url_warranty',
			'label' => __( 'Warranty URL', 'wp-super-dealer' ),
			'key' => $location_slug,
			'slug' => 'url_warranty',
			'type' => 'text',
			'value' => ( isset( $location['url_warranty'] ) ? $location['url_warranty'] : '' ),
			'class' => '',
			'tip' => array(
				'title' => __( 'Tip', 'wp-super-dealer' ),
				'content' => $tip_content,
			),
            'placeholder' => __( 'exp: https://some-site.com/warranty/', 'wp-super-dealer' ),
		),
    );
    
	$fields = apply_filters( 'wpsd_location_links_fields_filter', $fields, 'links', $location );
    return $fields;
}

function wpsd_get_location_fields_all( $time = '123', $location_slug = 'default', $location = array( 'name' => 'Default' ) ) {
    $address = wpsd_get_location_fields_address( $time, $location_slug, $location );
    $vehicle = wpsd_get_location_fields_vehicle( $time, $location_slug, $location );
    $contact = wpsd_get_location_fields_contact( $time, $location_slug, $location );
    $links = wpsd_get_location_fields_links( $time, $location_slug, $location );
    $fields = array_merge( $address, $vehicle, $contact, $links );
    $fields = apply_filters( 'wpsd_get_location_fields_all_filter', $fields, $time, $location_slug, $location );
    return $fields;
}

function wpsd_locations_instructions() {
    $html = '';
	$html .= '<div class="wpsd-instructions-wrap wpsd-instructions-wrap-locations">';
		$html .= '<div class="wpsd-instructions-title" data-tab="locations">';
			$html .= '<span class="dashicons dashicons-welcome-learn-more"></span>';
			$html .=  __( 'Tips & Tricks - Locations', 'wp-super-dealer' );
		$html .= '</div>';

		$html .=  '<ul class="wpsd-instructions">';
            $html .= '<li>';
                $html .= '<div class="wpsd-tnt-wrap">';
                    $html .= '<div class="wpsd-tnt-col">';
						$html .= '<div class="wpsd-video-wrap">';
                            $html .= '<span class="wpsd-youtube" data-video-url="https://www.youtube.com/embed/4-AkkprOyss"></span>';
						$html .= '</div>';
                    $html .= '</div>';
                    $html .= '<div class="wpsd-tnt-col">';
                        $html .= '<h2>';
                            $html .=  __( 'Tips & Tricks for Location Settings', 'wp-super-dealer' );
                        $html .= '</h2>';
                        $html .= '<ol>';
                            $html .= '<li>';
                                $html .=  __( 'WPSuperDealer supports multiple locations. These can be physical locations or departments with different contact information.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .=  __( 'Locations are important because they control the contact information and a couple basic price controls for your vehicles.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .=  __( 'If you don\'t add a location to a vehicle then WPSuperDealer will automatically assign the first location it finds to the vehicle.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .=  __( 'If you somehow delete all your locations then WPSuperDealer should create a default for you, if it doesn\'t then your vehicles may not display their prices. So if you find your prices missing then make sure you have a location setup.', 'wp-super-dealer' );
                            $html .= '</li>';
                        $html .= '</ol>';
                    $html .= '</div>';
                $html .= '</div>'; //= end $html .= '<div class="wpsd-tnt-wrap">';
                $html .= wpsd_instructions_footer();
            $html .= '</li>';
		$html .= '</ul>';
		$html .= '<div class="wpsd-instructions-hide" data-tab="locations">';
			$html .=  __( 'show', 'wp-super-dealer' );
		$html .= '</div>';
	$html .= '</div>';
    return $html;
}

function wpsd_locations_settings_save( $args = array() ) {
	if ( ! isset( $args[0]['fields'] ) ) {
		return __( 'No fields found to update.', 'wp-super-dealer' );
	}
	$args = apply_filters( 'wpsd_save_location_args_filter', $args );

	$locations = array();
	foreach( $args[0]['fields'] as $field=>$items ) {
		//= Swap dashes for underscores
		$_field = str_replace( '-', '_', $field );
		$location_term = get_term_by( 'slug', $field, 'location' );
		$locations[ $_field ] = (array)$items;
		if ( is_wp_error( $location_term ) ) {
			return print_r( is_wp_error( $location_term ), true );
		}

    $location_term_id = $location_term->term_id;
		//= Get  each field sent for this location
		foreach( $items as $k=>$v ) {
			//= if this is the location name field then handle it different
			if ( 'location_name' === $k ) {
				if ( empty( $v ) ) {
					//= if the name is blank then do nothing
					continue;
				}
				$term_args = array(
					'name' => $v,
				);
				$updated_term = wp_update_term( $location_term_id, 'location', $term_args );
				if ( is_wp_error( $updated_term ) ) {
					//= do something if the name update fails
				}
				//= go to the next item
				continue;
			}
			//= handle normal tax_meta fields
			$value = sanitize_text_field( $v );
			update_term_meta( $location_term_id, $k, $value );
		}
	}

	$x = __( 'Locations updated', 'wp-super-dealer' );
	return $x;
}

function wpsd_create_default_location( $location ) {
	if ( empty( $location ) ) {
		// since this is the default location we want to use 'default' as the slug
		$location = __( 'Default', 'wp-super-dealer' );
	}
	$slug = sanitize_title( $location );
	$slug = trim( $slug );
	$slug = strtolower( $slug );
    $_slug = str_replace( '-', '_', $slug );

	$term_exists = term_exists( $location['dealer_name'], 'vehicle_location' );

	if ( ! $term_exists ) {
		$location_id = wp_insert_term(
			$location, // the term 
			'location', // the taxonomy
			array(
				'description'=> $location,
				'slug' => $slug,
			)
		);
	}

	return $location_id;
}

function wpsd_location_fields() {
	$fields = array(
		'name' => '',
		'phone_main' => '',
		'address' => '',
		'city' => '',
		'state' => '',
		'zip' => '',
		'hide_prices' => false,
		'add_to_price' => 0,
		'contact_name' => '',
		'phone_contact' => '',
		'email' => '',
		'url_contact_form' => '',
		'url_finance' => '',
		'url_trade_in' => '',
		'url_warranty' => '',
	);
	$fields = apply_filters( 'wpsd_location_fields_filter', $fields );
	return $fields;
};
?>