<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_price_tab() {
	$html = '';
	$html = apply_filters( 'wpsd_admin_price_filter', $html );
	if ( ! empty( $html ) ) {
		return $html;
	}
	global $wpsd_options;
	//= Currency Start
	
	$global_fields = wpsd_get_global_fields();

	$form_fields = new WP_SUPER_DEALER\WPSD_FIELDS;
	$time = time();
	
    $html .= '<h3>';
        $html .= __( 'Price Options', 'wp-super-dealer' );
    $html .= '</h3>';
    
    $html .= wpsd_price_instructions();
    
	$wpsd_vehicle_options = wpsd_get_vehicle_options();
	$price_fields = get_price_fields();
	$global_specs = $wpsd_vehicle_options['global_specs'];
	$active_fields = array();
	$active_fields_value = array();
	foreach( $price_fields as $price_field=>$data ) {
		if ( isset( $global_specs[ $price_field ] ) ) {
			if ( isset( $global_specs[ $price_field ]['disabled'] ) ) {
				$disabled = $global_specs[ $price_field ]['disabled'];
			} else {
				$disabled = false;
			}
			$active_fields[ $price_field ] = array(
				'label' => $data['label'],
				'value' => $price_field,
			);
			if ( 'true' === $disabled || true === $disabled ) {
				continue;
			}
			$active_fields_value[] = $price_field;
		}
	}

	$note = __( 'You can change price labels and default values for the active price fields on the ', 'wp-super-dealer' );
	$note .= '<span class="wpsd-switch-tab" data-tab="vehicle_options">';
		$note .= __( 'Vehicle Options', 'wp-super-dealer' );
	$note .= '</span>';
	$note .= __( ' tab.', 'wp-super-dealer' );
	
	$tip_content_1 = '';
	$tip_content_1 .= '<p>';
		$tip_content_1 .= __( 'Uncheck a field to disable it.', 'wp-super-dealer' );
	$tip_content_1 .= '</p>';
	$tip_content_1 .= '<p>';
		$tip_content_1 .= __( 'This will not remove data from your vehicles if it has already been entered.', 'wp-super-dealer' );
	$tip_content_1 .= '</p>';

	$tip_content_2 = '';
	$tip_content_2 .= '<p>';
		$tip_content_2 .= __( 'The value in this field will appear directly before each vehicle price.', 'wp-super-dealer' );
	$tip_content_2 .= '</p>';
	$tip_content_2 .= '<p>';
		$tip_content_2 .= __( 'Typically this field is used for a currency symbol.', 'wp-super-dealer' );
	$tip_content_2 .= '</p>';
	
	$tip_content_3 = '';
	$tip_content_3 .= '<p>';
		$tip_content_3 .= __( 'The value in this field will appear directly after each vehicle price.', 'wp-super-dealer' );
	$tip_content_3 .= '</p>';
	$tip_content_3 .= '<p>';
		$tip_content_3 .= __( 'If you want your prices to have .00 at the end then you could enter it here.', 'wp-super-dealer' );
	$tip_content_3 .= '</p>';

	$tip_content_4 = '';
	$tip_content_4 .= '<p>';
		$tip_content_4 .= __( 'The value in this field will appear if a vehicle has no price or a price of 0.', 'wp-super-dealer' );
	$tip_content_4 .= '</p>';

	$fields = array(
		$time + 1 => array(
			'label' => __( 'Active Price Fields', 'wp-super-dealer' ),
			'slug' => 'active_fields',
			'type' => 'checkbox',
			'value' => $active_fields_value,
            'options' => $active_fields,
			'class' => '',
			'tip' => array(
				'title' => __( '', 'wp-super-dealer' ),
				'content' => $tip_content_1,
			),
		),
		$time + 2 => array(
			'label' => __( 'Note:', 'wp-super-dealer' ),
			'slug' => 'note',
			'type' => 'comment',
			'value' => $note,
			'class' => '',
			'tip' => array(),
		),
		$time + 3 => array(
			'label' => __( 'Before Price', 'wp-super-dealer' ),
			'slug' => 'price_before',
			'type' => 'text',
			'value' => ( isset( $wpsd_options['price_before'] ) ? $wpsd_options['price_before'] : __( '$', 'wp-super-dealer' ) ),
			'class' => 'price_before',
			'tip' => array(
				'title' => __( '', 'wp-super-dealer' ),
				'content' => $tip_content_2,
			),
			'placeholder' => '$',
		),
		$time + 4 => array(
			'label' => __( 'After Price', 'wp-super-dealer' ),
			'slug' => 'price_after',
			'type' => 'text',
			'value' => ( isset( $wpsd_options['price_after'] ) ? $wpsd_options['price_after'] : __( '', 'wp-super-dealer' ) ),
			'class' => '',
			'tip' => array(
				'title' => __( '', 'wp-super-dealer' ),
				'content' => $tip_content_3,
			),
			'placeholder' => 'example: .00',
		),
		$time + 5 => array(
			'label' => __( 'Default no price text', 'wp-super-dealer' ),
			'slug' => 'no_price',
			'type' => 'text',
			'value' => ( isset( $wpsd_options['no_price'] ) ? $wpsd_options['no_price'] : __( 'Call for price', 'wp-super-dealer' ) ),
			'class' => '',
			'tip' => array(
				'title' => __( '', 'wp-super-dealer' ),
				'content' => $tip_content_4,
			),
			'placeholder' => '',
		),
    );

	$fields = apply_filters( 'wpsd_admin_price_fields_filter', $fields );
	$label = __( 'Price Options', 'wp-super-dealer' );
    $content = $form_fields->get_form_fields( $fields );
	$html .= wpsd_admin_group_wrap( $label, $content, $fields );
	
	return $html;
}

function wpsd_price_settings_save( $args ) {
	//= Get the global settings so we can update them
	global $wpsd_options;
	if ( ! isset( $args[0]['fields'] ) ) {
		return __( 'No fields found to update.', 'wp-super-dealer' );
	}

    foreach( $args[0]['fields'] as $field=>$value ) {
		//= Swap dashes for underscores
		$_field = str_replace( '-', '_', $field );
		$wpsd_options[ $_field ] = sanitize_text_field( $value );
	}

	$active_price_fields = $args[0]['fields']->active_fields;
	$active_price_fields = array_flip( $active_price_fields );

	$wpsd_vehicle_options = wpsd_get_vehicle_options();
	$price_fields = get_price_fields();
	$global_specs = $wpsd_vehicle_options['global_specs'];

	foreach( $price_fields as $price_field=>$data ) {
		$spec_settings = $global_specs[ $price_field ];
		if ( isset( $active_price_fields[ $price_field ] ) ) {
			$spec_settings['disabled'] = false;
		} else {
			$spec_settings['disabled'] = true;			
		}
		$global_specs[ $price_field ] = $spec_settings;
	}
	$wpsd_vehicle_options['global_specs'] = $global_specs;
	
	//= Update vehicle options
	update_option( 'wpsd-vehicle-options', $wpsd_vehicle_options, false );
	//= Update the general options
	update_option( 'wpsd_options', $wpsd_options );

	return __( 'Settings Saved', 'wp-super-dealer' );
}

function wpsd_price_instructions() {
    $html = '';
	$html .= '<div class="wpsd-instructions-wrap wpsd-instructions-wrap-price">';
		$html .= '<div class="wpsd-instructions-title" data-tab="price">';
			$html .= '<span class="dashicons dashicons-welcome-learn-more"></span>';
			$html .=  __( 'Tips & Tricks - Price Options', 'wp-super-dealer' );
		$html .= '</div>';
		$html .=  '<ul class="wpsd-instructions">';
            $html .= '<li>';
                $html .= '<div class="wpsd-tnt-wrap">';
                    $html .= '<div class="wpsd-tnt-col">';
						$html .= '<div class="wpsd-video-wrap">';
                            $html .= '<span class="wpsd-youtube" data-video-url="https://www.youtube.com/embed/nd-PtLS5Hdg"></span>';
						$html .= '</div>';
                    $html .= '</div>';
                    $html .= '<div class="wpsd-tnt-col">';
                        $html .= '<h2>';
                            $html .=  __( 'Tips & Tricks for Price Options', 'wp-super-dealer' );
                        $html .= '</h2>';
                        $html .= '<ol>';
                            $html .= '<li>';
                                $html .=  __( 'By default there are 4 price fields availabe; Retail Price, Rebates, Discounts & Price.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .=  __( 'You may turn off any of the price fields by unchecking them and saving.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .=  __( 'You also have the ability to change what shows up right before or right after your price.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .=  __( 'There is also an option for what to show if a vehicle doesn\'t have a price.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .=  __( 'The ability to bulk turn off prices can be found in your location settings.', 'wp-super-dealer' );
                            $html .= '</li>';
                        $html .= '</ol>';
                    $html .= '</div>';
                $html .= '</div>'; //= end $html .= '<div class="wpsd-tnt-wrap">';
                $html .= wpsd_instructions_footer();
            $html .= '</li>';
		$html .= '</ul>';
		$html .= '<div class="wpsd-instructions-hide" data-tab="price">';
			$html .=  __( 'show', 'wp-super-dealer' );
		$html .= '</div>';
	$html .= '</div>';
    return $html;
}

?>