<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_news_content() {
	$plugin_file = WPSD_DIR . 'wp-super-dealer.php';
	$plugin_data = get_plugin_data( $plugin_file, true, true );
	$wpsd_version = $plugin_data['Version'];

	$html = '';
    $html .= '<h3>';
        $html .= __( 'News', 'wp-super-dealer' );
    $html .= '</h3>';
	
	return $html;
}

?>