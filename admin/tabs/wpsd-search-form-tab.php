<?php
function wpsd_search_form_tab() {
	$html = '';
	$html = apply_filters( 'wpsd_admin_search_filter', $html );
	if ( ! empty( $html ) ) {
		return $html;
	}

    $html .= '<h3>';
        $html .= __( 'Search Form', 'wp-super-dealer' );
    $html .= '</h3>';
    
    $html .= wpsd_search_instructions();
    
    //= Search Result Start
	$html .= wpsdsp_options_do_page();
	return $html;
}

function wpsd_search_instructions() {
    $html = '';
	$html .= '<div class="wpsd-instructions-wrap wpsd-instructions-wrap-search">';
		$html .= '<div class="wpsd-instructions-title" data-tab="search">';
			$html .= '<span class="dashicons dashicons-welcome-learn-more"></span>';
			$html .=  __( 'Tips & Tricks - Search Form', 'wp-super-dealer' );
		$html .= '</div>';

		$html .=  '<ul class="wpsd-instructions">';
            $html .= '<li>';
                $html .= '<div class="wpsd-tnt-wrap">';
                    $html .= '<div class="wpsd-tnt-col">';
						$html .= '<div class="wpsd-video-wrap">';
                            $html .= '<span class="wpsd-youtube" data-video-url="https://www.youtube.com/embed/xwq-RU-vqd4"></span>';
						$html .= '</div>';
                    $html .= '</div>';
                    $html .= '<div class="wpsd-tnt-col">';
                        $html .= '<h2>';
                            $html .=  __( 'Tips & Tricks for Vehicle Search Forms', 'wp-super-dealer' );
                        $html .= '</h2>';
                        $html .= '<ol>';
                            $html .= '<li>';
                                $html .=  __( 'If you have a small inventory then you might want to limit the number of searchable fields to reduce the chances of someone not finding any matching vehicles.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .=  __( 'If have a larger inventory, say more than 300 or so active vehicles, then you might need to reduce your search fields to just 4 or 5 top fields.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .=  __( 'The search form is powered by a static json file. Think of it as a file that has a miniture version of your inventory in it along with all the searchable fields. You don\'t want that file to be too big, so if you have a large inventory make sure you test things from a user perspective to make sure it flows quickly and smoothly.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .=  __( 'If you have a super large inventory then you might want to turn off the inventory count, then create your own static json file containing just the core vehicle information. That way you fully control the size of the json file and it\'s contents.', 'wp-super-dealer' );
                            $html .= '</li>';
                        $html .= '</ol>';
                    $html .= '</div>';
                $html .= '</div>'; //= end $html .= '<div class="wpsd-tnt-wrap">';
                $html .= wpsd_instructions_footer();
            $html .= '</li>';

			$html .= '<li>';
	
				$html .= __('This PlugIn uses a json file to populate the search form(s).', 'wp-super-dealer');
				$html .= '<br />';
				$html .= '<br />';

				$html .= __("Think of it as a text file that gets downloaded to your visitor's browser and contains the vehicle information displayed in the form.", "wp-super-dealer/search");
				$html .= '<br />';
				$html .= '<br />';

				$html .= __('You can click on the "Build json file" button on this tab to regenerate your json file at anytime.', 'wp-super-dealer');
				$html .= '<br />';
				$html .= '<br />';

				$html .= __('There is also a setting to schedule a daily update of the json file.', 'wp-super-dealer');
				$html .= '<br />';
				$html .= '<br />';

				$html .= "<b style='color:#f00;'>".__("DON'T forget to setup your schedule!", "wp-super-dealer/search")."</b>";
				$html .= '<br />';
				$html .= '<br />';

				$html .= "<b>".__ ("Your search form needs to update daily to correctly reflect your inventory.", "wp-super-dealer/search")."</b>";
				$html .= '<br />';
				$html .= '<br />';

				$html .= __('It is suggested that you setup the schedule for early in the morning.', 'wp-super-dealer');
				$html .= '<br />';
				$html .= '<br />';

				$html .= __('Anytime you make significant changes to your inventory you can manually "Build json file" to keep your search form up to date.', 'wp-super-dealer');
				$html .= __('', 'wp-super-dealer');
				$html .= '<br />';
				$html .= '<br />';

				$html .= '<h3>'.__('Displaying your form', 'wp-super-dealer').'</h3>';

				$html .= __('Your form can be displayed with the block, the widget or the shortcode [wpsd_search].', 'wp-super-dealer');
				$html .= '<br />';

				$html .= __('You can use the following parameters with the shortcode;', 'wp-super-dealer');
				$html .= '<br />';
				$html .= '<ul>';
					$html .= '<li>';
						$atts = array(
							'title' => __( 'This is an optional form title you can add to your form.', 'wp-super-dealer' ),
							'custom_class' => 'wpsdsp-search-form-one, wpsdsp-search-form-two, wpsdsp_no_expand or your own class(es)',
							'form_action' => get_option('siteurl') .' //= change the form "action" to a specific url.',
							'hide_count' => 'on' . ' //= hide item counts in the form - on is true and will hide counts',
							'field_map' => 'Use this to order your fields. Enter their slugs in a list seperated by pipes |.',
						);

						$fields = wpsdsp_active_fields();
						foreach( $fields as $field=>$options ) {
							$atts[ $field ] = __( 'Form field. * See instructions below.', 'wp-super-dealer' );
						}

						ob_start();
							echo '<pre>';
								print_r( $atts );
							echo '</pre>';
						$params = ob_get_contents();
						ob_end_clean();
						$params = str_replace('Array', '', $params);
						$params = str_replace('(', '', $params);
						$params = str_replace(')', '', $params);
						$html .= esc_html( $params );
					$html .= '</li>';
				$html .= '</ul>';
				$html .= '<b>' . __( 'Example:', 'wp-super-dealer' ) . '</b>';
				$html .= '<br />';
				$html .= '[wpsd_search title="" form_action="' . get_option('siteurl') . '" label_button="Search"]';
				$html .= '<br />';
				$html .= '<br />';
				$html .= __( 'This would create a search form that points to ' . get_option('siteurl') . ' with a blank title and a submit button that says "Search".', 'wp-super-dealer' );
				$html .= '<br />';
				$html .= '<hr />';
                
                $html .= '<h3>'.__('Field Parameters', 'wp-super-dealer').'</h3>';
				$html .= __( 'Field parameters can be used to remove fields, hide fields, give them default values, change their labels, change the element type and load the field "closed" so it has to be clicked on to expand it.', 'wp-super-dealer' );
				$html .= '<br />';
				$html .= '<br />';
				$html .= __( 'Field parameters can be in this format: "label=Your Custom Label&type=select&value=your default value&closed=1&hide=1&remove=1"', 'wp-super-dealer' );
				$html .= '<br />';
				$html .= '<br />';
				$html .= __( 'You can think of them as parameters for each field parameter. So parameters that have parameters, that\'s not confusing right?', 'wp-super-dealer' );
				$html .= '<br />';
				$html .= '<br />';

				$html .= __( 'For example, if you wanted to create a form without a year field you would use a shortcode like this:', 'wp-super-dealer' );
				$html .= '<br />';
				$html .= '<br />';
				$html .= '[wpsd_search year="remove=1"]';

				$html .= '<br />';
				$html .= '<br />';
				$html .= __( 'If you wanted to create a form for a "trucks" page and only wanted to filter trucks then you could hide the body style and set the default value to "trucks" like this:', 'wp-super-dealer' );
				$html .= '<br />';
				$html .= '<br />';
				$html .= '[wpsd_search body_style="hide=1&value=truck"]';

				$html .= '<br />';
				$html .= '<br />';
				$html .= __( 'If you wanted to create a form without a year field for a "trucks" page and and wanted to change the "make" field to checkboxes and label it "Built By" you would do this:', 'wp-super-dealer' );
				$html .= '<br />';
				$html .= '<br />';
				$html .= '[wpsd_search year="remove=1" body_style="hide=1&value=truck" make="type=checkbox&label=Built By"]';

				$html .= '<hr />';
				$html .= '<br />';
				$html .= '<h3>' . __( 'Tips', 'wp-super-dealer' ) . '</h3>';
				$html .= __( 'You can call the search form function directly in your template:', 'wp-super-dealer' );
				$html .= '<br />';
				$html .= 'wpsd_search_form( $settings );';
				$html .= '<br />';
				$html .= '<br />';
				$html .= __( '$settings needs to be an associative array using the fields listed above under "Displaying your form".', 'wp-super-dealer' );
				$html .= '<br />';
				$html .= '$settings = array ("title" = > "My Title", "location" => array( $field_atts ) );';
	
			$html .= '</li>';
		$html .= '</ul>';
		$html .= '<div class="wpsd-instructions-hide" data-tab="search">';
			$html .=  __( 'show', 'wp-super-dealer' );
		$html .= '</div>';
	$html .= '</div>';
    return $html;
}

function wpsd_get_search_settings() {
	$default_settings = wpsd_default_search_settings();
	$settings = get_option( 'wpsd-search-settings', $default_settings );
	$settings = wp_parse_args( $settings, $default_settings );
	return $settings;
}

function wpsd_default_search_settings() {
	$fields = array(
		'hour' => '1',
		'minute' => '00',
		'email_report' => '',
		'update_save_json' => '1',
		'save_json' => __( 'on', 'wp-super-dealer' ),
		'hide_count' => __( 'off', 'wp-super-dealer' ),
		'autoload' => '1',
		'mobile_action' => __( 'tap click', 'wp-super-dealer' ),
	);
	return $fields;
}

function wpsd_get_search_fields() {
	$default_settings = wpsd_default_search_fields();
	$settings = get_option( 'wpsd-search-fields', $default_settings );
	$settings = wp_parse_args( $settings, $default_settings );
	return $settings;
}

function wpsd_default_search_fields() {
	$fields = array (
      'location' => 
      array (
        'label' => 'Location',
        'search_field' => 'location',
        'search_field_min' => '',
        'search_field_max' => '',
        'field_name' => 'location',
        'field_type' => 'meta',
        'type' => 'select',
        'min' => '',
        'max' => '',
        'gap' => '',
        'value' => '',
        'json_column' => '',
      ),
      'stock_number' => 
      array (
        'label' => 'Stock Number',
        'search_field' => 'stock_number',
        'search_field_min' => '',
        'search_field_max' => '',
        'field_name' => 'stock_number',
        'field_type' => 'meta',
        'type' => 'select',
        'min' => '',
        'max' => '',
        'gap' => '',
        'value' => '',
        'json_column' => '',
      ),
      'vin' => 
      array (
        'label' => 'Vin',
        'search_field' => 'vin',
        'search_field_min' => '',
        'search_field_max' => '',
        'field_name' => 'vin',
        'field_type' => 'meta',
        'type' => 'select',
        'min' => '',
        'max' => '',
        'gap' => '',
        'value' => '',
        'json_column' => '',
      ),
      'condition' => 
      array (
        'label' => 'Condition',
        'search_field' => 'condition',
        'search_field_min' => '',
        'search_field_max' => '',
        'field_name' => 'condition',
        'field_type' => 'meta',
        'type' => 'select',
        'min' => '',
        'max' => '',
        'gap' => '',
        'value' => '',
        'json_column' => '',
      ),
      'vehicle_year' => 
      array (
        'active' => '1',
        'label' => 'Vehicle Year',
        'search_field' => 'vehicle_year',
        'search_field_min' => 'vehicle_year_min',
        'search_field_max' => 'vehicle_year_max',
        'field_name' => 'vehicle_year',
        'field_type' => 'meta',
        'type' => 'range',
        'min' => '1980',
        'max' => '2021',
        'gap' => '1',
        'value' => '',
        'filter_by' => '1',
        'json_column' => 'a',
      ),
      'make' => 
      array (
        'active' => '1',
        'label' => 'Make',
        'search_field' => 'make',
        'search_field_min' => '',
        'search_field_max' => '',
        'field_name' => 'make',
        'field_type' => 'meta',
        'type' => 'select',
        'min' => '',
        'max' => '',
        'gap' => '',
        'value' => '',
        'dynamic' => '1',
        'filter_by' => '1',
        'json_column' => 'b',
      ),
      'model' => 
      array (
        'active' => '1',
        'label' => 'Model',
        'search_field' => 'model',
        'search_field_min' => '',
        'search_field_max' => '',
        'field_name' => 'model',
        'field_type' => 'meta',
        'type' => 'select',
        'min' => '',
        'max' => '',
        'gap' => '',
        'value' => '',
        'dynamic' => '1',
        'filter_by' => '1',
        'json_column' => 'c',
      ),
      'vehicle_tags' => 
      array (
        'label' => 'Vehicle Tags',
        'search_field' => 'vehicle_tags',
        'search_field_min' => '',
        'search_field_max' => '',
        'field_name' => 'vehicle_tags',
        'field_type' => 'meta',
        'type' => 'select',
        'min' => '',
        'max' => '',
        'gap' => '',
        'value' => '',
        'json_column' => '',
      ),
      'retail_price' => 
      array (
        'label' => 'Retail Price',
        'search_field' => 'retail_price',
        'search_field_min' => '',
        'search_field_max' => '',
        'field_name' => 'retail_price',
        'field_type' => 'meta',
        'type' => 'select',
        'min' => '',
        'max' => '',
        'gap' => '',
        'value' => '',
        'json_column' => '',
      ),
      'rebates' => 
      array (
        'label' => 'Rebates',
        'search_field' => 'rebates',
        'search_field_min' => '',
        'search_field_max' => '',
        'field_name' => 'rebates',
        'field_type' => 'meta',
        'type' => 'select',
        'min' => '',
        'max' => '',
        'gap' => '',
        'value' => '',
        'json_column' => '',
      ),
      'discount' => 
      array (
        'label' => 'Discount',
        'search_field' => 'discount',
        'search_field_min' => '',
        'search_field_max' => '',
        'field_name' => 'discount',
        'field_type' => 'meta',
        'type' => 'select',
        'min' => '',
        'max' => '',
        'gap' => '',
        'value' => '',
        'json_column' => '',
      ),
      'price' => 
      array (
        'active' => '1',
        'label' => 'Price',
        'search_field' => 'price',
        'search_field_min' => 'price_min',
        'search_field_max' => 'price_max',
        'field_name' => 'price',
        'field_type' => 'meta',
        'type' => 'range',
        'min' => '0',
        'max' => '150000',
        'gap' => '10000',
        'value' => '',
        'filter_by' => '1',
        'json_column' => 'd',
      ),
      'body_style' => 
      array (
        'active' => '1',
        'label' => 'Body Style',
        'search_field' => 'body_style',
        'search_field_min' => '',
        'search_field_max' => '',
        'field_name' => 'body_style',
        'field_type' => 'meta',
        'type' => 'select',
        'min' => '',
        'max' => '',
        'gap' => '',
        'value' => '',
        'dynamic' => '1',
        'filter_by' => '1',
        'json_column' => 'e',
      ),
      'mileage' => 
      array (
        'active' => '1',
        'label' => 'Mileage',
        'search_field' => 'mileage',
        'search_field_min' => 'mileage_min',
        'search_field_max' => 'mileage_max',
        'field_name' => 'mileage',
        'field_type' => 'meta',
        'type' => 'range',
        'min' => '0',
        'max' => '500000',
        'gap' => '50000',
        'value' => '',
        'json_column' => 'f',
      ),
      'mpg_highway' => 
      array (
        'label' => 'Mpg Highway',
        'search_field' => 'mpg_highway',
        'search_field_min' => 'mpg_highway_min',
        'search_field_max' => 'mpg_highway_max',
        'field_name' => 'mpg_highway',
        'field_type' => 'meta',
        'type' => 'range',
        'min' => '',
        'max' => '',
        'gap' => '',
        'value' => '',
        'json_column' => '',
      ),
      'mpg_city' => 
      array (
        'label' => 'Mpg City',
        'search_field' => 'mpg_city',
        'search_field_min' => 'mpg_city_min',
        'search_field_max' => 'mpg_city_max',
        'field_name' => 'mpg_city',
        'field_type' => 'meta',
        'type' => 'range',
        'min' => '',
        'max' => '',
        'gap' => '',
        'value' => '',
        'json_column' => '',
      ),
      'trim_level' => 
      array (
        'label' => 'Trim Level',
        'search_field' => 'trim_level',
        'search_field_min' => '',
        'search_field_max' => '',
        'field_name' => 'trim_level',
        'field_type' => 'meta',
        'type' => 'select',
        'min' => '',
        'max' => '',
        'gap' => '',
        'value' => '',
        'json_column' => '',
      ),
      'exterior_color' => 
      array (
        'label' => 'Exterior Color',
        'search_field' => 'exterior_color',
        'search_field_min' => '',
        'search_field_max' => '',
        'field_name' => 'exterior_color',
        'field_type' => 'meta',
        'type' => 'select',
        'min' => '',
        'max' => '',
        'gap' => '',
        'value' => '',
        'json_column' => '',
      ),
      'interior_color' => 
      array (
        'label' => 'Interior Color',
        'search_field' => 'interior_color',
        'search_field_min' => '',
        'search_field_max' => '',
        'field_name' => 'interior_color',
        'field_type' => 'meta',
        'type' => 'select',
        'min' => '',
        'max' => '',
        'gap' => '',
        'value' => '',
        'json_column' => '',
      ),
      'engine_type' => 
      array (
        'label' => 'Engine Type',
        'search_field' => 'engine_type',
        'search_field_min' => '',
        'search_field_max' => '',
        'field_name' => 'engine_type',
        'field_type' => 'meta',
        'type' => 'select',
        'min' => '',
        'max' => '',
        'gap' => '',
        'value' => '',
        'json_column' => '',
      ),
      'transmission' => 
      array (
        'label' => 'Transmission',
        'search_field' => 'transmission',
        'search_field_min' => '',
        'search_field_max' => '',
        'field_name' => 'transmission',
        'field_type' => 'meta',
        'type' => 'select',
        'min' => '',
        'max' => '',
        'gap' => '',
        'value' => '',
        'json_column' => '',
      ),
      'driveline' => 
      array (
        'label' => 'Driveline',
        'search_field' => 'driveline',
        'search_field_min' => '',
        'search_field_max' => '',
        'field_name' => 'driveline',
        'field_type' => 'meta',
        'type' => 'select',
        'min' => '',
        'max' => '',
        'gap' => '',
        'value' => '',
        'json_column' => '',
      ),
      'tank' => 
      array (
        'label' => 'Tank',
        'search_field' => 'tank',
        'search_field_min' => '',
        'search_field_max' => '',
        'field_name' => 'tank',
        'field_type' => 'meta',
        'type' => 'select',
        'min' => '',
        'max' => '',
        'gap' => '',
        'value' => '',
        'json_column' => '',
      ),
      'anti_brake_system' => 
      array (
        'label' => 'Anti Brake System',
        'search_field' => 'anti_brake_system',
        'search_field_min' => '',
        'search_field_max' => '',
        'field_name' => 'anti_brake_system',
        'field_type' => 'meta',
        'type' => 'select',
        'min' => '',
        'max' => '',
        'gap' => '',
        'value' => '',
        'json_column' => '',
      ),
      'steering_type' => 
      array (
        'label' => 'Steering Type',
        'search_field' => 'steering_type',
        'search_field_min' => '',
        'search_field_max' => '',
        'field_name' => 'steering_type',
        'field_type' => 'meta',
        'type' => 'select',
        'min' => '',
        'max' => '',
        'gap' => '',
        'value' => '',
        'json_column' => '',
      ),
    );
	return $fields;
}

function wpsd_search_form_settings_save( $args ) {
	$wpsdsp_custom_fields = wpsd_get_search_fields();
	
	if ( ! isset( $args[0]['fields'] ) ) {
		return __( 'No fields found to update.', 'wp-super-dealer' );
	}

	//	$settings = wpsd_get_search_settings();
	$field_settings = array();
    foreach( $args[0]['fields'] as $field=>$value ) {
		//= Swap dashes for underscores
		$_field = str_replace( '-', '_', $field );
		$field_settings[ $_field ] = json_decode( sanitize_text_field( $value ) );
	}
	$settings = wpsd_default_search_settings();
	
	$fields = array();
	if ( isset( $field_settings['fields'] ) ) {
		//= check for general search settings
		foreach( $settings as $setting=>$value ) {
			if ( isset( $field_settings['fields']->search->$setting ) ) {
				$settings[ $setting ] = $field_settings['fields']->search->$setting;
            }
		}
		update_option( 'wpsd-search-settings', $settings, false );

		//= get the settings for the spec fields
		if ( isset( $field_settings['fields']->field ) ) {
			if ( is_object( $field_settings['fields']->field ) ) {
                $total_active = 0;
				foreach( $field_settings['fields']->field as $field=>$options ) {
					$fields[ $field ] = (array)$options;

                    if ( isset( $fields[ $field ]['active'] ) ) {
                        ++$total_active;
                        $fields[ $field ]['json_column'] = wpsd_number_to_letter( $total_active );
                    }
                    
				} //= end foreach( $field_settings['fields']['field'] as $field=>$options ) {
			} //= end if ( is_array( $field_settings['fields']['field'] ) ) {
		} //= end if ( isset( $field_settings['fields']['field'] ) ) {

		update_option( 'wpsd-search-fields', $fields, false );
		
		wpsdsp_delete_cache();
		wpsdsp_build_cache();
	} //= end if ( isset( $field_settings['fields'] ) ) {

	return __( 'Search form settings updated.', 'wp-super-dealer' );
}

function wpsd_number_to_letter( $number ) {
    $number = $number - 1;
    $letters = range('a', 'z');
    return $letters[ $number ];    
}
?>