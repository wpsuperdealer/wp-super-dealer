<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_setup_guide() {
	$html = '';
	$html = apply_filters( 'wpsd_admin_setup_guide_filter', $html );
	if ( ! empty( $html ) ) {
		return $html;
	}
	$html = '<h3><span class="dashicons dashicons-book-alt"></span>' . __( 'Start up guide', 'wp-super-dealer' ) . '</h3>';

    $html .= wpsd_startup_guide_instructions();
    
	$html .= '<p>';
		$html .= '<h4 class="wpsd_setup_step">';
			$html .= '<span class="dashicons dashicons-admin-generic"></span>';
			$html .= __( '1. Setup your vehicle types', 'wp-super-dealer' );
		$html .= '</h4>';
		$html .= __( 'By default WPSuperDealer is setup for automobiles. ', 'wp-super-dealer' );
		$html .= __( 'If this site will be listing other vehicle types then you can add them below or on the "Vehicle Options" tab.', 'wp-super-dealer' );
	$html .= '</p>';

	$html .= '<div class="wpsd-default-types-wrap">';
		$types = wpsd_get_type_maps();
		$default_types = wpsd_default_type_maps();
		foreach( $default_types as $type=>$options ) {
			$class = '';
			if ( isset( $types[ $type ] ) ) {
				$class = ' wpsd-default-type-exists';
			}

			$html .= '<div class="wpsd-default-type-item' . esc_attr( $class ) . '" data-type="' . esc_attr( $type ) . '" title="' . esc_attr( $type ) . '">';
				$html .= '<label>';
					$html .= esc_html( $options['label'] );
				$html .= '</label>';
				$html .= '<div class="wpsd-default-type-icon-wrap">';
					$html .= '<img src="' . esc_url( $options['icon'] ) . '" />';
				$html .= '</div>';
			$html .= '</div>';
		}
	$html .= '</div>';
	
	$html .= '<p>&nbsp;</p>';
	
	$html .= '<p>';
		$html .= '<h4 class="wpsd_setup_step">';
			$html .= '<span class="dashicons dashicons-location-alt"></span>';
			$html .= __( '2. How many locations do you have?', 'wp-super-dealer' );
		$html .= '</h4>';
		$html .= __( 'WPSuperDealer supports multiple locations. If you have more than one location then you can visit the "Locations" tab to mange them.', 'wp-super-dealer' );
	
		$html .= '<p>';
			$html .= __( 'Make sure you fill out your ', 'wp-super-dealer' );
			$html .= __( 'Location Settings as completely as possible.', 'wp-super-dealer' );
		$html .= '</p>';

	$html .= '</p>';
		
	$html .= '<p>';
		$html .= '<h4 class="wpsd_setup_step">';
			$html .= '<span class="dashicons dashicons-migrate"></span>';
			$html .= __( '3. Now add the inventory to a page with either the block or shortcode.', 'wp-super-dealer');
		$html .= '<h4>';
	
	$html .= '</p>';
	$html .= '<pre>';
		$html .= '<b>[wpsd_inventory]</b>';
	$html .= '</pre>';

	$html .= '<p class="wpsd_setup_text">';
		$html .= '<b>';
			$html .= __( 'Parameters to filter inventory - accepts any active specification field, including the globals, as a parameters.', 'wp-super-dealer');
		$html .= '</b>';
	$html .= '</p>';
	$html .= '<p class="wpsd_setup_text">
			'.__('Example', 'wp-super-dealer').': [wpsd_inventory title="'.__('View our Inventory', 'wp-super-dealer').'" make="dodge" condition="new"]
		</p>
		<p class="wpsd_setup_inventory">
			'.__('Enter a page name here and hit submit and we\'ll create the page, insert the inventory block and if the box is checked we\'ll also add the vehicle search form block right above it.', 'wp-super-dealer').'
			<br /><br />
			<input type="text" name="create_inventory" class="create_inventory" id="create_inventory" value="'.__('Inventory', 'wp-super-dealer').'" />
			<br /><br />';
			$nonce = wp_create_nonce( 'wpsd_create_inventory_nonce' );
			$html .= '<input type="hidden" name="wpsd_create_inventory_nonce" id="wpsd_create_inventory_nonce" value="' . $nonce . '" />';
			$html .= '<input type="checkbox" name="create_inventory_search" class="create_inventory_search" value="yes" checked="checked" /> ';
			$html .= __('Add a search form to inventory page', 'wp-super-dealer') . '
			<br /><br />
			<input type="button" value="'.__('Add Inventory Page', 'wp-super-dealer').'" class="create_inventory_btn" />
			<span class="create_inventory_results"></span>
		</p>
		<p class="wpsd_setup_text">
			'.__('To change the visual appearce of your vehicles you can use custom CSS and PHP filters or you can use our WPSD Template Manager.', 'wp-super-dealer').'
		</p>
		<h4 class="wpsd_setup">
			'.__('Our Website has', 'wp-super-dealer').' <a href="http://wpsuperdealer.com" target="_blank">'.__('more information', 'wp-super-dealer').'</a> '.__('on creating your own layouts and leveraging WPSuperDealer in your theme.', 'wp-super-dealer').'
		</h4>';
	
		$html .= '<h4 class="wpsd_setup_step">';
			$html .= '<span class="dashicons dashicons-car"></span>';
			$html .= __( '4. After you\'ve setup your Inventory Page you might want to import a sample inventory.', 'wp-super-dealer');
		$html .= '</h4>';
		$html .= '<p class="wpsd_setup_text">
			<blockquote>
				<p class="wpsd_setup_text">
					'. __('Click the button below to import sample vehicles.', 'wp-super-dealer') .'
				</p>
				<p class="wpsd_setup_text">
					'. __('Number of sample vehicles to import:', 'wp-super-dealer') .' '. wpsd_select_sample_qty() .'
					<br />
					<input type="button" class="wpsd_insert_samples_btn" value="'. __('Insert Sample Vehicles Now', 'wp-super-dealer') .'" />';
					$nonce = wp_create_nonce( 'wpsd_insert_samples_nonce' );
					$html .= '<input type="hidden" name="wpsd_insert_samples_nonce" id="wpsd_insert_samples_nonce" value="' . $nonce . '" />';
				$html .= '</p>
				<p class="wpsd_setup_text wpsd_sample_inventory">
					'. __('After you click the button please be patient while the vehicles and their images are inserted.', 'wp-super-dealer') .'
				</p>
				<p class="wpsd_setup_text">
					'. __('The more vehicles you select the longer your sample import will take.', 'wp-super-dealer') .'
				</p>
				<p class="wpsd_setup_text">
					'. __('Sample gallery photos will be linked to rather than imported.', 'wp-super-dealer') .'
				</p>
				<p class="wpsd_setup_text">
					'. __('If you would like to import the gallery photos please add the following to your wp-config.php file:', 'wp-super-dealer') .'
					<br />
					define("WPSD_IMPORT_SAMPLE_PHOTOS", true);
				</p>
				<p class="wpsd_setup_text">
					'. __('Add this right before the line that says: That\'s all, stop editing! Happy blogging.', 'wp-super-dealer') .'
				</p>
				<p class="wpsd_setup_text">
					<b>'. __('NOTE: Importing all photos may cause your server to timeout. Use with caution!', 'wp-super-dealer') .'</b>
				</p>

			</blockquote>
		</p>';
		$html .= '<h4 class="wpsd_setup_step">';
			$html .= '<span class="dashicons dashicons-admin-tools"></span>';
			$html .= __('5. Next click the tabs on this page and customize the features you want to use.', 'wp-super-dealer');
		$html .= '</h4>';
	
		$html .= '
			<p class="wpsd_setup_text">
				'.__('After you adjust your settings you should', 'wp-super-dealer').'
				 <a href="options-permalink.php" target="_blank">
					'.__('click here', 'wp-super-dealer').'
				</a> 
				'.__('to update your permalinks.', 'wp-super-dealer').'
			</p>
			<p class="wpsd_setup_text">
				'.__('We suggest you use a permalink structure like this:', 'wp-super-dealer').'
				<br /><br />/%postname%/%post_id%/</b>
			</p>
		<p class="wpsd_setup_text">
			'.__('If you run into trouble please visit the', 'wp-super-dealer').
			' <a href="http://wordpress.org/support/plugin/wp-super-dealer" target="_blank">'.
				__('WordPress support forum', 'wp-super-dealer').
			'</a> '.
			__('for WPSuperDealer.', 'wp-super-dealer').'
		</p>
		<h4>
			'.__('For professional one on one assistance please contact us at our website', 'wp-super-dealer').' <a href="http://WPSuperDealer.com" target="_blank">WPSuperDealer.com</a>
		</h4>
	';
	return $html;
}

function wpsd_startup_guide_instructions() {
    $html = '';
	$html .= '<div class="wpsd-instructions-wrap wpsd-instructions-wrap-startup-guide">';
		$html .= '<div class="wpsd-instructions-title" data-tab="startup-guide">';
			$html .= '<span class="dashicons dashicons-welcome-learn-more"></span>';
			$html .=  __( 'Tips & Tricks - Startup Guide', 'wp-super-dealer' );
		$html .= '</div>';
		$html .=  '<ul class="wpsd-instructions">';
			$html .= '<li>';
                $html .= '<div class="wpsd-tnt-wrap">';
                    $html .= '<div class="wpsd-tnt-col">';
						$html .= '<div class="wpsd-video-wrap">';
                            $html .= '<span class="wpsd-youtube" data-video-url="https://www.youtube.com/embed/IBGbO_OoeqY"></span>';
						$html .= '</div>';
                    $html .= '</div>';
                    $html .= '<div class="wpsd-tnt-col">';
                        $html .= '<h2>';
                            $html .=  __( 'Tips & Tricks for the Startup Guide', 'wp-super-dealer' );
                        $html .= '</h2>';
                        $html .= '<ol>';
                            $html .= '<li>' . __( 'This page was setup to help you get started as quickly as possible. All the settings found on this page can also be found on the other tabs in their respective areas.', 'wp-super-dealer' ) . '</li>';
                            $html .= '<li>' . __( 'If you\'ve setup WPSuperDealer before or you feel confident in just jumping in then feel free to skip this tab. But we do suggest you look through it at least once.', 'wp-super-dealer' ) . '</li>';                        
                            $html .= '<li>' . __( 'As you go through the different tabs you\'ll find they all have "Tips & Tricks" sections just like this one.', 'wp-super-dealer' ) . '</li>';
                            $html .= '<li>' . __( 'Take the time to read the on-screen documentation. WPSuperDealer is very flexible, but that flexibility comes at the cost of complexity. The on-screen documentation is provided to help you take advantage of even the most advanced features.', 'wp-super-dealer' ) . '</li>';
                            $html .= '<li>' . __( 'If you have questions and can\'t find the answers on screen then swing by ', 'wp-super-dealer' );
                                //= TO DO Future: switch this to WordPress.org support forum
                                $html .= '<a href="https://wpsuperdealer.com" target="_blank">';
                                    $html .= __( 'our website', 'wp-super-dealer' );
                                $html .= '</a>';
                                $html .= __( ' and let us know.', 'wp-super-dealer' );
                            $html .= '</li>';
                        $html .= '</ol>';
                    $html .= '</div>';
                $html .= '</div>'; //= end $html .= '<div class="wpsd-tnt-wrap">';
                $html .= wpsd_instructions_footer();
			$html .= '</li>';
		$html .= '</ul>';
		$html .= '<div class="wpsd-instructions-hide" data-tab="startup-guide">';
			$html .=  __( 'hide', 'wp-super-dealer' );
		$html .= '</div>';
    
	$html .= '</div>';
    return $html;
}
?>