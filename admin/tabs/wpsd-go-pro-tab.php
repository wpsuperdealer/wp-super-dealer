<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_go_pro_tab() {
	$html = '';
	$html = apply_filters( 'wpsd_admin_go_pro_filter', $html );
	if ( ! empty( $html ) ) {
		return $html;
	}
    
    $html .= '<h3>';
        $html .= __( 'Go Pro!', 'wp-super-dealer' );
    $html .= '</h3>';
	//= TO DO Future: Edit Pro Tab
	$html .= wpsd_go_pro_instructions();
    
    $html .= '<div class="wpsd-col-wrap">';
        //= Start col 1
        $html .= '<div class="wpsd-col">';
            $html .= '<h2>';
                $html .= '<a href="http://wpsuperdealer.com" class="wpsd_home" target="_blank">';
                    $html .= 'You can see all of our add-ons at WPSuperDealer.com';
                $html .= '</a>';
            $html .= '</h2>';
            $html .= wpsd_add_on_list();
        $html .= '</div>';
    
        //= Start col 2
        $html .= '<div class="wpsd-col">';
            $html .= '
                    <div class="wpsd_go_pro_box">
                        <h2>WPSuperDealer comes with a lot of super powers.</h2>
                        <h3>But power is one of those things you can never have enough of.</h3>
                        <hr />
                        <h3>
                            Our Pro PlugIns & Custom Services can super charge your site!
                        </h3>
                        <hr />
                        <h4 class="wpsd_go_pro_header">
                            In addition to custom design and development work we offer a full line of PlugIns designed to extend WPSuperDealer and help you build a high quality, professional site for vehicle sales.
                        </h4>
                        <div class="wpsd_welcome_clear"></div>
                        <h3>
                            If you would like an estimate on custom design, development or consulting please contact us on our website.
                        </h3>
                        <div class="wpsd_welcome_clear"></div>';
                        $splash = array(
                            'title' => __( 'WPSuperDealer - All Add-Ons', 'wp-super-dealer' ),
                            'link' => 'https://wpsuperdealer.com/product-category/plugins/',
                            'image' => 'add-on wpsd.jpg',
                            'description' => __( 'View all of our PlugIns here', 'wp-super-dealer' ),
                        );
                        $html .= '<div class="wpsd-pro-splash">';
                            $html .= '<a href="' . esc_url( $splash['link'] ) . '" target="_blank" />';
                                $html .= '<img src="' . esc_url( WPSD_PATH . 'images/' . $splash['image'] ) . '" />';
                            $html .= '</a>';
                        $html .= '</div>';
    
                        $html .= '<h4 align="center">
                            <a href="http://wpsuperdealer.com" class="wpsd_home" target="_blank">
                                WPSuperDealer.com
                            </a>
                        </h4>
                    </div>
                ';
        $html .= '</div>';
    $html .= '</div>';
	return $html;
}

function wpsd_add_on_list() {
    $html = '';
    $addons = wpsd_pro_add_ons();
    $html = '<ul class="wpsd-addons">';
    foreach( $addons as $key=>$options ) {
        $html .= '<li>';
            $html .= '<a href="' . $options['link'] . '" target="_blank" />';
                $html .= '<img src="' . esc_url( WPSD_PATH . 'images/' . $options['image'] ) . '" />';
            $html .= '</a>';
            $html .= '<label>';
                $html .= $options['title'];
            $html .= '</label>';
            $html .= '<span>';
                $html .= $options['description'];
            $html .= '</span>';
            $html .= '<a href="' . esc_url( $options['link'] ) . '" target="_blank" />';
                $html .= '<input type="button" value="' . __( 'learn more', 'wp-super-dealer' ) . '" />';
            $html .= '</a>';
        $html .= '</li>';
    }
    $html .= '</ul>';    
    return $html;
}

function wpsd_pro_add_ons() {
    $addons = array();
    $addons['wpsd-data-services'] = array(
        'title' => __( 'Data Services', 'wp-super-dealer' ),
        'link' => 'https://wpsuperdealer.com/wpsuperdealer-data-services',
        'image' => 'add-on data services.jpg',
        'description' => __( '', 'wp-super-dealer' ),
    );
    $addons['wpsd-import-inventory'] = array(
        'title' => __( 'WPSD Import Inventory', '' ),
        'link' => 'https://wpsuperdealer.com/product/wpsd-import-inventory',
        'image' => 'add-on import inventory.jpg',
        'description' => __( '', 'wp-super-dealer' ),
    );
    $addons['wpsd-export-inventory'] = array(
        'title' => __( 'WPSD Export Inventory', '' ),
        'link' => 'https://wpsuperdealer.com/product/wpsd-export-inventory',
        'image' => 'add-on export inventory.jpg',
        'description' => __( '', 'wp-super-dealer' ),
    );
    $addons['wpsd-template-manager'] = array(
        'title' => __( 'Template Manager', 'wp-super-dealer' ),
        'link' => 'https://wpsuperdealer.com/product/wpsd-template-manager',
        'image' => 'add-on template manager.jpg',
        'description' => __( '', 'wp-super-dealer' ),
    );
    $addons['wpsd-compare-vehicles'] = array(
        'title' => __( 'Compare Vehicles', 'wp-super-dealer' ),
        'link' => 'https://wpsuperdealer.com/product/wpsd-compare-vehicles',
        'image' => 'add-on compare vehicles.jpg',
        'description' => __( '', 'wp-super-dealer' ),
    );    
    $addons['wpsd-embed-listings'] = array(
        'title' => __( 'Embed Inventory', 'wp-super-dealer' ),
        'link' => 'https://wpsuperdealer.com/product/wpsd-embed-listings',
        'image' => 'add-on embed inventory.jpg',
        'description' => __( '', 'wp-super-dealer' ),
    );
    $addons['wpsd-featured-vehicles'] = array(
        'title' => __( 'Featured Vehicles', 'wp-super-dealer' ),
        'link' => 'https://wpsuperdealer.com/product/wpsd-featured-vehicles',
        'image' => 'add-on featured vehicles.jpg',
        'description' => __( '', 'wp-super-dealer' ),
    );
    $addons['wpsd-keyword-search'] = array(
        'title' => __( 'Keyword Search', 'wp-super-dealer' ),
        'link' => 'https://wpsuperdealer.com/product/wpsd-keyword-search',
        'image' => 'add-on keyword search.jpg',
        'description' => __( '', 'wp-super-dealer' ),
    );
    $addons['wpsd-print-vehicles'] = array(
        'title' => __( 'Print Vehicles', 'wp-super-dealer' ),
        'link' => 'https://wpsuperdealer.com/product/wpsd-print-vehicles',
        'image' => 'add-on print vehicles.jpg',
        'description' => __( '', 'wp-super-dealer' ),
    );    

    return $addons;
}

function wpsd_go_pro_instructions() {
    $html = '';
	$html .= '<div class="wpsd-instructions-wrap wpsd-instructions-wrap-go-pro">';
		$html .= '<div class="wpsd-instructions-title" data-tab="go-pro">';
			$html .= '<span class="dashicons dashicons-welcome-learn-more"></span>';
			$html .=  __( 'Tips & Tricks - Going Pro', 'wp-super-dealer' );
		$html .= '</div>';

		$html .=  '<ul class="wpsd-instructions">';
            $html .= '<li>';
                $html .= '<div class="wpsd-tnt-wrap">';
                    $html .= '<div class="wpsd-tnt-col">';
						$html .= '<div class="wpsd-video-wrap">';
                            $html .= '<span class="wpsd-youtube" data-video-url="https://www.youtube.com/embed/OHYN8RGY4dU"></span>';
						$html .= '</div>';
                    $html .= '</div>';
                    $html .= '<div class="wpsd-tnt-col">';
                        $html .= '<h2>';
                            $html .=  __( 'Tips & Tricks for Going Pro', 'wp-super-dealer' );
                        $html .= '</h2>';
                        $html .= '<ol>';
                            $html .= '<li>';
                                $html .=  __( 'You make your own destiny.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .=  __( 'You don\'t need to do everything alone.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .=  __( 'Just because something doesn\'t exist, doesn\'t mean it can\'t be built.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .=  __( 'Listen to ', 'wp-super-dealer' );
                                $html .= '<a href="https://www.youtube.com/watch?v=Ff-0pHwyQ1g" target="_blank">';
                                    $html .=  __( 'powerful music', 'wp-super-dealer' );
                                $html .= '</a>';
                                $html .=  __( '.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .=  __( 'Take time for yourself and those you care about.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .=  __( 'Learn to value your worth.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .=  __( 'Stop reading thinly veiled filler posing as pseudo philosophical advice ;-)', 'wp-super-dealer' );
                            $html .= '</li>';
                        $html .= '</ol>';
                    $html .= '</div>';
                $html .= '</div>'; //= end $html .= '<div class="wpsd-tnt-wrap">';
                $html .= wpsd_instructions_footer();
            $html .= '</li>';
		$html .= '</ul>';
		$html .= '<div class="wpsd-instructions-hide" data-tab="locations">';
			$html .=  __( 'show', 'wp-super-dealer' );
		$html .= '</div>';
	$html .= '</div>';
    return $html;
}

?>