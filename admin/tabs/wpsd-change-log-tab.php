<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_change_log_tab() {
    $html = '';
	$html = apply_filters( 'wpsd_admin_change_log_filter', $html );
	if ( ! empty( $html ) ) {
		return $html;
	}
	$readme = wpsd_readme( 'Changelog' );

    $html .= '<h3>';
        $html .= __( 'Change Log', 'wp-super-dealer' );
    $html .= '</h3>';

	$html .= '
	<p class="wpsd_setup_text">
		<h3>WPSuperDealer Change log</h3>
	' . $readme . '
	</p>
	<p>
	'.__('If you\'ve recently updated and are having an issue please accept our apology and post a comment on our website so we can investigate the issue and issue patches as needed.', 'wp-super-dealer').'
	</p>
	<p>
	'.__('In closing we want to thank everyone for their continued support and look forward to many more updates!', 'wp-super-dealer').'
	</p>
	<div class="wpsd_welcome_logo">
		<a href="http://wpsuperdealer.com/" target="_blank">
			<img src="' . WPSD_PATH . '/images/wpsd-certified-support.png" />
		</a>
	</div>
	';
	return $html;	
}
?>