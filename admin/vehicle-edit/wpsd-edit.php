<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_restrict_listings_by_sold( $echo = true ) {
    global $typenow;
    global $wp_query;
	$html = '';

    if ( $typenow == WPSD_POST_TYPE ) {
		$html .= '<div class="wpsd_admin_filter">';
			$html .= '<div class="wpsd_admin_filter_label">';
				$html .= __( 'Sold', 'wp-super-dealer' );
			$html .= '</div>';
			$html .= '<div class="wpsd_admin_filter_field">';
				$html .= '<select name="sold" id="sold">';
					$html .= '<option value="">' . __( 'All', 'wp-super-dealer' ) . '</option>';
					$html .= '<option value="' . __( 'no', 'wp-super-dealer' ) . '">' . __( 'No', 'wp-super-dealer' ) . '</option>';
					$html .= '<option value="' . __( 'yes', 'wp-super-dealer' ) . '">' . __( 'Yes', 'wp-super-dealer' ) . '</option>';
				$html .= '</select>';
			$html .= '</div>';
		$html .= '</div>';

		$html .= '<div class="wpsd_admin_filter">';
			$html .= '<div class="wpsd_admin_filter_label">';
				$html .= __( 'Location', 'wp-super-dealer' );
			$html .= '</div>';
			$html .= '<div class="wpsd_admin_filter_field">';
				$args = array(
					'tax' => 'vehicle_location',
					'first_option' => __( 'All Locations', 'wp-super-dealer' ),
					'first_value' => '-1',
					'name' => 'vehicle_location',
					'id' => 'vehicle_location',
				);
				$html .= wpsd_tax_term_dropdown( $args );
			$html .= '</div>';
		$html .= '</div>';
		$html .= '<div class="wpsd_admin_filter">';
			$html .= '<div class="wpsd_admin_filter_label">';
				$html .= __( 'Condition', 'wp-super-dealer' );
			$html .= '</div>';
			$html .= '<div class="wpsd_admin_filter_field">';
				$args = array(
					'tax' => 'vehicle_condition',
					'first_option' => __( 'All Conditions', 'wp-super-dealer' ),
					'first_value' => '-1',
					'name' => 'vehicle_condition',
					'id' => 'vehicle_condition',
				);
				$html .= wpsd_tax_term_dropdown( $args );
			$html .= '</div>';
		$html .= '</div>';
	}
	if ( $echo ) {
		echo wpsd_kses_admin( $html );
	} else {
        //= return and escape it anyway
		return wpsd_kses_admin( $html );
	}
}

function wpsd_filter_by_sold( $query ) {
	global $pagenow;
	$both = 0;
	if ( isset( $_GET['post_type'] ) ) {
		if ( is_admin() && $pagenow == 'edit.php' && $_GET['post_type'] == WPSD_POST_TYPE )  {
			if ( isset( $_GET['sold'] ) ) {
				if ( $_GET['sold'] != '' ) {
					set_query_var( 'meta_query', array( array( 'key' => 'sold', 'value' => sanitize_text_field( $_GET['sold'] ) ) ) );
					$both = 1;
				}
			}
		}
	}
}

function wpsd_tax_term_dropdown( $args ) {
	$params = array(
		'hide_empty'		 => false,
		'taxonomy'           => $args['tax'],
		);
	$terms = get_categories( $params );

	$select = '<select name="' . $args['name'] . '" id="' . $args['id'] . '" class="postform">';
		$select .= '<option value="' . $args['first_value'] . '">' . $args['first_option'] . '</option>';
		foreach( $terms as $term ) {
			$select .= '<option value="' . esc_attr( $term->slug ) . '">' . esc_html( $term->name ) . '</option>';
		}
	$select .= '</select>';

	return $select;	
}
?>