<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_get_history_reports_form( $atts ) {
    $x = '';
    global $wpsd_options;
    
    $history_report = get_post_meta( $atts['post_id'], '_history_report', true );

    $x .= '<div class="wpsd-service-history-report">';
        //= Add our nonce field
        $nonce = wp_create_nonce( 'wpsd_get_history_report_nonce' );
        $x .= '<input type="hidden" name="wpsd_get_history_report_nonce" id="wpsd_get_history_report_nonce" value="' . $nonce . '" />';
    
        $decoded_class = '';
        if ( $history_report ) {
            $decoded_class = ' wpsd-history-report-result-label';
        }
        $x .= '<div class="wpsd-service-history-report-results-wrap' . esc_attr( $decoded_class ) . '">';
            $x .= '<textarea class="wpsd-service-history-report-results">';
                if ( $history_report ) {
                    $x .= json_encode( $history_report );
                }
            $x .= '</textarea>';
        $x .= '</div>';

        if ( isset( $wpsd_options['wpsd_api_validated'] )
           && is_object( $wpsd_options['wpsd_api_validated'] )
           && isset( $wpsd_options['wpsd_api_key'] )
           && ! empty( $wpsd_options['wpsd_api_key'] ) ) 
        {
            $services = get_option( 'wpsdapi-services', array() );
        } else {
            $services = array();
        }

        if ( isset( $services->history_reports ) 
            && is_object( $services->history_reports ) ) 
        {
            $x .= '<div class="wpsd-service-status-history-reports">';
                $x .= '<div class="wpsd-service-status-label">';
                    $x .= __( 'Most recent service status', 'wp-super-dealer' );
                $x .= '</div>';
                if ( isset( $services->history_reports->total ) ) {
                    $x .= '<div class="wpsd-history-reports-total-wrap">';
                        $x .= '<div class="wpsd-history-reports-total-label">';
                            $x .= __( 'History Reports Total', 'wp-super-dealer' );
                        $x .= '</div>';
                        $x .= '<div class="wpsd-history-reports-total">';
                            $x .= esc_html( $services->history_reports->total );
                        $x .= '</div>';
                    $x .= '</div>';
                }
                if ( isset( $services->history_reports->used ) ) {
                    $x .= '<div class="wpsd-history-reports-used-wrap">';
                        $x .= '<div class="wpsd-history-reports-used-label">';
                            $x .= __( 'History Reports Used', 'wp-super-dealer' );
                        $x .= '</div>';
                        $x .= '<div class="wpsd-history-reports-used">';
                            $x .= esc_html( $services->history_reports->used );
                        $x .= '</div>';
                    $x .= '</div>';
                }
                if ( isset( $services->history_reports->available ) ) {
                    $available_class = '';
                    //= TO DO Future: make constant WPSD_HISTORY_REFILL an option
                    if ( ! defined( 'WPSD_HISTORY_REFILL' ) ) {
                        define( 'WPSD_HISTORY_REFILL', 10 );
                    }
                    if ( WPSD_HISTORY_REFILL > $services->history_reports->available ) {
                        $available_class = ' wpsd-service-refill';
                    }
                    $x .= '<div class="wpsd-history-reports-available-wrap' . esc_attr( $available_class ) . '">';
                        $x .= '<div class="wpsd-history-reports-available-label">';
                            $x .= __( 'History Reports Available', 'wp-super-dealer' );
                        $x .= '</div>';
                        $x .= '<div class="wpsd-history-reports-available">';
                            $x .= esc_html( $services->history_reports->available );
                        $x .= '</div>';

                        $x .= '<div class="wpsd-refill-message">';
                            $x .= __( 'You\'re running low on History Reports. ', 'wp-super-dealer' );
                            $x .= '<a href="https://wpsuperdealer.com/product/wp-super-dealer-history-reports" target="_blank">';
                                $x .= __( 'Recharge here.', 'wp-super-dealer' );
                            $x .= '</a>';
                        $x .= '</div>';
                    $x .= '</div>';
                }
            $x .= '</div>';
        } else {
            $x .= '<div class="wpsd-service-status-history-reports">';
                $x .= __( 'You do not have an active History Reports Service. Please check your API Key and recharge or reactivate it as needed.', 'wp-super-dealer' );
            $x .= '</div>';
        }

        //= History Reports Form
        $x .= '<div class="wpsd-service-status-history-reports-form">';
            $x .= '<div class="wpsd-service-history-report-label">';
                if ( ! $history_report || empty( $history_report ) ) {
                    //= TO DO Future: remove
                    //$x .= __( 'Enter Vin # and click "Get Report".', 'wp-super-dealer' );
                } else {
                    $x .= '<span class="wpsd-history-report-result-label">';
                        $x .= __( 'NOTE: History report has already been pulled. ', 'wp-super-dealer' );
                    $x .= '</span>';
                    $x .= '<textarea class="wpsd-history-report-results">';
                        $history_report_array = (array)$history_report;
                        $history_report_json = json_encode( $history_report_array );
                        $x .= print_r( $history_report_json, true );
                    $x .= '</textarea>';
                    
                    $report = wpsd_history_report_template( $history_report );
                    $x .= $report;
                    
                }
            $x .= '</div>';
            $x .= '<div class="wpsd-service-history-report-button-wrap">';
                if ( $history_report && ! empty( $history_report ) ) {
                    $x .= '<span class="wpsd-data-api-button wpsd-view-history-report" data-post-id="' . esc_attr( $atts['post_id'] ) . '">' . __( 'View Report', 'wp-super-dealer' ) . '</span>';
                    $x .= '<span class="wpsd-data-api-button wpsd-remove-history-report" data-post-id="' . esc_attr( $atts['post_id'] ) . '">' . __( 'Remove Report', 'wp-super-dealer' ) . '</span>';
                }
                $x .= '<input type="button" data-type="history-report" name="wpsd-service-history-report-button" class="wpsd-data-api-button wpsd-service-history-report-button" value="' . __( 'Get History Report', 'wp-super-dealer' ) . '" data-post-id="' . esc_attr( $atts['post_id'] ) . '" />';
            $x .= '</div>';
            $x .= '<div class="wpsd-service-history-report-message">';
            $x .= '</div>';
        $x .= '</div>';
    $x .= '</div>';
    
    return $x;
}

add_action( 'wp_ajax_wpsd_get_history_report', 'wpsd_get_history_report' );
function wpsd_get_history_report() {
	if ( ! is_admin() ) {
		return;
	}
	if ( ! current_user_can( 'manage_options' ) ) {
		return;
	}
	$nonce = $_POST['nonce'];
	if ( ! wp_verify_nonce( $nonce, 'wpsd_get_history_report_nonce' ) ) {
		echo esc_html( __( 'Nonce check failed - no changes saved.', 'wp-super-dealer' ) );
		exit();
	}

    global $wpsd_options;
    $response = array();
    $response['result'] = false;
    $response['post_id'] = sanitize_text_field( $_POST['post_id'] );
    $response['vin'] = sanitize_text_field( $_POST['vin'] );

    //$services = get_option( 'wpsdapi-services', array() );
    
    if ( ! isset( $wpsd_options['wpsd_api_key'] )
       || empty( $wpsd_options['wpsd_api_key'] ) )
    {
        $response['message'] = __( 'No API Key was sent', 'wp-super-dealer' );
    } else {
        if ( isset( $wpsd_options['wpsd_api_validated'] )
           && is_object( $wpsd_options['wpsd_api_validated'] )
           && isset( $wpsd_options['wpsd_api_key'] )
           && ! empty( $wpsd_options['wpsd_api_key'] ) ) 
        {
            $response['wpsd_api_key'] = $wpsd_options['wpsd_api_key'];
            $response['message'] = __( 'Vin sent for history report', 'wp-super-dealer' );
            $results = wpsd_get_history_report_results( $response['wpsd_api_key'], $response['vin'] );
            update_post_meta( $response['post_id'], '_vin', $response['vin'] );
            
            if ( $results ) {
                $result = $results->result;
                //= Update our local service credits
                $response['services'] = $results->services;

                $report = array();
                if ( isset( $result->history_reports ) ) {
                    wpsd_update_service_credits( $response['services'] );
                    $base64 = $result->history_reports;
                    $base64 = base64_decode( $base64 );
                    $json = base64_decode( $base64 );
                    $report = json_decode( $json );
                }
            }

            $imported = wpsd_import_history_report( $response['post_id'], $report );
            if ( $imported ) {
                $response['result'] = $report;
                $response['edit'] = get_edit_post_link( $response['post_id'], false ) . '&data_api_update=1';;
            } else {
                $response['result'] = false;
            }
        } else {
            $response['message'] = __( 'No API Key was sent', 'wp-super-dealer' );
        }
    }
    $response['edit'] = get_edit_post_link( $response['post_id'], false ) . '&data_api_update=1';;
    $json = json_encode( $response );
    echo wpsd_kses_json( $json );
    
    exit();
}

function wpsd_import_history_report( $post_id, $value ) {
    $result = update_post_meta( $post_id, '_history_report', $value );
    return $result;
}

?>