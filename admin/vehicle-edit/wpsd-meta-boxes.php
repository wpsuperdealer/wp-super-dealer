<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action( 'admin_init', 'wpsd_start_meta_boxes' );
function wpsd_start_meta_boxes() {
	//= TO DO Future: Turn on Welcome Tab on vehicle edit page
//	add_meta_box( 'wpsd_edit_vehicle_welcome_metabox', __( 'Welcome', 'wp-super-dealer' ), 'wpsd_edit_vehicle_welcome_metabox', WPSD_POST_TYPE, 'normal', 'high' );
	add_meta_box( 'wpsd_main_metabox', __( 'Vehicle Details', 'wp-super-dealer' ), 'wpsd_edit_vehicle_details', WPSD_POST_TYPE, 'normal', 'high' );
	add_meta_box( 'wpsd_images_metabox', __( 'Vehicle Photos', 'wp-super-dealer' ), 'wpsd_images_metabox', WPSD_POST_TYPE, 'normal', 'high' );
	add_meta_box( 'wpsd_specifications_metabox', __( 'Vehicle Specifications', 'wp-super-dealer' ), 'vehicle_specifications_metabox', WPSD_POST_TYPE, 'normal', 'high' );
	add_meta_box( 'wpsd_options_metabox', __( 'Vehicle Options', 'wp-super-dealer' ), 'vehicle_options_metabox', WPSD_POST_TYPE, 'normal', 'high' );
	add_meta_box( 'wpsd_sold_metabox', __( 'Sales Status', 'wp-super-dealer' ), 'wpsd_sold_metabox', WPSD_POST_TYPE, 'side', 'high' );
	add_meta_box( 'wpsd_price_metabox', __( 'Vehicle Price', 'wp-super-dealer' ), 'wpsd_price_metabox', WPSD_POST_TYPE, 'side', 'high' );
}

function wpsd_edit_vehicle_welcome_metabox( $post ) {
	$post_id = $post->ID;
	$x = '';
	$x .= '<div class="wpsd-edit-vehicle-welcome-wrap">';
		$x .= '<label>';
			$x .= '<span class="dashicons dashicons-welcome-learn-more"></span>';
			$x .= __( 'Basic Instructions', 'wp-super-dealer' );
		$x .= '</label>';
		$x .= '<div class="wpsd-clear"></div>';
		$x .= '<div class="wpsd-edit-vehicle-welcome">';
			//= TO DO Future: add basic instructions on editing vehicles
			$x .= __( 'INSERT INSTRUCTIONS / VIDEO HERE', 'wp-super-dealer' );

		$x .= '</div>';
		$x .= '<div class="wpsd-clear"></div>';
		$x .= '<input type="button" class="wpsd-edit-vehicle-welcome-hide" value="' . __( 'Hide this panel', 'wp-super-dealer' ) . '" />';
		$x .= '<div class="wpsd-clear"></div>';
	$x .= '</div>';
    
	/*
    this function is not currently in use
    escape any data that comes from database and then echo as needed
    echo wpsd_kses_admin( $x );
    */
	return;
}

function wpsd_edit_vehicle_details( $post ) {
	global $wpsd_options;
	$post_id = $post->ID;
	$x = '';
	$maps = wpsd_get_type_maps();
	$vehicle_type = wpsd_get_vehicle_type( $post_id );

	$x .= wp_nonce_field( 'admin-edit-vehicle-nonce', 'admin-edit-vehicle-nonce', true, false );
	if ( 1 === count( $maps ) ) {
		$x .= '<input type="hidden" name="vehicle-type" id="vehicle-type" value="' . esc_attr( $vehicle_type ) . '" />';
	} else {
		$x .= '<div class="wpsd-edit-vehicle-type-wrap">';
			if ( is_array( $maps ) && 0 < count( $maps ) ) {
				if ( ! isset( $maps[ $vehicle_type ] ) ) {
					$vehicle_type = array_key_first( $maps );
				}

				$type_icon = $maps[ $vehicle_type ]['icon'];
				$x .= '<div class="wpsd-edit-select-type-title">';
					$x .= '<span class="dashicons dashicons-performance"></span>';
					$x .= __( 'Vehicle Type', 'wp-super-dealer' );
				$x .= '</div>';
				$x .= '<select name="vehicle-type" class="wpsd-edit-select-vehicle-type">';
					foreach( $maps as $map=>$map_data ) {
						$selected = ( $vehicle_type === $map ? ' selected' : '' );
						$x .= '<option'. $selected . ' value="' . esc_attr( $map ) . '" data-icon="' . esc_attr( $map_data['icon'] ) . '">' . esc_html( $map_data['label'] ) . '</option>';
					}
				$x .= '</select>';
				$x .= '<div class="wpsd-clear"></div>';
				$x .= '<img class="wpsd-edit-vehicle-type-icon" src="' . esc_url( $type_icon ) . '" />';
			}
		$x .= '</div>';
	}
	$x .= '<div class="wpsd-clear"></div>';

    $x .= '<div class="wpsd-edit-vehicle-data-api-wrap">';
        if ( isset( $wpsd_options['wpsd_api_key'] )
           && ! empty( $wpsd_options['wpsd_api_key'] ) ) 
        {
            $x .= '<div class="wpsd-edit-vehicle-data-api-vin-wrap">';
                $x .= '<div class="wpsd-service-status-vin-decodes-form">';
                    $x .= '<div class="wpsd-service-data-api-label-wrap">';
                        $x .= '<span class="wpsd-data-api-label">' . __( 'Data API', 'wp-super-dealer' ) . '</span>';
                    $x .= '</div>';
                    $x .= '<div class="wpsd-service-data-api-wrap">';
                        $vin = get_post_meta( $post_id, '_vin', true );
                        $x .= '<input type="text" name="wpsd-service-vin-value" class="wpsd-service-vin-value" placeholder="' . __( 'Enter VIN here', 'wp-super-dealer' ) . '" value="' . esc_attr( $vin ) . '" autocomplete="off">';
                        $button_text = '';
                        if ( isset( $_GET['data_api_update'] ) ) {
                            $button_text = __( 'Close Data API', 'wp-super-dealer' );
                        } else {
                            $button_text = __( 'Open Data API', 'wp-super-dealer' );
                        }
                        $x .= '<input type="button" name="wpsd-open-data-api" class="wpsd-open-data-api" value="' . esc_attr( $button_text ) . '">';
                    $x .= '</div>';
                    $x .= '<div class="wpsd-clear"></div>';
            
                    $x .= '<small>';
                        $x .= __( 'Enter a VIN and then click "Open Data API".', 'wp-super-dealer' );
                    $x .= '</small>';
                    $x .= '<div class="wpsd-clear"></div>';
                    $x .= '<div class="wpsd-service-data-api-message"></div>';
                $x .= '</div>';
            $x .= '</div>';
            $x .= '<div class="wpsd-clear"></div>';

            $api_class = '';
            if ( isset( $_GET['data_api_update'] ) ) {
                $api_class = ' wpsd-edit-vehicle-data-api-show';
            }
            $x .= '<div class="wpsd-edit-vehicle-data-api' . esc_attr( $api_class ) . '">';
                $api_fields = array(
                    'vin_decodes', 'history_reports', ' _value', 'ownership_cost',
                );
                $in_active = true;
                foreach( $api_fields as $api_field ) {
                    if ( isset( $wpsd_options['wpsd_api_' . $api_field ] )
                        && __( 'Yes', 'wp-super-dealer' ) === $wpsd_options['wpsd_api_' . $api_field ] ) 
                    {
                        $in_active = false;
                        $slug = str_replace( '_', '-', $api_field );
                        $x .= '<div class="wpsd-edit-vehicle-' . esc_attr( $slug ) . '-wrap">';
                            $x .= '<label>';
                                $label = str_replace( '_', ' ', $api_field );
                                $label = ucwords( $label );
                                //= TO DO future: localize labels
                                $x .= $label;
                            $x .= '</label>';

                            if ( function_exists( 'wpsd_get_' . $api_field . '_form' ) ) {
                                $x .= call_user_func( 'wpsd_get_' . $api_field . '_form', array( 'post_id' => $post_id ) );
                            }

                        $x .= '</div>';
                    }
                }
                $x .= '<div class="wpsd-clear"></div>';
                $x .= '<div class="wpsd-service-data-api-message"></div>';
                $x .= '<div class="wpsd-clear"></div>';
            $x .= '</div>';
            if ( $in_active ) {
                $x .= '<div class="wpsd_vpd_api_items_inactive">';
                    $x .= __( 'You have activated your API Key, but did not turn on any services - please visit the API Keys tab on the settings page to turn on your services.', 'wp-super-dealer' );
                $x .= '</div>';
                $x .= '<div class="wpsd-clear"></div>';
            }
        }
    $x .= '</div>';
	
	$x .= '<div class="wpsd-edit-vehicle-details-wrap">';
	
		$x .= '<div class="wpsd-edit-vehicle-description-wrap">';
			$x .= '<label>';
				$x .= __( 'Vehicle Description', 'wp-super-dealer' );
			$x .= '</label>';
			$x .= '<span>';
				$x .= '<textarea id="post_content" name="post_content" class="wpsd-edit-vehicle-description">';
					$x .= esc_html( $post->post_content );
				$x .= '</textarea>';
			$x .= '</span>';
		$x .= '</div>';
		$x .= '<div class="wpsd-clear"></div>';

		$x .= '<div class="wpsd-edit-vehicle-excerpt-wrap">';
			$x .= '<label>';
				$x .= __( 'Vehicle Excerpt', 'wp-super-dealer' );
			$x .= '</label>';
			$x .= '<span>';
				$x .= '<textarea id="post_excerpt" name="post_excerpt" class="wpsd-edit-vehicle-excerpt">';
					$x .= esc_html( $post->post_excerpt );
				$x .= '</textarea>';
			$x .= '</span>';
		$x .= '</div>';
		$x .= '<div class="wpsd-clear"></div>';
	$x .= '</div>';

    echo wpsd_kses_admin( $x );
	return;
}

function vehicle_specifications_metabox( $post ) {
	$x = get_vehicle_specifications_metabox( $post );
	echo wpsd_kses_admin( $x );
	return;
}

function get_vehicle_specifications_metabox( $post ) {
	$post_id = $post->ID;
	$x = '';
	$maps = wpsd_get_type_maps();

	$vehicle_type = wpsd_get_vehicle_type( $post_id );
	$vehicle_map = wpsd_get_vehicle_map( $post_id );
	$default_global_fields = wpsd_global_fields();

	$vehicle_options = wpsd_get_vehicle_options();
	if ( isset( $vehicle_options['global_specs'] ) 
		&& is_array( $vehicle_options['global_specs'] )
		&& ( 0 < count( $vehicle_options['global_specs'] ) )
		) {

		$global_fields = $vehicle_options['global_specs'];
	} else {
		$global_fields = array();
	}
	$global_fields = wp_parse_args( $global_fields, $default_global_fields );

	//= Is this type grouped?
	$no_group_specs = false;
	if ( isset( $vehicle_options['no_group_specs'] ) ) {
		if ( is_array( $vehicle_options ) 
			&& isset( $vehicle_options['no_group_specs'] ) 
			&& $vehicle_options['no_group_specs'] ) {

			$no_group_specs = true;
		}
	}

	$form_fields = new WP_SUPER_DEALER\WPSD_FIELDS;
	$time = time();

	$maps_json = json_encode( $maps );
	$maps_base64 = base64_encode( $maps_json );
	$x = '';
	$x .= '<div class="wpsd-edit-vehicle-specs-metabox-wrap" data-maps="' . esc_attr( $maps_base64 ) . '" data-post-id="' . esc_attr( $post_id ) . '">';
		if ( 1 < count( $maps ) ) {
			$x .= '<div class="wpsd-edit-vehicle-type-wrap">';
				if ( is_array( $maps ) && 0 < count( $maps ) ) {
					if ( ! isset( $maps[ $vehicle_type ] ) ) {
						$vehicle_type = array_key_first( $maps );
					}
					$type_icon = $maps[ $vehicle_type ]['icon'];
					$x .= '<div class="wpsd-edit-select-type-title">';
						$x .= '<span class="dashicons dashicons-performance"></span>';
						$x .= __( 'Vehicle Type', 'wp-super-dealer' );
					$x .= '</div>';
					$x .= '<select name="vehicle-type" class="wpsd-edit-select-vehicle-type">';
						foreach( $maps as $map=>$map_data ) {
							$selected = ( $vehicle_type === $map ? ' selected' : '' );
							$x .= '<option'. esc_attr( $selected ) . ' value="' . esc_attr( $map ) . '" data-icon="' . esc_attr( $map_data['icon'] ) . '">' . esc_html( $map_data['label'] ) . '</option>';
						}
					$x .= '</select>';
					$x .= '<div class="wpsd-clear"></div>';
					$x .= '<img class="wpsd-edit-vehicle-type-icon" src="' . esc_url( $type_icon ) . '" />';
				}
			$x .= '</div>';
		}
		$x .= '<div class="wpsd-edit-vehicle-specs-wrap">';
			$fields = array();
			$global_fields_args = array();

			foreach( $global_fields as $field=>$data ) {
				if ( isset( $data['label'] ) ) {
					if ( isset( $data['disabled'] ) ) {
						if ( 'true' === $data['disabled'] || true === $data['disabled'] ) {
							continue;
						}
					}
					$global_fields_args[ $field ] = $data;
				}
			}

			$global_specs_title = __( 'Global Specifications', 'wp-super-dealer' );
			$global_specs_title = apply_filters( 'global_specs_title_filter', $global_specs_title, $post_id );
			$global_fields_array = array(
				$global_specs_title => $global_fields_args,
			);
			if ( isset( $vehicle_map['specifications'] ) ) {
				$vehicle_map['specifications'] = array_merge( $global_fields_array, $vehicle_map['specifications'] );
			} else {
				$vehicle_map['specifications'] = $global_fields_array;
			}
			if ( isset( $vehicle_map['specifications' ] ) ) {
				if ( is_array( $vehicle_map['specifications' ] ) ) {
					foreach( $vehicle_map['specifications' ] as $spec_group=>$specs ) {						
						$x .= '<div class="wpsd-edit-vehicle-spec-group">';
							$x .= '<div class="wpsd-edit-vehicle-spec-group-title">';
								$x .= $spec_group;
							$x .= '</div>';
							if ( is_array( $specs ) ) {
								$fields = array();
								foreach( $specs as $spec=>$spec_settings ) {
									if ( isset( $spec_settings['hide_edit'] ) ) {
										if ( true === $spec_settings['hide_edit']
											|| 'true' === $spec_settings['hide_edit'] )
										{
											continue;
										}
									}
									$slug = sanitize_title( $spec );
									$_slug = str_replace( '-', '_', $slug );
									//=================================
									//= Get the current vehicle's value
									//=================================
									$spec_value = get_post_meta( $post_id, '_' . $_slug, true );

									$field_type = 'text';
									$field_options = array();
									if ( ! isset( $spec_settings['restrict'] ) ) {
										$spec_settings['restrict'] = false;
									}
									if ( isset( $spec_settings['taxonomy'] ) && $spec_settings['taxonomy'] ) {
										$field_type = 'maybe_select';
										if ( 'true' === $spec_settings['restrict'] || true === $spec_settings['restrict'] ) {
											$field_type = 'select';
										}
										$terms = get_terms( $_slug, array(
											'hide_empty' => false,
										) );
										if ( ! is_wp_error( $terms ) && 0 < count( $terms ) ) {
											//= if there is only one option then do not add an empty option
											if ( 1 < count( $terms ) ) {
												$field_options[] = array(
													'label' => '',
													'name' => '',
													'value' => '',
												);
											}
											
											foreach( $terms as $term=>$term_data ) {
												if ( ! is_object( $term_data ) ) {
													continue;
												}
												$field_options[] = array(
													'label' => $term_data->name,
													'name' => $term_data->name,
													'value' => $term_data->slug,
												);
											}
										}
									}
									if ( ! isset( $spec_settings['default-value'] ) ) {
										$spec_settings['default-value'] = '';
									}
									
									if ( empty( $spec_value ) ) {
										$spec_value = $spec_settings['default-value'];
									}
									if ( ! isset( $spec_settings['label'] ) 
										|| empty( $spec_settings['label'] ) ) 
									{
										$label = $spec;
									} else {
										$label = $spec_settings['label'];
									}
									$fields[] = array(
										'label' => $label,
										'slug' => $slug,
										'type' => $field_type,
										'value' => $spec_value,
										'class' => 'wpsd-edit-spec wpsd-edit-spec-' . $spec_group . '-' . $slug,
										'tip' => array(
										),
										'options' => $field_options,
										'placeholder' => $spec_settings['default-value'],
										'id_name' => 'spec[' . $slug . ']',
									);
								} //= end foreach( $specs as $spec=>$spec_settings ) {
								$terms = '';
								$x .= $form_fields->get_form_fields( $fields );
								$x .= '<div class="wpsd-clear"></div>';
							}
						$x .= '</div>';
					} //= end foreach( $vehicle_map['specifications' ] as $spec_group=>$specs ) {
				}
			}

		$x .= '</div>';	
	$x .= '</div>';

	return $x;
}

function vehicle_options_metabox( $post ) {
	$x = get_vehicle_options_metabox( $post );
	echo wpsd_kses_admin( $x );
	return;
}

function get_vehicle_options_metabox( $post ) {
	$post_id = $post->ID;
	$x = '';
	$maps = wpsd_get_type_maps();
	$vehicle_type = wpsd_get_vehicle_type( $post_id );
	if ( ! isset( $maps[ $vehicle_type ] ) ) {
		$vehicle_type = array_key_first( $maps );
	}
	$vehicle_map = $maps[ $vehicle_type ];
	$vehicle_options = wpsd_get_vehicle_options();
	
	//= Is this type grouped?
	$no_group_options = '';
	if ( isset( $vehicle_options['no_group_options'] ) ) {
		if ( is_array( $vehicle_options ) 
			&& isset( $vehicle_options['no_group_options'] ) 
			&& $vehicle_options['no_group_options'] ) {

			$no_group_options = ' wpsd-edit-no-group-options';
		}
	}

	//= Create  variable to store a CSS class if the toggles should be hidden
	$hide_toggles = '';

	//= Can users manually enter options
	$group_options_list = false;
	$group_options_list_css = '';
	if ( isset( $vehicle_options['group_options_list'][ $vehicle_type ] ) && ( $vehicle_options['group_options_list'][ $vehicle_type ] ) ) {
		$group_options_list = true;
		$group_options_list_css = ' active';
		$hide_toggles = ' in-active';
	}

    //= Should we make the option field to edit all options visable?
    $enable_edit_all_options = '';
    if ( $group_options_list && ! empty( $no_group_options ) ) {
        $enable_edit_all_options = ' wpsd-edit-vehicle-options-list-all-enabled';
    }
    
	$form_fields = new WP_SUPER_DEALER\WPSD_FIELDS;
	$time = time();

	$maps_json = json_encode( $maps );
	$maps_base64 = base64_encode( $maps_json );
	
	//= These are the options specific to the vehicle itself
	//= not to be confused with $vehicle_options - which are options for all vehicles
	//= I know, $vehicle_settings would have been better
	//= if you agree then do a pull request ;-)
	$options = get_post_meta( $post_id, '_options', true );
	if ( empty( $options ) ) {
		$options_array = array();
	}
	if ( false !== strpos( $options, ',' ) ){
		$options_array = explode( ',', $options );
	} else {
        //$options_array = array( 'Default', $options );
		$options_array = array( $options );
	}
	
	$x = '';
	$x .= '<div class="wpsd-edit-vehicle-options-metabox-wrap' . esc_attr( $no_group_options ) . '" data-post-id="' . esc_attr( $post_id ) . '">';
		$x .= '<div class="wpsd-edit-vehicle-options-wrap">';
			$fields = array();
			//= This is the field that updates the vehicle's options
			$x .= '<textarea name="wpsd-edit-vehicle-options-list-all" class="wpsd-edit-vehicle-options-list-all' . $enable_edit_all_options . '">';
				$x .= esc_html( $options );
			$x .= '</textarea>';

			if ( isset( $vehicle_map['options' ] ) ) {
				if ( is_array( $vehicle_map['options' ] ) ) {
					foreach( $vehicle_map['options' ] as $options_group=>$options_sub_groups ) {
						$x .= '<div class="wpsd-edit-vehicle-options-group' . $enable_edit_all_options . '">';
							$x .= '<div class="wpsd-edit-vehicle-options-group-title">';
								$x .= ucwords( $options_group );
							$x .= '</div>';
							if ( is_array( $options_sub_groups ) ) {
								foreach( $options_sub_groups as $options_sub_group=>$options ) {
									$sub_group_slug = sanitize_title( $options_sub_group );
									$x .= '<div class="wpsd-edit-vehicle-options-sub-group">';
										$x .= '<div class="wpsd-edit-vehicle-options-sub-group-title">';
											$x .= esc_html( $options_sub_group );
										$x .= '</div>';

										$x .= '<textarea class="wpsd-edit-vehicle-options-list' . esc_attr( $group_options_list_css ) . ' wpsd-edit-vehicle-options-list-' . esc_attr( $options_group . '-' . $sub_group_slug ) . '">';
											$x .= '';
										$x .= '</textarea>';

										if ( false !== strpos( $options, ',' ) ) {
											$option_items = explode( ',', $options );
											$field_args = array(
												'type' => $vehicle_type,
												'group' => $options_group,
												'sub_group' => $sub_group_slug,
											);

											foreach( $option_items as $option_item ) {
												$checked = false;
												if ( in_array( $option_item, $options_array ) ) {
													$checked = true;
												}
												$option_item = trim( $option_item );
												$field_args['value'] = $option_item;
												$json_args = json_encode( $field_args );
												$base64_args = base64_encode( $json_args );
												$x .= '<div class="wpsd-edit-vehicle-options-item-wrap' . esc_attr( $hide_toggles ) . '">';
													$args = array(
														'fieldname' => '[option-item][' . $options_group . '][' . $sub_group_slug . '][' . sanitize_title( $option_item ) . ']',
														'label' => $option_item,
														'value' => '',
														'base64' => $base64_args,
														'type' => $option_item,
														'section' => 'option',
														'action' => 'wpsd-edit-options-toggle',
														'class' => 'wpsd-edit-vehicle-options-item wpsd-edit-vehicle-options-item-' . $options_group . '-' . $sub_group_slug . ' ' . $options_group . '-' . $sub_group_slug . '-' . sanitize_title( $option_item ),
														'checked' => $checked,
													);
													$x .= wpsd_get_toggle( $args );
												$x .= '</div>';
											}
										} else {
											$x .= '<div class="wpsd-edit-vehicle-options-item-wrap">';
											$x .= '</div>';
										}
									$x .= '</div>';
								} //= end foreach( $specs as $spec=>$spec_settings ) {
								$x .= '<div class="wpsd-clear"></div>';
							}
						$x .= '</div>';
					} //= foreach( $vehicle_map['options' ] as $options_group=>$options_sub_groups ) {
				}
			}
		$x .= '</div>';	
	$x .= '</div>';
	$x .= '<div class="wpsd-clear"></div>';
	return $x;
}

function wpsd_sold_metabox( $post ) {
	$post_id = $post->ID;
	$status = get_post_meta( $post_id, 'sold', true );
	if ( $status == __( 'Yes', 'wp-super-dealer') ) {
		$yes = ' selected';
		$no = '';
	} else {
		$no = ' selected';
		$yes = '';
	}
	$x = '';
	$x .= '<div class="wpsd-is-sold-wrap">';
		$x .= '<label>';
			$x .= '<span class="dashicons dashicons-tag"></span>';
			$x .= __( 'Sold', 'wp-super-dealer' );
		$x .= '</label>';
		$x .= '<select name="sold" id="sold">';
			$x .= '<option value="' . __( 'No', 'wp-super-dealer' ) . '"' . $no . '>' . __( 'No', 'wp-super-dealer' ) . '</option>';
			$x .= '<option value="' . __( 'Yes', 'wp-super-dealer' ) . '"' . $yes . '>' . __( 'Yes', 'wp-super-dealer' ) . '</option>';
		$x .= '</select>';
	$x .= '</div>';
	echo wpsd_kses_admin( $x );
	return;
}

function wpsd_price_metabox( $post ) {
	global $wpsd_options;
	$wpsd_vehicle_options = wpsd_get_vehicle_options();
	$post_id = $post->ID;
	$x = '';
	$price_fields = get_price_fields();
	$global_specs = $wpsd_vehicle_options['global_specs'];
	$active_fields = array();
	
	//= Get the before currency value
	$price_before = '';
	if ( isset( $wpsd_options['price_before'] ) ) {
		$price_before = $wpsd_options['price_before'];
	}
	//= Get the after currency value
	$price_after = '';
	if ( isset( $wpsd_options['price_after'] ) ) {
		$price_after = $wpsd_options['price_after'];
	}

	foreach( $price_fields as $price_field=>$data ) {
		if ( isset( $global_specs[ $price_field ] ) ) {
			if ( isset( $global_specs[ $price_field ]['disabled'] ) ) {
				$disabled = $global_specs[ $price_field ]['disabled'];
			} else {
				$disabled = false;
			}
			if ( 'true' === $disabled || true === $disabled ) {
				continue;
			}
			$active_fields[ $price_field ] = $global_specs[ $price_field ];
		}
	}

	$x .= '<div class="wpsd-edit-vehicle-prices-wrap">';
		foreach( $active_fields as $slug=>$data ) {
			$_slug = str_replace( '-', '_', $slug );
			$price = get_post_meta( $post_id, '_' . $_slug, true );
            $price = esc_html( $price );
			$x .= '<div class="wpsd-edit-vehicle-price-item wpsd-edit-vehicle-price-item-' . $slug . '">';
				$x .= '<label>';
					$x .= '<i class="dashicons dashicons-money-alt"></i>';
					if ( isset( $data['label'] ) ) {
						$x .= $data['label'];
					} else {
						$x .= ucwords( $slug );
					}
				$x .= '</label>';
				$x .= '<span>';
					if ( empty( $price ) || '0' === $price || 0 === $price || '0.00' === $price ) {
						if ( isset( $data['default-value'] ) ) {
							$price = $data['default-value'];
						}
					}
					$x .= '<div class="wpsd-edit-vehicle-before-price">';
						$x .= wp_kses_post( $price_before );
					$x .= '</div>';
					$x .= '<input type="number" name="spec[' . $slug . ']" id="price_field_' . $_slug . '" placeholder="" min="0" max="1000000" step="1" value="' . $price . '" />';
					$x .= '<div class="wpsd-edit-vehicle-after-price">';
						$x .= wp_kses_post( $price_after );
					$x .= '</div>';
				$x .= '</span>';
			$x .= '</div>';
		}
	$x .= '</div>';
	$x .= '<div class="wpsd-clear"></div>';

	echo wpsd_kses_admin( $x );
	return;
}

add_filter( 'comments_open', 'wpsd_prefix_comments_open', 10 , 2 );
function wpsd_prefix_comments_open( $open, $post_id ) {
	$post_type = get_post_type( $post_id );
	if ( $post_type == WPSD_POST_TYPE ) {
		$no_vehicle_comments = false;
		$no_vehicle_comments = apply_filters( 'no_vehicle_comments_filter', $no_vehicle_comments );
		return $no_vehicle_comments;
	}
	return $open;
}
?>