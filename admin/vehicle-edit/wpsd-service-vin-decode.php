<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_get_vin_decodes_form( $atts ) {
    $x = '';
    global $wpsd_options;
    
    $vin_decode = get_post_meta( $atts['post_id'], '_vin_decode', true );

    $x .= '<div class="wpsd-service-vin-decode">';
        //= Add our nonce field
        $nonce = wp_create_nonce( 'wpsd_get_vin_decode_nonce' );
        $x .= '<input type="hidden" name="wpsd_get_vin_decode_nonce" id="wpsd_get_vin_decode_nonce" value="' . $nonce . '" />';
    
        $decoded_class = '';
        if ( $vin_decode ) {
            $decoded_class = ' wpsd-vin-decode-result-label';
        }
        $x .= '<div class="wpsd-service-vin-decode-results-wrap' . esc_attr( $decoded_class ) . '">';
            $x .= '<textarea class="wpsd-service-vin-decode-results">';
                if ( $vin_decode ) {
                    $x .= json_encode( $vin_decode );
                }
            $x .= '</textarea>';
        $x .= '</div>';

        if ( isset( $wpsd_options['wpsd_api_validated'] )
           && is_object( $wpsd_options['wpsd_api_validated'] )
           && isset( $wpsd_options['wpsd_api_key'] )
           && ! empty( $wpsd_options['wpsd_api_key'] ) ) 
        {
            $services = get_option( 'wpsdapi-services', array() );
        } else {
            $services = array();
        }

        if ( isset( $services->vin_decodes )
            && is_object( $services->vin_decodes ) ) 
        {
            $x .= '<div class="wpsd-service-status-vin-decodes">';
                $x .= '<div class="wpsd-service-status-label">';
                    $x .= __( 'Most recent service status', 'wp-super-dealer' );
                $x .= '</div>';
                if ( isset( $services->vin_decodes->total ) ) {
                    $x .= '<div class="wpsd-vin-decodes-total-wrap">';
                        $x .= '<div class="wpsd-vin-decodes-total-label">';
                            $x .= __( 'VIN Decodes Total', 'wp-super-dealer' );
                        $x .= '</div>';
                        $x .= '<div class="wpsd-vin-decodes-total">';
                            $x .= esc_html( $services->vin_decodes->total );
                        $x .= '</div>';
                    $x .= '</div>';
                }
                if ( isset( $services->vin_decodes->used ) ) {
                    $x .= '<div class="wpsd-vin-decodes-used-wrap">';
                        $x .= '<div class="wpsd-vin-decodes-used-label">';
                            $x .= __( 'VIN Decodes Used', 'wp-super-dealer' );
                        $x .= '</div>';
                        $x .= '<div class="wpsd-vin-decodes-used">';
                            $x .= esc_html( $services->vin_decodes->used );
                        $x .= '</div>';
                    $x .= '</div>';
                }
                if ( isset( $services->vin_decodes->available ) ) {
                    $available_class = '';
                    //= TO DO Future: make constant WPSD_DECODE_REFILL an option
                    if ( ! defined( 'WPSD_DECODE_REFILL' ) ) {
                        define( 'WPSD_DECODE_REFILL', 10 );
                    }
                    if ( WPSD_DECODE_REFILL > $services->vin_decodes->available ) {
                        $available_class = ' wpsd-service-refill';
                    }
                    $x .= '<div class="wpsd-vin-decodes-available-wrap' . esc_attr( $available_class ) . '">';
                        $x .= '<div class="wpsd-vin-decodes-available-label">';
                            $x .= __( 'VIN Decodes Available', 'wp-super-dealer' );
                        $x .= '</div>';
                        $x .= '<div class="wpsd-vin-decodes-available">';
                            $x .= esc_html( $services->vin_decodes->available );
                        $x .= '</div>';

                        $x .= '<div class="wpsd-refill-message">';
                            $x .= __( 'You\'re running low on VIN Decodes. ', 'wp-super-dealer' );
                            $x .= '<a href="https://wpsuperdealer.com/product/wp-super-dealer-vin-decodes" target="_blank">';
                                $x .= __( 'Recharge here.', 'wp-super-dealer' );
                            $x .= '</a>';
                        $x .= '</div>';
                    $x .= '</div>';
                }
            $x .= '</div>';
        } else {
            $x .= '<div class="wpsd-service-status-vin-decodes">';
                $x .= __( 'You do not have an active VIN Decode Service. Please check your API Key and recharge or reactivate it as needed.', 'wp-super-dealer' );
            $x .= '</div>';
        }
    
        //= Decode VIN Form
        $x .= '<div class="wpsd-service-status-vin-decodes-form">';
            $x .= '<div class="wpsd-service-vin-decode-label">';
                if ( ! $vin_decode || empty( $vin_decode ) ) {
                    $x .= __( 'Enter Vin # and click "Decode Now".', 'wp-super-dealer' );
                } else {
                    $x .= '<span class="wpsd-vin-decode-result-label">';
                        $x .= __( 'NOTE: This vehicle has already been decoded.', 'wp-super-dealer' );
                    $x .= '</span>';
                    $x .= '<textarea class="wpsd-vin-decode-results">';
                        $vin_decode_array = (array)$vin_decode;
                        $vin_decode_json = json_encode( $vin_decode_array );
                        $x .= print_r( $vin_decode_json, true );
                    $x .= '</textarea>';
                }
            $x .= '</div>';
            $x .= '<div class="wpsd-service-vin-decode-button-wrap">';
                $x .= '<input type="button" data-type="vin-decode" name="wpsd-service-vin-decode-button" class="wpsd-data-api-button wpsd-service-vin-decode-button" value="' . __( 'Decode Now', 'wp-super-dealer' ) . '" data-post-id="' . esc_attr( $atts['post_id'] ) . '" />';
            $x .= '</div>';
            $x .= '<div class="wpsd-service-vin-decode-message">';
            $x .= '</div>';
        $x .= '</div>';
    $x .= '</div>';
    
    return $x;
}

add_action( 'wp_ajax_wpsd_get_vin_decode', 'wpsd_get_vin_decode' );
function wpsd_get_vin_decode() {
	if ( ! is_admin() ) {
		return;
	}
	if ( ! current_user_can( 'manage_options' ) ) {
		return;
	}
	$nonce = $_POST['nonce'];
	if ( ! wp_verify_nonce( $nonce, 'wpsd_get_vin_decode_nonce' ) ) {
		echo esc_html( __( 'Nonce check failed - no changes saved.', 'wp-super-dealer' ) );
		exit();
	}

    global $wpsd_options;
    $response = array();
    $response['result'] = false;
    $response['post_id'] = sanitize_text_field( $_POST['post_id'] );
    $response['vin'] = sanitize_text_field( $_POST['vin'] );

    //$services = get_option( 'wpsdapi-services', array() );
    
    if ( ! isset( $wpsd_options['wpsd_api_key'] )
       || empty( $wpsd_options['wpsd_api_key'] ) )
    {
        $response['message'] = __( 'No API Key was sent', 'wp-super-dealer' );
    } else {
        if ( isset( $wpsd_options['wpsd_api_validated'] )
           && is_object( $wpsd_options['wpsd_api_validated'] )
           && isset( $wpsd_options['wpsd_api_key'] )
           && ! empty( $wpsd_options['wpsd_api_key'] ) ) 
        {
            $response['wpsd_api_key'] = $wpsd_options['wpsd_api_key'];
            $response['message'] = __( 'Vin sent for decoding', 'wp-super-dealer' );
            $results = wpsd_get_vin_decode_results( $response['wpsd_api_key'], $response['vin'] );
            $response['service'] = $results->service;
            if ( $results ) {
                $result = $results->result;
                //= Update our local service credits
                $response['services'] = $results->services;
                $decode = array();
                if ( isset( $result->vin_decodes ) ) {
                    wpsd_update_service_credits( $response['services'] );
                    $base64 = $result->vin_decodes;
                    $json = base64_decode( $base64 );
                    $decode = json_decode( $json );
                    if ( ! is_object( $decode ) ) {
                        $decode = new stdClass();
                    }
                    $decode->vin = $response['vin'];
                }
            }

            $imported = wpsd_import_vin_decode( $response['post_id'], $decode );
            update_post_meta( $response['post_id'], '_vin_decode', $decode );
            if ( $imported ) {
                $response['result'] = $decode;
                $response['edit'] = get_edit_post_link( $response['post_id'], false ) . '&data_api_update=1';;
            } else {
                $response['result'] = false;
            }
        } else {
            $response['message'] = __( 'No API Key was sent', 'wp-super-dealer' );
        }
    }
    $response['edit'] = get_edit_post_link( $response['post_id'], false ) . '&data_api_update=1';;
    $json = json_encode( $response );
    echo wpsd_kses_json( $json );
    
    exit();
}

function wpsd_import_vin_decode( $post_id, $decode ) {
    $result = false;
    //= Make sure this post has been saved
    
    $post = get_post( $post_id );

    if ( ! isset( $post->post_title ) || empty( $post->post_title ) || __( 'Auto Draft', 'wp-super-dealer' ) === $post->post_title ) {
        $post_title = $decode->model_year . ' ' . $decode->make . ' ' . $decode->model . ' - ' . $post_id;
    } else {
        $post_title = $post->post_title;
    }
    $time = time();
    $arg = array(
        'ID'            => $post_id,
        'post_date'     => date( 'Y-m-d H:i:s', $time ),
        'post_date_gmt' => gmdate( 'Y-m-d H:i:s', $time ),
        'edit_date'     => true,
        'post_status'   => 'draft',
        'post_title'    => $post_title,
    );
    $updated_post = wp_update_post( $arg );
    
    if ( ! is_wp_error( $updated_post ) ) {
        //= TO DO: check specs map, if tax add there too
        if ( is_object( $decode ) ) {
            foreach( $decode as $spec=>$value ) {
                $slug = sanitize_title( $spec );
                $_slug = str_replace( '-', '_', $slug );
                if ( 'options' === $slug ) {
                    if ( is_array( $value ) && 0 < count( $value ) ) {
                        $array = (array)$value;
                        $value = implode( ',', $array );
                    }
                }
                if ( 'model_year' === $_slug ) {
                    $slug = 'vehicle-year';
                    $_slug = 'vehicle_year';
                }
                if ( false !== strpos( $_slug, 'price' ) 
                    || false !== strpos( $_slug, 'msrp' )
                    || false !== strpos( $_slug, 'discount' ) )
                {
                    $number = preg_replace( "/[^0-9]/", "", $value );
                    $value = $number;
                }
                update_post_meta( $post_id, '_' . $_slug, $value );
            }
            $result = true;
        }
    }
    return $result;
}

/**
* Validate a given VIN
*
* @param string $vin
* @return bool
*/
function wpsd_validate_vin( $vin ) {

    $vin = strtolower( $vin );
    if ( ! preg_match( '/^[^\Wioq]{17}$/', $vin ) ) { 
        return false; 
    }

    $weights = array(8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2);

    $transliterations = array(
        "a" => 1, "b" => 2, "c" => 3, "d" => 4,
        "e" => 5, "f" => 6, "g" => 7, "h" => 8,
        "j" => 1, "k" => 2, "l" => 3, "m" => 4,
        "n" => 5, "p" => 7, "r" => 9, "s" => 2,
        "t" => 3, "u" => 4, "v" => 5, "w" => 6,
        "x" => 7, "y" => 8, "z" => 9
    );

    $sum = 0;

    for( $i = 0 ; $i < strlen( $vin ) ; $i++ ) { // loop through characters of VIN
        // add transliterations * weight of their positions to get the sum
        if( ! is_numeric( $vin[ $i ] ) ) {
            $sum += $transliterations[ $vin[ $i ] ] * $weights[ $i ];
        } else {
            $sum += $vin[ $i ] * $weights[ $i ];
        }
    }

    // find checkdigit by taking the mod of the sum

    $checkdigit = $sum % 11;

    if( $checkdigit == 10 ) { // checkdigit of 10 is represented by "X"
        $checkdigit = "x";
    }

    return ( $checkdigit == $vin[ 8 ] );
}

function wpsd_update_service_credits( $services ) {
    update_option( 'wpsdapi-services', $services, false );
    return true;
}
?>