<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_images_metabox( $post ) {
	// Show currently attached photos
	$post_id = $post->ID;
	$popup_imgs = '';
    $html = '';
	//= Are main images set to load from a third party link?
	if ( defined( 'WPSD_LINK_MAIN_IMAGE' ) ) {
		if ( WPSD_LINK_MAIN_IMAGE ) {
			$html .= '<div class="wpsd_link_main_image_true">';
				$html .= '<h3>';
					$html .= __( 'The constant WPSD_LINK_MAIN_IMAGE has been defined as true.', 'wp-super-dealer' );
				$html .= '</h3>';
				$html .= '<h3>';
					$html .= __( 'This means WPSuperDealer will ignore the "Featured Images" and instead will use the first linked image as the main photo for each vehicle.', 'wp-super-dealer' );
				$html .= '</h3>';
			$html .= '</div>';
		}
	}

	$html .= '<input type="button" value="' . __( 'Upload Photos', 'wp-super-dealer' ) . '" class="wp-core-ui button-primary custom_media_upload manage_vehicle_photos" data-post-id="' . $post_id . '" /></a>';
	$html .= '<div class="wpsd_reverse_attachments_btn button-primary">' . __( 'Reverse Image Order', 'wp-super-dealer' ) . '</div>';
	$html .= '<div class="wpsd-clear"></div>';
	$html .= '
	<img class="custom_media_image" src="" />
		<input class="custom_media_url" type="hidden" name="attachment_url" value="">
		<input class="custom_media_id" type="hidden" name="attachment_id" id="attachment_id" value="">
		<input type="hidden" name="attachment_post_id" id="attachment_post_id" value="' . esc_attr( $post_id ) . '">
	';

	//= Handle linked images - these come from 3rd party domains
	$image_list = get_post_meta( $post_id, '_image_urls', true );
    
    if ( '###' === $image_list ) {
        $image_list = WPSD_PATH . 'images/coming-soon.jpg';
    }

	if ( empty( $image_list ) && defined( 'WPSD_USE_IMAGE_LINKS' ) && WPSD_USE_IMAGE_LINKS ) {
		$image_list = WPSD_PATH . 'images/coming-soon.jpg';
	}
	$this_car = '';
	$cnt = 1;
	if ( ! empty( $image_list ) ) {
		$html .= '<h3>' . __( 'Linked Photos', 'wp-super-dealer' ) . '</h3>';
		$html .= '<small>';
			$html .= __( 'This feature allows you to use URLs from 3rd party sites as part of your Vehicle Gallery.', 'wp-super-dealer' );
		$html .= '</small>';
		$html .= '<div class="wpsd-clear"></div>';
		$html .= '<small class="wpsd_edit_image_links" data-status="closed">';
			$html .= __( 'Click here to edit the list of links', 'wp-super-dealer' );
		$html .= '</small>';
		$html .= '<div class="wpsd-clear"></div>';
		$html .= '<div class="wpsd_image_links_list_wrap">';
			$html .= '<p class="">';
				$html .= __( 'You can manually update the image links in the textarea below. Place a comma between each image URL with no spaces. Then click the "Update Image Links" button.', 'wp-super-dealer' );
			$html .= '</p>';
			$html .= '<div class="wpsd-clear"></div>';
			$html .= '<textarea class="wpsd_image_links_list">';
				$html .= esc_html( $image_list );
			$html .= '</textarea>';
			$html .= '<div class="wpsd-clear"></div>';

			//= Add our nonce field
			$nonce = wp_create_nonce( 'wpsd_update_image_links_nonce' );
			$html .= '<input type="hidden" name="wpsd_update_image_links_nonce" id="wpsd_update_image_links_nonce" value="' . $nonce . '" />';
			$html .= '<div class="wpsd_update_image_links_btn button-primary">' . __( 'Update Image Links', 'wp-super-dealer' ) . '</div>';
			$html .= '<div class="wpsd_close_image_links button-primary">' . __( 'Cancel', 'wp-super-dealer' ) . '</div>';
		$html .= '</div>';
		$html .= '<div class="wpsd-clear"></div>';
		$thumbnails = explode( ",", $image_list );
		$thumbnails = apply_filters( 'wpsd_get_thumbnails', $thumbnails, $post_id, 'linked' );
		foreach( $thumbnails as $thumbnail ) {
			$pos = true;
			if( $pos == true ) {
                $thumbnail = esc_url( $thumbnail );
				$this_car .= wpsd_image_box( $post_id, $cnt, $thumbnail, $thumbnail, 'linked' );
				$cnt = $cnt + 1;
			}
		}
	}
	$this_car = '<div id="vehicle_photo_links" data-post-id="' . $post_id . '">' . $this_car . '</div>';
	$html .= $this_car;
	$html .= '<div class="wpsd-clear"></div>';
	$this_car = '';

	//= Handle the main photo and all attached images
	$thumbnails = get_children( array('post_parent' => $post_id, 'post_type' => 'attachment', 'post_mime_type' =>'image', 'orderby' => 'menu_order') );
	$thumbnails = apply_filters( 'wpsd_get_thumbnails', $thumbnails, $post_id, 'media' );
	$html .= '<h3>'. __( 'Attached Photos', 'wp-super-dealer' ) .'</h3><br />';
	if ( ! defined( 'WPSD_LINK_MAIN_IMAGE' ) || false === WPSD_LINK_MAIN_IMAGE ) {
		$html .= '<div class="wpsd_main_image_wrapper">';
			$html .= '<h4>';
				$html .= __( 'Main Vehicle Photo', 'wp-super-dealer' );
			$html .= '</h4>';
			$main_image = wpsd_main_photo( $post_id );
			if ( empty( $main_image ) ) {
				$main_image = WPSD_PATH . 'images/coming-soon.jpg';
				if ( defined( 'WPSD_CUSTOM_NO_PHOTO' ) ) {
					$main_image = WPSD_CUSTOM_NO_PHOTO;
				}
			}
			$html .= '<div class="wpsd_main_image" data-post-id="' . esc_attr( $post_id ) . '">';
				$html .= '<img id="set-post-thumbnail-btn" src="' . esc_url( $main_image ) . '" />';
				$html .= '<div class="howto" id="set-post-thumbnail-desc-2">' . __( 'Click the image to edit or update', 'wp-super-dealer' ) . '</div>';
				$html .= '<div id="remove-post-thumbnail-btn">' . __( 'Remove main vehicle photo', 'wp-super-dealer' ) . '</div>';
			$html .= '</div>';
		$html .= '</div>';
	}
	$cnt = 0;
	foreach( $thumbnails as $thumbnail ) {
		$guid = wp_get_attachment_url( $thumbnail->ID );
		if ( ! empty( $guid ) ) {
			$this_car .= wpsd_image_box( $thumbnail->ID, $cnt, $guid, $thumbnail->ID, 'attached' );
			++$cnt;
		}
	}
	$this_car = '<div id="vehicle_photo_attachments">' . $this_car . '</div>';
	$html .= $this_car;
	$html .= '<div class="wpsd-clear"></div>';
	$html .= '<input type="button" value="' . __( 'Upload Photos', 'wp-super-dealer' ) . '" class="wp-core-ui button-primary custom_media_upload manage_vehicle_photos" data-post-id="' . $post_id . '" /></a>';
	$html .= '<div class="wpsd_reverse_attachments_btn button-primary">' . __( 'Reverse Image Order', 'wp-super-dealer' ) . '</div>';
	$html .= '<div class="wpsd-clear"></div>';

    echo wpsd_kses_admin( $html );

    return;
}

function wpsd_image_box( $post_id, $cnt, $thumbnail, $src_id, $type = 'linked' ) {
	$html = '<div id="vehicle_photo_' . esc_attr( $cnt . '_' . $type ) . '" data-type="' . esc_attr( $type ) . '" name="vehicle_photo_' . esc_attr( $cnt ) . '" class="admin_box_vehicle_photo admin_box_vehicle_photo_' . esc_attr( $type ) . '" data-post-id="' . esc_attr( $post_id ) . '" data-src="' . esc_attr( trim( $src_id ) ) . '">';
		$html .= '<div class="vehicle_photo_remove" data-type="' . esc_attr( $type ) . '" data-car-link="' . esc_url( $thumbnail ) . '" data-src-id="' . esc_attr( $src_id ) . '" data-cnt="' . esc_attr( $cnt ) . '">';
			$html .= 'X';
		$html .= '</div>';
		$html .= '<div align="center">';
			$html .= '<img class="wpsd_thumbs" style="cursor:pointer" src="' . esc_url( trim( $thumbnail ) ) . '" width="162" />';
		$html .= '</div>';
	$html .= '</div>';
	return $html;
}
?>