<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//add_filter( 'map_meta_cap', 'wpsd_map_meta_cap', 10, 4 );
function wpsd_map_meta_cap( $caps, $cap, $user_id, $args ) {
	/* If editing, deleting, or reading a vehicle, get the post and post type object. */
	if ( 'edit_vehicle' == $cap || 'delete_vehicle' == $cap || 'read_vehicle' == $cap ) {
		$post = get_post( $args[0] );
		$post_type = get_post_type_object( $post->post_type );

		/* Set an empty array for the caps. */
		$caps = array();
	}

	/* If editing a vehicle, assign the required capability. */
	if ( 'edit_vehicle' == $cap ) {
		if ( $user_id == $post->post_author )
			$caps[] = $post_type->cap->edit_posts;
		else
			$caps[] = $post_type->cap->edit_others_posts;
	}

	/* If deleting a vehicle, assign the required capability. */
	elseif ( 'delete_vehicle' == $cap ) {
		if ( $user_id == $post->post_author )
			$caps[] = $post_type->cap->delete_posts;
		else
			$caps[] = $post_type->cap->delete_others_posts;
	}

	/* If reading a private vehicle, assign the required capability. */
	elseif ( 'read_vehicle' == $cap ) {

		if ( 'private' != $post->post_status )
			$caps[] = 'read';
		elseif ( $user_id == $post->post_author )
			$caps[] = 'read';
		else
			$caps[] = $post_type->cap->read_private_posts;
	}

	/* Return the capabilities required by the user. */
	return $caps;
}

//= TO DO Future: Create an admin option to reset roles to defaults
//add_action( 'admin_init', 'wpsd_add_caps');
function wpsd_add_caps() {
	$role_names = array(
		'administrator',
		'editor',
		'author',
	);
	$role_names = apply_filters( 'wpsd_role_names_filter', $role_names );
	foreach( $role_names as $role_name ) {
		$role = get_role( $role_name );
		$caps = array(
			'read',
			'read_vehicle',
			'edit_vehicle',
			'delete_vehicle',
			'read_private_vehicles',
			'edit_vehicles',
			'edit_others_vehicles',
			'edit_published_vehicles',
			'create_vehicles',
			'publish_vehicles',
			'delete_vehicles',
			'delete_published_vehicles',
			'delete_others_vehicles',
		);
		$caps = apply_filters( 'wpsd_caps_filter', $caps, $role_name );
		foreach( $caps as $cap ) {
			$role->add_cap( $cap );
		}
	}
}
?>