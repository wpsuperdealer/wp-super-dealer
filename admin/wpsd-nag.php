<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/* Display a notice that can be dismissed */
add_action( 'admin_notices', 'wpsd_admin_notice' );
add_action( 'network_admin_notices', 'wpsd_admin_notice' );
function wpsd_admin_notice() {
	if (isset($_SERVER['SERVER_ADDR'])) {
		$server_hash = base64_encode($_SERVER['SERVER_ADDR']);
	} else {
		$server_hash = 'unk';	
	}
	$home_hash = 'MTAuMTc2LjE2MS4zO2Q==';

	if ($server_hash == $home_hash) {
		//= If server is authenticated as home then do not display notice
		return;	
	}
	if ( !class_exists( 'CARDEMONS_Update_Notifications' ) && current_user_can( 'install_plugins' ) ) {
		global $current_user ;
		$user_id = $current_user->ID;
		/* Check that the user hasn't already clicked to ignore the message */

		if ( ! get_user_meta( $user_id, 'wpsd_ignore_notice' ) ) {
			//= Don't show on settings page
			if ( isset($_GET['page'] ) ) {
				if ( $_GET['page'] == 'wpsd_settings_options' ) {
					return;
				}
			}
            $html = '';
			$html .= '<div class="updated"><p>';
                $html .= __( 'Expand WPSuperDealer with add-ons and themes at ', 'wp-super-dealer' );
                $html .= '<a href="http://wpsuperdealer.com/" target="cduwin" title="' . __( 'Download Now &raquo;', 'wp-super-dealer' ) . '">WPSuperDealer.com</a>.';
                $html .= ' ';
                $html .= __( 'Stay up-to-date with design tips, development information and new releases.' , 'wp-super-dealer' );
                $html .= '<br />';
                $html .= '<a href="http://wpsuperdealer.com/" target="cduwin">WPSuperDealer.com</a>';
                $html .= ' | ';
                $html .= '<a href="?wpsd_nag_ignore=0">';
                    $html .= __( 'Hide Notice', 'wp-super-dealer' );
                $html .= '</a>';
            $html .= "</p></div>";
            $elements = array(
                'a' => array(
                    'href' => array(),
                    'title' => array(),
                    'target' => array(),
                ),
                'br' => array(),
                'div' => array(
                    'class' => array(),
                ),
                'p' => array(),
            );

            echo wp_kses( $html, $elements );
		}
	}
}

add_action( 'admin_init', 'wpsd_nag_ignore' );
function wpsd_nag_ignore() {
	global $current_user;
	$user_id = $current_user->ID;
	/* If user clicks to ignore the notice, add that to their user meta */
	if ( isset( $_GET['wpsd_nag_ignore'] ) && '0' == $_GET['wpsd_nag_ignore'] ) {
		 add_user_meta( $user_id, 'wpsd_ignore_notice', 'true', true );
	}
}
?>