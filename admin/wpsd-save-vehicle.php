<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if (is_admin()) {
	$post_type = wpsd_get_current_post_type();
	if ( $post_type == WPSD_POST_TYPE ) {
		add_action( 'save_post','wpsd_save_vehicle' );
	}
}

//= TO DO: Create serlialized object field with all data for keyword search, vehicle_object
function wpsd_save_vehicle( $post_id ) {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
		return;
	if ( isset( $_POST['post_type'] ) ) {
		if ( WPSD_POST_TYPE == $_POST['post_type'] ) {
			if ( ! current_user_can( 'edit_post', $post_id ) ) {
				return;
			}
		} else {
			return;
		}
	} else {
		return;
	}

	if ( isset( $_POST['sold'] ) ) {
		$sold = sanitize_text_field( $_POST['sold'] );
		update_post_meta( $post_id, 'sold', $sold );
	}
	
	if ( isset( $_POST['image_urls'] ) ) {
		$image_urls = sanitize_text_field( $_POST['image_urls'] );
		update_post_meta( $post_id, '_image_urls', $image_urls );
	}

	if ( isset( $_POST['price_field'] ) && is_array( $_POST['price_field'] ) ) {
		foreach( $_POST['price_field'] as $field=>$value ) {
			$slug = sanitize_title( $field );
			$_slug = str_replace( '-', '_', $slug );
			$value = sanitize_text_field( $value );
			update_post_meta( $post_id, '_prices_' . $_slug, $value );
		}
	}

	if ( isset( $_POST['vehicle-type'] ) ) {
		$vehicle_type = sanitize_text_field( $_POST['vehicle-type'] );
	} else {
		//= get default vehicle type
		$vehicle_type = wpsd_get_vehicle_type( 0 );
	}

	//= TO DO Future: make sure this $vehicle_type exists
	update_post_meta( $post_id, '_type', $vehicle_type );

	//= Get the vehicle's map
	$map = wpsd_get_vehicle_map( $post_id );

	//= Let's get the global fields
	$default_global_fields = wpsd_global_fields();
	$vehicle_options = wpsd_get_vehicle_options();
	if ( isset( $vehicle_options['global_specs'] ) 
		&& is_array( $vehicle_options['global_specs'] )
		&& ( 0 < count( $vehicle_options['global_specs'] ) )
		) {
		$global_fields = $vehicle_options['global_specs'];
	} else {
		$global_fields = array();
	}
	$global_fields = wp_parse_args( $global_fields, $default_global_fields );

	//= get an array of all the specs for the vehicle's type
	$registered_specs = wpsd_flatten_specs( $map );

	//= Merge in the global fields
	$registered_specs = array_merge( $registered_specs, $global_fields );

	if ( isset( $_POST['spec'] ) && is_array( $_POST['spec'] ) ) {

		foreach( $_POST['spec'] as $spec=>$value ) {
			$slug = sanitize_title( $spec );
			$_slug = str_replace( '-', '_', $slug );
			$value = sanitize_text_field( $value );
			update_post_meta( $post_id, '_' . $_slug, $value );		
			//= is this field a registered spec?

			if ( isset( $registered_specs[ $slug ] ) ) {
				//= is this field a taxonomy?
				if ( isset( $registered_specs[ $slug ]['taxonomy'] ) 
					&& ( true === $registered_specs[ $slug ]['taxonomy'] 
						|| 'true' === $registered_specs[ $slug ]['taxonomy'] ) ) 
				{
					//= was there more than one value sent in a comma seperated list?
					if ( ! empty( $value ) && false !== strpos( ',', $value ) ) {
						//= split it into multiple values
						$tags = explode( ',', $value );
						foreach( $tags as $tag ) {
							//= save each value as a term
							wp_set_post_terms( $post_id, $tag, $_slug );
						}
					} else {
						//= only one term was sent - save it
						wp_set_post_terms( $post_id, $value, $_slug );
					}
				}
			} //= end if ( isset( $registered_specs[ $slug ] ) ) {
		} //= end foreach( $_POST['spec'] as $spec=>$value ) {
	}
	
	if ( isset( $_POST['wpsd-edit-vehicle-options-list-all'] ) ) {
		$sold = sanitize_text_field( $_POST['wpsd-edit-vehicle-options-list-all'] );
		update_post_meta( $post_id, '_options', $sold );
	}
	
	if ( isset( $_POST['vehicle-description'] ) ) {
		$content = sanitize_text_field( $_POST['vehicle-description'] );
	} else {
		$content = '';
	}
	if ( isset( $_POST['vehicle-excerpt'] ) ) {
		$excerpt = sanitize_text_field( $_POST['vehicle-excerpt'] );
	} else {
		$excerpt = '';
	}

	//= TO DO Future: save default location if no location sent
	//= if default doesn't exist then create it

	return;
}

//= Create a default vehicle excerpt
add_filter( 'default_excerpt', 'wpsd_vehicle_excerpt', 10, 2 );
function wpsd_vehicle_excerpt( $post_excerpt, $post ) {
    //= do nothing if not the correct post type
    if ( WPSD_POST_TYPE != $post->post_type ) {
        return;
	}

	if ( empty( $post_excerpt ) ) {
		$post_excerpt = $post->post_title;
	}
	$post_excerpt = apply_filters( 'wpsd_post_excerpt', $post_excerpt, $post );

	return $post_excerpt;
}

function wpsd_get_current_post_type() {
	global $post, $typenow, $current_screen;
	if ( $post && $post->post_type ) {
		$post_type = $post->post_type;
	} else if ( $typenow ) {
		$post_type = $typenow;
	} else if ( $current_screen && $current_screen->post_type ) {
		$post_type = $current_screen->post_type;
	} else if( isset( $_REQUEST['post_type'] ) ) {
		$post_type = sanitize_key( $_REQUEST['post_type'] );
	} else if ( isset( $_GET['post'] ) ) {
		$post_type = get_post_type( sanitize_text_field( $_GET['post'] ) );
	} else {
		$post_type = 'post';
	}
	return $post_type;
}

//= TO DO Future: check if used, remove or do sprintf variables
function wpsd_does_meta_value_exist( $meta, $value ) {
	global $wpdb;
	$prefix = $wpdb->prefix;
	$query = "SELECT post_id FROM " . $prefix . "postmeta
		WHERE " . $prefix . "postmeta.meta_key = '" . $meta . "'
		AND " . $prefix . "postmeta.meta_value = '" . $vin . "'";
	$items = $wpdb->get_results( sprintf( $query ) );
	if ( ! empty( $items ) ) {
		foreach ( $items as $item ) {
			$id = $item->post_id;
			break;
		}
	} else {
		$id = 0;
	}
	return $id;
}
?>