<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

require_once( 'wpsd-columns.php' );
require_once( 'wpsd-map_meta_cap.php' );
require_once( 'wpsd-get-options.php' );
require_once( 'wpsd-nag.php' );
require_once( 'wpsd-updates.php' );
require_once( 'wpsd-admin-handler.php' );
require_once( 'wpsd-api.php' );
require_once( 'wpsd-settings-page.php' );
require_once( 'wpsd-save-vehicle.php' );
require_once( 'wpsd-insert-sample-vehicles.php' );

require_once( 'vehicle-edit/wpsd-edit.php' );
require_once( 'vehicle-edit/wpsd-manage-photos.php' );
require_once( 'vehicle-edit/wpsd-meta-boxes.php' );
require_once( 'vehicle-edit/wpsd-service-vin-decode.php' );
require_once( 'vehicle-edit/wpsd-service-history-reports.php' );

require_once( 'vehicle-types/vehicle-types.php' );

require_once( 'tabs/wpsd-welcome-tab.php' );
require_once( 'tabs/wpsd-price-tab.php' );
require_once( 'tabs/wpsd-general-settings-tab.php' );
require_once( 'tabs/wpsd-api-tab.php' );
require_once( 'tabs/wpsd-layout-tab.php' );
require_once( 'tabs/wpsd-news-tab.php' );
require_once( 'tabs/wpsd-setup-guide-tab.php' );
require_once( 'tabs/wpsd-change-log-tab.php' );
require_once( 'tabs/wpsd-go-pro-tab.php' );
require_once( 'tabs/wpsd-vehicle-options-tab.php' );
require_once( 'tabs/wpsd-search-form-tab.php' );
require_once( 'tabs/wpsd-locations-tab.php' );
require_once( 'tabs/wpsd-forms-tab.php' );
require_once( 'tabs/wpsd-car-demon-tab.php' );
require_once( 'readme.php' );

add_action( 'admin_init', 'wpsd_admin_init' );
function wpsd_admin_init() {
	if ( is_admin() ) {
		add_action( 'restrict_manage_posts','wpsd_restrict_listings_by_sold' );
		add_filter( 'parse_query', 'wpsd_filter_by_sold' );
	}
	$post_type = wpsd_get_current_post_type();
	//= TO DO Future: restrict admin CSS loading
    global $pagenow;
    
	if ( WPSD_POST_TYPE === $post_type || ( isset( $_GET['page'] ) && 'wpsd_settings_options' === $_GET['page'] ) 
		|| ( isset( $_GET['post_type'] ) && WPSD_POST_TYPE === $_GET['post_type'] )
        || ( 'widgets.php' === $pagenow ) ) {
		add_action( 'admin_print_scripts', 'wpsd_admin_scripts' );
		add_action( 'admin_print_styles', 'wpsd_admin_styles' );
	}
}

add_filter( 'plugin_action_links_wp-super-dealer/wp-super-dealer.php', 'wpsd_plugin_action_links', 10, 2 );
function wpsd_plugin_action_links( $links_array ) {
    //= TO DO Future: don't hard code wp-admin link
    $settings_link = '<a href="/wp-admin/edit.php?post_type=vehicle&page=wpsd_settings_options">' . __( 'Settings', 'wp-super-dealer' ) . '</a>';
    array_unshift( $links_array, $settings_link );
	return $links_array;
}

function wpsd_admin_scripts() {
	if ( defined( 'WPSD_NON_NUMERIC_PRICE' ) ) {
		$non_numeric_price = 'Yes';
	} else {
		$non_numeric_price = 'No';
	}
	$no_photo = WPSD_PATH . 'images/coming-soon.jpg';
	if ( defined( 'WPSD_CUSTOM_NO_PHOTO' ) ) {
		$no_photo = WPSD_CUSTOM_NO_PHOTO;
	}

	wp_enqueue_media();
	$strings = array(
		'str0' => __( 'PLEASE CONFIRM', 'wp-super-dealer' ),
		'str1' => __( 'Do really want to do this?', 'wp-super-dealer' ),
		'str2' => __( 'Type not added', 'wp-super-dealer' ),
		'str3' => __( ' already exists.', 'wp-super-dealer' ),
		'str4' => __( 'Type not added', 'wp-super-dealer' ),
		'str5' => __( 'You did not enter a valid type name.', 'wp-super-dealer' ),
		'str6' => __( 'Group not added', 'wp-super-dealer' ),
		'str7' => __( ' already exists.', 'wp-super-dealer' ),
		'str8' => __( 'Group not added', 'wp-super-dealer' ),
		'str9' => __( 'You did not enter a valid group name.', 'wp-super-dealer' ),
		'str10' => __( 'Specification not added', 'wp-super-dealer' ),
		'str11' => __( ' already exists.', 'wp-super-dealer' ),
		'str12' => __( 'Specification not added', 'wp-super-dealer' ),
		'str13' => __( 'You did not enter a valid specification name.', 'wp-super-dealer' ),
		'str14' => __( 'Option Group not added', 'wp-super-dealer' ),
		'str15' => __( ' already exists.', 'wp-super-dealer' ),
		'str16' => __( 'Option Group not added', 'wp-super-dealer' ),
		'str17' => __( 'You did not enter a valid option group name.', 'wp-super-dealer' ),
		'str18' => __( 'Option Sub-Group not added', 'wp-super-dealer' ),
		'str19' => __( ' already exists.', 'wp-super-dealer' ),
		'str20' => __( 'Option Sub-Group not added', 'wp-super-dealer' ),
		'str21' => __( 'You did not enter a valid option sub-group name.', 'wp-super-dealer' ),
		'str22' => __( 'Option not added', 'wp-super-dealer' ),
		'str23' => __( ' already exists.', 'wp-super-dealer' ),
		'str24' => __( 'Option not added', 'wp-super-dealer' ),
		'str25' => __( 'You did not enter a valid option name.', 'wp-super-dealer' ),
		'str26' => __( 'The ', 'wp-super-dealer' ),
		'str27' => __( ' field must have 2 or more characters', 'wp-super-dealer' ),
		'str28' => __( 'Saving... Please wait.', 'wp-super-dealer' ),
		'str29' => __( 'hide', 'wp-super-dealer' ),
		'str30' => __( 'show', 'wp-super-dealer' ),
		'str31' => __( 'Taxonomy UI', 'wp-super-dealer' ),
		'str32' => __( 'This option will turn on the native WordPress UI for this taxonomy.<br /><br />Directly editing a taxonomy may cause unexpected results.', 'wp-super-dealer' ),
		'str33' => __( 'Sorry, a type with that name already exists.', 'wp-super-dealer' ),
		'str34' => __( 'Default', 'wp-super-dealer' ),
		'str35' => __( 'Reloading page', 'wp-super-dealer' ),
		'str36' => __( 'Loading vehicle type', 'wp-super-dealer' ),
		'str37' => __( 'Vehicle Type Icon', 'wp-super-dealer' ),
		'str38' => __( 'Change Vehicle Type Icon', 'wp-super-dealer' ),
		'str39' => __( 'Enter a name for your new location:', 'wp-super-dealer' ),
		'str40' => __( 'Adding Location', 'wp-super-dealer' ),
		'str41' => __( 'Removing Location', 'wp-super-dealer' ),
		'str42' => __( 'You can\'t remove the last vehicle./n Please add a new location before trying to remove this one.', 'wp-super-dealer' ),
		'str43' => __( 'Updating vehicle', 'wp-super-dealer' ),
        'str44' => __( 'Updating JSON', 'wp-super-dealer' ),
        'str45' => __( 'Updating JSON Schedule', 'wp-super-dealer' ),
        'str46' => __( 'Removing JSON Schedule', 'wp-super-dealer' ),
		'str47' => __( 'No credits available.', 'wp-super-dealer' ),
		'str48' => __( 'Yes', 'wp-super-dealer' ),
		'str49' => __( 'No', 'wp-super-dealer' ),
        'str50' => __( 'Retrieving data', 'wp-super-dealer' ),
        'str51' => __( 'Open Data API', 'wp-super-dealer' ),
        'str52' => __( 'Close Data API', 'wp-super-dealer' ),
        'str53' => __( 'Checking API Key', 'wp-super-dealer' ),
        'str54' => __( 'Importing Car Demon', 'wp-super-dealer' ),
        'str55' => __( 'Did you back up your site?', 'wp-super-dealer' ),
        'str56' => __( 'Removing Report', 'wp-super-dealer' ),
        'str57' => __( 'Are you sure you want to remove this report?', 'wp-super-dealer' ),
        'str58' => __( 'Your VIN does not appear to be valid. Please double check it.', 'wp-super-dealer' ),
	);
	$strings = apply_filters( 'cdcs_strings_filter', $strings );
	wp_register_script( 'wp-super-dealer-admin-js', WPSD_PATH . '/js/wpsd-admin.js', array( 'jquery','jquery-ui-core', 'jquery-ui-autocomplete', 'jquery-ui-sortable', 'jquery-ui-droppable' ), WPSD_VER, true );
	wp_localize_script( 'wp-super-dealer-admin-js', 'wpsdAdminParams', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'error1' => __( 'You must fill out both fields before adding a new option group.', 'wp-super-dealer' ),
		'spinner' => WPSD_PATH . 'images/wpspin_light.gif',
		'sample_msg' => __( 'Please be patient while the vehicles and their images import.', 'wp-super-dealer' ),
		'msg_update' => __( 'Option Group Updated', 'wp-super-dealer' ),
		'bad_price_msg' => __( 'Please use numeric values only.', 'wp-super-dealer' ),
		'reset_msg' => __( 'Are you sure? This will reset all of your WPSuperDealer Settings!', 'wp-super-dealer' ),
		'bad_image_links' => __( 'The list of image URLs you entered was not valid. Please check them and try again.', 'wp-super-dealer' ),
		'no_photo' => esc_html( $no_photo ),
		'non_numeric_price' => esc_html( $non_numeric_price ),
		'str_manage_vehicle_photos' => __( 'Manage Vehicle Photos', 'wp-super-dealer' ),
		'str_attach_photos_to_vehicle' => __( 'Attach Photos to Vehicle', 'wp-super-dealer' ),
		'remove_image_msg' => __( 'Are you sure you want to delete this image? If you confirm this photo will be immediately removed.', 'wp-super-dealer' ),
		'strings' => $strings,
	));
	wp_enqueue_script( 'wp-super-dealer-admin-js' );
}

function wpsd_admin_styles() {
    wp_enqueue_style( 'wp-super-dealer-admin-css', WPSD_PATH . '/css/wpsd-admin.css', array(), WPSD_VER );
	wp_enqueue_style( 'thickbox' );
}

function wpsd_welcome_screen_do_activation_redirect() {
	//= Check for constant that bypasses redirect - fix for users on older versions of WordPress
	if ( defined( 'WP_SUPER_DEALER_NO_WELCOME' ) ) {
		return;
	}

	//= Bail if no activation redirect
	if ( ! get_transient( '_wpsd_welcome_screen_activation_redirect' ) ) {
		return;
	}
	
	//= Delete the redirect transient
	delete_transient( '_wpsd_welcome_screen_activation_redirect' );
	
	//= Bail if activating from network, or bulk
	if ( is_network_admin() || isset( $_GET['activate-multi'] ) ) {
		return;
	}
	
    //= let's make sure we have at least the default location
    $args = array(
        'style'              => 'none',
        'show_count'         => 0,
        'use_desc_for_title' => 0,
        'hierarchical'       => true,
        'echo'               => 0,
        'hide_empty'		 => 0,
        'taxonomy'           => 'location',
    );
    $locations = get_categories( $args );
    if ( 1 > count( $locations ) ) {
        $location = __( 'Default', 'wp-super-dealer' );
		$add_location = wp_insert_term( $location, 'location' );
    }
    
	//= Redirect to about page
	wp_safe_redirect( add_query_arg( array( 'page' => 'wpsd_settings_options', 'post_type' => WPSD_POST_TYPE ), admin_url( 'edit.php' ) ) );

}
add_action( 'admin_init', 'wpsd_welcome_screen_do_activation_redirect' );

?>