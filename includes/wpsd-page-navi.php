<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_nav( $position, $search_query ) {
	$x = '';
	global $wp_query;
	if ( $position == 'top' ) {
		$second_position = 'above';
		$third_position = '-top';
	}
	if ( $position == 'bottom' ) {
		$second_position = 'below';
		$third_position = '';
	}
	if ( $search_query->max_num_pages > 1 ) {
		$x .= '<div id="wpsd-nav-' . $second_position . '">';
		if( function_exists( 'wp_pagenavi' ) ) {
			$nav_list_str = wp_pagenavi( array( 'query' => $search_query, 'echo' => false ) );
			if ( $position == 'top' ) {
				$nav_list_str = str_replace( 'nextpostslink','nextpostslink-' . $second_position, $nav_list_str );
			} else {
				$nav_list_str = str_replace( 'wp-pagenavi', 'wp-pagenavi nav-bottom', $nav_list_str );
			}
			$x .= $nav_list_str;
		} else {
			$x .= wpsd_page_navi( $search_query );
		} 
		$x .= '</div><!-- #nav-' . $second_position . ' -->';
	} else {
		if( function_exists( 'wp_pagenavi' ) ) {
			$x .= '<div id="wpsd-nav-' . esc_attr( $second_position ) . '" class="navigation-' . esc_attr( $position ) . ' inventory_nav"><span class="wp-pagenavi"><span class="pages">'. esc_attr( $wp_query->post_count ) . esc_attr( __( 'Results Found', 'wp-super-dealer') ) . '</span></span>';
			$x .= '</div>';
		} else {
			$x .= wpsd_page_navi( $search_query );
		}
	}
	$x = str_replace( 'none', '', $x );
	$x = apply_filters( 'wpsd_nav_filter', $x, $position, $search_query );
    
    $elements = wpsd_page_navi_elements();
    //= lock it down to just our elements
    $x = wp_kses( $x, $elements );
    
	return $x;
}

function wpsd_page_navi_elements() {
    $elements = array(
        'a' => array(
            'href' => array(),
            'class' => array(),
        ),
        'div' => array(
            'id' => array(),
            'class' => array(),
        ),
        'span' => array(
            'id' => array(),
            'class' => array(),
        ),
        'ul' => array(
            'id' => array(),
            'class' => array(),
        ),
        'li' => array(
            'id' => array(),
            'class' => array(),
            'title' => array(),
        ),
    );
    $elements = apply_filters( 'wpsd_nav_esc_elements_filter', $elements );
    return $elements;
}

function wpsd_page_navi( $query ) {
	$html = '';

    /** Stop execution if there's only 1 page */
    if ( $query->max_num_pages <= 1 ) {
        return;
	}
 
    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $query->max_num_pages );
 
    /** Add current page to the array */
    if ( $paged >= 1 ) {
        $links[] = $paged;
	}
 
    /** Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }
 
    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }
 
    $html .= '<div class="wpsd-navigation">';
		$html .= '<ul>';
 
		/** Previous Post Link */
		if ( get_previous_posts_link() ) {
			$html .= '<li class="wpsd-navigation-li" title="' . __( 'Previous Page', 'wp-super-dealer' ) . '">' . get_previous_posts_link( '«' ) . '</li>';
		}
	
		/** Link to first page, plus ellipses if necessary */
		if ( ! in_array( 1, $links ) ) {
			$class = 1 == $paged ? ' class="wpsd-navigation-li active"' : ' class="wpsd-navigation-li"';
	 
			$html .= '<li' . $class . '><a class="wpsd-navigation-li-a" href="' . esc_url( get_pagenum_link( 1 ) ) . '">1</a></li>';
	 
			if ( ! in_array( 2, $links ) ) {
				$html .= '<li class="wpsd-navigation-li">…</li>';
			}
		}
	
		/** Link to current page, plus 2 pages in either direction if necessary */
		sort( $links );
		foreach ( (array) $links as $link ) {
			$class = $paged == $link ? ' class="wpsd-navigation-li active"' : ' class="wpsd-navigation-li"';
			$html .= '<li' . $class . '><a class="wpsd-navigation-li-a" href="' . esc_url( get_pagenum_link( $link ) ) . '">' . esc_html( $link ) . '</a></li>';
		}
	 
		/** Link to last page, plus ellipses if necessary */
		if ( ! in_array( $max, $links ) ) {
			if ( ! in_array( $max - 1, $links ) )
				$html .= '<li>…</li>';
	 
			$class = $paged == $max ? ' class="wpsd-navigation-li active"' : ' class="wpsd-navigation-li"';
			$html .= '<li' . $class . '><a class="wpsd-navigation-li-a" href="' . esc_url( get_pagenum_link( $max ) ) . '">' . esc_html( $max ) . '</a></li>';
		}

		/** Next Post Link */
		if ( get_next_posts_link( '»', $max ) ) {
			$html .= '<li class="wpsd-navigation-li nextpostslink" title="' . __( 'Next Page', 'wp-super-dealer' ) . '">' . get_next_posts_link( '»', $max ) . '</li>';
		}
 
	    $html .= '</ul>';
	$html .= '</div>';
	$html = apply_filters( 'wpsd_page_navi_filter', $html, $query );
	return $html;
}
?>