<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Query for WPSuperDealer Search
 *
 * @param $query optional query terms
 *
 * @todo
 *     - consolidate with wpsd_archive_query()
 */
function wpsd_query_search( $query = array() ) {
	global $wpsd_options;
	$get = array_merge( $query, $_GET );

	if ( is_array( $get ) ) {
		//= TO DO Future: break down to array_map
		foreach ( $get as $key=>$value ) {
			if ( $value == 'ALL' ) {
				$get[ $key ] = '';
			}
			if ( ! is_array( $value ) ) {
				$get[ $key ] = sanitize_text_field( $value );
			}
		}
	}

	if ( isset( $wpsd_options['vehicles_per_page'] ) ) {
		$vehicles_per_page = $wpsd_options['vehicles_per_page'];
	} else {
		$vehicles_per_page = 9;
	}
	if ( isset( $get['vehicles_per_page'] ) ) {
		$vehicles_per_page = $get['vehicles_per_page'];
	}

	if ( isset( $get[ WPSD_POST_TYPE ] ) ) {
		add_filter( 'wp_title', 'wpsd_filter_search_title', 10, 3 );

		if ( is_home() || is_front_page() ) {
			$paged = get_query_var( 'page' ) ? get_query_var( 'page' ) : 1;
		} else {
			$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
		}

		if ( $wpsd_options['show_sold'] != __( 'Yes', 'wp-super-dealer' ) ) {
			$meta_query = array(
				array(
					'key' => 'sold',
					'value' => __( 'No', 'wp-super-dealer' ),
					'compare' => '=',
				)
			);
		} else {
			$meta_query = array();
		}

		if ( isset( $get['show_sold'] ) ) {
			if ( $get['show_sold'] == __( 'Yes', 'wp-super-dealer' ) ) {
				$meta_query = array();
			}
		}
		
		if ( isset( $get['price_min'] ) ) {
			$min_price = sanitize_text_field( $get['price_min'] );
		} else {
			$min_price = '';
		}
		if ( isset( $get['price_max'] ) ) {
			$max_price = sanitize_text_field( $get['price_max'] );
		} else {
			$max_price = '';
		}
		if ( $max_price > 0 ) {
			if ( $min_price == 0 ) { $min_price = -1; }
			$meta_query = array_merge( $meta_query, array( array( 'key' => '_price','value' => array( $min_price, $max_price ), 'compare' => 'BETWEEN', 'type' => 'numeric' ) ) );
		} else {
			if ( $min_price > 0 ) {
				$meta_query = array_merge( $meta_query, array( array( 'key' => '_price','value' => $min_price, 'compare' => '>', 'type' => 'numeric' ) ) );
			}
		}

		$spec_fields = wpsd_get_all_specifications();
        $spec_fields['type'] = 'Type';
		if ( 0 < count( $spec_fields ) ) {
			foreach( $spec_fields as $spec_field=>$data ) {
				$slug = sanitize_title( $spec_field );
				$_slug = str_replace( '-', '_', $slug );
				if ( 'vehicle_tags' === $_slug ) {
					continue;
				}
				if ( ! isset( $get[ $_slug ] ) ) {
					continue;
				}
				//= was more than one value sent for this field?
				//= props: https://wordpress.stackexchange.com/questions/104060/using-or-conditions-in-meta-query-for-query-posts-argument
				if ( false !== strpos( $get[ $_slug ], '|' ) ) {
					//= explode the values and let's make an array for an OR statement
					$spec_values = explode( '|', $get[ $_slug ] );
					$spec_query = array();
					$spec_query['relation'] = 'OR';
					foreach( $spec_values as $spec_value ) {
						$spec_query[] = array( 
								'key' => '_' . $_slug,
								'value' => $spec_value, 
								'compare' => '=', 
						);
					}
					$meta_query = array_merge( $meta_query, array( $spec_query ) );
				} else {
					$meta_query = array_merge(
						$meta_query, array( 
							array( 
								'key' => '_' . $_slug,
								'value' => $get[ $_slug ], 
								'compare' => '=', 
							)
						)
					);
				} //= end if ( false !== strpos( $get[ $_slug ], ',' ) ) {
			}
		} //= end if ( 0 < count( $spec_fields ) ) {

		// Search decode field
		if ( isset( $get['criteria'] ) ) {
			$criteria = sanitize_text_field( $get['criteria'] );
			if ( $criteria ) {
				if ( strpos( $criteria, ',' ) ) {
					$criteria_array = explode( ',', $criteria );
					foreach( $criteria_array as $search_criteria ) {
						$meta_query = array_merge( $meta_query, array( array( 'value' => $search_criteria, 'compare' => 'LIKE', 'type' => 'text' ) ) );	
					}
				} else {
                    $meta_query = array_merge( $meta_query, array( array( 'value' => $criteria, 'compare' => 'LIKE', 'type' => 'text' ) ) );
				}
			}
		}

		$vehicle_query = array(
			'post_type' => WPSD_POST_TYPE,
			'is_paged' => true,
			'paged' => $paged,
			'posts_per_page' => $vehicles_per_page,
			'meta_query' => $meta_query,
			'orderby' => 'meta_value_num',
		);
        
		if ( isset( $get['vehicle_tags'] ) ) {
			if ( $get['vehicle_tags'] ) {
				if ( strpos( $get['vehicle_tags'], '|' ) !== false ) {
					$tag_array = explode( '|', $get['vehicle_tags'] );
					$tag_tax = array();
					$tag_tax['taxonomy'] = 'vehicle_tags';
					$tag_tax['terms'] = $tag_array;
					$tag_tax['field'] = 'slug';
					$tag_tax['compare'] = '=';
					$tag_tax['type'] = 'string';
					unset($vehicle_query['vehicle_tags']);
					$vehicle_query['tax_query'][] = $tag_tax;
				} else {
					$vehicle_tags = sanitize_text_field( $get['vehicle_tags'] );
					$vehicle_query = array_merge( $vehicle_query, array( 'vehicle_tags' => $vehicle_tags ) );
				}
			}
		}

		/*   FIX BY MT 20180720 - Thanks! j */
		if ( isset( $get['location'] ) ) {
			if ( $get['location'] ) {
				if ( strpos( $get['location'], '|' ) !== false ) {
					$location_array = explode( '|', $get['location'] );
				} else {
					$location_array = array( $get['location'] );
				}

				$location_tax = array();
				$location_tax['taxonomy'] = 'location';
				$location_tax['terms'] = $location_array;
				$location_tax['field'] = 'slug';
				$location_tax['compare'] = '=';
				$location_tax['type'] = 'string';
				unset($vehicle_query['location']);
				$vehicle_query['tax_query'][] = $location_tax;

			}
		}
		/*   End Added 20180720  */

		$vehicle_query = apply_filters( 'wpsd_query_filter', $vehicle_query );

        return $vehicle_query;
	}
}

/**
 * Query for WPSuperDealer archive pages
 *
 * @todo
 *     - consolidate with wpsd_search_query()
 */
function wpsd_query_archive() {
	global $wpsd_options;
	global $query_string;
	if ( isset( $wpsd_options['vehicles_per_page'] ) ) {
		$vehicles_per_page = $wpsd_options['vehicles_per_page'];
	} else {
		$vehicles_per_page = 9;
	}
	
	$order_by = '_price_value';
	$order_by_dir = 'ASC';
	if ( isset( $_GET['order_by'] ) ) {
		$order_by = sanitize_text_field( $_GET['order_by'] );
	} else {
		$order_by = '';
	}
	if ( isset( $_GET['order_by_dir'] ) ) {
		$order_by_dir = sanitize_text_field( $_GET['order_by_dir'] );
	} else {
		$order_by_dir = '';
	}
	$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
	if ( isset( $_GET['search_price_min'] ) ) {
		$min_price = sanitize_text_field( $_GET['search_price_min'] );
	} else {
		$min_price = '';
	}
	if ( isset( $_GET['search_price_max'] ) ) {
		$max_price = sanitize_text_field( $_GET['search_price_max'] );
	} else {
		$max_price = '';
	}
	if ( $wpsd_options['show_sold'] != __( 'Yes', 'wp-super-dealer' ) ) {
		$meta_query = array(
				array(
					'key' => 'sold',
					'value' => __( 'No', 'wp-super-dealer' ),
					'compare' => '=',
				)
			);
	}
	$vehicle_query = array(
			'post_type' => WPSD_POST_TYPE,
			'is_paged' => true,
			'paged' => $paged,
			'posts_per_page' => $vehicles_per_page,
			'meta_query' => $meta_query,
			'orderby' => 'meta_value_num',
			'meta_key' => $order_by,
			'order' => $order_by_dir,
		);
	$vehicle_query = wp_parse_args( $query_string, $vehicle_query );
	$vehicle_query = apply_filters( 'wpsd_query_filter', $vehicle_query );
	return $vehicle_query;
}

/**
 * Filter search page title
 *
 * @param $title the current page title of the search result page
 *
 */
function wpsd_filter_search_title( $title ) {
	$title = str_replace( __( 'Page not found', 'wp-super-dealer' ), __( 'Search Results', 'wp-super-dealer' ), $title );
	return $title;
}

//= Switch out the Query if it's a vehicle search page or archive page
add_filter( 'pre_get_posts','wpsd_replace_query' );
function wpsd_replace_query( $query ) {
	if ( is_preview() && is_admin() && is_singular() && is_404() ) {
		return $query;
	}
	if ( is_admin() ) {
		return $query;
	}
	if ( $query->is_home() ) {
		return $query;
	}
	//= Set $query equal to WPSuperDealer Search query
	if ( $query->is_main_query() ) {
		$search_query = wpsd_query_search();
		if ( isset( $_GET['s'] ) ) {
			if ( isset( $_GET[ WPSD_POST_TYPE ] ) ) {
                $search_query = wpsd_query_search();
                foreach ( $search_query as $key=>$val ) {
                    $query->query_vars[$key] = $val;
                }
                return $query;
			}
		}
	}
	if ( $query->is_main_query() ) {
		if ( is_post_type_archive( WPSD_POST_TYPE ) ) {
			//= Set $query equal to WPSuperDealer archive Query
			$search_query = wpsd_query_archive();
			foreach ( $search_query as $key=>$val ) {
				$query->query_vars[$key] = $val;
			}
		}
	}

    return $query;
}
?>