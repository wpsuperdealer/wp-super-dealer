<?php
//= https://developers.elementor.com/creating-a-new-widget/
//= https://developers.elementor.com/elementor-controls/
//= https://www.soliddigital.com/blog/creating-a-custom-elementor-widget
namespace WPSD;
use Elementor\Plugin;

function wpsd_load_elementor() {
    require_once( WPSD_DIR . 'classes/class-elementor-inventory.php' );

	$widget = new Elementor_WPSD_Inventory_Widget();

	// Let Elementor know about our widget
	Plugin::instance()->widgets_manager->register_widget_type( $widget );
    
}
add_action( 'elementor/widgets/widgets_registered', 'WPSD\wpsd_load_elementor' );
?>