<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! function_exists( 'wpsd_recreate_post_type' ) && ! function_exists( 'cdcs_register_cpt_projects' ) ) {
	add_action( 'init', 'wpsd_create_post_type' );
}

if ( ! function_exists( 'cdcs_register_cpt_projects' ) ) {
	add_action( 'init', 'wpsd_tax_init' );
}

/**
 * Register vehicle post type
 *
 */
function wpsd_create_post_type() {
	global $wpsd_options;
    $slug = $wpsd_options['wpsd_slug'];
	if ( empty( $slug ) ) {
		$slug = 'vehicles-for-sale';
		update_option( 'wp-super-dealer-slug', $slug );
	}
	if ( defined( 'WPSD_POST_TYPE' ) ){
		$post_type = WPSD_POST_TYPE;
	} else {
		$post_type = WPSD_POST_TYPE;
	}

	$labels = array(
        'name'                  => _x( 'Vehicles For Sale', 'Post type general name', 'wp-super-dealer' ),
        'singular_name'         => _x( 'Vehicle', 'Post type singular name', 'wp-super-dealer' ),
        'menu_name'             => _x( 'Vehicles', 'Admin Menu text', 'wp-super-dealer' ),
        'name_admin_bar'        => _x( 'Vehicle', 'Add New on Toolbar', 'wp-super-dealer' ),
        'add_new'               => __( 'Add New', 'wp-super-dealer' ),
        'add_new_item'          => __( 'Add New vehicle', 'wp-super-dealer' ),
        'new_item'              => __( 'New vehicle', 'wp-super-dealer' ),
        'edit_item'             => __( 'Edit vehicle', 'wp-super-dealer' ),
        'view_item'             => __( 'View vehicle', 'wp-super-dealer' ),
        'all_items'             => __( 'All vehicles', 'wp-super-dealer' ),
        'search_items'          => __( 'Search vehicle', 'wp-super-dealer' ),
        'parent_item_colon'     => __( 'Parent vehicle:', 'wp-super-dealer' ),
        'not_found'             => __( 'No vehicles found.', 'wp-super-dealer' ),
        'not_found_in_trash'    => __( 'No vehicles found in Trash.', 'wp-super-dealer' ),
        'featured_image'        => _x( 'Main Vehicle Photo', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'wp-super-dealer' ),
        'set_featured_image'    => _x( 'Set main photo', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'wp-super-dealer' ),
        'remove_featured_image' => _x( 'Remove main photo', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'wp-super-dealer' ),
        'use_featured_image'    => _x( 'Use as main photo', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'wp-super-dealer' ),
        'archives'              => _x( 'Vehicle archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'wp-super-dealer' ),
        'insert_into_item'      => _x( 'Insert into vehicle', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'wp-super-dealer' ),
        'uploaded_to_this_item' => _x( 'Uploaded to this vehicle', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'wp-super-dealer' ),
        'filter_items_list'     => _x( 'Filter vehicles list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'wp-super-dealer' ),
        'items_list_navigation' => _x( 'Vehicles list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'wp-super-dealer' ),
        'items_list'            => _x( 'Vehicles list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'wp-super-dealer' ),
    );

	$wpsd_supports = array( 'title', 'thumbnail' );
	$wpsd_supports = apply_filters( 'wpsd_supports_filter', $wpsd_supports );

	$atts = array(				  
		'labels' => $labels,
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'query_var' => $slug,
		'capability_type' => array( WPSD_POST_TYPE, 'vehicles' ),
		'capabilities' => array(
			'publish_posts' => 'publish_vehicles',
			'edit_posts' => 'edit_vehicles',
			'edit_others_posts' => 'edit_others_vehicles',
			'delete_posts' => 'delete_vehicles',
			'delete_others_posts' => 'delete_others_vehicles',
			'read_private_posts' => 'read_private_vehicles',
			'edit_published_posts' => 'edit_published_vehicles',
			'edit_post' => 'edit_vehicle',
			'delete_post' => 'delete_vehicle',
			'read_post' => 'read_vehicle',
			'create_posts' => 'create_vehicles',
		),
		'map_meta_cap' => true,
		'rewrite' => array( 'slug' => $slug, 'with_front' => false, ),
		'has_archive' => true,
		'supports' => $wpsd_supports,
		'show_in_rest' => true,
		'menu_icon' => plugins_url( '../images/wpsd_icon.png', __FILE__ ),
	);
	$atts = apply_filters( 'wpsd_post_type_atts_filter', $atts );

	register_post_type( $post_type, $atts );

    if ( current_user_can( 'manage_options' ) ) {
        $flush = get_option( 'wpsd_flush_rewrite', false );
        if ( $flush ) {
            global $wp_rewrite; $wp_rewrite->flush_rules( true ); $wp_rewrite->init();
            delete_option( 'wpsd_flush_rewrite' );
        }
    }
}

/**
 * Register vehicle taxonomies
 *
 */
function wpsd_tax_init() {
	$show_ui = true;

	//= Register vehicle_type since all vehicles should have this
	register_taxonomy(
		'vehicle_type',
		WPSD_POST_TYPE,
		array(
			'label' => __( 'Vehicle Types', 'wp-super-dealer' ),
			'labels' => array(
				'add_new_item' => __( 'Add New Vehicle Type', 'wp-super-dealer' ),
				'edit_item' =>  __( 'Edit Vehicle Type', 'wp-super-dealer' )
				),
			'sort' => true,
			'args' => array( 'orderby' => 'term_order' ),
			'rewrite' => array( 'slug' => 'vehicle_type' ),
			'show_ui' => false,
		)
	);

	//= Let's take care of the global fields
	$default_global_fields = wpsd_global_fields();
	$vehicle_options = wpsd_get_vehicle_options();
	if ( isset( $vehicle_options['global_specs'] ) 
		&& is_array( $vehicle_options['global_specs'] )
		&& ( 0 < count( $vehicle_options['global_specs'] ) )
		) {
		$global_fields = $vehicle_options['global_specs'];
	} else {
		$global_fields = array();
	}
	$global_fields = wp_parse_args( $global_fields, $default_global_fields );

	foreach( $global_fields as $global_field=>$field_options ) {
		if ( isset( $field_settings['allow_taxonomy'] ) && false === $field_settings['allow_taxonomy'] ) {
			continue;
		}
		if ( isset( $field_settings['taxonomy'] ) && false === $field_settings['taxonomy'] ) {
			continue;
		}
		$specification_slug = sanitize_title( $field_options['label'] );
		$specification_slug = str_replace( '-', '_', $specification_slug );
		if ( empty( $specification_slug ) ) {
			continue;
		}
		
		$show_ui = false;
		if ( isset( $field_options['show-ui'] ) 
			&& ( 'true' === $field_options['show-ui']
			   || true === $field_options['show-ui'] ) )
		{
			$show_ui = true;
		}

		$plural = $field_options['label'];
		if ( isset( $field_options['plural'] ) && ! empty( $field_options['plural'] ) ) {
			$plural = $field_options['plural'];
		}

		register_taxonomy(
			$specification_slug,
			WPSD_POST_TYPE,
			array(
				'label' => $plural,
				'labels' => array(
					'singular_name' => $field_options['label'],
					'add_new_item' => __( 'Add New ', 'wp-super-dealer' ) . $field_options['label'],
					'edit_item' =>  __( 'Edit ', 'wp-super-dealer' ) . $field_options['label'],
				),
				'sort' => true,
				'args' => array( 'orderby' => 'term_order' ),
				'rewrite' => array( 'slug' => $specification_slug ),
				'show_ui' => $show_ui,
			)
		);
	}

	//= Add Default location to prevent it from being deleted
	$taxonomy = 'location';
	$key = get_option( 'default_' . $taxonomy );
	$term = get_term_by( 'name', 'Default', $taxonomy );
	if ( empty( $key ) && $term ) {
		 update_option( $key, $term->term_id );
	}

	//= Let's find out if we have any custom specs that should be registered as taxonomies
	$types = wpsd_get_type_maps();
	foreach( $types as $types=>$data ) {
		if ( isset( $data['specifications'] ) ) {
			if ( is_array( $data['specifications'] ) && count( $data['specifications'] ) > 0 ) {
				foreach( $data['specifications'] as $group=>$spec ) {
					if ( is_array( $spec ) && count( $spec ) > 0 ) {
						foreach( $spec as $spec_name=>$spec_options ) {
							if ( isset( $spec_options['taxonomy'] ) && $spec_options['taxonomy'] ) {
								$show_ui = false;
								if ( isset( $spec_options['show-ui'] ) && $spec_options['show-ui'] ) {
									$show_ui = true;
								}
								$label = $spec_name;
								if ( isset( $spec_options['label'] ) && ! empty( $spec_options['label'] ) ) {
									$label = $spec_options['label'];
								}
								$plural = $label;
								if ( isset( $spec_options['plural'] ) && ! empty( $spec_options['plural'] ) ) {
									$plural = $spec_options['plural'];
								}
								wpsd_register_spec_tax( $spec_name, $label, $plural, $show_ui );
							}
						}
					}
				}
			}
		} //= It's like a stairway to heaven around here
	}	
}

function wpsd_register_spec_tax( $spec, $label, $plural, $show_ui = false ) {
	$spec_rewrite_slug = sanitize_title( $spec );
	$spec_slug = str_replace( '-', '_', $spec_rewrite_slug );
	if ( 'true' === $show_ui ) {
		$show_ui = true;
	}
	register_taxonomy(
		$spec_slug,
		WPSD_POST_TYPE,
		array(
			'label' => wpsd_single_to_plural( $plural ),
			'labels' => array(
				'singular_name' => $label,
				'add_new_item' => __( 'Add New ', 'wp-super-dealer' ) . $label,
				'edit_item' =>  __( 'Edit ', 'wp-super-dealer' ) . $label,
				),
			'sort' => true,
			'args' => array( 'orderby' => 'term_order' ),
			'rewrite' => array( 'slug' => $spec_rewrite_slug ),
			'show_ui' => $show_ui,
		)
	);
}

function wpsd_single_to_plural( $string ) {
	$plural = $string;
	
	$plural = apply_filters( 'wpsd_single_to_plural_filter', $plural, $string );
	return $plural;
}
?>