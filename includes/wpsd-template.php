<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_output_content_wrapper() {
	global $wpsd_options;
	if ( isset( $wpsd_options['vehicle_container'] ) ) {
		$container = $wpsd_options['vehicle_container'];
	} else {
		$container = '';
	}
    echo '<div id="wpsd-inventory-wrap" class="' . esc_attr( $container ) . '">';
}
add_action( 'wpsd_before_content_action', 'wpsd_output_content_wrapper', 10 );

function wpsd_output_content_wrapper_end() {
	global $wpsd_options;
	echo '</div><!-- #wpsd-container-wrap -->';
}
add_action( 'wpsd_after_content_action', 'wpsd_output_content_wrapper_end', 10 );

function get_vehicle_title( $post_id ) {
	global $wpsd_options;
	if ( $wpsd_options['use_post_title'] == __( 'Yes', 'wp-super-dealer' ) ) {
		$vehicle_title = get_the_title( $post_id );
	} else {
		$vehicle_title = '';
	}
	if ( empty( $vehicle_title ) ) {
		$vehicle_year = get_post_meta( $post_id, '_vehicle_year', true );
		$vehicle_make = get_post_meta( $post_id, '_make', true );
		$vehicle_model = get_post_meta( $post_id, '_model', true );
		$vehicle_title = $vehicle_year . ' ' . $vehicle_make . ' '. $vehicle_model;
	}
	$vehicle_title = trim( $vehicle_title );
	if ( isset( $wpsd_options['title_trim'] ) ) {
		if ( $wpsd_options['title_trim'] > 1 ) {
			$vehicle_title = substr( $vehicle_title, 0, $wpsd_options['title_trim'] );
		}
	}
	$vehicle_title = apply_filters( 'wpsd_title_filter', $vehicle_title, $post_id );
	return $vehicle_title;
}

function wpsd_is_true( $value ) {
	if ( empty( $value )
		|| __( 'off', 'wp-super-dealer' ) === $value
		|| __( 'no', 'wp-super-dealer' ) === $value
		|| __( 'No', 'wp-super-dealer' ) === $value
		|| __( 'false', 'wp-super-dealer' ) === $value
		|| false === $value ) 
	{
		return false;
	} else {
		return true;
	}
}

function wpsd_no_search_results( $searched = '' ) {
	global $wpsd_options;

	if ( isset( $wpsd_options['no_results'] ) ) {
		$no_results = $wpsd_options['no_results'];
	} else {
		$no_results = __( 'Sorry, but nothing matched your search criteria. Please try using a broader search selection.', 'wp-super-dealer' );
	}
	if ( isset( $wpsd_options['no_results_title'] ) ) {
		$no_results_title = $wpsd_options['no_results_title'];
	} else {
		$no_results_title = __( 'No vehicle results found', 'wp-super-dealer' );
	}
	
    $html = '';
    $html .= '<div class="wpsd-no-results-msg-wrap">';
        $html .= '<div class="wpsd-no-results-msg">';
            $html .= $no_results_title;
        $html .= '</div>';
        $html .= '<p class="sorry">' . $no_results . '</p>';
    $html .= '</div>';
    $html = apply_filters( 'wpsd_no_results_filter', $html );

	return $html;
}

function wpsd_get_the_content_with_formatting( $more_link_text = '(more...)', $stripteaser = 0, $more_file = '' ) {
	global $wpsd_options;
	$content = get_the_content( $more_link_text, $stripteaser, $more_file );
	//= Do not apply filters to content
	remove_filter( 'the_content', 'wpsd_filter_vehicle_content', 20 );
	remove_filter( 'the_excerpt', 'wpsd_filter_vehicle_content', 20 );
	$content = apply_filters( 'the_content', $content );
	$content = str_replace( ']]>', ']]&gt;', $content );
	return $content;
}

function wpsd_facebook_meta() {
	$post_id = get_the_ID();
	$title = get_vehicle_title( $post_id );
	$url = get_permalink( $post_id );
	$image = wpsd_main_photo( $post_id );
	echo '
		<meta property="og:title" content="' . esc_attr( $title ) . '"/>
		<meta property="og:url" content="' . esc_url( $url ) . '"/>
		<meta property="og:image" content="' . esc_url( $image ) . '"/>';
}

function wpsd_main_photo( $post_id, $type = 'full' ) {
	global $wpsd_options;
    
	if ( defined( 'WPSD_LINK_MAIN_IMAGE' ) ) {
		$wpsd_options['link_main_image'] = 'Yes';
	}

	if ( isset( $wpsd_options['link_main_image'] ) ) {
		if ( $wpsd_options['link_main_image'] == 'Yes' ) {
			$image_links = get_post_meta( $post_id, '_image_urls', true );
			$image_links_array = explode( ',', $image_links );
			if ( is_array( $image_links_array ) ) {
				$main_photo = trim( $image_links_array[0] );
			} else {
				$main_photo = trim( $image_links );
			}
			if ( ! empty( $main_photo ) ) {
				return $main_photo;
			}
		}
	}

	remove_filter( 'wp_get_attachment_url', 'wpsd_stop_get_attachment_url', 10, 2 );
	$main_photo = '';
	if ( 'thumbnail' === $type ) {
		$main_photo = wp_get_attachment_thumb_url( get_post_thumbnail_id( $post_id ) );
	} else {
        $main_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), $type );
        if ( ! is_wp_error( $main_image ) ) {
            if ( isset( $main_image[0] ) ) {
                $main_photo = $main_image[0];
            }
        }
	}
	
	if ( empty( $main_photo ) && isset( $wpsd_options['error_image'] ) ) {
		$main_photo = $wpsd_options['error_image'];
	}
    $main_photo = apply_filters( 'wpsd_main_photo_url_filter', $main_photo, $post_id, $type );

	return $main_photo;
}

function wpsd_thumbnails( $post_id ) {
	// Thumbnails
	$order = '';
	$thumbnails = get_children( array( 'post_parent' => $post_id, 'post_type' => 'attachment', 'post_mime_type' =>'image', 'orderby' => 'menu_order ID', 'order' => $order ) );
	$thumbnails = apply_filters( 'wpsd_get_thumbnails', $thumbnails, $post_id, 'media' );
	$cnt = 0;

	$array = array();
	
	foreach( $thumbnails as $thumbnail ) {
		$guid = wp_get_attachment_url( $thumbnail->ID );
		if ( ! empty( $guid ) ) {
			$array[ $cnt ] = $guid;
			++$cnt;
		}
	}

	// Check if vehicle has a list of photo urls that arent part of the normal gallery
	$image_list = get_post_meta( $post_id, '_image_urls', true );
	if ( ! empty( $image_list ) ) {
		$thumbnails = explode( ',', $image_list );

		$thumbnails = apply_filters( 'wpsd_get_thumbnails', $thumbnails, $post_id, 'linked' );
		foreach( $thumbnails as $thumbnail ) {
			//= $pos = strpos($thumbnail,'.jpg');
			//= We need a different way to check if it really is an image file
			//= maybe check file ext array?
			$thumbnail = trim( $thumbnail );
			//= prevent empty images from being added
			if ( empty( $thumbnail ) ) {
				continue;	
			}
			if ( $cnt > -1 ) {
				$array[ $cnt ] = trim( $thumbnail );
			}
			++$cnt;
		}
	}

	// End Thumbnails
	return $array;
}

/**
 * This function/filter was added to help stop theme's from outputting the vehicle's featured image
*/
function wpsd_stop_get_attachment_url( $url, $post_id ) {
    if ( defined( 'WPSD_ALLOW_GET_ATTACHMENT_URL' ) && WPSD_ALLOW_GET_ATTACHMENT_URL ) {
		return $url;
	}
	if ( ! is_single() ) {
		return $url;
	}
	global $post;
	if ( ! is_object( $post ) ) {
		return $url;
	}

    $thumbnail_id = get_post_thumbnail_id( $post );
    if ( $post_id !== $thumbnail_id ) {
        return $url;
    }

	$post_id = $post->ID;
	$post_type = get_post_type( $post_id );
	if ( WPSD_POST_TYPE !== $post_type ) {
		return $url;
	}
   return '';
}
add_filter( 'wp_get_attachment_url', 'wpsd_stop_get_attachment_url', 10, 2 );

function wpsd_get_default_description() {
	global $wpsd_options;
	$description = $wpsd_options['default_description'];
	return $description;
}

function wpsd_get_vehicle( $post_id ) {
	global $wpsd_options;
	$vehicle = array();

	$vehicle['title'] = wpsd_esc_data( get_vehicle_title( $post_id ) );
	$vehicle['link'] = get_permalink( $post_id );
    $vehicle['type'] = wpsd_get_vehicle_type( $post_id );
    
    $tags = get_wpsd_term( $post_id, 'vehicle_tags' );
    $vehicle['vehicle_tags'] = $tags;
    
	//= get full post
	$post = get_post( $post_id );
	if ( is_object( $post ) ) {
		$post_arr = (array)$post;
		$vehicle['post'] = $post_arr;
		$content = $post->post_content;
		$content = str_replace( ']]>', ']]&gt;', $content );
		$content = str_replace( '"', '&quot;', $content );
        $content = wpautop( $content );
    }

	if ( empty( $content ) ) {
		$content = wpsd_get_default_description( $post_id );
	}
	$vehicle['description'] = wp_kses_post( $content );
	$vehicle['main-image'] = esc_url( wpsd_main_photo( $post_id ) );
	$vehicle['thumbnails'] = wpsd_thumbnails( $post_id );
	$vehicle['thumbnails_list'] = implode( ',', $vehicle['thumbnails'] );
    if ( isset( $meta['_image_urls'] ) ) {
        if ( isset( $meta['_image_urls'][0] ) ) {
            $vehicle['image_urls'] = esc_url( $meta['_image_urls'][0] );
        }
    }
	
	$meta = get_post_meta( $post_id );
    if ( isset( $meta['_options'] ) ) {
        if ( is_array( $meta['_options'] ) ) {
            $vehicle['options'] = wpsd_esc_data( $meta['_options'][0] );
        } else {
            $vehicle['options'] = wpsd_esc_data( $meta['_options'] );
        }
    }

	$specs = wpsd_get_all_specifications();

    if ( defined( 'WPSD_SEND_META' ) ) {
        $vehicle['meta'] = $meta;
    }
    
	foreach( $specs as $spec=>$options ) {
		$slug = sanitize_title( $spec );
		$_slug = str_replace( '-', '_', $slug );
		if ( isset( $meta[ '_' . $_slug ] ) ) {
			$vehicle[ $_slug ] = wpsd_esc_data( $meta[ '_' . $_slug][0] );
		}
		if ( isset( $meta[ $_slug ] ) ) {
			$vehicle[ $_slug ] = wpsd_esc_data( $meta[ $_slug][0] );
		}
	}

	//= add location info
	$vehicle['location-details'] = get_vehicle_location( $post_id );
    if ( is_array( $vehicle['location-details'] ) && isset( $vehicle['location-details']['slug'] ) ) {
        $vehicle['location-slug'] = wpsd_esc_data( $vehicle['location-details']['slug'] );
        $vehicle['location'] = wpsd_esc_data( $vehicle['location-details']['name'] );
    } else {
        $vehicle['location-slug'] = __( 'default', 'wp-super-dealer' );
        $vehicle['location'] = __( 'Default', 'wp-super-dealer' );
    }

	if ( isset( $vehicle['location-details']['hide_prices'] )
		&& ( wpsd_is_true( $vehicle['location-details']['hide_prices'] ) ) )
	{
		if ( isset( $wpsd_options['no_price'] )
		   && ! empty( $wpsd_options['no_price'] ) ) 
		{
			$vehicle['price'] = wpsd_esc_data( $wpsd_options['no_price'] );
		}
	} else {
	
		if ( isset( $vehicle['location-details']['add_to_price'] )
			&& ( wpsd_is_true( $vehicle['location-details']['add_to_price'] ) )
		   && isset( $vehicle['price'] ) )
		{
			$vehicle['price'] = wpsd_esc_data( $vehicle['price'] + $vehicle['location-details']['add_to_price'] );
		}
	}
    
    //= Let's make sure the vehicle year is also just in "year" because people will expect it
    if ( isset( $vehicle['vehicle_year'] ) ) {
        $vehicle['year'] = wpsd_esc_data( $vehicle['vehicle_year'] );
    }
	
    $vehicle = apply_filters( 'wpsd_get_vehicle_filter', $vehicle, $post_id );
	
	return $vehicle;
}

function wpsd_esc_data( $data ) {
    $original_data = $data;
    $data = esc_html( $data );
    $data = apply_filters( 'wpsd_esc_data_filter', $data, $original_data );
    return $data;
}

function get_wpsd_term( $post_id, $tax ) {
	$name = '';
	$terms = wp_get_object_terms( $post_id, $tax );
	if ( is_wp_error( $terms ) ) {
		return '';
	}
	$cnt = 1;
	foreach( $terms as $term_item ) {
		if ( $cnt === 1 ) {
			$name = $term_item->name;
		} else {
			$name .= ', ' . $term_item->name;
		}
		++$cnt;
	}	
	return $name;
}

function wpsd_tag_filter( $post_id, $content, $vehicle = false ) {
	if ( false == $vehicle ) {
		$vehicle = wpsd_get_vehicle( $post_id );
	}

	if ( isset( $vehicle['title'] ) ) {
		$content = str_replace( '{title}', $vehicle['title'], $content );
	} else {
		$content = str_replace( '{title}', '', $content );
	}

	if ( is_array( $vehicle ) ) {
		foreach( $vehicle as $item=>$value ) {
            if ( 'options' === $item ) {
                if ( function_exists( 'wpsdtm_filter_html' ) ) {
                    continue;
                }
            }
            $value = str_replace( "'", "&#39;", $value );
			if ( ! is_array( $value ) ) {
				$content = str_replace( '{' . $item . '}', $value, $content );
			}
		}
	}

	$specs = wpsd_get_all_specifications();
	$all_specs = wpsd_get_all_specifications( false, false, true, true );

	foreach( $specs as $spec=>$options ) {
		$slug = sanitize_title( $spec );
		$_slug = str_replace( '-', '_', $slug );

		if ( isset( $vehicle[ $_slug ] ) ) {
			if ( is_array( $vehicle[ $_slug ] ) ) {
				foreach( $vehicle[ $_slug ] as $sub_key=>$sub_value ) {
                    $sub_slug = sanitize_title( $sub_key );
                    $_sub_slug = str_replace( '-', '_', $sub_slug );
                    $sub_value = str_replace( "'", "&#39;", $sub_value );
                    $content = str_replace( '{' . $_slug . '_' . $_sub_slug . '}', $sub_value, $content );
                    $content = str_replace( '{' . $_sub_slug . '}', $sub_value, $content );
                }
            } else {
                $vehicle[ $_slug ] = str_replace( "'", "&#39;", $vehicle[ $_slug ] );
                $content = str_replace( '{' . $_slug . '}', $vehicle[ $_slug ], $content );                
            }
			
			if ( isset( $all_specs[ $_slug ] ) && is_array( $all_specs[ $_slug ] ) ) {
				foreach ( $all_specs[ $_slug ] as $sub_key=>$sub_value ) {
					$content = str_replace( '{' . $_slug . '_' . $sub_key . '}', $sub_value, $content );
				}
			}
			
		} else {
			$content = str_replace( '{' . $_slug . '}', '', $content );			
		}
	}

	$tags = '<pre>' . print_r( $vehicle, true ) . '</pre>';

	$content = str_replace( '{tags}', $tags, $content );

    if ( ! function_exists( 'wpsdtm_filter_html' ) ) {
        $re = '/{.*?}/m';
        preg_match_all( $re, $content, $matches, PREG_SET_ORDER, 0 );
        if ( is_array( $matches ) && 0 < count( $matches ) ) {
            foreach( $matches as $match ) {
                if ( 20 > strlen( $match[0] ) 
                    && 2 < strlen( $match[0] ) 
                    && false === strpos( $match[0], ';' )
                    && false === strpos( $match[0], ':' ) )
                {
                    $content = str_replace( $match[0], '', $content );
                }
            }
        }
    }

	return $content;
}

//= TO DO Future: option to turn off exceprt filter by type - ie. single, archive, etc.
add_filter( 'the_content', 'wpsd_filter_vehicle_content', 20 );
if ( defined( 'WPSD_FILTER_EXCERPT' ) ) {
	add_filter( 'the_excerpt', 'wpsd_filter_vehicle_content', 20 );
}
function wpsd_filter_vehicle_content( $content ) {
	if ( ! is_admin() ) {
		$post_id = get_the_ID();
		$post_type = get_post_type( $post_id );
		//= If it's a vehicle post type then keep going
		if ( WPSD_POST_TYPE === $post_type ) {
			if ( is_single() ) {
				//= If it's single then it's the single vehicle page
				$content = wpsd_vdp_item( $post_id, $content );
			}
			if ( is_post_type_archive( WPSD_POST_TYPE ) || is_tax( 'vehicle_year' ) || is_tax( 'vehicle_make' ) || is_tax( 'vehicle_model' ) || is_tax( 'vehicle_condition' ) || is_tax( 'vehicle_body_style' ) || is_tax( 'vehicle_location' ) ) {
				// If it's an archive page then filter the loop
				$content = wpsd_srp_item( $post_id, $content );
			}
		}
		//= If it's a search page then it needs to be filtered
		if ( is_search() ) {
			//= Handle search page
			if ( $post_type == WPSD_POST_TYPE ) {
				if ( isset( $_GET[ WPSD_POST_TYPE ] ) ) {
					if ( empty( $content ) ) {
						$content = wpsd_srp_item( $post_id, $content );
					}
				}
			}
		}
		//= Returns the content.
	}
    return $content;
}
?>