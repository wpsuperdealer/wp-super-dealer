<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_calculator_form( $atts ) {
	global $wpsd_options;
	$default_atts = array(
		'price' => __( '25000', 'wp-super-dealer' ),
        'downpayment' => __( '1500', 'wp-super-dealer' ),
		'apr' => __( '10', 'wp-super-dealer' ),
		'term' => __( '60', 'wp-super-dealer' ),
		'disclaimer1' => __( 'This is not an offer for credit nor a quote.', 'wp-super-dealer' ),
		'disclaimer2' => __( 'This calculator provides an estimated monthly payment. Your actual payment may vary based upon your specific loan and final purchase price.', 'wp-super-dealer' )
	 );
	$atts = wp_parse_args( $atts, $default_atts );

    $price = '';
    
	if ( is_single() ) {
		global $post;
		if ( WPSD_POST_TYPE === $post->post_type ) {
			$price = get_post_meta( $post->ID, '_price', true );
		}
	}

	if ( empty( $price ) ) {
		if ( isset( $_GET['xP'] ) ) {
			$price = $_GET['xP'];
		} else {
			$price = '25000';
		}
	}

	if ( ! empty( $price ) ) {
		$atts['price'] = $price;
	}
	if ( empty( $atts['down'] ) ) {
		$atts['down'] = '0';
	}
    
    $amount_financed = $atts['price'] - $atts['down'];
	
	if ( isset( $wpsd_options['price_before'] ) ) {
		$currency_symbol = $wpsd_options['price_before'];
	} else {
		$currency_symbol = "$";
	}
	if ( isset($wpsd_options['price_after'] ) ) {
		$currency_symbol_after = $wpsd_options['price_after'];
		if ( ! empty( $currency_symbol_after ) ) {
			$currency_symbol = "";
		}
	} else {
		$currency_symbol_after = "";
	}

	$form = '
    <form name="calc" action="" class="wpsd-calculator">
        <table align="center" class="calc_table">
            <tr>
                <td>' . __( 'Estimated Price', 'wp-super-dealer' ) . ':</td>
                <td>' . esc_html( $currency_symbol ) . '<input class="calc_box wpsd-calc-price" name="pv" type="text" size="5" maxlength="10" value="' . esc_attr( $atts['price'] ) . '" />' . esc_html( $currency_symbol_after ) . '
                </td>
            </tr>
            <tr>
                <td>' . __( 'Down Payment', 'wp-super-dealer' ) . ':</td>
                <td>' . esc_html( $currency_symbol ) . '<input class="calc_box wpsd-calc-down-payment" name="down" type="text" size="5" maxlength="10" value="' . esc_attr( $atts['down'] ) . '" />' . esc_html( $currency_symbol_after ) . '
                </td>
            </tr>
            
            <tr>
                <td>' . __( 'Amount Financed', 'wp-super-dealer' ) . ':</td>
                <td>' . esc_html( $currency_symbol ) . '<input class="calc_box wpsd-calc-amount-financed" name="total" disabled="disabled" type="text" size="5" maxlength="10" value="' . esc_attr( $amount_financed ) . '" />' . esc_html( $currency_symbol_after ) . '
                </td>
            </tr>
            
            <tr> 
                <td>' . __( 'Annual Percentage Rate', 'wp-super-dealer' ). ':</td>
                <td><input class="calc_box wpsd-calc-pr" name="rate" type="text" size="2" maxlength="6" value="' . esc_attr( $atts['apr'] ) . '" />%</td>
			</tr>
            <tr> 
                <td>' . __( 'Total Number of Payments', 'wp-super-dealer' ) . ':</td>
                <td>
                	<input class="calc_box wpsd-calc-payments" type="text" size="2" maxlength="4" name="numPmtYr" value="' . esc_attr( $atts['term'] ) . '" />
					<input type="hidden" size="2" maxlength="2" name="numYr" value="5" />
	            </td>
            </tr>
        </table>
        <div align="center"></div>
        <p align="center">
            <input type="button" class="calc_btn wpsd-calc-button" value="' . __( 'Calculate', 'wp-super-dealer' ) . '" onClick="returnPayment()" />
            <input type="button" class="calc_btn wpsd-calc-reset" value="' . __( 'Reset', 'wp-super-dealer' ) . '" onClick="this.form.reset()" />
            <br />
            ' . esc_html( $atts['disclaimer1'] ) . '
		</p>
        <div align="center"> 
            <b>' . __( '*Estimated Monthly Payment', 'wp-super-dealer') . ':</b>
            <br />
            <input type="text" size="7" maxlength="7" class="wpsd-calc-payment" name="pmt" id="calc_pmt" />
        </div>
        <div align="center" class="calc_text">
            <hr width="100%">
            ' . esc_html( $atts['disclaimer2'] ) . '
        </div>
    </form>';
    
    $elements = array(
        'form' => array(
            'name' => array(),
            'action' => array(),
            'class' => array(),
        ),
        'div' => array(
            'id' => array(),
            'class' => array(),
            'align' => array(),
        ),
        'table' => array(
            'id' => array(),
            'class' => array(),
            'align' => array(),
        ),
        'tr' => array(),
        'td' => array(),
        'p' => array(
            'align' => array(),
            'class' => array(),
        ),
        'b' => array(),
        'br' => array(),
        'hr' => array(
            'width' => array(),
        ),
        'input' => array(
            'type' => array(),
            'class' => array(),
            'value' => array(),
            'onClick' => array(),
            'size' => array(),
            'maxlength' => array(),
            'name' => array(),
            'id' => array(),
            'disabled' => array(),
        ),
    );
    $elements = apply_filters( 'wpsd_nav_esc_elements_filter', $elements );

    //= lock it down to just our elements
    $form = wp_kses( $form, $elements );

	return $form;
}
?>