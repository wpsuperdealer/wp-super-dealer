<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function get_active_price_fields() {
	$wpsd_vehicle_options = wpsd_get_vehicle_options();
	$price_fields = get_price_fields();
	$global_specs = $wpsd_vehicle_options['global_specs'];
	$active_fields = array();
	foreach( $price_fields as $price_field=>$data ) {
		if ( isset( $global_specs[ $price_field ] ) ) {
			if ( isset( $global_specs[ $price_field ]['disabled'] ) ) {
				$disabled = $global_specs[ $price_field ]['disabled'];
			} else {
				$disabled = false;
			}
			if ( 'true' === $disabled || true === $disabled ) {
				continue;
			}
			$active_fields[ $price_field ] = $data;
		}
	}
	return $active_fields;
}

function get_price_fields() {
	$wpsd_options = wpsd_get_vehicle_options();
	$global_specs = $wpsd_options['global_specs'];

	$fields = array(
		__( 'retail-price', 'wp-super-dealer' ) => array(
			'label' => __( 'Retail Price', 'wp-super-dealer' ),
			'taxonomy' => false,
			'allow_taxonomy' => false,
			'hide_edit' => true,
		),
		__( 'rebates', 'wp-super-dealer' ) => array(
			'label' => __( 'Rebates', 'wp-super-dealer' ),
			'taxonomy' => false,
			'allow_taxonomy' => false,
			'hide_edit' => true,
		),
		__( 'discount', 'wp-super-dealer' ) => array(
			'label' => __( 'Discount', 'wp-super-dealer' ),
			'taxonomy' => false,
			'allow_taxonomy' => false,
			'hide_edit' => true,
		),
		__( 'price', 'wp-super-dealer' ) => array(
			'label' => __( 'Price', 'wp-super-dealer' ),
			'taxonomy' => false,
			'allow_taxonomy' => false,
			'hide_edit' => true,
			'locked' => true,
		),
	);
	
	$fields = apply_filters( 'wpsd_price_fields_filter', $fields );
	return $fields;
}

/**
 * Function to return vehicle price
 *
 * @todo
 *     - Ability to return price parts as array for templating
 */
function wpsd_get_vehicle_price( $post_id ) {
	global $wpsd_options;
	$is_sold = get_post_meta( $post_id, 'sold', true );
	$vehicle = wpsd_get_vehicle( $post_id );
	$html = '';
	$html .= '<div class="vehicle-price-box">';

	if ( $is_sold == __( 'Yes', 'wp-super-dealer' ) ) {
		$html .= '<div class="vehicle-sold">';
			$html .= __( 'SOLD', 'wp-super-dealer' );
		$html .= '</div>';
	} else {
		$price_fields = get_active_price_fields();
		foreach( $price_fields as $price_field=>$options ) {
			$slug = sanitize_title( $price_field );
			$_slug = str_replace( '-', '_', $slug );
			$price_value = '';
			if ( isset( $vehicle[ $_slug ] ) ) {
				$price_value = $vehicle[ $_slug ];
			}
			if ( empty( $price_value ) || '0' === $price_value ) {
				if ( 'price' === $_slug ) {
					$html .= '<div class="price-item no-price price-item-' . esc_attr( $_slug ) . '">';
						$html .= '<div class="price-item-label">';
							$html .= esc_html( $options['label'] );
						$html .= '</div>';
						$html .= '<div class="price-item-value-wrap">';
							$html .= esc_html( $wpsd_options['no_price'] );
						$html .= '</div>';
					$html .= '</div>';
				}
				continue;
			}
			$html .= '<div class="price-item price-item-' . esc_attr( $_slug ) . '">';
				$html .= '<div class="price-item-label">';
					$html .= esc_html( $options['label'] );
				$html .= '</div>';
				$html .= '<div class="price-item-value-wrap">';
					$html .= '<span class="price-before">';
						$html .= esc_html( $wpsd_options['price_before'] );
					$html .= '</span>';
			
					$html .= '<span class="price-item-value price-item-value-' . esc_attr( $_slug ) . '"">';
						$html .= esc_html( wpsd_price_format_func( $price_value ) );
					$html .= '</span>';
			
					$html .= '<span class="price-after">';
						$html .= esc_html( $wpsd_options['price_after'] );
					$html .= '</span>';
				$html .= '</div>';
			$html .= '</div>';
		}
	}

	$html .= '</div>';
	
	if ( '<div class="vehicle-price-box"></div>' === $html ) {
		//= no price fields
	}
	
    $html = apply_filters( 'wpsd_get_vehicle_price_filter', $html, $post_id );
    
	return $html;
}

add_filter( 'wpsd_price_filter', 'wpsd_price_format_func', 10, 1 );
function wpsd_price_format_func( $price ) {
	$price = apply_filters( 'wpsd_price_format', $price );
	return $price;	
}
?>