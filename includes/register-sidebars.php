<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Register sidebar areas on single vehicle page
 *
 */
function wpsd_register_sidebars() {

	register_sidebar( array(
		'name'=> __( 'Single Vehicle Before', 'wp-super-dealer' ),
		'id' => 'single_vehicle_before',
		'before_widget' => '<div id="%1$s" class="vdp-widget vdp-widget-before %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="offscreen">',
		'after_title' => '</h2>',
	));

	register_sidebar( array(
		'name'=> __( 'Single Vehicle Summary', 'wp-super-dealer' ),
		'id' => 'single_vehicle_summary',
		'before_widget' => '<div id="%1$s" class="vdp-widget vdp-widget-summary %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="offscreen">',
		'after_title' => '</h2>',
	));

	register_sidebar( array(
		'name'=> __( 'Single Vehicle Middle', 'wp-super-dealer' ),
		'id' => 'single_vehicle_middle',
		'before_widget' => '<div id="%1$s" class="vdp-widget vdp-widget-middle %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="offscreen">',
		'after_title' => '</h2>',
	));

	register_sidebar( array(
		'name'=> __( 'Single Vehicle After', 'wp-super-dealer' ),
		'id' => 'single_vehicle_after',
		'before_widget' => '<div id="%1$s" class="vdp-widget vdp-widget-after %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="offscreen">',
		'after_title' => '</h2>',
	));

}
?>