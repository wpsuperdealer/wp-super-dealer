<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_history_report_template( $data ) {
    if ( is_object( $data ) ) {
        $array = (array)$data;
    } else {
        $array = $data;
    }

    if ( is_object( $array ) ) {
        $array = (array)$array;
    }
    $default_array = array(
        'vin' => '123',
        'id' => '181328755669',
        'date' => date( 'Y-m-d h:i', time() ),
        'success' => 0,
        'error' => '',
        'attributes' => array(),
        'titles' => array(),
        'jsi' => array(),
        'lies' => array(),
        'thefts' => array(),
        'accidents' => array(),
        'salvage' => array(),
        'sale' => array(),
        'checks' => array(),
    );
    $array = wp_parse_args( $array, $default_array );

    $html = '';

    $html .= '<div class="wpsd-vehicle-history-report-wrap">';
        $html .= '<div class="wpsd-vehicle-history-report">';
            $html .= '<div class="wpsd-close-report">';
                $html .= '<i>';
                    $html .= 'X';
                $html .= '</i>';
                $html .= '<span>';
                    $html .= __( 'Close Report', 'wp-super-dealer' );
                $html .= '</spab>';
            $html .= '</div>';
            $html .= '
            <div class="wpsd-vhr-header">
                <div class="wpsd-vhr-section">
                    <img class="wpsd-vhr-logo" src="' . esc_url( WPSD_PATH ) . '/images/report_logo.png">
                    <span class="wpsd-vhr-title">' . __( 'Vehicle History Report', 'wp-super-dealer' ) . '</span>
                    <div class="wpsd-clear"></div>

                    <table class="wpsd-vhr-table">
                        <tbody>
                            <tr>
                                <td>
                                    <span class="wpsd-vhr-sub-title"><b>' . __( 'VIN:' , 'wpsd-super-dealer' ) . '</b> ' . esc_html( $array['vin'] ) . '</span><br>
                                </td>
                                <td class="wpsd-vhr-info">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td><b>' . __( 'Report ID: ', 'wp-super-dealer' ) . '</b></td>
                                                <td><span id="report_id">#' . esc_html( $array['id'] ) . '</span></td>
                                            </tr>
                                            <tr>
                                                <td><b>' . __( 'Generated: ', 'wp-super-dealer' ) . '</b></td>
                                                <td><span id="report_time">' . esc_html( $array['date'] ) . '</span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="wpsd-clear"></div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="wpsd-clear"></div>
                </div>
                <div class="wpsd-clear"></div>
            </div>
            <div class="wpsd-clear"></div>
            ';

            $sections = array(
                'attributes', 'titles', 'jsi', 'lies', 'thefts', 'accidents', 'salvage', 'sales', 'checks', 
            );
            foreach( $sections as $section ) {
                if ( $array[ $section ] ) {
                    if ( function_exists( 'wpsd_get_history_section_' . $section ) ) {
                        $html .= call_user_func( 'wpsd_get_history_section_' . $section, $array );
                    } else {
                        $html .= '<div>';
                            $html .= __( 'Section function not found: wpsd_get_history_section_', 'wp-super-dealer' ) . print_r( $section, true );
                        $html .= '</div>';
                    }
                }
            }
        $html .= '</div>'; //= end report
    $html .= '</div>'; //= end wrap

    return $html;
}

function wpsd_get_history_section_attributes( $atts ) {
    $html = '';
    if ( isset( $atts['attributes'] ) && is_object( $atts['attributes'] ) ) {
        $html .= '<div class="wpsd-report-section wpsd-report-section-attributes">';
            $html .= '<div class="wpsd-report-section-header">';
                $html .= '<div class="wpsd-report-section-title">';
                    $html .= __( 'Vehicle Specifications', 'wp-super-dealer' );
                $html .= '</div>';
                $html .= '<div class="wpsd-report-section-summary">';
                    $html .= __( 'This section lists basic vehicle details encoded by the VIN.', 'wp-super-dealer' );
                $html .= '</div>';
            $html .= '</div>';
            foreach( $atts['attributes'] as $key=>$value ) {
                $html .= '<div class="wpsd-report-item wpsd-report-item-attributes">';
                    $html .= '<div class="wpsd-report-item-label">';
                        $html .= esc_html( $key );
                    $html .= '</div>';
                    $html .= '<div class="wpsd-report-item-value">';
                        $html .= esc_html( $value );
                    $html .= '</div>';
                $html .= '</div>';
            }
        $html .= '</div>';
    }

    return $html;
}

function wpsd_get_history_section_titles( $atts ) {
    $html = '';
    $html .= '<div class="wpsd-report-section wpsd-report-section-titles">';
        $html .= '<div class="wpsd-report-section-header">';
            $html .= '<div class="wpsd-report-section-title">';
                $html .= __( 'Title Records', 'wp-super-dealer' );
            $html .= '</div>';
            $html .= '<div class="wpsd-report-section-summary">';
                $html .= __( 'This section lists title records.', 'wp-super-dealer' );
            $html .= '</div>';
            $html .= '<div class="wpsd-section-source">';
                $html .= __( 'Source: NMVTIS', 'wp-super-dealer' );
            $html .= '</div>';
        $html .= '</div>';
        $html .= '<table class="wpsd-report-item-table">';
            $html .= '<tr class="wpsd-report-item-table-row">';
                $html .= '<th>';
                    $html .= __( 'Date', 'wp-super-dealer' );
                $html .= '</th>';
                $html .= '<th>';
                    $html .= __( 'State of Tite', 'wp-super-dealer' );
                $html .= '</th>';
                $html .= '<th>';
                    $html .= __( 'Type', 'wp-super-dealer' );
                $html .= '</th>';
                $html .= '<th>';
                    $html .= __( 'Mileage', 'wp-super-dealer' );
                $html .= '</th>';
                $html .= '<th>';
                    $html .= __( 'VIN', 'wp-super-dealer' );
                $html .= '</th>';
            $html .= '</tr>';
            if ( isset( $atts['titles'] ) && is_array( $atts['titles'] ) ) {
                foreach( $atts['titles'] as $key=>$value ) {
                    $html .= '<tr class="wpsd-report-item-table-row">';
                        $html .= '<td>';
                            $html .= esc_html( $value->date );
                        $html .= '</td>';
                        $html .= '<td>';
                            $html .= esc_html( $value->state );
                        $html .= '</td>';
                        $html .= '<td>';
                            $html .= esc_html( $value->current );
                        $html .= '</td>';
                        $html .= '<td>';
                            $html .= esc_html( $value->meter . ' ' . $value->meter_unit );
                        $html .= '</td>';
                        $html .= '<td>';
                            $html .= esc_html( $atts['vin'] );
                        $html .= '</td>';
                    $html .= '</tr>';
                }
            } else {
                $html .= '<tr>';
                    $html .= '<td colspan="3" class="wpsd-report-section-no-record">';
                        $html .= __( 'No records found!', 'wp-super-dealer' );
                    $html .= '</td>';
                $html .= '</tr>';
            }
        $html .= '</table>';
    $html .= '</div>';

    return $html;
}

function wpsd_get_history_section_jsi( $atts ) {
    $html = '';

    $html .= '<div class="wpsd-report-section wpsd-report-section-attributes">';
        $html .= '<div class="wpsd-report-section-header">';
            $html .= '<div class="wpsd-report-section-title">';
                $html .= __( 'Junk / Salvage / Insurance Records', 'wp-super-dealer' );
            $html .= '</div>';
            $html .= '<div class="wpsd-report-section-summary">';
                $html .= __( 'This section lists junk, salvage, and insurance records (if any).', 'wp-super-dealer' );
            $html .= '</div>';
            $html .= '<div class="wpsd-section-source">';
                $html .= __( 'Source: NMVTIS', 'wp-super-dealer' );
            $html .= '</div>';
        $html .= '</div>';
        $html .= '<table class="wpsd-report-item-table">';
            $html .= '<tr class="wpsd-report-item-table-row">';
                $html .= '<th>';
                    $html .= __( 'Date', 'wp-super-dealer' );
                $html .= '</th>';
                $html .= '<th>';
                    $html .= __( 'Reporting Entity', 'wp-super-dealer' );
                $html .= '</th>';
                $html .= '<th>';
                    $html .= __( 'Details', 'wp-super-dealer' );
                $html .= '</th>';
            $html .= '</tr>';
            if ( isset( $atts['jsi'] ) && is_array( $atts['jsi'] ) ) {
                foreach( $atts['jsi'] as $key=>$value ) {
                    $html .= '<tr class="wpsd-report-item-table-row">';
                        $html .= '<td>';
                            $html .= esc_html( $value->date );
                        $html .= '</td>';
                        $html .= '<td>';
                            $html .= esc_html( $value->brander_name );
                            $html .= '<br />';
                            $html .= esc_html( $value->brander_city ) . ', ' . esc_html( $value->brander_state );
                            if ( ! empty( $value->brander_phone ) ) {
                                $html .= '<br />';
                                $html .= esc_html( $value->brander_phone );
                            }
                            if ( ! empty( $value->brander_email ) ) {
                                $html .= '<br />';
                                $html .= esc_html( $value->brander_email );
                            }
                        $html .= '</td>';
                        $html .= '<td>';
                            $html .= __( 'Damage Type: ', 'wp-super-dealer' ) . esc_html( $value->record_type );
                            if ( ! empty( $value->brander_email ) ) {
                                $html .= '<br />';
                                $html .= __( 'Disposition: ', 'wp-super-dealer' ) . esc_html( $value->vehicle_disposition );
                            }
                            if ( ! empty( $value->intended_for_export ) ) {
                                $html .= '<br />';
                                $html .= __( 'Intended for Export: ', 'wp-super-dealer' ) . esc_html( $value->intended_for_export );
                            }
                        $html .= '</td>';
                    $html .= '</tr>';
                }
            } else {
                $html .= '<tr>';
                    $html .= '<td colspan="3" class="wpsd-report-section-no-record">';
                        $html .= __( 'No records found!', 'wp-super-dealer' );
                    $html .= '</td>';
                $html .= '</tr>';
            }
        $html .= '</table>';
    $html .= '</div>';

    return $html;
}

function wpsd_get_history_section_thefts( $atts ) {
    $html = '';

    $html .= '<div class="wpsd-report-section wpsd-report-section-thefts">';
        $html .= '<div class="wpsd-report-section-header">';
            $html .= '<div class="wpsd-report-section-title">';
                $html .= __( 'Theft Records', 'wp-super-dealer' );
            $html .= '</div>';
            $html .= '<div class="wpsd-report-section-summary">';
                $html .= __( 'This section lists active theft and theft recovery records for this VIN.', 'wp-super-dealer' );
            $html .= '</div>';
            $html .= '<div class="wpsd-section-source">';
                $html .= __( 'Source: VA', 'wp-super-dealer' );
            $html .= '</div>';
        $html .= '</div>';
        $html .= '<table class="wpsd-report-item-table">';
            if ( isset( $atts['thefts'] ) && is_array( $atts['thefts'] ) ) {
                foreach( $atts['thefts'] as $key=>$value ) {
                    $html .= '<tr>';
                        $html .= '<th colspan="3">';
                            $html .= __( 'Theft Report - ', 'wp-super-dealer' ) . esc_html( $value->theft_status );
                        $html .= '</th>';
                    $html .= '</tr>';
                    $html .= '<tr class="wpsd-report-item-table-row">';
                        $html .= '<td colspan="3" class="wpsd-report-theft-record">';
                            if ( is_object( $value ) ) {
                                foreach( $value as $value_key=>$value_value ) {
                                    $html .= '<div class="wpsd-report-theft-item">';
                                        $html .= '<div class="wpsd-report-theft-item-label">';
                                            $label = str_replace( '_', ' ', $value_key );
                                            $label = ucwords( $label );
                                            $html .= esc_html( $label );
                                        $html .= '</div>';
                                        $html .= '<div class="wpsd-report-theft-item-value">';
                                            $html .= esc_html( $value_value );
                                        $html .= '</div>';
                                    $html .= '</div>';
                                } //= end foreach( $value as $value_key=>$value_value ) {
                            }
                        $html .= '</td>';
                    $html .= '</tr>';
                } //= end foreach( $atts['thefts'] as $key=>$value ) {
            } else {
                $html .= '<tr>';
                    $html .= '<td colspan="3" class="wpsd-report-section-no-record">';
                        $html .= __( 'No records found!', 'wp-super-dealer' );
                    $html .= '</td>';
                $html .= '</tr>';
            }
        $html .= '</table>';
    $html .= '</div>';

    return $html;
}

function wpsd_get_history_section_accidents( $atts ) {
    $html = '';

    $html .= '<div class="wpsd-report-section wpsd-report-section-accidents">';
        $html .= '<div class="wpsd-report-section-header">';
            $html .= '<div class="wpsd-report-section-title">';
                $html .= __( 'Accident Records', 'wp-super-dealer' );
            $html .= '</div>';
            $html .= '<div class="wpsd-report-section-summary">';
                $html .= __( 'This section lists accident records from available independent sources.', 'wp-super-dealer' );
            $html .= '</div>';
            $html .= '<div class="wpsd-section-source">';
                $html .= __( 'Source: VA', 'wp-super-dealer' );
            $html .= '</div>';
        $html .= '</div>';
        $html .= '<table class="wpsd-report-item-table">';
            if ( isset( $atts['accidents'] ) && is_array( $atts['accidents'] ) ) {
                foreach( $atts['accidents'] as $key=>$value ) {
                    $html .= '<tr>';
                        $html .= '<th colspan="3">';
                            $html .= __( 'Accident Report - ', 'wp-super-dealer' ) . esc_html( $value->date );
                        $html .= '</th>';
                    $html .= '</tr>';
                    $html .= '<tr class="wpsd-report-item-table-row">';
                        $html .= '<td colspan="3" class="wpsd-report-accident-record">';
                            if ( is_object( $value ) ) {
                                foreach( $value as $value_key=>$value_value ) {
                                    $html .= '<div class="wpsd-report-accident-item">';
                                        $html .= '<div class="wpsd-report-accident-item-label">';
                                            $label = str_replace( '_', ' ', $value_key );
                                            $label = ucwords( $label );
                                            $html .= esc_html( $label );
                                        $html .= '</div>';
                                        $html .= '<div class="wpsd-report-accident-item-value">';
                                            $html .= esc_html( $value_value );
                                        $html .= '</div>';
                                    $html .= '</div>';
                                } //= end foreach( $value as $value_key=>$value_value ) {
                            }
                        $html .= '</td>';
                    $html .= '</tr>';
                } //= end foreach( $atts['accidents'] as $key=>$value ) {
            } else {
                $html .= '<tr>';
                    $html .= '<td colspan="3" class="wpsd-report-section-no-record">';
                        $html .= __( 'No records found!', 'wp-super-dealer' );
                    $html .= '</td>';
                $html .= '</tr>';
            }
        $html .= '</table>';
    $html .= '</div>';

    return $html;
}

function wpsd_get_history_section_salvage( $atts ) {
    $html = '';

    $html .= '<div class="wpsd-report-section wpsd-report-section-salvages">';
        $html .= '<div class="wpsd-report-section-header">';
            $html .= '<div class="wpsd-report-section-title">';
                $html .= __( 'Salvage Records', 'wp-super-dealer' );
            $html .= '</div>';
            $html .= '<div class="wpsd-report-section-summary">';
                $html .= __( 'This section lists salvage records from available independent sources.', 'wp-super-dealer' );
            $html .= '</div>';
            $html .= '<div class="wpsd-section-source">';
                $html .= __( 'Source: VA', 'wp-super-dealer' );
            $html .= '</div>';
        $html .= '</div>';
        $html .= '<table class="wpsd-report-item-table">';
            if ( isset( $atts['salvages'] ) && is_array( $atts['salvages'] ) ) {
                foreach( $atts['salvages'] as $key=>$value ) {
                    $html .= '<tr>';
                        $html .= '<th colspan="3">';
                            $html .= __( 'Salvage Report - ', 'wp-super-dealer' ) . esc_html( $value->date );
                        $html .= '</th>';
                    $html .= '</tr>';
                    $html .= '<tr class="wpsd-report-item-table-row">';
                        $html .= '<td colspan="3" class="wpsd-report-salvage-record">';
                            if ( is_object( $value ) ) {
                                foreach( $value as $value_key=>$value_value ) {
                                    $html .= '<div class="wpsd-report-salvage-item">';
                                        $html .= '<div class="wpsd-report-salvage-item-label">';
                                            $label = str_replace( '_', ' ', $value_key );
                                            $label = ucwords( $label );
                                            $html .= esc_html( $label );
                                        $html .= '</div>';
                                        $html .= '<div class="wpsd-report-salvage-item-value">';
                                            $html .= esc_html( $value_value );
                                        $html .= '</div>';
                                    $html .= '</div>';
                                } //= end foreach( $value as $value_key=>$value_value ) {
                            }
                        $html .= '</td>';
                    $html .= '</tr>';
                } //= end foreach( $atts['salvages'] as $key=>$value ) {
            } else {
                $html .= '<tr>';
                    $html .= '<td colspan="3" class="wpsd-report-section-no-record">';
                        $html .= __( 'No records found!', 'wp-super-dealer' );
                    $html .= '</td>';
                $html .= '</tr>';
            }
        $html .= '</table>';
    $html .= '</div>';

    return $html;
}

function wpsd_get_history_section_sales( $atts ) {
    $html = '';

    $html .= '<div class="wpsd-report-section wpsd-report-section-sales">';
        $html .= '<div class="wpsd-report-section-header">';
            $html .= '<div class="wpsd-report-section-title">';
                $html .= __( 'Sale Records', 'wp-super-dealer' );
            $html .= '</div>';
            $html .= '<div class="wpsd-report-section-summary">';
                $html .= __( 'This section lists sale records from available independent sources.', 'wp-super-dealer' );
            $html .= '</div>';
            $html .= '<div class="wpsd-section-source">';
                $html .= __( 'Source: VA', 'wp-super-dealer' );
            $html .= '</div>';
        $html .= '</div>';
        $html .= '<table class="wpsd-report-item-table">';
            if ( isset( $atts['sales'] ) && is_array( $atts['sales'] ) ) {
                foreach( $atts['sales'] as $key=>$value ) {
                    $html .= '<tr>';
                        $html .= '<th colspan="3">';
                            $html .= __( 'Sale Report - ', 'wp-super-dealer' ) . esc_html( $value->date );
                        $html .= '</th>';
                    $html .= '</tr>';
                    $html .= '<tr class="wpsd-report-item-table-row">';
                        $html .= '<td colspan="3" class="wpsd-report-sale-record">';
                            if ( is_object( $value ) ) {
                                foreach( $value as $value_key=>$value_value ) {
                                    $html .= '<div class="wpsd-report-sale-item">';
                                        $html .= '<div class="wpsd-report-sale-item-label">';
                                            $label = str_replace( '_', ' ', $value_key );
                                            $label = ucwords( $label );
                                            $html .= esc_html( $label );
                                        $html .= '</div>';
                                        $html .= '<div class="wpsd-report-sale-item-value">';
                                            $html .= esc_html( $value_value );
                                        $html .= '</div>';
                                    $html .= '</div>';
                                } //= end foreach( $value as $value_key=>$value_value ) {
                            }
                        $html .= '</td>';
                    $html .= '</tr>';
                } //= end foreach( $atts['sales'] as $key=>$value ) {
            } else {
                $html .= '<tr>';
                    $html .= '<td colspan="3" class="wpsd-report-section-no-record">';
                        $html .= __( 'No records found!', 'wp-super-dealer' );
                    $html .= '</td>';
                $html .= '</tr>';
            }
        $html .= '</table>';
    $html .= '</div>';

    return $html;
}

function wpsd_get_history_section_checks( $atts ) {
    $html = '';

    $html .= '<div class="wpsd-report-section wpsd-report-section-checks">';
        $html .= '<div class="wpsd-report-section-header">';
            $html .= '<div class="wpsd-report-section-title">';
                $html .= __( 'Title Checks', 'wp-super-dealer' );
            $html .= '</div>';
            $html .= '<div class="wpsd-report-section-summary">';
                $html .= __( 'This section lists check records from available independent sources.', 'wp-super-dealer' );
            $html .= '</div>';
            $html .= '<div class="wpsd-section-source">';
                $html .= __( 'Source: VA', 'wp-super-dealer' );
            $html .= '</div>';
        $html .= '</div>';
        $html .= '<table class="wpsd-report-item-table">';
            if ( isset( $atts['checks'] ) && is_array( $atts['checks'] ) ) {
                foreach( $atts['checks'] as $key=>$value ) {
                    $html .= '<tr>';
                        $html .= '<th colspan="3">';
                            $html .= __( 'Title Check Report - ', 'wp-super-dealer' ) . esc_html( $value->date );
                        $html .= '</th>';
                    $html .= '</tr>';
                    $html .= '<tr class="wpsd-report-item-table-row">';
                        $html .= '<td colspan="3" class="wpsd-report-check-record">';
                            if ( is_object( $value ) ) {
                                foreach( $value as $value_key=>$value_value ) {
                                    $html .= '<div class="wpsd-report-check-item">';
                                        $html .= '<div class="wpsd-report-check-item-label">';
                                            $label = str_replace( '_', ' ', $value_key );
                                            $label = ucwords( $label );
                                            $html .= esc_html( $label );
                                        $html .= '</div>';
                                        $html .= '<div class="wpsd-report-check-item-value">';
                                            $html .= esc_html( $value_value );
                                        $html .= '</div>';
                                    $html .= '</div>';
                                } //= end foreach( $value as $value_key=>$value_value ) {
                            }
                        $html .= '</td>';
                    $html .= '</tr>';
                } //= end foreach( $atts['checks'] as $key=>$value ) {
            } else {
                $html .= '<tr>';
                    $html .= '<td colspan="3" class="wpsd-report-section-no-record">';
                        $html .= __( 'No records found!', 'wp-super-dealer' );
                    $html .= '</td>';
                $html .= '</tr>';
            }
        $html .= '</table>';
    $html .= '</div>';

    return $html;
}
?>