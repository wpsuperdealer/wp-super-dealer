<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function get_vehicle_location( $post_id ) {
	$vehicle_contact = array();
	$location_terms = get_the_terms( $post_id, 'location' );
	if ( $location_terms ) {
		foreach ( $location_terms as $location_term ) {
            if ( ! is_object( $location_term ) || is_Wp_error( $location_term ) ) {
                $location_slug = wpsd_get_default_location_slug();
            } else {
                $location_slug = $location_term->slug;
            }
		}
	} else {
		$location_slug = wpsd_get_default_location_slug();
	}

	$location = array();

	$location_term = get_term_by( 'slug', $location_slug, 'location' );

	if ( is_wp_error( $location_term ) ) {
		return print_r( $location_term, true );
	}
	if ( ! is_object( $location_term ) ) {
		return print_r( $location_term, true );
	}
	$location_term_id = $location_term->term_id;
	$location['location_id'] = $location_term_id;
	$location['name'] = $location_term->name;
	$location['slug'] = $location_term->slug;
	
	$meta = get_term_meta( $location_term_id );
	if ( is_array( $meta ) && 0 < count( $meta ) ) {
		foreach( $meta as $field=>$data ) {
			if ( is_array( $data ) && 0 < count( $data ) ) {
				$location[ $field ] = $data[0];
			}
		}
	}
	
	return $location;
}

function wpsd_replace_contact_info_tags( $post_id, $body ) {
	if ( empty( $post_id ) ) {
		$post_id = 0;
	}
	$vehicle_contact = array();
	$current_location_terms = get_the_terms( $post_id, 'location' );
	if ( $current_location_terms ) {
		foreach ( $current_location_terms as $current_locations ) {
			$current_location = $current_locations->slug;
		}
	}
	if ( empty( $current_location ) ) {
		$current_location = wpsd_get_default_location_slug();
	}

	return $body;
}

function wpsd_get_default_location_slug() {
	if ( defined( 'WPSD_DEFAULT_LOCATION_SLUG' ) ) {
		return WPSD_DEFAULT_LOCATION_SLUG;
	}

	$terms = get_terms( 'location', array(
		'hide_empty' => false,
		'orderby' => 'term_id',
	) );

	if ( ! is_wp_error( $terms ) && isset( $terms[0] ) ) {
		$default_location = $terms[0]->slug;
	} else {
		$default_location = __( 'default', 'wp-super-dealer' );
	}
	return $default_location;
}

function wpsd_get_default_location_name() {
	if ( defined( 'WPSD_DEFAULT_LOCATION_NAME' ) ) {
		return WPSD_DEFAULT_LOCATION_NAME;
	}

	$terms = get_terms( 'location', array(
		'hide_empty' => false,
		'orderby' => 'term_id',
	) );

	if ( isset( $terms[0] ) ) {
		$default_location = $terms[0]->name;
	} else {
		$default_location = __( 'Default', 'wp-super-dealer' );
	}
	return $default_location;
}
?>