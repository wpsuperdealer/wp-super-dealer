<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Load front end styles
 *
 */
function wpsd_print_styles() {
	global $wpsd_options;
	wp_enqueue_style( 'dashicons' );	
	wp_enqueue_style( 'wpsd-lightGallery-css', WPSD_PATH . 'js/lightGallery-2.3.0/dist/css/lightgallery-bundle.min.css', array(), WPSD_VER );
    wp_enqueue_style( 'wpsd-jquery-ui-css', WPSD_PATH . 'css/jquery-ui.css',false,'1.9.0',false);
    wp_enqueue_style( 'wpsd-css', WPSD_PATH . 'css/wpsd.css', array( 'wpsd-jquery-ui-css' ), WPSD_VER );
}
add_filter( 'wp_print_styles', 'wpsd_print_styles' );

/**
 * Load front end scripts
 *
 * @todo
 *     - replace vanilla js with jQuery functions
 */
function wpsd_enqueue_scripts() {
	global $wpsd_options;
	wp_register_script( 'wpsd-js', WPSD_PATH . 'js/wpsd.js', array( 'jquery' ), WPSD_VER );
    $search_url = '';
    if ( isset( $wpsd_options['inventory_page'] ) ) {
        $search_url = $wpsd_options['inventory_page'];
    }

	$js_settings = array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'css_url' => get_bloginfo( 'stylesheet_url' ),
		'wpsd_path' => WPSD_PATH,
		'search_url' => $search_url,
		'error_image' => $wpsd_options['error_image'],
	);

	if ( $wpsd_options['dynamic_load'] === __( 'yes', 'wp-super-dealer' ) ) {
		$loader = WPSD_PATH . 'images/ajax-loader.gif';
		$loader = apply_filters( 'wpsd_load_more_filter', $loader );
		$js_settings['dynamic_load'] = true;
		$js_settings['container'] = esc_html( $wpsd_options['al_container'] );
		$js_settings['items'] = esc_html( $wpsd_options['al_items'] );
		$js_settings['pagination'] = esc_html( $wpsd_options['al_pagination'] );
		$js_settings['next'] = esc_html( $wpsd_options['al_next'] );
		$js_settings['loader'] = esc_html( $loader );
	} else {
		$js_settings['dynamic_load'] = false;
	}
    
	wp_localize_script( 'wpsd-js', 'wpsdParams', $js_settings );
	wp_enqueue_script( 'wpsd-js' );
	
	wp_register_script( 'wpsd-lightGallery-js', WPSD_PATH . 'js/lightGallery-2.3.0/dist/lightgallery.umd.js',  array( 'jquery' ), WPSD_VER );
	wp_localize_script( 'wpsd-lightGallery-js', 'cdTbParams', array(
		'is_mobile' => wp_is_mobile(),
	) );
	wp_enqueue_script( 'wpsd-lightGallery-js' );
	wp_enqueue_script( 'wpsd-lightGallery-share-js', WPSD_PATH . 'js/lightGallery-2.3.0/dist/plugins/share/lg-share.min.js', array( 'jquery', 'wpsd-lightGallery-js' ), WPSD_VER );
	wp_enqueue_script( 'wpsd-lightGallery-zoom-js', WPSD_PATH . 'js/lightGallery-2.3.0/dist/plugins/zoom/lg-zoom.min.js', array( 'jquery', 'wpsd-lightGallery-js' ), WPSD_VER );
	wp_enqueue_script( 'wpsd-lightGallery-fullscreen-js', WPSD_PATH . 'js/lightGallery-2.3.0/dist/plugins/fullscreen/lg-fullscreen.min.js', array( 'jquery', 'wpsd-lightGallery-js' ), WPSD_VER );
	wp_enqueue_script( 'wpsd-lightGallery-thumbnails-js', WPSD_PATH . 'js/lightGallery-2.3.0/dist/plugins/thumbnail/lg-thumbnail.min.js', array( 'jquery', 'wpsd-lightGallery-js' ), WPSD_VER );
	
}
add_action( 'wp_enqueue_scripts', 'wpsd_enqueue_scripts');

?>