<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! is_admin() ) {
    add_filter( 'widget_text', 'wpsd_text_filter', 11 );
    add_filter( 'widget_block_content', 'wpsd_text_filter', 11 );
	add_filter( 'the_content', 'wpsd_text_filter', 11 );
}

/**
 * Add post-thumbnails theme support, call wpsd_session(), define global $wpsd_options, register menus and sidebars
 *
 */
function wpsd_init() {
	global $wpsd_options;
    $wpsd_options = wpsd_get_options();

	/**
	 * Add support for Featured Images
	 */
	if ( function_exists( 'add_theme_support' ) ) {
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'srp-main-image', 640, 480, true );
		add_image_size( 'vdp-main-image', 1280, 920, true );
	}
}
add_action( 'init', 'wpsd_init' );

if ( function_exists('register_sidebar') ){
	add_action( 'widgets_init', 'wpsd_register_sidebars', 2 );
}

/**
 * Filter post and widget content, replace contact tags with their respective information
 *
 * @todo
 *     - add documention on usage
 */
function wpsd_text_filter( $body ) {
	$text = wpsd_replace_contact_info_tags( 0, $body );
	global $post;
	if ( is_object( $post ) ) {
		if ( WPSD_POST_TYPE === $post->post_type ) {
			$text = wpsd_tag_filter( $post->ID, $text );
		}
	}
	return $text;
}

/**
 * Get current page URL for sales affiliate redirect
 *
 */
function wpsd_self_url() {

    if( ! isset( $_SERVER['REQUEST_URI'] ) ){
        $serverrequri = $_SERVER['PHP_SELF'];
    } else {
        $serverrequri = $_SERVER['REQUEST_URI'];
    }

    $s = ( empty( $_SERVER["HTTPS"] ) ? '' : ( ( $_SERVER["HTTPS"] == "on" ) ? "s" : "" ) );
    $protocol = wpsd_str_left( strtolower( $_SERVER["SERVER_PROTOCOL"] ), "/" ) . $s;
    $port = ( $_SERVER["SERVER_PORT"] == "80" ) ? "" : ( ":".$_SERVER["SERVER_PORT"] );

    return $protocol . "://" . $_SERVER['SERVER_NAME'] . $port . $serverrequri;   
}

/**
 * Return portion of string based on parameters
 *
 * @param $s1 string to be trimmed
 * @param $s2 segment of string to begin trim
 * @todo
 *     - add documention on usage
 */
function wpsd_str_left( $s1, $s2 ) {
	return substr( $s1, 0, strpos( $s1, $s2 ) );
}

/**
 * Set query post_type if vehicle or associated taxonomies are loaded
 *
 * @todo
 *     - call wpsd_get_specification_taxonomies and loop it to get correct taxonomies
 */
if ( ! is_admin() ) {
	add_filter( 'pre_get_posts', 'get_wpsd_posts' );
}
function get_wpsd_posts( $query ) {
	if ( is_post_type_archive( WPSD_POST_TYPE ) 
		|| is_tax( 'condition' ) 
		|| is_tax( 'vehicle_year' ) 
		|| is_tax( 'make' ) 
		|| is_tax( 'model' ) 
		|| is_tax( 'body_style' ) )
	{
		if ( $query->is_main_query() ) {
			$query->set( 'post_type', array( WPSD_POST_TYPE ) );
		}
	}

	return $query;
}

/**
 * Patch for issue with Yoast SEO
 *
 * @param $excerpt - the current excerpt
 *
 * Issue: Yoast SEO returns blank vehicle pages if no excerpt is entered
 * Only occurs if twitter or facebook integration is enabled
 * These are on by default so easiest solution is to return an excerpt with a blank space in it
 * Code traced to wp-seo-main.php line 311 in Yoast ver 4.5
*/
function wpsd_get_the_excerpt( $excerpt ) {
	//= Is the excerpt empty?
	if ( empty( $excerpt ) ) {
		//= Get the current $post_id
		$post_id = get_the_ID();
		//= Did we get a current $post_id
		if ( ! empty( $post_id ) ) {
			//= Get current post type
			$post_type = get_post_type( $post_id );
			//= Is current post type WPSD_POST_TYPE
			if ( WPSD_POST_TYPE === $post_type ) {
				//= Send a blank space if vehicle excerpt is empty
			    return " ";
			}
		}
	}
	//= Return normal excerpt
	return $excerpt;
}
add_filter('get_the_excerpt', 'wpsd_get_the_excerpt', 1);

?>