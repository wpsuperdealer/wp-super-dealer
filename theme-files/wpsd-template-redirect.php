<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Template redirect if included template files are used
 *
 * @param $original_template the template being used when this function is called
 *
 * @todo
 *     - Depracate option to use included template files
 *     - The option to enable this is no longer used - leeping code for future reference
 *     - Loop specs and return taxonomies
 */
function wpsd_theme_redirect( $original_template ) {
	global $wpsd_options;
	if ( $wpsd_options['use_theme_files'] == 'Yes' ) {
		global $wp;
		$plugindir = dirname( __FILE__ );
		$plugindir = str_replace( 'includes/', '', $plugindir );
		$plugindir = str_replace( 'includes\\', '', $plugindir );
		$plugindir = str_replace( 'includes', '', $plugindir );
		// Custom Post Type vehicle
		$template_directory = get_template_directory();
		if ( isset( $wp->query_vars["post_type"] ) ) {
			if ( $wp->query_vars["post_type"] == WPSD_POST_TYPE ) {
				if ( isset( $wp->query_vars["vehicle"] ) ) {
					$templatefilename = 'single-vehicle.php';
				} else {
					$templatefilename = 'archive-vehicle.php';	
				}
				if ( file_exists( $template_directory . '/' . $templatefilename ) ) {
					$return_template = $template_directory . '/' . $templatefilename;
				} else {
					$return_template = $plugindir . '/theme-files/' . $templatefilename;
				}
				do_wpsd_theme_redirect( $return_template );
			} else {
				return $original_template;
			}
		// Custom Taxonomy
		} elseif ( isset( $wp->query_vars["condition"] ) ) {
			$templatefilename = 'archive-vehicle.php';
			if ( file_exists( $template_directory . '/' . $templatefilename ) ) {
				$return_template = $template_directory . '/' . $templatefilename;
			} else {
				$return_template = $plugindir . '/theme-files/' . $templatefilename;
			}
			do_wpsd_theme_redirect( $return_template );
		} elseif ( isset( $wp->query_vars["vehicle_year"] ) ) {
			$templatefilename = 'archive-vehicle.php';
			if ( file_exists( $template_directory . '/' . $templatefilename ) ) {
				$return_template = $template_directory . '/' . $templatefilename;
			} else {
				$return_template = $plugindir . '/theme-files/' . $templatefilename;
			}
			do_wpsd_theme_redirect( $return_template );
		} elseif ( isset($wp->query_vars["make"] ) ) {
			$templatefilename = 'archive-vehicle.php';
			if ( file_exists( $template_directory . '/' . $templatefilename ) ) {
				$return_template = $template_directory . '/' . $templatefilename;
			} else {
				$return_template = $plugindir . '/theme-files/' . $templatefilename;
			}
			do_wpsd_theme_redirect( $return_template );
		} elseif ( isset( $wp->query_vars["model"] ) ) {
			$templatefilename = 'archive-vehicle.php';
			if ( file_exists( $template_directory . '/' . $templatefilename ) ) {
				$return_template = $template_directory . '/' . $templatefilename;
			} else {
				$return_template = $plugindir . '/theme-files/' . $templatefilename;
			}
			do_wpsd_theme_redirect( $return_template );
		} elseif ( isset( $wp->query_vars["location"] ) ) {
			$templatefilename = 'archive-vehicle.php';
			if ( file_exists( $template_directory . '/' . $templatefilename ) ) {
				$return_template = $template_directory . '/' . $templatefilename;
			} else {
				$return_template = $plugindir . '/theme-files/' . $templatefilename;
			}
			do_wpsd_theme_redirect( $return_template );
		} elseif ( isset( $wp->query_vars["body_style"] ) ) {
			$templatefilename = 'archive-vehicle.php';
			if ( file_exists( $template_directory . '/' . $templatefilename ) ) {
				$return_template = $template_directory . '/' . $templatefilename;
			} else {
				$return_template = $plugindir . '/theme-files/' . $templatefilename;
			}
			do_wpsd_theme_redirect( $return_template );
		// Search Cars
		} elseif ( isset( $wp->query_vars["s"] ) ) {
			if ( $wp->query_vars['s'] == 'cars' || $wp->query_vars['s'] == 'vehicles' ) {
				if ( $_GET['car'] == 1  || isset( $_GET[WPSD_POST_TYPE] ) ) {
					$templatefilename = 'search.php';
					$return_template = $plugindir . '/theme-files/' . $templatefilename;
					global $post, $wp_query;
					$wp_query->is_404 = false;
					require_once( $return_template );
					die();
				}
			}
		} else {
			return $original_template;
		}
		return $return_template;
	} else {
		return $original_template;
	}
}
add_action( 'template_include', 'wpsd_theme_redirect', 1 );

/**
 * Force WP to use the template file we've selected
 *
 * @param $url the template file being used
 *
 * @todo
 *     - Depracate option to use included template files
 */
function do_wpsd_theme_redirect( $url ) {
    global $post, $wp_query;
	$url = str_replace( 'includes/', '', $url );
	$url = str_replace( 'includes\\', '', $url );
    if (have_posts()) {
        require_once( $url );
        die();
    } else {
        $wp_query->is_404 = true;
    }
}	
?>