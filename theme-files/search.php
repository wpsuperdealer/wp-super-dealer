<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage WPSuperDealer 
 * @since WPSuperDealer 1.0
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $wpsd_options;
$wpsd_query = wpsd_query_search();
$search_query = new WP_Query();
$search_query->query($wpsd_query);
$total_results = $search_query->found_posts;
$searched = wpsd_get_searched_by();
$searched = '';
header('HTTP/1.1 200 OK');
get_header();

do_action( 'wpsd_before_content_action' );
if ( $search_query->have_posts() ) {
	echo $wpsd_options['before_listings'];
	if ( isset($_GET['car']) || isset( $_GET[WPSD_POST_TYPE] ) ) { ?>
		<h1 class="page-title"><?php echo __( 'Search Results:', 'wp-super-dealer' ); ?></h1>
		<h4 class="results_found"><?php _e(' Results Found', 'wp-super-dealer' ); echo ': ' . esc_html( $total_results );?></h4>
		<?php echo esc_html( $searched ); ?>
		<?php echo wpsd_nav('top', $search_query); ?>
		<div id="wpsd-inventory-wrap" class="listing" role="main">
			<?php
			/*======= WPSuperDealer Loop ======================================================= */								 
			while ( $search_query->have_posts() ) : $search_query->the_post();
				$post_id = $search_query->post->ID;
				$html = apply_filters('wpsd_srp_filter', wpsd_srp_item( $post_id ), $post_id, array() );
				echo $html;
			endwhile;
			/* Display navigation to next/previous pages when applicable */ ?>
		</div>
		<?php echo wpsd_nav('bottom', $search_query);
	} else {
		 get_template_part( 'loop', 'search' );
	}
} else {
	echo wpsd_no_search_results();
}
do_action( 'wpsd_after_content_action' );
get_footer();
?>