<?php
/**
 * The Template for displaying all single cars.
 *
 * @package WordPress
 * @subpackage WPSuperDealer 
 * @since WPSuperDealer 1.0
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $wpsd_options;

get_header();
do_action( 'wpsd_before_content_action' );
do_action( 'wpsd_header_sidebar_action' );
	if ( have_posts() ) while ( have_posts() ) : the_post();
		$post_id = get_the_ID();
		$html = apply_filters( 'wpsd_vdp_filter', wpsd_vdp_item( $post_id ), $post_id );
		echo wpsd_kses_vdp_item( $html );
	endwhile; // end of the loop. ?>
<?php
do_action( 'wpsd_after_content_action' );
do_action( 'wpsd_vehicle_sidebar_action' );
get_footer(); 
?>