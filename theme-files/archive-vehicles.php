<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $wpsd_options;
get_header();
$wpsd_query = wpsd_query_archive();
if ( isset( $_GET['car'] ) || isset( $_GET[WPSD_POST_TYPE] ) ) {
	if ( $_GET['car'] == 1 || isset( $_GET[WPSD_POST_TYPE] ) ) {
		$wpsd_query = wpsd_query_search();
	}
}
query_posts( $wpsd_query );
$total_results = $wp_query->found_posts;

do_action( 'wpsd_before_content_srp_action', array() );
do_action( 'wpsd_before_content_action' );
	echo wp_kses_data( $wpsd_options['before_listings'] );
	?>
		<h4 class="results_found"><?php _e( 'Results Found','wp-super-dealer' ); echo ': ' . esc_html( $total_results ); ?></h4>
	<?php 
	echo wpsd_nav( 'top', $wp_query );
	/*======= WPSuperDealer Loop ======================================================= */
	while ( have_posts() ) : the_post();
		$post_id = $post->ID;
		$html = apply_filters( 'wpsd_srp_filter', wpsd_srp_item( $post_id ), $post_id, array() );
		echo wpsd_kses_srp( $html );
	endwhile; // End the loop. Whew. ?>
	<?php
	echo wpsd_nav( 'bottom', $wp_query );
do_action( 'wpsd_after_content_action' );
do_action( 'wpsd_after_content_srp_action', array() );
get_footer();
?>