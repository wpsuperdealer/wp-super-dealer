<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_srp_item( $post_id = 0, $atts = array() ) {
	$x = '';
	$x = apply_filters( 'wpsd_srp_filter', $x, $post_id, $atts );
	//= Did another plugin filter the SRP already?
	if ( ! empty( $x ) ) {
        //= escape our srp item
        $x = wpsd_kses_srp( $x );
		//= We already found a SRP template, so let's stop and return it
		return $x;
	}

	$vehicle = wpsd_get_vehicle( $post_id );

	$srp_item_css_class = 'srp-item srp-item-one';
	if ( isset( $_COOKIE['srp_style'] ) ) {
		$srp_item_css_class .= ' ' . sanitize_text_field( $_COOKIE['srp_style'] );
	} else {
		$srp_item_css_class .= ' ' . $atts['default_style'];
	}
	$srp_item_css_class = apply_filters( 'srp_item_css_class_filter', $srp_item_css_class, $post_id, $atts );

	$x .= '<div class="' . esc_attr( $srp_item_css_class ) . '">';
		$x .= '<div class="col">';
			$x .= '<div class="srp-main-photo-wrap">';
				$x .= '<a href="' . esc_url( $vehicle['link'] ) . '">';
					$x .= '<img class="srp-main-photo" src="' . esc_url( $vehicle['main-image'] ) . '" alt="" title="' . esc_attr( $vehicle['title'] ) . '">';
				$x .= '</a>';
			$x .= '</div>';
		$x .= '</div>';
	
		$x .= '<div class="col">';
			$x .= '<div class="srp-title">';
				$x .= '<a href="' . esc_url( $vehicle['link'] ) . '">';
					$x .= esc_html( $vehicle['title'] );
				$x .= '</a>';
			$x .= '</div>';

			$srp_fields = array(
				__( 'Stock Number', 'wp-super-dealer' ),
				__( 'Body Style', 'wp-super-dealer' ),
				__( 'Mileage', 'wp-super-dealer' ),
				__( 'Exterior Color', 'wp-super-dealer' ),
			);
			$all_specs = wpsd_get_all_specifications( false, false, true, true );

			foreach( $srp_fields as $srp_field ) {
				$slug = sanitize_title( $srp_field );
				$_slug = str_replace( '-', '_', $slug );

				if ( isset( $vehicle[ $_slug ] ) && ! empty( $vehicle[ $_slug ] ) ) {
					$x .= '<div class="srp-field srp-field-' . esc_attr( $_slug ) . '">';
						$x .= '<div class="srp-field-title">';
							if ( isset( $all_specs[ $_slug ] ) ) {
								if ( isset( $all_specs[ $_slug ]['label'] ) && ! empty( isset( $all_specs[ $_slug ]['label'] ) ) ) {
									$x .= esc_html( $all_specs[ $_slug ]['label'] );
								} else {
									$x .= esc_html( $srp_field );
								}
							} else {
								$x .= esc_html( $srp_field );
							}
						$x .= '</div>';
						$x .= '<div class="srp-field-value">';
							$x .= esc_html( $vehicle[ $_slug ] );
						$x .= '</div>';
					$x .= '</div>';
				}
			}
	
		$x .= '</div>';

		$x .= '<div class="col">';
			$x .= '{compare_pro}';
			$x .= wpsd_get_vehicle_price( $post_id );
		$x .= '</div>';
		$x .= '<div class="wpsd-clear"></div>';
	$x .= '</div>';

	$x = apply_filters( 'wpsd_srp_item_filter', $x, $post_id, $atts );
	$x = wpsd_tag_filter( $post_id, $x, $vehicle );

    //= escape our srp item
    $x = wpsd_kses_srp_item( $x );
    
	return $x;
}

function wpsd_kses_srp_item( $content ) {
    $elements = wpsd_srp_item_elements();
    return wp_kses( $content, $elements );
}

function wpsd_srp_item_elements() {
    $elements = array(
        'a' => array(
            'id' => array(),
            'name' => array(),
            'href' => array(),
            'title' => array(),
            'target' => array()
        ),
        'h2' => array(
            'class' => array(),
        ),
        'h3' => array(
            'class' => array(),
        ),
        'h4' => array(
            'class' => array(),
        ),
        'img' => array(
            'id' => array(),
            'name' => array(),
            'class' => array(),
            'width' => array(),
            'onerror' => array(),
            'src' => array(),
            'style' => array(),
        ),
        'br' => array(),
        'p' => array(),
        'i' => array(
            'class' => array(),
            'title' => array(),
        ),
        'div' => array(
            'id' => array(),
            'name' => array(),
            'class' => array(),
            'data-name' => array(),
            'data-id' => array(),
            'data-post-id' => array(),
            'data-slug' => array(),
            'data-base64' => array(),
            'data-option' => array(),
            'data-value' => array(),
            'data-checked' => array(),
            'data-section' => array(),
            'data-type' => array(),
            'data-group' => array(),
            'data-sub-group' => array(),
            'data-specification-group' => array(),
            'data-specification-group-value' => array(),
            'data-option-group' => array(),
            'data-option-group-value' => array(),
            'data-option-sub-group' => array(),
            'data-option-sub-group-value' => array(),
            'data-field' => array(),
            'data-field-slug' => array(),
            'data-car-link' => array(),
            'data-src-id' => array(),
            'data-src' => array(),
            'data-cnt' => array(),
        ),
        'span' => array(
            'id' => array(),
            'name' => array(),
            'class' => array(),
            'data-video-url' => array(),
            'data-field-slug' => array(),
            'data-post-id' => array(),
        ),
        'iframe' => array(
            'src' => array(),
            'frameborder' => array(),
            'allow' => array(),
        ),
        'small' => array(
            'class' => array(),
            'title' => array(),
        ),
        'label' => array(
            'class' => array(),
            'title' => array(),
        ),
        'input' => array(
            'class' => array(),
            'type' => array(),
            'data-post-id' => array(),
            'checked' => array(),
        ),
        'ul' => array(
            'class' => array(),
        ),
        'ol' => array(),
        'li' => array(),
        'table' => array(
            'class' => array(),
        ),
        'tr' => array(),
        'td' => array(),
    );
    $elements = apply_filters( 'wpsd_kses_srp_item_filter', $elements );
    return $elements;
}
?>