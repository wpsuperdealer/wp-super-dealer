<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_vdp_item( $post_id, $content ) {
    //= see if we already have content from the filter
    $html = apply_filters( 'wpsd_vdp_filter', '', $post_id );
    if ( ! empty( $html ) ) {
        if ( ! defined( 'WPSD_NO_VDP_WRAP' ) ) {
            //= someone has already filtered the VDP, wrap it and stop here since there's no need to send the default
            $html = '<div class="wpsd-wrap">' . $html . '</div>';
        }
        //= escape our vdp item
        $html = wpsd_kses_vdp( $html );
        return $html;
    }

	//= get an array of the vehicle data
    $vehicle = wpsd_get_vehicle( $post_id );

    //= create a variable to store our html in so we can return it later
    $html = '';
	
	$html .= '<div class="wpsd-vdp-wrap">';
        $html .= '<div class="vdp-item vdp-item-one">';

			$html .= '<div class="vdp-sidebar-before">';
				ob_start();
					dynamic_sidebar( 'single_vehicle_before' );
					$sidebar = ob_get_contents();
				ob_end_clean();
				$html .= $sidebar;
			$html .= '</div>';
			$html .= '<div class="wpsd-clear"></div>';
	
			$html .= '<div class="vdp-col">';
				$html .= '<div class="vdp-image-box">';
					$html .= '<div class="vdp-main-image-wrap">';
						$html .= '<div class="vdp-before-main-image">';
							$html .= apply_filters( 'vdp_before_main_image_filter', '', $post_id );
						$html .= '</div>';
						$html .= '<img src="' . esc_url( $vehicle['main-image'] ) . '" class="vdp-main-image" />';
						$html .= '<div class="vdp-after-main-image">';
							$html .= apply_filters( 'vdp_after_main_image_filter', '', $post_id );	
						$html .= '</div>';
					$html .= '</div>';
				$html .= '</div>';
			$html .= '</div>';

			$html .= '<div class="vdp-col">';
				$html .= '<div class="vdp-title">';
					$html .= esc_html( $vehicle['title'] );
				$html .= '</div>';
				$html .= '<div class="vdp-summary-wrap">';
	
					$html .= '<div class="vdp-sidebar-summary">';
						ob_start();
							dynamic_sidebar( 'single_vehicle_summary' );
							$summary_sidebar = ob_get_contents();
						ob_end_clean();
						$html .= $summary_sidebar;
					$html .= '</div>';
					$html .= '<div class="wpsd-clear"></div>';
	
					if ( empty( $summary_sidebar ) ) {
						$summary_specs = array(
							'vehicle_year',
							'make',
							'model',
							'stock_number',
							'vin',
							'mileage',
							'exterior_color',
						);
						$summary_specs = apply_filters( 'vdp_summary_specs_filter', $summary_specs );

						$all_specs = wpsd_get_all_specifications( false, false, true, true );
						
						foreach( $summary_specs as $summary_spec ) {
							if ( isset( $all_specs[ $summary_spec ] ) && isset( $vehicle[ $summary_spec ] ) ) {

                                if ( empty( $vehicle[ $summary_spec ] ) ) {
                                    continue;
                                }

								$html .= '<div class="vdp-summary-spec-item">';
									$html .= '<div class="vdp-summary-spec-label">';
								
										if ( isset( $all_specs[ $summary_spec ] ) ) {
											if ( isset( $all_specs[ $summary_spec ]['label'] ) 
												&& ! empty( isset( $all_specs[ $summary_spec ]['label'] ) ) )
											{
												$html .= esc_html( $all_specs[ $summary_spec ]['label'] );
											} else {
												$label = str_replace( '_', ' ', $summary_spec );
												$label = ucwords( $label );
												$html .= esc_html( $label );
											}
										} else {
											$html .= esc_html( $all_specs[ $summary_spec ] );
										}
								
									$html .= '</div>';
									$html .= '<div class="vdp-summary-spec-value">';
										$html .= esc_html( $vehicle[ $summary_spec ] );
									$html .= '</div>';						
								$html .= '</div>';
							}
						}
					} ///= end if ( empty( $summary_sidebar ) ) {
				$html .= '</div>'; //= end .vdp-summary-wrap
	
				$html .= '<div class="vdp-price-box-wrap">';
					$html .= wpsd_get_vehicle_price( $post_id );
				$html .= '</div>';
	
			$html .= '</div>';
			//= end $html .= '<div class="vdp-col">';
			$html .= wpsd_get_thumbnail_row( $vehicle['thumbnails'] );
			$html .= '<div class="wpsd-clear"></div>';

			$html .= '<div class="vdp-sidebar-middle">';
				ob_start();
					dynamic_sidebar( 'single_vehicle_middle' );
					$sidebar = ob_get_contents();
				ob_end_clean();
				$html .= $sidebar;
			$html .= '</div>';
			$html .= '<div class="wpsd-clear"></div>';
			$html .= wpsd_get_vdp_location( $post_id, $vehicle['location-details'], $vehicle );
			$html .= '<div class="wpsd-clear"></div>';

			$html .= '<div class="vdp-description-wrap">';
				$html .= '<div class="vdp-description-title">';
					$html .= __( 'Vehicle Description', 'wp-super-dealer' );
				$html .= '</div>';
				$html .= '<div class="vdp-description">';
					$html .= wp_kses_post( $vehicle['description'] );
				$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="wpsd-clear"></div>';

			$html .= wpsd_get_vdp_specs( $post_id );
    		$html .= wpsd_get_vdp_options( $post_id );

            //= History Reports
            $report = '';
            $history_report = get_post_meta( $post_id, '_history_report', true );
            if ( $history_report && ! empty( $history_report ) ) {
                $report .= '<div class="wpsd-history-report-title">' . __( 'Vehicle History Report', 'wp-super-dealer' ) . '</div>';
                $report .= wpsd_history_report_template( $history_report );
            }
            $report = apply_filters( 'wpsd_history_report_filter', $report, $post_id, $history_report );
            $html .= $report;
    
	       $html .= '<div class="wpsd-clear"></div>';
		$html .= '</div>';
	
		$html .= '<div class="vdp-sidebar-after">';
			ob_start();
				dynamic_sidebar( 'single_vehicle_after' );
				$sidebar = ob_get_contents();
			ob_end_clean();
			$html .= $sidebar;
		$html .= '</div>';
		$html .= '<div class="wpsd-clear"></div>';
	
	$html .= '</div>';
    
    //= NOTE: script is not currently allowed in wpsd_kses_vdp() and will not be output unless that is filtered
    global $wpsd_options;
    if ( isset( $wpsd_options['show-schema'] ) && wpsd_is_true( $wpsd_options['show-schema'] ) ) {
        $schema .= '
        <script type=\'application/ld+json\'>
            {
                "@context": "https://schema.org/",
                "@type": "Product",
                "@id": "{post_id}",
                "name": "{title}",
                "image": "{main_image}",
                "description": "{description}",
                "sku": "{stock_number}",
                "brand": "{make}",
                "model": "{model}",
                "color": "{exterior_color}",
                "manufacturer": {
                    "@type": "Organization",
                    "name": "{make}"
                },

                "offers": {
                    "@type": "Offer",
                    "url": "{url_contact_form}",
                    "priceCurrency": "USD",
                    "price": "{price}",
                    "priceValidUntil": "<?php echo $expire; ?>",
                    "itemCondition": "http://schema.org/UsedCondition",
                    "availability": "http://schema.org/InStock"
                }
            }
        </script>';
        $schema = apply_filters( 'wpsd_schema_filter', $schema, $post_id );
        $html .= $schema;
    }
    
    //= provide a filter for the default VDP
    $html = apply_filters( 'wpsd_vdp_default_filter', $html, $post_id );

    //= escape our vdp item
    $html = wpsd_kses_vdp( $html );
    return $html;
}

function wpsd_get_vdp_specs( $post_id ) {
	$maps = wpsd_get_type_maps();

	$vehicle_type = wpsd_get_vehicle_type( $post_id );
	$vehicle_map = wpsd_get_vehicle_map( $post_id );
	$default_global_fields = wpsd_global_fields();

	$vehicle_options = wpsd_get_vehicle_options();
	if ( isset( $vehicle_options['global_specs'] ) 
		&& is_array( $vehicle_options['global_specs'] )
		&& ( 0 < count( $vehicle_options['global_specs'] ) )
		) {

		$global_fields = $vehicle_options['global_specs'];
	} else {
		$global_fields = array();
	}
	$global_fields = wp_parse_args( $global_fields, $default_global_fields );

	//= Is this type grouped?
	$no_group_specs = false;
	if ( isset( $vehicle_options['no_group_specs'] ) ) {
		if ( is_array( $vehicle_options ) 
			&& isset( $vehicle_options['no_group_specs'] ) 
			&& $vehicle_options['no_group_specs'] ) {

			$no_group_specs = true;
		}
	}

	$x = '';
	$x .= '<div class="vdp-specs-wrap" data-post-id="' . esc_attr( $post_id ) . '">';
		$fields = array();
		$global_fields_args = array();

		foreach( $global_fields as $field=>$data ) {
			if ( isset( $data['label'] ) ) {
				if ( isset( $data['disabled'] ) ) {
					if ( 'true' === $data['disabled'] || true === $data['disabled'] ) {
						continue;
					}
				}
				$global_fields_args[ $field ] = $data;
			}
		}

		$global_specs_title = __( 'Vehicle Details', 'wp-super-dealer' );
		$global_specs_title = apply_filters( 'global_specs_title_filter', $global_specs_title, $post_id );
		$global_fields_array = array(
			$global_specs_title => $global_fields_args,
		);
		if ( isset( $vehicle_map['specifications'] ) ) {
			$vehicle_map['specifications'] = array_merge( $global_fields_array, $vehicle_map['specifications'] );
		} else {
			$vehicle_map['specifications'] = $global_fields_array;
		}
		if ( isset( $vehicle_map['specifications' ] ) ) {
			if ( is_array( $vehicle_map['specifications' ] ) ) {
				foreach( $vehicle_map['specifications' ] as $spec_group=>$specs ) {						
					$x .= '<div class="wpsd-vehicle-spec-group">';
						$x .= '<div class="wpsd-vehicle-spec-group-title">';
							$x .= esc_html( $spec_group );
						$x .= '</div>';
						if ( is_array( $specs ) ) {
							$fields = array();
							foreach( $specs as $spec=>$spec_settings ) {
								if ( isset( $spec_settings['hide_edit'] ) ) {
									if ( true === $spec_settings['hide_edit']
										|| 'true' === $spec_settings['hide_edit'] )
									{
										continue;
									}
								}
								$slug = sanitize_title( $spec );
								$_slug = str_replace( '-', '_', $slug );
								//=================================
								//= Get the current vehicle's value
								//=================================
								$spec_value = get_post_meta( $post_id, '_' . $_slug, true );

								$field_type = 'text';

								if ( ! isset( $spec_settings['default-value'] ) ) {
									$spec_settings['default-value'] = '';
								}

								if ( empty( $spec_value ) ) {
									$spec_value = $spec_settings['default-value'];
								}
                                
                                if ( empty( $spec_value ) ) {
                                    continue;
                                }
                                
								if ( ! isset( $spec_settings['label'] ) 
									|| empty( $spec_settings['label'] ) ) 
								{
									$label = $spec;
								} else {
									$label = $spec_settings['label'];
								}
								$x .= '<div class="vdp-spec-item vdp-spec-item-' . esc_attr( $_slug ) . '">';
									$x .= '<div class="vdp-spec-item-label">';
										$x .= esc_html( $label );
									$x .= '</div>';
									$x .= '<div class="vdp-spec-item-value">';
										$x .= esc_html( $spec_value );
									$x .= '</div>';
								$x .= '</div>';
							} //= end foreach( $specs as $spec=>$spec_settings ) {
							$x .= '<div class="wpsd-clear"></div>';
						}
					$x .= '</div>';
				} //= end foreach( $vehicle_map['specifications' ] as $spec_group=>$specs ) {
			}
		}

	$x .= '</div>';
	return $x;
}

function wpsd_get_vdp_options( $post_id ) {
	$x = '';
	$maps = wpsd_get_type_maps();
	$vehicle_type = wpsd_get_vehicle_type( $post_id );
	if ( ! isset( $maps[ $vehicle_type ] ) ) {
		$vehicle_type = array_key_first( $maps );
	}
	$vehicle_map = $maps[ $vehicle_type ];
	$vehicle_options = wpsd_get_vehicle_options();
	
	//= Is this type grouped?
	$no_group_options = '';
	if ( isset( $vehicle_options['no_group_options'] ) ) {
		if ( is_array( $vehicle_options ) 
			&& isset( $vehicle_options['no_group_options'] ) 
			&& $vehicle_options['no_group_options'] ) {

			$no_group_options = ' wpsd-edit-no-group-options';
		}
	}

	//= Create  variable to store a CSS class if the toggles should be hidden
	$hide_toggles = '';

	//= Can users manually enter options
	$group_options_list = false;
	$group_options_list_css = '';
	if ( isset( $vehicle_options['group_options_list'][ $vehicle_type ] ) && ( $vehicle_options['group_options_list'][ $vehicle_type ] ) ) {
		$group_options_list = true;
		$group_options_list_css = ' active';
		$hide_toggles = ' in-active';
	}

	//= These are the options specific to the vehicle itself
	//= not to be confused with $vehicle_options - which are options for all vehicles
	//= I know, $vehicle_settings would have been better
	//= if you agree then do a pull request ;-)
	$options = get_post_meta( $post_id, '_options', true );
	if ( empty( $options ) ) {
		$options_array = array();
	}
	if ( false !== strpos( $options, ',' ) ){
		$options_array = explode( ',', $options );
	} else {
		$options_array = array( $options );
	}
	
	$x = '';
	$x .= '<div class="wpsd-vehicle-options-wrap' . esc_attr( $no_group_options ) . '" data-post-id="' . esc_attr( $post_id ) . '">';
		$fields = array();

		if ( isset( $vehicle_map['options' ] ) ) {
			if ( is_array( $vehicle_map['options' ] ) ) {
				foreach( $vehicle_map['options' ] as $options_group=>$options_sub_groups ) {
                    $group_html = '';
					$group_html .= '<div class="wpsd-vehicle-options-group wpsd-vehicle-options-group-' . sanitize_title( $options_group ) . '" data-slug="' . sanitize_title( $options_group ) . '">';
						$group_html .= '<div class="wpsd-vehicle-options-group-title" data-slug="' . sanitize_title( $options_group ) . '">';
							$group_html .= ucwords( esc_html( $options_group ) );
						$group_html .= '</div>';
						if ( is_array( $options_sub_groups ) ) {
							$args = array(
								'options_group' => $options_group,
								'vehicle_type' => $vehicle_type,
								'options_array' => $options_array,
							);
                            $options_html = '';
							foreach( $options_sub_groups as $options_sub_group=>$options ) {
								$args['options_sub_group'] = $options_sub_group;
								$args['options'] = $options;
								$options_html .= wpsd_get_option_group( $args );
							} //= end foreach( $specs as $spec=>$spec_settings ) {
							$options_html .= '<div class="wpsd-clear"></div>';
                            $group_html .= $options_html;
						}
					$group_html .= '</div>';
                    
                    if ( '<div class="wpsd-clear"></div>' !== $options_html ) {
                        $x .= $group_html;
                    }
				} //= foreach( $vehicle_map['options' ] as $options_group=>$options_sub_groups ) {
			}
		}
	$x .= '</div>';
	$x .= '<div class="wpsd-clear"></div>';
	return $x;

}

function wpsd_get_option_group( $args ) {
	$group_empty = true;
	
	$sub_group_slug = sanitize_title( $args['options_sub_group'] );
	$x = '';
	$x .= '<div class="wpsd-vehicle-options-sub-group wpsd-vehicle-options-sub-group-' . $sub_group_slug . '" data-slug="' . $sub_group_slug . '">';
		$x .= '<div class="wpsd-vehicle-options-sub-group-title">';
			$x .= esc_html( $args['options_sub_group'] );
		$x .= '</div>';

		if ( false !== strpos( $args['options'], ',' ) ) {
			$option_items = explode( ',', $args['options'] );
			$field_args = array(
				'type' => $args['vehicle_type'],
				'group' => $args['options_group'],
				'sub_group' => $sub_group_slug,
			);

			foreach( $option_items as $option_item ) {
				$checked = false;
				if ( in_array( $option_item, $args['options_array'] ) ) {
					$checked = true;
				}
				$option_item = trim( $option_item );

				if ( $checked ) {
					$group_empty = false;
					$slug = sanitize_title( $option_item );
					$_slug = str_replace( '-', '_', $slug );
					$x .= '<div class="wpsd-vehicle-options-item-wrap">';
						$x .= '<div class="vdp-check">&#10003;</div>';
						$x .= '<div class="wpsd-vehicle-options-item wpsd-vehicle-options-item-' . esc_attr( $_slug ) . '">';
							$x .= esc_html( $option_item );
						$x .= '</div>';
					$x .= '</div>';	
				}

			}
		} else {
			$x .= '<div class="wpsd-vehicle-options-item-wrap">';
			$x .= '</div>';
		}
	$x .= '</div>';
	if ( $group_empty ) {
		return '';
	}
	return $x;
}

function wpsd_get_thumbnail_row( $thumbnails ) {
	$html = '';
	if ( is_array( $thumbnails ) && 0 < count( $thumbnails ) ) {
		$html .= '<div class="vdp-thumbnails-wrap">';
			$html .= '<div class="vdp-thumbnails">';
				foreach( $thumbnails as $key=>$thumbnail ) {
					if ( empty( $thumbnail ) ) {
						continue;
					}
					$html .= '<div class="vdp-thumbnail-wrap" data-count="' . esc_attr( $key ) . '" data-src="' . esc_url( $thumbnail ) . '">';
						$html .= '<img src="' . esc_url( $thumbnail ) . '" class="vdp-thumbanil" />';
					$html .= '</div>';
				}
			$html .= '</div>';
			$html .= '<div class="vdp-thumbnails-view-all">';
                $html .= '<i class="dashicons dashicons-images-alt"></i>';
				$html .= __( 'View All Images', 'wp-super-dealer' );
			$html .= '</div>';
		$html .= '</div>';
	}
	return $html;
}

function wpsd_get_vdp_location( $post_id, $location, $vehicle = false ) {
	$html = '';
	if ( ! isset( $location['name'] ) || empty( $location['name'] ) ) {
		if ( ! isset( $location['slug'] ) ) {
			//= no location information was sent
			return $html;
		}
		$location['name'] = $location['slug'];
	}
	$html .= '<div class="vdp-location-wrap vdp-location-wrap-' . esc_attr( $location['slug'] ) . '" data-location="' . esc_attr( $location['slug'] ) . '">';
		$html .= '<div class="vdp-location-name">';
			$html .= esc_html( $location['name'] );
		$html .= '</div>';
		$html .= '<div class="wpsd-clear"></div>';
	
		$html .= '<div class="vdp-location-address-wrap">';
			$address_fields = array(
				'address', 'city', 'state', 'zip'
			);
			foreach( $address_fields as $address_field ) {
				$html .= '<div class="vdp-location-' . esc_attr( $address_field ) . '">';
					if ( isset( $location[ $address_field ] ) ) {
						$html .= esc_html( $location[ $address_field ] );
					}
				$html .= '</div>';
			}
		$html .= '</div>';
		$html .= '<div class="wpsd-clear"></div>';
	
		$html .= '<div class="vdp-location-conact-name">';
			if ( isset( $location[ 'contact_name' ] ) ) {
				$html .= esc_html( $location[ 'contact_name' ] );
			}
		$html .= '</div>';
	
		$html .= '<div class="vdp-location-phone-wrap">';
			if ( isset( $location[ 'phone_contact' ] ) ) {
				$html .= esc_html( $location[ 'phone_contact' ] );
			}
		$html .= '</div>';
		$html .= '<div class="wpsd-clear"></div>';

		$html .= '<div class="vdp-location-links">';
			$links = array(
				'contact_form', 'trade_in', 'finance', 'warranty',
			);
			foreach( $links as $link ) {
				if ( isset( $location[ 'url_' . $link ] ) 
					&& ! empty( $location[ 'url_' . $link ] ) ) 
				{
					$filtered_link = $location[ 'url_' . $link ];
					$filtered_link = wpsd_tag_filter( $post_id, $filtered_link, $vehicle );
					$html .= '<div class="vdp-link-item vdp-link-item-' . esc_attr( $link ) . '">';
						$html .= '<a href="' . esc_url( $filtered_link ) . '" target="_blank" class="vdp-link">';
							$title = str_replace( '_', ' ', $link );
							$title = ucwords( $title );
                            $title = apply_filters( 'wpsd_vdp_button_label_filter', $title, $post_id, $filtered_link );
							$html .= esc_html( $title );
						$html .= '</a>';
					$html .= '</div>';
				}
			}
		$html .= '</div>';
		$html .= '<div class="wpsd-clear"></div>';
	
	$html .= '</div>';
	$html = apply_filters( 'vdp_location_filter', $html, $location );
    
	return $html;
}

function wpsd_kses_vdp( $content ) {
    $elements = array(
        'a' => array(
            'id' => array(),
            'name' => array(),
            'href' => array(),
            'title' => array(),
            'class' => array(),
            'target' => array()
        ),
        'h2' => array(
            'class' => array(),
        ),
        'h3' => array(
            'class' => array(),
        ),
        'h4' => array(
            'class' => array(),
        ),
        'img' => array(
            'id' => array(),
            'name' => array(),
            'class' => array(),
            'width' => array(),
            'onerror' => array(),
            'src' => array(),
            'style' => array(),
        ),
        'br' => array(),
        'p' => array(),
        'i' => array(
            'class' => array(),
            'title' => array(),
        ),
        'div' => array(
            'id' => array(),
            'name' => array(),
            'class' => array(),
            'data-name' => array(),
            'data-id' => array(),
            'data-slug' => array(),
            'data-base64' => array(),
            'data-option' => array(),
            'data-value' => array(),
            'data-checked' => array(),
            'data-section' => array(),
            'data-type' => array(),
            'data-group' => array(),
            'data-sub-group' => array(),
            'data-specification-group' => array(),
            'data-specification-group-value' => array(),
            'data-option-group' => array(),
            'data-option-group-value' => array(),
            'data-option-sub-group' => array(),
            'data-option-sub-group-value' => array(),
            'data-field' => array(),
            'data-field-slug' => array(),
            'data-car-link' => array(),
            'data-src-id' => array(),
            'data-src' => array(),
            'data-cnt' => array(),
        ),
        'span' => array(
            'id' => array(),
            'name' => array(),
            'class' => array(),
            'data-video-url' => array(),
            'data-field-slug' => array(),
        ),
        'iframe' => array(
            'src' => array(),
            'frameborder' => array(),
            'allow' => array(),
        ),
        'small' => array(
            'class' => array(),
            'title' => array(),
        ),
        'ul' => array(
            'class' => array(),
        ),
        'ol' => array(),
        'li' => array(),
        'table' => array(
            'class' => array(),
        ),
        'tr' => array(),
        'td' => array(),
    );
    $elements = apply_filters( 'wpsd_kses_vdp_filter', $elements, $content );

    if ( defined( 'WPSD_RAW_VDP' ) && true == WPSD_RAW_VDP ) {
        return $content;
    }

    return wp_kses( $content, $elements );
}
?>