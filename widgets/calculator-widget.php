<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_calculator_load_widgets() {
	register_widget( 'wpsd_calculator_Widget' );
}
add_action( 'widgets_init', 'wpsd_calculator_load_widgets' );

class wpsd_calculator_Widget extends WP_Widget {
	/**
	 * Widget setup.
	 */
	function __construct() {
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'wpsd_calculator', 'description' => __( 'Loan Calculator.', 'wp-super-dealer' ) );
		/* Widget control settings. */
		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'wpsd_calculator-widget' );
		/* Create the widget. */
		parent::__construct( 'wpsd_calculator-widget', __( 'WPSuperDealer Loan Calculator', 'wp-super-dealer' ), $widget_ops, $control_ops );
	}
	/**
	 * How to display the widget on the screen.
	 */
	function widget( $args, $atts ) {
		/* Our variables from the widget settings. */
		$title = apply_filters( 'widget_title', $atts['title'] );
		/* Before widget (defined by themes). */
		echo esc_html( $args['before_widget'] );
		/* Display the widget title if one was input (before and after defined by themes). */
		if ( ! empty( $title ) ) {
			echo esc_html( $args['before_title'] . $title . $args['after_title'] );
		}
		if ( is_singular( WPSD_POST_TYPE ) ) {
			global $wp_query;
			$post_id = $wp_query->post->ID;
			$price = get_post_meta( $post_id, "_price_value", true );
			echo wpsd_calculator_form( $atts );
		} else {
			echo wpsd_calculator_form( $atts );
		}
		/* After widget (defined by themes). */
		echo esc_html( $args['after_widget'] );
	}
	/**
	 * Update the widget settings.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		/* Strip tags to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['price'] = strip_tags( $new_instance['price'] );
        $instance['down'] = strip_tags( $new_instance['down'] );
		$instance['apr'] = strip_tags( $new_instance['apr'] );
		$instance['term'] = strip_tags( $new_instance['term'] );
		$instance['disclaimer1'] = strip_tags( $new_instance['disclaimer1'] );
		$instance['disclaimer2'] = strip_tags( $new_instance['disclaimer2'] );
		return $instance;
	}
	/**
	 * Displays the widget settings controls on the widget panel.
	 * Make use of the get_field_id() and get_field_name() function * when creating your form elements. This handles the confusing stuff.
	 */
	function form( $instance ) {
		/* Set up some default widget settings. */
		$defaults = array(
			'title' => __( 'Loan Calculator', 'wp-super-dealer' ),
			'price' => __( '25000', 'wp-super-dealer' ),
            'down' => __( '1500', 'wp-super-dealer' ),
			'apr' => __( '10', 'wp-super-dealer' ),
			'term' => __( '60', 'wp-super-dealer' ),
			'disclaimer1' => __( 'It is not an offer for credit nor a quote.', 'wp-super-dealer' ),
			'disclaimer2' => __( 'This calculator provides an estimated monthly payment. Your actual payment may vary based upon your specific loan and final purchase price.', 'wp-super-dealer' )
		 );
		$instance = wp_parse_args( (array) $instance, $defaults );

        $html = '';
		$html .= '<div class="wpsd-calc-admin-form">';
            $html .= '<div class="wpsd_wide">';
    			$html .= '<label for="'. esc_attr( $this->get_field_id( 'title' ) ) . '">' . __( 'Title:', 'wp-super-dealer' ) . '</label>';
	    		$html .= '<input id="' . esc_attr( $this->get_field_id( 'title' ) ) . '" name="' . esc_attr( $this->get_field_name( 'title' ) ) . '" value="' . esc_attr( $instance['title'] ) . '" class="wpsd_wide" />';
            $html .= '</div>';
            $html .= '<div class="wpsd_wide">';
		    	$html .= '<label for="' . esc_attr( $this->get_field_id( 'price' ) ) . '">' . __( 'Price:', 'wp-super-dealer' ) . '</label>';
			    $html .= '<input id="' . esc_attr( $this->get_field_id( 'price' ) ) . '" name="' . esc_attr( $this->get_field_name( 'price' ) ) . '" value="' . esc_attr( $instance['price'] ) . '" class="wpsd_wide" />';
            $html .= '</div>';
            $html .= '<div class="wpsd_wide">';
                $html .= '<label for="' . esc_attr( $this->get_field_id( 'down' ) ) . '">' . __( 'Down:', 'wp-super-dealer' ) . '</label>';
                $html .= '<input id="' . esc_attr( $this->get_field_id( 'down' ) ) . '" name="' . esc_attr( $this->get_field_name( 'down' ) ) . '" value="' . esc_attr( $instance['down'] ) . '" class="wpsd_wide" />';
            $html .= '</div>';
            $html .= '<div class="wpsd_wide">';
                $html .= '<label for="' . esc_attr( $this->get_field_id( 'apr' ) ) . '">' . __( 'APR:', 'wp-super-dealer' ) . '</label>';
                $html .= '<input id="' . esc_attr( $this->get_field_id( 'apr' ) ) . '" name="' . esc_attr( $this->get_field_name( 'apr' ) ) . '" value="' . esc_attr( $instance['apr'] ) . '" class="wpsd_wide" />';
            $html .= '</div>';
            $html .= '<div class="wpsd_wide">';
			    $html .= '<label for="' . esc_attr( $this->get_field_id( 'term' ) ) . '">' . __( 'Term:', 'wp-super-dealer' ) . '</label>';
			    $html .= '<input id="' . esc_attr( $this->get_field_id( 'term' ) ) . '" name="' . esc_attr( $this->get_field_name( 'term' ) ) . '" value="' . esc_attr( $instance['term'] ) . '" class="wpsd_wide" />';
			$html .= '</div>';
            $html .= '<div class="wpsd_wide">';
                $html .= '<label for="' . $this->get_field_id( 'disclaimer1' ) . '">' . __( 'Disclaimer #1:', 'wp-super-dealer' ) . '</label>';
                $html .= '<textarea class="calc_disclaimer" id="' . $this->get_field_id( 'disclaimer1' ) . '" name="' . $this->get_field_name( 'disclaimer1' ) . '">' . $instance['disclaimer1'] . '</textarea>';
            $html .= '</div>';
            $html .= '<div class="wpsd_wide">';
			    $html .= '<label for="' . esc_attr( $this->get_field_id( 'disclaimer2' ) ) . '">' . __( 'Disclaimer #2:', 'wp-super-dealer' ) . '</label>';
			    $html .= '<textarea class="calc_disclaimer" id="' . esc_attr( $this->get_field_id( 'disclaimer2' ) ) . '" name="' . esc_attr( $this->get_field_name( 'disclaimer2' ) ) . '">' . esc_attr( $instance['disclaimer2'] ) . '</textarea>';
            $html .= '</div>';
		$html .= '</div>';
        
        $elements = array(
            'a' => array(
                'href' => array(),
                'title' => array()
            ),
            'img' => array(
                'width' => array(),
                'onerror' => array(),
                'src' => array(),
            ),
            'br' => array(),
            'div' => array(),
            'select' => array(
                'id' => array(),
                'class' => array(),
                'data-type' => array(),
                'data-post-id' => array(),
            ),
            'textarea' => array(
                'id' => array(),
                'class' => array(),
                'data-type' => array(),
                'data-post-id' => array(),
            ),
            'option' => array(
                'value' => array(),
            ),
            'input' => array(
                'value' => array(),
                'data-type' => array(),
                'data-post-id' => array(),
                'class' => array(),
                'type' => array(),
                'size' => array(),
                'id' => array(),
            ),
        );

        echo wp_kses( $html, $elements );
	}
}
?>