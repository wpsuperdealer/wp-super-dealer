<?php
/**
* Plugin Name: WP Super Dealer
* Plugin URI: http://WPSuperDealer.com/product/wp-super-dealer/
* Description:  WPSuperDealer was designed specifically for vehicle sales. It was built for car dealers, boat dealers, rv dealers, atv dealers and just about any other vehicle dealer you can think of. Invisible Jets might be a bit tough, what with them being Invisible and all, but we've got dealers for just about every other mode of conveyance covered.
* Author: WPSuperDealer
* Version: 1.5.2
* Author URI: http://WPSuperDealer.com/
* Text Domain: wp-super-dealer
* Domain Path: /languages
* License: GPL2
* WPCD ID: 101
*
* Bitbucket Plugin URI: https://bitbucket.org/wpsuperdealer/wpsuperdealer/
* Bitbucket Branch:     master  
*/

/**
 * Return 403 error if loaded directly
 */
if ( ! defined( 'ABSPATH' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit;
}

/**
 * @const WPSD_VER
 */
define( 'WPSD_VER', '1.5.2' );

/**
 * Define the path to WPSuperDealer PlugIn Folder
 */
$wpsd_pluginpath = plugins_url( '/', __FILE__ );

/**
 * @const WPSD_PATH
 */
define( 'WPSD_PATH', $wpsd_pluginpath );

$wpsd_dir = plugin_dir_path( __FILE__ );
/**
 * @const WPSD_PATH
 */
define( 'WPSD_DIR', $wpsd_dir );

/**
 * Define constant to store our post type
 *
*/
if ( ! defined( 'WPSD_POST_TYPE' ) ) {
	define( 'WPSD_POST_TYPE', 'vehicle' );
}

/**
* Define main user permission needed to edit vehicle data
*/
if ( ! defined( 'WPSD_ADMIN_CAP' ) ) {
	define( 'WPSD_ADMIN_CAP', 'manage_options' );
}

/**
 * Init Functions
 */
require_once( 'includes/wpsd-init.php' );

/**
 * Rest API Functions
 */
require_once( 'rest-api/rest-routes.php' );

/**
 * Load core styles and scripts
 *
 * @todo
 *     - Refine and minimize all CSS and JS
 */
require_once( 'includes/wpsd-scripts-styles.php' );

/**
 * Require our file that handles from end ajax calls
 *
 */
require_once( 'includes/wpsd-ajax-handler.php' );

/**
 * Load WPSuperDealer Custom Query
 *
 * @todo
 *     - Consolidate search and archive queries into one function
 *     - Move sort function into its own file
 */
require_once( 'includes/wpsd-query.php' );

/**
 * Register custom post type and taxonomies
 *
 */
require_once( 'includes/create-post-types-tax.php' );

/**
 * Functions for WPSuperDealer admin settings
 *
 * @todo
 *     - Document functions
 *     - Make initial setup as quick and easy as possible
 */
require_once( 'admin/wpsd-admin.php' );

/**
 * Template redirect if included template files are used
 *
 * @todo
 *     - Depracate option to use included template files
 */
require_once( 'theme-files/wpsd-template-redirect.php' );

/**
 * Register custom sidebar areas
 *
 */
require_once( 'includes/register-sidebars.php');

/**
 * Function to return vehicle price
 *
 * @todo
 *     - Document functions
 */
require_once( 'includes/vehicle-price.php' );

/**
 * Functions for vehicle contact information
 *
 * @todo
 *     - Document functions
 */
require_once( 'includes/get-contact-info.php' );

/**
 * Functions for displaying vehicle history reports
 *
 * @todo
 *     - Document functions
 */
require_once( 'includes/wpsd-history-reports.php' );

/**
 * Functions for displaying vehicles
 *
 * @todo
 *     - Document functions
 */
require_once( 'templates/srp-item.php' );
require_once( 'templates/vdp-item.php' );

/**
 * Functions that supply template parts for vehicle display
 *
 * Function wpsd_get_vehicle( $post_id ) for returning vehicle data array
 *
 * @todo
 *     - Document functions
 */
require_once( 'includes/wpsd-template.php' );

/**
 * Functions for payement calculator widget
 *
 * @todo
 *     - Document functions
 */
require_once( 'includes/payment-calculator.php' );

/**
 * Functions to build fields for vehicle search forms
 *
 * @todo
 *     - Document functions
 */
require_once( 'search/search.php' );

/**
 * Functions to handle custom vehicle sorting
 *
 * @todo
 *     - Document functions
 */
require_once( 'sort/sort.php' );

//= TO DO Future: document, move file
require_once( 'classes/class-wpsd-fields.php' );

/**
 * Elementor Inventory widget
 *
 * @todo
 *     - Document class
 */
require_once( 'includes/wpsd-elementor.php' );

/**
 * Calculator widget
 *
 * @todo
 *     - Document functions
 */
require_once( 'widgets/calculator-widget.php' );

/**
 * Functions for our built in pagniation for vehicle listings
 *
 * @todo
 *     - Document functions
 */
require_once( 'includes/wpsd-page-navi.php' );

/**
 * Functions for inventory shortcode [wpsd_inventory]
 *
 * @todo
 *     - Document functions
 */
require_once( 'shortcodes/wpsd-inventory.php' );

/**
 * Functions to manage shortcodes
 *
 * @todo
 *     - Document functions
 */
require_once( 'shortcodes/shortcodes.php' );

/**
 * Functions for turning off Gutenberg support for editing vehicles
 *
 */
require_once( 'blocks/wpsd-gutenberg.php' );

/**
 * Functions and files for adding WPSuperDealer Blocks to Gutenberg
 *
 */
require_once( 'blocks/wpsd-blocks.php' );

/**
 * Load Localisation files.
 *
 * Note: the first-loaded translation file overrides any following ones if the same translation is present.
 *
 * Locales found in:
 *      - WP_LANG_DIR/wp-super-dealer/wp-super-dealer-LOCALE.mo
 *      - WP_LANG_DIR/plugins/wp-super-dealer-LOCALE.mo
 */
function wpsuperdealer_language(){
    $domain = 'wp-super-dealer';
    // The "plugin_locale" filter is also used in load_plugin_textdomain()
    $locale = apply_filters( 'plugin_locale', get_locale(), $domain );

    load_plugin_textdomain( $domain, FALSE, dirname(plugin_basename(__FILE__)).'/languages/' );
}
add_action( 'plugins_loaded', 'wpsuperdealer_language' );

/**
 * Run activation
 *
 * Set default options
 *
 * Set welcome screen transient
 */
function wpsd_activate() {
    $options = get_option( 'wpsd_options', '' );
    if ( empty( $options ) ) {
        wpsd_set_default_options();
    }
	set_transient( '_wpsd_welcome_screen_activation_redirect', true, 30 );
	//= register post type and flush rewrite rules
	wpsd_create_post_type();
	wpsd_add_caps();
	flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'wpsd_activate' );

/**
 * Run deactivation
 *
 * Do not delete settings
 *
 * @todo
 *     - add option to delete settings and post data
 */
function wpsd_deactivate() {
	//delete_option('wpsd_options');
	//$wpsd_options = array();
	//$wpsd_options = get_option( 'wpsd_options' );
	//update_option( 'wpsd_options', $wpsd_options );
}
register_deactivation_hook( __FILE__, 'wpsd_deactivate' );

?>