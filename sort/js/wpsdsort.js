// JavaScript Document
jQuery( document ).ready(function($) {
	
	var updateQueryStringParameter = function (url, key, value) {
		if ( ! url ) {
			url = window.location.href;
		}
		var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
		var separator = url.indexOf('?') !== -1 ? "&" : "?";
		if (url.match(re)) {
			return url.replace(re, '$1' + key + "=" + value + '$2');
		} else {
			return url + separator + key + "=" + value;
		}
	};

	var cdtb_submit_sort = function( type, direction ) {
		if ( 'undefined' !== typeof( wpsdSearchParams.wpsdsp_autoload ) ) {
			if ( true == wpsdSearchParams.wpsdsp_autoload ) {
				//= Auto load inventory is on
				first_load = false;
				if ( $( '.selectOptions:first' ).length > 0 ) {
					$( '.selectOptions:first' ).trigger( 'change' );
					return;
				}
			}
		}

		var url = window.location.href;
		url = updateQueryStringParameter( url, 'order_by', type );
		url = updateQueryStringParameter( url, 'order_by_dir', direction );
		
		url = url.replace('#/page/1', '');

		$(location).attr('href',url);
	};

	$( document ).on('change', 'select.wpsd-sort-by-combined', function() {
		var type = $( this ).find( ':selected' ).val();
		var direction = $( this ).find( ':selected' ).data( 'direction' );
		$( '.sort-item' ).removeClass( 'cdtb_selected' );
		$( this ).find( ':selected' ).addClass( 'cdtb_selected' );

		$( this ).data( 'type', type );
		$( this ).data( 'direction', direction );
		cdtb_submit_sort( type, direction );
	});

	$( document ).on('click', '.sort-item', function() {
		var type = $( this ).data( 'type' );
		var direction = $( this ).data( 'direction' );
		$( '.sort-item' ).removeClass( 'cdtb_selected' );
		$( this ).addClass( 'cdtb_selected' );

		$( '.cdtb_select_sort' ).data( 'type', type );
		$( '.cdtb_select_sort' ).data( 'direction', direction );
		cdtb_submit_sort( type, direction );
	});
	
	$( document ).on( 'change', '#wpsd-sort-by-combined_OLD', function() {
		var value = $( this ).val();
		var direction = $( this ).find( ':selected' ).data( 'direction' );

		$( '#order_by' ).val( value );
		$( '#order_by_dir' ).val( direction );

		var current_filters = $( '#wpsdsp-current-filters-middle .selected-wrap' );
		if ( 0 < current_filters.length ) {
			var search = {};
			$.each( current_filters, function( i, e ) {
				var field = $( e ).data( 'field' );
				var value = $( e ).data( 'value' );
				if ( '' !== value ) {
					if ( 'undefined' === typeof( search[ field ] ) ) {
						search[ field ] = value;
					} else {
						search[ field ] += '|' + value;
					}
					

				}
			});
			//= make the new querystring
			var search_string = '?vehicle=1';
			search_string += '&order_by=' + value;
			search_string += '&order_by_dir=' + direction;

			$.each( search, function( i, e ) {
				console.log( i );
				console.log(e);
				search_string += '&' + i + '=' + e;
			});
		}
		
		window.location = search_string;
	});

});