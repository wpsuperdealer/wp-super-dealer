// JavaScript Document
jQuery( document ).ready(function($) {

	$( document ).on( 'click', '.wpsdsort-admin-label', function() {
		var status = $( this ).data( 'active' );
		var field = $( this ).data( 'item' );
		if ( 'wpsdsort_inactive' === status ) {
			$( '.wpsdsort_' + field + '_wrap' ).slideDown();
			$( this ).data( 'active', 'wpsdsort_active' );
		} else {
			$( '.wpsdsort_' + field + '_wrap' ).slideUp();
			$( this ).data( 'active', 'wpsdsort_inactive' );
		}
	});

});