<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

define( 'WPSDSORT_VERSION', '0.1.1' );

//= Require our admin page
require_once( 'admin/wpsdsort-admin.php' );

//= Require our file that handles which items we can sort on
require_once( 'includes/wpsdsort-items.php' );

//= Require our file that hooks the WPSuperDealer Query and modifies the sorting
require_once( 'includes/wpsdsort-query.php' );

//= Require our file that swaps out the default sorting form
require_once( 'includes/wpsdsort-form.php' );

//= Require our file that swaps out the Template Builder Add-On's sorting form
require_once( 'includes/wpsdsort-template-builder.php' );

//= Register our admin CSS styles
function wpsdsort_admin_enqueue_style() {
	wp_enqueue_style( 'wpsdsort-admin-css', WPSD_PATH . 'sort/css/wpsdsort-admin.css', false, WPSDSORT_VERSION);
}
add_action( 'admin_enqueue_scripts', 'wpsdsort_admin_enqueue_style' );

//= Register our admin js scripts
function wpsdsort_admin_custom_scripts() {
	wp_register_script( 'wpsdsort-admin-js', WPSD_PATH . 'sort/js/wpsdsort-admin.js', false, WPSDSORT_VERSION );
	wp_localize_script( 'wpsdsort-admin-js', 'cdtbAdminParams', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'plugin_url' => WPSD_PATH,
	) );
	wp_enqueue_script( 'wpsdsort-admin-js' );
}
add_action( 'admin_print_styles', 'wpsdsort_admin_custom_scripts' );

//= Register our front end styles and scripts
function wpsdsort_enqueue_scripts() {
	wp_register_script( 'wpsdsort-js', WPSD_PATH . 'sort/js/wpsdsort.js', array( 'jquery' ), WPSDSORT_VERSION );
	wp_localize_script( 'wpsdsort-js', 'wpsdsortParams', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'spinner' => WPSD_PATH . '/images/wpspin_light.gif',
	) );

	wp_enqueue_script( 'wpsdsort-js' );
}
add_action( 'wp_enqueue_scripts', 'wpsdsort_enqueue_scripts' );
?>