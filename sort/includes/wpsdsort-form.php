<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//= Get the sort form
function wpsd_sort_form( $sort_form = '' ) {
	//= Get our items to sort by
	$items = wpsdsort_items();
	$default_sort = get_option( 'wpsdsort-default-sort', '' );

	$order_by = '';
	$order_by_direction = 'Desc';
	if ( ! empty( $default_sort ) 
		&& is_array( $items ) 
		&& is_array( $default_sort ) )
	{
		if ( isset( $default_sort[ 'first' ] ) && ! empty( $default_sort[ 'first' ] ) ) {
			$order_by = $default_sort[ 'first' ];
			$default_sort_direction = get_option( 'wpsdsort-default-direction', '' );
			if ( ! empty( $default_sort_direction ) ) {
				$order_by_direction = $default_sort_direction;
			}
		}
	}

    //= Did we set the direction with a QueryString
	if ( isset( $_GET['order_by_dir'] ) ) {
		$order_by_direction = sanitize_text_field( $_GET['order_by_dir'] );
	}

	if ( isset( $_GET['order_by'] ) ) {
		$order_by = sanitize_text_field( $_GET['order_by'] );
	}

	//= Blank out the sort form - we're gonna build a whole new one
	$sort_form = '';
	$sort_form .= '<div class="wpsd-sort-form-wrap">';
		$sort_form .= '<form id="frm_wpsd_sort" name="frm_wpsd_sort" action="" method="get">';
			$sort_form .= '<input type="hidden" id="order_by" name="order_by" value="' . esc_attr( $order_by ) . '" />';
			$sort_form .= '<input type="hidden" id="order_by_dir" name="order_by_dir" value="' . esc_attr( $order_by_direction ) . '" />';

			$sort_form .= '<span id="wpsd-sort-by-label" class="wpsd-sort-by-label">' . __( 'Sort By ','wp-super-dealer' ) . '</span>';

			//= Sort By Field
			$sort_form .= '<select id="wpsd-sort-by-combined" name="wpsd-sort-by-combined" class="wpsd-sort-by-combined">';
				foreach( $items as $item=>$data ) {
                    if ( ! empty( $data['label_asc'] ) ) {
                        $sort_form .= '<option value="' . esc_attr( $data['field_name'] ) . '"' . ( ( $data['field_name'] === $order_by && ( 'asc' === $order_by_direction || 'Asc' === $order_by_direction ) ) ? ' selected' : '' ) . ' data-direction="asc">' . esc_attr( $data['label_asc'] ) . '</option>';
                    }
                    if ( ! empty( $data['label_desc'] ) ) {
                        $sort_form .= '<option value="' . esc_attr( $data['field_name'] ) . '"' . ( ( $data['field_name'] === $order_by && ( 'desc' === $order_by_direction || 'Desc' === $order_by_direction ) ) ? ' selected' : '' ) . ' data-direction="desc">' . esc_attr( $data['label_desc'] ) . '</option>';
                    }
				}
			$sort_form .= '</select>';

		$sort_form .= '</form>';
	$sort_form .= '</div>';
	$sort_form = apply_filters( 'wpsd_sort_filter', $sort_form );

    $elements = wpsd_sort_form_elements();
    //= lock it down to just our elements
    $sort_form = wp_kses( $sort_form, $elements );
    
	//= Return our new sorting form
	return $sort_form;
}

function wpsd_sort_form_elements() {
    $elements = array(
        'div' => array(
            'class' => array(),
        ),
        'form' => array(
            'id' => array(),
            'name' => array(),
            'action' => array(),
            'method' => array(),
        ),
        'input' => array(
            'id' => array(),
            'name' => array(),
            'value' => array(),
            'type' => array(),
        ),
        'span' => array(
            'id' => array(),
            'class' => array(),
        ),
        'select' => array(
            'id' => array(),
            'name' => array(),
            'class' => array(),
        ),
        'option' => array(
            'value' => array(),
            'data-direction' => array(),
            'selected' => array(),
        ),
    );
    $elements = apply_filters( 'wpsd_sort_esc_elements_filter', $elements );
    return $elements;
}
?>