<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//= Link into the WPSuperDealer Template Builder and update the sorting options
add_filter( 'wpsdtb_sort_links_filter', 'wpsdsort_links_filter', 10, 2 );
function wpsdsort_links_filter( $links, $atts ) {
	global $wpsd_options;
	$items = wpsdsort_items();

	$links = array();
	foreach( $items as $item=>$data ) {
		$links[ $item . '_low_to_high'] = array( 'title' => $data['label_asc'], 'type' => $data['field_name'], 'direction' => 'asc', 'link' => $wpsd_options['inventory_page'] . '?order_by=' . $data['field_name'] . '&order_by_dir=asc' );
		$links[ $item . '_high_to_low'] = array( 'title' => $data['label_desc'], 'type' => $data['field_name'], 'direction' => 'desc', 'link' => $wpsd_options['inventory_page'] . '?order_by=' . $data['field_name'] . '&order_by_dir=desc' );
	}

	return $links;
}
?>