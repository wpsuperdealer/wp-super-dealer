<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//= Hook the WPSuperDealer Query and modify it based on current sorting selection
if ( ! is_admin() ) {
	add_filter( 'wpsd_query_filter', 'wpsdsort_query', 60, 1 );
}

function wpsdsort_query( $wpsd_query ) {
	// $wpsd_query contains the current query
	//= Check to make sure order_by was set
	$items = wpsdsort_items();
	if ( isset( $_GET['order_by'] ) && ! empty( $_GET['order_by'] ) ) {
		//= Sanitize it and set it to a variable
		$order_by = sanitize_text_field( $_GET['order_by'] );
		$order_by_item = '@@@' . $order_by;
		$order_by_item = str_replace( '@@@_', '', $order_by_item );
		$order_by_item = str_replace( '@@@', '', $order_by_item );
		if ( isset( $items[ $order_by_item ] ) ) {
			//= Do not add meta_value to query if we're dealing with a post_field
			if ( 'post_field' !== $items[ $order_by_item ]['type'] ) {
				$wpsd_query['meta_key'] = $order_by;
				$wpsd_query['orderby'] = $items[ $order_by_item ]['sort_type'];
				if ( isset( $_GET['order_by_dir'] ) ) {
					//= Sanitize it and set it to a variable
					$wpsd_query['order'] = sanitize_text_field( $_GET['order_by_dir'] );
				}
			}
		}
	} else {
		unset( $wpsd_query['meta_key'] );
		unset( $wpsd_query['order'] );
		unset( $wpsd_query['orderby'] );
		unset( $wpsd_query['order_by'] );
		unset( $wpsd_query['taxonomy'] );
		unset( $wpsd_query['term'] );
		$sort_meta_query = wpsdsort_default_sort( $wpsd_query );
		$wpsd_meta_query = $wpsd_query['meta_query'];
		$wpsd_query['meta_query'] = wp_parse_args( $wpsd_meta_query, $sort_meta_query );
		$wpsd_query['orderby'] = $wpsd_query['meta_query']['orderby'];
		unset( $wpsd_query['meta_query']['orderby'] );
		if ( isset( $_GET['order_by'] ) && empty( $_GET['order_by'] ) ) {
			if ( isset( $_GET['order_by_dir'] ) ) {
				//= Sanitize it and set it to a variable
				$wpsd_query['order'] = sanitize_text_field( $_GET['order_by_dir'] );
			}
		}
	}

	return $wpsd_query;
}

function wpsdsort_default_sort( $wpsd_query ) {
	$query = array();
	$order_by = array();
	$default_sort_direction = get_option( 'wpsdsort-default-direction', 'desc' );
	$default_sort = array( 
		'first' => '_price',
		'second' => '_make',
		'third' => '_model',
	);
	$sort = get_option( 'wpsdsort-default-sort', $default_sort );

	//= If no default sort has been defined then let's sort by date added and return
	if ( empty( $sort['first'] ) ) {
		$query['orderby'] = 'date';
		return $query;
	}
	
	$items = wpsdsort_items();
	$available_items = wpsdsort_available_items();
	$items = wp_parse_args( $items, $available_items );
	
	$query['relation'] = 'AND';
	//= Add the "sold" clause
	if ( isset( $wpsd_query['meta_query'][0] ) ) {
		if ( 'sold' === $wpsd_query['meta_query'][0]['key'] ) {
			$query['sold_clause'] = array(
				'key' => 'sold',
				'value' => $wpsd_query['meta_query'][0]['value'],
				'compare' => '=',
			);
		}
	}

	//= Add First sort item
	if ( isset( $sort['first'] ) ) {
		if ( ! empty( $sort['first'] ) ) {
			$type = 'CHAR';
            $first = substr( $sort['first'], 1 );
			if ( isset( $items[ $first ] ) && isset( $items[ $first ]['sort_type'] ) ) {
				if ( 'meta_value_num' === $items[ $first ]['sort_type'] ) {
					$type = 'NUMERIC';
				}
			}
			$query['sort_clause_first'] = array(
				'key' => $sort['first'],
				'compare' => 'EXISTS',
				'type' => $type,
			);
			$order_by['sort_clause_first'] = $default_sort_direction;
		}
	}
	//= Add second sort item
	if ( isset( $sort['second'] ) ) {
		if ( ! empty( $sort['second'] ) ) {
			$type = 'CHAR';
            $second = substr( $sort['second'], 1 );
			if ( isset( $items[ $second ] ) && isset( $items[ $second ]['sort_type'] ) ) {
				if ( 'meta_value_num' === $items[ $second ]['sort_type'] ) {
					$type = 'NUMERIC';
				}
			}
			$query['sort_clause_second'] = array(
				'key' => $sort['second'],
				'compare' => 'EXISTS',
				'type' => $type,
			);
			$order_by['sort_clause_second'] = $default_sort_direction;
		}
	}
	//= Add third sort item
	if ( isset( $sort['third'] ) ) {
		if ( ! empty( $sort['third'] ) ) {
			$type = 'CHAR';
            $third = substr( $sort['third'], 1 );
			if ( isset( $items[ $third ] ) && isset( $items[ $third ]['sort_type'] ) ) {
				if ( 'meta_value_num' === $items[ $third ]['sort_type'] ) {
					$type = 'NUMERIC';
				}
			}
			$query['sort_clause_third'] = array(
				'key' => $sort['third'],
				'compare' => 'EXISTS',
				'type' => $type,
			);
			$order_by['sort_clause_third'] = $default_sort_direction;
		}
	}
	$query['orderby'] = $order_by;

    return $query;
}
?>