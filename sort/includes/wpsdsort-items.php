<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//= Returns the array of available vehicle fields to sort
function wpsdsort_available_items() {
	global $wpsd_options;

	$specs = wpsd_get_all_specifications();
	$items = array(
		'price' => array( //= meta field price_value
			'type' => 'meta',
			'field_name' => '_price',
			'label' => __( 'Price', 'wp-super-dealer' ),
			'label_asc' => __( 'Price low to high', 'wp-super-dealer' ),
			'label_desc' => __( 'Price high to low', 'wp-super-dealer' ),
			'sort_type' => 'meta_value_num',
		),
		'mileage' => array( //= meta field price_value
			'type' => 'meta',
			'field_name' => '_mileage',
			'label' => __( 'Mileage', 'wp-super-dealer' ),
			'label_asc' => __( 'Mileage low to high', 'wp-super-dealer' ),
			'label_desc' => __( 'Mileage high to low', 'wp-super-dealer' ),
			'sort_type' => 'meta_value_num',
		),
		'post_date' => array( //= taxonomy vehicle_model
			'type' => 'post_field',
			'field_name' => '', //= We do not want to add a name for post_date because it's the default
			'label' => __( 'Date Added', 'wp-super-dealer' ),
			'label_asc' => __( 'Days on lot - oldest to newest', 'wp-super-dealer' ),
			'label_desc' => __( 'Days on lot - newest to oldest', 'wp-super-dealer' ),
			'sort_type' => 'date',
		),
	);

	if ( count( $specs ) > 0 ) {
		foreach( $specs as $spec=>$option ) {
			$slug = sanitize_title( $spec );
			$_slug = str_replace( '-', '_', $slug );

			if ( ! isset( $items[ $_slug ] ) ) {
				$items[ $_slug ] = array(
					'type' => 'meta',
					'field_name' => '_' . $_slug,
					'label' => $option,
					'label_asc' => $option . __( ' low to high', 'wp-super-dealer' ),
					'label_desc' => $option . __( ' high to low', 'wp-super-dealer' ),
					'sort_type' => 'meta_value',
				);
			}
		}
	}

	return $items;
}

//= Returns the array of additional vehicle fields to sort
function wpsdsort_items() {
	$default_items = default_wpsdsort_items();
//	delete_option( 'wpsdsort-items' );
	$items = get_option( 'wpsdsort-items', $default_items );
	$items = apply_filters( 'wpsdsort_filters', $items );
	return $items;
}

function default_wpsdsort_items() {
	global $wpsd_options;
	$items = array(
		'price' => array( //= meta field price
			'type' => 'meta',
			'field_name' => '_price',
			'label' => __( 'Price', 'wp-super-dealer' ),
			'label_asc' => __( 'Price low to high', 'wp-super-dealer' ),
			'label_desc' => __( 'Price high to low', 'wp-super-dealer' ),
			'sort_type' => 'meta_value_num',
		),
		'mileage' => array( //= meta field mileage
			'type' => 'meta',
			'field_name' => '_mileage',
			'label' => __( 'Mileage', 'wp-super-dealer' ),
			'label_asc' => __( 'Mileage low to high', 'wp-super-dealer' ),
			'label_desc' => __( 'Mileage high to low', 'wp-super-dealer' ),
			'sort_type' => 'meta_value_num',
		),
		'vehicle_year' => array( //= taxonomy vehicle_year
			'type' => 'meta',
			'field_name' => '_vehicle_year',
			'label' => __( 'Year', 'wp-super-dealer' ),
			'label_asc' => __( 'Year oldest to newest', 'wp-super-dealer' ),
			'label_desc' => __( 'Year newest to oldest', 'wp-super-dealer' ),
			'sort_type' => 'meta_value_num',
		),
		'make' => array( //= taxonomy vehicle_make
			'type' => 'meta',
			'field_name' => '_make',
			'label' => __( 'Make', 'wp-super-dealer' ),
			'label_asc' => __( 'Make A-Z', 'wp-super-dealer' ),
			'label_desc' => __( 'Make Z-A', 'wp-super-dealer' ),
			'sort_type' => 'meta_value',
		),
		'model' => array( //= taxonomy vehicle_model
			'type' => 'meta',
			'field_name' => '_model',
			'label' => __( 'Model', 'wp-super-dealer' ),
			'label_asc' => __( 'Model A-Z', 'wp-super-dealer' ),
			'label_desc' => __( 'Model Z-A', 'wp-super-dealer' ),
			'sort_type' => 'meta_value',
		),
		'post_date' => array( //= post table field
			'type' => 'post_field',
			'field_name' => '', //= We do not want to add a name for post_date because it's the default
			'label' => __( 'Date Added', 'wp-super-dealer' ),
			'label_asc' => __( 'Days on lot - oldest to newest', 'wp-super-dealer' ),
			'label_desc' => __( 'Days on lot - newest to oldest', 'wp-super-dealer' ),
			'sort_type' => 'date',
		),
	);
	
	return $items;	
}
?>