<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_sort_admin_settings( $x = '', $echo = false ) {
	if ( ! is_admin() ) {
		return;
	}
	if ( ! current_user_can( WPSD_ADMIN_CAP ) ) {
		return;
	}
	//delete_option( 'wpsdsort-items', $sort_fields );
    $x .= '<h3>';
        $x .= __( 'Vehicle Sorting Options', 'wp-super-dealer' );
    $x .= '</h3>';
    $x .= wpsd_sort_instructions();

	$x .= '<fieldset class="wpsd_admin_group">';
		$x .= '<legend>';
			$x .= __('+ Sorting Options','wp-super-dealer');
		$x .= '</legend>';
		$x .= '<span class="wpsdsort_option_group">';
			$default_sort_direction = get_option( 'wpsdsort-default-direction', 'desc' );
			$default_sort = array( 
				'first' => '_price',
				'second' => '_make',
				'third' => '_model',
			);
			$sort = get_option( 'wpsdsort-default-sort', $default_sort );
			$items = wpsdsort_items();
			$available_items = wpsdsort_available_items();
			$sort_items = wp_parse_args( $items, $available_items );
			ksort( $sort_items );

			$x .= '<div class="wpsdsort-default">';
				$x .= '<h3>';
					$x .= __( 'Default Sort', 'wp-super-dealer' );
				$x .= '</h3>';
				$x .= '<div class="wpsdsort-note">';
					$x .= '<p>';
						$x .= __( 'Select up to 3 different default sorting fields. For example you could sort by Make, then sort by Model and lastly by Price.', 'wp-super-dealer' );
					$x .= '</p>';
					$x .= '<p>';
						$x .= __( 'NOTE: Any field you select for the default sort MUST be checked below.', 'wp-super-dealer' );
					$x .= '</p>';
					$x .= '<p>';
						$x .= __( 'NOTE: If you do not select a field to first sort by then vehicles will be sorted by date added.', 'wp-super-dealer' );
					$x .= '</p>';
					//= TO DO Future: determine if the following note can be removed
					/*
					$x .= '<p>';
						$x .= __( 'NOTE: Date added is not available as part of the multiple sort options and can only be used as a default by not selecting any multiple sort fields.', 'wp-super-dealer' );
					$x .= '</p>';
					*/
				$x .= '</div>';
				$x .= '<div class="wpsdsort-clear"></div>';

				$x .= '<div class="wpsdsort-default-item">';
					$x .= '<label>';
						$x .= __( 'Sort by this first', 'wp-super-dealer' );
					$x .= '</label>';
					$x .= '<select name="wpsdsp_sort[first]" class="wpsdsp_sort_default wpsdsp_sort_first">';
						$x .= '<option value="">' . __( 'Date Published', 'wp-super-dealer' ) . '</option>';
						$x .= '<option value=""></option>';
						foreach( $sort_items as $item=>$data ) {
							if ( ! empty( $data['field_name'] ) ) {
								$x .= '<option value="' . esc_attr( $data['field_name'] ) . '"' . ( $data['field_name'] === $sort['first'] ? ' selected' : '' ) . '>' . esc_html( $data['label'] ) . '</option>';
							}
						}
					$x .= '</select>';
				$x .= '</div>';

				$x .= '<div class="wpsdsort-default-item">';
					$x .= '<label>';
						$x .= __( 'Sort by this Second', 'wp-super-dealer' );
					$x .= '</label>';
					$x .= '<select name="wpsdsp_sort[second]" class="wpsdsp_sort_default wpsdsp_sort_second">';
						$x .= '<option value=""></option>';
						foreach( $sort_items as $item=>$data ) {
							if ( ! empty( $data['field_name'] ) ) {
								$x .= '<option value="' . esc_attr( $data['field_name'] ) . '"' . ( $data['field_name'] === $sort['second'] ? ' selected' : '' ) . '>' . esc_html( $data['label'] ) . '</option>';
							}
						}
					$x .= '</select>';
				$x .= '</div>';

				$x .= '<div class="wpsdsort-default-item">';
					$x .= '<label>';
						$x .= __( 'Sort by this Third', 'wp-super-dealer' );
					$x .= '</label>';
					$x .= '<select name="wpsdsp_sort[third]" class="wpsdsp_sort_default wpsdsp_sort_third">';
						$x .= '<option value=""></option>';
						foreach( $sort_items as $item=>$data ) {
							if ( ! empty( $data['field_name'] ) ) {
								$x .= '<option value="' . esc_attr( $data['field_name'] ) . '"' . ( $data['field_name'] === $sort['third'] ? ' selected' : '' ) . '>' . esc_html( $data['label'] ) . '</option>';
							}
						}
					$x .= '</select>';
				$x .= '</div>';
			$x .= '</div>';

			$x .= '<div class="wpsdsort-default-direction">';
				$x .= '<h3>';
					$x .= __( 'Default Sort Direction', 'wp-super-dealer' );
				$x .= '</h3>';
				$x .= '<div class="wpsdsort-default-direction-item">';
					$x .= '<input type="radio" name="wpsd_sort-default-direction" value="asc" ' . ( 'asc' === $default_sort_direction ? 'checked="checked"' : '' ) . '/>';
					$x .= '<label>';
						$x .= __( 'Asc', 'wp-super-dealer' );
					$x .= '</label>';
				$x .= '</div>';
				$x .= '<div class="wpsdsort-default-direction-item">';
					$x .= '<input type="radio" name="wpsd_sort-default-direction" value="desc" ' . ( 'desc' === $default_sort_direction ? 'checked="checked"' : '' ) . '/>';
					$x .= '<label>';
						$x .= __( 'Desc', 'wp-super-dealer' );
					$x .= '</label>';
				$x .= '</div>';
			$x .= '</div>';

			$x .= '<div class="cdtb_options wpsdsort_options">';
				$x .= '<h3>';
					$x .= __( 'Sort Fields', 'wp-super-dealer' );
				$x .= '</h3>';
				$x .= '<div class="cdtb_options_group wpsdsort_options_group">';
					$x .= '<small class="cdtb_notes">';
						$x .= __( 'Manage the fields available for sorting vehicle results.', 'wp-super-dealer' );
					$x .= '</small>';
		
					$x .= '<div class="wpsdsort-note">';
						$x .= '<p>';
							$x .= __( 'NOTE: If a vehicle does not have a value for a field that is being sorted on then that vehicle will not be displayed.', 'wp-super-dealer' );
						$x .= '</p>';
						$x .= '<p>';
							$x .= __( 'For example, let\'s say a vehicle does not have a mileage value. If a sort is performed on mileage then that vehicle will NOT appear in the results.', 'wp-super-dealer' );
						$x .= '</p>';
					$x .= '</div>';

					//= $available_items is defined above
					ksort( $available_items );
					foreach( $available_items as $item=>$data ) {
						$active = false;
						$class = 'wpsdsort_inactive';
						if ( isset( $items[ $item ] ) ) {
							$active = true;
							$class = 'wpsdsort_active';
						}
						//= TO DO Future: ditch the $item_slug variable in favor of $_slug
						$item_slug = str_replace( '(', '', $item );
						$item_slug = str_replace( ')', '', $item_slug );
						$item_slug = str_replace( ' ', '_', $item_slug );
						$item_slug = str_replace( '-', '_', $item_slug );
						$item_slug = str_replace( '/', '_', $item_slug );
						$item_slug = str_replace( '\\', '-', $item_slug );
						$item_slug = str_replace( '.', '-', $item_slug );
						$item_slug = strtolower( $item_slug );
						
						$slug = sanitize_title( $item );
						$_slug = str_replace( '-', '_', $slug );
						$label = str_replace( '_', ' ', $_slug );
						$label = ucwords( $label );
						
						$x .= '<h4 class="wpsdsort-admin-label" data-active="wpsdsort_inactive" data-item="' . esc_attr( $item_slug ) . '">';
							$x .= '<input type="checkbox" name="wpsd_sort-field-active[' . esc_attr( $_slug ) . ']" value="1" ' . ( $active ? 'checked="checked"' : '' ) . '/>';
							$x .= '<label>';
								$x .= esc_html( $label ) . ' (' . $item . ')';
							$x .= '</label>';
						$x .= '</h4>';
						//= Make all the fields "inactive" (ie. closed) when we start
						$x .= '<div class="wpsdsort_inactive wpsdsort_' . esc_attr( $item_slug ) . '_wrap">';
							$x .= wpsdsort_item_html( $items, $item, $data );
						$x .= '</div>';
					}

					$x .= '<div class="cdtb-clear"></div>';
					$x .= '<small>';
						$x .= __( 'Notes about this field.', 'wp-super-dealer' );
					$x .= '</small>';
				$x .= '</div>';
				$x .= '<div class="cdtb-clear"></div>';
			$x .= '</div>';

		$x .= '</span>';
	$x .= '</fieldset>';

	if ( $echo ) {
		echo wpsd_kses_admin( $x );
	} else {
		return $x;
	}
}

function wpsdsort_item_html( $items, $item, $data ) {
	$html = '<div class="wpsdsort-item wpsdsort-item-' . $item . '">';
		//= These are all the settings we need for each sort item
		$fields = array(
			'type' => '', //= meta
			'field_name' => '', //= _price_value
			'label' => '', //= __( 'Price', 'wp-super-dealer' )
			'label_asc' => '', //= __( 'Price low to high', 'wp-super-dealer' )
			'label_desc' => '', //= __( 'Price high to low', 'wp-super-dealer' )
			'sort_type' => '', //= meta_value_num, meta_value or date

			//'link' => '', //= $wpsd_options['inventory_page']. '?order_by=_' . $item . '_value&order_by_dir=asc'
		);
		$slug = sanitize_title( $item );
		$_slug = str_replace( '-', '_', $slug );

		foreach( $fields as $field=>$value ) {
			$html .= '<div class="wpsdsort-item-field wpsdsort-item-field-' . esc_attr( $field ) . '">';
				$label = str_replace( '_', ' ', $field );
				$label = ucwords( $label );
				$html .= '<label>';
					$html .= $label;
				$html .= '</label>';
				$html .= '<span>';
					$value = '';
					if ( isset( $data[ $field ] ) ) {
						$value = $data[ $field ];
					}
					if ( isset( $items[ $item ][ $field ] ) ) {
						$value = $items[ $item ][ $field ];
					}
			
					if ( 'sort_type' === $field ) {
						$html .= wpsdsort_select_type( $_slug, $field, $value );
					} else {
						$type = 'text';
						$html .= '<input type="' . esc_attr( $type ) . '" class="wpsdsort_item_field wpsdsort_item_field_' . esc_attr( $field ) . '" name="wpsd_sort-field[' . esc_attr( $_slug ) . '][' . esc_attr( $field ) . ']" value="' . esc_attr( $value ) . '" />';
					}
				$html .= '</span>';
			$html .= '</div>';
		}
	$html .= '</div>';

	return $html;
}

function wpsdsort_select_type( $_slug, $field, $value = 'meta_value' ) {
	$html = '';
	$html .= '<select class="wpsdsort_item_field wpsdsort_item_field_' . esc_attr( $field ) . '" name="wpsd_sort-field[' . esc_attr( $_slug ) . '][' . esc_attr( $field ) . ']" />';
		$html .= '<option value="meta_value"' . ( 'meta_value' === $value ? ' selected' : '' ) . '>';
			$html .= __( 'Text', 'wp-super-dealer' );
		$html .= '</option>';
		$html .= '<option value="meta_value_num"' . ( 'meta_value_num' === $value ? ' selected' : '' ) . '>';
			$html .= __( 'Number', 'wp-super-dealer' );
		$html .= '</option>';
	$html .= '</select>';
	return $html;
}

function wpsdsort_save_settings() {
	if ( ! current_user_can( WPSD_ADMIN_CAP ) ) {
		return;
	}

	if ( isset( $_POST['wpsd_sort-field-active'] ) && isset( $_POST['wpsd_sort-field'] )
       && is_array( $_POST['wpsd_sort-field-active'] ) && is_array( $_POST['wpsd_sort-field'] ) 
       ) 
    {
        //= props: https://github.com/WordPress/WordPress-Coding-Standards/issues/1660
        $wpsd_sort_field_active = map_deep( wp_unslash( $_POST['wpsd_sort-field-active'] ), 'sanitize_text_field' );
        $wpsd_sort_field = map_deep( wp_unslash( $_POST['wpsd_sort-field'] ), 'sanitize_text_field' );
		$sort_fields = array();
		if ( is_array( $wpsd_sort_field_active ) ) {
			foreach( $wpsd_sort_field_active as $sort_field=>$checked ) {
				if ( isset( $wpsd_sort_field[ $sort_field ] ) ) {
					$sort_fields[ $sort_field ] = array();
					if ( is_array( $wpsd_sort_field[ $sort_field ] ) ) {
						foreach( $wpsd_sort_field[ $sort_field ] as $field=>$value ) {
							$sort_fields[ $sort_field ][ $field ] = $value;
						}
					}
				}
			}
		}

		update_option( 'wpsdsort-items', $sort_fields );
	}
	
	if ( isset( $_POST['wpsd_sort-default'] ) ) {
		$default_sort = sanitize_text_field( $_POST['wpsd_sort-default'] );
		update_option( 'wpsdsort-default', $default_sort );
	}
	
	if ( isset( $_POST['wpsd_sort-default-direction'] ) ) {
		$default_direction = sanitize_text_field( $_POST['wpsd_sort-default-direction'] );
		update_option( 'wpsdsort-default-direction', $default_direction );
	}

    $default_sort_direction = get_option( 'wpsdsort-default-direction', 'desc' );
	$default_sort = array( 
		'first' => '_price',
		'second' => '_make',
		'third' => '_model',
	);
	$sort = get_option( 'wpsdsort-default-sort', $default_sort );
	$sort = wp_parse_args( $sort, $default_sort );
	if ( isset( $_POST['wpsdsp_sort']['first'] ) ) {
		$sort['first'] = sanitize_text_field( $_POST['wpsdsp_sort']['first'] );
	}
	if ( isset( $_POST['wpsdsp_sort']['second'] ) ) {
		$sort['second'] = sanitize_text_field( $_POST['wpsdsp_sort']['second'] );
	}
	if ( isset( $_POST['wpsdsp_sort']['third'] ) ) {
		$sort['third'] = sanitize_text_field( $_POST['wpsdsp_sort']['third'] );
	}
	update_option( 'wpsdsort-default-sort', $sort );
}

function wpsd_sort_settings_save( $args ) {
	wpsdsort_save_settings();
	return __( 'Settings Saved', 'wp-super-dealer' );
}

function wpsd_sort_instructions() {
    $html = '';
	$html .= '<div class="wpsd-instructions-wrap wpsd-instructions-wrap-sort">';
		$html .= '<div class="wpsd-instructions-title" data-tab="sort">';
			$html .= '<span class="dashicons dashicons-welcome-learn-more"></span>';
			$html .=  __( 'Tips & Tricks - Vehicle Sorting', 'wp-super-dealer' );
		$html .= '</div>';

		$html .=  '<ul class="wpsd-instructions">';
            $html .= '<li>';
                $html .= '<div class="wpsd-tnt-wrap">';
                    $html .= '<div class="wpsd-tnt-col">';
						$html .= '<div class="wpsd-video-wrap">';
                            $html .= '<span class="wpsd-youtube" data-video-url="https://www.youtube.com/embed/PSC-BWIlCd4"></span>';
						$html .= '</div>';
                    $html .= '</div>';
                    $html .= '<div class="wpsd-tnt-col">';
                        $html .= '<h2>';
                            $html .=  __( 'Tips & Tricks for Vehicle Sorting', 'wp-super-dealer' );
                        $html .= '</h2>';
                        $html .= '<ol>';
    
                            $html .= '<li>';
                                $html .=  __( 'By default your vehicles are sorted with the newest vehicles first.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .=  __( 'You can select any vehicle specification field as a sotable field.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .=  __( 'When you check a field it will slide open a panel that will let you select the field type and customize labels.', 'wp-super-dealer' );
                            $html .= '</li>';

                            $html .= '<li>';
                                $html .=  __( 'Keep in mind that a vehicle MUST have data in a field being sorted on or it will not be included in the results.', 'wp-super-dealer' );
                            $html .= '</li>';
                            $html .= '<li>';
                                $html .=  __( 'You must make sure you have checked the box next to any of the fields you select for default sorting.', 'wp-super-dealer' );
                            $html .= '</li>';

                            $html .= '<li>';
                                $html .=  __( 'If a field is numeric then you need to select that after checking the field.', 'wp-super-dealer' );
                            $html .= '</li>';

                            $html .= '<li>';
                                $html .=  __( 'You can use the following shortcode to drop a vehicle sort into a page: [wpsd_sort] - but it only works if there\'s an inventory on the page.', 'wp-super-dealer' );
                            $html .= '</li>';

                        $html .= '</ol>';
                    $html .= '</div>';
                $html .= '</div>'; //= end $html .= '<div class="wpsd-tnt-wrap">';
                $html .= wpsd_instructions_footer();
            $html .= '</li>';

		$html .= '</ul>';
		$html .= '<div class="wpsd-instructions-hide" data-tab="sort">';
			$html .=  __( 'show', 'wp-super-dealer' );
		$html .= '</div>';
	$html .= '</div>';
    return $html;
}

?>