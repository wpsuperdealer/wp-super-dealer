=== WP Super Dealer ===
Contributors: theverylastperson
Donate link: https://wpsuperdealer.com/donate/
Tags: car, dealer, car-dealer, cardealer, automotive, sales, lots, auto, motorcycle, bike, boat, airplane, rvs, tractors, motorhomes, trailers
Requires at least: 3.4.2
Tested up to: 5.9.0
Stable tag: 1.5.2
Requires PHP: 5.4
License: GPLv2
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Bitbucket Plugin URI: https://bitbucket.org/wpsuperdealer/wpsuperdealer/

== Description ==

= WPSuperDealer was designed for vehicle sales. =

WPSuperDealer was built for more than just car dealers. It's flexible design makes it perfectly geared towards boat dealers, rv dealers, atv dealers and just about any other vehicle dealer you can think of. 

Invisible Jets might be a bit tough, what with them being Invisible and all, but we've got dealers for just about every other mode of conveyance covered.

= What can it do? Check out the trailer and find out =

[youtube https://youtu.be/OHYN8RGY4dU ]

Why does our PlugIn have a movie trailer? Because one of us got bored over the Holidays and that's the kind of thing people like us do to entertain ourselves. Gotta admit, the music is spot on - props to Envato, so worth it.

= Supports multiple vehicle types =

If you're building a site for a dealer who sells multiple vehicle types, like both Boats and ATVs, then you can setup each type with their own options and specifications.

= Supports multiple dealers =

Let's say you're building a site for an RV Dealer with 3 locations. You can list the inventory for all 3 and give them different contact information so you can display the correct location information and properly route leads. You also have the power to set each location with its own page displaying just their inventory. You also have the power to put all the vehicles together and then add location to your search form to allow users to filter for just one location if desired.

= Control your vehicle options =

Fully control your vehicle options. Add and remove specifications for each vehicle type. Select which fields are available for your search form and select the fields you want to sort on.

= On screen video tutorials =

Each one of the admin tabs has it's own Tips & Tricks section which includes video tutorials to help you leverage all the settings.

Here's the first video, "WPSuperDealer Startup Guide tab":

[youtube https://youtu.be/IBGbO_OoeqY]

= Powerful Add-ons =

Check out our website
https://WPSuperDealer.com for add-ons, data services, custom development and partnership opportunities.

== Installation ==

= Minimum Requirements =

* PHP 7.2 or greater is recommended
* MySQL 5.6 or greater is recommended

= Automatic installation =

Automatic installation is the easiest option -- WordPress will handles the file transfer, and you won’t need to leave your web browser. To do an automatic install of WPSuperDealer, log in to your WordPress dashboard, navigate to the Plugins menu, and click “Add New.”
 
In the search field type “WPSuperDealer” then click “Search Plugins.” Once you’ve found us,  you can view details about it such as the point release, rating, and description. Most importantly of course, you can install it by! Click “Install Now,” and WordPress will take it from there.

= Manual installation =

Manual installation method requires downloading the WP Super Dealer plugin and uploading it to your web server via your favorite FTP application. The WordPress codex contains [instructions on how to do this here](https://wordpress.org/support/article/managing-plugins/#manual-plugin-installation).

= Updating =

Automatic updates should work without issue, but we recommend you back up your site because it's the number one best practice any web professional should be following.

If you encounter issues after an update, flush the permalinks by going to WordPress > Settings > Permalinks and hitting “Save.” That should return things to normal.

= Sample data =

WP Super Dealer comes with sample vehicles you can use to see how things look. Visit the Startup Guide on the settings page to import the sample vehicles.

== Frequently Asked Questions ==

= Will using this PlugIn give me Super Powers? =

Do you mean like becoming invisible or  shooting lasers out of your eyes? Strong no on that, but we are working on some machine learning stuff that's a lot like reading minds, but that's at [WPSuperTeam.com](https://WPSuperTeam.com) and this is about WPSuperDealer, which gives you the ability to create awesome vehicle sales websites, which is kind of like a Super Power, if wielded properly.

= How do I... xyz? =

Go to Dashboard->Vehicles->Settings | each admin tab has a Tips & Tricks section to provide you with some of the secrets of WPSuperDealer. The default tabs also have video tutorials to help guide you through the settings. Before you go asking too many questions we'd like to suggest you check out the videos and then look through the articles on our [website](https://WPSuperDealer.com). There's a good chance you'll find the answers there.

= Why isn't my search form populating correctly? =

The search form uses a json file to populate its fields. That json file needs to update periodically to correctly reflect the vehicles you have in inventory. If your form isn't populating then go to Dashboard->Vehicles->Settings | Click on the "Search Form" tab and then watch the video in the Tips & Tricks section, then check out the on-screen documentation. That should get you sorted out.

= How do I change the look of my listings? =

If you're a developer then check our website for information on the available filters. You can also checkout our Template Manager add-on. It includes several different templates and gives you the ability to fully customize your vehicle output.

= How do I add a contact form to my listings? =

The default Single Vehicle Page has multiple sidebar areas you can use to add content, including forms, to your vehicles. WPSuperDealer filters text and HTML widgets and replaces data tags with the value from the currently viewed vehicle. On our website there's a guide on [adding Contact Form 7 to your vehicle's that leverages data tags](https://wpsuperdealer.com/how-to-integrate-with-contact-form-7/) and will walk you through setting up your first form.

= What is a data tag and how do I use them? =

Data tags are vehicle specification slugs wrapped inside braces. For example, the data tag for a vehicle's year is {year}, the data tag for mileage would be {mileage}. The slug is a lowercase version of a specification's name with spaces and other non alpha-numeric characters replaced with underscores. This means the data tag for body style is {body_style}. Data tags can be utilized in several places. Most importantly they can be used in text or HTML widgets that are dropped into the sidebar areas for the Single Vehicle pages. On our website there's a guide on [adding Contact Form 7 to your vehicles that leverages data tags](https://wpsuperdealer.com/how-to-integrate-with-contact-form-7/)

= How do I get support? =

Start by visiting our support page at [WordPress.org](https://WordPress.org/support/plugins/WP-Super-Dealer/) - The top post should list our current support policy. If you need assistance with one of our commercial add-ons then please visit our website and contact us directly. It is a violation of WordPress.org policies to support commercial PlugIns in the public support forums, so let's please be courteous of the policy and direct all support for commercial add-ons to [WPSuperDealer.com](https://WPSuperDealer.com)


== Screenshots ==

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif).
2. This is the second screen shot

== Changelog ==
= 1.5.2 =
* FIX: wpsd_get_vehicle() now returns location name for location and slug for location-slug
* NEW: Added target to the "a" tag element in wpsd_kses_srp and wpsd_kses_vdp
* NEW: Added filter wpsd_get_vehicle_price_filter for function wpsd_get_vehicle_price()
= 1.5.1 =
* FIX - When new specs and options are added they can now be sorted before saving
* FIX - Corrected location issue with wpsd_get_vehicle()
= 1.5.0 =
* NEW - Specs & Options can now be sorted and dragged from one group to another.
= 1.4.1 =
* FIX - When saving search fields the json column is now automatically assigned
* FIX - Type has been added as an explicit global field
* FIX - Type and vehicle tags are now part of vehicle array by default
* FIX - Vehicle type can now be added to search form
* FIX - Mobile search form js adjustments
* FIX - Empty specs do not populate their row by default
= 1.4.0 =
* FIX - Adding a max vehicle data tag length of 20 to prevent anything inside {} longer than that from being stripped
* FIX - Making images full width by default instead of inheriting width
* NEW - Adding new plugin logo assets
* NEW - Sort form asc and desc labels now display only if populated and one can be populated and not the other if needed
* NEW - Added constant to templates/vdp-item.php to allow returning raw content
* NEW - Adding filter for widget_block_content to filter vehicle tag data
= 1.3.1 =
* FIX: Model sorting bug
* PROPS: Reported by Steve Worland
= 1.3.0 =
* FIX - Car Demon migration now handles image links
* FIX - Modified search widget to match best practices
* NEW - Search form now provides unique classes for each select item and checkbox
* NEW - Added wpsd_slugify() function to wpsdspjs
* NEW - Added shortcode for sort widget [wpsd_sort]
= 1.2.5 =
* Bump tested up to (based on rc)
* Fixed CD agree checkbox element
* Updated old CDS_ constants to WPSDSP_
= 1.2.4 =
* FIX - Updated remaining legacy CD_ constants to use WPSD_ prefix
* FIX - Updated code that removes featured image on single vehicle pages to prevent attachment loading issues
= 1.2.3 =
* FIX - added true support for true in wpsd_sanitize_array()
= 1.2.2 =
* NEW - Added filter wpsd_vdp_button_label_filter @params $title, $post_id, $filtered_link
* CHANGE - block search form now has simple selects off by default
* FIX - Added class as accepted parameter for a element in wpsd_kses_vdp()
* FIX - Updated simple selects css class to use hyphen to keep consistent
* FIX - Slay the Demon:  Updated cdtb_getCookie() to wpsd_getCookie()
* FIX - Vehicle type array now runs through two sanitize functions, one to build and clean and a second to double chekc before save
* FIX - Correct improper escaping in multi-location edit screen
= 1.2.1 =
* FIX - Additional CSS tweaks for Template Manager compliance
= 1.2.0 =
* NEW - Admin page now has a Forms tab that provides information on integrating with different Form PlugIns
* Props to Kevin Davis @ The Harman Media & Marketing Group | thm2g.com
= 1.1.92 =
* FIX - Removed legacy option to disable updates (cd hold over)
* FIX - Added check to insure ABSPATH is defined before loading any php files
* FIX - Corrected escaping issues in single-vehicles.php
* FIX - Adjusted constant WPSD_PATH and insured it was used in all places
* FIX - Removed legacy files from Anton branch search_sort
= 1.1.91 =
* FIX - Corrected search widget field settings extra quote
= 1.1.9 =
* FIX - Adjusted wpsd_sanitize_array() to be used by map_deep()
= 1.1.8 =
* FIX - Added prefix as available attribute for input elements in the admin tabs
* FIX - Updated default specs for RVs, Boats and Buses
= 1.1.7 =
* FIX - Sanitizing sort data in wpsdsort-admin.php: 253, 261
* FIX - Corrected array_map in wpsd-vehicle-options-tab.php: 622, 625, 628, 631
* FIX - Corrected escaping issue in wpsd-template.php: 9
* FIX - Corrected icon paths to use WPSD_PATH in block.js: 20, 251, 601
* Props to the PlugIn review team at WordPRess.org
= 1.1.6 =
* FIX - Corrected issue preventing spec and options from switching when type was switched
* NEW - Moved location fields into their own functions so they can be called and filtered easily
* FIX - Added isset check to hide_prices in location settings
= 1.1.5 =
* FIX - corrected escaping issue in range items
* FIX - corrected min max range options when removing set limits and relying on dynamic values
= 1.1.4 =
* FIX - fixes #1 - Data Must be Sanitized, Escaped, and Validated
* FIX - fixes #2 - Don’t use esc_ functions to sanitize
* FIX - Reformatted quick links for editing price fields
* FIX - Removed calls to unused images in legacy CSS file
* FIX - fixes #3 - Insure all data output is escaped
= 1.1.3 =
* FIX - updated default search options so autoload works by default
= 1.1.2 =
* FIX - Corrected localization of the search form setting to Load json each time page is loaded.
= 1.1.1 =
* FIX - In wpsd-columns.php line 28: now 'msrp' & line 52: now 'price’
* Props to Jos Wanders (DVB4Free) for reporting the issue
= 1.1.0 =
* NEW - Added down payment field to payment calculator
* NEW - Payment calculator now auto calculates on load and on value changes
* PROPS - Payment calculator changes sponsored by Gogi Randhawa - www.msndrstdcreative.com
* FIX - cd check now looks for function car_demon_activate in place of cd_get_car()
* FIX - Data API tab no longer breaks if valid product key is mistakenly entered
= 1.0.7 =
* Added improved error handling if bad data API Key is entered
= 1.0.6 =
* New admin video for API Keys tab
* Videos on admin tabs now lazy load
* Corrected bad data tabs name on search and sort tabs (props @antonD)
= 1.0.5 =
* Pulled Market Value & Ownership Cost Reports
* Removed Chart.js
* Minor CSS adjustments
* Minor settings adjustment
= 1.0.4 =
* Added checkbox to CD import utility to insure users backup first
* CD Import tweaks
= 1.0.3 =
* Changed all references to cdAdminParams to wpsdAdminParams
* FIX - Corrected bad slug wpsdsp_- to wpsdsp- (Thanks Save! I mean Dave!)
* FIX - Corrected issue preventing attached photos from being removed (Thanks Steve!)
= 1.0.2 =
* New readme file
* Added basic Car Demon converter

== Upgrade Notice ==

