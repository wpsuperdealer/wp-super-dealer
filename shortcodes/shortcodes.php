<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_payment_calculator_shortcode( $atts ) {
	$atts = shortcode_atts( array(
		__( 'title', 'wp-super-dealer' ) => __( 'Finance Calculator', 'wp-super-dealer' ),
        __( 'price', 'wp-super-dealer' ) => '25000',
        __( 'down', 'wp-super-dealer' ) => '1500',
		__( 'apr', 'wp-super-dealer' ) => '10',
		__( 'term', 'wp-super-dealer' ) => '60',
		__( 'disclaimer1', 'wp-super-dealer' ) => __( 'It is not an offer for credit nor a quote.', 'wp-super-dealer' ),
		__( 'disclaimer2', 'wp-super-dealer' ) => __( 'This calculator provides an estimated monthly payment. Your actual payment may vary based upon your specific loan and final purchase price.', 'wp-super-dealer' ),
	), $atts, 'payment_calculator' );

	$x = wpsd_calculator_form( $atts );
	return $x;
}
add_shortcode( __( 'wpsd_payment_calculator', 'wp-super-dealer' ), 'wpsd_payment_calculator_shortcode' );

function wpsdsp_search_shortcode_func( $atts ) {
	global $wpsd_options;
	$search_url = get_option( 'site_url' );
	if ( isset( $wpsd_options['inventory_page'] ) ) {
		$search_url = $wpsd_options['inventory_page'];
	}

	$fields = wpsdsp_active_fields();
	$default_atts = array(
		'title' => __( 'Search', 'wp-super-dealer' ),
		'custom_class' => '',
		'style' => '',
		'label_reset' => __( 'Reset Filters', 'wp-super-dealer' ),
		'form_action' => $search_url,
        'result_form' => $search_url,
		'submit_label' => __( 'Search', 'wp-super-dealer' ),
		'field_map' => '',
		'sliders' => '',
		'show_sort' => __( 'no', 'wp-super-dealer' ),
		'hide_search_button' => __( 'no', 'wp-super-dealer' ),
        'tags_title' => __( 'Show me these vehicles:', 'wp-super-dealer' ),
        'tags_button' => __( 'Find Now', 'wp-super-dealer' ),
        'option_is_field_name' => false,
        'simple_selects' => false,
	);

	$field_atts = array();

	foreach( $fields as $field=>$field_settings ) {

		if ( isset( $atts[ $field ] ) ) {
			$field_atts[ $field ] = array();

			if ( ! empty( $atts[ $field ] ) ) {
				if ( strpos( $atts[ $field ], '&' ) !== false ) {

					$field_options_array = explode( '&', $atts[ $field ] );
					foreach( $field_options_array as $field_options_item ) {

						if ( strpos( $field_options_item, '=' ) !== false ) {
							$field_option_parts = explode( '=', $field_options_item );
							$field_atts[ $field ][ $field_option_parts[0] ] = $field_option_parts[1];
						}						
						
					}
					
				} else {

					if ( strpos( $atts[ $field ], '=' ) !== false ) {
						$field_option_parts = explode( '=', $atts[ $field ] );
						$field_atts[ $field ][ $field_option_parts[0] ] = $field_option_parts[1];
					}
					
				}

			}
		}

	}

	$atts = shortcode_atts( $default_atts, $atts, 'wpsd_search' );
	$atts = wp_parse_args( $field_atts, $atts );
    //return '<pre>'.print_r($atts,true).'</pre>';
	$x = wpsdsp_get_search_form( $atts );
	
	return $x;
}
add_shortcode( __( 'wpsd_search', 'wp-super-dealer' ), 'wpsdsp_search_shortcode_func' );

function wpsdsp_sort_shortcode_func( $atts ) {
	$atts = shortcode_atts( array(
		__( 'title', 'wp-super-dealer' ) => __( 'Finance Calculator', 'wp-super-dealer' ),
	), $atts, 'wpsd_sort' );

	$x = wpsd_sort_form( $atts );
	return $x;
}
add_shortcode( __( 'wpsd_sort', 'wp-super-dealer' ), 'wpsdsp_sort_shortcode_func' );
?>