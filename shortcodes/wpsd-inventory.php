<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_shortcode_init() {
	global $wpsd_options;
	if ( ! isset( $wpsd_options['inventory_page'] ) ) {
		// get homepage by default
		$wpsd_options['inventory_page'] = get_bloginfo( 'wpurl' );
		// we need to know if page is https or http
		if ( is_ssl() ) {
			$protocol = 'https://';
		} else {
			$protocol = 'http://';
		}
		// get $post_id from URL
		$post_id = url_to_postid( $protocol . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'] );
		$content_post = get_post( $post_id );
		if ( $content_post ) {
			$content = $content_post->post_content;
			if ( strpos( $content, '[inventory' ) !== false) {
				$wpsd_options['inventory_page'] = get_permalink( $post_id );
			}
		}
	}
}
add_action( 'init', 'wpsd_shortcode_init' );

function wpsd_inventory_shortcode_func( $atts ) {
	global $wpsd_options;
	if ( isset( $wpsd_options['vehicles_per_page'] ) ) {
		$vehicles_per_page = $wpsd_options['vehicles_per_page'];
	} else {
		$vehicles_per_page = 9;
	}

	$default_atts = array(
		__( 'title', 'wp-super-dealer' ) => __( 'View our inventory', 'wp-super-dealer' ), /* Optional inventory title */
		__( 'vehicle_tags', 'wp-super-dealer' ) => '', /* Filter by vehicle tag */
		__( 'show_sold', 'wp-super-dealer' ) => '', /* Show sold vehicles in list */
		__( 'show_only_sold', 'wp-super-dealer' ) => '', /* Show only sold vehicles in list */
		__( 'criteria', 'wp-super-dealer' ) => '', /* Filter by this keyword */
		__( 'vehicles_per_page', 'wp-super-dealer' ) => $vehicles_per_page, /* List a maximum of this many vehicles */
	);

	if ( is_array( $atts ) && 0 < count( $atts ) ) {
		//= Add all spec fields as potential attributes
		$spec_fields = wpsd_get_all_specifications();
		if ( 0 < count( $spec_fields ) ) {
			foreach( $spec_fields as $spec_field=>$data ) {
				$slug = sanitize_title( $spec_field );
				$_slug = str_replace( '-', '_', $slug );
				if ( 'vehicle_tags' === $_slug ) {
					continue;
				}
				if ( isset( $atts[ $_slug ] ) && ! empty( $atts[ $_slug ] ) ) {
					$default_atts[ $_slug ] = $atts[ $_slug ];
				}
			}
		}
	}
	
	$default_atts = wp_parse_args( $default_atts, $atts );
	$atts = shortcode_atts( $default_atts, $atts, 'wpsd_inventory' );

	foreach( $atts as $att=>$val ) {
		if ( empty( $val ) ) {
			unset( $atts[ $att ] );
		}
	}
	
	global $wpsd_doing_shortcode;
	$wpsd_doing_shortcode = true;

	$inventory = wpsd_show_inventory( $atts );

	return $inventory;
}
add_shortcode( __( 'wpsd_inventory', 'wp-super-dealer' ), 'wpsd_inventory_shortcode_func' );

function wpsd_show_inventory( $atts ) {
	global $wpsd_options;
	$atts = wp_parse_args( $atts, $wpsd_options );
	
	$searched = '';

	$query = array();
	$query[ WPSD_POST_TYPE ] = 1;
	$query = array_merge( $atts, $query );
	$wpsd_query = wpsd_query_search( $query );

	if ( isset( $query['show_only_sold'] ) ) {
		if ( ucwords( $query['show_only_sold'] ) === 'Yes' ) {
			$wpsd_query['meta_query'] = array(
				'key' => __( 'sold', 'wp-super-dealer' ),
				'value' => __( 'No', 'wp-super-dealer' ),
				'compare' => '!='
			);
		}
	}
	unset( $wpsd_query['pagename'] );

	global $post;
	$post_id = $post->ID;
	$result_page = get_permalink( $post_id );

	$vehicle_query = new WP_Query();
	$vehicle_query->query( $wpsd_query );
	$total_results = $vehicle_query->found_posts;

	$html = '';
	
	ob_start();
	do_action( 'wpsd_before_content_action' );
	do_action( 'wpsd_before_srp_list_action', $atts );
	$html .= ob_get_contents();
	ob_end_clean();

		$html .= '<div class="srp-before-listings-wrap">';
			$html .= '<div class="srp-before-listings">';
				$html .= esc_html( $wpsd_options['before_listings'] );
			$html .= '</div>';
			$html .= '<div class="wpsd-clear"></div>';
		$html .= '</div>';

		if ( isset( $atts['show_sort'] ) 
			&& wpsd_is_true( $atts['show_sort'] ) ) 
		{
			$html .= wpsd_sort_form();
		}

		ob_start();
		do_action( 'wpsd_after_sort_srp_action', $atts );
		$html .= ob_get_contents();
		ob_end_clean();

		if ( isset( $atts['show_results_found'] ) 
			&& wpsd_is_true( $atts['show_results_found'] ) ) 
		{
			$results_found = '<div class="srp-results-found">';
			$results_found .= __( 'Results Found: ','wp-super-dealer' );
			$results_found .= esc_html( $total_results );
			$results_found .= '</div>';
			$html .= apply_filters( 'wpsd_results_found_filter', $results_found, $total_results );
		}

		if ( isset( $atts['show_searched_by'] ) 
			&& wpsd_is_true( $atts['show_searched_by'] ) ) 
		{
			$html .= wpsd_get_searched_by( $result_page );
		}

		ob_start();
		do_action( 'wpsd_after_results_found_srp_action', $atts );
		$html .= ob_get_contents();
		ob_end_clean();

		if ( isset( $atts['show_switch_style'] )
			&& wpsd_is_true( $atts['show_switch_style'] ) ) 
		{
			if ( isset( $atts['default_style'] ) && !empty( $atts['default_style'] ) ) {
				$style = $atts['default_style'];
			} else {
				$style = __( 'full', 'wp-super-dealer' );
			}
			if ( isset( $_COOKIE['srp_style'] ) ) {
				$style = sanitize_text_field( $_COOKIE['srp_style'] );
			}
			$html .= wpsd_switch_srp_style( $style );
		}

		if ( isset( $atts['show_navigation'] )
			&& wpsd_is_true( $atts['show_navigation'] ) ) 
		{
			$html .= '<div class="wpsd-clear"></div>';
			$html .= wpsd_nav( 'top', $vehicle_query );
			$html .= '<div class="wpsd-clear"></div>';
		}

		$html .= '<div class="wpsd-clear"></div>';
		ob_start();
		do_action( 'wpsd_after_nav_srp_action', $atts );
		$html .= ob_get_contents();
		ob_end_clean();

		$html .= '<div class="wpsd-srp-wrap">';
			if ( $vehicle_query->have_posts() ) : while ( $vehicle_query->have_posts() ) : $vehicle_query->the_post();
				$post_id = $vehicle_query->post->ID;
				$srp_item = wpsd_srp_item( $post_id, $atts );
				$html .= $srp_item;

				endwhile;

			else:
                $html .= wpsd_no_search_results();

			endif;
		$html .= '</div>';

	if ( isset( $atts['show_navigation'] )
		&& wpsd_is_true( $atts['show_navigation'] ) ) 
	{
		$nav_bottom = wpsd_nav( 'bottom', $vehicle_query );
		$nav_bottom = str_replace( 'wp-pagenavi', 'wp-pagenavi inventory_nav_bottom', $nav_bottom );
		$nav_bottom = str_replace( 'wpsd-navigation', 'wpsd-navigation inventory_nav_bottom', $nav_bottom );
		$html .= '<div class="wpsd-clear"></div>';
		$html .= $nav_bottom;
		$html .= '<div class="wpsd-clear"></div>';
	}

	ob_start();
	do_action( 'wpsd_after_srp_list_action', $atts );
	do_action( 'wpsd_after_content_action' );
	$html .= ob_get_contents();
	$html .= '<div class="wpsd-clear"></div>';
	ob_end_clean();
	wp_reset_query();
    
    $html = wpsd_kses_srp( $html );
    
	return $html;
}

function wpsd_switch_srp_style( $active = 'full' ) {
	if ( wp_is_mobile() ) {
		return '';
	}
	$x = '';
	$x .= '<div class="wpsd-switch-srp-style-wrap">';
		$x .= '<span class="wpsd-switch-srp-style-item dashicons dashicons-excerpt-view' . ( 'full' === $active ? ' active' : '' ) . '" data-style="full"></span>';
		$x .= '<span class="wpsd-switch-srp-style-item dashicons dashicons-list-view' . ( 'list' === $active ? ' active' : '' ) . '" data-style="list"></span>';
		$x .= '<span class="wpsd-switch-srp-style-item dashicons dashicons-grid-view' . ( 'grid' === $active ? ' active' : '' ) . '" data-style="grid"></span>';
	$x .= '</div>';
    
    $elements = wpsd_switch_style_elements();    
    $x = wp_kses( $x, $elements );

	$x = apply_filters( 'wpsd_switch_styles_filter', $x, $active );
	return $x;
}

function wpsd_switch_style_elements() {
    $elements = array(
        'div' => array(
            'class' => array(),
        ),
        'span' => array(
            'class' => array(),
            'data-style' => array(),
        ),
    );

    $elements = apply_filters( 'wpsd_switch_srp_style_kses_element_filter', $elements );
    return $elements;
}

function wpsd_kses_srp( $content ) {
    $wpsd_searched_by_elements = wpsd_searched_by_elements();
    $elements = $wpsd_searched_by_elements;
    
    $wpsd_sort_form_elements = wpsd_sort_form_elements();
    $elements = array_merge_recursive( $elements, $wpsd_sort_form_elements );

    $wpsd_srp_item_elements = wpsd_srp_item_elements();
    $elements = array_merge_recursive( $elements, $wpsd_srp_item_elements );
    
    $wpsd_page_navi_elements = wpsd_page_navi_elements();
    $elements = array_merge_recursive( $elements, $wpsd_page_navi_elements );    

    $wpsd_switch_style_elements = wpsd_switch_style_elements();
    $elements = array_merge_recursive( $elements, $wpsd_switch_style_elements );

    $elements = apply_filters( 'wpsd_kses_srp_filter', $elements, $content );

    return wp_kses( $content, $elements );
}
?>