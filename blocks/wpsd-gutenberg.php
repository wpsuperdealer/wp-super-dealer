<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Turn off Gutenberg support for the vehicle post type
 */
add_filter( 'use_block_editor_for_post_type', 'wpsd_disable_gutenberg_for_vehicle', 10, 2 );
function wpsd_disable_gutenberg_for_vehicle( $is_enabled, $post_type ) {
	if ( in_array( $post_type, array( WPSD_POST_TYPE ) ) ) {
		return false;
    }

    return $is_enabled;
}
?>