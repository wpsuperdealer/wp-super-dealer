jQuery(document).ready(function($) {
	"use strict";

// JavaScript Document
    var el = wp.element.createElement,
        registerBlockType = wp.blocks.registerBlockType,
        ServerSideRender = wp.components.ServerSideRender,
        PanelRow = wp.components.PanelRow,
        PanelBody = wp.components.PanelBody,
        Button  = wp.components.Button,
        TextControl = wp.components.TextControl,
        TextareaControl = wp.components.TextareaControl,
        SelectControl = wp.components.SelectControl,
        CheckboxControl = wp.components.CheckboxControl,
        ToggleControl = wp.components.ToggleControl,
        InspectorControls = wp.editor.InspectorControls;

    /* Icon for Inventory Block */
    var wpsdInventoryIcon = el ("img", {
        src: wpsdBlocksParams.icon_inventory,
        width: "24px",
        height: "24px"
    });

    console.log( 'wpsdBlocksParams ===>' );
    console.log( wpsdBlocksParams );

    /* Block for Vehicle Inventory */
    registerBlockType( 'wp-super-dealer/wpsd-inventory', {
        title: wpsdBlocksParams.strings.block_title_inventory,
        category: 'embed',
        icon: {
            src: wpsdInventoryIcon
        },

        edit: function( props ) {
            var atts = [
                el( ServerSideRender, {
                    block: 'wp-super-dealer/wpsd-inventory',
                    attributes: props.attributes,
                } ),
                el( InspectorControls, {},
                    el( PanelBody, { title: wpsdBlocksParams.strings.filter_inventory, initialOpen: false, className: 'wpsd-block-filter-inventory', },
                        el( PanelRow, {},
                            el( TextControl, {
                                label: wpsdBlocksParams.strings.stock,
                                value: props.attributes.stock,
                                onChange: ( value ) => { props.setAttributes( { stock: value } ); },
                            } ),
                        ),
                        el( PanelRow, {},
                            el( TextControl, {
                                label: wpsdBlocksParams.strings.year,
                                value: props.attributes.year,
                                onChange: ( value ) => { props.setAttributes( { year: value } ); },
                            } ),
                        ),
                        el( PanelRow, {},
                            el( TextControl, {
                                label: wpsdBlocksParams.strings.make,
                                value: props.attributes.make,
                                onChange: ( value ) => { props.setAttributes( { make: value } ); },
                            } ),
                        ),
                        el( PanelRow, {},
                            el( TextControl, {
                                label: wpsdBlocksParams.strings.model,
                                value: props.attributes.model,
                                onChange: ( value ) => { props.setAttributes( { model: value } ); },
                            } ),
                        ),
                        el( PanelRow, {},
                            el( TextControl, {
                                label: wpsdBlocksParams.strings.location,
                                value: props.attributes.location,
                                onChange: ( value ) => { props.setAttributes( { location: value } ); },
                            } ),
                        ),
                        el( PanelRow, {},
                            el( TextControl, {
                                label: wpsdBlocksParams.strings.body_style,
                                value: props.attributes.body_style,
                                onChange: ( value ) => { props.setAttributes( { body_style: value } ); },
                            } ),
                        ),
                        el( PanelRow, {},
                            el( TextControl, {
                                label: wpsdBlocksParams.strings.mileage,
                                value: props.attributes.mileage,
                                onChange: ( value ) => { props.setAttributes( { mileage: value } ); },
                            } ),
                        ),
                        el( PanelRow, {},
                            el( TextControl, {
                                label: wpsdBlocksParams.strings.min_price,
                                value: props.attributes.min_price,
                                onChange: ( value ) => { props.setAttributes( { min_price: value } ); },
                            } ),
                        ),
                        el( PanelRow, {},
                            el( TextControl, {
                                label: wpsdBlocksParams.strings.max_price,
                                value: props.attributes.max_price,
                                onChange: ( value ) => { props.setAttributes( { max_price: value } ); },
                            } ),
                        ),
                        el( PanelRow, {},
                            el( TextControl, {
                                label: wpsdBlocksParams.strings.transmission,
                                value: props.attributes.transmission,
                                onChange: ( value ) => { props.setAttributes( { transmission: value } ); },
                            } ),
                        ),
                        el( PanelRow, {},
                            el( TextControl, {
                                label: wpsdBlocksParams.strings.vehicle_tag,
                                value: props.attributes.vehicle_tag,
                                onChange: ( value ) => { props.setAttributes( { vehicle_tag: value } ); },
                            } ),
                        ),
                    ),
                   el( PanelBody, { title: wpsdBlocksParams.strings.list_settings, initialOpen: true, className: 'wpsd-block-list-settings', },
                        el( PanelRow, {},
                            el( SelectControl, {
                                label: wpsdBlocksParams.strings.default_style,
                                value: props.attributes.default_style,
                                options: [
                                    { label: wpsdBlocksParams.strings.full, value: wpsdBlocksParams.strings._full },
                                    { label: wpsdBlocksParams.strings.list, value: wpsdBlocksParams.strings._list },
                                    { label: wpsdBlocksParams.strings.grid, value: wpsdBlocksParams.strings._grid },
                                ],
                                onChange: ( value ) => { props.setAttributes( { default_style: value } ); },
                            } ),
                        ),
                        el( PanelRow, {},
                            el( SelectControl, {
                                label: wpsdBlocksParams.strings.show_sort,
                                value: props.attributes.show_sort,
                                options: [
                                    { label: wpsdBlocksParams.strings.no, value: wpsdBlocksParams.strings._no },
                                    { label: wpsdBlocksParams.strings.yes, value: wpsdBlocksParams.strings._yes },
                                ],
                                onChange: ( value ) => { props.setAttributes( { show_sort: value } ); },
                            } ),
                        ),
                        el( PanelRow, {},
                            el( SelectControl, {
                                label: wpsdBlocksParams.strings.show_navigation,
                                value: props.attributes.show_navigation,
                                options: [
                                    { label: wpsdBlocksParams.strings.no, value: wpsdBlocksParams.strings._no },
                                    { label: wpsdBlocksParams.strings.yes, value: wpsdBlocksParams.strings._yes },
                                ],
                                onChange: ( value ) => { props.setAttributes( { show_navigation: value } ); },
                            } ),
                        ),
                        el( PanelRow, {},
                            el( SelectControl, {
                                label: wpsdBlocksParams.strings.show_switch_style,
                                value: props.attributes.show_switch_style,
                                options: [
                                    { label: wpsdBlocksParams.strings.no, value: wpsdBlocksParams.strings._no },
                                    { label: wpsdBlocksParams.strings.yes, value: wpsdBlocksParams.strings._yes },
                                ],
                                onChange: ( value ) => { props.setAttributes( { show_switch_style: value } ); },
                            } ),
                        ),
                        el( PanelRow, {},
                            el( SelectControl, {
                                label: wpsdBlocksParams.strings.show_results_found,
                                value: props.attributes.show_results_found,
                                options: [
                                    { label: wpsdBlocksParams.strings.no, value: wpsdBlocksParams.strings._no },
                                    { label: wpsdBlocksParams.strings.yes, value: wpsdBlocksParams.strings._yes },
                                ],
                                onChange: ( value ) => { props.setAttributes( { show_results_found: value } ); },
                            } ),
                        ),
                        el( PanelRow, {},
                            el( TextControl, {
                                label: wpsdBlocksParams.strings.vehicles_per_page,
                                value: props.attributes.vehicles_per_page,
                                onChange: ( value ) => { props.setAttributes( { vehicles_per_page: value } ); },
                            } ),
                        ),
                        el( PanelRow, {},
                            el( SelectControl, {
                                label: wpsdBlocksParams.strings.show_sold,
                                value: props.attributes.show_sold,
                                options: [
                                    { label: wpsdBlocksParams.strings.no, value: wpsdBlocksParams.strings._no },
                                    { label: wpsdBlocksParams.strings.yes, value: wpsdBlocksParams.strings._yes },
                                ],
                                onChange: ( value ) => { props.setAttributes( { show_sold: value } ); },
                            } ),
                        ),
                        el( PanelRow, {},
                            el( SelectControl, {
                                label: wpsdBlocksParams.strings.show_only_sold,
                                value: props.attributes.show_only_sold,
                                options: [
                                    { label: wpsdBlocksParams.strings.no, value: wpsdBlocksParams.strings._no },
                                    { label: wpsdBlocksParams.strings.yes, value: wpsdBlocksParams.strings._yes },
                                ],
                                onChange: ( value ) => { props.setAttributes( { show_only_sold: value } ); },
                            } ),
                        ),
                    ),
                )
            ];

            /* If Template Builder is installed then add the options for it */
            if ( wpsdBlocksParams.template_builder_installed ) {
                var selectTemplateID = el( TextControl, {
                            label: wpsdBlocksParams.strings.template_builder_label,
                            value: props.attributes.template_id,
                            onChange: ( value ) => { props.setAttributes( { template_id: value } ); },
                        } );
                atts[1].props.children.push( selectTemplateID );
                var selectProSort = el( SelectControl, {
                            label: wpsdBlocksParams.strings.pro_sort_label,
                            value: props.attributes.pro_sort,
                            options: [
                                { label: wpsdBlocksParams.strings.no, value: '' },
                                { label: wpsdBlocksParams.strings.yes, value: wpsdBlocksParams.strings.yes },
                            ],
                            onChange: ( value ) => { props.setAttributes( { pro_sort: value } ); },
                        } );
                atts[1].props.children.push( selectProSort );
                var selectSwitchStyle = el( SelectControl, {
                            label: wpsdBlocksParams.strings.switch_styles_label,
                            value: props.attributes.switch_style,
                            options: [
                                { label: wpsdBlocksParams.strings.no, value: '' },
                                { label: wpsdBlocksParams.strings.yes, value: wpsdBlocksParams.strings.yes },
                            ],
                            onChange: ( value ) => { props.setAttributes( { switch_style: value } ); },
                        } );
                atts[1].props.children.push( selectSwitchStyle );
            }
            return atts;
        },

        save: function(props) {
            return null;
        }
    });

    /* Icon for Search Block */
    var wpsdSearchIcon = el ("img", {
        src: wpsdBlocksParams.icon_search,
        width: "24px",
        height: "24px"
    });

    /* Block for Vehicle Search Form */
    registerBlockType( 'wp-super-dealer/wpsd-search', {
        title: wpsdBlocksParams.strings.search_title,
        category: 'embed',
        icon: {
            src: wpsdSearchIcon
        },

        edit: function( props ) {
            
            var wpsd_build_field_selection = function( args ) {
                var templates = [];

                var active_fields = wpsdBlocksParams.search_fields;
                
                var checkboxes = Object.keys(active_fields).map(function(active_fieldsId) {
                    var field_label = active_fieldsId;
                    var re = new RegExp( '_', 'g' );
                    field_label = field_label.replace( re, ' ' );

                    return el(CheckboxControl, {
                        label: field_label,
                        checked: props.attributes[active_fieldsId],
                        value: active_fieldsId,
                        class: 'components-checkbox-control__input wpsd-block-field',
                        onChange: ( value ) => {
                            //= if it's false then make it empty so it plays nice
                            if ( false === value ) {
                                value = '';
                            }
                            //= make sure everything is reordered properly now that we've added or removed a field
                            wpsd_reorder_search_fields( props );
                            $( '.wpsd-reorder-fields-panel' ).addClass( 'is-opened' );
                            $( '.wpsd-reorder-fields-panel .commonents-button' ).attr( 'aria-expanded', 'true' );
                            
                            //= reset the fieldmap
                            var field_map = wpsd_remove_unchecked_fields( '' );
                            //= update our block
                            props.setAttributes({[active_fieldsId]: value, field_map: field_map});
                        },
                      });
                });
                
                return checkboxes;
            }

            var active_search_field_settings = function() {
                var settings = [];
                //settings.active = wpsdBlocksParams.strings.yes;
                settings.label = wpsdBlocksParams.strings.yes;
                settings.type = wpsdBlocksParams.strings.yes;
                settings.dynamic = wpsdBlocksParams.strings.yes;
                settings.filter_by = wpsdBlocksParams.strings.yes;
                settings.closed = wpsdBlocksParams.strings.yes;
                return settings;
            };

            var wpsd_build_field_settings = function( args ) {
                var field_label = args.setting;
                var re = new RegExp( '_', 'g' );
                field_label = field_label.replace( re, ' ' );

                var css_class = '';

                var active_fields = active_search_field_settings();
                if ( 'undefined' === typeof( active_fields[ args.setting ] ) ) {
                    return;
                }

                if ( 'dynamic' === args.setting 
                   || 'filter_by' === args.setting ) {
                    //= we're not allowing these to be changed on a per form basis at this time
                    return;
                }
                if ( 'active' === args.setting 
                    || 'filter_by' === args.setting 
                    || 'dynamic' === args.setting 
                    || 'closed' === args.setting ) 
                {
                    var template = el( PanelRow, { className: 'wpsd-block-field' },
                        el( SelectControl, {
                            id: args.field + '-' + args.setting,
                            name: 'vehicle_field[' + args.field + '][' + args.setting + ']',
                            field: field_label,
                            setting: args.setting,
                            class: 'vehicle-search-field vehicle-search-field-' + args.field + ' vehicle-search-field-' + args.field + '-' + args.setting,
                            label: args.setting,
                            value: props.attributes[ args.field + '_' + args.setting ],
                            options: [
                                { label: wpsdBlocksParams.strings.no, value: '' },
                                { label: wpsdBlocksParams.strings.yes, value: wpsdBlocksParams.strings.yes },
                            ],
                            onChange: ( value ) => { props.setAttributes({[ args.field + '_' + args.setting ]: value}); },
                        } ),
                    );
                } else if ( 'search_field' === args.setting ) {
                    var template = el( PanelRow, { className: 'wpsd-block-field' },
                        el( TextControl, {
                            id: args.field + '-' + args.setting,
                            name: 'vehicle_field[' + args.field + '][' + args.setting + ']',
                            field: args.field,
                            setting: args.setting,
                            class: 'vehicle-search-field vehicle-search-field-' + args.field + ' vehicle-search-field-' + args.field + '-' + args.setting,
                            label: field_label,
                            value: props.attributes[ args.field + '_' + args.setting ],
                            onChange: ( value ) => { props.setAttributes({[ args.field + '_' + args.setting ]: value}); },
                            help: ''
                        } ),
                    );
                } else if ( 'type' === args.setting ) {
                    var template = el( PanelRow, { className: 'wpsd-block-field' },
                        el( SelectControl, {
                            id: args.field + '-' + args.setting,
                            name: 'vehicle_field[' + args.field + '][' + args.setting + ']',
                            field: args.field,
                            setting: args.setting,
                            class: 'vehicle-search-field vehicle-search-field-' + args.field + ' vehicle-search-field-' + args.field + '-' + args.setting,
                            label: field_label,
                            value: props.attributes[ args.field + '_' + args.setting ],
                            options: [
                                { label: wpsdBlocksParams.strings.select_box, value: wpsdBlocksParams.strings.select_box_value },
                                { label: wpsdBlocksParams.strings.checkboxes, value: wpsdBlocksParams.strings.checkboxes_value },
                                { label: wpsdBlocksParams.strings.range_selector, value: wpsdBlocksParams.strings.range_selector_value },
                            ],
                            onChange: ( value ) => { props.setAttributes({[ args.field + '_' + args.setting ]: value}); },
                            help: ''
                        } ),
                    );
                } else {
                    var template = el( PanelRow, { className: 'wpsd-block-field' },
                        el( TextControl, {
                            id: args.field + '-' + args.setting,
                            name: 'vehicle_field[' + args.field + '][' + args.setting + ']',
                            field: args.field,
                            setting: args.setting,
                            class: 'vehicle-search-field vehicle-search-field-' + args.field + ' vehicle-search-field-' + args.field + '-' + args.setting,
                            label: field_label,
                            value: props.attributes[ args.field + '_' + args.setting ],
                            onChange: ( value ) => { props.setAttributes({[ args.field + '_' + args.setting ]: value}); },
                            help: ''
                        } ),
                    );
                }
                return template;
            };
            
            var atts = [
                el( ServerSideRender, {
                    block: 'wp-super-dealer/wpsd-search',
                    attributes: props.attributes,
                } ),
                el( InspectorControls, {},
                    // 1st Panel – Form Settings
                    el( PanelBody, { title: wpsdBlocksParams.strings.panel_title, initialOpen: true, className: 'wpsd-block-settings', },
                        // Text field
                        el( PanelRow, { className: 'wpsd-block-style' },

                            el( SelectControl, {
                                label: wpsdBlocksParams.strings.style,
                                className: 'wpsdsp-block-select-style',
                                value: props.attributes.style,
                                options: [
                                    { label: wpsdBlocksParams.strings.form_style_one, value: wpsdBlocksParams.strings._form_style_one },
                                    { label: wpsdBlocksParams.strings.form_style_two, value: wpsdBlocksParams.strings._form_style_two },
                                    { label: wpsdBlocksParams.strings.form_style_three, value: wpsdBlocksParams.strings._form_style_three },
                                    { label: wpsdBlocksParams.strings.form_style_four, value: wpsdBlocksParams.strings._form_style_four },
                                ],
                                onChange: ( value ) => { props.setAttributes( { style: value } ); },
                            } ),
                         ),
                        el( PanelRow, { className: 'wpsd-block-custom-class' },
                            el( TextControl, {
                                label: wpsdBlocksParams.strings.custom_class,
                                value: props.attributes.custom_class,
                                className: 'wpsdsp-custom-class',
                                onChange: ( value ) => { props.setAttributes( { custom_class: value } ); },
                            } ),
                        ),
                       el( PanelRow, { className: 'wpsd-block-search-result-page' },
                            el( TextControl, {
                                label: wpsdBlocksParams.strings.search_result_page,
                                value: props.attributes.result_page,
                                onChange: ( value ) => { props.setAttributes( { result_page: value } ); },
                            } ),
                        ),
                        el( PanelRow, { className: 'wpsd-block-show-sort' },
                            el( SelectControl, {
                                label: wpsdBlocksParams.strings.show_sort,
                                value: props.attributes.show_sort,
                                options: [
                                    { label: wpsdBlocksParams.strings.no, value: wpsdBlocksParams.strings.no },
                                    { label: wpsdBlocksParams.strings.yes, value: wpsdBlocksParams.strings.yes },
                                ],
                                onChange: ( value ) => { props.setAttributes( { show_sort: value } ); },
                            } ),
                        ),
                       
                       
                        el( PanelRow, { className: 'wpsd-block-option-is-field-name' },
                            el( SelectControl, {
                                label: wpsdBlocksParams.strings.option_is_field_name,
                                value: props.attributes.option_is_field_name,
                                options: [
                                    { label: wpsdBlocksParams.strings.no, value: wpsdBlocksParams.strings.no },
                                    { label: wpsdBlocksParams.strings.yes, value: wpsdBlocksParams.strings.yes },
                                ],
                                onChange: ( value ) => { props.setAttributes( { option_is_field_name: value } ); },
                            } ),
                        ),
                       
                        el( PanelRow, { className: 'wpsd-block-tags-title' },
                                el( TextControl, {
                                label: wpsdBlocksParams.strings.tags_title,
                                value: props.attributes.tags_title,
                                onChange: ( value ) => { props.setAttributes( { tags_title: value } ); },
                            } ),
                        ),
                        el( PanelRow, { className: 'wpsd-block-tags-button' },
                                el( TextControl, {
                                label: wpsdBlocksParams.strings.tags_button,
                                value: props.attributes.tags_button,
                                onChange: ( value ) => { props.setAttributes( { tags_button: value } ); },
                            } ),
                        ),
                        el( PanelRow, { className: 'wpsd-block-simple-selects' },
                            el( SelectControl, {
                                label: wpsdBlocksParams.strings.simple_selects,
                                value: props.attributes.simple_selects,
                                options: [
                                    { label: wpsdBlocksParams.strings.no, value: wpsdBlocksParams.strings.no },
                                    { label: wpsdBlocksParams.strings.yes, value: wpsdBlocksParams.strings.yes },
                                ],
                                onChange: ( value ) => { props.setAttributes( { simple_selects: value } ); },
                            } ),
                        ),
                        el( PanelRow, { className: 'wpsd-block-hide-search-button' },
                            el( SelectControl, {
                                label: wpsdBlocksParams.strings.hide_search_button,
                                value: props.attributes.hide_search_button,
                                options: [
                                    { label: wpsdBlocksParams.strings.no, value: wpsdBlocksParams.strings.no },
                                    { label: wpsdBlocksParams.strings.yes, value: wpsdBlocksParams.strings.yes },
                                ],
                                onChange: ( value ) => { props.setAttributes( { hide_search_button: value } ); },
                            } ),
                        ),
                        el( PanelRow, { className: 'wpsd-block-submit-label' },
                                el( TextControl, {
                                label: wpsdBlocksParams.strings.submit_label,
                                value: props.attributes.submit_label,
                                onChange: ( value ) => { props.setAttributes( { submit_label: value } ); },
                            } ),
                        ),

                        //= this field is actually hidden by CSS and used for ordering fields
                        el( PanelRow, { className: 'wpsd-block-field-map' },
                                el( TextControl, {
                                class: 'wpsd-block-field-map-input',
                                label: wpsdBlocksParams.strings.field_map,
                                value: props.attributes.field_map,
                                onChange: ( value ) => { /*props.setAttributes( { field_map: value } );*/ },
                            } ),
                        ),
                       
                    ), //= end PanelBody

                ),
            ];

            var args = {};
            args.fields = wpsdBlocksParams.search_fields;
            args.props = props;
            args.value = '';
            var field_checkboxes = wpsd_build_field_selection( args );
            var select_fields = el( PanelBody, { title: wpsdBlocksParams.strings.active_fields, initialOpen: true, className: 'wpsd-block-wrap', }, field_checkboxes );

            atts[1].props.children.props.children.push( select_fields );

            var reorder_form =  
                el( PanelBody, { title: wpsdBlocksParams.strings.reorder_fields, 
                    initialOpen: false, 
                    onToggle: ( value ) => { 
                        if ( true === value ) {
                            wpsd_reorder_search_fields( props );
                        } else {
                            $( '.wpsd-reorder-fields-wrap' ).remove();
                            $( '.wpsd-block-field-order' ).remove();
                        }
                    },
                    className: 'wpsd-block-settings wpsd-reorder-fields-panel', },
            );
            atts[1].props.children.props.children.push( reorder_form );

            //= Build field options
            if ( wpsdBlocksParams.search_fields ) {
                var fields = wpsdBlocksParams.search_fields;
                var active_search_fields = {};
                var field_options = [];
                field_options.push( 'fieldname' );

                for ( var field in fields ) {
                    if ( fields.hasOwnProperty( field ) ) {
                        var field_settings = fields[field];
                        var active_fields = [];
                        
                        var cnt = 0;
                        for ( var setting in field_settings ) {
                            ++cnt;

                            if ( field_settings.hasOwnProperty( setting ) ) {
                                if ( 'undefined' === typeof( active_search_fields[ field ] ) ) {
                                    active_search_fields[ field ] = {}
                                }
                                active_search_fields[ field ][ setting ] = field_settings[ setting ];

                                var args = {};
                                args.field = field;
                                args.settings = field_settings[ setting ];
                                args.setting = setting;
                                args.props = props;
                                args.value = props.attributes.field_map;

                                var template = wpsd_build_field_settings( args );
                                active_fields.push( template );
                            }
                        } /* for ( var setting in field_settings ) { */
                    } /* end if ( fields.hasOwnProperty( field ) ) { */

                    var searchField = el( PanelBody, { title: fields[ field ].label, initialOpen: false, className: 'wpsd-search-field-settings wpsd-search-field-settings-' + field, 'field': field }, active_fields );
                    atts[1].props.children.props.children.push( searchField );

                } /* end for ( var field in fields ) { */

            } /* end if ( wpsdBlocksParams.search_fields ) { */

            return atts;
        },

        save: function(props) {
            return null;
        }
    });

    /* Icon for Calculator Block */
    var wpsdCalculatorIcon = el ("img", {
        src: wpsdBlocksParams.icon_calendar,
        width: "24px",
        height: "24px"
    });

    /* Block for Vehicle Finance Calculator Form */
    registerBlockType( 'wp-super-dealer/wpsd-calculator', {
        title: wpsdBlocksParams.strings.block_title_finance_calculator,
        category: 'embed',
        icon: {
            src: wpsdCalculatorIcon
        },

        edit: function( props ) {
            return [
                el( ServerSideRender, {
                    block: 'wp-super-dealer/wpsd-calculator',
                    attributes: props.attributes,
                } ),
                el( InspectorControls, {},
                    el( TextControl, {
                        label: wpsdBlocksParams.strings.finance_title,
                        value: props.attributes.title,
                        class: 'components-text-control__input wpsd-calc-block-field',
                        onChange: ( value ) => { props.setAttributes( { title: value } ); },
                    } ),
                    el( TextControl, {
                        label: wpsdBlocksParams.strings.finance_down_payment,
                        value: props.attributes.down,
                        class: 'components-text-control__input wpsd-calc-block-field',
                        onChange: ( value ) => { props.setAttributes( { down: value } ); },
                    } ),
                   
                    el( TextControl, {
                        label: wpsdBlocksParams.strings.finance_price,
                        value: props.attributes.price,
                        class: 'components-text-control__input wpsd-calc-block-field',
                        onChange: ( value ) => { props.setAttributes( { price: value } ); },
                    } ),
                   
                    el( TextControl, {
                        label: wpsdBlocksParams.strings.finance_apr,
                        value: props.attributes.apr,
                        class: 'components-text-control__input wpsd-calc-block-field',
                        onChange: ( value ) => { props.setAttributes( { apr: value } ); },
                    } ),
                    el( TextControl, {
                        label: wpsdBlocksParams.strings.finance_term,
                        value: props.attributes.term,
                        class: 'components-text-control__input wpsd-calc-block-field',
                        onChange: ( value ) => { props.setAttributes( { term: value } ); },
                    } ),
                    el( TextareaControl, {
                        label: wpsdBlocksParams.strings.finance_disclaimer_1,
                        value: props.attributes.disclaimer1,
                        class: 'components-text-control__input wpsd-calc-block-field',
                        onChange: ( value ) => { props.setAttributes( { disclaimer1: value } ); },
                    } ),
                    el( TextareaControl, {
                        label: wpsdBlocksParams.strings.finance_disclaimer_2,
                        value: props.attributes.disclaimer2,
                        class: 'components-text-control__input wpsd-calc-block-field',
                        onChange: ( value ) => { props.setAttributes( { disclaimer2: value } ); },
                    } ),
                ),
            ];
        },

        save: function(props) {
            return null;
        }
    });

    var wpsd_reorder_search_fields = function( props ) {
        //= get the active fields checkboxes
        var available_field_elements = $( '.wpsd-block-field' );
        //= create a variable to store all the available fields
        var available_fields = [];
        //= create a variable to store the active fields in
        var active_fields = [];
        
        $.each( available_field_elements, function( i, e ) {
            var value = $( e ).val();
            available_fields.push( value );
            if ( true === $( e ).prop( 'checked' ) ) {
                active_fields.push( value );
            }
        });

        //= get the current field map order
        var field_map = $( '.wpsd-block-field-map-input' ).val();
        field_map = wpsd_remove_unchecked_fields( field_map );
        if ( '' === field_map ) {
            //= fields have not be ordered, so let's create a default order
            field_map = wpsd_default_search_field_order( active_fields );
        }
        $( '.wpsd-block-field-map-input' ).val( field_map );
        
        //= create an array to store our field map items in
        var field_map_array = [];
        
        //= if we have a pipe then there should be more than one value
        if ( 0 < field_map.indexOf( '|' ) ) {
            field_map_array = field_map.split( '|' );
        } else {
            //= no pipe should mean it's a single value, so let's put that value in the array
            field_map_array = [ field_map ];
        }
        
        var html = '';
        html += '<div class="wpsd-reorder-fields-wrap">';
            html += '<div class="wpsd-reorder-fields-tip">';
                html += wpsdBlocksParams.strings.reorder_fields_tip;
            html += '</div>';
            html += '<ol class="wpsd-block-sort-fields-ol">';
            $.each( field_map_array, function( i, e ) {
                if ( '' !== e ) {
                    html += '<li>' + e + '</li>';
                }
            });
            html += '</ol>';
        html += '</div>';
        
        var field_order_exists = $( '.wpsd-block-field-order' ).length;
        if ( 1 > field_order_exists ) {
            var wrap = '<div class="components-panel__row wpsd-block-field-order"></div>';
            $( '.wpsd-reorder-fields-panel' ).append( wrap );
        }
        $( '.wpsd-block-field-order' ).html( html );
        
        $( '.wpsd-block-sort-fields-ol' ).sortable({
            update: function( event, ui ) {
                var order_elements = $( '.wpsd-block-sort-fields-ol li' );
                var new_order = '';
                $.each( order_elements, function( i, e ) {
                    new_order += $( e ).html() + '|';
                });
                new_order += '##';
                new_order = new_order.replace( '|##', '' );
                new_order = new_order.replace( '##', '' );
                $( '.wpsd-block-field-map-input' ).val( new_order );
                wpsd_reorder_search_fields( props );
                props.setAttributes( { field_map: new_order } )
            }
        });
        
        return;
    }; //= end wpsd_reorder_search_fields()

    var wpsd_default_search_field_order = function( active_fields ) {
        var new_field_map = '';
        $.each( active_fields, function( i, e ) {
            new_field_map += e + '|';
        });
        new_field_map += '##';
        new_field_map = new_field_map.replace( '|##', '' );
        new_field_map = new_field_map.replace( '##', '' );
        $( '.wpsd-block-field-map-input' ).val( new_field_map );
        return new_field_map;
    };

    var wpsd_remove_unchecked_fields = function( field_map ) {
        //= get the active fields checkboxes
        var available_field_elements = $( '.wpsd-block-field' );
        //= Loop field checkboxes and make sure the unchecked ones are not in the map
        $.each( available_field_elements, function( i, e ) {
            var value = $( e ).val();
            if ( true !== $( e ).prop( 'checked' ) ) {
                var field = $( e ).val();
                var re = new RegExp( field + '|', 'g' );
                field_map = field_map.replace( re, '' );
                re = new RegExp( '|' + field, 'g' );
                field_map = field_map.replace( re, '' );
                re = new RegExp( field, 'g' );
                field_map = field_map.replace( re, '' );
            }
        });
  
        return field_map;
    };
    
});
