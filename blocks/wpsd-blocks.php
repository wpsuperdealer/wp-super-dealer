<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function wpsd_block_init() {
	if ( ! function_exists( 'register_block_type' ) ) {
		return;
	}
	
	//= Load our front end styles into the admin area so Gutenberg can load them
	global $wpsd_options;
	wp_enqueue_style( 'wpsd-jquery-ui-css', WPSD_PATH . 'css/jquery-ui.css',false,'1.9.0',false);
	wp_enqueue_style( 'wpsd-css', WPSD_PATH . 'css/wpsd.css', array( 'wpsd-jquery-ui-css' ), WPSD_VER );

	//= Load Front End Scripts into the admin area so Gutenberg can load them
	wpsd_enqueue_scripts();
	wpsdsp_enqueue_style();

	$dir = dirname( __FILE__ );
	$js = '/js/wpsd-blocks.js';
	wp_register_script(
		'wpsd-blocks',
		plugins_url( $js, __FILE__ ),
		array( 'wp-blocks', 'wp-element', 'wp-components', 'wp-editor' ),
		WPSD_VER
	);

	$template_builder_installed = false;
	if ( defined( 'CDTB_VERSION' ) ) {
		$template_builder_installed = true;
	}

	$js_strings = array();
	
	$js_strings_inventory = array(
		'block_title_inventory' => __( 'Vehicle Inventory', 'wp-super-dealer' ),
		'title' => __( 'Title', 'wp-super-dealer' ),
		'filter_inventory' => __( 'Filter Inventory', 'wp-super-dealer' ),
		'list_settings' => __( 'List Settings', 'wp-super-dealer' ),
		'query' => __( 'Query', 'wp-super-dealer' ),
		'stock' => __( 'Stock Number', 'wp-super-dealer' ),
		'condition' => __( 'Condition', 'wp-super-dealer' ),
		'year' => __( 'Year', 'wp-super-dealer' ),
		'make' =>__( 'Make', 'wp-super-dealer' ),
		'model' => __( 'Model', 'wp-super-dealer' ),
		'location' => __( 'Location', 'wp-super-dealer' ),
		'body_style' => __( 'Body Style', 'wp-super-dealer' ),
		'mileage' => __( 'Mileage', 'wp-super-dealer' ),
		'min_price' => __( 'Min Price', 'wp-super-dealer' ),
		'max_price' => __( 'Max Price', 'wp-super-dealer' ),
		'transmission' => __( 'Transmission', 'wp-super-dealer' ),
		'vehicle_tag' => __( 'Vehicle Tag', 'wp-super-dealer' ),
		'show_sold' => __( 'Show sold', 'wp-super-dealer' ),
		'show_only_sold' => __( 'Show only sold', 'wp-super-dealer' ),
		'criteria' => __( 'Criteria', 'wp-super-dealer' ),
		'default_style' => __( 'Default Style', 'wp-super-dealer' ),
		'show_sort' => __( 'Show Sort', 'wp-super-dealer' ),
		'show_switch_style' => __( 'Show Switch Style', 'wp-super-dealer' ),
		'show_navigation' => __( 'Show Navigation', 'wp-super-dealer' ),
		'show_results_found' => __( 'Show results found', 'wp-super-dealer' ),
		'vehicles_per_page' => __( 'Vehicles per page', 'wp-super-dealer' ),
		'full' => __( 'Full', 'wp-super-dealer' ),
		'list' => __( 'List', 'wp-super-dealer' ),
		'grid' => __( 'Grid', 'wp-super-dealer' ),
		'_full' => __( 'full', 'wp-super-dealer' ),
		'_list' => __( 'list', 'wp-super-dealer' ),
		'_grid' => __( 'grid', 'wp-super-dealer' ),
		'_yes' => __( 'yes', 'wp-super-dealer' ),
		'_no' => __( 'no', 'wp-super-dealer' ),
		'yes' => __( 'Yes', 'wp-super-dealer' ),
		'no' => __( 'No', 'wp-super-dealer' ), /* end inventory strings */
		'template_builder_label' => __( 'Template ID', 'wp-super-dealer' ),
		'switch_styles_label' => __( 'Add Switch Styles Icon', 'wp-super-dealer' ), /* end template builder integration */
        'reorder_fields' => __( 'Reorder Fields', 'wp-super-dealer' ),
        'reorder_fields_tip' => __( 'Drag the fields into the order you wish them displayed in your form. If you change which fields are active then you may need to update your order again.', 'wp-super-dealer' ),
	);
	$js_strings = array_merge( $js_strings, $js_strings_inventory );

	$js_strings_search = array(
		'search_title' => __( 'Vehicle Search Form', 'wp-super-dealer' ),
		'panel_title' => __( 'Form Settings', 'wp-super-dealer' ),
		'style' => __( 'Form Style', 'wp-super-dealer' ),
        'tags_title' => __( 'Title for selected tags group', 'wp-super-dealer' ),
        'tags_button' => __( 'Label for button in tags group', 'wp-super-dealer' ),
        'option_is_field_name' => __( 'Make first select item the field name', 'wp-super-dealer' ),
        'simple_selects' => __( 'Use simple dropdowns', 'wp-super-dealer' ),
        'form_style_one' => __( 'One', 'wp-super-dealer' ),
		'form_style_two' => __( 'Two', 'wp-super-dealer' ),
		'form_style_three' => __( 'Three', 'wp-super-dealer' ),
		'form_style_four' => __( 'Four', 'wp-super-dealer' ),
		'_form_style_one' => __( 'one', 'wp-super-dealer' ),
		'_form_style_two' => __( 'two', 'wp-super-dealer' ),
		'_form_style_three' => __( 'three', 'wp-super-dealer' ),
		'_form_style_four' => __( 'four', 'wp-super-dealer' ),
		'field_map' => __( 'Field Order', 'wp-super-dealer' ),
		'hide_search_button' => __( 'Hide Search Button', 'wp-super-dealer' ),
		'search_result_page' => __( 'Search Result Page', 'wp-super-dealer' ),
		'submit_label' => __( 'Search Button', 'wp-super-dealer'),
        'active_fields' => __( 'Active Fields', 'wp-super-dealer' ),
        'active_fields_hidden' => __( 'Active Fields Hidden', 'wp-super-dealer' ),
        'field_settings' => __( 'Field Settings', 'wp-super-dealer' ),
        'hidden_fields_map' => __( 'Hidden Fields Map', 'wp-super-dealer' ),
        'hidden_field_map' => __( 'Hidden Field Map', 'wp-super-dealer' ),
        'select_box' => __( 'Select Box', 'wp-super-dealer' ),
        'select_box_value' => __( 'select', 'wp-super-dealer' ),
        'checkboxes' => __( 'Checkboxes', 'wp-super-dealer' ),
        'checkboxes_value' => __( 'checkbox', 'wp-super-dealer' ),
        'range_selector' => __( 'Range Selector', 'wp-super-dealer' ),
        'range_selector_value' => __( 'range', 'wp-super-dealer' ),
        'custom_class' => __( 'Custom CSS Class', 'wp-super-dealer' ),
        'custom' => __( 'custom', 'wp-super-dealer' ),
	);
	$js_strings = array_merge( $js_strings, $js_strings_search );

	$js_strings_calculator = array(
		'block_title_finance_calculator' => __( 'Vehicle Finance Calculator', 'wp-super-dealer' ),
		'finance_title' => __( 'Calculator Title', 'wp-super-dealer' ),
		'finance_price' => __( 'Price', 'wp-super-dealer' ),
        'finance_down_payment' => __( 'Down Payment', 'wp-super-dealer' ),
		'finance_apr' => __( 'APR', 'wp-super-dealer' ),
		'finance_term' => __( 'Term', 'wp-super-dealer' ),
		'finance_disclaimer_1' => __( 'Disclaimer #1', 'wp-super-dealer' ),
		'finance_disclaimer_2' => __( 'Disclaimer #2', 'wp-super-dealer' ),
	);
	$js_strings = array_merge( $js_strings, $js_strings_calculator );

	wp_localize_script( 'wpsd-blocks', 'wpsdBlocksParams', array(
		'strings' => $js_strings,
		'template_builder_installed' => $template_builder_installed,
        'search_fields' => wpsdsp_active_fields(),
        'icon_inventory' => WPSD_PATH . 'blocks/images/cars-inventory.svg',
        'icon_search' => WPSD_PATH . 'blocks/images/search-vehicles.svg',
        'icon_calendar' => WPSD_PATH . 'blocks/images/calculator.svg',
	) );
	
	wp_enqueue_script( 'wpsd-blocks' );

	register_block_type( 'wp-super-dealer/wpsd-inventory', array(
		'editor_script' => 'wpsd-blocks',
		'attributes' => array(
			'stock' => array(
				'type' => 'string',
			),
			'condition' => array(
				'type' => 'string',
			),
			'year' => array(
				'type' => 'string',
			),
			'make' => array(
				'type' => 'string',
			),
			'model' => array(
				'type' => 'string',
			),
			'location' => array(
				'type' => 'string',
			),
			'body_style' => array(
				'type' => 'string',
			),
			'mileage' => array(
				'type' => 'string',
			),
			'min_price' => array(
				'type' => 'string',
			),
			'max_price' => array(
				'type' => 'string',
			),
			'transmission' => array(
				'type' => 'string',
			),
			'vehicle_tag' => array(
				'type' => 'string',
			),
			'show_sold' => array(
				'type' => 'string',
			),
			'show_only_sold' => array(
				'type' => 'string',
			),
			'criteria' => array(
				'type' => 'string',
			),
			'default_style' => array(
				'type' => 'string',
				'default' => $wpsd_options['default_style'],
			),
			'show_sort' => array(
				'type' => 'string',
				'default' => $wpsd_options['show_sort'],
			),
			'show_switch_style' => array(
				'type' => 'string',
				'default' => $wpsd_options['show_switch_style'],
			),
			'show_navigation' => array(
				'type' => 'string',
				'default' => $wpsd_options['show_navigation'],
			),
			'show_results_found' => array(
				'type' => 'string',
				'default' => $wpsd_options['show_results_found'],
			),
			'vehicles_per_page' => array(
				'type' => 'string',
				'default' => $wpsd_options['vehicles_per_page'],
			),
			'template_id' => array(
				'type' => 'string',
			),
		),
		'render_callback' => 'wpsd_inventory_shortcode_func'
	) );

    $attributes = array(
        'title' => array(
            'type' => 'string',
        ),
        'style' => array(
            'type' => 'string',
            'default' => 'one',
        ),
        'custom_class' => array(
            'type' => 'string',
            'default' => '',
        ),
        'show_sort' => array(
            'type' => 'string',
        ),
        'tags_title' => array(
            'type' => 'string',
            'default' => __( 'Show me these vehicles:', 'wp-super-dealer' ),
        ),
        'tags_button' => array(
            'type' => 'string',
            'default' => __( 'Find Now', 'wp-super-dealer' ),
        ),
        'option_is_field_name' => array(
            'type' => 'string',
            'default' => __( 'no', 'wp-super-dealer' ),
        ),
        'simple_selects' => array(
            'type' => 'string',
            'default' => __( '', 'wp-super-dealer' ),
            'content' => ''
        ),
        'result_page' => array(
            'type' => 'string',
            'default' => $wpsd_options['inventory_page'],
        ),
        'submit_label' => array(
            'type' => 'string',
            'default' => __( 'Search', 'wp-super-dealer' ),
        ),
        'hide_search_button' => array(
            'type' => 'string',
            'default' => __( 'no', 'wp-super-dealer' ),
        ),
        'field_map' => array(
            'type' => 'string',
        ),
    );
    
    $fields = wpsdsp_active_fields();
	$field_atts = array();

    $field_options = wpsdsp_active_field_options();
    
	foreach( $fields as $field=>$field_settings ) {
        $attributes[ $field ] = array(
            'type' => 'string',
            'default' => true,
        );
        $cnt = 0;

        foreach( $field_options as $field_name=>$options ) {
            ++$cnt;
            if ( $cnt < 15 ) {
            $attributes[ $field . '_' . $field_name ] = array(
                'type' => 'string',
                'default' => '',
                );
            }
        }

	}
    
    register_block_type( 'wp-super-dealer/wpsd-search', array(
		'editor_script' => 'wpsd-blocks',
		'attributes' => $attributes,
		'render_callback' => 'wpsdsp_search_block_func'
	) );

	register_block_type( 'wp-super-dealer/wpsd-calculator', array(
		'editor_script' => 'wpsd-blocks',
		'attributes' => array(
			'title' => array(
				'type' => 'string',
				'default' => __( 'Loan Calculator', 'wp-super-dealer' ),
			),
			'price' => array(
				'type' => 'string',
				'default' => '25000',
			),
			'down' => array(
				'type' => 'string',
				'default' => '1500',
			),
			'apr' => array(
				'type' => 'string',
				'default' => '10',
			),
			'term' => array(
				'type' => 'string',
				'default' => '60',
			),
			'disclaimer1' => array(
				'type' => 'string',
				'default' => __( 'It is not an offer for credit nor a quote.', 'wp-super-dealer' ),
			),
			'disclaimer2' => array(
				'type' => 'string',
				'default' => __( 'This calculator provides an estimated monthly payment. Your actual payment may vary based upon your specific loan and final purchase price.', 'wp-super-dealer' ),
			),
		),
		'render_callback' => 'wpsd_calculator_form'
	) );

}
add_action( 'init', 'wpsd_block_init' );

function wpsdsp_search_block_func( $atts ) {
    $x = '';

    //= Make an $args variable to store the arguements we'll use to build the form
    $args = array();
    
    $core_fields = array(
        'title',
        'custom_class',
        'style',
        'label_reset',
        'form_action',
        'result_page',
        'tags_title',
        'tags_button',
        'option_is_field_name',
        'simple_selects',
        'submit_label',
        'field_map',
        'sliders',
        'show_sort',
        'hide_search_button'
    );
    foreach( $core_fields as $field ) {
        if ( ! isset( $atts[ $field ] ) ) {
            continue;
        }
        $args[ $field ] = $atts[ $field ];
    }

    //= Get all of the active search fields
    $fields = wpsdsp_active_fields();
	$field_atts = array();

    $field_options = wpsdsp_active_field_options();
  
    $active_fields = '';
    $all_field_args = '';
	foreach( $fields as $field=>$field_settings ) {
        if ( ! isset( $atts[ $field ] ) || empty( $atts[ $field ] ) ) {
            continue;
        }
        $active_fields .= '|' . $field;

        //= Build the settings for each field
        $fld_args = ' ';
        foreach( $field_settings as $field_name=>$options ) {
            if ( ! isset( $atts[ $field . '_' . $field_name ] ) || empty( $atts[ $field . '_' . $field_name ] ) ) {
                continue;
            }

            if ( ! empty( $fld_args ) && ' ' !== $fld_args ) {
                $fld_args .= '&' . $field_name .'=' . $atts[ $field . '_' . $field_name];
            } else {
                $fld_args .= $field_name .'=' . $atts[ $field . '_' . $field_name];
            }
        }
        if ( ! empty( $fld_args ) && ' ' !== $fld_args ) {
            $fld_args = trim( $fld_args );
            $fld_args = $field . '="' . $fld_args . '"';
            $all_field_args .= ' ' . $fld_args;
        }
    }

    if ( ! empty( $active_fields ) && ( ! isset( $args['field_map'] ) || empty( $args['field_map'] ) ) ) {
        $args['field_map'] = $active_fields;
    } else {
        
        if ( ! empty( $active_fields ) && isset( $args['field_map'] ) && ! empty( $args['field_map'] ) 
            && ( false !== strpos( $active_fields, '|' ) )
            && ( false !== strpos( $args['field_map'], '|' ) )
            )
        {
            $new_active_fields = '';
            $active_fields_array = explode( '|', $active_fields );
            $active_map_array = explode( '|', $args['field_map'] );
            foreach( $active_map_array as $field ) {
                if ( empty( $field ) ) {
                    continue;
                }
                $new_active_fields .= $field.'|';
                unset( $active_fields_array[ $field ] );
            }
            if ( 0 < count( $active_fields_array ) ) {
                foreach( $active_fields_array as $field ) {
                    if ( empty( $field ) ) {
                        continue;
                    }
                    $new_active_fields .= $field.'|';
                }
            }
            $new_active_fields .= '##';
            $new_active_fields = str_replace( '|##', '', $new_active_fields );
            $new_active_fields = str_replace( '##', '', $new_active_fields );
            $args['field_map'] = $new_active_fields;
        }
        
    }

    $args_str = '';

    if ( isset( $args['result_page'] ) && ! empty( $args['result_page'] ) ) {
        //= if we set a result page then let's make it the form action as well
        $args['form_action'] = $args['result_page'];
    }
    
    //= build the args for the shortcode
    foreach( $args as $arg=>$value ) {
        $args_str .= ' ' . $arg . '="' . $value . '"';
    }

    $args_str .= ' ' . $all_field_args;
    $shortcode = '[wpsd_search ' . $args_str . ']';

    $x .= do_shortcode( $shortcode );
    
    return $x;
}
?>