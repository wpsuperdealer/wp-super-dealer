<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class WPSD_REST_Controller extends WP_REST_Controller {
    //= TO DO Future: finish rest routes
  /**
   * Register the routes for the objects of the controller.
   * https://developer.wordpress.org/rest-api/extending-the-rest-api/controller-classes/
   * https://developer.wordpress.org/rest-api/extending-the-rest-api/adding-custom-endpoints/
   * https://developer.wordpress.org/rest-api/extending-the-rest-api/adding-rest-api-support-for-custom-content-types/
   * https://developer.wordpress.org/rest-api/extending-the-rest-api/adding-rest-api-support-for-custom-content-types/#registering-a-custom-post-type-with-rest-api-support
   * https://developer.wordpress.org/rest-api/extending-the-rest-api/adding-rest-api-support-for-custom-content-types/#registering-a-custom-taxonomy-with-rest-api-support
   */
	public function register_routes() {
		$version = '1';
		$namespace = 'wpsd/v' . $version;
		$base = 'inventory';

		register_rest_route( $namespace, '/' . $base, array(
			array(
				'methods'             => WP_REST_Server::READABLE,
				'callback'            => array( $this, 'get_items' ),
				'permission_callback' => array( $this, 'get_items_permissions_check' ),
				'args'                => array(

				),
				'schema' => 'wpsd_get_inventory_schema',
			),
			array(
				'methods'             => WP_REST_Server::CREATABLE,
				'callback'            => array( $this, 'create_item' ),
				'permission_callback' => array( $this, 'create_item_permissions_check' ),
				'args'                => $this->get_endpoint_args_for_item_schema( true ),
			),
		) );

		register_rest_route( $namespace, '/' . $base . '/(?P<id>[\d]+)', array(
		  array(
			'methods'             => WP_REST_Server::READABLE,
			'callback'            => array( $this, 'get_item' ),
			'permission_callback' => array( $this, 'get_item_permissions_check' ),
			'args'                => array(
			  'context' => array(
				'default' => 'view',
			  ),
			),
		  ),
		  array(
			'methods'             => WP_REST_Server::EDITABLE,
			'callback'            => array( $this, 'update_item' ),
			'permission_callback' => array( $this, 'update_item_permissions_check' ),
			'args'                => $this->get_endpoint_args_for_item_schema( false ),
		  ),
		  array(
			'methods'             => WP_REST_Server::DELETABLE,
			'callback'            => array( $this, 'delete_item' ),
			'permission_callback' => array( $this, 'delete_item_permissions_check' ),
			'args'                => array(
			  'force' => array(
				'default' => false,
			  ),
			),
		  ),
		) );
/*
		register_rest_route( $namespace, '/' . $base . '/schema', array(
		  'methods'  => WP_REST_Server::READABLE,
		  'callback' => array( $this, 'wpsd_get_public_item_schema' ),
		) );
*/
	}

	/**
	* Get a collection of items
	*
	* @param WP_REST_Request $request Full data about the request.
	* @return WP_Error|WP_REST_Response
	*/
	public function get_items( $request ) {
		$query[ WPSD_POST_TYPE ] = 1;
		if ( isset( $atts['query'] ) ) {
			if ( ! empty( $atts['query'] ) ) {
				if ( strpos( $atts['query'], ',' ) !== false ) {
					$query_parts = explode( ',', $atts['query'] );
				} else {
					$query_parts = array( $atts['query'] );
				}
				foreach ( $query_parts as $query_part ) {
					$part = explode( '=', $query_part );
					if ( ! empty( $part[1] ) ) {
						if ( trim( $part[0] ) != 'stock' && trim( $part[0] ) != 'mileage' && trim( $part[0] ) != 'min_price'  && trim( $part[0] ) != 'max_price'  && trim( $part[0] ) != 'show_sold' && trim( $part[0] ) != 'show_only_sold' && trim( $part[0] ) != 'criteria' && trim( $part[0] ) != 'vehicles_per_page' ) {
							$query['search_'.trim( $part[0] )] = $part[1];
						} else {
							if ( trim( $part[0] ) == 'mileage' ) {
								$query['search_mileage'] = $part[1];
							} else if ( trim( $part[0] ) == 'criteria' ) {
								$query['criteria'] = $part[1];
							} else if ( trim( $part[0] ) == 'min_price' ) {
								$query['search_price_min'] = $part[1];
							} else if ( trim( $part[0] ) == 'max_price' ) {
								$query['search_price_max'] = $part[1];
							} else if ( trim( $part[0] ) == 'vehicles_per_page' ) {
								$query['vehicles_per_page'] = $part[1];
							} else {
								$query[trim( $part[0] )] = $part[1];	
							}
						}
					}
				} //= end foreach ( $query_parts as $query_part )
			}
		}
		$wpsd_query = wpsd_query_search( $query );

		if ( isset( $query['show_only_sold'] ) ) {
			if ( ucwords( $query['show_only_sold'] ) === 'Yes' ) {
				$wpsd_query['meta_query'] = array(
					'key' => __( 'sold', 'wp-super-dealer' ),
					'value' => __( 'No', 'wp-super-dealer' ),
					'compare' => '!='
				);
			}
		}

		unset($wpsd_query['pagename']);
		$vehicle_query = new WP_Query();
		$vehicle_query->query( $wpsd_query );

		$data = array();
		//foreach( $items as $item ) {
		if ( $vehicle_query->have_posts() ) : while ( $vehicle_query->have_posts() ) : $vehicle_query->the_post();
			$post_id = $vehicle_query->post->ID;
			$item = wpsd_get_car( $post_id );
			$itemdata = $this->prepare_item_for_response( $item, $request );
			$data[] = $this->prepare_response_for_collection( $itemdata );

			endwhile;

		else:
			//= done
		endif;
		wp_reset_query();

		return new WP_REST_Response( $data, 200 );
	}

	/**
	* Get one item from the collection
	*
	* @param WP_REST_Request $request Full data about the request.
	* @return WP_Error|WP_REST_Response
	*/
	public function get_item( $request ) {
		//get parameters from request
		$params = $request->get_params();
		$item = array();//do a query, call another class, etc
		$item = wpsd_get_car( $params['id'] );
		$item = (object) $item;
		$data = $this->prepare_item_for_response( $item, $request );

		// return a response or error based on some conditional
		if ( 1 == 1 ) {
			return new WP_REST_Response( $data, 200 );
		} else {
			return new WP_Error( 'code', __( 'message', 'wp-super-dealer' ) );
		}
	}

	/**
	* Create one item from the collection
	*
	* @param WP_REST_Request $request Full data about the request.
	* @return WP_Error|WP_REST_Response
	*/
	public function create_item( $request ) {
		$item = $this->prepare_item_for_database( $request );

		if ( function_exists( 'slug_some_function_to_create_item' ) ) {
			$data = slug_some_function_to_create_item( $item );
			if ( is_array( $data ) ) {
				return new WP_REST_Response( $data, 200 );
			}
		}

		return new WP_Error( 'cant-create', __( 'message', 'wp-super-dealer' ), array( 'status' => 500 ) );
	}

	/**
	* Update one item from the collection
	*
	* @param WP_REST_Request $request Full data about the request.
	* @return WP_Error|WP_REST_Response
	*/
	public function update_item( $request ) {
		$item = $this->prepare_item_for_database( $request );

		if ( function_exists( 'slug_some_function_to_update_item' ) ) {
			$data = slug_some_function_to_update_item( $item );
			if ( is_array( $data ) ) {
				return new WP_REST_Response( $data, 200 );
			}
		}

		return new WP_Error( 'cant-update', __( 'message', 'wp-super-dealer' ), array( 'status' => 500 ) );
	}

	/**
	* Delete one item from the collection
	*
	* @param WP_REST_Request $request Full data about the request.
	* @return WP_Error|WP_REST_Response
	*/
	public function delete_item( $request ) {
		$item = $this->prepare_item_for_database( $request );

		if ( function_exists( 'slug_some_function_to_delete_item' ) ) {
			$deleted = slug_some_function_to_delete_item( $item );
			if ( $deleted ) {
				return new WP_REST_Response( true, 200 );
			}
		}

		return new WP_Error( 'cant-delete', __( 'message', 'wp-super-dealer' ), array( 'status' => 500 ) );
	}

	/**
	* Check if a given request has access to get items
	*
	* @param WP_REST_Request $request Full data about the request.
	* @return WP_Error|bool
	*/
	public function get_items_permissions_check( $request ) {
		return true; //<--use to make readable by all
		return current_user_can( 'manage_options' );
	}

	/**
	* Check if a given request has access to get a specific item
	*
	* @param WP_REST_Request $request Full data about the request.
	* @return WP_Error|bool
	*/
	public function get_item_permissions_check( $request ) {
		return $this->get_items_permissions_check( $request );
	}

	/**
	* Check if a given request has access to create items
	*
	* @param WP_REST_Request $request Full data about the request.
	* @return WP_Error|bool
	*/
	public function create_item_permissions_check( $request ) {
		return current_user_can( 'edit_something' );
	}

	/**
	* Check if a given request has access to update a specific item
	*
	* @param WP_REST_Request $request Full data about the request.
	* @return WP_Error|bool
	*/
	public function update_item_permissions_check( $request ) {
		return $this->create_item_permissions_check( $request );
	}

	/**
	* Check if a given request has access to delete a specific item
	*
	* @param WP_REST_Request $request Full data about the request.
	* @return WP_Error|bool
	*/
	public function delete_item_permissions_check( $request ) {
		return $this->create_item_permissions_check( $request );
	}

	/**
	* Prepare the item for create or update operation
	*
	* @param WP_REST_Request $request Request object
	* @return WP_Error|object $prepared_item
	*/
	protected function prepare_item_for_database( $request ) {
		return array();
	}

	/**
	* Prepare the item for the REST response
	*
	* @param mixed $item WordPress representation of the item.
	* @param WP_REST_Request $request Request object.
	* @return mixed
	*/
	public function prepare_item_for_response( $item, $request ) {
		return $item;
	}

	/**
	* Get the query params for collections
	*
	* @return array
	*/
	public function get_collection_params() {
		return array(
		  'page'     => array(
			'description'       => 'Current page of the collection.',
			'type'              => 'integer',
			'default'           => 1,
			'sanitize_callback' => 'absint',
		  ),
		  'per_page' => array(
			'description'       => 'Maximum number of items to be returned in result set.',
			'type'              => 'integer',
			'default'           => 10,
			'sanitize_callback' => 'absint',
		  ),
		  'search'   => array(
			'description'       => 'Limit results to those matching a string.',
			'type'              => 'string',
			'sanitize_callback' => 'sanitize_text_field',
		  ),
		);
	}

	/**
	 * Get our schema for inventory.
	 */
	public function wpsd_get_inventory_schema() {
		$schema = array(
			// This tells the spec of JSON Schema we are using which is draft 4.
			'$schema'              => 'http://json-schema.org/draft-04/schema#',
			// The title property marks the identity of the resource.
			'title'                => 'comment',
			'type'                 => 'object',
			// In JSON Schema you can specify object properties in the properties attribute.
			'properties'           => array(
				'id' => array(
					'description'  => esc_html__( 'Unique identifier for the object.', 'wp-super-dealer' ),
					'type'         => 'integer',
					'context'      => array( 'view', 'edit', 'embed' ),
					'readonly'     => true,
				),
				'author' => array(
					'description'  => esc_html__( 'The id of the user object, if author was a user.', 'wp-super-dealer' ),
					'type'         => 'integer',
				),
				'content' => array(
					'description'  => esc_html__( 'The content for the object.', 'wp-super-dealer' ),
					'type'         => 'string',
				),
			),
		);

		return $schema;
	}
	
}

//= Function to register our new routes from the controller.
function wpsd_register_wpsd_rest_routes() {
	$controller = new WPSD_REST_Controller();
	$controller->register_routes();
}

add_action( 'rest_api_init', 'wpsd_register_wpsd_rest_routes' );
?>