<?php
/**
 * Elementor oEmbed Widget.
 *
 * Elementor widget that inserts an embbedable content into the page, from any given URL.
 *
 * @since 1.0.0
 */
namespace WPSD;

use Elementor\Repeater;
use Elementor\Widget_Base;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Elementor_WPSD_Inventory_Widget extends Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve oEmbed widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'wpsuperdealer';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve oEmbed widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'WPSuperDealer', 'wp-super-dealer' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve oEmbed widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fa fa-car';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the oEmbed widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'general' ];
	}

	/**
	 * Register oEmbed widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {
        global $wpsd_options;
        
		$this->start_controls_section(
			'content_section',
			[
				'label' => __( 'Content', 'plugin-name' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'default_style',
			[
				'label' => __( 'Default Style', 'wp-super-dealer' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'input_type' => 'select',
                'options' => array(
                    'grid' => __( 'Grid', 'wp-super-dealer' ),
                    'list' => __( 'List', 'wp-super-dealer' ),
                    'full' => __( 'Full', 'wp-super-dealer' ),
                ),
                'default' => 'full',
			]
		);
        
		$this->add_control(
			'show_sort',
			[
				'label' => __( 'Show Sort', 'wp-super-dealer' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'input_type' => 'select',
                'options' => array(
                    'no' => __( 'No', 'wp-super-dealer' ),
                    'yes' => __( 'Yes', 'wp-super-dealer' ),
                ),
                'default' => 'yes',
			]
		);
        
		$this->add_control(
			'show_navigation',
			[
				'label' => __( 'Show Navigation', 'wp-super-dealer' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'input_type' => 'select',
                'options' => array(
                    'no' => __( 'No', 'wp-super-dealer' ),
                    'yes' => __( 'Yes', 'wp-super-dealer' ),
                ),
                'default' => 'yes',
			]
		);
        
		$this->add_control(
			'show_switch_style',
			[
				'label' => __( 'Show Switch Style', 'wp-super-dealer' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'input_type' => 'select',
                'options' => array(
                    'no' => __( 'No', 'wp-super-dealer' ),
                    'yes' => __( 'Yes', 'wp-super-dealer' ),
                ),
                'default' => 'yes',
			]
		);
        
		$this->add_control(
			'show_results_found',
			[
				'label' => __( 'Show Results Found', 'wp-super-dealer' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'input_type' => 'select',
                'options' => array(
                    'no' => __( 'No', 'wp-super-dealer' ),
                    'yes' => __( 'Yes', 'wp-super-dealer' ),
                ),
                'default' => 'yes',
			]
		);
        
        if ( ! isset( $wpsd_options['vehicles_per_page'] ) ) {
            $wpsd_options['vehicles_per_page'] = '9';
        }
        
		$this->add_control(
			'vehicles_per_page',
			[
				'label' => __( 'Vehicles Per Page', 'wp-super-dealer' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'input_type' => 'text',
                'default' => $wpsd_options['vehicles_per_page'],
                'placeholder' => $wpsd_options['vehicles_per_page'],
			]
		);
        
		$this->add_control(
			'show_sold',
			[
				'label' => __( 'Show Sold', 'wp-super-dealer' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'input_type' => 'select',
                'options' => array(
                    'no' => __( 'No', 'wp-super-dealer' ),
                    'yes' => __( 'Yes', 'wp-super-dealer' ),
                ),
                'default' => 'no',
			]
		);

		$this->add_control(
			'show_only_sold',
			[
				'label' => __( 'Show Only Sold', 'wp-super-dealer' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'input_type' => 'select',
                'options' => array(
                    'no' => __( 'No', 'wp-super-dealer' ),
                    'yes' => __( 'Yes', 'wp-super-dealer' ),
                ),
                'default' => 'no',
			]
		);

        //= Is Template Manager installed?
        if ( function_exists( 'wpsdtm_setup' ) ) {
            $this->add_control(
                'template_id',
                [
                    'label' => __( 'Template ID', 'wp-super-dealer' ),
                    'type' => \Elementor\Controls_Manager::TEXT,
                    'input_type' => 'text',
                    'default' => '',
                    'placeholder' => 'exp: 2378',
                ]
            );
        }
        
		$this->end_controls_section();
	}

	/**
	 * Render widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {

        $fields = array(
            'default_style' => 1,
            'show_sort' => 1,
            'show_navigation' => 1,
            'show_switch_style' => 1,
            'show_results_found' => 1,
            'vehicles_per_page' => 1,
            'show_sold' => 1,
            'show_only_sold' => 1,
        );
        
        //= Is Template Manager installed?
        if ( function_exists( 'wpsdtm_setup' ) ) {
            $fields['template_id'] = 1;
        }

        $settings = $this->get_settings_for_display();
        $params = '';
        foreach( $settings as $key=>$value ) {
            if ( ! isset( $fields[ $key ] ) ) {
                continue;
            }
            $params .= ' ' . $key . '=' . $value;
        }
        $shortcode = '[wpsd_inventory' . $params . ']';

		echo '<div class="wpsuperdealer-elementor-widget">';
            echo do_shortcode( esc_html( $shortcode ) );
		echo '</div>';

	}

}