<?php
/**
 * Class WPSD_FIELDS
 *
 * Output forms and form fields.
 *
 * @package WP_SUPER_DEALER\WPSD_FIELDS
 * @author  WPSuperDealer
 */

namespace WP_SUPER_DEALER;

/*
 * Exit if called directly.
 */
if ( ! defined( 'WPINC' ) ) {
	die;
}

class WPSD_FIELDS {

	/**
	 * Construct the class
	*/
	function __construct() {
		//= Nothing to construct
	}

	/**
	 * Give it an array of fields and it will return the HTML for them
	 *
	 * @param $fields - array of fields - return public function default_custom_fields() for example
	*/
	public function get_form_fields( $fields ) {
		$html = '';
		foreach( $fields as $field_key=>$field_data ) {
			if ( ! isset( $field_data['id'] ) ) {
				$field_data['id'] = time();
			}
			$field_data['id'] = $field_key;
			$html .= self::get_form_field( $field_key, $field_data );
		}
		return $html;
	}

	/**
	 * Give it an array of field settings and it will return the HTML for it
	 *
	 * @param $field_key - the unique key/slug you want to give the field - exp: first_name
	 * @param $field_data - settings for this field - return public function default_custom_fields() for example of different field types
	*/
	public function get_form_field( $field_key, $field_data ) {
		$html = '';
        $field_html = '';

		if ( ! isset( $field_data['type'] ) ) {
			$html .= __( 'No field type sent', 'wp-super-dealer-template-builder' );
			$html .= '<pre>';
				$html .= print_r( $field_data, true );
			$html .= '</pre>';
		}

		if ( 'text' === $field_data['type'] ) {
			$field_html = self::get_field_text( $field_data );
			$html .= self::field_wrap( $field_html, $field_key, $field_data );
		} else if ( 'number' === $field_data['type'] ) {
			$field_html = self::get_field_text( $field_data, 'number' );
			$html .= self::field_wrap( $field_html, $field_key, $field_data );
        } else if ( 'email' === $field_data['type'] ) {
			$field_html = self::get_field_text( $field_data, 'email' );
			$html .= self::field_wrap( $field_html, $field_key, $field_data );
		} else if ( 'color' === $field_data['type'] ) {
			$field_html = self::get_field_text( $field_data, 'color' );
			$html .= self::field_wrap( $field_html, $field_key, $field_data );
		} else if ( 'image' === $field_data['type'] ) {
			$field_html = self::get_field_text( $field_data, 'image' );
			$html .= self::field_wrap( $field_html, $field_key, $field_data );
		} else if ( 'textarea' === $field_data['type'] ) {
			$field_html = self::get_field_textarea( $field_data );
			$html .= self::field_wrap( $field_html, $field_key, $field_data );
		} else if ( 'select' === $field_data['type'] ) {
			$field_html = self::get_field_select( $field_data );
			$html .= self::field_wrap( $field_html, $field_key, $field_data );
		} else if ( 'maybe_select' === $field_data['type'] ) {
			$field_html = self::get_field_maybe_select( $field_data );
			$html .= self::field_wrap( $field_html, $field_key, $field_data );
		} else if ( 'radio' === $field_data['type'] ) {
			$field_html = self::get_field_radio( $field_data );
			$html .= self::field_wrap( $field_html, $field_key, $field_data );
		} else if ( 'checkbox' === $field_data['type'] ) {
			$field_html = self::get_field_checkbox( $field_data );
			$html .= self::field_wrap( $field_html, $field_key, $field_data );
		} else if ( 'toggle' === $field_data['type'] ) {
			$field_html = self::get_field_toggle( $field_data );
			$html .= self::field_wrap( $field_html, $field_key, $field_data );
		} else if ( 'button' === $field_data['type'] ) {
			$field_html = self::get_field_button( $field_data );
			$html .= self::field_wrap( $field_html, $field_key, $field_data );
		} else if ( 'comment' === $field_data['type'] ) {
			$field_html = self::get_field_comment( $field_data );
			$html .= self::field_wrap( $field_html, $field_key, $field_data );
		} else {
            $field_html = apply_filters( 'wpsd_custom_field_type_filter', $field_html, $field_key, $field_data );
            if ( empty( $field_html ) ) {
                $html .= __( 'Invalid field', 'wp-super-dealer-template-builder' );
            }
            $html .= self::field_wrap( $field_html, $field_key, $field_data );
		}
	//    $html .= '<div class="wpsd-clear"></div>';

		return $html;
	}

	/**
	 * Returns the wrapped and formatted HTML for a single field
	 *
	 * @param $field_html - the html for this specific field (ie. <select> or <input, etc.)
	 * @param $field_key - the unique key/slug you want to give the field - exp: first_name
	 * @param $field_data - settings for this field - return public function default_custom_fields() for example of different field types
	*/
	public function field_wrap( $field_html, $field_key, $field_data ) {
		$html = '';

		$class = 'wpsd-field-wrap wpsd-field-wrap-type-' . esc_attr( $field_data['type'] ) . ' wpsd-field-wrap-' . esc_attr( $field_data['slug'] );
		if ( isset( $field_data['class'] ) && ! empty( $field_data['class'] ) ) {
			$class .= ' ' . $field_data['class'];
		}

		$html .= '<div class="' . esc_attr( $class ) . '" data-id="' . esc_attr( $field_key ) . '" data-slug="' . esc_attr( $field_data['slug'] ) . '">';
			$html .= '<div class="wpsd-remove-field" data-id="' . esc_attr( $field_key ) . '" data-slug="' . esc_attr( $field_data['slug'] ) . '">&#215;</div>';
			if ( isset( $field_data['tip'] ) && ! empty( $field_data['tip']['content'] ) ) {
				$html .= '<div class="wpsd-tip-wrap wpsd-tip-wrap-' . esc_attr( $field_data['slug'] ) . '">';
					$html .= '<div class="wpsd-tip">';
						$html .= '<div class="wpsd-tip-title">';
							$html .= esc_html( $field_data['tip']['title'] );
						$html .= '</div>';
						$html .= '<div class="wpsd-tip-content">';
							$html .= esc_html( $field_data['tip']['content'] );
						$html .= '</div>';
					$html .= '</div>';
				$html .= '</div>';
			}
			$html .= '<label>';
				$html .= esc_html( $field_data['label'] );
			$html .= '</label>';
			$html .= '<small class="wpsd-field-tag" title="{'. esc_attr( $field_data['slug'] ) . '}">Tag: {';
				$html .= esc_html( $field_data['slug'] );
			$html .= '}</small>';
			$html .= '<div class="wpsd-clear"></div>';
			$html .= '<span>';
				$html .= $field_html;
			$html .= '</span>';
			$html .= '<div class="wpsd-clear"></div>';
		$html .= '</div>';

		return $html;
	}

	/**
	 * Returns a single basic form field defined by the $args array sent to it
	 *
	 * @param $name - the html for this specific field (ie. <select> or <input, etc.)
	 * @param $args - an array of settings for this field
	*/
	public function get_field( $name, $args = false ) {
		$html = '';
		if ( empty( $name ) ) {
			return '';
		}
		if ( ! isset( $args['name'] ) || empty( $args['name'] ) ) {
			$args['name'] = '';
		}
		if ( false === $args ) {
			$args = array(
				'label' => $name,
				'type' => 'text',
				'name' => $name,
				'value' => '',
				'options' => array(),
				'checked' => false,
			);
		}

		if ( 'text' === $args['type'] ) {
			$html .= '<input type="text" name="' . esc_attr( $args['name'] ) . '" class="wpsd-admin-' . esc_attr( $args['name'] ) . '" value="' . esc_attr( $args['value'] ) . '" />';
		} else if ( 'select' === $args['type'] ) {
			$html .= '<select name="' . esc_attr( $args['name'] ) . '" class="wpsd-admin-' . esc_attr( $args['name'] ) . '">';
				$html .= '<option value="' . esc_attr( $args['value'] ) . '"></option>';
				if ( count( $args['options'] ) > 0 ) {
					foreach( $args['options'] as $label=>$value ) {
						$html .= '<option value="' . esc_attr( $value ) . '"' . ( $args['value'] === $value ? ' selected' : '' ) . '>' . esc_html( $label ) . '</option>';
					}
				}
			$html .= '</select>';
		} else if ( 'radio' === $args['type'] ) {
			$html .= '<ul class="wpsd-admin-radio-wrapper wpsd-admin-' . esc_attr( $args['name'] ) . '">';
				if ( count( $args['options'] ) > 0 ) {
					foreach( $args['options'] as $label=>$value ) {
						$html .= '<li><span><input type="radio" name="' . esc_attr( $value ) . '" value="' . esc_attr( $value ) . '"' . ( $args['value'] === $value ? ' checked' : '' ) . '></span><label>' . esc_html( $label ) . '</label></li>';
					}
				}
			$html .= '</ul>';
		} else if ( 'checkbox' === $args['type'] ) {
			$html .= '<ul class="wpsd-admin-radio-wrapper wpsd-admin-' . $args['name'] . '">';
				if ( count( $args['options'] ) > 0 ) {
					foreach( $args['options'] as $label=>$value ) {
						$html .= '<li><span><input type="checkbox" name="' . esc_attr( $value ) . '" value="' . esc_attr( $value ) . '"' . ( $args['value'] === $value ? ' checked' : '' ) . '></span><label>' . esc_html( $label ) . '</label></li>';
					}
				}
			$html .= '</ul>';
		} else if ( 'textarea' === $args['type'] ) {
			$html .= '<textarea name="' . esc_attr( $args['name'] ) . '" class="wpsd-admin-' . esc_attr( $args['name'] ) . '">';
				$html .= esc_html( $args['value'] );
			$html .= '</textarea>';
		}

		//= Put it all together
		$html = '<label>' . esc_html( $args['label'] ) . '</label><span>' . esc_html( $html ) . '</span>';
		$html = '<div class="wpsd-admin-field-wrap">' . esc_html( $html ) . '</div>';

		return $html;
	}
	
	//=============================================
	//= Start Field Types
	//=============================================

	/**
	 * Returns HTML for input text, image & color fields
	 *
	 * @param $field_data - settings for this field - return public function default_custom_fields() for example of different field types
	 * @param $type - text, image or color
	*/
	public function get_field_text( $field_data, $type = 'text' ) {
		$html = '';
        $field_type = 'text';

		$value = '';
		if ( isset( $field_data['value'] ) ) {
			$value = $field_data['value'];
		}

		$placeholder = '';
		if ( isset( $field_data['placeholder'] ) ) {
			$placeholder = $field_data['placeholder'];
		}
		$class = '';
		$image = '';

		if ( ! isset( $field_data['key'] ) ) {
			$field_data['key'] = '';
		}

		$required = '';
		if ( isset( $field_data['required'] ) && true === $field_data['required'] ) {
			$required = ' required';
		}
		
		if ( 'color' === $type ) {
			$class = 'color ';
		} else if ( 'image' === $type ) {
			$class = 'image ';
			$id_name = 'cdff-image-src-' . $field_data['slug'];
			if ( 'SLUG-VALUE' === $value ) {
				$value = '';
			}
			$image = '<div class="cdff-image-wrap cdff-image-wrap-' . esc_attr( $field_data['slug'] ) . '">';
				$image .= '<i class="fa fa-fw fa-eye">';
					$image .= '<img name="' . esc_attr( $id_name ) . '" id="' . esc_attr( $id_name ) . '" src="' . esc_attr( $value ) . '" />';
				$image .= '</i>';
			$image .= '</div>';
		} else {
			$field_type = $type;
		}
		if ( isset( $field_data['id_name'] ) ) {
			$id_name = $field_data['id_name'];
		} else {
			$id_name = 'cdff-' . $type . '-' . $field_data['slug'];
		}
		$html .= '<input name="' . esc_attr( $id_name ) . '" id="' . esc_attr( $id_name ) . '" class="' . esc_attr( $class ) . 'cdff cdff-' . esc_attr( $type ) . ' cdff-' . esc_attr( $type ) . '-' . esc_attr( $field_data['slug'] ) . '" type="' . esc_attr( $field_type ) . '" data-slug="' . esc_attr( $field_data['slug'] ) . '" data-key="' . esc_attr( $field_data['key'] ) . '" value="' . esc_attr( $value ) . '" placeholder="' . esc_attr( $placeholder ) . '"' . esc_attr( $required ) . ' />';
		$html .= $image;
		return $html;
	}

	/**
	 * Returns HTML for a textarea field
	 *
	 * @param $field_data - settings for this field - return public function default_custom_fields() for example of different field types
	*/
	public function get_field_textarea( $field_data ) {
		$html = '';

		$value = '';
		if ( isset( $field_data['value'] ) ) {
			$value = $field_data['value'];
		}

		$placeholder = '';
		if ( isset( $field_data['placeholder'] ) ) {
			$placeholder = $field_data['placeholder'];
		}

		$html .= '<textarea class="cdff cdff-textarea cdff-textarea-' . esc_attr( $field_data['slug'] ) . '" placeholder="' . esc_attr( $placeholder ) . '">';
			if ( isset( $field_data['value'] ) ) {
				$html .= esc_html( $field_data['value'] );
			}
		$html .= '</textarea>';

		return $html;
	}

	/**
	 * Returns HTML for a select field
	 *
	 * @param $field_data - settings for this field - return public function default_custom_fields() for example of different field types
	*/
	public function get_field_select( $field_data ) {
		$html = '';

		$value = '';
		if ( isset( $field_data['value'] ) ) {
			$value = $field_data['value'];
		}

		$placeholder = '';
		if ( isset( $field_data['placeholder'] ) ) {
			$placeholder = $field_data['placeholder'];
		}

		if ( isset( $field_data['id_name'] ) ) {
			$id_name = $field_data['id_name'];
		} else {
			$id_name = 'cdff-select-' . $field_data['slug'];
		}

		if ( ! isset( $field_data['key'] ) ) {
			$field_data['key'] = '';
		}

		$required = '';
		if ( isset( $field_data['required'] ) && true === $field_data['required'] ) {
			$required = ' required';
		}

		$html .= '<select name="'. esc_attr( $id_name ) . '" id="'. esc_attr( $id_name ) . '" data-key="' . esc_attr( $field_data['key'] ) . '" class="cdff cdff-select cdff-select-' . esc_attr( $field_data['slug'] ) . '" placeholder="' . esc_attr( $placeholder ) . '"' . esc_attr( $required ) . '>';
			if ( isset( $field_data['options'] ) && is_array( $field_data['options'] ) ) {
				//$html .= $field_data['value'];
				foreach( $field_data['options'] as $key=>$data ) {
					$html .= '<option value="' . esc_attr( $data['value'] ) . '"' . ( $field_data['value'] === $data['value'] ? ' selected' : '' ) . '>';
						$html .= esc_html( $data['label'] );
					$html .= '</option>';
				}
			}
		$html .= '</select>';

		return $html;
	}

	/**
	 * Returns HTML for a select field
	 *
	 * @param $field_data - settings for this field - return public function default_custom_fields() for example of different field types
	*/
	public function get_field_maybe_select( $field_data ) {
		$html = '';

		$value = '';
		if ( isset( $field_data['value'] ) ) {
			$value = $field_data['value'];
		}

		$placeholder = '';
		if ( isset( $field_data['placeholder'] ) ) {
			$placeholder = $field_data['placeholder'];
		}

		if ( isset( $field_data['id_name'] ) ) {
			$id_name = $field_data['id_name'];
		} else {
			$id_name = 'cdff-maybe-select-' . $field_data['slug'];
		}
		$html .= '<div class="cdff cdff-maybe-select cdff-maybe-select-' . esc_attr( $field_data['slug'] ) . '" placeholder="' . esc_attr( $placeholder ) . '">';
			if ( isset( $field_data['options'] ) && is_array( $field_data['options'] ) ) {
				$html .= '<input type="text" name="' . esc_attr( $id_name ) . '-text" autocomplete="off" class="cdff-maybe-select-text" value="' . esc_attr( $field_data['value'] ) . '" />';
				$html .= '<input type="text" name="' . esc_attr( $id_name ) . '" autocomplete="off" class="cdff-maybe-select-value" value="' . esc_attr( $field_data['value'] ) . '" />';
				$options_json = json_encode( $field_data['options'] );
				$options_base64 = base64_encode( $options_json );
				$html .= '<ul class="cdff-maybe-select-option-wrap" data-options="' . esc_attr( $options_base64 ) . '">';
					foreach( $field_data['options'] as $key=>$data ) {
						$html .= '<li class="cdff-maybe-select-option" data-text="' . esc_attr( $data['label'] ) . '" data-value="' . esc_attr( $data['value'] ) . '"' . ( $field_data['value'] === $data['value'] ? ' selected' : '' ) . '>';
							$html .= esc_html( $data['label'] );
						$html .= '</li>';
					}
				$html .= '</ul>';
			}
		$html .= '</div>';

		return $html;
	}

	/**
	 * Returns HTML for a radio field
	 *
	 * @param $field_data - settings for this field - return public function default_custom_fields() for example of different field types
	*/
	public function get_field_radio( $field_data ) {
		$html = '';

		$value = '';
		if ( isset( $field_data['value'] ) ) {
			$value = $field_data['value'];
		}

		$placeholder = '';
		if ( isset( $field_data['placeholder'] ) ) {
			$placeholder = $field_data['placeholder'];
		}

		$html .= '<div class="cdff cdff-radio cdff-radio-' . esc_attr( $field_data['slug'] ) . '">';
			if ( isset( $field_data['options'] ) && is_array( $field_data['options'] ) ) {
				//$html .= $field_data['value'];
				foreach( $field_data['options'] as $key=>$data ) {
					$html .= '<div class="cdff-item cdff-item-radio" data-id="' . esc_attr( $field_data['id'] ) . '">';
						$html .= '<span data-slug="' . esc_attr( $field_data['slug'] ) . '" data-value="' . esc_attr( $data['value'] ) . '">';
							$html .= '<input type="radio" name="' . esc_attr( $field_data['slug'] ) . '" data-slug="' . esc_attr( $field_data['slug'] ) . '" class="cdff-item-radio-' . esc_attr( $field_data['slug'] ) . '" data-value="' . esc_attr( $data['value'] ) . '" value="' . esc_attr( $data['value'] ) . '"' . ( $field_data['value'] === $data['value'] ? ' checked' : '' ) . '>';
						$html .= '</span>';
						$html .= '<label>';
							$html .= esc_html( $data['label'] );
						$html .= '</label>';
					$html .= '</div>';
				}
			}
		$html .= '</div>';

		return $html;
	}

	/**
	 * Returns HTML for a checkbox field
	 *
	 * @param $field_data - settings for this field - return public function default_custom_fields() for example of different field types
	*/
	public function get_field_checkbox( $field_data ) {
		$html = '';

		$value = '';
		if ( isset( $field_data['value'] ) ) {
			$value = $field_data['value'];
		}

		$placeholder = '';
		if ( isset( $field_data['placeholder'] ) ) {
			$placeholder = $field_data['placeholder'];
		}

		$html .= '<div class="cdff cdff-checkbox cdff-checkbox-' . esc_attr( $field_data['slug'] ) . '">';
			if ( isset( $field_data['options'] ) && is_array( $field_data['options'] ) ) {
				//$html .= $field_data['value'];
				foreach( $field_data['options'] as $key=>$data ) {
					if ( ! is_array( $field_data['value'] ) ) {
						$field_data['value'] = array();
					}
					$html .= '<div class="cdff-item cdff-item-checkbox" data-id="' . esc_attr( $field_data['id'] ) . '">';
						$html .= '<span>';
                            $id_name = 'cdff-checkbox-' . $field_data['slug'];
							$html .= '<input class="cdff-checkbox-item cdff-checkbox-item-' . esc_attr( $field_data['slug'] ) . '" data-slug="' . esc_attr( $field_data['slug'] ) . '" type="checkbox" id="' . esc_attr( $field_data['slug'] ) . '" name="' . esc_attr( $id_name ) . '" value="' . esc_attr( $data['value'] ) . '"' . ( in_array( $data['value'], $field_data['value'] ) ? ' checked' : '' ) . '>';
						$html .= '</span>';
						$html .= '<label>';
							$html .= esc_html( $data['label'] );
						$html .= '</label>';
					$html .= '</div>';
                }
			}
		$html .= '</div>';

        return $html;
	}

	/**
	 * Returns HTML for a toggle/checkbox field
	 *
	 * @param $field_data - settings for this field - return public function default_custom_fields() for example of different field types
	*/
	public function get_field_toggle( $field_data ) {
		$html = '';
		$value = '';
		if ( isset( $field_data['value'] ) ) {
			$value = $field_data['value'];
		}

		$placeholder = '';
		if ( isset( $field_data['placeholder'] ) ) {
			$placeholder = $field_data['placeholder'];
		}

		$html .= '<div class="cdff cdff-checkbox cdff-checkbox-' . esc_attr( $field_data['slug'] ) . '">';
		if ( isset( $field_data['options'] ) && is_array( $field_data['options'] ) ) {
			//$html .= $field_data['value'];
			foreach( $field_data['options'] as $key=>$data ) {
				if ( ! is_array( $field_data['value'] ) ) {
					$field_data['value'] = array();
				}

				$html .= '<div class="cdff-item cdff-item-toggle" data-id="' . esc_attr( $field_data['id'] ) . '">';
					$html .= '<div class="wpsd-toggle-wrap">';
						$html .= '<label class="wpsd-toggle">';
							$id_name = 'cdff-checkbox-' . $field_data['slug'];
							$html .= '<input class="cdff-checkbox-item cdff-checkbox-item-' . esc_attr( $field_data['slug'] ) . '" data-slug="' . esc_attr( $field_data['slug'] ) . '" type="checkbox" id="' . esc_attr( $field_data['slug'] ) . '" name="' . esc_attr( $id_name ) . '" value="' . esc_attr( $data['value'] ) . '"' . ( in_array( $data['value'], $field_data['value'] ) ? ' checked' : '' ) . '>';
							$html .= '<span class="slider round"></span>';
						$html .= '</label>';
						$html .= '<div class="wpsd-toggle-title" data-action="group-specs" data-type="' . esc_attr( $type ) . '">';
							$html .= esc_html( $data['label'] );
						$html .= '</div>';
					$html .= '</div>';
				$html .= '</div>';
			}
		}
					
		return $html;
	}
	
	/**
	 * Returns HTML for a button field
	 *
	 * @param $field_data - settings for this field - return public function default_custom_fields() for example of different field types
	*/
	public function get_field_button( $field_data ) {
		$html = '';

		$value = '';
		if ( isset( $field_data['value'] ) ) {
			$value = $field_data['value'];
		}

		$placeholder = '';
		if ( isset( $field_data['placeholder'] ) ) {
			$placeholder = $field_data['placeholder'];
		}

		$html .= '<input type="button" class="cdff cdff-button cdff-button-' . esc_attr( $field_data['slug'] ) . '" value="' . esc_attr( $value ) . '" />';

		return $html;
	}

	/**
	 * Returns HTML for a comment - NOTE, this is not a form field, just an HTML block
	 *
	 * @param $field_data - settings for this field - return public function default_custom_fields() for example of different field types
	*/
	public function get_field_comment( $field_data ) {
		$html = '';

		$value = '';
		if ( isset( $field_data['value'] ) ) {
			$value = $field_data['value'];
		}

		$html .= $value;

		return $html;
	}

    /**
     * Returns a table given a simple array
     *
    */
    public function get_table( $table, $class = '' ) {
        if ( ! is_array( $table ) ) {
            return '';
        }
        //= Five lines of codes just to add a space only when needed...
        //= Well, seven lines if you include the comments ;-)
        if ( empty( $class ) ) {
            $class = 'wpsd_field_table';
        } else {
            $class = 'wpsd_field_table ' . $class;
        }

        $html = '<table class="' . esc_attr( $class ) . '">';
        foreach( $table as $row=>$column ) {
            $html .= '<tr>';
                foreach( $column as $cell ) {
                    $html .= '<td>';
                        $html .= $cell;
                    $html .= '</td>';
                }
            $html .= '</tr>';
        }
        
        $html .= '</table>';
        return $html;
    }
    
	/**
	 * Returns an array of default field data that can be used as an example of how to format the data
	 *
	*/
	public function default_custom_fields() {
		$time = time();
		$custom_fields = array(
			$time + 1 => array(
				'label' => 'SLUG-LABEL',
				'slug' => 'SLUG-SLUG',
				'type' => 'text',
				'value' => 'SLUG-VALUE',
				'class' => '',
				'tip' => array(
					'title' => __( 'Tip title', 'wp-super-dealer-template-builder' ),
					'content' => __( 'SLUG-TIP', 'wp-super-dealer-template-builder' ),
				),
				'placeholder' => 'SLUG-PLACEHOLDER',
			),
			$time + 2 => array(
				'label' => 'SLUG-LABEL',
				'slug' => 'SLUG-SLUG',
				'type' => 'textarea',
				'value' => 'SLUG-VALUE',
				'class' => '',
				'tip' => array(
					'title' => __( 'Tip title', 'wp-super-dealer-template-builder' ),
					'content' => __( 'SLUG-TIP', 'wp-super-dealer-template-builder' ),
				),
				'placeholder' => 'SLUG-PLACEHOLDER',
			),
			$time + 3 => array(
				'label' => 'SLUG-LABEL',
				'slug' => 'SLUG-SLUG',
				'type' => 'select',
				'value' => '',
				'class' => '',
				'tip' => array(
					'title' => __( 'Tip title', 'wp-super-dealer-template-builder' ),
					'content' => __( 'SLUG-TIP', 'wp-super-dealer-template-builder' ),
				),
				'options' => array(
					array(
						'label' => 'My label',
						'value' => 'my value',
					),
					array(
						'label' => 'My label 2',
						'value' => 'my value 2',
					),
				)
			),
			$time + 4 => array(
				'label' => 'SLUG-LABEL',
				'slug' => 'SLUG-SLUG',
				'type' => 'radio',
				'value' => '',
				'class' => '',
				'tip' => array(
					'title' => __( 'Tip title', 'wp-super-dealer-template-builder' ),
					'content' => __( 'SLUG-TIP', 'wp-super-dealer-template-builder' ),
				),
				'options' => array(
					array(
						'label' => 'My label',
						'value' => 'my value',
					),
					array(
						'label' => 'My label 2',
						'value' => 'my value 2',
					),
				)
			),
			$time + 5 => array(
				'label' => 'SLUG-LABEL',
				'slug' => 'SLUG-SLUG',
				'type' => 'checkbox',
				'value' => array( 'a', 'b' ),
				'class' => '',
				'tip' => array(
					'title' => __( 'Tip title', 'wp-super-dealer-template-builder' ),
					'content' => __( 'SLUG-TIP', 'wp-super-dealer-template-builder' ),
				),
				'options' => array(
					array(
						'label' => 'My label',
						'value' => 'my value',
					),
					array(
						'label' => 'My label 2',
						'value' => 'my value 2',
					),
					array(
						'label' => 'My label 3',
						'value' => 'my value 3',
					),
				)
			),
			$time + 6 => array(
				'label' => 'SLUG-LABEL',
				'slug' => 'SLUG-SLUG',
				'type' => 'button',
				'tip' => array(
					'title' => __( 'Tip title', 'wp-super-dealer-template-builder' ),
					'content' => __( 'SLUG-TIP', 'wp-super-dealer-template-builder' ),
				),
				'value' => 'SLUG',
				'class' => '',
			),
			$time + 7 => array(
				'label' => 'SLUG-LABEL',
				'slug' => 'SLUG-SLUG',
				'type' => 'comment',
				'tip' => array(
					'title' => __( 'Tip title', 'wp-super-dealer-template-builder' ),
					'content' => __( 'SLUG-TIP', 'wp-super-dealer-template-builder' ),
				),
				'value' => __( 'SLUG-VALUE', 'wp-super-dealer-template-builder' ),
				'class' => '',
			),
			$time + 8 => array(
				'label' => 'SLUG-LABEL',
				'slug' => 'SLUG-SLUG',
				'type' => 'color',
				'value' => 'SLUG-VALUE',
				'class' => '',
				'tip' => array(
					'title' => __( 'Tip title', 'wp-super-dealer-template-builder' ),
					'content' => __( 'SLUG-TIP', 'wp-super-dealer-template-builder' ),
				),
				'placeholder' => 'SLUG-PLACEHOLDER',
			),
			$time + 9 => array(
				'label' => 'SLUG-LABEL',
				'slug' => 'SLUG-SLUG',
				'type' => 'image',
				'value' => 'SLUG-VALUE',
				'class' => '',
				'tip' => array(
					'title' => __( 'Tip title', 'wp-super-dealer-template-builder' ),
					'content' => __( 'SLUG-TIP', 'wp-super-dealer-template-builder' ),
				),
				'placeholder' => 'SLUG-PLACEHOLDER',
			),
		);
		return $custom_fields;
	}

	//= Start general utility functions
	/**
	 * Attempt to get request IP
	 * Just a handy function that sometimes comes in use
	 *
	*/
	private function request_ip() {
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		
		if ( strpos( $ip, ',' ) !== false ) {
			$ips = explode( ',', $ip );
			$ip = $ips[0];
		}
		
		return $ip;
	}

	/**
	 * Save log items to WordPress debug.log
	 *
	 * @param $log - line to record in log file
	*/
    private function write_log ( $log )  {
        if ( true === WP_DEBUG ) {
            if ( is_array( $log ) || is_object( $log ) ) {
                error_log( 'MY_CLASS: ' . print_r( $log, true ) );
            } else {
                error_log( 'MY_CLASS: ' . $log );
            }
        }
    }

}
?>